//
//  HttpRequests.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//
#import "HttpRequests.h"
#import <AFNetworkReachabilityManager.h>
#import <AFHTTPSessionManager.h>

typedef void(^SuccessBlock)(NSURLSessionDataTask *operation, id responseObject);
typedef void(^FailureBlock)(NSURLSessionDataTask *operation, NSError *error);

@interface HttpRequests(){
    AppDelegate *_delegate;
    AFHTTPSessionManager *_manager;
}

@end


@implementation HttpRequests

- (id)init{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configSession
        =[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLCache *cache=[[NSURLCache alloc]initWithMemoryCapacity:3*1024*1024
                                                       diskCapacity:8*1024*1024
                                                           diskPath:nil];
        configSession.URLCache=cache;
        
        //设置缓存策略
        configSession.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        //设置超时时间
        configSession.timeoutIntervalForRequest=300;
        
        _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configSession];
        
        //无证书模式
        _manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        //设置response序列化器
        AFJSONResponseSerializer * response = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        response.removesKeysWithNullValues = YES;   //把Null-->nil
        _manager.responseSerializer = response;
        
        //接受的类型
        [_manager.responseSerializer setAcceptableContentTypes:
         [NSSet setWithObjects:
          @"text/plain",
          @"application/json",
          @"text/json",
          @"text/javascript",
          @"text/html",
          nil]];
    }
    return self;
}
+ (HttpRequests *)defaultClient{
    static HttpRequests *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters prepareExecute:(PrepareExecuteBlock)prepareExecute
                success:(void (^)(NSURLSessionDataTask *, id))success
                failure:(void (^)(NSURLSessionDataTask *, NSError *))failure{
    _delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    //预处理（显示加载信息啥的）
    if (prepareExecute) {
        prepareExecute();
        //        NSLog(@"\n请求参数：%@",parameters);
    }
    
    //检查地址中是否有中文
    NSString *urlStr=[NSURL URLWithString:url]?url:[self strUTF8Encoding:url];
    
    switch (method) {
        case HttpRequestGet:{
            [self openActivity];
            [_manager GET:urlStr parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(task,responseObject);
                [self closeActivity];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(task,error);
                if (error.code==-1001) {
                    [self showExceptionDialog:error];
                }
                [self closeActivity];
            }];
        }
            break;
            
        case HttpRequestPost:{
            [self openActivity];
//            [_manager POST:urlStr parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//                success(task,responseObject);
//                [self closeActivity];
//            } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                failure(task,error);
//                if (error.code==-1001) {
//                    [self showExceptionDialog:error];
//                }
//                [self closeActivity];
//            }];
            [_manager POST:urlStr parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(task,responseObject);
                [self closeActivity];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(task,error);
                if (error.code==-1001) {
                    [self showExceptionDialog:error];
                }
                [self closeActivity];
            }];
        }
            break;
        default:
            break;
    }
}

-(NSString *)strUTF8Encoding:(NSString *)str{
    return [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}

- (void)showExceptionDialog:(NSError *)error{
    [[[UIAlertView alloc] initWithTitle:@"提示"
                                message:error.localizedDescription
                               delegate:self
                      cancelButtonTitle:@"好的"
                      otherButtonTitles:nil, nil] show];
}

// 开启状态栏菊花
- (void)openActivity{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

// 关闭状态栏菊花
- (void)closeActivity{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
@end

