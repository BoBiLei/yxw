
//
//  MMGSign.m
//  MMG
//
//  Created by mac on 15/11/10.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "MMGSign.h"
#import <CommonCrypto/CommonDigest.h>

@implementation MMGSign

+(MMGSign *)shareSign{
    
    static MMGSign *sign;
    
    static dispatch_once_t once;
    
    dispatch_once(&once,^{
        sign=[[MMGSign alloc]init];
    });
    
    return sign;
}

//1、把字典处理成类似（a=123&d=789&f=456&）
-(NSString *)disposeDictionary:(NSDictionary *)dic{
    
    NSString *disposeString=@"";
    
    NSArray *_keyArr=[dic allKeys];
    
    NSArray *sortKeyArr = [_keyArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        
        return [obj1 compare:obj2 options:NSNumericSearch];
        
    }];
    
    for (NSString *keyStr in sortKeyArr) {
        
        NSString *keyStr01=[keyStr stringByAppendingString:@"="];
        
        NSString *str02=[NSString stringWithFormat:@"%@",[dic valueForKey:keyStr]];
        
        NSString *lastKeyValue=[NSString stringWithFormat:@"%@%@",keyStr01,[str02 isEqualToString:@"null"]?@"":str02];
        
        NSString *str=[NSString stringWithFormat:@"%@%@",disposeString,lastKeyValue];
        
        disposeString=[NSString stringWithFormat:@"%@&",str];
        
    }
    return disposeString;
}

//生产随机数
+(NSString *)randomNumber{
    
    const int N = 32;
    
    NSString *sourceString = @"0123456789abcdefghijklmnopqrstuvwxyz";
    
    NSMutableString *result = [[NSMutableString alloc] init];
    
    srand((int)time(0));
    
    for (int i = 0; i < N; i++){
        
        unsigned index = rand() % [sourceString length];
        
        NSString *s = [sourceString substringWithRange:NSMakeRange(index, 1)];
        
        [result appendString:[NSString stringWithFormat:@"%@",s]];
        
    }
    return result;
}

//md5加密后转换为大写
-(NSString *)md5Encrypt:(NSString *)params{
    
    const char *cStr = [params UTF8String];
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5( cStr, (unsigned int)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02X", digest[i]];
    
    return [output uppercaseString];
}

//3
-(NSString *)getSign:(NSString *)sec forDictionary:(NSDictionary *)dic{
    
    NSString *dicStr=[[MMGSign shareSign] disposeDictionary:dic];
    
    //将第一步处理后的字符串加上signkey=youxia1
    NSString *singSec=[NSString stringWithFormat:@"signkey=%@",sec];
    NSString *md5DisposeStr=[NSString stringWithFormat:@"%@%@",dicStr,singSec];
    
    
    return [[MMGSign shareSign] md5Encrypt:md5DisposeStr];
}

@end
