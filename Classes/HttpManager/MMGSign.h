//
//  MMGSign.h
//  MMG
//
//  Created by mac on 15/11/10.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//
/**
 ******************************
 ************签名方法***********
 ******************************
 **/
#import <Foundation/Foundation.h>

@interface MMGSign : NSObject

+(MMGSign *)shareSign;

+(NSString *)randomNumber;

-(NSString *)getSign:(NSString *)sec forDictionary:(NSDictionary *)dic;

@end
