//
//  XybgListModel.h
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XybgListModel : NSObject

@property (nonatomic,copy) NSString *bgId;
@property (nonatomic,assign) NSInteger tag;       //删除用到
@property (nonatomic,copy) NSString *bgImgStr;
@property (nonatomic,copy) NSString *bgTitle;

-(void)firstJsonDate:(NSDictionary *)dic;

-(void)secondJsonDate:(NSDictionary *)dic;

@end
