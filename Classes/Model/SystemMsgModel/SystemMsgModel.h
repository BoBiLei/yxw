//
//  SystemMsgModel.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemMsgModel : NSObject

@property (nonatomic,copy) NSString *msgId;
@property (nonatomic,copy) NSString *msgTitle;
@property (nonatomic,copy) NSString *msgTime;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
