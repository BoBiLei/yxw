//
//  SystemMsgModel.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SystemMsgModel.h"

@implementation SystemMsgModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.msgId=dic[@""];
    self.msgTitle=dic[@""];
    self.msgTime=dic[@""];
}

@end
