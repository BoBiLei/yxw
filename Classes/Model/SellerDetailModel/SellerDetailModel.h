//
//  SellerDetailModel.h
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellerDetailModel : NSObject

@property (nonatomic,copy) NSString *sellerId;
@property (nonatomic,copy) NSString *sellerName;
@property (nonatomic,copy) NSString *sellerPhoto;

//店长推荐
@property (nonatomic,copy) NSString *goodsId;
@property (nonatomic,copy) NSString *goods_num;
@property (nonatomic,copy) NSString *goodsImgStr;
@property (nonatomic,copy) NSString *goodsTitleStr;
@property (nonatomic,copy) NSString *mmPrice;//猫猫价
@property (nonatomic,copy) NSString *stagePrice;//分期价

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
