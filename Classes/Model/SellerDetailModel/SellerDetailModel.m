//
//  SellerDetailModel.m
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SellerDetailModel.h"

@implementation SellerDetailModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.goodsId=dic[@"id"];
    self.goods_num=dic[@"goods_number"];
    self.goodsImgStr=dic[@"goods_img"];
    self.goodsTitleStr=dic[@"name"];
    self.mmPrice=dic[@"shop_price"];
    self.stagePrice=dic[@"period"];
}

@end
