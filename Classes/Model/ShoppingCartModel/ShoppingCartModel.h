//
//  ShoppingCartModel.h
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingCartModel : NSObject

@property (nonatomic,copy) NSString *goodsId;
@property (nonatomic,copy) NSString *goodsImgStr;
@property (nonatomic,copy) NSString *goodsTitle;
@property (nonatomic,copy) NSString *goodsPrice;

@property(assign,nonatomic)BOOL selectState;//是否选中状态

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
