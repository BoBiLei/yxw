//
//  ShoppingCartModel.m
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ShoppingCartModel.h"

@implementation ShoppingCartModel

#pragma mark - override method

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.goodsId=dic[@"goods_id"];
    self.goodsImgStr=dic[@"goods_thumb"];
    self.goodsTitle=dic[@"goods_name"];
    self.goodsPrice=dic[@"market_price"];
}

@end
