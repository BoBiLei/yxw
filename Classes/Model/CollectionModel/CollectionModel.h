//
//  CollectionModel.h
//  MMG
//
//  Created by mac on 15/10/14.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionModel : NSObject

@property (nonatomic,copy) NSString *goodsId;
@property (nonatomic,copy) NSString *goods_num;
@property (nonatomic,copy) NSString *goodsImgStr;
@property (nonatomic,copy) NSString *goodsTitle;
@property (nonatomic,copy) NSString *goodsPrice;
@property (nonatomic,copy) NSString *collectionId;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
