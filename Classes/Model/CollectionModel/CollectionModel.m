//
//  CollectionModel.m
//  MMG
//
//  Created by mac on 15/10/14.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CollectionModel.h"

@implementation CollectionModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.goodsId=dic[@"goods_id"];
    self.goods_num=dic[@"goods_number"];
    self.goodsImgStr=dic[@"goods_thumb"];
    self.goodsTitle=dic[@"goods_name"];
    self.goodsPrice=dic[@"market_price"];
    self.collectionId=dic[@"rec_id"];
}

@end
