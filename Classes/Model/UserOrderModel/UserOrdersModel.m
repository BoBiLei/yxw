//
//  UserOrdersModel.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "UserOrdersModel.h"

@implementation UserOrdersModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.orderId=dic[@"order_id"];
    self.orderNumber=dic[@"order_sn"];
    self.couponId=dic[@"bonus_id"];
    self.couponMoney=dic[@"bonus_format"];
     //查找全部匹配的，并替换
    NSMutableString *statusStr=[NSMutableString stringWithString:dic[@"order_status"]];
    NSRange range = [statusStr rangeOfString:@"&nbsp;"];
    while (range.location != NSNotFound) {
        [statusStr replaceCharactersInRange:range withString:@" "];
        range = [statusStr rangeOfString:@"&nbsp;"];
    }
    self.headerStatus=statusStr;
   
    NSString *totalFeeStr=dic[@"total_fee"];
    NSString *stageFeeStr=dic[@"fenqi_fee"];
    CGFloat totalFeeFloat=totalFeeStr.floatValue;
    CGFloat stageFeeFloat=stageFeeStr.floatValue;
    CGFloat totalFee=totalFeeFloat+stageFeeFloat;
    self.orderTotalPrice=[NSString stringWithFormat:@"%.f",totalFee];
    
    self.orderStatus=dic[@"order_status1"];
    self.goodSum=dic[@"orders_goods_nums"];
    self.totalFee=dic[@"total_fee"];
    self.stageFee=dic[@"fenqi_fee"];
    self.stageSum=dic[@"fenqi"];
    self.stageOnePrice=dic[@"total_fee_true"];
    
    self.goodModelArr=dic[@"orders_goods"];
}

-(void)goodsDataForDictonary:(NSDictionary *)dic{
    self.goodId=dic[@"goods_id"];
    self.goodImg=dic[@"goods_thumb"];
    self.goodTitle=dic[@"goods_name"];
    self.goodPrice=dic[@"goods_price"];
}

@end
