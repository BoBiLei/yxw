//
//  UserOrdersModel.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserOrdersModel : NSObject

@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *orderNumber;     //订单编号
@property (nonatomic,copy) NSString *headerStatus;
@property (nonatomic,copy) NSString *orderStatus;     //订单状态
@property (nonatomic,copy) NSString *goodSum;     //商品数量
@property (nonatomic,copy) NSString *totalFee;     //总金额
@property (nonatomic,copy) NSString *stageFee;     //分期手续费
@property (nonatomic,copy) NSString *couponId;     //优惠券ID
@property (nonatomic,copy) NSString *couponMoney;     //优惠券ID
@property (nonatomic,copy) NSString *stageSum;     //分期数
@property (nonatomic,copy) NSString *stageOnePrice;//每期金额

@property (nonatomic,copy) NSString *orderTotalPrice;//订单总金额

@property (nonatomic,copy) NSString *goodId;
@property (nonatomic,copy) NSString *goodImg;
@property (nonatomic,copy) NSString *goodTitle;
@property (nonatomic,copy) NSString *goodPrice;

@property (nonatomic,assign) NSArray *goodModelArr;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

-(void)goodsDataForDictonary:(NSDictionary *)dic;
@end
