//
//  HadSellerGoodsModel.h
//  MMG
//
//  Created by mac on 16/8/24.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HadSellerGoodsModel : NSObject

@property (nonatomic, copy) NSString *goodsID;
@property (nonatomic, copy) NSString *goodsImg;
@property (nonatomic, copy) NSString *goodsTitle;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *jicun_uid;
@property (nonatomic, copy) NSString *goodsPrice;
@property (nonatomic, copy) NSString *js_status;
@property (nonatomic, copy) NSString *order_id;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
