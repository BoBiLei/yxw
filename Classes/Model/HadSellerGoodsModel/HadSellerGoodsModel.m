//
//  HadSellerGoodsModel.m
//  MMG
//
//  Created by mac on 16/8/24.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "HadSellerGoodsModel.h"

@implementation HadSellerGoodsModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.goodsID=dic[@"goods_id"];
    self.goodsImg=dic[@"goods_img"];
    self.goodsTitle=dic[@"goods_name"];
    self.status=dic[@"goods_status"];
    self.jicun_uid=dic[@"jicun_uid"];
    self.goodsPrice=dic[@"jiesuan_price"];
    self.js_status=dic[@"js_status"];
    self.order_id=dic[@"order_id"];
}

@end
