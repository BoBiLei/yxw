//
//  ProductListModel.h
//  CatShopping
//
//  Created by mac on 15/9/24.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductListModel : NSObject

@property (nonatomic,copy) NSString *idStr;
@property (nonatomic,copy) NSString *good_num;
@property (nonatomic,copy) NSString *imgStr;
@property (nonatomic,copy) NSString *isNewStr;
@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,copy) NSString *mmPriceStr;
@property (nonatomic,copy) NSString *stagePriceStr;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

-(void)jsonDataForTopicDictionary:(NSDictionary *)dic;

@end
