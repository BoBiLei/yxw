//
//  ProductListModel.m
//  CatShopping
//
//  Created by mac on 15/9/24.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProductListModel.h"

@implementation ProductListModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.idStr=dic[@"goods_id"];
    self.good_num=dic[@"goods_number"];
    self.imgStr=dic[@"goods_thumb"];
    self.isNewStr=dic[@"level"];
    self.titleStr=dic[@"goods_name"];
    self.mmPriceStr=dic[@"shop_price"];
    self.stagePriceStr=dic[@"period"];
}

-(void)jsonDataForTopicDictionary:(NSDictionary *)dic{
    self.idStr=dic[@"goods_id"];
    self.good_num=dic[@"goods_number"];
    self.imgStr=dic[@"goods_img"];
    self.titleStr=dic[@"goods_name"];
    self.mmPriceStr=dic[@"shop_price"];
    self.stagePriceStr=dic[@"fenqi_shop_price"];
}

@end
