//
//  OrderDetailAddressModel.m
//  MMG
//
//  Created by mac on 15/10/27.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderDetailAddressModel.h"

@implementation OrderDetailAddressModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.receiverName=dic[@"consignee"];
    self.receiverAddress=dic[@"address"];
    self.receiverPhone=dic[@"mobile"];
    self.orderTime=dic[@"add_time"];
}

@end
