//
//  OrderDetailAddressModel.h
//  MMG
//
//  Created by mac on 15/10/27.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailAddressModel : NSObject

@property (nonatomic,copy) NSString *receiverName;
@property (nonatomic,copy) NSString *receiverAddress;
@property (nonatomic,copy) NSString *receiverPhone;
@property (nonatomic,copy) NSString *orderTime;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
