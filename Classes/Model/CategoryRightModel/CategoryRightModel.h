//
//  CategoryRightModel.h
//  MMG
//
//  Created by mac on 15/10/29.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryRightModel : NSObject

@property (nonatomic,copy) NSString *cateId;
@property (nonatomic,copy) NSString *cateName;
@property (nonatomic,copy) NSString *catePic;
@property (nonatomic,copy) NSString *cateUrl;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
