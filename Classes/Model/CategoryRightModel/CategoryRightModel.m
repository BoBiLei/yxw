//
//  CategoryRightModel.m
//  MMG
//
//  Created by mac on 15/10/29.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CategoryRightModel.h"

@implementation CategoryRightModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.cateId=dic[@"id"];
    self.cateName=dic[@"name"];
    self.catePic=dic[@"pic4"];
    self.cateUrl=dic[@"url"];
}

@end
