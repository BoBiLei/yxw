//
//  MaijiaGoodsManagerModel.m
//  MMG
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaGoodsManagerModel.h"

@implementation MaijiaGoodsManagerModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.goodsID=dic[@"goods_id"];
    self.goodsImg=dic[@"goods_thumb"];
    self.goodsTitle=dic[@"goods_name"];
    self.goodsPrice=dic[@"shop_price"];
    self.stagePrice=dic[@"fenqi"];
    self.fenqi_fee=dic[@"fenqi_fee"];
}

-(void)jsonManagerDataForDictionary:(NSDictionary *)dic{
    self.goodsID=dic[@"goods_id"];
    self.goodsImg=dic[@"goods_thumb"];
    self.goodsTitle=dic[@"goods_name"];
    self.goodsPrice=dic[@"jiesuan_price"];
    self.stagePrice=dic[@"fenqi"];
    self.jishou_status=dic[@"jishou_status"];
}

@end
