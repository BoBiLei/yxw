//
//  MaijiaGoodsManagerModel.h
//  MMG
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MaijiaGoodsManagerModel : NSObject

@property(assign,nonatomic) BOOL selectState;//是否选中状态

@property (nonatomic, copy) NSString *goodsID;
@property (nonatomic, copy) NSString *goodsImg;
@property (nonatomic, copy) NSString *goodsTitle;
@property (nonatomic, copy) NSString *goodsPrice;
@property (nonatomic, copy) NSString *stagePrice;   //fenqi
@property (nonatomic, copy) NSString *goods_number;  //=1未售 0 已售
@property (nonatomic, copy) NSArray *fenqi_fee;  //
@property (nonatomic, copy) NSString *jishou_status;  //

-(void)jsonDataForDictionary:(NSDictionary *)dic;

-(void)jsonManagerDataForDictionary:(NSDictionary *)dic;

@end
