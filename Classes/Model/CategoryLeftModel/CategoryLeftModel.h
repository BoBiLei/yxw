//
//  CategoryLeftModel.h
//  MMG
//
//  Created by mac on 15/10/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryLeftModel : NSObject

@property (nonatomic,copy) NSString *cid;
@property (nonatomic,copy) NSString *name;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
