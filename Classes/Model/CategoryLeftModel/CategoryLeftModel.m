//
//  CategoryLeftModel.m
//  MMG
//
//  Created by mac on 15/10/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CategoryLeftModel.h"

@implementation CategoryLeftModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.cid=dic[@"cid"];
    self.name=dic[@"name"];
}

@end
