//
//  UseCouponModel.m
//  MMG
//
//  Created by mac on 15/10/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "UseCouponModel.h"

@implementation UseCouponModel

-(void)jsonDataForDictionay:(NSDictionary *)dic{
    self.couponID=dic[@"bonus_id"];
    self.couponMoney=dic[@"type_money"];
    self.couponEndTime=dic[@"end_time_text"];
}

@end
