//
//  UseCouponModel.h
//  MMG
//
//  Created by mac on 15/10/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UseCouponModel : NSObject

@property(nonatomic ,copy) NSString *couponID;
@property(nonatomic ,copy) NSString *couponMoney;
@property(nonatomic ,copy) NSString *couponEndTime;
@property (nonatomic,assign) BOOL isSelectedCoupon;

-(void)jsonDataForDictionay:(NSDictionary *)dic;

@end
