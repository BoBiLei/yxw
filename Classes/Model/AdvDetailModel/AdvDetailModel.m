//
//  AdvDetailModel.m
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "AdvDetailModel.h"

@implementation AdvDetailModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.imgStr=dic[@"imgStr"];
    self.imgW=dic[@"imgW"];
    self.imgH=dic[@"imgH"];
}

@end
