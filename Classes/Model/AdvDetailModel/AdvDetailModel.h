//
//  AdvDetailModel.h
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdvDetailModel : NSObject

@property(nonatomic,copy) NSString *imgStr;
@property(nonatomic,copy) NSString *imgW;
@property(nonatomic,copy) NSString *imgH;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
