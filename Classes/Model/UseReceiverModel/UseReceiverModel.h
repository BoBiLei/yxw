//
//  UseReceiverModel.h
//  MMG
//
//  Created by mac on 15/10/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UseReceiverModel : NSObject

@property (nonatomic,copy) NSString *useReceiverId;
@property (nonatomic,copy) NSString *useReceiverName;

@end
