//
//  HomePageGoodModel.m
//  CatShopping
//
//  Created by mac on 15/9/16.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "HomePageGoodModel.h"

@implementation HomePageGoodModel

-(void)getJsonDataFromDictionary:(NSDictionary *)dic{
    self.idStr_bigImg=dic[@"a"][@"href"];
    self.imgStr_bigImg=dic[@"a"][@"src"];
    self.idStr_smallImg01=dic[@"b"][@"href"];
    self.imgStr_smallImg01=dic[@"b"][@"src"];
    self.idStr_smallImg02=dic[@"c"][@"href"];
    self.imgStr_smallImg02=dic[@"c"][@"src"];
}

@end
