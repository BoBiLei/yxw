//
//  HomePageGoodModel.h
//  CatShopping
//
//  Created by mac on 15/9/16.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageGoodModel : NSObject

@property (nonatomic,copy) NSString *idStr_bigImg;
@property (nonatomic,copy) NSString *imgStr_bigImg;

@property (nonatomic,copy) NSString *idStr_smallImg01;
@property (nonatomic,copy) NSString *imgStr_smallImg01;

@property (nonatomic,copy) NSString *idStr_smallImg02;
@property (nonatomic,copy) NSString *imgStr_smallImg02;

-(void)getJsonDataFromDictionary:(NSDictionary *)dic;

@end
