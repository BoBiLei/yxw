//
//  SellerFashionModel.m
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import "SellerFashionModel.h"
#import "UIImage+LK.h"
@implementation SellerFashionModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.sellerId=dic[@"goods_id"];
    self.w=200;
    self.img=dic[@"thumb_url"];
    self.title=dic[@"img_desc"];
    
    //字符串处理
    NSRange rang=[self.img rangeOfString:@"_P_"];
    if(rang.location!=NSNotFound){
        NSString *str01=[self.img substringFromIndex:rang.location+rang.length];
        NSRange _rang=[str01 rangeOfString:@"_"];
        if(_rang.location!=NSNotFound){
            NSString *str02=[str01 substringFromIndex:_rang.location+_rang.length];
            NSRange dRang=[str02 rangeOfString:@"."];
            if (dRang.location!=NSNotFound) {
                NSString *str03=[str02 substringToIndex:dRang.location];
                NSRange whRange=[str03 rangeOfString:@"_"];
                if (whRange.location!=NSNotFound) {
                    NSString *wStr=[str03 substringToIndex:whRange.location];
                    NSString *hStr=[str03 substringFromIndex:whRange.location+whRange.length];
                    
                    CGFloat wStrFloat=wStr.floatValue;
                    CGFloat hStrFloat=hStr.floatValue;
                    CGFloat reHegiht=(hStrFloat*200)/wStrFloat;
                    self.h=reHegiht;
                }
               
            }
        }
    }
}

@end
