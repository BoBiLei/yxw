//
//  SellerFashionModel.h
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerFashionModel : UIView

@property (nonatomic ,copy) NSString *sellerId;
@property (nonatomic, assign) CGFloat w;
@property (nonatomic, assign) CGFloat h;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *title;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
