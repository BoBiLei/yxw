//
//  GoodParamModel.h
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodParamModel : NSObject

@property(nonatomic,copy) NSString *paramName;
@property(nonatomic,copy) NSString *paramValue;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
