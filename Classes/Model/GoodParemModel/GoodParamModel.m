//
//  GoodParamModel.m
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "GoodParamModel.h"

@implementation GoodParamModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.paramName=dic[@"name"];
    self.paramValue=dic[@"value"];
}

@end
