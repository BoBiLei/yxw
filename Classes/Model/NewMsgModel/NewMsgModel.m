//
//  NewMsgModel.m
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "NewMsgModel.h"

@implementation NewMsgModel

-(void)jsonDataForDictionay:(NSDictionary *)dic{
    self.msgId=dic[@"id"];
    self.msgTime=dic[@"add_time"];
    self.msgImgStr=dic[@"file_url"];
    self.msgTitle=dic[@"title"];
}

@end
