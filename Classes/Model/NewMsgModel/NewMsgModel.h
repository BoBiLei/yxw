//
//  NewMsgModel.h
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewMsgModel : NSObject

@property(nonatomic,copy) NSString *msgId;
@property(nonatomic,copy) NSString *msgTime;
@property(nonatomic,copy) NSString *msgImgStr;
@property(nonatomic,copy) NSString *msgTitle;

-(void)jsonDataForDictionay:(NSDictionary *)dic;

@end
