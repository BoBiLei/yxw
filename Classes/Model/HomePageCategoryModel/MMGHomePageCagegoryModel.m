//
//  MMGHomePageCagegoryModel.m
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGHomePageCagegoryModel.h"

@implementation MMGHomePageCagegoryModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.href=dic[@"href"];
    self.name=dic[@"name"];
    self.img=dic[@"src"];
    self.busId=dic[@"id"];
}

@end
