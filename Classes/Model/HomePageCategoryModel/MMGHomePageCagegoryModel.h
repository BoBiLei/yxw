//
//  MMGHomePageCagegoryModel.h
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMGHomePageCagegoryModel : NSObject

@property (nonatomic,copy) NSString *href;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *busId;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
