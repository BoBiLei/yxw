//
//  HomePageBottomModel.h
//  CatShopping
//
//  Created by mac on 15/9/18.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageBottomModel : NSObject

@property (nonatomic,copy) NSString *idStr;
@property (nonatomic,copy) NSString *nameStr;
@property (nonatomic,copy) NSString *iconStr;

-(void)getJsonDataFromDictionary:(NSDictionary *)dic;

@end
