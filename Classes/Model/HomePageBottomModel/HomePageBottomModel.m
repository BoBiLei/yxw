//
//  HomePageBottomModel.m
//  CatShopping
//
//  Created by mac on 15/9/18.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "HomePageBottomModel.h"

@implementation HomePageBottomModel

-(void)getJsonDataFromDictionary:(NSDictionary *)dic{
    self.idStr=dic[@"id"];
    self.nameStr=dic[@"imgTitle"];
    self.iconStr=dic[@"imgStr"];
}

@end
