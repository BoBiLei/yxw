//
//  OrderDetailStageModel.h
//  MMG
//
//  Created by mac on 15/10/27.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailStageModel : NSObject

@property (nonatomic,copy) NSString *stageNum;
@property (nonatomic,copy) NSString *cutMoneyTime;
@property (nonatomic,copy) NSString *stageMoney;
@property (nonatomic,copy) NSString *status;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
