//
//  OrderDetailStageModel.m
//  MMG
//
//  Created by mac on 15/10/27.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderDetailStageModel.h"

@implementation OrderDetailStageModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.stageNum=dic[@"fenqi_this"];
    self.cutMoneyTime=dic[@"confirm_time"];
    self.stageMoney=dic[@"order_amount"];
    
    if([dic objectForKey:@"pay_status"]) {
        self.status=dic[@"pay_status"];
    } else {
        self.status=@"1001";
    }
    
}

@end
