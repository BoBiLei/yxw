//
//  MMGCouponModel.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGCouponModel.h"

@implementation MMGCouponModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.couponId=dic[@"bonus_id"];
    self.couponNumber=dic[@"bonus_number"];
    self.priceSum=dic[@"type_money"];
    self.validity=dic[@"use_enddate"];
    
    NSMutableString *statusStr=[NSMutableString stringWithString:dic[@"handler"]];
    NSRange range=[statusStr rangeOfString:@"<em>"];
    if (range.location!=NSNotFound) {
        [statusStr deleteCharactersInRange:range];
        NSRange range1=[statusStr rangeOfString:@"</em>"];
        if (range1.location!=NSNotFound) {
            [statusStr deleteCharactersInRange:range1];
        }
    }
    
    NSLog(@"%@",statusStr);
    self.status=statusStr;
}

@end
