//
//  FillterViewModel.h
//  MMG
//
//  Created by mac on 15/11/17.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FillterViewModel : NSObject

@property (nonatomic,copy) NSString *categoryId;
@property (nonatomic,copy) NSString *categoryName;
@property (nonatomic,assign) NSArray *categoryArray;

/**
 分类的
 */
-(void)jsonDataForDictionary:(NSDictionary *)dic;

/**
 品牌的
 */
-(void)brandJsonDataForDictionary:(NSDictionary *)dic;

@end
