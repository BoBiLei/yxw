//
//  SellerDetailUserImgModel.h
//  MMG
//
//  Created by mac on 15/11/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellerDetailUserImgModel : NSObject

@property(nonatomic,copy) NSString *imgStr;
@property (nonatomic, assign) CGFloat w;
@property (nonatomic, assign) CGFloat h;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
