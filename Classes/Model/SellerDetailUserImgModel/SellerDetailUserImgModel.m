//
//  SellerDetailUserImgModel.m
//  MMG
//
//  Created by mac on 15/11/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SellerDetailUserImgModel.h"

@implementation SellerDetailUserImgModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.w=200;
    self.imgStr=dic[@"thumb_url"];
    
    //字符串处理
    NSRange rang=[self.imgStr rangeOfString:@"_P_"];
    if(rang.location!=NSNotFound){
        NSString *str01=[self.imgStr substringFromIndex:rang.location+rang.length];
        NSRange _rang=[str01 rangeOfString:@"_"];
        if(_rang.location!=NSNotFound){
            NSString *str02=[str01 substringFromIndex:_rang.location+_rang.length];
            NSRange dRang=[str02 rangeOfString:@"."];
            if (dRang.location!=NSNotFound) {
                NSString *str03=[str02 substringToIndex:dRang.location];
                NSRange whRange=[str03 rangeOfString:@"_"];
                if (whRange.location!=NSNotFound) {
                    NSString *wStr=[str03 substringToIndex:whRange.location];
                    NSString *hStr=[str03 substringFromIndex:whRange.location+whRange.length];
                    
                    CGFloat wStrFloat=wStr.floatValue;
                    CGFloat hStrFloat=hStr.floatValue;
                    CGFloat reHegiht=(hStrFloat*200)/wStrFloat;
                    self.h=reHegiht;
                }
                
            }
        }
    }
}

@end
