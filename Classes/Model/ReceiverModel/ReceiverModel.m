//
//  ReceiverModel.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ReceiverModel.h"

@implementation ReceiverModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.receiverId=dic[@"address_id"];
    self.phoneNum=dic[@"mobile"];
    self.receiverName=dic[@"consignee"];
    self.address=dic[@"address"];
}

@end
