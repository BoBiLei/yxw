//
//  ReceiverModel.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiverModel : NSObject


@property (nonatomic,copy) NSString *receiverId;
@property (nonatomic,copy) NSString *receiverName;
@property (nonatomic,copy) NSString *phoneNum;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,assign) BOOL isDefaultAdr;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
