//
//  CollectionCell.h
//  MMG
//
//  Created by mac on 15/10/14.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionModel.h"

@protocol CollectionDelegate <NSObject>

@optional
/**
 立即购买
 */
-(void)buyForModel:(CollectionModel *)model;
/**
 取消收藏
 */
-(void)deleteForModel:(CollectionModel *)model;

@end

@interface CollectionCell : UITableViewCell

@property (copy, nonatomic) NSString *goodId;
@property (copy, nonatomic) NSString *colleId;
@property (copy, nonatomic) NSString *imgStr;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) id<CollectionDelegate> delegate;

-(void)reflushDataForModel:(CollectionModel *)model;

@end
