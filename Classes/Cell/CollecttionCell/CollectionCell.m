//
//  CollectionCell.m
//  MMG
//
//  Created by mac on 15/10/14.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CollectionCell.h"
#import "Macro2.h"
@implementation CollectionCell

- (void)awakeFromNib {
    self.buyBtn.layer.cornerRadius=2;
    self.cancelBtn.layer.cornerRadius=2;
    self.buyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(CollectionModel *)model{
    self.goodId=model.goodsId;
    self.imgStr=model.goodsImgStr;
    self.colleId=model.collectionId;
    NSString *imgUrlStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImgStr];
    [self.imgv setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:[UIImage imageNamed:DEFAULTImage]];
    self.titleLabel.text=model.goodsTitle;
    self.price.text=[NSString stringWithFormat:@"价格：%@",model.goodsPrice];
    if ([model.goods_num isEqualToString:@"0"]) {
        self.buyBtn.backgroundColor=[UIColor lightGrayColor];
        self.buyBtn.enabled=NO;
    }
}

- (IBAction)clickBuyBtn:(id)sender {
    CollectionModel *model=[[CollectionModel alloc]init];
    model.goodsId=self.goodId;
    model.goodsImgStr=self.imgStr;
    model.goodsTitle=self.titleLabel.text;
    model.goodsPrice=self.price.text;
    [self.delegate buyForModel:model];
}

- (IBAction)clickCancelBtn:(id)sender {
    CollectionModel *scModel=[[CollectionModel alloc]init];
    scModel.goodsId=self.goodId;
    scModel.collectionId=self.colleId;
    scModel.goodsImgStr=self.imgStr;
    scModel.goodsTitle=self.titleLabel.text;
    scModel.goodsPrice=self.price.text;
    [self.delegate deleteForModel:scModel];
}


@end
