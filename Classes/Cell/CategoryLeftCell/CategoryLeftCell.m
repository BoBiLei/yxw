//
//  CategoryLeftCell.m
//  MMG
//
//  Created by mac on 16/6/13.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "CategoryLeftCell.h"
#import "Macro2.h"
@implementation CategoryLeftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor colorWithHexString:@"#f8f8f8"];
    self.myTitle.textColor=[UIColor colorWithHexString:@"#929292"];
    self.myTitle.font=[UIFont systemFontOfSize:IPHONE5?14:15];
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 48, self.width, 1)];
    line.backgroundColor=[UIColor whiteColor];
    [self addSubview:line];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
