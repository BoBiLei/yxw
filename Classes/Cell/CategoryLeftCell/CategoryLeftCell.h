//
//  CategoryLeftCell.h
//  MMG
//
//  Created by mac on 16/6/13.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryLeftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myTitle;

@end
