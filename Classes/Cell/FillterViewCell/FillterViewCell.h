//
//  FillterViewCell.h
//  MMG
//
//  Created by mac on 15/11/19.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FillterViewModel.h"

@protocol SubCategoryDelegate <NSObject>

@optional

-(void)didSelectedSubButtonForModel:(FillterViewModel *)model;

@end

@interface FillterViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *categoryLabel;
@property (nonatomic,assign) NSMutableArray *dataArr;
@property (weak, nonatomic) id<SubCategoryDelegate> delegate;
-(void)relushDataForArray;

@end
