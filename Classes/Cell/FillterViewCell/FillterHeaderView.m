//
//  FillterViewCell.m
//  MMG
//
//  Created by mac on 15/11/17.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "FillterHeaderView.h"

#define FILLTER_BTN_W (SCREENSIZE.width-102-30)/2
#define BUTTON_Y 3
#define BUTTON_HEIGHT 30
@implementation FillterHeaderView{
    UIButton *btn;
    
    UIButton *seleBtn;
}

-(void)relushCategoryDataForArray{
    CGRect frame=self.frame;
    CGFloat cellHeight = 0.0;
    
    //label
    _leftLabel=[[UILabel alloc]init];
    _leftLabel.text=@"分类";
    _leftLabel.font=[UIFont systemFontOfSize:16];
    _leftLabel.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_leftLabel];
    
    //line
    UIView *line=[[UILabel alloc]init];
    CGRect lineFrame=CGRectMake(101, 0, 1.0f, 21);
    line.frame=lineFrame;
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line];
    
    //btnCorner
    UIView *btnCorner=[[UILabel alloc]init];
    CGRect btnCornerFrame=CGRectMake(102, 0, SCREENSIZE.width-102, 21);
    btnCorner.frame=btnCornerFrame;
    btnCorner.userInteractionEnabled=YES;
    [self addSubview:btnCorner];
    
    //button
    for (int i=0; i<_dataArr.count; i++) {
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        int j=i;
        if (j==0) {
            j=0;
        }else if (j%2==0) {
            j=0;
        }else{
            j=1;
        }
        NSInteger btnX=(j+1)*10+j*FILLTER_BTN_W;
        
        int btnY=0;
        NSInteger testY=(i+1)*10+i*FILLTER_BTN_W;
        if (testY<btnCorner.frame.size.width) {
            btnY=BUTTON_Y;
        }else{
            btnY=(BUTTON_HEIGHT+BUTTON_Y)*(i/2)+BUTTON_Y;
        }
        btn.frame=CGRectMake(btnX, btnY, FILLTER_BTN_W, BUTTON_HEIGHT);
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ababab"].CGColor;
        btn.layer.cornerRadius=2;
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        btn.tag=i;
        [btnCorner addSubview:btn];
        
        FillterViewModel *model=_dataArr[i];
        [btn setTitle:model.categoryName forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        objc_setAssociatedObject(btn, "fillterObject", model.categoryArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(btn, "selectedFillterId", model.categoryId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        //得到cell的高度
        if (i==_dataArr.count-1) {
            cellHeight=btnY+BUTTON_HEIGHT+BUTTON_Y;
        }
        if (btn.tag==_selectBtnTag) {
            btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.enabled=NO;
        }
    }
    
    frame.size.height=cellHeight;
    self.frame=frame;
    lineFrame.size.height=cellHeight;
    line.frame=lineFrame;
    btnCornerFrame.size.height=cellHeight;
    btnCorner.frame=btnCornerFrame;
    _leftLabel.frame=CGRectMake(0, 0, 100, 21);
    _leftLabel.center=CGPointMake(50, cellHeight/2);
    
    
    //line
    UIView *btmLine=[[UILabel alloc]initWithFrame:CGRectMake(0, self.frame.size.height-1, SCREENSIZE.width, 1.0f)];
    btmLine.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:btmLine];
}

#pragma mark - button event
-(void)clickBtn:(id)sender{
    UIButton *button=(UIButton *)sender;
    button.enabled=NO;
    NSArray *fillterArr = objc_getAssociatedObject(button, "fillterObject");
    NSString *selectedFillterId = objc_getAssociatedObject(button, "selectedFillterId");
    NSMutableArray *handelArr=[NSMutableArray array];
    //添加默认的全部
    FillterViewModel *model=[[FillterViewModel alloc]init];
    model.categoryId=@"0";
    model.categoryName=@"全部";
    [handelArr addObject:model];
    for (NSDictionary *modelDic in fillterArr) {
        FillterViewModel *model=[[FillterViewModel alloc]init];
        [model brandJsonDataForDictionary:modelDic];
        [handelArr addObject:model];
    }
    switch (self.selectedBtnType) {
        case tyFillterCategory:
            [self.delegate didSelectedButtonForModel:handelArr andSelectButton:button andSelectId:selectedFillterId];
            break;
        case tyFillterBrand:
            [self.delegate didSelectedBrandButtonForSelectId:selectedFillterId];
            break;
        case tyFillterPrice:
            [self.delegate didSelectedPriceButtonForSelectId:selectedFillterId];
            break;
            
        default:
            break;
    }
}

-(instancetype)init{
    self=[super init];
    if (self) {
        //label
        _leftLabel=[[UILabel alloc]init];
        _leftLabel.font=[UIFont systemFontOfSize:16];
        _leftLabel.textAlignment=NSTextAlignmentCenter;
    }
    return self;
}

-(void)relushOtherDataForArray{
    CGRect frame=self.frame;
    CGFloat cellHeight = 0.0;
    
    //line
    UIView *line=[[UILabel alloc]init];
    CGRect lineFrame=CGRectMake(101, 0, 1.0f, 21);
    line.frame=lineFrame;
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line];
    
    //btnCorner
    UIView *btnCorner=[[UILabel alloc]init];
    CGRect btnCornerFrame=CGRectMake(102, 0, SCREENSIZE.width-102, 21);
    btnCorner.frame=btnCornerFrame;
    btnCorner.userInteractionEnabled=YES;
    [self addSubview:btnCorner];
    
    //button
    for (int i=0; i<_dataArr.count; i++) {
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        int j=i;
        if (j==0) {
            j=0;
        }else if (j%2==0) {
            j=0;
        }else{
            j=1;
        }
        NSInteger btnX=(j+1)*10+j*FILLTER_BTN_W;
        
        int btnY=i;
        NSInteger testY=(i+1)*10+i*FILLTER_BTN_W;
        if (testY<btnCorner.frame.size.width) {
            btnY=BUTTON_Y;
        }else{
            btnY=(BUTTON_HEIGHT+BUTTON_Y)*(i/2)+BUTTON_Y;
        }
        btn.frame=CGRectMake(btnX, btnY, FILLTER_BTN_W, BUTTON_HEIGHT);
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ababab"].CGColor;
        btn.layer.cornerRadius=2;
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        btn.tag=i;
        [btnCorner addSubview:btn];
        
        FillterViewModel *model=_dataArr[i];
        objc_setAssociatedObject(btn, "selectedOtherFillterId", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [btn setTitle:model.categoryName forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickOtherBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        //得到cell的高度
        if (i==_dataArr.count-1) {
            cellHeight=btnY+BUTTON_HEIGHT+BUTTON_Y;
        }
        
        //默认选中第一个
        if (btn.tag==_selectBtnTag) {
            btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            seleBtn=btn;
        }
    }
    frame.size.height=cellHeight;
    self.frame=frame;
    lineFrame.size.height=cellHeight;
    line.frame=lineFrame;
    btnCornerFrame.size.height=cellHeight;
    btnCorner.frame=btnCornerFrame;
    _leftLabel.frame=CGRectMake(0, 0, 100, 21);
    _leftLabel.center=CGPointMake(50, cellHeight/2);
    [self addSubview:_leftLabel];
    
    //line
    UIView *btmLine=[[UILabel alloc]initWithFrame:CGRectMake(0, self.frame.size.height-1, SCREENSIZE.width, 1.0f)];
    btmLine.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:btmLine];
}

-(void)clickOtherBtn:(id)sender{
    seleBtn.backgroundColor=[UIColor clearColor];
    seleBtn.enabled=YES;
    [seleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    UIButton *button=(UIButton *)sender;
    button.enabled=NO;
    button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     FillterViewModel *selectedFillterModel = objc_getAssociatedObject(button, "selectedOtherFillterId");

    seleBtn=button;
    
    switch (self.selectedBtnType) {
        case tyFillterBrand:
            [self.delegate didSelectedBrandButtonForSelectId:selectedFillterModel.categoryId];
            break;
        case tyFillterPrice:
            [self.delegate didSelectedPriceButtonForSelectId:selectedFillterModel.categoryId];
            break;
            
        default:
            break;
    }
}

@end
