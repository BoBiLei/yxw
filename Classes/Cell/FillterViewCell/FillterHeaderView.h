//
//  FillterViewCell.h
//  MMG
//
//  Created by mac on 15/11/17.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FillterViewModel.h"

@protocol FillterViewDelegate <NSObject>

@optional

typedef NS_ENUM(NSInteger, FillterViewSelectedBtnType) {
    tyFillterCategory,
    tyFillterBrand,
    tyFillterPrice
};

-(void)didSelectedButtonForModel:(NSArray *)array andSelectButton:(UIButton *)button andSelectId:(NSString *)selectedId;

-(void)didSelectedBrandButtonForSelectId:(NSString *)selectedId;

-(void)didSelectedPriceButtonForSelectId:(NSString *)selectedId;

@end

@interface FillterHeaderView : UIView

@property (nonatomic,strong) UILabel *categoryLabel;
@property (nonatomic,assign) NSMutableArray *dataArr;
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,assign) NSInteger selectBtnTag;
@property (nonatomic) FillterViewSelectedBtnType selectedBtnType;
@property (weak, nonatomic) id<FillterViewDelegate> delegate;

-(void)relushCategoryDataForArray;

-(void)relushOtherDataForArray;
@end
