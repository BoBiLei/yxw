//
//  FillterViewCell.m
//  MMG
//
//  Created by mac on 15/11/19.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "FillterViewCell.h"
#define FILLTER_BTN_W (SCREENSIZE.width-102-30)/2
@implementation FillterViewCell{
    UIButton *btn;
    
    UIButton *seleBtn;
}

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)relushDataForArray{
    
    CGRect frame=self.frame;
    CGFloat cellHeight = 0.0;
    
    //label
    UILabel *leftLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 21)];
    leftLabel.text=@"子分类";
    leftLabel.font=[UIFont systemFontOfSize:16];
    leftLabel.textAlignment=NSTextAlignmentCenter;
    [self addSubview:leftLabel];
    
    //line
    UIView *line=[[UILabel alloc]init];
    CGRect lineFrame=CGRectMake(101, 0, 1.0f, 21);
    line.frame=lineFrame;
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line];
    
    //btnCorner
    UIView *btnCorner=[[UILabel alloc]init];
    CGRect btnCornerFrame=CGRectMake(102, 0, SCREENSIZE.width-102, 21);
    btnCorner.frame=btnCornerFrame;
    btnCorner.userInteractionEnabled=YES;
    [self addSubview:btnCorner];
    
    //button
    for (int i=0; i<_dataArr.count; i++) {
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        int j=i;
        if (j==0) {
            j=0;
        }else if (j%2==0) {
            j=0;
        }else{
            j=1;
        }
        NSInteger btnX=(j+1)*10+j*FILLTER_BTN_W;
        
        NSInteger testY=(i+1)*10+i*FILLTER_BTN_W;
        int btnY;
        if (testY<btnCorner.frame.size.width) {
            btnY=3;
        }else{
            btnY=31*(i/2)+3;
        }
        btn.frame=CGRectMake(btnX, btnY, FILLTER_BTN_W, 29);
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ababab"].CGColor;
        btn.layer.cornerRadius=2;
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        btn.tag=i;
        [btnCorner addSubview:btn];
        
        FillterViewModel *model=_dataArr[i];
        [btn setTitle:model.categoryName forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        objc_setAssociatedObject(btn, "fillterObject", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        //得到cell的高度
        if (i==_dataArr.count-1) {
            cellHeight=btnY+32;
        }
        
        //默认选中第一个
        if (btn.tag==0) {
            btn.backgroundColor=[UIColor colorWithHexString:@"#ebb900"];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            seleBtn=btn;
        }
    }
    frame.size.height=cellHeight;
    self.frame=frame;
    lineFrame.size.height=cellHeight;
    line.frame=lineFrame;
    btnCornerFrame.size.height=cellHeight;
    btnCorner.frame=btnCornerFrame;
    leftLabel.center=CGPointMake(50, cellHeight/2);
    
    
    //line
    UIView *btmLine=[[UILabel alloc]initWithFrame:CGRectMake(0, self.frame.size.height-1, SCREENSIZE.width, 1)];
    btmLine.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:btmLine];
}

#pragma mark - button event
-(void)clickBtn:(id)sender{
    seleBtn.backgroundColor=[UIColor clearColor];
    [seleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    seleBtn.enabled=YES;
    
    UIButton *button=(UIButton *)sender;
    button.backgroundColor=[UIColor colorWithHexString:@"#ebb900"];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    FillterViewModel *model = objc_getAssociatedObject(button, "fillterObject");
    [self.delegate didSelectedSubButtonForModel:model];
    button.enabled=NO;
    
    seleBtn=button;
}

@end
