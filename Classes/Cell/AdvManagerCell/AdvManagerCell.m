//
//  AdvManagerCell.m
//  MMG
//
//  Created by mac on 16/9/9.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "AdvManagerCell.h"

@implementation AdvManagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.imgv=[[UIImageView alloc] initWithFrame:CGRectMake(10, 8, SCREENSIZE.width-20, SCREENSIZE.height/3-28)];
        [self addSubview:self.imgv];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
