//
//  ShopCell.h
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListModel.h"

@interface ShopCell : UICollectionViewCell

-(void)reflushDataForModel:(ProductListModel *)model;

@end
