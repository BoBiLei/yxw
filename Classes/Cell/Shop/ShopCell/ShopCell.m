//
//  ShopCell.m
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "ShopCell.h"
#import "Macro2.h"

#define IMGHEIGHT (SCREENSIZE.width)/2-14
@implementation ShopCell{
    UIButton *_isNewGood;
    UIImageView *imgv;
    UILabel *titleLabel;  //
    UILabel *stagePrice;  //分期价
    UILabel *shopName;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.height=IMGHEIGHT+88;
    
    
    //图片
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGHEIGHT, IMGHEIGHT)];
    imgv.contentMode=UIViewContentModeScaleAspectFit;
    [self addSubview:imgv];
    
    //isNew
    _isNewGood=[UIButton buttonWithType:UIButtonTypeCustom];
    _isNewGood.titleLabel.font=[UIFont systemFontOfSize:10];
    [self addSubview:_isNewGood];
    
    _isNewGood.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _isNewGoodH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_isNewGood(28)]-1-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_isNewGood)];
    [NSLayoutConstraint activateConstraints:_isNewGoodH];
    NSArray* _isNewGoodV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[_isNewGood(28)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_isNewGood)];
    [NSLayoutConstraint activateConstraints:_isNewGoodV];
    _isNewGood.layer.cornerRadius=14;
    
    //名称
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, imgv.height, IMGHEIGHT-8, 38)];
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    titleLabel.numberOfLines=0;
    [self addSubview:titleLabel];
    
    //分期价
    stagePrice=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height+4, titleLabel.width, 19)];
    stagePrice.font=[UIFont systemFontOfSize:IPHONE5?13:14];
    stagePrice.adjustsFontSizeToFitWidth=YES;
    stagePrice.textColor=[UIColor colorWithHexString:@"#ff0808"];
    stagePrice.textAlignment=NSTextAlignmentCenter;
    [self addSubview:stagePrice];
    
    //店铺名
    shopName=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, stagePrice.origin.y+stagePrice.height+2, titleLabel.width, 17)];
    shopName.font=[UIFont systemFontOfSize:13];
    shopName.textColor=[UIColor colorWithHexString:@"#999999"];
    shopName.textAlignment=NSTextAlignmentCenter;
    [self addSubview:shopName];
}

-(void)reflushDataForModel:(ProductListModel *)model{
    [imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.imgStr]] placeholderImage:[UIImage imageNamed:@"placeholder_160x180"]];
    if ([model.good_num isEqualToString:@"0"]) {
        _isNewGood.backgroundColor=[UIColor lightGrayColor];
        [_isNewGood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_isNewGood setTitle:@"已售" forState:UIControlStateNormal];
        _isNewGood.hidden=NO;
    }else{
        _isNewGood.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        [_isNewGood setTitle:@"全新" forState:UIControlStateNormal];
        [_isNewGood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        if ([model.isNewStr isEqualToString:@"1"]) {
            _isNewGood.hidden=NO;
        }else{
            _isNewGood.hidden=YES;
        }
    }
    NSString *labelTtt=model.titleStr;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;//对齐方式
    [paragraphStyle setLineSpacing:2];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    titleLabel.attributedText = attributedString;
    
    stagePrice.text=[NSString stringWithFormat:@"分期价：%@",model.stagePriceStr];
    shopName.text=[NSString stringWithFormat:@"游侠价：%@",model.mmPriceStr];
}

@end
