//
//  ShopReusableView.m
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "ShopReusableView.h"
#import "ShopReusableButton.h"
#import "ShopFillterButton.h"

@implementation ShopReusableView{
    UIView *line;
    ShopReusableButton *seleBtn;
    
    ShopFillterButton *fillterBtn01;
    ShopFillterButton *fillterBtn02;
    ShopFillterButton *fillterBtn03;
    ShopFillterButton *fillterBtn04;
    
    BOOL rqIsUp;        //人气是否向上
    BOOL xpIsUp;        //新品是否向上
    BOOL jgIsUp;        //价格是否向上
    
    NSString *orderByStr;   //升序或降序
    NSString *sortType;     //(综合、人气、新品、价格)
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self setUpUIWithArray:@[
                                 @{@"img":@"dprs_01",@"name":@"店铺首页"},
                                 @{@"img":@"dprs_02",@"name":@"全部商品"},
                                 @{@"img":@"dprs_03",@"name":@"新品上架"}]];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self setUpUIWithArray:@[
                                 @{@"img":@"dprs_01",@"name":@"店铺首页"},
                                 @{@"img":@"dprs_02",@"name":@"全部商品"},
                                 @{@"img":@"dprs_03",@"name":@"新品上架"}]];
    }
    return self;
}

-(void)setUpUIWithArray:(NSArray *)arr{
    CGFloat btnX=0;
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    for (int i=0; i<arr.count; i++) {
        btnX=i*btnWidth;
        ShopReusableButton *btn=[[ShopReusableButton alloc] initWithFrame:CGRectMake(btnX, 0, btnWidth, self.bounds.size.height-2)];
        btn.imgv.image=[UIImage imageNamed:arr[i][@"img"]];
        btn.titleLabel.text=arr[i][@"name"];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        btn.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        btn.tag=i;
        [self addSubview:btn];
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    line=[[UIView alloc] initWithFrame:CGRectMake(0, self.height-2, btnWidth, 2)];
    line.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [self addSubview:line];
    
    [self createFillterBtn:@[
                             @{@"name":@"综合",@"sort":@"g.add_time"},
                             @{@"name":@"人气",@"sort":@"g.click_count"},
                             @{@"name":@"新品",@"sort":@"goods_id"},
                             @{@"name":@"价格",@"sort":@"shop_price"}
                             ]
     ];
}

-(void)clickBtn:(id)sender{
    
    seleBtn.enabled=YES;
    
    ShopReusableButton *btn=sender;
    btn.enabled=NO;
    CGFloat btnX=0;
    NSString *tagStr=[NSString stringWithFormat:@"%ld",btn.tag];
    switch (btn.tag) {
        case 0:
            btnX=0;
            break;
        case 1:
            btnX=SCREENSIZE.width/3;
            break;
        default:
            btnX=SCREENSIZE.width/3*2;
            break;
    }
    [UIView animateWithDuration:0.25 animations:^{
        line.frame=CGRectMake(btnX, 62, SCREENSIZE.width/3, 2);
    } completion:^(BOOL finished) {
        [self.delegate clickBtnWithTag:tagStr];
    }];
    
    seleBtn=btn;
}

-(void)createFillterBtn:(NSArray *)arr{
    CGFloat magin=10;
    CGFloat btnX=0;
    CGFloat btnWidth=(SCREENSIZE.width-(arr.count+1)*magin)/arr.count;
    for (int i=0; i<arr.count; i++) {
        NSDictionary *dic=arr[i];
        btnX=btnWidth*i+magin*i+magin;
        ShopFillterButton *btn=[[ShopFillterButton alloc] initWithFrame:CGRectMake(btnX, 72, btnWidth, 38)];
        btn.imgv.image=[UIImage imageNamed:@"fillter_nor"];
        btn.titleLabel.text=dic[@"name"];
        btn.backgroundColor=[UIColor orangeColor];
        btn.tag=i;
        objc_setAssociatedObject(btn, "ShopFillterObject", dic, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        orderByStr=@"DESC";
        sortType=@"g.add_time";
        if (i==0) {
            fillterBtn01=btn;
            fillterBtn01.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            fillterBtn01.imgv.image=[UIImage imageNamed:@"fillter_all"];
            fillterBtn01.enabled=NO;
        }else if (i==1){
            fillterBtn02=btn;
        }else if (i==2){
            fillterBtn03=btn;
        }else if (i==3){
            fillterBtn04=btn;
        }
        [self addSubview:btn];
        [btn addTarget:self action:@selector(clickFillterBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)clickFillterBtn:(id)sender{
    ShopFillterButton *btn=sender;
    NSDictionary *dic = objc_getAssociatedObject(btn, "ShopFillterObject");
    switch (btn.tag) {
        case 0:
        {
            //
            fillterBtn01.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            fillterBtn01.imgv.image=[UIImage imageNamed:@"fillter_all"];
            fillterBtn01.enabled=NO;
            
            //
            fillterBtn02.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn02.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            rqIsUp=NO;
            
            //
            fillterBtn03.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn03.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            xpIsUp=NO;
            
            //
            fillterBtn04.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn04.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            jgIsUp=NO;
            
            orderByStr=@"DESC";
        }
            break;
        case 1:
        {
            //
            fillterBtn01.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn01.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            fillterBtn01.enabled=YES;
            
            //
            fillterBtn02.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            if (rqIsUp) {
                fillterBtn02.imgv.image=[UIImage imageNamed:@"fillter_up"];
            }else{
                fillterBtn02.imgv.image=[UIImage imageNamed:@"fillter_down"];
            }
            orderByStr=rqIsUp?@"ASC":@"DESC";
            rqIsUp=!rqIsUp;
            
            //
            fillterBtn03.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn03.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            xpIsUp=NO;
            
            //
            fillterBtn04.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn04.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            jgIsUp=NO;
        }
            break;
        case 2:
        {
            //
            fillterBtn01.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn01.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            fillterBtn01.enabled=YES;
            
            //
            fillterBtn02.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn02.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            rqIsUp=NO;
            
            //
            fillterBtn03.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            if (xpIsUp) {
                fillterBtn03.imgv.image=[UIImage imageNamed:@"fillter_up"];
            }else{
                fillterBtn03.imgv.image=[UIImage imageNamed:@"fillter_down"];
            }
            orderByStr=xpIsUp?@"ASC":@"DESC";
            xpIsUp=!xpIsUp;
            
            //
            fillterBtn04.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn04.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            jgIsUp=NO;
        }
            break;
        default:
        {
            //
            fillterBtn01.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn01.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            fillterBtn01.enabled=YES;
            
            //
            fillterBtn02.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn02.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            rqIsUp=NO;
            
            //
            fillterBtn03.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
            fillterBtn03.imgv.image=[UIImage imageNamed:@"fillter_nor"];
            xpIsUp=NO;
            
            //
            fillterBtn04.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            fillterBtn04.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
            if (jgIsUp) {
                fillterBtn04.imgv.image=[UIImage imageNamed:@"fillter_up"];
            }else{
                fillterBtn04.imgv.image=[UIImage imageNamed:@"fillter_down"];
            }
            orderByStr=jgIsUp?@"ASC":@"DESC";
            jgIsUp=!jgIsUp;
        }
            break;
    }
    [self.delegate clickFillterWithType:dic[@"sort"] sort:orderByStr];
}

@end
