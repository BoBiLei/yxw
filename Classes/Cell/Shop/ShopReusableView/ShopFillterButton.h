//
//  ShopFillterButton.h
//  MMG
//
//  Created by mac on 16/9/1.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopFillterButton : UIControl

@property (nonatomic, strong) UIImageView *imgv;
@property (nonatomic, strong) UILabel *titleLabel;

@end
