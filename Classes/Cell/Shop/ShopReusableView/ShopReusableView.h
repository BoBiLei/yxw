//
//  ShopReusableView.h
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ClickShopReusableDelegate <NSObject>

-(void)clickBtnWithTag:(NSString *)str;

-(void)clickFillterWithType:(NSString *)type sort:(NSString *)sort;

@end

@interface ShopReusableView : UICollectionReusableView

@property (nonatomic, weak) id<ClickShopReusableDelegate> delegate;

@end
