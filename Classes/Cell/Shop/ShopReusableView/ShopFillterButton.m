//
//  ShopFillterButton.m
//  MMG
//
//  Created by mac on 16/9/1.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "ShopFillterButton.h"

@implementation ShopFillterButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.imgv];
    [self addSubview:self.titleLabel];
}

-(UIImageView *)imgv{
    if (!_imgv) {
        _imgv=[[UIImageView alloc] init];
    }
    return _imgv;
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.textAlignment=NSTextAlignmentCenter;
        _titleLabel.font=[UIFont systemFontOfSize:16];
        _titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    }
    return _titleLabel;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.imgv.frame = CGRectMake(0, 0, width, height);
    self.titleLabel.frame = CGRectMake(0, 0, width-17,height);
}

@end
