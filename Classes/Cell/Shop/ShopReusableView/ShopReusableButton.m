//
//  ShopReusableButton.m
//  youxia
//
//  Created by mac on 16/8/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ShopReusableButton.h"

@implementation ShopReusableButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.imgv];
    [self addSubview:self.titleLabel];
}

-(UIImageView *)imgv{
    if (!_imgv) {
        _imgv=[[UIImageView alloc] init];
        _imgv.contentMode=UIViewContentModeScaleAspectFit;
    }
    return _imgv;
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.imgv.frame = CGRectMake(0, 8, width, height/2);
    self.titleLabel.frame = CGRectMake(0, 4+height/2, width,height/2);
}

@end
