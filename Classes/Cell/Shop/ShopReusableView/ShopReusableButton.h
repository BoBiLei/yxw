//
//  ShopReusableButton.h
//  youxia
//
//  Created by mac on 16/8/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopReusableButton : UIControl

@property (nonatomic, strong) UIImageView *imgv;       //日期
@property (nonatomic, strong) UILabel *titleLabel;     //价格

@end
