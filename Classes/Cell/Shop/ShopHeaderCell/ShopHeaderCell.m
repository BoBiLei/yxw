//
//  ShopHeaderCell.m
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "ShopHeaderCell.h"
#import "Macro2.h"
@implementation ShopHeaderCell{
    UIImageView *userPhone;
    UILabel *shopName;
    UILabel *fzrLabel;
    UILabel *phoneLabel;
    UIImageView *advImg;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUI];
}

-(void)setUpUI{
    
    UIView *userInfoView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 68)];
    userInfoView.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [self addSubview:userInfoView];
    
    userPhone=[[UIImageView alloc] initWithFrame:CGRectMake(18, 10, 48, 48)];
    userPhone.layer.masksToBounds=YES;
    userPhone.layer.cornerRadius=userPhone.height/2;
    [userInfoView addSubview:userPhone];
    
#pragma mark 店铺名
    shopName=[[UILabel alloc] initWithFrame:CGRectMake(userPhone.origin.x+userPhone.width+12, userPhone.origin.y, SCREENSIZE.width/2-(userPhone.origin.x+userPhone.width+12), userPhone.height)];
    shopName.font=[UIFont systemFontOfSize:17];
    shopName.textColor=[UIColor whiteColor];
    shopName.numberOfLines=0;
    [userInfoView addSubview:shopName];
    
#pragma mark 负责人
    fzrLabel=[[UILabel alloc] initWithFrame:CGRectMake(shopName.origin.x+shopName.width+6, 12, SCREENSIZE.width/2, (userInfoView.height-24)/2)];
    fzrLabel.font=[UIFont systemFontOfSize:15];
    fzrLabel.textColor=[UIColor whiteColor];
    [userInfoView addSubview:fzrLabel];
    
#pragma mark 电话
    phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(fzrLabel.origin.x, fzrLabel.origin.y+fzrLabel.height, fzrLabel.width, fzrLabel.height)];
    phoneLabel.font=[UIFont systemFontOfSize:15];
    phoneLabel.textColor=[UIColor whiteColor];
    [userInfoView addSubview:phoneLabel];
    
    //大广告图
    //550*260=width*x
    advImg=[[UIImageView alloc] initWithFrame:CGRectMake(0, userInfoView.origin.y+userInfoView.height, SCREENSIZE.width, SCREENSIZE.width/2)];
    advImg.image=[UIImage imageNamed:@"dp_adv"];
    advImg.contentMode=UIViewContentModeScaleAspectFill;
    [self addSubview:advImg];
}

-(void)setModel:(NSDictionary *)model{
    [userPhone setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost03,model[@"pic"]]] placeholderImage:[UIImage imageNamed:@"bottom_VIP"]];
    shopName.text=model[@"business_name"];
    fzrLabel.text=[NSString stringWithFormat:@"负责人：%@",model[@"person_name"]];
    phoneLabel.text=[NSString stringWithFormat:@"电话：%@",model[@"business_phone"]];
    [advImg setImageWithURL:[NSURL URLWithString:model[@"business_poster_h5"]] placeholderImage:[UIImage imageNamed:@"dp_adv"]];
}

@end
