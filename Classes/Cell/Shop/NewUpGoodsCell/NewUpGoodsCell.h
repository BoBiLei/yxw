//
//  NewUpGoodsCell.h
//  MMG
//
//  Created by mac on 16/8/31.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListModel.h"

@interface NewUpGoodsCell : UICollectionViewCell

-(void)reflushDataForModel:(ProductListModel *)model;

@end
