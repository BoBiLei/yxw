//
//  NewUpGoodsCell.m
//  MMG
//
//  Created by mac on 16/8/31.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewUpGoodsCell.h"
#import <TTTAttributedLabel.h>
#import "Macro2.h"
@implementation NewUpGoodsCell{
    UIImageView *imgv;
    UILabel *title;
    UILabel *fqPrice;
    UILabel *mmPrice;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUI];
}
//SCREENSIZE.width-58
-(void)setUpUI{
    imgv=[[UIImageView alloc] initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, SCREENSIZE.width-126)];
    imgv.contentMode=UIViewContentModeScaleAspectFit;
    [self addSubview:imgv];
    
    title=[[UILabel alloc] initWithFrame:CGRectMake(16, imgv.origin.y+imgv.height+8, imgv.width/2, 44)];
    title.numberOfLines=0;
    title.font=[UIFont systemFontOfSize:16];
    [self addSubview:title];
    
    fqPrice=[[UILabel alloc] initWithFrame:CGRectMake(title.origin.x+title.width+8, title.origin.y, imgv.width-(title.origin.x+title.width+16), title.height/2)];
    fqPrice.textColor=[UIColor colorWithHexString:@"#ff0808"];
    fqPrice.textAlignment=NSTextAlignmentRight;
    fqPrice.font=[UIFont systemFontOfSize:15];
    [self addSubview:fqPrice];
    
    mmPrice=[[UILabel alloc] initWithFrame:CGRectMake(fqPrice.origin.x, fqPrice.origin.y+fqPrice.height, fqPrice.width, fqPrice.height)];
    mmPrice.textColor=[UIColor colorWithHexString:@"#959595"];
    mmPrice.textAlignment=NSTextAlignmentRight;
    mmPrice.font=[UIFont systemFontOfSize:15];
    [self addSubview:mmPrice];
}

-(void)reflushDataForModel:(ProductListModel *)model{
    //
    [imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.imgStr]] placeholderImage:[UIImage imageNamed:@"img_default"]];
    title.text=model.titleStr;
    fqPrice.text=model.stagePriceStr;
    mmPrice.text=[NSString stringWithFormat:@"游侠价：%@",model.mmPriceStr];
}

@end
