//
//  NewGoodsAddedThreeTopCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedThreeTopCell.h"

@implementation NewGoodsAddedThreeTopCell

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
#pragma mark
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.cornerRadius=2;
        btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        btn.frame=CGRectMake(12, 20, 80, 32);
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:@"上传照片" forState:UIControlStateNormal];
        [self addSubview:btn];
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(btn.origin.x+btn.width+6, btn.origin.y, SCREENSIZE.width-(btn.origin.x+btn.width+18), btn.height)];
        [label setText:@"JPG、PNG格式图片，大小不超过2M"];
        label.textColor=[UIColor redColor];
        label.font=[UIFont systemFontOfSize:15];
        label.adjustsFontSizeToFitWidth=YES;
        [self addSubview:label];
        
    }
    return self;
}

@end
