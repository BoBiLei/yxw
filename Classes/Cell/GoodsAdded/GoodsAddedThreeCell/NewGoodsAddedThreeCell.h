//
//  NewGoodsAddedThreeCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewAddGoodsImagesDelegate <NSObject>

-(void)clickImgv;

@end

@interface NewGoodsAddedThreeCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imgv;

@property (nonatomic, weak) id<NewAddGoodsImagesDelegate> delegate;

-(void)reflushPictureForData:(NSData *)data;

@end
