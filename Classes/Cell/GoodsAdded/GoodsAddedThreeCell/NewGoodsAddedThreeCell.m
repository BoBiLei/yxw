//
//  NewGoodsAddedThreeCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedThreeCell.h"

@implementation NewGoodsAddedThreeCell

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
#pragma mark 图片
        self.imgv=[[UIImageView alloc] initWithFrame:CGRectMake(12, 0, self.width-24, self.height-20)];
        self.imgv.image = [UIImage imageNamed:@"shangjia_sc_imgv"];
        [self addSubview:self.imgv];
    }
    return self;
}

-(void)clickImgv{
    [self.delegate clickImgv];
}

-(void)reflushPictureForData:(NSData *)data{
    self.imgv.image=[UIImage imageWithData:data];
}

@end
