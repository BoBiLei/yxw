//
//  NewGoodsAddedSecondBtmCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewGoodsAddedSecondBtmCell : UICollectionViewCell<UITextViewDelegate>

//期望价格
@property (nonatomic, copy) void (^hopePriceBlock)(NSString *tfText);

//备注说明
@property (nonatomic, copy) void (^commentTextBlock)(NSString *tfText);

@end
