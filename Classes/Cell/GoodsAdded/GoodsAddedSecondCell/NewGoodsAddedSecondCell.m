//
//  NewGoodsAddedSecondCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedSecondCell.h"
#import "ShangjiaMacro.h"
@implementation NewGoodsAddedSecondCell{
    UILabel *nameLabel;
    UIImageView *nameView;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateParamLabel:) name:Shangjia_ParamItem_Notification object:nil];
        
        self.backgroundColor=[UIColor whiteColor];
#pragma mark 商品名称
        nameLabel=[UILabel new];
        nameLabel.font=[UIFont systemFontOfSize:16];
        nameLabel.textAlignment=NSTextAlignmentRight;
        nameLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self.contentView addSubview:nameLabel];
        
        //
        nameView=[UIImageView new];
        nameView.userInteractionEnabled=YES;
        [self.contentView addSubview:nameView];
        
        self.nameTF=[UITextField new];
        self.nameTF.font=[UIFont systemFontOfSize:16];
        self.nameTF.textColor=nameLabel.textColor;
        self.nameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        self.nameTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        self.nameTF.delegate=self;
        self.nameTF.returnKeyType=UIReturnKeyNext;
        [nameView addSubview:self.nameTF];
    }
    return self;
}

-(void)upDateParamLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    self.nameTF.text=model.typeName;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    nameLabel.frame=CGRectMake(10, 20, 84, 38);
    nameView.frame=CGRectMake(nameLabel.origin.x+nameLabel.width, nameLabel.origin.y, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+12), nameLabel.height);
    self.nameTF.frame=CGRectMake(4, 0, nameView.width-8, nameView.height);
}

-(void)setModel:(SubmitOrder_SeletedTypeModel *)model{
    
    nameLabel.text=[NSString stringWithFormat:@"%@：",model.paramArr_ItemName];
    SubmitOrder_SeletedTypeModel *mmodel=model.paramArr_Item[model.seleTag.integerValue];
    
    if ([model.input_type isEqualToString:@"0"]||[model.input_type isEqualToString:@"2"]) {
        nameView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        self.nameTF.placeholder=[NSString stringWithFormat:@"请输入%@",model.paramArr_ItemName];
        self.nameTF.text=model.attr_value;
        self.nameTF.enabled=YES;
        nameView.userInteractionEnabled=YES;
    }else if([model.input_type isEqualToString:@"1"]){
        nameView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        self.nameTF.placeholder=nil;
        self.nameTF.text=mmodel.typeName;
        self.nameTF.enabled=NO;
        nameView.userInteractionEnabled=NO;
    }else{
        nameView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        self.nameTF.placeholder=nil;
        self.nameTF.text=model.typeName;
        self.nameTF.enabled=YES;
        nameView.userInteractionEnabled=YES;
    }
}

-(void)selectSell:(NSString *)nameStr{
    //获取当前的indexPath
    UICollectionView *tableView = (UICollectionView *)self.contentView.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.delegate setNameTextfield:nameStr indexPath:indexPath];
}

#pragma mark - delegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self selectSell:textField.text];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
