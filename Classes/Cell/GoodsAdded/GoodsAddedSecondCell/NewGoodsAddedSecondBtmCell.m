//
//  NewGoodsAddedSecondBtmCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedSecondBtmCell.h"
#import "ShangjiaMacro.h"
@implementation NewGoodsAddedSecondBtmCell{
    UITextField *nameTF;
    UITextView *myTextView;
    UITextField *txtF;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDatePriceLabel:) name:Shangjia_HopePrice_Notification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateCommentLabel:) name:Shangjia_Comment_Notification object:nil];
        
#pragma mark 期望售价
        UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 20, 84, 38)];
        nameLabel.font=[UIFont systemFontOfSize:16];
        nameLabel.textAlignment=NSTextAlignmentRight;
        nameLabel.text=@"商品售价：";
        nameLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:nameLabel];
        
        UIView *nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width, nameLabel.origin.y, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+38), nameLabel.height)];
        nameView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        nameView.layer.borderWidth=0.7f;
        nameView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        [self addSubview:nameView];
        
        nameTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, nameView.width-8, nameView.height)];
        nameTF.font=[UIFont systemFontOfSize:16];
        nameTF.textColor=nameLabel.textColor;
        nameTF.placeholder=@"输入期望售价";
        nameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        nameTF.keyboardType=UIKeyboardTypeNumberPad;
        nameTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        [nameView addSubview:nameTF];
        [nameTF addTarget:self action:@selector(hopePriceChange:) forControlEvents:UIControlEventEditingChanged];
        
        UILabel *yLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameView.origin.x+nameView.width, nameView.origin.y, 26, nameView.height)];
        yLabel.font=[UIFont systemFontOfSize:16];
        yLabel.textAlignment=NSTextAlignmentCenter;
        yLabel.text=@" 元";
        yLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:yLabel];
        
#pragma mark 备注说明
        UILabel *bzLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, nameLabel.origin.y+nameLabel.height+20, nameLabel.width, nameLabel.height)];
        bzLabel.font=[UIFont systemFontOfSize:16];
        bzLabel.textAlignment=NSTextAlignmentRight;
        bzLabel.text=@"备注说明：";
        bzLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:bzLabel];
        
        // 给TextView添加带有内容和布局的容器
        myTextView=[[UITextView alloc] initWithFrame:CGRectMake(nameView.origin.x, bzLabel.origin.y,SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+12), 134)];
        myTextView.textColor=[UIColor colorWithHexString:@"#363636"];
        myTextView.font=[UIFont systemFontOfSize:16];
        myTextView.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        myTextView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        myTextView.layer.borderWidth=0.7f;
        myTextView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        myTextView.delegate=self;
        [self addSubview:myTextView];
        
        txtF=[[UITextField alloc] initWithFrame:CGRectMake(6, 9, myTextView.width-12, 21)];
        txtF.placeholder=@"请输入备注说明";
        txtF.font=[UIFont systemFontOfSize:16];
        txtF.enabled=NO;
        [myTextView addSubview:txtF];
    }
    return self;
}

-(void)upDatePriceLabel:(NSNotification *)noti{
    NSString *str=noti.object;
    nameTF.text=str;
}

-(void)upDateCommentLabel:(NSNotification *)noti{
    NSString *str=noti.object;
    txtF.text=str;
}

#pragma mark - textField
- (void)hopePriceChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.hopePriceBlock) {
        self.hopePriceBlock(field.text);
    }
}

#pragma mark - delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [myTextView isFirstResponder];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    txtF.hidden=textView.text.length==0?NO:YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    txtF.hidden=[textView.text isEqualToString:@""]?NO:YES;
    if (self.commentTextBlock) {
        self.commentTextBlock(textView.text);
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
