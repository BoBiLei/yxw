//
//  NewGoodsAddedSecondCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubmitOrder_SeletedTypeModel.h"

@protocol NewGoodsAddDelegate <NSObject>

-(void)setNameTextfield:(NSString *)nameStr indexPath:(NSIndexPath *)indexPath;

@end

@interface NewGoodsAddedSecondCell : UICollectionViewCell<UITextFieldDelegate>

@property (nonatomic, strong) SubmitOrder_SeletedTypeModel *model;

@property (nonatomic, strong) UITextField *nameTF;

@property (nonatomic, weak) id<NewGoodsAddDelegate> delegate;

@end
