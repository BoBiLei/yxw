//
//  GoodsEditBtmCell.h
//  MMG
//
//  Created by mac on 16/8/24.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsEditBottomDelegate <NSObject>

/**
 *  点击保存按钮
 */
-(void)clickSaveBtn;

/**
 *  点击保存并上架按钮
 */
-(void)clickSaveAndOffBtn;

@end

@interface GoodsEditBtmCell : UICollectionViewCell

@property (nonatomic, weak) id<GoodsEditBottomDelegate> delegate;

@property (nonatomic, assign) BOOL isGoodsManageer;

@end
