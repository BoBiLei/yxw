//
//  GoodsEditBtmCell.m
//  MMG
//
//  Created by mac on 16/8/24.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "GoodsEditBtmCell.h"

@implementation GoodsEditBtmCell{
    BOOL isSele;
    UIButton *agreeBtn;
    UIButton *leftBtn;
    UIButton *rightBtn;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 34)];
        [self addSubview:view];
        
        agreeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        agreeBtn.frame=CGRectMake(0, 0, view.height, view.height);
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_nor"] forState:UIControlStateNormal];
        [agreeBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [view addSubview:agreeBtn];
        [agreeBtn addTarget:self action:@selector(clickAgreeBtn) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *str=@"我已阅读同意《猫猫购收货协议》";
        CGSize size=[AppUtils getStringSize:str withFont:15];
        UILabel *Label=[[UILabel alloc] initWithFrame:CGRectMake(agreeBtn.origin.x+agreeBtn.width+4, agreeBtn.origin.y, size.width, agreeBtn.height)];
        [Label setText:str];
        Label.font=[UIFont systemFontOfSize:15];
        [view addSubview:Label];
        
        CGRect frame=view.frame;
        frame.size.width=agreeBtn.width+Label.width+4;
        view.frame=frame;
        view.center=CGPointMake(SCREENSIZE.width/2, 34);
        
#pragma mark 提交按钮
        CGFloat btnX=12;
        CGFloat btnY=view.origin.y+view.height+16;
        CGFloat btnWidth=(SCREENSIZE.width-36)/2;
        CGFloat btnHeight=44;
        for (int i=0 ; i<2; i++) {
            btnX=btnX+i*btnWidth+i*btnX;
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.layer.cornerRadius=2;
            btn.enabled=NO;
            btn.backgroundColor=[UIColor lightGrayColor];
            btn.frame=CGRectMake(btnX, btnY, btnWidth, btnHeight);
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.titleLabel.font=[UIFont systemFontOfSize:18];
            [btn setTitle:i==0?@"保存并重新上架":@"保存商品信息" forState:UIControlStateNormal];
            btn.tag=i;
            if (i==0) {
                leftBtn=btn;
            }else{
                rightBtn=btn;
            }
            [self addSubview:btn];
            [btn addTarget:self action:@selector(clickSubmitBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        CGFloat laseYH=btnY+btnHeight+49;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",laseYH] forKey:@"shangjia_fourcell_height"];
    }
    return self;
}

-(void)setIsGoodsManageer:(BOOL)isGoodsManageer{
    NSLog(@"%d-------",isGoodsManageer);
    if (isGoodsManageer) {
        leftBtn.hidden=YES;
        rightBtn.frame=CGRectMake(leftBtn.origin.x, leftBtn.origin.y, SCREENSIZE.width-2*leftBtn.origin.x, 44);
    }else{
        
    }
}

#pragma mark - 点击同意按钮
-(void)clickAgreeBtn{
    if (isSele) {
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_nor"] forState:UIControlStateNormal];
        leftBtn.enabled=NO;
        leftBtn.backgroundColor=[UIColor lightGrayColor];
        rightBtn.enabled=NO;
        rightBtn.backgroundColor=[UIColor lightGrayColor];
    }else{
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_sele"] forState:UIControlStateNormal];
        leftBtn.enabled=YES;
        leftBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        rightBtn.enabled=YES;
        rightBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    }
    isSele=!isSele;
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn:(id)sender{
    UIButton *btn=sender;
    if (btn.tag==0) {
        [self.delegate clickSaveBtn];
    }else{
        [self.delegate clickSaveAndOffBtn];
    }
}

@end
