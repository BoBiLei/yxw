//
//  NewGoodsAddedFirstCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewAddGoodsFirstDelegate <NSObject>

/**
 *  点击分类
 */
-(void)clickFenlei;

/**
 *  点击二级分类
 */
-(void)clickErjiFenlei;

/**
 *  点击商品品牌
 */
-(void)clickPinpai;

/**
 *  点击二手列表
 */
-(void)clickErShouList;

/**
 *  点击新按钮
 */
-(void)hadClickNewBtn;

/**
 *  点击二手按钮
 */
-(void)hadClickErShouBtn;

@end

@interface NewGoodsAddedFirstCell : UICollectionViewCell<UITextFieldDelegate>

@property (nonatomic, copy) void (^GoodsNameBlock)(NSString *tfText);

@property (nonatomic, weak) id<NewAddGoodsFirstDelegate> delegate;

@end
