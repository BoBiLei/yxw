//
//  CustomProductFenleiButton.h
//  MMG
//
//  Created by mac on 16/8/10.
//  Copyright © 2016年 猫猫购. All rights reserved.
//  自定义商品分类按钮

#import <UIKit/UIKit.h>

@interface CustomProductFenleiButton : UIControl

@property (nonatomic, strong) UILabel *titleLabel;          //
@property (nonatomic, strong) UIImageView *imageView;       //

@end
