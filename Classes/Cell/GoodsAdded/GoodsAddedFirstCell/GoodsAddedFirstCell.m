//
//  GoodsAddedFirstCell.m
//  MMG
//
//  Created by mac on 16/8/24.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "GoodsAddedFirstCell.h"
#import "CustomProductFenleiButton.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import "ShangjiaMacro.h"
#define Btn_Y_Margin 4
@implementation GoodsAddedFirstCell{
    CustomProductFenleiButton *seleUserInfoBtn;
    CustomProductFenleiButton *notSeleUserInfoBtn;
    UIView *nameView;
    UILabel *fenleiLabel;
    UITextField *nameTF;
    UITextField *fenleiTF;
    UITextField *fenlei_erjiTF;
    UIImageView *fenlei_erjiView;
    UITextField *ppTF;
    
    //二手
    UIImageView *ersImgv;
    UILabel *esLabel;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updataGoodNameLabel:) name:Shangjia_GoodName_Notification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateFenleiLabel:) name:Shangjia_Fenlei_Notification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateFenleiLabel:) name:Shangjia_Fenlei_Notification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateErjiFlLabel:) name:Shangjia_ErjiFl_Notification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBranceLabel:) name:Shangjia_Brance_Notification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUseInfoLabel:) name:Shangjia_UseInfo_Notification object:nil];
        
#pragma mark 商品名称
        UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 20, 84, 38)];
        nameLabel.font=[UIFont systemFontOfSize:16];
        nameLabel.textAlignment=NSTextAlignmentRight;
        nameLabel.text=@"商品名称：";
        nameLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:nameLabel];
        
        nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width, nameLabel.origin.y, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+12), nameLabel.height)];
        nameView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        nameView.layer.borderWidth=0.7f;
        nameView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        [self addSubview:nameView];
        
        nameTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, nameView.width-8, nameView.height)];
        nameTF.font=[UIFont systemFontOfSize:16];
        nameTF.textColor=nameLabel.textColor;
        nameTF.placeholder=@"输入商品名称";
        nameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        nameTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        [nameTF addTarget:self action:@selector(goodsNameChange:) forControlEvents:UIControlEventEditingChanged];
        [nameView addSubview:nameTF];
        
#pragma mark 商品分类
        fenleiLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+20, nameLabel.width, nameLabel.height)];
        fenleiLabel.font=[UIFont systemFontOfSize:16];
        fenleiLabel.textAlignment=NSTextAlignmentRight;
        fenleiLabel.text=@"商品分类：";
        fenleiLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:fenleiLabel];
        
        //
        UIImageView *fenleiView=[[UIImageView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width, fenleiLabel.origin.y, nameView.width, nameLabel.height)];
        fenleiView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        [self addSubview:fenleiView];
        fenleiView.userInteractionEnabled=YES;
        UITapGestureRecognizer *flTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickFenleiView)];
        [fenleiView addGestureRecognizer:flTap];
        
        fenleiTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, fenleiView.width-8, fenleiView.height)];
        fenleiTF.font=[UIFont systemFontOfSize:16];
        fenleiTF.textColor=nameLabel.textColor;
        fenleiTF.placeholder=@"请选择";
        fenleiTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        fenleiTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        fenleiTF.enabled=NO;
        [fenleiView addSubview:fenleiTF];
        
        fenlei_erjiView=[[UIImageView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width, fenleiView.origin.y+fenleiView.height+12, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+12), nameLabel.height)];
        fenlei_erjiView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        [self addSubview:fenlei_erjiView];
        fenlei_erjiView.userInteractionEnabled=YES;
        UITapGestureRecognizer *ejflTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickErjiFenleiView)];
        [fenlei_erjiView addGestureRecognizer:ejflTap];
        
        fenlei_erjiTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, fenlei_erjiView.width-8, fenlei_erjiView.height)];
        fenlei_erjiTF.font=[UIFont systemFontOfSize:16];
        fenlei_erjiTF.textColor=nameLabel.textColor;
        fenlei_erjiTF.placeholder=@"请选择";
        fenlei_erjiTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        fenlei_erjiTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        fenlei_erjiTF.enabled=NO;
        [fenlei_erjiView addSubview:fenlei_erjiTF];
        
#pragma mark 商品品牌
        UILabel *ppLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, fenlei_erjiView.origin.y+fenlei_erjiView.height+20, nameLabel.width, nameLabel.height)];
        ppLabel.font=[UIFont systemFontOfSize:16];
        ppLabel.textAlignment=NSTextAlignmentRight;
        ppLabel.text=@"商品品牌：";
        ppLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:ppLabel];
        
        UIImageView *ppView=[[UIImageView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width, fenlei_erjiView.origin.y+fenlei_erjiView.height+20, fenlei_erjiView.width, nameLabel.height)];
        ppView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        [self addSubview:ppView];
        ppView.userInteractionEnabled=YES;
        UITapGestureRecognizer *ppTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickPinpaiView)];
        [ppView addGestureRecognizer:ppTap];
        
        ppTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, ppView.width-8, ppView.height)];
        ppTF.font=[UIFont systemFontOfSize:16];
        ppTF.textColor=nameLabel.textColor;
        ppTF.placeholder=@"请选择";
        ppTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        ppTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        ppTF.enabled=NO;
        [ppView addSubview:ppTF];
        
#pragma mark 使用情况
        UILabel *useInfoLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, ppLabel.origin.y+ppLabel.height+16, ppLabel.width, ppLabel.height)];
        useInfoLabel.font=[UIFont systemFontOfSize:16];
        useInfoLabel.textAlignment=NSTextAlignmentRight;
        useInfoLabel.text=@"使用情况：";
        useInfoLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:useInfoLabel];
        
        CGFloat uBtnW=(nameView.width+12)/3;        //按钮宽度
        CGFloat uBtnH=38;                           //按钮高度
        CGFloat firstBtnX=nameView.origin.x;        //第一个按钮x轴
        CGFloat uBtnY=useInfoLabel.origin.y;        //按钮Y
        CGFloat laseYH=0;
        for (int i=0; i<2; i++) {
            uBtnY=uBtnY+i*uBtnH+i*4;
            CustomProductFenleiButton *btn=[[CustomProductFenleiButton alloc] init];
            btn.frame=CGRectMake(firstBtnX, uBtnY, uBtnW, uBtnH);
            btn.imageView.image=[UIImage imageNamed:i==0?@"spfl_sele":@"spfl_nor"];
            btn.titleLabel.text=i==0?@"全新":@"二手";
            btn.titleLabel.textColor=[UIColor colorWithHexString:i==0?@"#ec6b00":@"#363636"];
            btn.titleLabel.font=[UIFont systemFontOfSize:15];
            btn.titleLabel.adjustsFontSizeToFitWidth=YES;
            btn.tag=i;
            [self addSubview:btn];
            [btn addTarget:self action:@selector(clickUserInfoBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            if (i==0) {
                seleUserInfoBtn=btn;
            }else{
                notSeleUserInfoBtn=btn;
                laseYH=btn.origin.y;
            }
        }
        
        ersImgv=[[UIImageView alloc] initWithFrame:CGRectMake(firstBtnX+uBtnW-8, laseYH, SCREENSIZE.width/4, uBtnH)];
        ersImgv.image=[UIImage imageNamed:@"shangjia_ershou"];
        ersImgv.contentMode=UIViewContentModeScaleAspectFit;
        ersImgv.userInteractionEnabled=NO;
        [self addSubview:ersImgv];
        
        esLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ersImgv.width-20, ersImgv.height)];
        esLabel.textColor=[UIColor colorWithHexString:@"#b7b7b7"];
        esLabel.font=[UIFont systemFontOfSize:15];
        esLabel.textAlignment=NSTextAlignmentCenter;
        esLabel.text=@"99成新";
        [ersImgv addSubview:esLabel];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectErShou)];
        [ersImgv addGestureRecognizer:tap];
    }
    return self;
}

-(void)updataGoodNameLabel:(NSNotification *)noti{
    NSString *str=noti.object;
    nameTF.text=str;
}

-(void)upDateFenleiLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    fenleiTF.text=model.typeName;
}

-(void)updateErjiFlLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    if ([model.typeId isEqualToString:@"0"]) {
        fenlei_erjiTF.placeholder=@"不可选择";
        fenlei_erjiTF.text=nil;
        fenlei_erjiView.userInteractionEnabled=NO;
        fenlei_erjiView.backgroundColor=[UIColor lightGrayColor];
    }else{
        fenlei_erjiTF.text=model.typeName;
        fenlei_erjiView.userInteractionEnabled=YES;
    }
}

-(void)updateBranceLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    ppTF.text=model.typeName;
}

-(void)updateUseInfoLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    esLabel.text=model.typeName;
    if (![model.typeId isEqualToString:@"1"]) {
        seleUserInfoBtn.imageView.image=[UIImage imageNamed:@"spfl_nor"];
        seleUserInfoBtn.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        notSeleUserInfoBtn.imageView.image=[UIImage imageNamed:@"spfl_sele"];
        notSeleUserInfoBtn.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
        ersImgv.userInteractionEnabled=YES;
        esLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        seleUserInfoBtn=notSeleUserInfoBtn;
    }
}

#pragma mark - 点击使用情况
-(void)clickUserInfoBtn:(id)sender{
    seleUserInfoBtn.imageView.image=[UIImage imageNamed:@"spfl_nor"];
    seleUserInfoBtn.titleLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    
    CustomProductFenleiButton *btn=sender;
    if (btn.tag==1) {
        [self.delegate hadClickErShouBtn];
    }else{
        [self.delegate hadClickNewBtn];
    }
    ersImgv.userInteractionEnabled=btn.tag==0?NO:YES;
    esLabel.textColor=[UIColor colorWithHexString:btn.tag==0?@"#b7b7b7":@"#363636"];
    btn.imageView.image=[UIImage imageNamed:@"spfl_sele"];
    btn.titleLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
    seleUserInfoBtn=btn;
}

#pragma mark - 选择二手列表
-(void)selectErShou{
    [self.delegate clickErShouList];
}

#pragma mark - 选择商品分类
-(void)clickFenleiView{
    [self.delegate clickFenlei];
}

-(void)clickErjiFenleiView{
    [self.delegate clickErjiFenlei];
}

-(void)clickPinpaiView{
    [self.delegate clickPinpai];
}

#pragma mark - textField
- (void)goodsNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.GoodsNameBlock) {
        self.GoodsNameBlock(field.text);
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
