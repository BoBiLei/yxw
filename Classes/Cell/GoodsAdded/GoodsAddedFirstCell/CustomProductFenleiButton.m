//
//  CustomProductFenleiButton.m
//  MMG
//
//  Created by mac on 16/8/10.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "CustomProductFenleiButton.h"

@implementation CustomProductFenleiButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.titleLabel];
    [self addSubview:self.imageView];
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.textAlignment=NSTextAlignmentLeft;
    }
    return _titleLabel;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView=[[UIImageView alloc] init];
        _imageView.contentMode=UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.imageView.frame = CGRectMake(1, 3, width/3-4, height-6);
    self.titleLabel.frame = CGRectMake(width/3+3, 0, width-(width/3+3), height);
}

@end
