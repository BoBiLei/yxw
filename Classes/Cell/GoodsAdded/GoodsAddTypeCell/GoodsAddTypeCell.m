//
//  GoodsAddTypeCell.m
//  MMG
//
//  Created by mac on 16/8/19.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "GoodsAddTypeCell.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import "ShangjiaMacro.h"
@implementation GoodsAddTypeCell{
    UITextField *leixTF;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateGoodTypeLabel:) name:Shangjia_GoodType_Notification object:nil];
        
#pragma mark 商品类型
        UILabel *leixLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 20, 84, 38)];
        leixLabel.font=[UIFont systemFontOfSize:16];
        leixLabel.textAlignment=NSTextAlignmentRight;
        leixLabel.text=@"商品类型：";
        leixLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        [self addSubview:leixLabel];
        
        UIImageView *leixView=[[UIImageView alloc] initWithFrame:CGRectMake(leixLabel.origin.x+leixLabel.width, leixLabel.origin.y, SCREENSIZE.width-(leixLabel.origin.x+leixLabel.width+12), leixLabel.height)];
        leixView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        [self addSubview:leixView];
        leixView.userInteractionEnabled=YES;
        UITapGestureRecognizer *ppTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickLeixView)];
        [leixView addGestureRecognizer:ppTap];
        
        leixTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, leixView.width-8, leixView.height)];
        leixTF.font=[UIFont systemFontOfSize:16];
        leixTF.textColor=leixLabel.textColor;
        leixTF.placeholder=@"请选择商品类型";
        leixTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        leixTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        leixTF.enabled=NO;
        [leixView addSubview:leixTF];
        
    }
    return self;
}

#pragma mark - 点击类型View
-(void)clickLeixView{
    [self.delegate clickGoodsType];
}

-(void)upDateGoodTypeLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    leixTF.text=model.typeName;
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
