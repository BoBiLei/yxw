//
//  GoodsAddTypeCell.h
//  MMG
//
//  Created by mac on 16/8/19.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsTypeDelegate <NSObject>

/**
 *  点击类型
 */
-(void)clickGoodsType;


@end

@interface GoodsAddTypeCell : UICollectionViewCell

@property (nonatomic, weak) id<GoodsTypeDelegate> delegate;

@end
