//
//  NewGoodsAddedFourCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SubmitGoodsDelegate <NSObject>

/**
 *  点击提交按钮
 */
-(void)clickSubmitBtn;

@end

@interface NewGoodsAddedFourCell : UICollectionViewCell

@property (nonatomic, weak) id<SubmitGoodsDelegate> delegate;

@end
