//
//  NewGoodsAddedFourCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedFourCell.h"

@implementation NewGoodsAddedFourCell{
    BOOL isSele;
    UIButton *agreeBtn;
    UIButton *subBtn;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 34)];
        [self addSubview:view];
        
        agreeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        agreeBtn.frame=CGRectMake(0, 0, view.height, view.height);
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_nor"] forState:UIControlStateNormal];
        [agreeBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [view addSubview:agreeBtn];
        [agreeBtn addTarget:self action:@selector(clickAgreeBtn) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *str=@"我已阅读同意《猫猫购收货协议》";
        CGSize size=[AppUtils getStringSize:str withFont:15];
        UILabel *Label=[[UILabel alloc] initWithFrame:CGRectMake(agreeBtn.origin.x+agreeBtn.width+4, agreeBtn.origin.y, size.width, agreeBtn.height)];
        [Label setText:str];
        Label.font=[UIFont systemFontOfSize:15];
        [view addSubview:Label];
        
        CGRect frame=view.frame;
        frame.size.width=agreeBtn.width+Label.width+4;
        view.frame=frame;
        view.center=CGPointMake(SCREENSIZE.width/2, 34);
        
#pragma mark 提交按
        subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        subBtn.layer.cornerRadius=2;
        subBtn.enabled=NO;
        subBtn.backgroundColor=[UIColor lightGrayColor];
        subBtn.frame=CGRectMake(12, view.origin.y+view.height+16, SCREENSIZE.width-24, 44);
        [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        subBtn.titleLabel.font=[UIFont systemFontOfSize:18];
        [subBtn setTitle:@"提交寄售信息" forState:UIControlStateNormal];
        [self addSubview:subBtn];
        [subBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

#pragma mark - 点击同意按钮
-(void)clickAgreeBtn{
    if (isSele) {
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_nor"] forState:UIControlStateNormal];
        subBtn.enabled=NO;
        subBtn.backgroundColor=[UIColor lightGrayColor];
    }else{
        [agreeBtn setImage:[UIImage imageNamed:@"editgoods_sele"] forState:UIControlStateNormal];
        subBtn.enabled=YES;
        subBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    }
    isSele=!isSele;
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [self.delegate clickSubmitBtn];
}

@end
