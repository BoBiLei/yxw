//
//  HomePageBottomCell.h
//  CatShopping
//
//  Created by mac on 15/9/18.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageBottomModel.h"

@interface HomePageBottomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *title;

-(void)reflushForModel:(HomePageBottomModel *)model;

@end
