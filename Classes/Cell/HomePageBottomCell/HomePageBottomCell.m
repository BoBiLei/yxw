//
//  HomePageBottomCell.m
//  CatShopping
//
//  Created by mac on 15/9/18.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "HomePageBottomCell.h"

@implementation HomePageBottomCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)reflushForModel:(HomePageBottomModel *)model{
    [self.imgv setImage:[UIImage imageNamed:model.iconStr]];
    self.title.text=model.nameStr;
}

@end
