//
//  TopicCell.h
//  MMG
//
//  Created by mac on 15/10/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@end
