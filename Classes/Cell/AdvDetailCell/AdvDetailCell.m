//
//  AdvDetailCell.m
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "AdvDetailCell.h"
#import "Macro2.h"
@implementation AdvDetailCell

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(AdvDetailModel *)model{
    
    //获取当前cell frame
    CGRect frame = [self frame];
    
    CGFloat imgWFloat=model.imgW.floatValue;
    CGFloat imgHFloat=model.imgH.floatValue;
    CGFloat imgH=(SCREENSIZE.width*imgHFloat)/imgWFloat;
    
    _imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, imgH)];
    _imgv.contentMode=UIViewContentModeScaleAspectFit;
    NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost,model.imgStr];
    [_imgv setImageWithURL:[NSURL URLWithString:imgStr]];
    [self addSubview:_imgv];
    
    CGFloat cellHeight;
    
    cellHeight=imgH;
    
    frame.size.height =cellHeight;
    
    //重新设置cell height
    self.frame = frame;
    
}

@end
