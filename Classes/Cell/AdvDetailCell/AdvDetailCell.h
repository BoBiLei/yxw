//
//  AdvDetailCell.h
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvDetailModel.h"

@interface AdvDetailCell : UITableViewCell

@property(nonatomic,strong) UIImageView *imgv;

-(void)reflushDataForModel:(AdvDetailModel *)model;

@end
