//
//  CouponCell.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMGCouponModel.h"

@interface CouponCell : UITableViewCell

@property (nonatomic,copy) NSString *couponId;
@property (nonatomic ,strong) UILabel *couponNumberLabel;
@property (nonatomic ,strong) UILabel *priceSumLabel;
@property (nonatomic ,strong) UILabel *validityLabel;
@property (nonatomic ,strong) UILabel *statusLabel;//状态

-(void)reflushDataForModel:(MMGCouponModel *)model;

@end
