//
//  CouponCell.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CouponCell.h"

#define CellFont 13
@implementation CouponCell

- (void)awakeFromNib {
    //
    _couponNumberLabel=[[UILabel alloc]init];
    _couponNumberLabel.text=@"1235541987438";
    _couponNumberLabel.font=[UIFont systemFontOfSize:CellFont];
    _couponNumberLabel.textAlignment=NSTextAlignmentCenter;
    _couponNumberLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_couponNumberLabel];
    _couponNumberLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *stageStr=[NSString stringWithFormat:@"H:|[_couponNumberLabel(%f)]",(SCREENSIZE.width-16)/3+6];
    NSArray* _stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:stageStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(_couponNumberLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_h];
    
    NSArray* _stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_couponNumberLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_couponNumberLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_w];
    
    //line01
    UIView *line01=[[UIView alloc]init];
    line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line01];
    line01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_couponNumberLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_couponNumberLabel,line01)];
    [NSLayoutConstraint activateConstraints:line01_h];
    
    NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
    [NSLayoutConstraint activateConstraints:line01_w];
    
    //
    _priceSumLabel=[[UILabel alloc]init];
    _priceSumLabel.text=@"500元";
    _priceSumLabel.font=[UIFont systemFontOfSize:CellFont];
    _priceSumLabel.textAlignment=NSTextAlignmentCenter;
    _priceSumLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_priceSumLabel];
    _priceSumLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line01][_priceSumLabel(64)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,_priceSumLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_h];
    
    NSArray* _cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_priceSumLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceSumLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_w];
    
    //line02
    UIView *line02=[[UIView alloc]init];
    line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line02];
    line02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_priceSumLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceSumLabel,line02)];
    [NSLayoutConstraint activateConstraints:line02_h];
    
    NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
    [NSLayoutConstraint activateConstraints:line02_w];
    
    //有效期
    _validityLabel=[[UILabel alloc]init];
    _validityLabel.text=@"2016-12-31";
    _validityLabel.font=[UIFont systemFontOfSize:CellFont];
    _validityLabel.textAlignment=NSTextAlignmentCenter;
    _validityLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_validityLabel];
    _validityLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *validStr=[NSString stringWithFormat:@"H:[line02][_validityLabel(%f)]",(SCREENSIZE.width-16)/4+10];
    NSArray* _priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:validStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,_validityLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_h];
    
    NSArray* _priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_validityLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_validityLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_w];
    
    //line03
    UIView *line03=[[UIView alloc]init];
    line03.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line03];
    line03.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line03_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_validityLabel][line03(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_validityLabel,line03)];
    [NSLayoutConstraint activateConstraints:line03_h];
    
    NSArray* line03_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line03]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03)];
    [NSLayoutConstraint activateConstraints:line03_w];
    
    //状态
    _statusLabel=[[UILabel alloc]init];
    _statusLabel.text=@"未使用";
    _statusLabel.font=[UIFont systemFontOfSize:CellFont];
    _statusLabel.textAlignment=NSTextAlignmentCenter;
    _statusLabel.textColor=[UIColor colorWithHexString:@"#00a650"];
    [self addSubview:_statusLabel];
    _statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _statusLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line03][_statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03,_statusLabel)];
    [NSLayoutConstraint activateConstraints:_statusLabel_h];
    
    NSArray* _statusLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_statusLabel)];
    [NSLayoutConstraint activateConstraints:_statusLabel_w];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(MMGCouponModel *)model{
    if ([model.status isEqualToString:@"未使用"]) {
        _statusLabel.textColor=[UIColor redColor];
    }
    _couponId=model.couponId;
    _couponNumberLabel.text=model.couponNumber;
    _priceSumLabel.text=model.priceSum;
    _validityLabel.text=model.validity;
    _statusLabel.text=model.status;
}

@end
