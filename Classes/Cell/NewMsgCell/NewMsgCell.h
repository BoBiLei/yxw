//
//  NewMsgCell.h
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMsgModel.h"
@interface NewMsgCell : UITableViewCell

@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UIImageView *goodImgv;
@property(nonatomic,strong) UILabel *goodTitle;

-(void)reflushModelDataForModel:(NewMsgModel *)model;
@end
