//
//  NewMsgCell.m
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "NewMsgCell.h"
#import "Macro2.h"
@implementation NewMsgCell

- (void)awakeFromNib {
    self.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    //timeLabel
    _timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 8, 180, 21)];
    _timeLabel.center=CGPointMake(SCREENSIZE.width/2, 18);
    _timeLabel.backgroundColor=[UIColor colorWithHexString:@"#cccccc"];
    _timeLabel.textColor=[UIColor whiteColor];
    _timeLabel.font=[UIFont systemFontOfSize:15];
    _timeLabel.textAlignment=NSTextAlignmentCenter;
    _timeLabel.text=@"2015年11月26日  18:01";
    [self addSubview:_timeLabel];
    
    //contentView
    UIView *contentView=[[UIView alloc]initWithFrame:CGRectMake(8, _timeLabel.frame.origin.y+_timeLabel.frame.size.height+8, SCREENSIZE.width-16, 290)];
    contentView.backgroundColor=[UIColor whiteColor];
    contentView.layer.cornerRadius=4;
    [self addSubview:contentView];
    
    //goodImgv
    _goodImgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, contentView.width-16, 200)];
    _goodImgv.contentMode=UIViewContentModeScaleAspectFit;
    _goodImgv.image=[UIImage imageNamed:@"newMsg_dfimg"];
    [contentView addSubview:_goodImgv];
    
    //goodTitle
    _goodTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, _goodImgv.frame.origin.y+_goodImgv.frame.size.height+4, contentView.width-16, 36)];
    _goodTitle.textColor=[UIColor lightGrayColor];
    _goodTitle.font=[UIFont systemFontOfSize:14];
    _goodTitle.numberOfLines=0;
    _goodTitle.text=@"HERMES KELLY 28 CK 银扣 紫 鳄鱼皮两点 外缝 R刻 95新";
    [contentView addSubview:_goodTitle];
    
    //line
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(8, _goodTitle.frame.origin.y+_goodTitle.frame.size.height+6, contentView.width-16, 0.8f)];
    line.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [contentView addSubview:line];
    
    //readLabel
    UILabel *readLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, line.frame.origin.y+line.frame.size.height+6, 160, 21)];
    readLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    readLabel.font=[UIFont systemFontOfSize:16];
    readLabel.text=@"阅读全文";
    [contentView addSubview:readLabel];
    
    //arrowImg
    UIImageView *arrowImg=[[UIImageView alloc]initWithFrame:CGRectMake(contentView.width-16, readLabel.frame.origin.y+3, 8, 16)];
    arrowImg.image=[UIImage imageNamed:@"newMsg_arrow"];
    [contentView addSubview:arrowImg];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushModelDataForModel:(NewMsgModel *)model{
    _timeLabel.text=model.msgTime;
    [_goodImgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.msgImgStr]] placeholderImage:[UIImage imageNamed:@"placeholder_320x160"]];
    _goodTitle.text=model.msgTitle;
}

@end
