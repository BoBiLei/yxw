//
//  UserOrdersCell.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserOrdersModel.h"


@interface UserOrdersCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *goodTitle;
@property (weak, nonatomic) IBOutlet UILabel *goodPrice;

-(void)reflushDataForModel:(UserOrdersModel *)model;

@end
