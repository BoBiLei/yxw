//
//  UserOrdersCell.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "UserOrdersCell.h"

@implementation UserOrdersCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(UserOrdersModel *)model{
    self.goodTitle.text=model.goodTitle;
    self.goodPrice.text=model.goodPrice;
}

@end
