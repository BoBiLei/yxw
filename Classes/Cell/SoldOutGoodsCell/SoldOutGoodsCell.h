//
//  SoldOutGoodsCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaijiaGoodsManagerModel.h"

@protocol SoldOutGoodsDelegate <NSObject>

@optional
/**
 点击赛选按钮
 */
-(void)selectedGoodCellForModel:(MaijiaGoodsManagerModel *)model indexPath:(NSIndexPath *)indexPath;

/**
 *  编辑商品
 */
-(void)editGoods:(MaijiaGoodsManagerModel *)model;

/**
 *  重新上架
 */
-(void)reShangjia:(MaijiaGoodsManagerModel *)model;

/**
 *  删除商品
 */
-(void)deleteGoods:(MaijiaGoodsManagerModel *)model;

@end

@interface SoldOutGoodsCell : UITableViewCell

@property (nonatomic,strong) MaijiaGoodsManagerModel *model;

@property (nonatomic, weak) id<SoldOutGoodsDelegate> delegate;

@property (nonatomic, assign) BOOL isSelePiliang;

@property(assign,nonatomic)BOOL selectState;//选中状态

-(void)reflushDataForModel:(MaijiaGoodsManagerModel *)model andIsXiajia:(BOOL)isXiajia;

@end
