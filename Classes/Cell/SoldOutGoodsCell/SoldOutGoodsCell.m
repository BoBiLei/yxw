//
//  SoldOutGoodsCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "SoldOutGoodsCell.h"
#import <TTTAttributedLabel.h>
#import "Macro2.h"
@implementation SoldOutGoodsCell{
    UIImageView *imgv;
    UIImageView *llImgv;
    UILabel *nameLabel;
    TTTAttributedLabel *sellPriceLabel;
    UILabel *stageLabel;
    UILabel *sxfLabel;
    UIView *contain_topView;
    UIImageView *seleImgv;
    UIButton *firstBtn;
    UIButton *secondBtn;
    UIButton *threeBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#pragma mark 图片
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(12, 12, SCREENSIZE.width/3, SCREENSIZE.width/3)];
        imgv.layer.borderWidth=0.5f;
        imgv.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        imgv.contentMode=UIViewContentModeScaleToFill;
        
        CGFloat lliHeight=imgv.width/5;
        llImgv=[[UIImageView alloc] initWithFrame:CGRectMake(imgv.width/2, imgv.height-lliHeight, imgv.width/2, lliHeight)];
        [imgv addSubview:llImgv];
        
        seleImgv=[[UIImageView alloc] initWithFrame:CGRectMake(imgv.origin.x-5, imgv.origin.y-5, imgv.width/3+4, imgv.width/3+4)];
        seleImgv.image=[UIImage imageNamed:@"editgoods_nor"];
        
        CGFloat rightH=imgv.height/4;
        CGFloat lineH=0.6f;
#pragma mark 名称
        nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgv.origin.x+imgv.width+10, imgv.origin.y, SCREENSIZE.width-(imgv.origin.x+imgv.width+20), rightH)];
        nameLabel.numberOfLines=0;
        nameLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        nameLabel.font=[UIFont systemFontOfSize:14];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, nameLabel.origin.y+nameLabel.height)];
        
#pragma mark 寄售价
        sellPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height, nameLabel.width, rightH)];
        sellPriceLabel.numberOfLines=0;
        sellPriceLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sellPriceLabel.font=[UIFont systemFontOfSize:14];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, sellPriceLabel.origin.y+sellPriceLabel.height)];
        
#pragma mark 分期价
        stageLabel=[[UILabel alloc] initWithFrame:CGRectMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height, sellPriceLabel.width, rightH)];
        stageLabel.numberOfLines=0;
        stageLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        stageLabel.font=[UIFont systemFontOfSize:14];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(stageLabel.origin.x, stageLabel.origin.y+stageLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, stageLabel.origin.y+stageLabel.height)];
        
#pragma mark 手续费
        sxfLabel=[[UILabel alloc] initWithFrame:CGRectMake(sellPriceLabel.origin.x, stageLabel.origin.y+stageLabel.height, stageLabel.width, rightH)];
        sxfLabel.numberOfLines=0;
        sxfLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sxfLabel.font=[UIFont systemFontOfSize:14];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sxfLabel.origin.x, sxfLabel.origin.y+sxfLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, sxfLabel.origin.y+sxfLabel.height)];
        
        contain_topView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, imgv.origin.y+imgv.height+16)];
        [self addSubview:contain_topView];
        [contain_topView addSubview:imgv];
        [contain_topView addSubview:seleImgv];
        [contain_topView addSubview:nameLabel];
        [contain_topView addSubview:sellPriceLabel];
        [contain_topView addSubview:stageLabel];
        [contain_topView addSubview:sxfLabel];
        
        contain_topView.userInteractionEnabled=YES;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectSell)];
        [contain_topView addGestureRecognizer:tap];
        
#pragma mark 三个按钮
        CGFloat lsYH=[self createBtn:@[@"编辑商品",@"商品下架",@"删除商品"] andView:contain_topView];
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lsYH] forKey:@"SoldOutGoodsCell_Height"];
    }
    return self;
}

-(CGFloat)createBtn:(NSArray *)array andView:(UIView *)view{
    CGFloat lastYH=0;
    CGFloat btnX=36;
    CGFloat btnY=view.origin.y+view.height;
    CGFloat btnW=(SCREENSIZE.width-40)/3;
    CGFloat btnHeight=36;
    for (int i=0; i<array.count; i++) {
        btnX=12+i*btnW+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        btn.layer.cornerRadius=2;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        switch (i) {
            case 0:
                firstBtn=btn;
                break;
            case 1:
                secondBtn=btn;
                break;
            default:
                threeBtn=btn;
                break;
        }
        
        lastYH=btn.origin.y+btn.height+20;
    }
    return lastYH;
}

-(void)clcikBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
            [self.delegate editGoods:self.model];
            break;
        case 1:
            [self.delegate reShangjia:self.model];
            break;
        default:
            [self.delegate deleteGoods:self.model];
            break;
    }
}

-(void)setIsSelePiliang:(BOOL)isSelePiliang{
    [UIView animateWithDuration:0.3 animations:^{
        if (isSelePiliang) {
            contain_topView.userInteractionEnabled=YES;
            firstBtn.enabled=NO;
            secondBtn.enabled=NO;
            threeBtn.enabled=NO;
            seleImgv.hidden=NO;
            firstBtn.backgroundColor=[UIColor colorWithHexString:@"#959595"];
            secondBtn.backgroundColor=[UIColor colorWithHexString:@"#959595"];
            threeBtn.backgroundColor=[UIColor colorWithHexString:@"#959595"];
        }else{
            contain_topView.userInteractionEnabled=NO;
            firstBtn.enabled=YES;
            secondBtn.enabled=YES;
            threeBtn.enabled=YES;
            seleImgv.hidden=YES;
            firstBtn.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
            secondBtn.backgroundColor=[UIColor colorWithHexString:@"#e13f3f"];
            threeBtn.backgroundColor=[UIColor colorWithHexString:@"#e13f3f"];
        }
    }];
}

-(void)selectSell{
    //获取当前的indexPath
    UITableView *tableView = (UITableView *)self.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.delegate selectedGoodCellForModel:self.model indexPath:indexPath];
}

-(void)reflushDataForModel:(MaijiaGoodsManagerModel *)model andIsXiajia:(BOOL)isXiajia{
    
    [imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImg]]];
    
    if(isXiajia){
        llImgv.image=[UIImage imageNamed:@"maijia_xiajia"];
    }else{
        llImgv.image=[UIImage imageNamed:[model.jishou_status isEqualToString:@"1"]?@"maijia_ycs":@"maijia_jsz"];
    }
    
    nameLabel.text=[NSString stringWithFormat:@"名  称：%@",model.goodsTitle];
    
    NSString *sellpStr=[NSString stringWithFormat:@"寄售价：%@元",model.goodsPrice];
    [sellPriceLabel setText:sellpStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",model.goodsPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff0000"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    NSString *fqiStr=[NSString stringWithFormat:@"%@",model.stagePrice];
    NSRange rang=[fqiStr rangeOfString:@"."];
    if (rang.location!=NSNotFound) {
        fqiStr=[fqiStr substringToIndex:rang.location];
    }
    stageLabel.text=[NSString stringWithFormat:@"分期价：%@元*12期",fqiStr];
    
    //
    NSString *sxfStr=@"";
    NSString *sum;
    for (int i=0; i<model.fenqi_fee.count; i++) {
        sum=[NSString stringWithFormat:@"%d期%@ ",i*3+3,model.fenqi_fee[i]];
        sxfStr=[sxfStr stringByAppendingString:sum];
        
    }
    sxfLabel.text=[NSString stringWithFormat:@"手续费：%@",sxfStr];
    
    [secondBtn setTitle:isXiajia?@"重新上架":@"商品下架" forState:UIControlStateNormal];
    
    
    self.model=model;       //重要
    if (model.selectState){
        _selectState = YES;
        seleImgv.image=[UIImage imageNamed:@"editgoods_sele"];
    }else{
        _selectState = NO;
        seleImgv.image=[UIImage imageNamed:@"editgoods_nor"];
    }
}

@end
