//
//  SellerDetailCell.m
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SellerDetailCell.h"
#import "Macro2.h"
@implementation SellerDetailCell

- (void)awakeFromNib {
    _joinShopCartBtn.layer.cornerRadius=2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(SellerDetailModel *)model{
    self.model=model;
    self.goodsId=model.goodsId;
    NSString *imgUrlStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImgStr];
    [self.imgv setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:[UIImage imageNamed:DEFAULTImage]];
    self.titleLabel.text=model.goodsTitleStr;
    self.mmPriceLabel.text=model.mmPrice;
    self.stagePriceLabel.text=model.stagePrice;
    if ([model.goods_num isEqualToString:@"0"]) {
        self.joinShopCartBtn.enabled=NO;
        self.joinShopCartBtn.backgroundColor=[UIColor lightGrayColor];
    }
}

- (IBAction)clickJoinShopCartBtn:(id)sender {
    [self.delegate joinShoppingCartForGoodId:self.model];
}

@end
