//
//  SellerDetailCell.h
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SellerDetailModel.h"

@protocol SellerDetailDelegate <NSObject>

-(void)joinShoppingCartForGoodId:(SellerDetailModel *)model;

@end

@interface SellerDetailCell : UITableViewCell

@property (copy, nonatomic) NSString *goodsId;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mmPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *stagePriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *joinShopCartBtn;
@property (assign,nonatomic) SellerDetailModel *model;
@property (weak, nonatomic) id<SellerDetailDelegate> delegate;


-(void)reflushDataForModel:(SellerDetailModel *)model;

@end
