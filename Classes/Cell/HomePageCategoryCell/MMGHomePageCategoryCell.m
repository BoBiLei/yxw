//
//  HomePageCategoryCell.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "MMGHomePageCategoryCell.h"
#import "Macro2.h"
@implementation MMGHomePageCategoryCell

- (void)awakeFromNib {
    self.title.textColor=[UIColor colorWithHexString:@"#282828"];
}

-(void)reflushDataForModel:(MMGHomePageCagegoryModel *)model{
    self.title.text=model.name;
    NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost,model.img];
    [self.imgv setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:[UIImage imageNamed:@"placeholder_100x100"]];
}

@end
