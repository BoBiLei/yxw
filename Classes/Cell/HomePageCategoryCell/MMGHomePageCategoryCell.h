//
//  HomePageCategoryCell.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMGHomePageCagegoryModel.h"

@interface MMGHomePageCategoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@property (weak, nonatomic) IBOutlet UILabel *title;

-(void)reflushDataForModel:(MMGHomePageCagegoryModel *)model;

@end
