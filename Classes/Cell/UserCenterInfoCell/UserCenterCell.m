//
//  UserCenterCell.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "UserCenterCell.h"

@implementation UserCenterCell

- (void)awakeFromNib {
    self.title.textColor=[UIColor colorWithHexString:@"#292929"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
