//
//  MMGUserInfoCell.m
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGUserInfoCell.h"
#import "ShangjiaMacro.h"
#import "Macro2.h"
@implementation MMGUserInfoCell

- (void)awakeFromNib {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePhotoNoti) name:NotificationUpdatePhoto object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfoNoti) name:NotifitionLoginSuccess object:nil];
    //头像
    _imgv=[[UIImageView alloc]initWithFrame:CGRectMake(20, 18, 56, 56)];
    _imgv.backgroundColor=[UIColor colorWithHexString:@"#282828"];
    _imgv.layer.cornerRadius=_imgv.frame.size.width/2;
    _imgv.layer.masksToBounds=YES;
    if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
        _imgv.image=[UIImage imageNamed:@"user_default"];
    }else{
        [_imgv setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
    }
    _imgv.contentMode=UIViewContentModeScaleAspectFit;
    [self addSubview:_imgv];
    
    //用户名
    _userNameLabel=[[UILabel alloc]init];
    _userNameLabel.font=[UIFont systemFontOfSize:15];
    _userNameLabel.textColor=[UIColor colorWithHexString:@"#292929"];
    [self addSubview:_userNameLabel];
    _userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* nameLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_imgv]-12-[_userNameLabel]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv,_userNameLabel)];
    [NSLayoutConstraint activateConstraints:nameLabel_h];
    
    NSArray* nameLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[_userNameLabel(22)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_userNameLabel)];
    [NSLayoutConstraint activateConstraints:nameLabel_w];
    _userNameLabel.text=[NSString stringWithFormat:@"用户  %@",[AppUtils getValueWithKey:User_Name]];
    
    //用户余额
    _userBalanceLabel=[[UILabel alloc]init];
    _userBalanceLabel.font=[UIFont systemFontOfSize:15];
    _userBalanceLabel.textColor=[UIColor colorWithHexString:@"#292929"];
    [self addSubview:_userBalanceLabel];
    _userBalanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* moneyLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_imgv]-12-[_userBalanceLabel]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv,_userBalanceLabel)];
    [NSLayoutConstraint activateConstraints:moneyLabel_h];
    
    NSArray* moneyLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_userNameLabel]-6-[_userBalanceLabel(22)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_userNameLabel,_userBalanceLabel)];
    [NSLayoutConstraint activateConstraints:moneyLabel_w];
    _userBalanceLabel.text=[NSString stringWithFormat:@"余额  %@",[AppUtils getValueWithKey:User_Money]];
}

-(void)updatePhotoNoti{
    [_imgv setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
}

-(void)updateUserInfoNoti{
    if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
        _imgv.image=[UIImage imageNamed:@"user_default"];
    }else{
        [_imgv setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
    }
    _userNameLabel.text=[NSString stringWithFormat:@"用户  %@",[AppUtils getValueWithKey:User_Name]];
    _userBalanceLabel.text=[NSString stringWithFormat:@"余额  %@",[AppUtils getValueWithKey:User_Money]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
