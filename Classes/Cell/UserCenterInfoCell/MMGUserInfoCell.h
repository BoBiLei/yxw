//
//  MMGUserInfoCell.h
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMGUserInfoCell : UITableViewCell

@property (strong, nonatomic) UIImageView *imgv;
@property (strong, nonatomic) UILabel *userNameLabel;
@property (strong, nonatomic) UILabel *userBalanceLabel;

@end
