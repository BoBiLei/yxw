//
//  MaijiaInfoCell.m
//  MMG
//
//  Created by mac on 16/8/10.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaInfoCell.h"
#import "Macro2.h"
#define MJCell_Height 92
@implementation MaijiaInfoCell{
    UIImageView *phoneView;
    UILabel *dpNameLabel;
    UILabel *yyeLabel;
    UILabel *spSumLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfoNoti) name:NotifitionLoginSuccess object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unDatePhone) name:NotificationUpdatePhoto object:nil];
        
        CGRect frame=self.frame;
        frame.size.height=MJCell_Height;
        self.frame=frame;
        
        phoneView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.height*2/3, self.height*2/3)];
        phoneView.center=CGPointMake(phoneView.width-6, self.height/2);
        phoneView.layer.cornerRadius=phoneView.height/2;
        phoneView.layer.masksToBounds=YES;
        if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
            phoneView.image=[UIImage imageNamed:@"user_default"];
        }else{
            [phoneView setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
        }
        phoneView.contentMode=UIViewContentModeScaleAspectFit;
        [self addSubview:phoneView];
        
        //
        dpNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(phoneView.origin.x+phoneView.width+16, phoneView.origin.y, SCREENSIZE.width-(phoneView.origin.x+phoneView.width+32),phoneView.height/3)];
        dpNameLabel.font=[UIFont systemFontOfSize:16];
        dpNameLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
        dpNameLabel.text=[NSString stringWithFormat:@"店铺名：%@",[AppUtils getValueWithKey:Business_Name]];
        [self addSubview:dpNameLabel];
        
        //
        yyeLabel=[[UILabel alloc] initWithFrame:CGRectMake(dpNameLabel.origin.x, dpNameLabel.origin.y+dpNameLabel.height, dpNameLabel.width, dpNameLabel.height)];
        yyeLabel.font=[UIFont systemFontOfSize:16];
        yyeLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
        [self addSubview:yyeLabel];
        
        //
        spSumLabel=[[UILabel alloc] initWithFrame:CGRectMake(yyeLabel.origin.x, yyeLabel.origin.y+yyeLabel.height, yyeLabel.width, yyeLabel.height)];
        spSumLabel.font=[UIFont systemFontOfSize:16];
        spSumLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
        [self addSubview:spSumLabel];
        
        [self requestParamData];
    }
    return self;
}

-(void)unDatePhone{
    [phoneView setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
}

-(void)updateUserInfoNoti{
    if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
        phoneView.image=[UIImage imageNamed:@"user_default"];
    }else{
        [phoneView setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
    }
    dpNameLabel.text=[NSString stringWithFormat:@"店铺名：%@",[AppUtils getValueWithKey:Business_Name]];
    [self requestParamData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)requestParamData{
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"refresh_user",
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@user.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"usercenter_ending_reflush" object:nil];
        NSLog(@"%@",responseObject);
        yyeLabel.text=[NSString stringWithFormat:@"营业额：%@",responseObject[@"retData"][@"amount"]];
        spSumLabel.text=[NSString stringWithFormat:@"总商品数量：%@",responseObject[@"retData"][@"goods_count"]];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
