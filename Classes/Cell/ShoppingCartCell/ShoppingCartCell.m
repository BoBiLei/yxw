//
//  ShoppingCartCell.m
//  CatShopping
//
//  Created by mac on 15/9/17.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "ShoppingCartCell.h"
#import "Macro2.h"
@implementation ShoppingCartCell

- (void)awakeFromNib {
    self.joinCollectBtn.layer.cornerRadius=2;
    self.deleteBtn.layer.cornerRadius=2;
    self.joinCollectBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    self.titleLabel.textColor=[UIColor colorWithHexString:@"#282828"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(ShoppingCartModel *)model{
    self.model=model;
    self.goodId=model.goodsId;
    self.imgStr=model.goodsImgStr;
    
    NSString *imgUrlStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImgStr];
    [self.imgv setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholderImage:[UIImage imageNamed:@"placeholder_100x100"]];
    self.titleLabel.text=model.goodsTitle;
    
    NSRange rangeStr = [model.goodsPrice rangeOfString:@"元"];
    if(rangeStr.location!=NSNotFound){
        self.price.text=[NSString stringWithFormat:@"价格：%@",model.goodsPrice];
    }else{
        self.price.text=[NSString stringWithFormat:@"价格：%@元",model.goodsPrice];
    }
    
    if (model.selectState){
        _selectState = YES;
        [_seleteImg setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
    }else{
        _selectState = NO;
        [_seleteImg setImage:[UIImage imageNamed:@"allBtn_nor"] forState:UIControlStateNormal];
    }
}


- (IBAction)clickSelectedBtn:(id)sender {
    
    //获取当前的indexPath
    UITableView *tableView = (UITableView *)self.superview.superview;

    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.delegate selectedGoodCellForModel:self.model indexPath:indexPath];
}

- (IBAction)clickJoinCollectBtn:(id)sender {
    ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
    model.goodsId=self.goodId;
    model.goodsImgStr=self.imgStr;
    model.goodsTitle=self.titleLabel.text;
    model.goodsPrice=self.price.text;
    [self.delegate joinCollectionForModel:model];
}
- (IBAction)clickDeleteBtn:(id)sender {
    ShoppingCartModel *scModel=[[ShoppingCartModel alloc]init];
    scModel.goodsId=self.goodId;
    scModel.goodsImgStr=self.imgStr;
    scModel.goodsTitle=self.titleLabel.text;
    scModel.goodsPrice=self.price.text;
    
    [self.delegate deleteGoodForModel:scModel];
}


@end
