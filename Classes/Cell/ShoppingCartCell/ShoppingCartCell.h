//
//  ShoppingCartCell.h
//  CatShopping
//
//  Created by mac on 15/9/17.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartModel.h"

@protocol ShoppingCartDelegate <NSObject>

@optional

/**
 点击赛选按钮
 */
-(void)selectedGoodCellForModel:(ShoppingCartModel *)model indexPath:(NSIndexPath *)indexPath;
/**
 加入收藏
 */
-(void)joinCollectionForModel:(ShoppingCartModel *)model;
/**
 删除购物车
 */
-(void)deleteGoodForModel:(ShoppingCartModel *)model;

@end

@interface ShoppingCartCell : UITableViewCell

@property (copy, nonatomic) NSString *goodId;
@property (copy, nonatomic) NSString *colleId;
@property (copy, nonatomic) NSString *imgStr;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *joinCollectBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *seleteImg;
@property(assign,nonatomic)BOOL selectState;//选中状态

@property (nonatomic,assign) ShoppingCartModel *model;
@property (weak, nonatomic) id<ShoppingCartDelegate> delegate;

-(void)reflushDataForModel:(ShoppingCartModel *)model;

@end
