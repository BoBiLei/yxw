//
//  HomePageGoodCell.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageGoodModel.h"

//--------------
//homepagegood delegate
//--------------
@protocol HomePageGoodDelegate <NSObject>

-(void)goDetailPageWithId:(NSString *)modelId;

@end

@interface HomePageGoodCell : UITableViewCell

@property (nonatomic,copy) NSString *imgBigId;
@property (nonatomic,strong) UIImageView *imgBig;

@property (nonatomic,copy) NSString *img01Id;
@property (nonatomic,strong) UIImageView *img01;

@property (nonatomic,copy) NSString *img02Id;
@property (nonatomic,strong) UIImageView *img02;
@property id<HomePageGoodDelegate> homepageGoodDelegate;

-(void)setUpUIWithIndexPath:(NSIndexPath *)indexPath;
-(void)reflushModel:(HomePageGoodModel *)model;

@end
