//
//  HomePageGoodCell.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "HomePageGoodCell.h"
#import "Macro2.h"
@implementation HomePageGoodCell

- (void)awakeFromNib {
    
}

-(void)setUpUIWithIndexPath:(NSIndexPath *)indexPath{
    
    _imgBig=[UIImageView new];
    _imgBig.contentMode=UIViewContentModeScaleToFill;
    [_imgBig setTag:0];
    [self addSubview:_imgBig];
    _imgBig.translatesAutoresizingMaskIntoConstraints = NO;
    
    _img01=[UIImageView new];
    _img01.contentMode=UIViewContentModeScaleToFill;
    [_img01 setTag:1];
    _img01.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_img01];
    
    _img02=[UIImageView new];
    _img02.contentMode=UIViewContentModeScaleToFill;
    [_img02 setTag:2];
    _img02.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_img02];
    
    UITapGestureRecognizer *tapBigImg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBigImg)];
    _imgBig.userInteractionEnabled=YES;
    [_imgBig addGestureRecognizer:tapBigImg];
    
    UITapGestureRecognizer *tapSmallImg01=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSmallImg01)]
    ;
    _img01.userInteractionEnabled=YES;
    [_img01 addGestureRecognizer:tapSmallImg01];
    
    UITapGestureRecognizer *tapSmallImg02=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSmallImg02)];
    _img02.userInteractionEnabled=YES;
    [_img02 addGestureRecognizer:tapSmallImg02];
    
    UIView *vLine=[[UIView alloc]init];
    vLine.translatesAutoresizingMaskIntoConstraints = NO;
    vLine.backgroundColor=[UIColor colorWithHexString:@"#f5f5f4"];
    [self addSubview:vLine];
    
    UIView *hLine=[[UIView alloc]init];
    hLine.translatesAutoresizingMaskIntoConstraints = NO;
    hLine.backgroundColor=[UIColor colorWithHexString:@"#f5f5f4"];
    [self addSubview:hLine];
    
    if (indexPath.section%2==0) {
        //1
        NSArray* imgBig_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[_imgBig(165)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgBig)];
        [NSLayoutConstraint activateConstraints:imgBig_h];
        
        NSArray* imgBig_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_imgBig]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgBig)];
        [NSLayoutConstraint activateConstraints:imgBig_w];
        
        //vline
        NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_imgBig]-8-[vLine(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgBig,vLine)];
        [NSLayoutConstraint activateConstraints:line_h];
        
        NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[vLine]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine)];
        [NSLayoutConstraint activateConstraints:line_w];
        
        //2
        NSArray* img01_h = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"H:[vLine]-8-[_img01]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine,_img01)];
        [NSLayoutConstraint activateConstraints:img01_h];
        
        NSArray* img01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_img01(78)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01)];
        [NSLayoutConstraint activateConstraints:img01_w];
        
        //hline
        NSArray* hline_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[vLine][hLine]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine,hLine)];
        [NSLayoutConstraint activateConstraints:hline_h];
        
        NSArray* hline_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_img01][hLine(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01,hLine)];
        [NSLayoutConstraint activateConstraints:hline_w];
        
        //3
        NSArray* img02_h = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"H:[vLine]-8-[_img02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine,_img02)];
        [NSLayoutConstraint activateConstraints:img02_h];
        
        NSArray* img02_w = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"V:[hLine][_img02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(hLine,_img02)];
        [NSLayoutConstraint activateConstraints:img02_w];
        
    }else{
        //1
        NSArray* img01_h = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"H:|-8-[_img01(156)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01)];
        [NSLayoutConstraint activateConstraints:img01_h];
        
        NSArray* img01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_img01(78)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01)];
        [NSLayoutConstraint activateConstraints:img01_w];
        
        //hline
        NSArray* hline_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[hLine(_img01)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(hLine,_img01)];
        [NSLayoutConstraint activateConstraints:hline_h];
        
        NSArray* hline_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_img01][hLine(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01,hLine)];
        [NSLayoutConstraint activateConstraints:hline_w];
        
        //2
        NSArray* img02_h = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"H:|-8-[_img02(156)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img02)];
        [NSLayoutConstraint activateConstraints:img02_h];
        
        NSArray* img02_w = [NSLayoutConstraint constraintsWithVisualFormat:
                            @"V:[_img01][_img02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01,_img02)];
        [NSLayoutConstraint activateConstraints:img02_w];
        
        //vline
        NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_img01][vLine(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_img01,vLine)];
        [NSLayoutConstraint activateConstraints:line_h];
        
        NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[vLine]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine)];
        [NSLayoutConstraint activateConstraints:line_w];
        
        //3
        NSArray* imgBig_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[vLine]-8-[_imgBig]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vLine,_imgBig)];
        [NSLayoutConstraint activateConstraints:imgBig_h];
        
        NSArray* imgBig_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_imgBig]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgBig)];
        [NSLayoutConstraint activateConstraints:imgBig_w];
    }
}

-(void)reflushModel:(HomePageGoodModel *)model{
    //取得图片
    NSString *bigImgUrl=[NSString stringWithFormat:@"%@%@",ImageHost,model.imgStr_bigImg];
    NSString *smallImgUrl01=[NSString stringWithFormat:@"%@%@",ImageHost,model.imgStr_smallImg01];
    NSString *smallImgUrl02=[NSString stringWithFormat:@"%@%@",ImageHost,model.imgStr_smallImg02];
    
    [self.imgBig setImageWithURL:[NSURL URLWithString:bigImgUrl] placeholderImage:[UIImage imageNamed:@"placeholder_160x180"]];
    [self.img01 setImageWithURL:[NSURL URLWithString:smallImgUrl01] placeholderImage:[UIImage imageNamed:@"placeholder_180x80"]];
    [self.img02 setImageWithURL:[NSURL URLWithString:smallImgUrl02] placeholderImage:[UIImage imageNamed:@"placeholder_180x80"]];
    
    //取得id
    self.imgBigId=model.idStr_bigImg;
    self.img01Id=model.idStr_smallImg01;
    self.img02Id=model.idStr_smallImg02;
}

-(void)tapBigImg{
    [self.homepageGoodDelegate goDetailPageWithId:self.imgBigId];
}

-(void)tapSmallImg01{
    [self.homepageGoodDelegate goDetailPageWithId:self.img01Id];
}

-(void)tapSmallImg02{
    [self.homepageGoodDelegate goDetailPageWithId:self.img02Id];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
