//
//  SellerWaterFlowLayout.h
//  MMG
//
//  Created by mac on 15/11/6.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerWaterFlowLayout : UICollectionViewFlowLayout

// 总列数
@property (nonatomic, assign) NSInteger columnCount;

// 商品数据数组
@property (nonatomic, strong) NSMutableArray *goodsList;

@end
