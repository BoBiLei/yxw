//
//  SellerDetailUserImgCell.h
//  MMG
//
//  Created by mac on 15/11/6.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SellerDetailUserImgModel.h"

@interface SellerDetailUserImgCell : UITableViewCell

@property (nonatomic,strong) UIImageView *imgv;

-(void)setCellFrameForImgUrl:(SellerDetailUserImgModel *)model;

@end
