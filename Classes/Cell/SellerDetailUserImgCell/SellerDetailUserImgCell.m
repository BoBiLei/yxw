//
//  SellerDetailUserImgCell.m
//  MMG
//
//  Created by mac on 15/11/6.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SellerDetailUserImgCell.h"
#import "Macro2.h"
@implementation SellerDetailUserImgCell

- (void)awakeFromNib {
    
}

-(void)setCellFrameForImgUrl:(SellerDetailUserImgModel *)model{
    //获取当前cell frame
    CGRect frame = [self frame];
    
    CGFloat imgWFloat=model.w;
    CGFloat imgHFloat=model.h;
    CGFloat imgH=(SCREENSIZE.width*imgHFloat)/imgWFloat;
    
    _imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, imgH)];
    _imgv.contentMode=UIViewContentModeScaleAspectFit;
    NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.imgStr];
    [_imgv setImageWithURL:[NSURL URLWithString:imgStr]];
    [self addSubview:_imgv];
    
    CGFloat cellHeight=0.0f;
    
    cellHeight=imgH;
    
    frame.size.height =cellHeight;
    
    //重新设置cell height
    self.frame = frame;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
