//
//  SellerfashionCell.m
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import "SellerfashionCell.h"
#import "SellerFashionModel.h"
#import "UIImageView+WebCache.h"
#import "Macro2.h"
@interface SellerfashionCell ()
@property (weak, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) UILabel *priceLabel;
@end

@implementation SellerfashionCell

- (void)awakeFromNib {
    [self commitInit];
}

- (void)commitInit{
    UIImageView *imageView = [[UIImageView alloc] init];
    [self addSubview:imageView];
    self.imageView = imageView;
    
    UILabel *priceLabel = [[UILabel alloc] init];
    priceLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25];//设置背景透明色
    priceLabel.textAlignment = NSTextAlignmentCenter;
    priceLabel.textColor = [UIColor whiteColor];
    [self addSubview:priceLabel];
    self.priceLabel = priceLabel;
}

-(void)reflushDataForModel:(SellerFashionModel *)model{
    self.imageView.frame=CGRectMake(0, 0, 200, model.h);
    self.priceLabel.frame=CGRectMake(0, model.h-60, 200, 41);
    if ([model.title isEqualToString:@""]) {
        self.priceLabel.backgroundColor=[UIColor clearColor];
    }
    self.priceLabel.text = model.title;
    NSString *urlStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.img];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"loading"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
}

@end
