//
//  SellerfashionCell.h
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SellerFashionModel;
@interface SellerfashionCell : UICollectionViewCell

-(void)reflushDataForModel:(SellerFashionModel *)model;
@end
