//
//  DeliveryAddressCell.h
//  CatShopping
//
//  Created by mac on 15/9/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiverModel.h"


@interface DeliveryAddressCell : UITableViewCell

@property (strong,nonatomic) NSString *receiverId;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *receiverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiverPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiverAddressLabel;

-(void)reflushModeDataForModel:(ReceiverModel *)model;

@end
