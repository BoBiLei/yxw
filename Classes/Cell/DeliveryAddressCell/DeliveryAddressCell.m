//
//  DeliveryAddressCell.m
//  CatShopping
//
//  Created by mac on 15/9/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "DeliveryAddressCell.h"

@implementation DeliveryAddressCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushModeDataForModel:(ReceiverModel *)model{
    self.receiverId=model.receiverId;
    self.receiverNameLabel.text=model.receiverName;
    self.receiverPhoneLabel.text=model.phoneNum;
    self.receiverAddressLabel.text=model.address;
}

@end
