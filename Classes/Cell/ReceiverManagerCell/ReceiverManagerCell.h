//
//  ReceiverManagerCell.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiverModel.h"

@interface ReceiverManagerCell : UITableViewCell

-(void)reflushDataForModel:(ReceiverModel *)model;

@end
