//
//  CategoryCell.m
//  MMG
//
//  Created by mac on 15/10/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CategoryCell.h"
#import "Macro2.h"
@implementation CategoryCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)reflushDataForModel:(CategoryRightModel *)model{
    NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost01,model.catePic];
    [self.imgv setImageWithURL:[NSURL URLWithString:imgStr]];
    self.title.text=model.cateName;
}

@end
