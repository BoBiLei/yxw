//
//  UploadXybgCell.h
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UploadXybgDelegate <NSObject>

/**
 *  点击上传信用报告
 */
-(void)clickUploadButton;

@end

@interface UploadXybgCell : UITableViewCell

@property (nonatomic,weak) id<UploadXybgDelegate> delegate;

@property (nonatomic,copy) NSString *status;

@end
