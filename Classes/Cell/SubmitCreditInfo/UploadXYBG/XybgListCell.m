//
//  XybgListCell.m
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "XybgListCell.h"

@implementation XybgListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickScan:(id)sender {
    [self.delegate scanBaogaoButton:self.lModel];
}

- (IBAction)clickDelete:(id)sender {
    [self.delegate deleteBaogaoButton:self.lModel sender:sender];
}

-(void)reflushModel:(XybgListModel *)model validateStatus:(NSString *)status{
    self.titleName.text=model.bgTitle;
    [self.imgv setImageWithURL:[NSURL URLWithString:model.bgImgStr]];
    
    //=2审核中 =3验证通过，（隐藏保存信息按钮）
    if ([status isEqualToString:@"2"]) {
        _scanBtn.hidden=YES;
        _deleteBtn.enabled=NO;
        [_deleteBtn setTitle:@"审核中" forState:UIControlStateNormal];
    }
    
    if ([status isEqualToString:@"3"]) {
        _scanBtn.hidden=YES;
        _deleteBtn.enabled=NO;
        [_deleteBtn setTitle:@"审核通过" forState:UIControlStateNormal];
    }
}

@end
