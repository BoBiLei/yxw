//
//  XybgListCell.h
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XybgListModel.h"

@protocol XybgManagerDelegate <NSObject>

/**
 *  查看
 */
-(void)scanBaogaoButton:(XybgListModel *)model;

/**
 *  删除
 */
-(void)deleteBaogaoButton:(XybgListModel *)model sender:(id)sender;

@end

@interface XybgListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@property (weak, nonatomic) IBOutlet UILabel *titleName;

@property (nonatomic,retain) XybgListModel *lModel;

@property (nonatomic,weak) id<XybgManagerDelegate> delegate;

-(void)reflushModel:(XybgListModel *)model validateStatus:(NSString *)status;

@end
