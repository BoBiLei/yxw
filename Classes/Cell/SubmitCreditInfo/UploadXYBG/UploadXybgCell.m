//
//  UploadXybgCell.m
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "UploadXybgCell.h"

@implementation UploadXybgCell{
    UIButton *uploadBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpUI];
}

-(void)setUpUI{
    //title
    UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(12, 8, SCREENSIZE.width-24, 40)];
    [self addSubview:title];
    title.font=[UIFont systemFontOfSize:16];
    title.textColor=[UIColor colorWithHexString:@"#282828"];
    title.numberOfLines=0;
    NSString *titleStr=@"请上传本人更多资料（如房产证、银行流水、人行征信报告等），获取更高分期额度。";
    title.text = titleStr;
    
    //上传按钮
    uploadBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    uploadBtn.frame=CGRectMake(12, title.origin.y+title.height+8, (SCREENSIZE.width-48)/3, 38);
    uploadBtn.layer.cornerRadius=3;
    [uploadBtn setTitle:@"上传文件" forState:UIControlStateNormal];
    [uploadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    uploadBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    uploadBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [uploadBtn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:uploadBtn];
    
    //提示
    UILabel *upLoadTip01=[[UILabel alloc]initWithFrame:CGRectMake(12, uploadBtn.origin.y+uploadBtn.height+8, SCREENSIZE.width-24, 26)];
    upLoadTip01.text=@"注：JPG、PNG格式图片，大小不超过2M";
    upLoadTip01.font=[UIFont systemFontOfSize:14];
    upLoadTip01.textColor=[UIColor redColor];
    upLoadTip01.textAlignment=NSTextAlignmentLeft;
    upLoadTip01.numberOfLines=0;
    [self addSubview:upLoadTip01];
    
    CGRect frame=self.frame;
    frame.size.height=upLoadTip01.origin.y+upLoadTip01.height+8;
    self.frame=frame;
}

-(void)setStatus:(NSString *)status{
    if ([status isEqualToString:@"2"]||[status isEqualToString:@"3"]) {
        uploadBtn.enabled=NO;
        uploadBtn.backgroundColor=[UIColor lightGrayColor];
    }else{
        
    }
}

-(void)clickBtn{
    [self.delegate clickUploadButton];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
