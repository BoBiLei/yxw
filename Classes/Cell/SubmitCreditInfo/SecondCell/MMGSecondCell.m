//
//  MMGSecondCell.m
//  youxia
//
//  Created by mac on 16/1/4.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MMGSecondCell.h"
#import <UIButton+WebCache.h>

#define textBGColor @"#fafafa"
#define YxColor_Blue @"#e56b02"

@implementation MMGSecondCell{
    
    int ReSetTime;
    int timerCount;
    
    UIView *phoneView;
    UITextField *phoneTf;
    UIView *passView;
    UILabel *passLabel;
    UITextField *passTf;
    UILabel *validLeftLabel;
    UIView *validView;          //验证码（短信或图片）
    UITextField *validTf;
    
    UIButton *reGetValidBtn;    //重新获取验证码按钮
    
    UIButton *saveBtn;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateValida:) name:@"GetCodeNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAllStatusNoti:) name:@"UpdateAllStatusNotification" object:nil];
        
        //手机号码
        UILabel *phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 12, 88, 34)];;
        phoneLabel.text=@"手机号码:";
        phoneLabel.font=[UIFont systemFontOfSize:16];
        phoneLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        phoneLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:phoneLabel];
        
        phoneView=[[UIView alloc]initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, phoneLabel.origin.y, SCREENSIZE.width-(phoneLabel.origin.x+phoneLabel.width+32), phoneLabel.height)];
        phoneView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        phoneView.layer.borderWidth=0.7f;
        phoneView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        phoneView.layer.cornerRadius=3;
        [self addSubview:phoneView];
        
        phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, phoneView.width-10, phoneView.height)];
        phoneTf.placeholder=@"请输入手机号码";
        phoneTf.font=[UIFont systemFontOfSize:15];
        phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
        phoneTf.keyboardType=UIKeyboardTypePhonePad;
        [phoneView addSubview:phoneTf];
        
        //查询密码
        passLabel=[[UILabel alloc]initWithFrame:CGRectMake(phoneLabel.origin.x, phoneLabel.origin.y+phoneLabel.height+12, phoneLabel.width, phoneLabel.height)];
        passLabel.text=@"服务密码:";
        passLabel.font=[UIFont systemFontOfSize:16];
        passLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        passLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:passLabel];
        
        passView=[[UIView alloc]initWithFrame:CGRectMake(passLabel.origin.x+passLabel.width+8, phoneView.origin.y+phoneView.height+12, SCREENSIZE.width-(passLabel.origin.x+passLabel.width+32), passLabel.height)];
        passView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        passView.layer.borderWidth=0.7f;
        passView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        passView.layer.cornerRadius=3;
        [self addSubview:passView];
        
        passTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, passView.width-10, passView.height)];
        passTf.placeholder=@"请输入服务密码";
        passTf.font=[UIFont systemFontOfSize:15];
        passTf.secureTextEntry=YES;
        passTf.clearButtonMode=UITextFieldViewModeWhileEditing;
        [passView addSubview:passTf];
        
        //验证码
        validLeftLabel=[[UILabel alloc]initWithFrame:CGRectMake(phoneLabel.origin.x, passLabel.origin.y+passLabel.height+12, passLabel.width, passLabel.height)];
        validLeftLabel.text=@"登录验证码:";
        validLeftLabel.font=[UIFont systemFontOfSize:16];
        validLeftLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        validLeftLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:validLeftLabel];
        
        validView=[[UIView alloc]initWithFrame:CGRectMake(validLeftLabel.origin.x+validLeftLabel.width+8, validLeftLabel.origin.y, passView.width, validLeftLabel.height)];
        validView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        validView.layer.borderWidth=0.7f;
        validView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        validView.layer.cornerRadius=3;
        [self addSubview:validView];
        
        validTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, validView.width-90, validView.height)];
        validTf.font=[UIFont systemFontOfSize:15];
        [validView addSubview:validTf];
        
        ReSetTime=60;
        timerCount = ReSetTime;
        reGetValidBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        reGetValidBtn.frame=CGRectMake(validView.width-82,2, 80, validView.height-4);
        reGetValidBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        [reGetValidBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [reGetValidBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        reGetValidBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        reGetValidBtn.layer.cornerRadius=3;
        [reGetValidBtn addTarget:self action:@selector(clickReGetValidBtn) forControlEvents:UIControlEventTouchUpInside];
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        [validView addSubview:reGetValidBtn];
        
        
        //saveBtn
        saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        saveBtn.tag=99;
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        saveBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        saveBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        saveBtn.layer.cornerRadius=3;
        [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:saveBtn];
        
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }
    return self;
}

#pragma mark - 点击重新获取按钮
-(void)clickReGetValidBtn{
    [AppUtils closeKeyboard];
    ReSetTime=60;
    timerCount = ReSetTime;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
    [self.delegate reGetValideCode:phoneTf.text];
}

-(void)setTimer:(NSTimer *)timer{
    if (timerCount<0){
        [timer invalidate];
        [reGetValidBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        reGetValidBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        reGetValidBtn.enabled=YES;
        timerCount = ReSetTime;
        return;
    }else{
        reGetValidBtn.backgroundColor=[UIColor lightGrayColor];
        reGetValidBtn.enabled=NO;
        [reGetValidBtn setTitle:[NSString stringWithFormat:@"%d秒后获取",timerCount] forState:UIControlStateNormal];
    }
    timerCount--;
}

#pragma mark 点击保存按钮
-(void)clickSaveBtn{
    //tag=100 手机号、查询密码
    //tag=200 手机号、查询密码、图形码
    [AppUtils closeKeyboard];
    if (saveBtn.tag==99) {
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPass:phoneTf.text password:passTf.text];
        }
    }else if (saveBtn.tag==100){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入图片验证码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPassAndPicture:phoneTf.text password:passTf.text picture:validTf.text];
        }
    }else if (saveBtn.tag==200){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入短信验证码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPassAndPicture:phoneTf.text password:passTf.text picture:validTf.text];
            //验证短信
            //[self.delegate submitPhoneAndDuanxin:phoneTf.text duanxin:passTf.text];
        }
    }else if (saveBtn.tag==999){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入短信验证码" inView:self.window];
        }else{
            //验证短信
            [self.delegate submitPhoneAndDuanxin:phoneTf.text duanxin:validTf.text];
        }
    }
}

#pragma mark - NSNotification
-(void)upDateValida:(NSNotification *)noti{
    NSString *str=noti.object;
    ReSetTime=str.intValue;
    timerCount=ReSetTime;
}

-(void)updateAllStatusNoti:(NSNotification *)noti{
    NSArray *arr=noti.object;
    [self reflushDataForArray:arr];
}

#pragma mark - 生成/刷新UI或数据
-(void)reflushDataForArray:(NSArray *)array{
    /*phone_status
     0   未通过
     1   通过
     100 需要图片验证码
     200 短信验证码
     999 第二个短信验证码
     */
    NSString *rcphoneStr=array[0];
    NSString *rcStatusStr=array[1];
    NSString *imgStr=array[3];
    NSString *validTime=array[5];
    ReSetTime=validTime.intValue;
    
    phoneTf.text=rcphoneStr;
    if ([rcStatusStr isEqualToString:@""]
        ||[rcStatusStr isEqualToString:@"0"]) {
        if ([array[4] isEqualToString:@"电信"]) {
            passTf.text=array[5];
        }
        
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        
        //
        validLeftLabel.text=@"";
        validLeftLabel.hidden=YES;
        validView.hidden=YES;
        
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
        
    }else if([rcStatusStr isEqualToString:@"1"]){
        if ([array[4] isEqualToString:@"电信"]) {
            passTf.text=array[5];
        }
        
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        
        //
        phoneTf.enabled=NO;
        phoneTf.textColor=[UIColor grayColor];
        phoneTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        
        passLabel.hidden=YES;
        passView.hidden=YES;
        
        validLeftLabel.text=@"";
        validLeftLabel.hidden=YES;
        validView.hidden=YES;
        
        saveBtn.hidden=YES;
        
        CGRect frame=self.frame;
        frame.size.height=phoneView.origin.y+phoneView.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"100"]){
        
        saveBtn.tag=100;
        
        //
        NSString *recPass=array[2];
        passTf.text=recPass;
        passTf.secureTextEntry=YES;
        passLabel.hidden=NO;
        passView.hidden=NO;
        
        reGetValidBtn.hidden=NO;
        reGetValidBtn.enabled=NO;
        [reGetValidBtn sd_setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal];
        
        //
        validLeftLabel.text=@"图片验证码";
        validLeftLabel.hidden=NO;
        validView.hidden=NO;
        validTf.placeholder=@"请输入图片验证码";
        
        //
        saveBtn.hidden=NO;
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"200"]){
        
        saveBtn.tag=200;
        
        //
        NSString *recPass=array[2];
        passTf.text=recPass;
        passTf.secureTextEntry=YES;
        reGetValidBtn.enabled=NO;
        passLabel.hidden=NO;
        passView.hidden=NO;
        
        reGetValidBtn.hidden=YES;
        
        //
        validLeftLabel.text=@"短信验证码";
        validLeftLabel.hidden=NO;
        validView.hidden=NO;
        validTf.placeholder=@"请输入短信验证码";
        
        //
        saveBtn.hidden=NO;
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"999"]){
        timerCount = ReSetTime;
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        
        saveBtn.tag=999;
        
        passLabel.hidden=YES;
        passView.hidden=YES;
        
        reGetValidBtn.hidden=NO;
        
        //
        validLeftLabel.frame=passLabel.frame;
        validView.frame=passView.frame;
        validLeftLabel.text=@"短信验证码";
        validTf.placeholder=@"再次输入短信验证码";
        
        //
        saveBtn.hidden=NO;
        [saveBtn setTitle:@"提交验证" forState:UIControlStateNormal];
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
