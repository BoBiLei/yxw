//
//  OrderPayCell.h
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderPayCellDelegate <NSObject>

@optional

-(void)tapOrderNumLabel:(NSString *)orderNum;

@end

@interface OrderPayCell : UITableViewCell

@property (copy, nonatomic) NSString *orderId;
@property (strong, nonatomic)  TTTAttributedLabel *orderNumLabel;//订单编号
@property (strong, nonatomic)  TTTAttributedLabel *totalPriceLabel;//总计金额
@property (strong, nonatomic)  TTTAttributedLabel *couponLabel;//已优惠
@property (strong, nonatomic)  TTTAttributedLabel *poundageFee;//手续费
@property (strong, nonatomic)  TTTAttributedLabel *shuiFee;     //税费
@property (strong, nonatomic)  TTTAttributedLabel *orderPriceLabel;//应付金额
@property (weak, nonatomic) id<OrderPayCellDelegate> delegate;
-(void)reflushDataForDictionary:(NSDictionary *)dic;

@end
