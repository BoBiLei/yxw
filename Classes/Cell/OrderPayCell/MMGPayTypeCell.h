//
//  MMGPayTypeCell.h
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMGPayTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *_payTypeImgv;
@property (weak, nonatomic) IBOutlet UIImageView *selePayTypeImgv;
@property (weak, nonatomic) IBOutlet UILabel *_payTypeName;

@end
