//
//  MMGPayTypeCell.m
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGPayTypeCell.h"

@implementation MMGPayTypeCell

- (void)awakeFromNib {
    self._payTypeName.textColor=[UIColor colorWithHexString:@"#282828"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
