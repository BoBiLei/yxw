//
//  OrderPayCell.m
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderPayCell.h"

@implementation OrderPayCell{
    UILabel *tipLabel;
    UILabel *btmTip01;
    UILabel *btmTip02;
}

- (void)awakeFromNib {
    
    //
    tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 8, self.frame.size.width, 18)];
    tipLabel.text=@"订单提交成功,请您尽快付款！";
    tipLabel.font=[UIFont systemFontOfSize:15];
    [self addSubview:tipLabel];
    
    //
    _orderNumLabel=[TTTAttributedLabel new];
    _orderNumLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_orderNumLabel];
    
    //
    _totalPriceLabel=[TTTAttributedLabel new];
    _totalPriceLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_totalPriceLabel];
    
    //
    _couponLabel=[TTTAttributedLabel new];
    _couponLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_couponLabel];
    
    //
    _poundageFee=[TTTAttributedLabel new];
    _poundageFee.font = [UIFont systemFontOfSize:14];
    [self addSubview:_poundageFee];
    
    //
    _shuiFee=[TTTAttributedLabel new];
    _shuiFee.font = [UIFont systemFontOfSize:14];
    [self addSubview:_shuiFee];
    
    //
    _orderPriceLabel=[TTTAttributedLabel new];
    _orderPriceLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_orderPriceLabel];
    
    //
    btmTip01=[UILabel new];
    btmTip01.font=[UIFont systemFontOfSize:15];
    btmTip01.text=@"提示";
    [self addSubview:btmTip01];
    
    btmTip02=[UILabel new];
    btmTip02.font=[UIFont systemFontOfSize:14];
    btmTip02.text=@"请您在1小时之内完成支付，否则订单会被自动取消";
    [self addSubview:btmTip02];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForDictionary:(NSDictionary *)dic{
    self.orderId=dic[@"order_id"];
    self.orderNumLabel.userInteractionEnabled=YES;
    self.orderNumLabel.text=[NSString stringWithFormat:@"订单编号: %@",dic[@"order_sn"]];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOrderNumLabel)];
    [_orderNumLabel addGestureRecognizer:tap];
    
    
    NSString *totStr=dic[@"goods_amount"];
    NSString *couponId=dic[@"bonus_id"];
    NSString *stageSum=dic[@"fenqi"];
    NSString *perStr=dic[@"per_amount"];
    
    //
    NSString *totalPriceStr=[NSString stringWithFormat:@"总计金额: %@元(分%@期,每期应付%@元)",totStr,stageSum,perStr];
    [self.totalPriceLabel setText:totalPriceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"(分%@期,每期应付%@元)",stageSum,perStr] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    
    //
    NSString *stageFeeStr=[NSString stringWithFormat:@"分期手续费: %@元",dic[@"fenqi_fee"]];
    [self.poundageFee setText:stageFeeStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",dic[@"fenqi_fee"]] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    
    //税费
    NSString *sfStr=dic[@"shuiwu"];
    if (sfStr.floatValue>=9000) {
        sfStr=@"9000";
    }
    NSString *shuiFeeStr=[NSString stringWithFormat:@"税费: %@元",sfStr];
    [self.shuiFee setText:shuiFeeStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",sfStr] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    
    //
    NSString *couponStr=[NSString stringWithFormat:@"已优惠: %@元",dic[@"bonus"]];
    [self.couponLabel setText:couponStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",dic[@"bonus"]] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    
    //
    NSString *orderPriceStr=[NSString stringWithFormat:@"应付金额: %@元(含全部手续费、税费)",dic[@"order_amount"]];
    [self.orderPriceLabel setText:orderPriceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",dic[@"order_amount"]] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    
    
    CGRect newFrame=self.frame;
    //判断是否选择有优惠券
    if ([couponId isEqualToString:@"0"]) {
        _orderNumLabel.frame=CGRectMake(8, tipLabel.frame.origin.y+tipLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _totalPriceLabel.frame=CGRectMake(8, _orderNumLabel.frame.origin.y+_orderNumLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _couponLabel.frame=CGRectMake(0, 0, 0, 0);
        _poundageFee.frame=CGRectMake(8, _totalPriceLabel.frame.origin.y+_totalPriceLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _shuiFee.frame=CGRectMake(8, _poundageFee.frame.origin.y+_poundageFee.frame.size.height+6, tipLabel.frame.size.width, 18);
        _orderPriceLabel.frame=CGRectMake(8, _shuiFee.frame.origin.y+_shuiFee.frame.size.height+6, tipLabel.frame.size.width, 18);
        btmTip01.frame=CGRectMake(8, _orderPriceLabel.frame.origin.y+_orderPriceLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        
        btmTip02.frame=CGRectMake(8, btmTip01.frame.origin.y+btmTip01.frame.size.height+6, tipLabel.frame.size.width, 18);
        
        newFrame.size.height=btmTip02.frame.origin.y+btmTip02.frame.size.height+8;
    }else{
        _orderNumLabel.frame=CGRectMake(8, tipLabel.frame.origin.y+tipLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _totalPriceLabel.frame=CGRectMake(8, _orderNumLabel.frame.origin.y+_orderNumLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _poundageFee.frame=CGRectMake(8, _totalPriceLabel.frame.origin.y+_totalPriceLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        _shuiFee.frame=CGRectMake(8, _poundageFee.frame.origin.y+_poundageFee.frame.size.height+6, tipLabel.frame.size.width, 18);
        _couponLabel.frame=CGRectMake(8, _shuiFee.frame.origin.y+_shuiFee.frame.size.height+6, tipLabel.frame.size.width, 18);
        _orderPriceLabel.frame=CGRectMake(8, _couponLabel.frame.origin.y+_couponLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        
        btmTip01.frame=CGRectMake(8, _orderPriceLabel.frame.origin.y+_orderPriceLabel.frame.size.height+6, tipLabel.frame.size.width, 18);
        
        btmTip02.frame=CGRectMake(8, btmTip01.frame.origin.y+btmTip01.frame.size.height+6, tipLabel.frame.size.width, 18);
        
        newFrame.size.height=btmTip02.frame.origin.y+btmTip02.frame.size.height+8;
    }
    self.frame=newFrame;
}

-(void)tapOrderNumLabel{
    [self.delegate tapOrderNumLabel:self.orderId];
}

@end
