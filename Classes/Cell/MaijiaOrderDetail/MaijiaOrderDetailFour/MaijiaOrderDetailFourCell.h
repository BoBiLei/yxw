//
//  MaijiaOrderDetailFourCell.h
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SaveMaijiaOrderDetailDelegate <NSObject>

-(void)saveMaijiaOrderDetail;

@end

@interface MaijiaOrderDetailFourCell : UITableViewCell

@property (nonatomic, weak) id<SaveMaijiaOrderDetailDelegate> delegate;

@end
