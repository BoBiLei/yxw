//
//  MaijiaOrderDetailFourCell.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaOrderDetailFourCell.h"

@implementation MaijiaOrderDetailFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIButton *saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        saveBtn.layer.cornerRadius=2;
        saveBtn.frame=CGRectMake(24, 26, SCREENSIZE.width-48, 44);
        saveBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        [saveBtn setTitle:@"保存信息" forState:UIControlStateNormal];
        [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        saveBtn.titleLabel.font=[UIFont systemFontOfSize:17];
        [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:saveBtn];
        
        CGFloat lastYH=saveBtn.origin.y+saveBtn.height*2+14;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lastYH] forKey:@"MaijiaOrderDetailFourCell_Height"];
    }
    return self;
}

-(void)clickSaveBtn{
    [self.delegate saveMaijiaOrderDetail];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
