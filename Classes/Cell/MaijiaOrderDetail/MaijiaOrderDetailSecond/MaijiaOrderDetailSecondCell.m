//
//  MaijiaOrderDetailSecondCell.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaOrderDetailSecondCell.h"
#import "Macro2.h"
@implementation MaijiaOrderDetailSecondCell{
    UILabel *nameLeft;
    UILabel *nameRight;
    UILabel *phoneLeft;
    UILabel *phoneRight;
    UILabel *addressLeft;
    UILabel *addresseRight;
    UILabel *liuyanLeft;
    UILabel *liuyanRight;
    UIImageView *goodsImgv;
    UIButton *payStatus;
    UILabel *mcLabel;
    TTTAttributedLabel *sellPriceLabel;
    TTTAttributedLabel *payPriceLabel;
    UILabel *sfeeLabel;
    UILabel *sfLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#pragma mark 买家名称
        nameLeft=[[UILabel alloc] initWithFrame:CGRectMake(12, 16, 76, 21)];
        nameLeft.font=[UIFont systemFontOfSize:15];
        nameLeft.textColor=[UIColor colorWithHexString:@"#252525"];
        nameLeft.text=@"买家名称：";
        [self addSubview:nameLeft];
        
        nameRight=[[UILabel alloc] initWithFrame:CGRectMake(nameLeft.origin.x+nameLeft.width, nameLeft.origin.y, SCREENSIZE.width-(nameLeft.origin.x+nameLeft.width+12), nameLeft.height)];
        nameRight.font=nameLeft.font;
        nameRight.textColor=nameLeft.textColor;
        [self addSubview:nameRight];
        
#pragma mark 手机号
        phoneLeft=[[UILabel alloc] initWithFrame:CGRectMake(nameLeft.origin.x, nameLeft.origin.y+nameLeft.height+8, 76, 21)];
        phoneLeft.font=[UIFont systemFontOfSize:15];
        phoneLeft.textColor=[UIColor colorWithHexString:@"#252525"];
        phoneLeft.text=@"手机号码：";
        [self addSubview:phoneLeft];
        
        phoneRight=[[UILabel alloc] initWithFrame:CGRectMake(nameRight.origin.x, phoneLeft.origin.y, nameRight.width, phoneLeft.height)];
        phoneRight.font=phoneLeft.font;
        phoneRight.textColor=phoneLeft.textColor;
        [self addSubview:phoneRight];
        
#pragma mark 收货地址
        addressLeft=[[UILabel alloc] initWithFrame:CGRectMake(phoneLeft.origin.x, phoneLeft.origin.y+phoneLeft.height+8, 76, 21)];
        addressLeft.font=[UIFont systemFontOfSize:15];
        addressLeft.textColor=[UIColor colorWithHexString:@"#252525"];
        addressLeft.text=@"收货地址：";
        [self addSubview:addressLeft];
        
        addresseRight=[[UILabel alloc] initWithFrame:CGRectMake(nameRight.origin.x, addressLeft.origin.y, nameRight.width, addressLeft.height)];
        addresseRight.font=addressLeft.font;
        addresseRight.textColor=addressLeft.textColor;
        [self addSubview:addresseRight];
        
#pragma mark 买家留言
        liuyanLeft=[[UILabel alloc] initWithFrame:CGRectMake(phoneLeft.origin.x, addresseRight.origin.y+addresseRight.height+8, 76, 21)];
        liuyanLeft.font=[UIFont systemFontOfSize:15];
        liuyanLeft.textColor=[UIColor colorWithHexString:@"#252525"];
        liuyanLeft.text=@"买家留言：";
        [self addSubview:liuyanLeft];
        
        liuyanRight=[[UILabel alloc] initWithFrame:CGRectMake(nameRight.origin.x, liuyanLeft.origin.y, nameRight.width, liuyanLeft.height)];
        liuyanRight.font=liuyanLeft.font;
        liuyanRight.textColor=liuyanLeft.textColor;
        [self addSubview:liuyanRight];
        
#pragma mark 商品信息
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(liuyanLeft.origin.x, liuyanRight.origin.y+liuyanRight.height+14, SCREENSIZE.width-2*liuyanLeft.origin.x, 200)];
        view.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        view.layer.borderWidth=0.6f;
        view.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        [self addSubview:view];
        
#pragma mark 商品图片
        CGFloat imgHeight=(SCREENSIZE.width-34)/3;
        goodsImgv=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, imgHeight, imgHeight+8)];
        goodsImgv.layer.borderWidth=0.6f;
        goodsImgv.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        [view addSubview:goodsImgv];
        
        payStatus=[UIButton buttonWithType:UIButtonTypeCustom];
        payStatus.frame=CGRectMake(goodsImgv.origin.x, goodsImgv.origin.y+goodsImgv.height+6, goodsImgv.width, 34);
        [payStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        payStatus.titleLabel.font=[UIFont systemFontOfSize:14];
        [view addSubview:payStatus];
        
        CGFloat rightH=(goodsImgv.height+payStatus.height+6)/5;
        CGFloat lineH=0.6f;
        
#pragma mark 名称
        mcLabel=[[UILabel alloc] initWithFrame:CGRectMake(goodsImgv.origin.x+goodsImgv.width+10, goodsImgv.origin.y, view.width-(goodsImgv.origin.x+goodsImgv.width+20), rightH)];
        mcLabel.numberOfLines=0;
        mcLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        mcLabel.font=[UIFont systemFontOfSize:13];
        [view addSubview:mcLabel];
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(mcLabel.origin.x, mcLabel.origin.y+mcLabel.height) endPoint:CGPointMake(view.width-10, mcLabel.origin.y+mcLabel.height)];
        
#pragma mark 寄售价
        sellPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(mcLabel.origin.x, mcLabel.origin.y+mcLabel.height, mcLabel.width, rightH)];
        sellPriceLabel.numberOfLines=0;
        sellPriceLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sellPriceLabel.font=[UIFont systemFontOfSize:13];
        [view addSubview:sellPriceLabel];
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height) endPoint:CGPointMake(view.width-10, sellPriceLabel.origin.y+sellPriceLabel.height)];
        
#pragma mark 待结算
        payPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height, sellPriceLabel.width, rightH)];
        payPriceLabel.numberOfLines=0;
        payPriceLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        payPriceLabel.font=[UIFont systemFontOfSize:13];
        [view addSubview:payPriceLabel];
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(payPriceLabel.origin.x, payPriceLabel.origin.y+payPriceLabel.height) endPoint:CGPointMake(view.width-10, payPriceLabel.origin.y+payPriceLabel.height)];
        
#pragma mark 分期价
        sfeeLabel=[[UILabel alloc] initWithFrame:CGRectMake(payPriceLabel.origin.x, payPriceLabel.origin.y+payPriceLabel.height, payPriceLabel.width, rightH)];
        sfeeLabel.numberOfLines=0;
        sfeeLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sfeeLabel.font=[UIFont systemFontOfSize:13];
        sfeeLabel.adjustsFontSizeToFitWidth=YES;
        [view addSubview:sfeeLabel];
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sfeeLabel.origin.x, sfeeLabel.origin.y+sfeeLabel.height) endPoint:CGPointMake(view.width-10, sfeeLabel.origin.y+sfeeLabel.height)];
        
#pragma mark 首付价
        sfLabel=[[UILabel alloc] initWithFrame:CGRectMake(sfeeLabel.origin.x, sfeeLabel.origin.y+sfeeLabel.height, sfeeLabel.width, rightH)];
        sfLabel.numberOfLines=0;
        sfLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sfLabel.font=[UIFont systemFontOfSize:13];
        sfLabel.adjustsFontSizeToFitWidth=YES;
        [view addSubview:sfLabel];
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sfLabel.origin.x, sfLabel.origin.y+sfLabel.height) endPoint:CGPointMake(view.width-10, sfLabel.origin.y+sfLabel.height)];
        
        CGRect frame=view.frame;
        frame.size.height=payStatus.origin.y+payStatus.height+16;
        view.frame=frame;
        
        CGFloat lastYH=view.origin.y+view.height+20;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lastYH] forKey:@"MaijiaOrderDetailSecondCell_Height"];
    }
    return self;
}

-(void)reflushViewWithUser:(NSString *)user mobile:(NSString *)mobile address:(NSString *)address orderMsg:(NSString *)orderMsg goodsImg:(NSString *)goodsImg js_status:(NSString *)js_status goodsName:(NSString *)goodsName jiesuan_price:(NSString *)price js_price:(NSString *)js_price fenqiFee:(NSString *)fenqiFee fenqi:(NSString *)fenqi sxffee:(NSString *)sxffee shuiFee:(NSString *)shuiFee{
    nameRight.text=user;
    phoneRight.text=mobile;
    addresseRight.text=address;
    liuyanRight.text=[orderMsg isEqualToString:@""]?@"无":orderMsg;
    [goodsImgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,goodsImg]]];
    payStatus.backgroundColor=[UIColor colorWithHexString:[js_status isEqualToString:@"1"]||[js_status isEqualToString:@"3"]?@"#e3403d":@"#363636"];
    [payStatus setTitle:[js_status isEqualToString:@"1"]?@"待结账":[js_status isEqualToString:@"2"]?@"已结算":@"结账中" forState:UIControlStateNormal];
    mcLabel.text=[NSString stringWithFormat:@"名  称：%@",goodsName];
    
    NSString *sellpStr=[NSString stringWithFormat:@"寄售价：%@元",price];
    [sellPriceLabel setText:sellpStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",price] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff0000"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    NSString *ppStr=[NSString stringWithFormat:[js_status isEqualToString:@"1"]?@"待结账：%@元":@"已结算：%@元",js_price];
    [payPriceLabel setText:ppStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",js_price] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff0000"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    sfeeLabel.text=[NSString stringWithFormat:@"分期价：%@元*%@期",fenqiFee,fenqi];
    
    CGFloat sfFloat=fenqiFee.floatValue+sxffee.floatValue+shuiFee.floatValue;
    NSString *sfSr=[NSString stringWithFormat:@"%.2f",sfFloat];
    
    if (shuiFee.floatValue>=9000) {
        shuiFee=@"9000";
    }
    sfLabel.text=[fenqi isEqualToString:@"1"]?[NSString stringWithFormat:@"首付价：%@元\n(税费%@元)",sfSr,shuiFee]:[NSString stringWithFormat:@"首付价：%@元\n(含手续费%@元，税费%@元)",sfSr,sxffee,shuiFee];
}

@end
