//
//  MaijiaOrderDetailSecondCell.h
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaijiaOrderDetailSecondCell : UITableViewCell

-(void)reflushViewWithUser:(NSString *)user mobile:(NSString *)mobile address:(NSString *)address orderMsg:(NSString *)orderMsg goodsImg:(NSString *)goodsImg js_status:(NSString *)js_status goodsName:(NSString *)goodsName jiesuan_price:(NSString *)price js_price:(NSString *)js_price fenqiFee:(NSString *)fenqiFee fenqi:(NSString *)fenqi sxffee:(NSString *)sxffee shuiFee:(NSString *)shuiFee;

@end
