//
//  MaijiaOrderDetailThreeCell.h
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MaijiaOrderDetailStatusDelegate <NSObject>

-(void)clickGoodsStatusView;

@end

@interface MaijiaOrderDetailThreeCell : UITableViewCell

@property (nonatomic, weak) id<MaijiaOrderDetailStatusDelegate> delegate;

@property (nonatomic, copy) void (^kdCompanyBlock)(NSString *tfText);

@property (nonatomic, copy) void (^kdNumberBlock)(NSString *tfText);

@property (nonatomic, copy) void (^kdDongtaiBlock)(NSString *tfText);

-(void)reflushDateForStatus:(NSString *)status kdCompany:(NSString *)kdCompany kdNumber:(NSString *)kdNumber kdDongtai:(NSString *)kdDongtai;

@end
