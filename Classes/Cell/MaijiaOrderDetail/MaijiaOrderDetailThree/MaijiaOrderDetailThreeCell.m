//
//  MaijiaOrderDetailThreeCell.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaOrderDetailThreeCell.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import "ShangjiaMacro.h"
@implementation MaijiaOrderDetailThreeCell{
    UIImageView *statusView;
    UITextField *statusTF;
    UIImageView *companyView;
    UITextField *companyTF;
    UIImageView *numberView;
    UITextField *numberTF;
    UIImageView *dtView;
    UITextField *dtTF;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateGoodStatusLabel:) name:Maijia_OrderDetail_GoodsStatusNoti object:nil];
        
        UILabel *topLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 12, SCREENSIZE.width, 36)];
        topLabel.font=[UIFont systemFontOfSize:16];
        topLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        topLabel.textAlignment=NSTextAlignmentCenter;
        topLabel.text=@"猫猫购寄售平台商品物流动态";
        [self addSubview:topLabel];
        
#pragma mark 商品状态
        UILabel *statusLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, topLabel.origin.y+topLabel.height+12, 76, 38)];
        statusLabel.font=[UIFont systemFontOfSize:15];
        statusLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        statusLabel.text=@"商品状态：";
        [self addSubview:statusLabel];
        
        statusView=[[UIImageView alloc] initWithFrame:CGRectMake(statusLabel.origin.x+statusLabel.width, statusLabel.origin.y, SCREENSIZE.width-(statusLabel.origin.x+statusLabel.width+12), statusLabel.height)];
        statusView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        [self addSubview:statusView];
        statusView.userInteractionEnabled=YES;
        UITapGestureRecognizer *ppTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickStatusView)];
        [statusView addGestureRecognizer:ppTap];
        
        statusTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, statusView.width-8, statusView.height)];
        statusTF.placeholder=@"请选择商品状态";
        statusTF.textColor=statusLabel.textColor;
        statusTF.font=statusLabel.font;
        statusTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        statusTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        statusTF.enabled=NO;
        [statusView addSubview:statusTF];
        
#pragma mark 快递公司
        UILabel *companyLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, statusLabel.origin.y+statusLabel.height+14, 76, 38)];
        companyLabel.font=[UIFont systemFontOfSize:15];
        companyLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        companyLabel.text=@"快递公司：";
        [self addSubview:companyLabel];
        
        companyView=[[UIImageView alloc] initWithFrame:CGRectMake(companyLabel.origin.x+companyLabel.width, companyLabel.origin.y, SCREENSIZE.width-(companyLabel.origin.x+companyLabel.width+12), companyLabel.height)];
        companyView.userInteractionEnabled=YES;
        companyView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        [self addSubview:companyView];
        
        companyTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, companyView.width-8, companyView.height)];
        companyTF.placeholder=@"快递公司需要商家填写";
        companyTF.textColor=companyLabel.textColor;
        companyTF.font=companyLabel.font;
        companyTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        companyTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        [companyTF addTarget:self action:@selector(companyChange:) forControlEvents:UIControlEventEditingChanged];
        [companyView addSubview:companyTF];
        
#pragma mark 快递单号
        UILabel *numberLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, companyLabel.origin.y+companyLabel.height+14, 76, 38)];
        numberLabel.font=[UIFont systemFontOfSize:15];
        numberLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        numberLabel.text=@"快递单号：";
        [self addSubview:numberLabel];
        
        numberView=[[UIImageView alloc] initWithFrame:CGRectMake(numberLabel.origin.x+numberLabel.width, numberLabel.origin.y, SCREENSIZE.width-(numberLabel.origin.x+numberLabel.width+12), numberLabel.height)];
        numberView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        numberView.userInteractionEnabled=YES;
        [self addSubview:numberView];
        
        numberTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, numberView.width-8, numberView.height)];
        numberTF.placeholder=@"快递单号需要商家填写";
        numberTF.textColor=numberLabel.textColor;
        numberTF.font=numberLabel.font;
        numberTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        numberTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        [numberTF addTarget:self action:@selector(numberChange:) forControlEvents:UIControlEventEditingChanged];
        [numberView addSubview:numberTF];
        
#pragma mark 快递动态
        UILabel *dtLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, numberLabel.origin.y+numberLabel.height+14, 76, 38)];
        dtLabel.font=[UIFont systemFontOfSize:15];
        dtLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        dtLabel.text=@"快递动态：";
        [self addSubview:dtLabel];
        
        dtView=[[UIImageView alloc] initWithFrame:CGRectMake(dtLabel.origin.x+dtLabel.width, dtLabel.origin.y, SCREENSIZE.width-(dtLabel.origin.x+dtLabel.width+12), dtLabel.height)];
        dtView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        dtView.userInteractionEnabled=YES;
        [self addSubview:dtView];
        
        dtTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, dtView.width-8, dtView.height)];
        dtTF.placeholder=@"填写快递动态";
        dtTF.textColor=dtLabel.textColor;
        dtTF.font=dtLabel.font;
        dtTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        dtTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        [dtTF addTarget:self action:@selector(dongtaiChange:) forControlEvents:UIControlEventEditingChanged];
        [dtView addSubview:dtTF];
        
        CGFloat lastYH=dtLabel.origin.y+dtLabel.height+24;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lastYH] forKey:@"MaijiaOrderDetailThreeCell_Height"];
    }
    return self;
}

- (void)clickStatusView{
    [self.delegate clickGoodsStatusView];
}

-(void)upDateGoodStatusLabel:(NSNotification *)noti{
    SubmitOrder_SeletedTypeModel *model=noti.object;
    statusTF.text=model.typeName;
}

-(void)reflushDateForStatus:(NSString *)status kdCompany:(NSString *)kdCompany kdNumber:(NSString *)kdNumber kdDongtai:(NSString *)kdDongtai{
    NSString *statusStr;
    if ([status isEqualToString:@"0"]||[status isEqualToString:@"1"]) {
        statusStr=@"商品未发货";
        statusView.userInteractionEnabled=YES;
        companyTF.enabled=YES;
        numberTF.enabled=YES;
        dtTF.enabled=YES;
        statusView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        companyView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        numberView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        dtView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        
        companyTF.text=[kdCompany isEqualToString:@""]||kdCompany ==nil?@"":kdCompany;
        
        numberTF.text=[kdNumber isEqualToString:@""]||kdNumber ==nil?@"":kdNumber;
        
        dtTF.text=[kdDongtai isEqualToString:@""]||kdDongtai ==nil?@"":kdDongtai;
    }else if ([status isEqualToString:@"2"]){
        statusStr=@"商品已发货";
        statusView.userInteractionEnabled=YES;
        companyTF.enabled=YES;
        numberTF.enabled=YES;
        dtTF.enabled=YES;
        statusView.image=[UIImage imageNamed:@"shangjia_sc_img"];
        companyView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        numberView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        dtView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        
        companyTF.text=[kdCompany isEqualToString:@""]||kdCompany ==nil?@"暂无数据":kdCompany;
        
        numberTF.text=[kdNumber isEqualToString:@""]||kdNumber ==nil?@"暂无数据":kdNumber;
        
        dtTF.text=[kdDongtai isEqualToString:@""]||kdDongtai ==nil?@"暂无数据":kdDongtai;
    }else{
        statusStr=@"商品已收货";
        statusView.userInteractionEnabled=NO;
        companyTF.enabled=NO;
        numberTF.enabled=NO;
        dtTF.enabled=NO;
        statusView.image=[UIImage imageNamed:@"maijia_detail"];
        companyView.image=[UIImage imageNamed:@"maijia_detail"];
        numberView.image=[UIImage imageNamed:@"maijia_detail"];
        dtView.image=[UIImage imageNamed:@"maijia_detail"];
        
        companyTF.text=[kdCompany isEqualToString:@""]||kdCompany ==nil?@"暂无数据":kdCompany;
        
        numberTF.text=[kdNumber isEqualToString:@""]||kdNumber ==nil?@"暂无数据":kdNumber;
        
        dtTF.text=[kdDongtai isEqualToString:@""]||kdDongtai ==nil?@"暂无数据":kdDongtai;
    }
    statusTF.text=statusStr;
}

#pragma mark - textField change
- (void)companyChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.kdCompanyBlock) {
        self.kdCompanyBlock(field.text);
    }
}

- (void)numberChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.kdNumberBlock) {
        self.kdNumberBlock(field.text);
    }
}

- (void)dongtaiChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.kdDongtaiBlock) {
        self.kdDongtaiBlock(field.text);
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
