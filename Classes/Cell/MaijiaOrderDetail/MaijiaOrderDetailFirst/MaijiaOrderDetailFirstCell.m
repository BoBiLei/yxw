//
//  MaijiaOrderDetailFirstCell.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaOrderDetailFirstCell.h"

@implementation MaijiaOrderDetailFirstCell{
    UILabel *statusLabel;
    UILabel *numLabel;
    UILabel *orderTimeLabel;
    UILabel *payTimeLabel;
    UILabel *fahuoTimeLabel;
    UITextField *bzRight;
    UIImageView *bzView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#pragma mark 订单状态
        statusLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 16, SCREENSIZE.width-24, 21)];
        statusLabel.font=[UIFont systemFontOfSize:15];
        statusLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        [self addSubview:statusLabel];
        
#pragma mark 订单编号
        numLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, statusLabel.origin.y+statusLabel.height+8, SCREENSIZE.width-24, 21)];
        numLabel.font=[UIFont systemFontOfSize:15];
        numLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        [self addSubview:numLabel];
        
#pragma mark 下单时间
        orderTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, numLabel.origin.y+numLabel.height+8, SCREENSIZE.width-24, 21)];
        orderTimeLabel.font=[UIFont systemFontOfSize:15];
        orderTimeLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        [self addSubview:orderTimeLabel];
        
#pragma mark 付款时间
        payTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, orderTimeLabel.origin.y+orderTimeLabel.height+8, SCREENSIZE.width-24, 21)];
        payTimeLabel.font=[UIFont systemFontOfSize:15];
        payTimeLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        [self addSubview:payTimeLabel];
        
#pragma mark 发货时间
        fahuoTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, payTimeLabel.origin.y+payTimeLabel.height+8, SCREENSIZE.width-24, 21)];
        fahuoTimeLabel.font=[UIFont systemFontOfSize:15];
        fahuoTimeLabel.textColor=[UIColor colorWithHexString:@"#252525"];
        [self addSubview:fahuoTimeLabel];
        
#pragma mark 卖家备注
        UILabel *bzLeft=[[UILabel alloc] initWithFrame:CGRectMake(12, fahuoTimeLabel.origin.y+fahuoTimeLabel.height+8, 76, 36)];
        bzLeft.font=[UIFont systemFontOfSize:15];
        bzLeft.textColor=[UIColor colorWithHexString:@"#252525"];
        bzLeft.text=@"卖家备注：";
        [self addSubview:bzLeft];
        
        bzView=[[UIImageView alloc] initWithFrame:CGRectMake(bzLeft.origin.x+bzLeft.width, bzLeft.origin.y, SCREENSIZE.width-(bzLeft.origin.x+bzLeft.width+12), bzLeft.height)];
        bzView.userInteractionEnabled=YES;
        bzView.image=[UIImage imageNamed:@"shangjia_sc_norjt"];
        [self addSubview:bzView];
        
        bzRight=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, bzView.width-8, bzView.height)];
        bzRight.font=bzLeft.font;
        bzRight.placeholder=@"请输入卖家备注";
        bzRight.textColor=bzLeft.textColor;
        bzRight.clearButtonMode=UITextFieldViewModeWhileEditing;
        [bzRight addTarget:self action:@selector(zbTextChange:) forControlEvents:UIControlEventEditingChanged];
        [bzView addSubview:bzRight];
        
        CGFloat lastYH=bzView.origin.y+bzView.height+40;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lastYH] forKey:@"MaijiaOrderDetailFirstCell_Height"];
    }
    return self;
}

#pragma mark - textField change
- (void)zbTextChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.bzTextFieldBlock) {
        self.bzTextFieldBlock(field.text);
    }
}

-(void)reflushViewForOrderStatus:(NSString *)orderStatus orderSn:(NSString *)orderSn addTime:(NSString *)addTime payTime:(NSString *)payTime shippingTime:(NSString *)shippingTime liuyan:(NSString *)liuyan{
    
    NSString *statusShow;
    if ([orderStatus isEqualToString:@"0"]||[orderStatus isEqualToString:@"1"]) {
        statusShow=@"未发货";
        bzRight.enabled=YES;
    }else if ([orderStatus isEqualToString:@"2"]){
        statusShow=@"已发货";
        bzRight.enabled=YES;
    }else{
        statusShow=@"已收货";
        bzRight.enabled=NO;
    }
    statusLabel.text=[NSString stringWithFormat:@"订单状态：%@",statusShow];
    
    numLabel.text=[NSString stringWithFormat:@"订单编号：%@",orderSn];
    
    //时间戳转换
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:timeZone];
    
    NSTimeInterval time01=[addTime doubleValue];
    NSDate *date01=[NSDate dateWithTimeIntervalSince1970:time01];
    NSString *tStr01=[dateFormatter stringFromDate: date01];
    orderTimeLabel.text=[NSString stringWithFormat:@"下单时间：%@",[addTime isEqualToString:@"0"]?@"未知":tStr01];
    
    NSTimeInterval time02=[payTime doubleValue];
    NSDate *date02=[NSDate dateWithTimeIntervalSince1970:time02];
    NSString *tStr02=[dateFormatter stringFromDate: date02];
    payTimeLabel.text=[NSString stringWithFormat:@"付款时间：%@",[payTime isEqualToString:@"0"]?@"未知":tStr02];
    
    NSTimeInterval time03=[shippingTime doubleValue];
    NSDate *date03=[NSDate dateWithTimeIntervalSince1970:time03];
    NSString *tStr03=[dateFormatter stringFromDate: date03];
    fahuoTimeLabel.text=[NSString stringWithFormat:@"发货时间：%@",[shippingTime isEqualToString:@"0"]?@"未知":tStr03];
    
    bzRight.text=liuyan;
    
}

@end
