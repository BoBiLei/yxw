//
//  MaijiaOrderDetailFirstCell.h
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaijiaOrderDetailFirstCell : UITableViewCell

@property (nonatomic, copy) void (^bzTextFieldBlock)(NSString *tfText);

-(void)reflushViewForOrderStatus:(NSString *)orderStatus orderSn:(NSString *)orderSn addTime:(NSString *)addTime payTime:(NSString *)payTime shippingTime:(NSString *)shippingTime liuyan:(NSString *)liuyan;

@end
