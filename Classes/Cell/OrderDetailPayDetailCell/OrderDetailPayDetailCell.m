//
//  OrderDetailPayDetailCell.m
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderDetailPayDetailCell.h"

@implementation OrderDetailPayDetailCell

- (void)awakeFromNib {
    //期数
    _stageLabel=[[UILabel alloc]init];
    _stageLabel.font=[UIFont systemFontOfSize:13];
    _stageLabel.textAlignment=NSTextAlignmentCenter;
    _stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_stageLabel];
    _stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_stageLabel(52)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_h];
    
    NSArray* _stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_stageLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_w];
    
    //line01
    UIView *line01=[[UIView alloc]init];
    line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line01];
    line01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_stageLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel,line01)];
    [NSLayoutConstraint activateConstraints:line01_h];
    
    NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
    [NSLayoutConstraint activateConstraints:line01_w];
    
    //扣款日期
    _cutPayTimeLabel=[[UILabel alloc]init];
    _cutPayTimeLabel.font=[UIFont systemFontOfSize:13];
    _cutPayTimeLabel.textAlignment=NSTextAlignmentCenter;
    _cutPayTimeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_cutPayTimeLabel];
    _cutPayTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line01][_cutPayTimeLabel(110)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,_cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_h];
    
    NSArray* _cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_cutPayTimeLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_w];
    
    //line02
    UIView *line02=[[UIView alloc]init];
    line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line02];
    line02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_cutPayTimeLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_cutPayTimeLabel,line02)];
    [NSLayoutConstraint activateConstraints:line02_h];
    
    NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
    [NSLayoutConstraint activateConstraints:line02_w];
    
    //金额
    _priceLabel=[[UILabel alloc]init];
    _priceLabel.font=[UIFont systemFontOfSize:13];
    _priceLabel.textAlignment=NSTextAlignmentCenter;
    _priceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_priceLabel];
    _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line02][_priceLabel(80)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_h];
    
    NSArray* _priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_priceLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_w];
    
    //line03
    UIView *line03=[[UIView alloc]init];
    line03.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line03];
    line03.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line03_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_priceLabel][line03(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceLabel,line03)];
    [NSLayoutConstraint activateConstraints:line03_h];
    
    NSArray* line03_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line03]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03)];
    [NSLayoutConstraint activateConstraints:line03_w];
    
    //状态
    _statusLabel=[[UILabel alloc]init];
    _statusLabel.font=[UIFont systemFontOfSize:13];
    _statusLabel.textAlignment=NSTextAlignmentCenter;
    _statusLabel.textColor=[UIColor redColor];
    [self addSubview:_statusLabel];
    _statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _statusLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line03][_statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03,_statusLabel)];
    [NSLayoutConstraint activateConstraints:_statusLabel_h];
    
    NSArray* _statusLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_statusLabel)];
    [NSLayoutConstraint activateConstraints:_statusLabel_w];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(OrderDetailStageModel *)model indexPath:(NSIndexPath *)indexPath{
    self.stageLabel.text=[NSString stringWithFormat:@"%@期",model.stageNum];
    self.cutPayTimeLabel.text=model.cutMoneyTime;
    
    NSString *priceStr=indexPath.row==0?[NSString stringWithFormat:@"%@元(含手续费)",model.stageMoney]:[NSString stringWithFormat:@"%@元",model.stageMoney];
    _priceLabel.text=priceStr;
    if ([model.status isEqualToString:@"2"]) {
        self.statusLabel.text=@"已支付";
        self.statusLabel.textColor=[UIColor colorWithHexString:@"#00a650"];
    }else{
        self.statusLabel.text=@"待扣款";
        self.statusLabel.textColor=[UIColor redColor];
    }
}

@end
