//
//  OrderDetailPayDetailCell.h
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailStageModel.h"

@interface OrderDetailPayDetailCell : UITableViewCell

@property (nonatomic ,strong) UILabel *stageLabel;
@property (nonatomic ,strong) UILabel *cutPayTimeLabel;//扣款如期
@property (nonatomic ,strong) UILabel *priceLabel;//金额
@property (nonatomic ,strong) UILabel *statusLabel;//状态

-(void)reflushDataForModel:(OrderDetailStageModel *)model indexPath:(NSIndexPath *)indexPath;

@end
