//
//  OrderDetailAddressCell.h
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailAddressModel.h"

@interface OrderDetailAddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *receiver;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *orderTime;

-(void)reflushDataForModel:(OrderDetailAddressModel *)model;

@end
