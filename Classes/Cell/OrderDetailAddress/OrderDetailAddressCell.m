//
//  OrderDetailAddressCell.m
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderDetailAddressCell.h"

@implementation OrderDetailAddressCell

- (void)awakeFromNib {
    self.receiver.textColor=[UIColor colorWithHexString:@"#282828"];
    self.address.textColor=[UIColor colorWithHexString:@"#282828"];
    self.phone.textColor=[UIColor colorWithHexString:@"#282828"];
    self.orderTime.textColor=[UIColor colorWithHexString:@"#282828"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(OrderDetailAddressModel *)model{
    self.receiver.text=[NSString stringWithFormat:@"收货人：%@",model.receiverName];
    self.address.text=[NSString stringWithFormat:@"地址：%@",model.receiverAddress];
    self.phone.text=[NSString stringWithFormat:@"手机号码：%@",model.receiverPhone];
    self.orderTime.text=[NSString stringWithFormat:@"下单时间：%@",model.orderTime];
}

@end
