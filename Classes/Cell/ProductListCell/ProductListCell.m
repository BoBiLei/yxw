//
//  ProductListCell.m
//  CatShopping
//
//  Created by mac on 15/9/24.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProductListCell.h"
#import "Macro2.h"
#define IMGHEIGHT SCREENSIZE.width/2-30
@implementation ProductListCell

- (void)awakeFromNib {
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.width=(SCREENSIZE.width)/2-14;
    frame.size.height=_stagePriceLabel.origin.y+_stagePriceLabel.height+8;
    self.frame=frame;
    
    self.layer.cornerRadius=5;
    self.backgroundColor=[UIColor whiteColor];
    //图片
    _goodImg=[[UIImageView alloc]initWithFrame:CGRectMake(8, 6, IMGHEIGHT, IMGHEIGHT+6)];
    _goodImg.contentMode=UIViewContentModeScaleAspectFit;
    [self addSubview:_goodImg];
    
    //isNew
    _isNewGood=[UIButton buttonWithType:UIButtonTypeCustom];
    _isNewGood.frame=CGRectMake(self.width-29, 1, 28, 28);
    _isNewGood.titleLabel.font=[UIFont systemFontOfSize:10];
    [self addSubview:_isNewGood];
    _isNewGood.layer.cornerRadius=_isNewGood.height/2;
    
    //title
    _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, _goodImg.origin.y+_goodImg.height, self.width-16, 32)];
    _titleLabel.numberOfLines=0;
    _titleLabel.font=[UIFont systemFontOfSize:13];
    _titleLabel.textAlignment=NSTextAlignmentLeft;
    _titleLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_titleLabel];
    
    //price
    _mmPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, _titleLabel.origin.y+_titleLabel.height+4, self.width-16, 14)];
    _mmPriceLabel.textAlignment=NSTextAlignmentLeft;
    _mmPriceLabel.textColor=[UIColor lightGrayColor];
    _mmPriceLabel.font=[UIFont systemFontOfSize:12];
    [self addSubview:_mmPriceLabel];
    
    //stageLabel
    _stagePriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, _mmPriceLabel.origin.y+_mmPriceLabel.height+4, self.width-16, _mmPriceLabel.height)];
    _stagePriceLabel.textAlignment=NSTextAlignmentLeft;
    _stagePriceLabel.textColor=[UIColor colorWithHexString:@"#fc131f"];
    _stagePriceLabel.font=[UIFont systemFontOfSize:12];
    
    [self addSubview:_stagePriceLabel];
}

-(void)reflushDataForModel:(ProductListModel *)model{
    self.goodId=model.idStr;
    [self.goodImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.imgStr]] placeholderImage:[UIImage imageNamed:@"placeholder_160x180"]];
    if ([model.good_num isEqualToString:@"0"]) {
        _isNewGood.backgroundColor=[UIColor lightGrayColor];
        [_isNewGood setTitle:@"已售" forState:UIControlStateNormal];
        _isNewGood.hidden=NO;
    }else{
        _isNewGood.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        [_isNewGood setTitle:@"全新" forState:UIControlStateNormal];
        if ([model.isNewStr isEqualToString:@"1"]) {
            _isNewGood.hidden=NO;
        }else{
            _isNewGood.hidden=YES;
        }
    }
    self.titleLabel.text=model.titleStr;
    self.mmPriceLabel.text=[NSString stringWithFormat:@"游侠价：%@元",model.mmPriceStr];
    self.stagePriceLabel.text=[NSString stringWithFormat:@"分期价：%@",model.stagePriceStr];
    NSLog(@"%@---%@",model.mmPriceStr,model.stagePriceStr);
}

-(void)reflushTopicDataForModel:(ProductListModel *)model{
    self.goodId=model.idStr;
    [self.goodImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.imgStr]] placeholderImage:[UIImage imageNamed:@"placeholder_160x180"]];
    self.titleLabel.text=model.titleStr;
    self.mmPriceLabel.text=[NSString stringWithFormat:@"猫猫价：%@",model.mmPriceStr];
    self.stagePriceLabel.text=[NSString stringWithFormat:@"分期价：%@ X 6期",model.stagePriceStr];
}

@end
