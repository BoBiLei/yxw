//
//  ProductListCell.h
//  CatShopping
//
//  Created by mac on 15/9/24.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListModel.h"


@interface ProductListCell : UICollectionViewCell

@property (nonatomic,copy) NSString *goodId;
@property (nonatomic,strong) UIImageView *goodImg;
@property (nonatomic,strong) UIButton *isNewGood;//是否是新商品标记
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *mmPriceLabel;//游侠价
@property (nonatomic,strong) UILabel *stagePriceLabel;//分期价

-(void)reflushDataForModel:(ProductListModel *)model;


-(void)reflushTopicDataForModel:(ProductListModel *)model;
@end
