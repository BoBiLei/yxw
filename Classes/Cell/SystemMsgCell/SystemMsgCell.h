//
//  SystemMsgCell.h
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SystemMsgModel.h"

@interface SystemMsgCell : UITableViewCell

-(void)reflushDataForModel:(SystemMsgModel *)model;

@end
