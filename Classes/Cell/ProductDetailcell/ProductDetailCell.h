//
//  ProductDetailCell.h
//  MMG
//
//  Created by mac on 15/10/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailCell : UITableViewCell<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic ,assign)NSMutableArray *dataArr;
@property(nonatomic ,strong)UITableView *table;
@end
