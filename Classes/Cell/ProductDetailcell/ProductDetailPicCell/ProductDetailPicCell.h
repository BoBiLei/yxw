//
//  ProductDetailPicCell.h
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailPicCell : UITableViewCell
@property (strong, nonatomic) UIImageView *imgv;

-(void)setUpUIAndInitDataForDictuinary:(NSDictionary *)dic;

@end
