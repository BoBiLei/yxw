//
//  ProductDetailPicCell.m
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProductDetailPicCell.h"
#import "Macro2.h"
@implementation ProductDetailPicCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUpUIAndInitDataForDictuinary:(NSDictionary *)dic{
    
    CGRect frame=self.frame;
    
    NSString *wStr=dic[@"imgW"];
    NSString *imgWidth=![AppUtils isStringIncludeDigitChar:wStr]?@"750":dic[@"imgW"];
    NSString *hStr=dic[@"imgH"];
    NSString *imgHeight=hStr.floatValue>1200?@"780":hStr;
    
    CGFloat imgWidFloat=imgWidth.floatValue;
    CGFloat imgHeiFloat=imgHeight.floatValue;
    CGFloat imgReHeight=0.0f;;
    imgReHeight=(SCREENSIZE.width*imgHeiFloat)/imgWidFloat;
    _imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, imgReHeight)];
    _imgv.contentMode=UIViewContentModeScaleAspectFit;
    if ([AppUtils isStringIncludeDigitChar:wStr]) {
        [_imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,dic[@"imgStr"]]]];
    }
    [self addSubview:_imgv];
    
    CGFloat cellHeight=0.0f;
    
    cellHeight=imgReHeight;
    
    frame.size.height =cellHeight;
    
    //重新设置cell height
    self.frame = frame;
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

@end
