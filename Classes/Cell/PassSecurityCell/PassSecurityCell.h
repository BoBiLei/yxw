//
//  PassSecurityCell.h
//  CatShopping
//
//  Created by mac on 15/9/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassSecurityCell : UITableViewCell

@property (strong, nonatomic) UIImageView *imgv;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *rightTitleLabel;

-(void)reflushDataForIndexPath:(NSIndexPath *)indexPath verifyString:(NSString *)verifyStr;

@end
