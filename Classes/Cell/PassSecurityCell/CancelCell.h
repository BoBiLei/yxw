//
//  CancelCell.h
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SafeCenterCancelDelegate <NSObject>

-(void)clickCancelButton;

@end

@interface CancelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *cencelBtn;
@property (nonatomic, weak) id<SafeCenterCancelDelegate> delegate;
@end
