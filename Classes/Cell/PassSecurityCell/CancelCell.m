//
//  CancelCell.m
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "CancelCell.h"

@implementation CancelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _cencelBtn.layer.cornerRadius=3;
    _cencelBtn.backgroundColor=[UIColor redColor];
    [_cencelBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    _cencelBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [_cencelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cencelBtn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickBtn{
    [self.delegate clickCancelButton];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
