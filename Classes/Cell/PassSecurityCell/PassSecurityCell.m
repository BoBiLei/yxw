//
//  PassSecurityCell.m
//  CatShopping
//
//  Created by mac on 15/9/28.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "PassSecurityCell.h"

#define TITLELABELSTEING01 @"登录密码"
#define TITLELABELSTEING02 @"邮箱验证"

#define RIGHTTITLELABELSTEING01 @"修改密码"
#define RIGHTTITLELABELSTEING02 @""
#define RIGHTTITLELABELSTEING03 @""

@implementation PassSecurityCell{
    NSString *phoneLabelStr;
}

- (void)awakeFromNib {
    
}

-(void)reflushDataForIndexPath:(NSIndexPath *)indexPath verifyString:(NSString *)verifyStr{
    //imageview
    _imgv=[[UIImageView alloc]init];
    _imgv.contentMode=UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_imgv];
    _imgv.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _imgv_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[_imgv(32)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv)];
    [NSLayoutConstraint activateConstraints:_imgv_h];
    
    NSArray* _imgv_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_imgv(32)]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv)];
    [NSLayoutConstraint activateConstraints:_imgv_w];
    
    //title
    _titleLabel=[[UILabel alloc]init];
    _titleLabel.numberOfLines=0;
    _titleLabel.font=[UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _titleLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_imgv]-8-[_titleLabel(100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv,_titleLabel)];
    [NSLayoutConstraint activateConstraints:_titleLabel_h];
    
    NSArray* _titleLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_titleLabel(32)]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_titleLabel)];
    [NSLayoutConstraint activateConstraints:_titleLabel_w];
    
    //righttitle
    _rightTitleLabel=[[UILabel alloc]init];
    _rightTitleLabel.numberOfLines=0;
    _rightTitleLabel.textColor=[UIColor colorWithHexString:@"#ababab"];
    _rightTitleLabel.textAlignment=NSTextAlignmentRight;
    _rightTitleLabel.font=[UIFont systemFontOfSize:14];
    [self.contentView addSubview:_rightTitleLabel];
    _rightTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _rightTitleLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_rightTitleLabel(130)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_rightTitleLabel)];
    [NSLayoutConstraint activateConstraints:_rightTitleLabel_h];
    
    NSArray* _rightTitleLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_rightTitleLabel(32)]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_rightTitleLabel)];
    [NSLayoutConstraint activateConstraints:_rightTitleLabel_w];
    
    if ([verifyStr isEqualToString:@""]) {
        phoneLabelStr=@"手机未绑定";
    }else{
        phoneLabelStr=@"手机验证";
    }
    
    switch (indexPath.section) {
        case 0:
            _imgv.image=[UIImage imageNamed:@"PassSecurity_tb01"];
            _titleLabel.text=TITLELABELSTEING01;
            _rightTitleLabel.text=RIGHTTITLELABELSTEING01;
            break;
        case 1:
            _imgv.image=[UIImage imageNamed:@"PassSecurity_tb03"];
            _titleLabel.text=phoneLabelStr;
            _rightTitleLabel.text=RIGHTTITLELABELSTEING03;
            break;
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
