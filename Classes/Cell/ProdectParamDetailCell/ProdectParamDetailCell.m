//
//  ProdectParamDetailCell.m
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProdectParamDetailCell.h"
@implementation ProdectParamDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderWidth=0.5f;
    self.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    _leftLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 2, (SCREENSIZE.width-24)/2-0.5f, self.frame.size.height-4)];
    _leftLabel.textColor=[UIColor lightGrayColor];
    _leftLabel.font=[UIFont systemFontOfSize:13];
    [self addSubview:_leftLabel];
    
    //
    UIView *vLine=[[UIView alloc]initWithFrame:CGRectMake(SCREENSIZE.width/2, 0, 1, self.frame.size.height)];
    vLine.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:vLine];
    
    //
    _rightLabel=[[UILabel alloc]initWithFrame:CGRectMake(vLine.frame.origin.x+vLine.frame.size.width+12, 2, _leftLabel.frame.size.width-12, _leftLabel.frame.size.height)];
    _rightLabel.textColor=[UIColor lightGrayColor];
    _rightLabel.font=[UIFont systemFontOfSize:13];
    [self addSubview:_rightLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(GoodParamModel *)model{
    _leftLabel.text=model.paramName;
    _rightLabel.text=model.paramValue;
}

@end
