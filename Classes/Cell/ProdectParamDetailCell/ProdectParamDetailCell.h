//
//  ProdectParamDetailCell.h
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodParamModel.h"
@interface ProdectParamDetailCell : UITableViewCell
@property (strong, nonatomic) UILabel *leftLabel;
@property (strong, nonatomic) UILabel *rightLabel;

-(void)reflushDataForModel:(GoodParamModel *)model;

@end
