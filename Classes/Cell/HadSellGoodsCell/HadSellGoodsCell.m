//
//  HadSellGoodsCell.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "HadSellGoodsCell.h"
#import "Macro2.h"
@implementation HadSellGoodsCell{
    UIImageView *imgv;
    UIImageView *llImgv;
    UILabel *nameLabel;
    TTTAttributedLabel *sellPriceLabel;
    UILabel *wuliuLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#pragma mark 图片
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(12, 12, SCREENSIZE.width/4, SCREENSIZE.width/4)];
        imgv.layer.borderWidth=0.5f;
        imgv.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
        [self addSubview:imgv];
        
        CGFloat lliHeight=imgv.width/4+3;
        llImgv=[[UIImageView alloc] initWithFrame:CGRectMake(imgv.width/3, imgv.height-lliHeight, imgv.width*2/3, lliHeight)];
        [imgv addSubview:llImgv];
        
#pragma mark 订单详情按钮
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(imgv.origin.x, imgv.origin.y+imgv.height+6, imgv.width, imgv.height/3+3);
        btn.layer.cornerRadius=2;
        btn.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:16];
        [btn setTitle:@"订单详情" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        CGFloat rightH=(imgv.height+btn.height+6)/3;
        CGFloat lineH=0.6f;
#pragma mark 名称
        nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgv.origin.x+imgv.width+10, imgv.origin.y, SCREENSIZE.width-(imgv.origin.x+imgv.width+20), rightH)];
        nameLabel.numberOfLines=0;
        nameLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        nameLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview:nameLabel];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, nameLabel.origin.y+nameLabel.height)];
        
#pragma mark 寄售价
        sellPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height, nameLabel.width, rightH)];
        sellPriceLabel.numberOfLines=0;
        sellPriceLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        sellPriceLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview:sellPriceLabel];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, sellPriceLabel.origin.y+sellPriceLabel.height)];
        
#pragma mark 物流
        wuliuLabel=[[UILabel alloc] initWithFrame:CGRectMake(sellPriceLabel.origin.x, sellPriceLabel.origin.y+sellPriceLabel.height, sellPriceLabel.width, rightH)];
        wuliuLabel.numberOfLines=0;
        wuliuLabel.textColor=[UIColor colorWithHexString:@"#363636"];
        wuliuLabel.font=[UIFont systemFontOfSize:15];
        [self addSubview:wuliuLabel];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:lineH firstPoint:CGPointMake(wuliuLabel.origin.x, wuliuLabel.origin.y+wuliuLabel.height) endPoint:CGPointMake(SCREENSIZE.width-12, wuliuLabel.origin.y+wuliuLabel.height)];
        
        CGFloat lsYH=btn.origin.y+btn.height+18;
        [AppUtils saveValue:[NSString stringWithFormat:@"%.2f",lsYH] forKey:@"HadSellGoodsCell_Height"];
    }
    return self;
}

-(void)clickBtn:(id)sender{
    [self.delegate scanOrderDetail:self.model];
}

-(void)reflushDataForModel:(HadSellerGoodsModel *)model{
    
    [imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImg]]];
    
    NSString *jsimg;
    if ([model.js_status isEqualToString:@"1"]) {
        jsimg=@"maijia_waitpay";
    }else if ([model.js_status isEqualToString:@"1"]){
        jsimg=@"maijia_hanpay";
    }else{
        jsimg=@"maijia_paying";
    }
    llImgv.image=[UIImage imageNamed:jsimg];
    
    nameLabel.text=[NSString stringWithFormat:@"名  称：%@",model.goodsTitle];
    
    NSString *sellpStr=[NSString stringWithFormat:@"寄售价：%@元",model.goodsPrice];
    [sellPriceLabel setText:sellpStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",model.goodsPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff0000"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    NSString *wlStr;
    switch (model.status.integerValue) {
        case 1:
            wlStr=@"未发货";
            break;
        case 2:
            wlStr=@"已发货";
            break;
        case 3:
            wlStr=@"已收货";
            break;
        default:
            wlStr=@"未发货";
            break;
    }
    wuliuLabel.text=[NSString stringWithFormat:@"物 流：%@",wlStr];
    
}

@end
