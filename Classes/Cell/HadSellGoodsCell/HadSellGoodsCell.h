//
//  HadSellGoodsCell.h
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HadSellerGoodsModel.h"

@protocol HadSellGoodsDelegate <NSObject>

/**
 *  订单详情
 */
-(void)scanOrderDetail:(HadSellerGoodsModel *)model;

@end

@interface HadSellGoodsCell : UITableViewCell

@property (nonatomic, weak) id<HadSellGoodsDelegate> delegate;

@property (nonatomic, strong) HadSellerGoodsModel *model;

-(void)reflushDataForModel:(HadSellerGoodsModel *)model;

@end
