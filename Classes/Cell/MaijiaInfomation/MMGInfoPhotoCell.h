//
//  MMGInfoPhotoCell.h
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMGInfoPhotoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@end
