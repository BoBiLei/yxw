//
//  MMGInfoPhotoCell.m
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "MMGInfoPhotoCell.h"

@implementation MMGInfoPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    _imgv.layer.masksToBounds=YES;
    _imgv.layer.cornerRadius=self.imgv.height/2;
    _imgv.layer.borderWidth=0.7f;
    _imgv.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
}

-(void)upDatePhoto:(NSNotification *)noti{
    [_imgv setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:@""]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
