//
//  SubmitOrderGoodListCell.h
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartModel.h"


@interface SubmitOrderGoodListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;

-(void)reflushModeDate:(ShoppingCartModel *)model;

@end
