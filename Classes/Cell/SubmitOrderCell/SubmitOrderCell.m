//
//  SubmitOrderCell.m
//  MMG
//
//  Created by mac on 15/10/12.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SubmitOrderCell.h"

@implementation SubmitOrderCell

- (void)awakeFromNib {
    _leftLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    _rightLabel.textColor=[UIColor lightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
