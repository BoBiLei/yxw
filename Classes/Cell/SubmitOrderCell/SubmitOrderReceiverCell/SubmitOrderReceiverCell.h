//
//  SubmitOrderReceiverCell.h
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiverModel.h"

@interface SubmitOrderReceiverCell : UITableViewCell

@property (strong,nonatomic) UILabel *receiverNameLabel;
@property (strong,nonatomic) UILabel *receiverPhoneLabel;
@property (strong,nonatomic) UILabel *receiverAddressLabel;

-(void)reflushDataForModel:(ReceiverModel *)model;

@end
