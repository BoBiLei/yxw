//
//  SubmitOrderReceiverCell.m
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SubmitOrderReceiverCell.h"

@implementation SubmitOrderReceiverCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(ReceiverModel *)model{
    
    //获取当前cell frame
    CGRect frame = [self frame];
    UILabel *topLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, 107, 44)];
    topLabel.text=@"收货人信息";
    topLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    topLabel.font=[UIFont systemFontOfSize:13];
    [self addSubview:topLabel];
    
    CGFloat cellHeight=0.0f;
    
    if (model.receiverId==nil) {
        UILabel *rigLabel=[UILabel new];
        rigLabel.text=@"未添加收货人";
        rigLabel.textColor=[UIColor lightGrayColor];
        rigLabel.font=[UIFont systemFontOfSize:13];
        rigLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:rigLabel];
        rigLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* rigLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[topLabel]-8-[rigLabel]-32-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topLabel,rigLabel)];
        [NSLayoutConstraint activateConstraints:rigLabel_h];
        
        NSArray* rigLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rigLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(rigLabel)];
        [NSLayoutConstraint activateConstraints:rigLabel_w];
        
        
        cellHeight=topLabel.frame.size.height;
        frame.size.height = cellHeight;
    }else{
        //
        _receiverNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, topLabel.frame.origin.y+topLabel.frame.size.height-6, 80, 21)];
        _receiverNameLabel.text=model.receiverName;
        _receiverNameLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        _receiverNameLabel.font=[UIFont systemFontOfSize:13];
        
        //
        _receiverPhoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(_receiverNameLabel.frame.origin.x+_receiverNameLabel.frame.size.width, _receiverNameLabel.frame.origin.y, 160, 21)];
        _receiverPhoneLabel.text=model.phoneNum;
        _receiverPhoneLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        _receiverPhoneLabel.font=[UIFont systemFontOfSize:13];
        
        //
        _receiverAddressLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, _receiverPhoneLabel.frame.origin.y+_receiverPhoneLabel.frame.size.height+4, SCREENSIZE.width, 21)];
        _receiverAddressLabel.text=model.address;
        _receiverAddressLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        _receiverAddressLabel.font=[UIFont systemFontOfSize:13];
        
        cellHeight=_receiverAddressLabel.frame.origin.y+_receiverAddressLabel.frame.size.height+8;
        frame.size.height =cellHeight;
        
        [self addSubview:_receiverNameLabel];
        [self addSubview:_receiverPhoneLabel];
        [self addSubview:_receiverAddressLabel];
    }
    
    
    //重新设置cell height
    self.frame = frame;
    
//    cellHeight=0.0f;
}

@end
