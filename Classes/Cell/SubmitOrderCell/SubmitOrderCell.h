//
//  SubmitOrderCell.h
//  MMG
//
//  Created by mac on 15/10/12.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
