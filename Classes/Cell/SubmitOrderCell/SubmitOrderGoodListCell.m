//
//  SubmitOrderGoodListCell.m
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SubmitOrderGoodListCell.h"
#import "Macro2.h"
@implementation SubmitOrderGoodListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushModeDate:(ShoppingCartModel *)model{
    [_imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,model.goodsImgStr]]];
    _title.text=model.goodsTitle;
    
    NSRange range=[model.goodsPrice rangeOfString:@"元"];
    if (range.location!=NSNotFound) {
        _price.text=[NSString stringWithFormat:@"价格：%@",model.goodsPrice];
    }else{
        _price.text=[NSString stringWithFormat:@"价格：%@元",model.goodsPrice];
    }
}

@end
