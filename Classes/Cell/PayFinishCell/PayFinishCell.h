//
//  PayFinishCell.h
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayFinishCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *paySuccessLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNum;
@property (weak, nonatomic) IBOutlet UILabel *orderTitle;

@end
