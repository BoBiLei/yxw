//
//  PayFinishCell.m
//  MMG
//
//  Created by mac on 15/10/13.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "PayFinishCell.h"

@implementation PayFinishCell

- (void)awakeFromNib {
    self.paySuccessLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
