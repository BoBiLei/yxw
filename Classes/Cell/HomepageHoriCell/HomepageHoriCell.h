//
//  HomepageHoriCell.h
//  MMG
//
//  Created by mac on 16/9/2.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMGHomePageCagegoryModel.h"

@protocol ClickBusinessDelegate <NSObject>

-(void)clickBusinessWithModel:(MMGHomePageCagegoryModel *)model;

@end

@interface HomepageHoriCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) id<ClickBusinessDelegate> businessDelegate;

@property (nonatomic, strong) UITableView *myTable;

-(void)reflushDataForArray:(NSArray *)array;

@end
