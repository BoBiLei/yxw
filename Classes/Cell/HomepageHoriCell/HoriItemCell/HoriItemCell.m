//
//  HoriItemCell.m
//  MMG
//
//  Created by mac on 16/9/2.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "HoriItemCell.h"
#import <UIImageView+WebCache.h>
#import "Macro2.h"
#define tab_height 120
#define x_margin 0          //X轴间距
#define y_margin 8          //X轴间距
#define label_height 24     //label高度
#define sum 4               //个数
#define item_height (SCREENSIZE.width-(sum+1)*y_margin)/sum


@implementation HoriItemCell{
    UIImageView *imgv;
    UILabel *titleLabel;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
#pragma mark 图片
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(y_margin, item_height-x_margin, tab_height-2*y_margin-label_height, tab_height-2*y_margin-label_height)];
        imgv.contentMode=UIViewContentModeScaleAspectFit;
        imgv.center=CGPointMake(tab_height/2-label_height/2, item_height/2);
        //逆时针90°
        CGAffineTransform transform = CGAffineTransformMakeRotation(-90 * M_PI_4);
        [imgv setTransform:transform];
        imgv.layer.masksToBounds=YES;
        imgv.layer.cornerRadius=imgv.height/2;
        [self addSubview:imgv];
        
#pragma mark 标题
        titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgv.origin.x+imgv.width, imgv.origin.x+imgv.width-6, imgv.height+12, label_height)];
        titleLabel.center=CGPointMake(tab_height-y_margin-label_height/2+2, (item_height)/2);
        titleLabel.textColor=[UIColor colorWithHexString:@"#898989"];
        titleLabel.font=[UIFont systemFontOfSize:15];
        titleLabel.textAlignment=NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        //逆时针90°
        CGAffineTransform laTransform = CGAffineTransformMakeRotation(-90 * M_PI_4);
        [titleLabel setTransform:laTransform];
        [self addSubview:titleLabel];
    }
    return self;
}


-(void)reflushBusinessDataForModel:(MMGHomePageCagegoryModel *)model{
    
    if ([model.busId isEqualToString:@"noid"]) {
        imgv.image=[UIImage imageNamed:model.img];
    }else{
//        NSLog(@"%@--",[NSString stringWithFormat:@"%@%@",ImageHost03,model.img]);
        [imgv sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost03,model.img]]];
    }
    titleLabel.text=model.name;
}

@end
