//
//  HoriItemCell.h
//  MMG
//
//  Created by mac on 16/9/2.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMGHomePageCagegoryModel.h"

@interface HoriItemCell : UITableViewCell

-(void)reflushBusinessDataForModel:(MMGHomePageCagegoryModel *)model;

@end
