//
//  HomepageHoriCell.m
//  MMG
//
//  Created by mac on 16/9/2.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "HomepageHoriCell.h"
#import "HoriItemCell.h"

#define tab_height 120
@implementation HomepageHoriCell{
    NSArray *dataArr;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _myTable=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 0, tab_height, SCREENSIZE.width) style:UITableViewStyleGrouped];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        _myTable.backgroundColor=[UIColor whiteColor];
        _myTable.center=CGPointMake(SCREENSIZE.width/2, tab_height/2);
        [_myTable setContentOffset:CGPointMake(0,SCREENSIZE.height+164) animated:NO];
        _myTable.showsVerticalScrollIndicator=YES;
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [self addSubview:_myTable];
        
        //顺时针90°
        CGAffineTransform transform = CGAffineTransformMakeRotation(90 * M_PI_4);
        [_myTable setTransform:transform];
    }
    return self;
}

-(void)reflushDataForArray:(NSArray *)array{
    dataArr=array;
    [_myTable reloadData];
}


#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (SCREENSIZE.width-40)/4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 8;
    }
    return 0.0000000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HoriItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoriItemCell"];
    if (!cell) {
        cell=[[HoriItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoriItemCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    MMGHomePageCagegoryModel *model=dataArr[indexPath.section];
    [cell reflushBusinessDataForModel:model];
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MMGHomePageCagegoryModel *model=dataArr[indexPath.section];
    NSLog(@"%@----------",model.img);
    [self.businessDelegate clickBusinessWithModel:model];
}

@end
