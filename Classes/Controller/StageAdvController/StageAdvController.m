//
//  StageAdvController.m
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "StageAdvController.h"
#import "AdvDetailCell.h"

@interface StageAdvController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation StageAdvController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)setNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    //    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    //UIColor *color1=[UIColor colorWithRed:0.0 green:127.0 blue:181.0 alpha:1];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    //UIColor *color2=[[UIColor alloc] initWithRed:2.0f green:116.0f blue:181.0f alpha:1];
    //[statusBarView setBackgroundColor:color1];
    
    [self.view addSubview:statusBarView];
    
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"分期购物";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
    
}
-(void)fanhui
{
    
    [self .navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
//    [self setCustomNavigationTitle:@"分期购物"];
    [self setUpUI];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"StageAdv" ofType:@"plist"];
    NSArray *plistArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    for (NSDictionary *dic in plistArr) {
        AdvDetailModel *model=[[AdvDetailModel alloc]init];
        [model jsonDataForDictionary:dic];
        [dataArr addObject:model];
    }
    [myTable reloadData];
}

#pragma mark - init UI
-(void)setUpUI{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [myTable registerNib:[UINib nibWithNibName:@"AdvDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
    myTable.tableFooterView=[[UIView alloc]init];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    AdvDetailModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    AdvDetailModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    return  cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
