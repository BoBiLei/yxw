//
//  ReceiverManagerController.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ReceiverManagerController.h"
#import "Macro2.h"
@interface ReceiverManagerController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ReceiverManagerController




-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"收件人信息";
    [btnlab setTextColor:[UIColor whiteColor]];
    btnlab.textAlignment =NSTextAlignmentCenter;

    [self.view addSubview:btnlab];
    
}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self seNavBar];
//    [self setCustomNavigationTitle:@"收件人信息"];
}

-(void)setUpUI{
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.image=[UIImage imageNamed:@"login_baground"];
    
    UITableView *myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    [self.view addSubview:myTable];
    myTable.backgroundView=bagView;
    myTable.dataSource=self;
    myTable.delegate=self;
}

#pragma mark TableView delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
