//
//  PassWordModifyController.m
//  CatShopping
//
//  Created by mac on 15/9/17.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "PassWordModifyController.h"
#import "MMGSign.h"
#import "HttpRequests.h"
#import "Macro2.h"
@interface PassWordModifyController ()

@end

@implementation PassWordModifyController{
    UITextField *ognTextField;
    UITextField *newTextField;
    UITextField *confirmTextField;
    UIButton *submitBtn;
}
-(void)setNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    //UIColor *color1=[UIColor colorWithRed:0.0 green:127.0 blue:181.0 alpha:1];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    //UIColor *color2=[[UIColor alloc] initWithRed:2.0f green:116.0f blue:181.0f alpha:1];
    //[statusBarView setBackgroundColor:color1];
    
    [self.view addSubview:statusBarView];
    
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [fanHuiButton setBackgroundImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"修改密码";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
    
}
-(void)fanhui
{
    
    [self .navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
//    [self setCustomNavigationTitle:@"修改密码"];
    
    [self setUpUI];
}

-(void)setUpUI{
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.userInteractionEnabled=YES;
    bagView.image=[UIImage imageNamed:@"login_baground"];
    [self.view addSubview:bagView];
    
    UIView *mainView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 204)];
    mainView.layer.cornerRadius=3;
    mainView.backgroundColor=[UIColor whiteColor];
    [bagView addSubview:mainView];
    
    
    //原始密码
    UIView *originalPassView=[[UIView alloc]init];
    [mainView addSubview:originalPassView];
    originalPassView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* originalPassView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[originalPassView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalPassView)];
    [NSLayoutConstraint activateConstraints:originalPassView_h];
    
    NSArray* originalPassView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[originalPassView(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalPassView)];
    [NSLayoutConstraint activateConstraints:originalPassView_w];
    originalPassView.layer.cornerRadius=2;
    originalPassView.layer.borderWidth=.5;
    originalPassView.layer.borderColor=[UIColor colorWithHexString:@"#e0e0e0"].CGColor;
    
    //
    UILabel *originalLabel=[UILabel new];
    originalLabel.font=[UIFont systemFontOfSize:15];
    originalLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    originalLabel.textAlignment=NSTextAlignmentCenter;
    originalLabel.text=@"原始密码";
    [originalPassView addSubview:originalLabel];
    originalLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* originalLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-6-[originalLabel(64)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalLabel)];
    [NSLayoutConstraint activateConstraints:originalLabel_h];
    
    NSArray* originalLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[originalLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalLabel)];
    [NSLayoutConstraint activateConstraints:originalLabel_w];

    //
    ognTextField=[UITextField new];
    ognTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    [originalPassView addSubview:ognTextField];
    [ognTextField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    ognTextField.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* ognTextField_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[originalLabel]-8-[ognTextField]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalLabel,ognTextField)];
    [NSLayoutConstraint activateConstraints:ognTextField_h];
    
    NSArray* ognTextField_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ognTextField]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(ognTextField)];
    [NSLayoutConstraint activateConstraints:ognTextField_w];
    ognTextField.font=[UIFont systemFontOfSize:15];
    ognTextField.placeholder=@"输入原始密码";
    ognTextField.secureTextEntry = YES;
    ognTextField.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    
    //新密码
    UIView *newPassView=[[UIView alloc]init];
    [mainView addSubview:newPassView];
    newPassView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* newPassView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[newPassView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newPassView)];
    [NSLayoutConstraint activateConstraints:newPassView_h];
    
    NSArray* newPassView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[originalPassView]-8-[newPassView(originalPassView)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(originalPassView,newPassView)];
    [NSLayoutConstraint activateConstraints:newPassView_w];
    newPassView.layer.cornerRadius=2;
    newPassView.layer.borderWidth=.5;
    newPassView.layer.borderColor=[UIColor colorWithHexString:@"#e0e0e0"].CGColor;
    
    //
    UILabel *newLabel=[UILabel new];
    newLabel.font=[UIFont systemFontOfSize:15];
    newLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    newLabel.textAlignment=NSTextAlignmentCenter;
    newLabel.text=@"新 密 码";
    [newPassView addSubview:newLabel];
    newLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* newLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-6-[newLabel(originalLabel)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newLabel,originalLabel)];
    [NSLayoutConstraint activateConstraints:newLabel_h];
    
    NSArray* newLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[newLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newLabel)];
    [NSLayoutConstraint activateConstraints:newLabel_w];
    
    //
    newTextField=[UITextField new];
    newTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    [newPassView addSubview:newTextField];
    [newTextField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    newTextField.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* newTextField_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[newLabel]-8-[newTextField]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newLabel,newTextField)];
    [NSLayoutConstraint activateConstraints:newTextField_h];
    
    NSArray* newTextField_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[newTextField]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newTextField)];
    [NSLayoutConstraint activateConstraints:newTextField_w];
    newTextField.font=[UIFont systemFontOfSize:15];
    newTextField.placeholder=@"输入新密码";
    newTextField.secureTextEntry = YES;
    newTextField.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    
    //确认密码
    UIView *confirmPassView=[[UIView alloc]init];
    [mainView addSubview:confirmPassView];
    confirmPassView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* confirmPassView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[confirmPassView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmPassView)];
    [NSLayoutConstraint activateConstraints:confirmPassView_h];
    
    NSArray* confirmPassView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[newPassView]-8-[confirmPassView(newPassView)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(newPassView,confirmPassView)];
    [NSLayoutConstraint activateConstraints:confirmPassView_w];
    confirmPassView.layer.cornerRadius=2;
    confirmPassView.layer.borderWidth=.5;
    confirmPassView.layer.borderColor=[UIColor colorWithHexString:@"#e0e0e0"].CGColor;
    
    //
    UILabel *confirmLabel=[UILabel new];
    confirmLabel.font=[UIFont systemFontOfSize:15];
    confirmLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    confirmLabel.textAlignment=NSTextAlignmentCenter;
    confirmLabel.text=@"确认密码";
    [confirmPassView addSubview:confirmLabel];
    confirmLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* confirmLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-6-[confirmLabel(originalLabel)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmLabel,originalLabel)];
    [NSLayoutConstraint activateConstraints:confirmLabel_h];
    
    NSArray* confirmLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[confirmLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmLabel)];
    [NSLayoutConstraint activateConstraints:confirmLabel_w];
    
    //
    confirmTextField=[UITextField new];
    confirmTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    [confirmPassView addSubview:confirmTextField];
    [confirmTextField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    confirmTextField.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* confirmTextField_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[confirmLabel]-8-[confirmTextField]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmLabel,confirmTextField)];
    [NSLayoutConstraint activateConstraints:confirmTextField_h];
    
    NSArray* confirmTextField_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[confirmTextField]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmTextField)];
    [NSLayoutConstraint activateConstraints:confirmTextField_w];
    confirmTextField.font=[UIFont systemFontOfSize:15];
    confirmTextField.placeholder=@"输入确认密码";
    confirmTextField.secureTextEntry = YES;
    confirmTextField.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    
    //button
    submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.enabled=NO;
    [submitBtn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.backgroundColor=[UIColor lightGrayColor];
    [submitBtn setTitle:@"保存修改" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mainView addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* submitBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[submitBtn]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_h];
    
    NSArray* submitBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[confirmPassView]-8-[submitBtn]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(confirmPassView,submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_w];
    submitBtn.layer.cornerRadius=2;
}

- (void)textDidChange:(id) sender {
    if ((![ognTextField.text isEqualToString:@""]&&
        ![newTextField.text isEqualToString:@""]&&
        ![confirmTextField.text isEqualToString:@""])&&([newTextField.text isEqualToString:confirmTextField.text])) {
        submitBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        submitBtn.enabled=YES;
    }else{
        submitBtn.backgroundColor=[UIColor lightGrayColor];
        submitBtn.enabled=NO;
    }
}

#pragma mark - 点击修改密码
-(void)clickBtn{
    [self requestHttpsForOrgPwd:ognTextField.text newPwd:newTextField.text confirmPwd:confirmTextField.text];
}

#pragma mark Request 请求
-(void)requestHttpsForOrgPwd:(NSString *)orgPwd newPwd:(NSString *)newPwd confirmPwd:(NSString *)confirmPwd{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"act_edit_password" forKey:@"act"];
    [dict setObject:orgPwd forKey:@"old_password"];
    [dict setObject:newPwd forKey:@"new_password"];
    [dict setObject:confirmPwd forKey:@"comfirm_password"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrgPwd:ognTextField.text newPwd:newTextField.text confirmPwd:confirmTextField.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                /*******
                 1、把修改密码覆盖到保存密码，以方便自动登录
                 2、pop到上级Controller
                 *******/
                [AppUtils showSuccessMessage:@"修改密码成功" inView:self.view];
                // 延迟加载
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AppUtils saveValue:newPwd forKey:User_Pass];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
