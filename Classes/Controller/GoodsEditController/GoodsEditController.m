//
//  GoodsEditController.m
//  MMG
//
//  Created by mac on 16/8/23.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "GoodsEditController.h"
#import "GoodsAddedFirstCell.h"
#import "NewGoodsAddedSecondCell.h"
#import "NewGoodsAddedSecondBtmCell.h"
#import "NewGoodsAddedThreeTopCell.h"
#import "PictureCell.h"
#import "GoodsEditBtmCell.h"
#import "GoodsAddTypeCell.h"
#import "CustomPickerView.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TZImagePickerController.h"
#import "TZImageManager.h"
#import <IDMPhotoBrowser.h>
#import "Macro2.h"
@interface GoodsEditController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,NewAddGoodsFirstDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GoodsTypeDelegate,GoodsEditBottomDelegate,NewGoodsAddDelegate>

@end

@implementation GoodsEditController{
    
    NSMutableArray *_selectedAssets;
    
    UICollectionView *myCollectionView;
    
    NSMutableArray *fenleiArr;          //分类Array
    NSMutableArray *erjFlArr;           //二级分类Array
    NSMutableArray *branchArr;          //商品品牌Array
    NSMutableArray *goodTypeArr;        //商品类型
    NSMutableArray *paramArr;           //商品品牌Array
    NSMutableArray *useInfoArr;         //使用情况
    
    NSString *seleFenleiId;
    NSString *seleErjiFlId;             //提交的分类ID
    NSString *seleBranceId;             //提交的品牌ID
    NSString *seleuseInfoId;            //提交的使用情况ID
    NSString *ershouIdTag;              //标识二手选择的
    NSString *seleGoodTypeId;           //提交的商品类型ID
    NSString *selePraemItemId;
    NSString *subGoodsName;             //提交的商品名称
    NSString *subHopePrice;             //提交的期望价格
    NSString *subComment;               //提交的备注说明
    NSMutableArray *subParamIDArr;      //提交属性ID数组
    NSMutableArray *subParamValueArr;   //提交属性值数组
    
    NSString *recorParamID;
    NSMutableArray *recorParamArr;      //记录的Param属性
    
    NSMutableArray *upLoadImgArr;       //
}



-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"商品编辑";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self seNavBar];
//    [self setCustomNavigationTitle:@"商品编辑"];
    
    [self setUpUI];
    
    [self editGoodsInfoRequest:self.goodsID];
}

-(void)setUpUI{
    
    subComment=@"";
    
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    myCollectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    myCollectionView.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [myCollectionView registerClass:[GoodsAddedFirstCell class] forCellWithReuseIdentifier:@"GoodsAddedFirstCell"];
    [myCollectionView registerClass:[GoodsAddTypeCell class] forCellWithReuseIdentifier:@"GoodsAddTypeCell"];
    [myCollectionView registerClass:[NewGoodsAddedSecondCell class] forCellWithReuseIdentifier:@"NewGoodsAddedSecondCell"];
    [myCollectionView registerClass:[NewGoodsAddedSecondBtmCell class] forCellWithReuseIdentifier:@"NewGoodsAddedSecondBtmCell"];
    [myCollectionView registerClass:[NewGoodsAddedThreeTopCell class] forCellWithReuseIdentifier:@"NewGoodsAddedThreeTopCell"];
    [myCollectionView registerClass:[PictureCell class] forCellWithReuseIdentifier:@"PictureCell"];
    [myCollectionView registerClass:[GoodsEditBtmCell class] forCellWithReuseIdentifier:@"GoodsEditBtmCell"];
    myCollectionView.dataSource=self;
    myCollectionView.delegate=self;
    [self.view addSubview:myCollectionView];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 7;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:case 1:case 3:case 4:case 6:
            return 1;
            break;
        case 2:
            return paramArr.count;
            break;
        default:
            return upLoadImgArr.count+1;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        GoodsAddedFirstCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsAddedFirstCell" forIndexPath:indexPath];
        cell.delegate=self;
        cell.GoodsNameBlock=^(NSString *tfText){
            subGoodsName=tfText;
        };
        return cell;
    }else if (indexPath.section==1) {
        GoodsAddTypeCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsAddTypeCell" forIndexPath:indexPath];
        cell.delegate=self;
        return cell;
    }else if (indexPath.section==2) {
        NewGoodsAddedSecondCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedSecondCell" forIndexPath:indexPath];
        if(indexPath.row<paramArr.count){
            SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
            cell.model=model;
            cell.delegate=self;
            cell.nameTF.tag=indexPath.row;
        }
        return cell;
    }else if (indexPath.section==3){
        NewGoodsAddedSecondBtmCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedSecondBtmCell" forIndexPath:indexPath];
        cell.hopePriceBlock=^(NSString *tfText){
            subHopePrice=tfText;
        };
        cell.commentTextBlock=^(NSString *tfText){
            subComment=tfText;
        };
        return cell;
    }else if (indexPath.section==4){
        NewGoodsAddedThreeTopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedThreeTopCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewGoodsAddedThreeTopCell alloc]init];
        }
        return cell;
    }else if (indexPath.section==5){
        PictureCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PictureCell" forIndexPath:indexPath];
        if (indexPath.row!=upLoadImgArr.count) {
            UIImage *img=upLoadImgArr[indexPath.row];
            cell.imageView.image=img;
            cell.deleteBtn.hidden = NO;
        }else{
            cell.imageView.image = [UIImage imageNamed:@"shangjia_sc_imgv"];
            cell.deleteBtn.hidden = YES;
        }
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        GoodsEditBtmCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsEditBtmCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[GoodsEditBtmCell alloc]init];
        }
        cell.delegate=self;
        cell.isGoodsManageer=_isGoodsManageer;
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            return CGSizeMake(SCREENSIZE.width, 338);
        }
            break;
        case 1:
            return CGSizeMake(SCREENSIZE.width, 58);
            break;
        case 2:
            return CGSizeMake(SCREENSIZE.width, 58);
            break;
        case 3:
        {
            return CGSizeMake(SCREENSIZE.width, 232);
        }
            break;
        case 4:
            return CGSizeMake(SCREENSIZE.width, 72);
            break;
        case 5:
            return CGSizeMake((SCREENSIZE.width)/3-12, SCREENSIZE.width/3-12);
            break;
        default:
        {
            return CGSizeMake(SCREENSIZE.width, 148);
        }
            break;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==1||section==2||section==4) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else if(section==5){
        return UIEdgeInsetsMake(6, 6, 6, 6);
    }else{
        return UIEdgeInsetsMake(0, 0, 18, 0);
    }
}

//定义每个item间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==5) {
        return 6;
    }else{
        return 0;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==2) {
        /*
         重要方法
         */
        SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
        NSLog(@"%@--%@--%@",model.seleTag,model.typeName,model.attr_value);
        if ([model.input_type isEqualToString:@"1"]) {
            CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:model.paramArr_Item selectedTypeId:model.seleTag];
            [pick showPickerViewCompletion:^(id selectedObject) {
                SubmitOrder_SeletedTypeModel *seleModel=selectedObject;
                model.seleTag=[NSString stringWithFormat:@"%@",seleModel.typeId];
                model.typeName=seleModel.typeName;
                model.attr_value=seleModel.typeName;
                model.input_type=@"1";
                
                //
                
                [paramArr replaceObjectAtIndex:indexPath.row withObject:model];
                
                //刷新行
                [myCollectionView reloadItemsAtIndexPaths:@[indexPath]];
            }];
        }
        
    }else if (indexPath.section==5){
        if (indexPath.row==upLoadImgArr.count) {
            [[MMPopupWindow sharedWindow] cacheWindow];
            [MMPopupWindow sharedWindow].touchWildToHide = YES;
            
            MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
            sheetConfig.buttonFontSize=15;
            sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
            
            MMPopupItemHandler block = ^(NSInteger index){
                switch (index) {
                    case 0:
                        [self snapImage];
                        break;
                    case 1:
                        [self pickImage];
                        break;
                    default:
                        break;
                }
            };
            
            MMPopupBlock completeBlock = ^(MMPopupView *popupView){
                
            };
            NSArray *items =
            @[MMItemMake(@"拍照", MMItemTypeNormal, block),
              MMItemMake(@"从相册选择", MMItemTypeNormal, block)];
            
            [[[MMSheetView alloc] initWithTitle:nil
                                          items:items] showWithBlock:completeBlock];
        }else{
            UIImage *img=upLoadImgArr[indexPath.row];
            NSArray *photo=[IDMPhoto photosWithImages:@[img]];
            IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photo];
            browser.displayToolbar=NO;
            browser.displayCounterLabel = YES;
            browser.displayActionButton = NO;
            
            // Show
            [self presentViewController:browser animated:YES completion:nil];
        }
    }
}

#pragma mark - 点击图片删除按钮
- (void)deleteBtnClik:(UIButton *)sender {
    [upLoadImgArr removeObjectAtIndex:sender.tag];
    //    [_selectedAssets removeObjectAtIndex:sender.tag];
    
    [myCollectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:5];
        [myCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [myCollectionView reloadData];
    }];
}

#pragma mark - 上架商品第一步 Delegate
//点击分类
-(void)clickFenlei{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:fenleiArr selectedTypeId:seleFenleiId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        
        [erjFlArr removeAllObjects];
        
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleFenleiId=model.typeId;
        //默认二级分类
        NSArray *ejflArr=model.ejFlArr;
        if (ejflArr.count!=0) {
            for (NSDictionary *dic in ejflArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getFenleiJsonDataForDictionary:dic];
                [erjFlArr addObject:model];
            }
        }else{
            SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
            model.typeId=@"0";
            model.typeName=@"0";
            [erjFlArr addObject:model];
        }
        SubmitOrder_SeletedTypeModel *erjflModel=erjFlArr[0];
        seleErjiFlId=erjflModel.typeId;
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:erjflModel];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Fenlei_Notification object:model];
    }];
}

//点击二级分类
-(void)clickErjiFenlei{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:erjFlArr selectedTypeId:seleErjiFlId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleErjiFlId=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:model];
    }];
}

//点击商品品牌
-(void)clickPinpai{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:branchArr selectedTypeId:seleBranceId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleBranceId=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Brance_Notification object:model];
    }];
}

//点击二手列表
-(void)clickErShouList{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:useInfoArr selectedTypeId:ershouIdTag];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleuseInfoId=[NSString stringWithFormat:@"%@",model.typeId];
        ershouIdTag=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_UseInfo_Notification object:model];
    }];
}

//点击全新按钮
-(void)hadClickNewBtn{
    seleuseInfoId=@"1";
}

//点击二手按钮
-(void)hadClickErShouBtn{
    seleuseInfoId=ershouIdTag;
}

//点击商品类型
-(void)clickGoodsType{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:goodTypeArr selectedTypeId:seleGoodTypeId];
    
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleGoodTypeId=[NSString stringWithFormat:@"%@",model.typeId];
        [self getParamRequest:seleGoodTypeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_GoodType_Notification object:model];
    }];
}

-(void)setNameTextfield:(NSString *)nameStr indexPath:(NSIndexPath *)indexPath{
    SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
    model.attr_value=nameStr;
    model.input_type=@"0";
    [paramArr replaceObjectAtIndex:indexPath.row withObject:model];
    
    if (indexPath!=nil) {
        [myCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    }
}

//点击保存按钮
-(void)clickSaveBtn{
    if (subGoodsName.length==0) {
        [AppUtils showSuccessMessage:@"请填写商品名称" inView:self.view];
    }else if([seleGoodTypeId isEqualToString:@""]||seleGoodTypeId==nil){
        [AppUtils showSuccessMessage:@"请选择商品类型" inView:self.view];
    }else{
        [subParamIDArr removeAllObjects];
        [subParamValueArr removeAllObjects];
        for (SubmitOrder_SeletedTypeModel *model in paramArr) {
            [subParamIDArr addObject:model.paramArr_ItemId];
            if(model.attr_value==nil||[model.attr_value isEqualToString:@""]){
                [subParamValueArr addObject:@""];
            }else{
                [subParamValueArr addObject:model.attr_value];
            }
        }
        
        //
        if(subHopePrice.length==0){
            [AppUtils showSuccessMessage:@"请填写期望价格" inView:self.view];
        }else if(subComment.length==0){
            [AppUtils showSuccessMessage:@"请填写备注说明" inView:self.view];
        }else{
            [self submitRequestWithAct:@"save_sj_goods"];
        }
    }
}

//点击保存并上架按钮
-(void)clickSaveAndOffBtn{
     
    if (subGoodsName.length==0) {
        [AppUtils showSuccessMessage:@"请填写商品名称" inView:self.view];
    }else if([seleGoodTypeId isEqualToString:@""]||seleGoodTypeId==nil){
        [AppUtils showSuccessMessage:@"请选择商品类型" inView:self.view];
    }else{
        
        
        
        [subParamIDArr removeAllObjects];
        [subParamValueArr removeAllObjects];
        for (SubmitOrder_SeletedTypeModel *model in paramArr) {
            [subParamIDArr addObject:model.paramArr_ItemId];
            if(model.attr_value==nil||[model.attr_value isEqualToString:@""]){
                [subParamValueArr addObject:@""];
            }else{
                [subParamValueArr addObject:model.attr_value];
            }
        }
        
        //
        if(subHopePrice.length==0){
            [AppUtils showSuccessMessage:@"请填写期望价格" inView:self.view];
        }else if(upLoadImgArr.count==0){
            [AppUtils showSuccessMessage:@"至少选择一张图片" inView:self.view];
        }else{
            [self submitRequestWithAct:@"save_goods"];
        }
    }
}

#pragma mark - 拍照
- (void)snapImage{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"请在iPhone的“设置-隐私-相机”选项中，允许猫猫购商户版访问您的相机" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.videoQuality = UIImagePickerControllerQualityTypeLow;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark UIImagePickerController 拍照选择照片后回调 delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:@"public.image"]) {
        TZImagePickerController *pickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:30 delegate:nil];
        pickerVc.sortAscendingByModificationDate = YES;//拍照按拍摄时间排序
        [pickerVc showProgressHUD];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        //保存图片，获取到asset
        [[TZImageManager manager] savePhotoWithImage:image completion:^{
            [[TZImageManager manager] getCameraRollAlbum:NO allowPickingImage:YES completion:^(TZAlbumModel *model) {
                [[TZImageManager manager] getAssetsFromFetchResult:model.result allowPickingVideo:NO allowPickingImage:YES completion:^(NSArray<TZAssetModel *> *models) {
                    [pickerVc hideProgressHUD];
                    TZAssetModel *assetModel = [models firstObject];
                    if (pickerVc.sortAscendingByModificationDate) {
                        assetModel = [models lastObject];
                    }
                    [_selectedAssets addObject:assetModel.asset];
                    [upLoadImgArr addObject:image];
                    [myCollectionView reloadData];
                }];
            }];
        }];
    }
}

#pragma mark - 从相册选择

- (void) pickImage{
    TZImagePickerController *pickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:30 delegate:nil];
    pickerVc.navigationBar.barTintColor = [UIColor colorWithHexString:@"#282828"];
    pickerVc.navigationBar.tintColor = [UIColor colorWithHexString:@"#282828"];//旁边按钮颜色
    pickerVc.barItemTextFont=[UIFont systemFontOfSize:17];
    NSDictionary * dict = [NSDictionary dictionaryWithObject:[UIColor colorWithHexString:@"#282828"] forKey:NSForegroundColorAttributeName];
    pickerVc.navigationBar.titleTextAttributes = dict;
    
#pragma mark 四类个性化设置，这些参数都可以不传，此时会走默认设置
    pickerVc.isSelectOriginalPhoto = YES;    //原图
    
    // 1.如果你需要将拍照按钮放在外面，不要传这个参数
    pickerVc.selectedAssets = _selectedAssets; // optional, 可选的
    pickerVc.allowTakePicture = NO;         // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    pickerVc.navigationBar.barTintColor = [UIColor colorWithHexString:@"#ec6b00"];
    pickerVc.oKButtonTitleColorDisabled = [UIColor colorWithHexString:@"#ec6b00"];
    pickerVc.oKButtonTitleColorNormal = [UIColor colorWithHexString:@"#ec6b00"];
    
    // 3. 设置是否可以选择视频/图片/原图
    pickerVc.allowPickingVideo = NO;
    pickerVc.allowPickingImage = YES;
    pickerVc.allowPickingOriginalPhoto = YES;
    
    // 4. 照片排列按修改时间升序
    pickerVc.sortAscendingByModificationDate = YES;
    
    // 你可以通过block或者代理，来得到用户选择的照片.
    [pickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [upLoadImgArr addObjectsFromArray:photos];
        _selectedAssets = [NSMutableArray arrayWithArray:assets];
        [myCollectionView reloadData];
    }];
    
    [self presentViewController:pickerVc animated:YES completion:nil];
}

#pragma mark - Request 提交请求

-(void)submitRequestWithAct:(NSString *)act{
    [AppUtils showProgressInView:self.view];
    NSMutableArray *imgs=[NSMutableArray array];
    for (UIImage *img in upLoadImgArr) {
        NSData *imageData = UIImageJPEGRepresentation(img, 0.2);
        NSMutableData *pictrueData=[NSMutableData data];
        [pictrueData appendData:imageData];
        [imgs addObject:pictrueData];
    }
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
        NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":act,
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr=@"",
                         @"yx_phone":pSr!=nil?pSr=@"",
                         @"goods_id":self.goodsID,
                         @"goods_name":subGoodsName,
                         @"cat_id":seleErjiFlId,
                         @"brand_id":seleBranceId,
                         @"level":seleuseInfoId,
                         @"jiesuan_price":subHopePrice,
                         @"explain":subComment,
                         @"attr_id_list":subParamIDArr,
                         @"attr_value_list":subParamValueArr,
                         @"goods_type":seleGoodTypeId,
                         @"goods_img":imgs
                         };

    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@edit_goods.php?",ImageHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"商品编辑成功" inView:self.view];
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:_isGoodsManageer?@"manager_eidt_success":@"xiajia_eidt_success" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@-%@",error,error.localizedFailureReason);
    }];
}

//
-(void)editGoodsInfoRequest:(NSString *)goodsId{
    
    fenleiArr=[NSMutableArray array];
    erjFlArr=[NSMutableArray array];
    branchArr=[NSMutableArray array];
    useInfoArr=[NSMutableArray array];
    goodTypeArr=[NSMutableArray array];
    paramArr=[NSMutableArray array];
    recorParamArr=[NSMutableArray array];
    upLoadImgArr=[NSMutableArray array];
    subParamIDArr=[NSMutableArray array];
    subParamValueArr=[NSMutableArray array];
    
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"edit_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@""forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@edit_goods.php?",ImageHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //商品名
            subGoodsName=responseObject[@"retData"][@"goods_info"][@"goods_name"];
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_GoodName_Notification object:subGoodsName];
            
            //获取大分类属性
            NSArray *flArr=responseObject[@"retData"][@"cate_list"];
            for (NSDictionary *dic in flArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getFenleiJsonDataForDictionary:dic];
                [fenleiArr addObject:model];
            }
            SubmitOrder_SeletedTypeModel *flmodel=[SubmitOrder_SeletedTypeModel new];
            flmodel.typeId=responseObject[@"retData"][@"goods_info"][@"parent"][@"parent_id"];
            seleFenleiId=flmodel.typeId;
            flmodel.typeName=responseObject[@"retData"][@"goods_info"][@"parent"][@"parent_name"];
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Fenlei_Notification object:flmodel];
            
            //二级分类
            NSArray *ejflArr=responseObject[@"retData"][@"goods_info"][@"brothers"];
            for (NSDictionary *dic in ejflArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getErjiFenleiForDictionary:dic];
                [erjFlArr addObject:model];
            }
            seleErjiFlId=responseObject[@"retData"][@"goods_info"][@"cat_id"];
            for (SubmitOrder_SeletedTypeModel *model in erjFlArr) {
                if ([model.typeId isEqualToString:seleErjiFlId]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:model];
                    break;
                }
            }
            
            //获取品牌属性
            NSArray *ppArr=responseObject[@"retData"][@"brand_list"];
            for (NSDictionary *dic in ppArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getBranceJsonDataForDictionary:dic];
                [branchArr addObject:model];
            }
            seleBranceId=responseObject[@"retData"][@"goods_info"][@"brand_id"];
            for (SubmitOrder_SeletedTypeModel *model in branchArr) {
                if ([model.typeId isEqualToString:seleBranceId]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Brance_Notification object:model];
                    break;
                }
            }
            
            //使用情况
            seleuseInfoId=responseObject[@"retData"][@"goods_info"][@"level"];
            if ([seleuseInfoId isEqualToString:@"1"]) {
                ershouIdTag=@"2";
            }else{
                ershouIdTag=seleuseInfoId;
            }
            
            for (int i=2; i<=6; i++) {
                NSString *nameStr;
                switch (i) {
                    case 2:
                        nameStr=@"99新";
                        break;
                    case 3:
                        nameStr=@"95新";
                        break;
                    case 4:
                        nameStr=@"9成新";
                        break;
                    case 5:
                        nameStr=@"85新";
                        break;
                    default:
                        nameStr=@"8成新";
                        break;
                }
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                model.typeId=[NSString stringWithFormat:@"%d",i];
                model.typeName=nameStr;
                [useInfoArr addObject:model];
            }
            /*
             for (int i=2; i<=13; i++) {
             NSString *nameStr;
             switch (i) {
             case 2:
             nameStr=@"99新";
             break;
             case 3:
             nameStr=@"98新";
             break;
             case 4:
             nameStr=@"97新";
             break;
             case 5:
             nameStr=@"96新";
             break;
             case 6:
             nameStr=@"95新";
             break;
             case 7:
             nameStr=@"94新";
             break;
             case 8:
             nameStr=@"93新";
             break;
             case 9:
             nameStr=@"92新";
             break;
             case 10:
             nameStr=@"91新";
             break;
             case 11:
             nameStr=@"9成新";
             break;
             case 12:
             nameStr=@"85新";
             break;
             case 13:
             nameStr=@"8成新";
             break;
             default:
             nameStr=@"8成新";
             break;
             }
             SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
             model.typeId=[NSString stringWithFormat:@"%d",i];
             model.typeName=nameStr;
             [useInfoArr addObject:model];
             }
             */
            
            SubmitOrder_SeletedTypeModel *useInfoModel=[SubmitOrder_SeletedTypeModel new];
            for (SubmitOrder_SeletedTypeModel *model in useInfoArr) {
                if ([seleuseInfoId isEqualToString:model.typeId]) {
                    useInfoModel=model;
                    break;
                }else{
                    useInfoModel.typeId=@"1";
                    useInfoModel.typeName=@"99新";
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_UseInfo_Notification object:useInfoModel];
            
            //获取商品类型
            NSArray *lxArr=responseObject[@"retData"][@"goods_type_list"];
            for (NSDictionary *dic in lxArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getGoodTypeDataForDictionary:dic];
                [goodTypeArr addObject:model];
            }
            seleGoodTypeId=responseObject[@"retData"][@"goods_info"][@"goods_type"];
            recorParamID=seleGoodTypeId;
            for (SubmitOrder_SeletedTypeModel *model in goodTypeArr) {
                if ([model.typeId isEqualToString:seleGoodTypeId]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_GoodType_Notification object:model];
                    break;
                }
            }
            
            //获取Param属性
            if (![seleGoodTypeId isEqualToString:@"0"]) {
                NSArray *prArr=responseObject[@"retData"][@"goods_info"][@"att_list"];
                for (NSDictionary *dic in prArr) {
                    SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                    [model getParamDataForDictionary:dic];
                    [paramArr addObject:model];
                    [recorParamArr addObject:model];
                }
            }
            for (SubmitOrder_SeletedTypeModel *model in paramArr) {
                [subParamIDArr addObject:model.paramArr_ItemId];
                [subParamValueArr addObject:model.attr_value];
            }
            
            //期望价格
            subHopePrice=responseObject[@"retData"][@"goods_info"][@"jiesuan_price"];
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_HopePrice_Notification object:subHopePrice];
            
            //备注说明
            subComment=responseObject[@"retData"][@"goods_info"][@"explain"];
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Comment_Notification object:subComment];
            
            //获取图片
            NSArray *ulImgArr=responseObject[@"retData"][@"goods_info"][@"goods_gallery"];
            for (NSDictionary *dic in ulImgArr) {
                NSString *str=[NSString stringWithFormat:@"%@%@",ImageHost01,dic[@"img_url"]];
                NSData *reData=[[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:str]];
                UIImage *img = [UIImage imageWithData:reData];
                [upLoadImgArr addObject:img];
            }
        }
        [myCollectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

-(void)getParamRequest:(NSString *)goodTypeId{
    [paramArr removeAllObjects];
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
        NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"by_cate_attr",
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"goods_type":goodTypeId
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@edit_goods.php?",ImageHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if (![goodTypeId isEqualToString:recorParamID]) {
            NSArray *prArr=responseObject[@"retData"];
            for (NSDictionary *dic in prArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getParamDataForDictionary:dic];
                [paramArr addObject:model];
            }
        }else{
            paramArr=[NSMutableArray arrayWithArray:recorParamArr];
        }
        [UIView animateWithDuration:0.000000001 animations:^{
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:2];
            [myCollectionView reloadSections:indexSet];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
