//
//  PayFinishController.h
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface PayFinishController : BaseController

@property (nonatomic,copy) NSString *payPrice;
@property (nonatomic,copy) NSString *orderNum;
@property (nonatomic,copy) NSString *goodName;

@end
