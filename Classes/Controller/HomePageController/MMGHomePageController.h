//
//  MMGHomePageController.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"
#import "MMGHomePageController.h"
@interface MMGHomePageController : BaseController

@end

@interface BottomPage01 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end

@interface BottomPage02 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end

@interface BottomPage03 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end

@interface BottomPage04 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end

@interface BottomPage05 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end

@interface BottomPage06 : BaseController

@property (nonatomic,copy) NSString *idStr;

@end
