//
//  MMGHomePageController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "MMGHomePageController.h"
#import "MMGSDCycleScrollView.h"
#import "AdvDetailController.h"
#import "MMGHomePageCategoryCell.h"
#import "HomePageGoodCell.h"
#import "HomePageGoodModel.h"
#import "HomePageBottomCell.h"
#import "ProductListController.h"
#import "ProductDetailController.h"
#import "SellerShowViewController.h"
#import "TopicController.h"
#import "StageAdvController.h"
#import "HomepageHoriCell.h"
#import "ShopController.h"
#import "MMGSign.h"
#import "HttpRequests.h"
#import "Macro2.h"
#import "YHT_NJKWebViewProgress.h"
#import "YHT_NJKWebViewProgressView.h"
#import "AdvDetailCell.h"
#import "AppDelegate.h"
#import "Macro2.h"
//#define IMAGEPLAY_HEIHGT SCREENSIZE.height/3-28
//#define CATEGORY_HEIHGT (SCREENSIZE.width-60)/4.0*2+24
@interface MMGHomePageController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,SDCycleScrollViewDelegates,HomePageGoodDelegate,ClickBusinessDelegate>

@end

@implementation MMGHomePageController{
    
    UITextField *searchTextField;
    
    NSMutableArray *advImageArr;//广告Array
    MMGSDCycleScrollView *cycleScrollView;
    UIView *searchViw;
    NSMutableArray *categoryArr;//快捷分类Array
    UICollectionView *categoryCollectionView;//快捷分类view
    NSMutableArray *businessArr;
    NSMutableArray *goodBigImgArr;    //商品大图
    NSMutableArray *goodSmallImgArr01;//商品小图1
    NSMutableArray *goodSmallImgArr02;//商品小图2
    UITableView *goodTableViwe;
    NSMutableArray *tableFootArr;
    UICollectionView *footColleView;
    UINavigationController *CustomNavigationController;
    UINavigationBar *cusBar;

}




-(void)seNavBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
        UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"游侠商城";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self seNavBar];
    
            [self setUpSearchView];
    
            [self setUpCategoryView];
    
            [self setUpTableGoodView];
    
            [self requestHomePageHttps];
   
}

-(void)fanhui
{
    
    [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
    
//    [self.tabBarController.navigationController.navigationBar setBarTintColor:[UIColor blueColor]];
//    HomePageController *home=[HomePageController new];
//    home.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:home animated:YES];
}

#pragma mark - init view
//搜索的View
-(void)setUpSearchView{
    searchViw=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 44)];
    [self.view addSubview:searchViw];
    
    //搜索处猫猫购logo
    UIImageView *searchLogo=[[UIImageView alloc]initWithFrame:CGRectMake(8, 9, 70, 32)];
    [searchViw addSubview:searchLogo];
    searchLogo.contentMode=UIViewContentModeScaleAspectFit;
    [searchLogo setImage:[UIImage imageNamed:@"youxialogo2"]];
    
    UIView *insideSearchView=[[UIView alloc]initWithFrame:CGRectMake(searchLogo.frame.origin.x+searchLogo.frame.size.width+8,4, SCREENSIZE.width-searchLogo.frame.size.width-24, 36)];
    insideSearchView.layer.borderWidth=.8;
    insideSearchView.layer.cornerRadius=1;
    insideSearchView.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    [searchViw addSubview:insideSearchView];
    
    //searBtn
    UIButton *searBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    searBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [searBtn setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    searBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [insideSearchView addSubview:searBtn];
    searBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* searBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[searBtn(40)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searBtn,searBtn)];
    [NSLayoutConstraint activateConstraints:searBtn_h];
    
    NSArray* searBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searBtn]-0.5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searBtn)];
    [NSLayoutConstraint activateConstraints:searBtn_w];
    [searBtn addTarget:self action:@selector(clickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //searchTextField
    searchTextField=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, insideSearchView.frame.size.width-50, insideSearchView.frame.size.height)];
    searchTextField.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    searchTextField.placeholder=@"Birkin";
    searchTextField.font=[UIFont systemFontOfSize:14];
    searchTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    [insideSearchView addSubview:searchTextField];
}

#pragma mark - 点击搜索按钮
-(void)clickSearchBtn{
    if ([searchTextField.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入搜索内容" inView:self.view];
    }else{
        ProductListController *prodContr=[[ProductListController alloc]init];
        prodContr.requestType=HomePageSearchRequestType;
        prodContr.searchKeyWord=searchTextField.text;
        prodContr.productListName=searchTextField.text;
        prodContr.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:prodContr animated:YES];
    }
    [AppUtils closeKeyboard];
}

#pragma mark - 设置滚动图片
-(MMGSDCycleScrollView *)addImageViewDisPlayView{
    
    //-----------------
    cycleScrollView = [[MMGSDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, IMAGEPLAY_HEIHGT-64)];
    
    cycleScrollView.autoScrollTimeInterval=6;//设置滚动间隔
    cycleScrollView.infiniteLoop = YES;
    cycleScrollView.delegate = self;
    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimateds;
    [self.view addSubview:cycleScrollView];
    //-----------------
    return cycleScrollView;
    
}

#pragma mark - SDCycleScrollView 点击图片


- (void)cycleScrollView:(MMGSDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSString *)index andAdvType:(NSString *)type
{
    

    AdvDetailController *advCtl=[[AdvDetailController alloc]init];
    advCtl.hidesBottomBarWhenPushed=YES;
    
    ProductListController *productList=[[ProductListController alloc]init];
    productList.hidesBottomBarWhenPushed=YES;
    
    TopicController *topicCon=[[TopicController alloc]init];
    topicCon.hidesBottomBarWhenPushed=YES;
    
    NSRange range=[index rangeOfString:@"id="];
    
    NSString *topicConId;
    
    if (range.location!=NSNotFound) {
        topicConId = [index substringFromIndex:range.location+range.length];
    }
    
    //带有推荐商品的广告
    if ([type isEqualToString:@"topic"]) {
//        topicCon.idStr=topicConId;
//        [self.navigationController pushViewController:topicCon animated:YES];
    }
    //纯广告
    else if ([type isEqualToString:@"html"]){
        [self.navigationController pushViewController:advCtl animated:YES];
    }
    //跳转到商品列表页
    else{
        productList.productListId=index;
        productList.productListName=@"推荐商品";
        productList.requestType=HomePageCategoryRequestType;
        [self.navigationController pushViewController:productList animated:YES];
    }
}

#pragma mark - init UI

//商品类别选择的View
-(void)setUpCategoryView{
    /*
     NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"HomePageCategoryArray" ofType:@"plist"];
     categoryArr = [[NSArray alloc] initWithContentsOfFile:plistPath];*/
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    categoryCollectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(10, IMAGEPLAY_HEIHGT-52, SCREENSIZE.width-20, CATEGORY_HEIHGT) collectionViewLayout:myLayout];
    [categoryCollectionView registerNib:[UINib nibWithNibName:@"MMGHomePageCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"collcell"];
    categoryCollectionView.backgroundColor=[UIColor clearColor];
    categoryCollectionView.dataSource=self;
    categoryCollectionView.delegate=self;
}

-(UIView *)setUpHeaderView{
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, IMAGEPLAY_HEIHGT+CATEGORY_HEIHGT-25)];
    headView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    [headView addSubview:[self addImageViewDisPlayView]];
    [headView addSubview:categoryCollectionView];
    return headView;
}

//整个TableView
-(void)setUpTableGoodView{
    
    goodTableViwe=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-157) style:UITableViewStyleGrouped];
    goodTableViwe.separatorStyle=UITableViewCellSeparatorStyleNone;
    goodTableViwe.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];

    goodTableViwe.dataSource=self;
    goodTableViwe.delegate=self;
    [goodTableViwe registerNib:[UINib nibWithNibName:@"HomePageGoodCell" bundle:nil] forCellReuseIdentifier:@"goodCellId"];
    [self.view addSubview:goodTableViwe];
    
    /*
     *下拉刷新
     */
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestHomePageHttps];
        });
    }];
    header.arrowView.hidden=NO;
    
    [header setMj_h:52];//设置高度
    [header setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    // 设置字体
    [header.stateLabel setMj_y:23];
    header.stateLabel.font = [UIFont systemFontOfSize:13];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:13];
    goodTableViwe.mj_header = header;
    

    //设置背景图片
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-113)];
    [imgv setImage:[UIImage imageNamed:@"login_baground"]];
    goodTableViwe.backgroundView=imgv;
    
    goodTableViwe.tableHeaderView=[self setUpHeaderView];
    
    //goodTableViwe.tableFooterView=[self setUpFooterView];
}

#pragma mark - Request 请求
-(void)requestHomePageHttps{
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        advImageArr=[NSMutableArray array];
        
        categoryArr=[NSMutableArray array];
        
        businessArr=[NSMutableArray array];
        
        goodBigImgArr=[NSMutableArray array];
        
        goodSmallImgArr01=[NSMutableArray array];
        
        goodSmallImgArr02=[NSMutableArray array];
        
        tableFootArr=[NSMutableArray array];
        
        //时间戳
        time_t now;
        time(&now);
        NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
        NSString *nonce_str	= [MMGSign randomNumber];
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        [dict setObject:@"1" forKey:@"is_phone"];
        [dict setObject:time_stamp forKey:@"stamp"];
        [dict setObject:nonce_str forKey:@"noncestr"];
      
        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
        NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
        [dict setObject:sign forKey:@"sign"];
        
        [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"index.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"%@------------",responseObject);
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
            if([codeStr isEqualToString:@"100"]){
                [self requestHttpsSignKey];
            }else{
                //广告
                for (NSDictionary *arr in responseObject[@"retData"][@"slider"]) {
                    [advImageArr addObject:arr];
                }
                [cycleScrollView setImageURLStringsGroup:advImageArr];
                
                //分类
                for (NSDictionary *categoryDic in responseObject[@"retData"][@"menu"]) {
                    MMGHomePageCagegoryModel *model=[[MMGHomePageCagegoryModel alloc]init];
                    [model jsonDataForDictionary:categoryDic];
                    [categoryArr addObject:model];
                }
                [categoryCollectionView reloadData];
                
                //商家
                NSArray *bsArr=responseObject[@"retData"][@"business"];
                for (NSDictionary *dic in bsArr) {
                    MMGHomePageCagegoryModel *model=[[MMGHomePageCagegoryModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [businessArr insertObject:model atIndex:0];
                }
                if (businessArr.count<=1) {
                    for (int i=0; i<3; i++) {
                        MMGHomePageCagegoryModel *model=[[MMGHomePageCagegoryModel alloc]init];
                        model.busId=@"noid";
                        model.name=@"即将入驻";
                        model.img=[NSString stringWithFormat:@"sj_t0%d",i+1];
                        [businessArr insertObject:model atIndex:0];
                    }
                }else if (businessArr.count<=2){
                    for (int i=0; i<2; i++) {
                        MMGHomePageCagegoryModel *model=[[MMGHomePageCagegoryModel alloc]init];
                        model.busId=@"noid";
                        model.name=@"即将入驻";
                        model.img=[NSString stringWithFormat:@"sj_t0%d",i+1];
                        [businessArr insertObject:model atIndex:0];
                    }
                }else if (businessArr.count<=3){
                    MMGHomePageCagegoryModel *model=[[MMGHomePageCagegoryModel alloc]init];
                    model.busId=@"noid";
                    model.name=@"即将入驻";
                    model.img=@"sj_t02";
                    [businessArr insertObject:model atIndex:0];
                }
                
                //商品
                for (NSDictionary *dic in responseObject[@"retData"][@"goods"]) {
                    HomePageGoodModel *hpGoodModel=[[HomePageGoodModel alloc]init];
                    [hpGoodModel getJsonDataFromDictionary:dic];
                    [goodBigImgArr addObject:hpGoodModel];
                }
                [goodTableViwe reloadData];
                
                //底部广告
                NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"HomePageBottom" ofType:@"plist"];
                NSArray *btmArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
                for (NSDictionary *btmDic in btmArr) {
                    HomePageBottomModel *model=[[HomePageBottomModel alloc]init];
                    [model getJsonDataFromDictionary:btmDic];
                    [tableFootArr addObject:model];
                }
                [footColleView reloadData];
                [goodTableViwe.mj_header endRefreshing];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"%@",error);
            [goodTableViwe.mj_header endRefreshing];
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:User_ID forKey:@"yx_uid"];
//    [dict setObject:User_Name forKey:@"yx_uname"];
//    [dict setObject:User_Phone forKey:@"yx_phone"];
    
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@-------------",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
//            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
            [self requestHomePageHttps];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - UICollectionView delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView==categoryCollectionView) {
        return categoryArr.count;
    }else{
        return tableFootArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==categoryCollectionView) {
        MMGHomePageCategoryCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"collcell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[MMGHomePageCategoryCell alloc]init];
        }
        MMGHomePageCagegoryModel *model=categoryArr[indexPath.row];
        [cell reflushDataForModel:model];
        return cell;
    }else{
        HomePageBottomCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"footcell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HomePageBottomCell alloc]init];
        }
        HomePageBottomModel *model=tableFootArr[indexPath.row];
        [cell reflushForModel:model];
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==categoryCollectionView) {
        return CGSizeMake((SCREENSIZE.width-60)/4.0, (SCREENSIZE.width-60)/4.0);
    }else{
        return CGSizeMake((SCREENSIZE.width)/3.0, 30);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (collectionView==categoryCollectionView) {
        return UIEdgeInsetsMake(0, 4, 0, 4);
    }else{
        return UIEdgeInsetsMake(6, 0, 6, 0);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductListController *productList=[[ProductListController alloc]init];
    productList.requestType=HomePageCategoryRequestType;
    
    SellerShowViewController *sellerFashion=[[SellerShowViewController alloc]init];
    
    AdvDetailController *advContr=[[AdvDetailController alloc]init];
    
    StageAdvController *stageContr=[[StageAdvController alloc]init];
    
    if (collectionView==categoryCollectionView) {
        MMGHomePageCagegoryModel *model=categoryArr[indexPath.row];
        switch (indexPath.row) {
            case 0:case 1:case 2:case 3:case 4:
                productList.productListId=model.href;
                productList.productListName=model.name;
                productList.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:productList animated:YES];
                break;
            case 5:
                sellerFashion.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:sellerFashion animated:YES];
                break;
            case 6:
                advContr.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:advContr animated:YES];
                break;
            case 7:
                stageContr.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:stageContr animated:YES];
                break;
                
            default:
                break;
        }
    }else{
        BottomPage01 *b01=[[BottomPage01 alloc]init];
        b01.hidesBottomBarWhenPushed=YES;
        BottomPage02 *b02=[[BottomPage02 alloc]init];
        b02.hidesBottomBarWhenPushed=YES;
        BottomPage03 *b03=[[BottomPage03 alloc]init];
        b03.hidesBottomBarWhenPushed=YES;
        BottomPage04 *b04=[[BottomPage04 alloc]init];
        b04.hidesBottomBarWhenPushed=YES;
        BottomPage05 *b05=[[BottomPage05 alloc]init];
        b05.hidesBottomBarWhenPushed=YES;
        BottomPage06 *b06=[[BottomPage06 alloc]init];
        b06.hidesBottomBarWhenPushed=YES;
        
        HomePageBottomModel *model=tableFootArr[indexPath.row];
        switch (indexPath.row) {
            case 0:
                b01.idStr=model.idStr;
                [self.navigationController pushViewController:b01 animated:YES];
                break;
            case 1:
                b02.idStr=model.idStr;
                [self.navigationController pushViewController:b02 animated:YES];
                break;
            case 2:
                b03.idStr=model.idStr;
                [self.navigationController pushViewController:b03 animated:YES];
                break;
            case 3:
                b04.idStr=model.idStr;
                [self.navigationController pushViewController:b04 animated:YES];
                break;
            case 4:
                b05.idStr=model.idStr;
                [self.navigationController pushViewController:b05 animated:YES];
                break;
            case 5:
                b06.idStr=model.idStr;
                [self.navigationController pushViewController:b06 animated:YES];
                break;
                
            default:
                break;
        }
    }
}


#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return goodBigImgArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==goodBigImgArr.count-1) {
        return 120;
    }
    return 173;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==goodBigImgArr.count-1){
        return 16;
    }
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 16;
    }
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==goodBigImgArr.count-1) {
        HomepageHoriCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HomepageHoriCell"];
        if (!cell) {
            cell=[[HomepageHoriCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomepageHoriCell"];
        }
        [cell reflushDataForArray:businessArr];
        cell.businessDelegate=self;
        return  cell;
    }else{
        HomePageGoodCell *cell=[tableView dequeueReusableCellWithIdentifier:@"goodCellId"];
        if (!cell) {
            cell=[[HomePageGoodCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"goodCellId"];
        }
        if (goodBigImgArr.count>0) {
            if (indexPath.section<goodBigImgArr.count) {
                [cell setUpUIWithIndexPath:indexPath];
                cell.homepageGoodDelegate=self;
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                HomePageGoodModel *model=goodBigImgArr[indexPath.section];
                [cell reflushModel:model];
            }
        }
        return  cell;
    }
}

#pragma mark - HomePageDelegate
-(void)goDetailPageWithId:(NSString *)modelId{
    //===========
    //1、如果包含搜索跳转到列表页
    //2、跳转到商品详情
    //===========
    if ([modelId rangeOfString:@"search.php?"].location != NSNotFound) {
        ProductListController *productList=[[ProductListController alloc]init];
        productList.productListId=modelId;
        productList.requestType=HomePageGoodsListRequestType;
        productList.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:productList animated:YES];
    }else{
        NSMutableString *str=[NSMutableString stringWithString:modelId];
        NSRange range = [str rangeOfString:@"id="];
        if (range.location != NSNotFound) {
            NSString *newGoodId=[str substringFromIndex:range.location+range.length];
            ProductDetailController *productDetail=[[ProductDetailController alloc]init];
            productDetail.hidesBottomBarWhenPushed=YES;
            productDetail.goodId=newGoodId;
            [self.navigationController pushViewController:productDetail animated:YES];
        }
    }
}

#pragma mark - 点击分类类别 Delegate
-(void)clickCategoryWithModel:(MMGHomePageCagegoryModel *)model indexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            StageAdvController *stageContr=[[StageAdvController alloc]init];
            stageContr.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:stageContr animated:YES];
        }
            break;
        case 1:
        {
            AdvDetailController *advContr=[[AdvDetailController alloc]init];
            advContr.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:advContr animated:YES];
        }
            break;
        case 2:
        {
            SellerShowViewController *sellerFashion=[[SellerShowViewController alloc]init];
            sellerFashion.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:sellerFashion animated:YES];
        }
            break;
            
        default:
        {
            ProductListController *productList=[[ProductListController alloc]init];
            productList.requestType=HomePageCategoryRequestType;
            productList.productListId=model.href;
            productList.productListName=model.name;
            productList.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:productList animated:YES];
        }
            break;
    }
}

#pragma mark - 点击商铺 Delegate
-(void)clickBusinessWithModel:(MMGHomePageCagegoryModel *)model{
    if (![model.busId isEqualToString:@"noid"]) {
        ShopController *shopCtr=[ShopController new];
        shopCtr.dp_id=model.busId;
        shopCtr.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:shopCtr animated:YES];
    }
}

@end

#pragma mark - page01
@interface BottomPage01 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage01{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setCustomNavigationTitle:@"正品保障"];
//    
//    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
//    _webView.scrollView.bounces = YES;
//    _webView.scrollView.showsHorizontalScrollIndicator = NO;
//    _webView.paginationMode = UIWebPaginationModeUnpaginated;
//    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
//    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
//    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    UIView *vv=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 2)];
    vv.backgroundColor=[UIColor redColor];
    _progressView.progressBarView=vv;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - page02
@interface BottomPage02 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage02{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"七天退款"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - page03
@interface BottomPage03 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage03{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"维护修养"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - page04
@interface BottomPage04 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage04{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"权威鉴定"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - page05
@interface BottomPage05 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage05{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"分期购物"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - page06
@interface BottomPage06 ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end
@implementation BottomPage06{
    UIWebView *_webView;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"关于我们"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@&is_phone=1",ImageHost,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)initialize {
//    // 1.appearance方法返回一个导航栏的外观对象
//    //修改了这个外观对象，相当于修改了整个项目中的外观
//    UINavigationBar *navigationBar = [UINavigationBar appearance];
//    
//    [navigationBar setBarTintColor:(UIColor * _Nullable):];
//    [[UINavigationBar appearance] setTranslucent:NO]
//    [navigationBar setTintColor:[UIColor whiteColor]];// iOS7的情况下,设置NavigationBarItem文字的颜色
//    // 3.设置导航栏文字的主题
//    NSShadow *shadow = [[NSShadow alloc]init];
//    [shadow setShadowOffset:CGSizeZero];
//    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColorwhiteColor],
//                                            NSShadowAttributeName : shadow}];
//}



@end
