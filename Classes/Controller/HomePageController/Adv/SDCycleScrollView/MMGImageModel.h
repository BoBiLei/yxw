//
//  MMGImageModel.h
//  Pods
//
//  Created by mac on 15/9/22.
//
//

#import <Foundation/Foundation.h>

@interface MMGImageModel : NSObject

@property (nonatomic,copy) NSString *imgId;
@property (nonatomic,copy) NSString *imgStr;
@property (nonatomic,copy) NSString *advType;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
