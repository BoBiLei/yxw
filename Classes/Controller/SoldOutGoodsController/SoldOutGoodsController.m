//
//  SoldOutGoodsController.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "SoldOutGoodsController.h"
#import "SoldOutGoodsCell.h"
#import "CustomIOSAlertView.h"
#import "MaijiaGoodsManagerModel.h"
#import "GoodsEditController.h"
#import "MMGSign.h"
#import "HttpRequests.h"
#import "Macro2.h"
@interface SoldOutGoodsController ()<UITableViewDataSource,UITableViewDelegate,SoldOutGoodsDelegate,UITextFieldDelegate>

@end

@implementation SoldOutGoodsController{
    
    NSInteger pageInt;
    MJRefreshAutoNormalFooter *footer;
    
    NSString *searKeyWork;
    NSMutableArray *dataArr;
    UITableView *myTable;
    CustomIOSAlertView *reShangjiaAlertView;
    CustomIOSAlertView *deleteAlertView;
    UIButton *confirmDeleteBtn;                 //确认删除按钮
    MaijiaGoodsManagerModel *clickModel;
    NSString *randomStr;
    UITextField *valiTF;
    UIView *volatileView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateView:) name:@"xiajia_eidt_success" object:nil];
    
    [self setCustomNavigationTitle:@"已下架商品"];
    
    [self setUpSearchView];
    
    [self setUpTableView];
    
    [self requestOffGoodsListDataWithKeyWork:searKeyWork];
}

-(void)upDateView:(NSNotification *)noti{
    pageInt=1;
    [dataArr removeAllObjects];
    [self requestOffGoodsListDataWithKeyWork:searKeyWork];
}

-(void)setUpSearchView{
    
    searKeyWork=@"";
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
    [self.view addSubview:view];
    
#pragma mark 搜索View
    UIView *sView=[[UIView alloc] initWithFrame:CGRectMake(12, 14, SCREENSIZE.width-24, 36)];
    sView.backgroundColor=[UIColor whiteColor];
    [view addSubview:sView];
    
    UITextField *searchTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, sView.width-sView.height-8, sView.height)];
    searchTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    searchTF.placeholder=@"Birkin";
    searchTF.font=[UIFont systemFontOfSize:15];
    searchTF.textColor=[UIColor colorWithHexString:@"#363636"];
    searchTF.returnKeyType=UIReturnKeySearch;
    searchTF.delegate=self;
    [searchTF addTarget:self action:@selector(searTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [sView addSubview:searchTF];
    
    //
    UIImageView *srImgv=[[UIImageView alloc] initWithFrame:CGRectMake(searchTF.origin.x+searchTF.width+4, 0, sView.height, sView.height)];
    srImgv.image=[UIImage imageNamed:@"maijia_search"];
    srImgv.userInteractionEnabled=YES;
    [sView addSubview:srImgv];
    UITapGestureRecognizer *stap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSearch)];
    [srImgv addGestureRecognizer:stap];
}

- (void)searTextDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    searKeyWork=field.text;
}

#pragma mark - 点击搜索
-(void)clickSearch{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [self requestOffGoodsListDataWithKeyWork:searKeyWork];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [self requestOffGoodsListDataWithKeyWork:searKeyWork];
    return YES;
}

#pragma mark - setUpTableView
-(void)setUpTableView{
    
    pageInt=1;
    
    dataArr=[NSMutableArray array];
    
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-128) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //加载更多
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = [UIFont systemFontOfSize:14];
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (pageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestOffGoodsListDataWithKeyWork:searKeyWork];
        });
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str=[AppUtils getValueWithKey:@"SoldOutGoodsCell_Height"];
    return str.floatValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==7) {
        return 20;
    }else{
        return 0.000000000001f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SoldOutGoodsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[SoldOutGoodsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    MaijiaGoodsManagerModel *model=dataArr[indexPath.section];
    [cell reflushDataForModel:model andIsXiajia:YES];
    cell.isSelePiliang=NO;
    cell.delegate=self;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 按钮delegate
//编辑商品
-(void)editGoods:(MaijiaGoodsManagerModel *)model{
    clickModel=model;
    GoodsEditController *ctr=[GoodsEditController new];
    ctr.goodsID=clickModel.goodsID;
    ctr.isGoodsManageer=NO;
    [self.navigationController pushViewController:ctr animated:YES];
}

//重新上架
-(void)reShangjia:(MaijiaGoodsManagerModel *)model{
    clickModel=model;
    reShangjiaAlertView = [[CustomIOSAlertView alloc] init];
    [reShangjiaAlertView setContainerView:[self createShangjiaAlertView]];
    [reShangjiaAlertView setButtonTitles:nil];
    
    [reShangjiaAlertView setUseMotionEffects:true];
    [reShangjiaAlertView show];
}

//删除商品
-(void)deleteGoods:(MaijiaGoodsManagerModel *)model{
    clickModel=model;
    deleteAlertView = [[CustomIOSAlertView alloc] init];
    [deleteAlertView setContainerView:[self deleteGoodsAlertView]];
    [deleteAlertView setButtonTitles:nil];
    
    [deleteAlertView setUseMotionEffects:true];
    [deleteAlertView show];
}

#pragma mark - 创建重新上架的

-(UIView *)createShangjiaAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.layer.cornerRadius=6;
    view.layer.borderWidth=0.5f;
    view.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 24, view.width, 24)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    tipLabel.text=@"是否确认重新上架？";
    [view addSubview:tipLabel];
    
    //
    CGFloat lastYH=[self createShangjiaBtn:@[@"取消",@"确认上架"] andToView:view afterView:tipLabel];
    CGRect frame=view.frame;
    frame.size.height=lastYH+24;
    view.frame=frame;
    
    return view;
}


-(CGFloat)createShangjiaBtn:(NSArray *)array andToView:(UIView *)view afterView:(UIView *)afterView{
    CGFloat lastYH=0;
    CGFloat btnX=36;
    CGFloat btnY=afterView.origin.y+afterView.height+17;
    CGFloat btnW=(view.width-36)/2;
    CGFloat btnHeight=40;
    for (int i=0; i<array.count; i++) {
        btnX=12+i*btnW+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        btn.layer.cornerRadius=2;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor colorWithHexString:i==array.count-1?@"e13f3f":@"#ea6b1f"];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikShangjiaBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        lastYH=btn.origin.y+btn.height+4;
    }
    return lastYH;
}

-(void)clcikShangjiaBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
            [reShangjiaAlertView close];
            break;
        default:
            [reShangjiaAlertView close];
            [self reOffGoodsRequest:clickModel.goodsID];
            break;
    }
}

/**
 点击Cell
 */
-(void)selectedGoodCellForModel:(MaijiaGoodsManagerModel *)model indexPath:(NSIndexPath *)indexPath{
    NSLog(@"-----");
}

#pragma mark - 创建删除商品

-(UIView *)deleteGoodsAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.layer.cornerRadius=6;
    view.layer.borderWidth=0.5f;
    view.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 24, view.width, 24)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    tipLabel.text=@"是否要删除商品？";
    [view addSubview:tipLabel];
    
#pragma mark 验证码View
    volatileView=[[UIView alloc] initWithFrame:CGRectMake(view.width/4, tipLabel.origin.y+tipLabel.height+16, view.width*3/5, 40)];
    volatileView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    volatileView.center=CGPointMake(view.width/2, volatileView.center.y);
    volatileView.layer.borderWidth=0.5f;
    volatileView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [view addSubview:volatileView];
    
    valiTF=[[UITextField alloc] initWithFrame:CGRectMake(2, 0, volatileView.width/2-4, volatileView.height)];
    valiTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    valiTF.placeholder=@"输入验证码";
    valiTF.keyboardType=UIKeyboardTypeNumberPad;
    valiTF.font=[UIFont systemFontOfSize:15];
    valiTF.textColor=[UIColor colorWithHexString:@"#363636"];
    valiTF.textAlignment=NSTextAlignmentCenter;
    [valiTF addTarget:self action:@selector(valiTFDidChange:) forControlEvents:UIControlEventEditingChanged];
    [volatileView addSubview:valiTF];
    
    UILabel *valiLabel=[[UILabel alloc] initWithFrame:CGRectMake(volatileView.width/2, 0, volatileView.width/2, volatileView.height)];
    valiLabel.backgroundColor=[UIColor colorWithHexString:@"#b7b7b7"];
    valiLabel.textAlignment=NSTextAlignmentCenter;
    valiLabel.font=[UIFont systemFontOfSize:17];
    valiLabel.textColor=[UIColor whiteColor];
    randomStr=[self randomNumber];
    valiLabel.text=randomStr;
    [volatileView addSubview:valiLabel];
    
    //
    CGFloat lastYH=[self createDeleteBtn:@[@"取消",@"确认删除"] andToView:view afterView:volatileView];
    CGRect frame=view.frame;
    frame.size.height=lastYH+24;
    view.frame=frame;
    
    return view;
}


-(CGFloat)createDeleteBtn:(NSArray *)array andToView:(UIView *)view afterView:(UIView *)afterView{
    CGFloat lastYH=0;
    CGFloat btnX=36;
    CGFloat btnY=afterView.origin.y+afterView.height+17;
    CGFloat btnW=(view.width-36)/2;
    CGFloat btnHeight=38;
    for (int i=0; i<array.count; i++) {
        btnX=12+i*btnW+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        btn.layer.cornerRadius=2;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor colorWithHexString:i==array.count-1?@"e13f3f":@"#ea6b1f"];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        if (i==array.count-1) {
            btn.enabled=NO;
            btn.backgroundColor=[UIColor lightGrayColor];
            confirmDeleteBtn=btn;
        }
        lastYH=btn.origin.y+btn.height+4;
    }
    return lastYH;
}

-(void)clcikDeleteBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
            [deleteAlertView close];
            break;
        default:
        {
            if (![valiTF.text isEqualToString:randomStr]) {
                [AppUtils showSuccessMessage:@"请输入正确验证码" inView:volatileView];
            }else{
                [deleteAlertView close];
                [self deleteGoodsRequest:clickModel.goodsID];
            }
        }
            break;
    }
}

//生产随机数
-(NSString *)randomNumber{
    
    const int N = 4;
    
    NSString *sourceString = @"0123456789";
    
    NSMutableString *result = [[NSMutableString alloc] init];
    
    srand((int)time(0));
    
    for (int i = 0; i < N; i++){
        
        unsigned index = rand() % [sourceString length];
        
        NSString *s = [sourceString substringWithRange:NSMakeRange(index, 1)];
        
        [result appendString:[NSString stringWithFormat:@"%@",s]];
        
    }
    return result;
}

- (void)valiTFDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (![field.text isEqualToString:@""]) {
        confirmDeleteBtn.backgroundColor=[UIColor colorWithHexString:@"e13f3f"];
        confirmDeleteBtn.enabled=YES;
    }else{
        confirmDeleteBtn.enabled=NO;
        confirmDeleteBtn.backgroundColor=[UIColor lightGrayColor];
    }
}

#pragma mark - Request 请求
-(void)requestOffGoodsListDataWithKeyWork:(NSString *)keyWork{
    
    if (pageInt==1) {
        [AppUtils showProgressInView:self.view];
    }
    
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:keyWork forKey:@"goods_name"];
    [dict setObject:@"off_goods_list" forKey:@"act"];
    [dict setObject:[NSString stringWithFormat:@"%ld",pageInt] forKey:@"page"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@off_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        //MaijiaGoodsManagerModel
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *ddArr=responseObject[@"retData"][@"off_goods_list"];
            for (NSDictionary *dic in ddArr) {
                MaijiaGoodsManagerModel *model=[MaijiaGoodsManagerModel new];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            
            //总页数
            NSString *totalPage=[responseObject[@"retData"][@"totalPageCount"] stringValue];
            if (pageInt<totalPage.integerValue) {
                pageInt++;
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
            
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//删除
-(void)deleteGoodsRequest:(NSString *)goodsId{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"del_a_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@off_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",responseObject,responseObject[@"retMsg"]);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
            // 延迟加载
            pageInt=1;
            [dataArr removeAllObjects];
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestOffGoodsListDataWithKeyWork:searKeyWork];
            });
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//重新上架
-(void)reOffGoodsRequest:(NSString *)goodsId{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"up_a_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@off_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
            // 延迟加载
            pageInt=1;
            [dataArr removeAllObjects];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestOffGoodsListDataWithKeyWork:searKeyWork];
            });
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
