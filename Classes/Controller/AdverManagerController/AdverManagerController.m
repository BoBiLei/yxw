//
//  AdverManagerController.m
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "AdverManagerController.h"
#import "AdvManagerCell.h"
#import <UIImage+MultiFormat.h>
#import "HttpRequests.h"
#import "Macro2.h"
@interface AdverManagerController ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@end

@implementation AdverManagerController{
    BOOL isH5;
    UIImage *img_h5;
    UIImage *img_pc;
    NSMutableArray *dataArr;
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavigation];
    
    [self setUpUI];
    
    [self getAdvInfoRequest];
}

#pragma mark - setUpNavigation

-(void)setUpNavigation{
    [self setCustomNavigationTitle:@"广告管理"];
    UIButton *navRightBtn=[self setCustomRightBarButtonItemSetFrame:CGRectMake(0, 0, 40, 40) Text:@"提交"];
    [navRightBtn addTarget:self action:@selector(clickSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickSaveBtn:(id)sender{
    [self submitRequest];
}

#pragma mark - init UI

-(void)setUpUI{
    //
    dataArr = [NSMutableArray array];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}


#pragma mark - table delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREENSIZE.height/3-12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvManagerCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AdvManagerCell"];
    if (cell==nil) {
        cell=[[AdvManagerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AdvManagerCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=myTable.backgroundColor;
    if (indexPath.section==0) {
        cell.imgv.image=img_h5;
    }else{
        cell.imgv.image=img_pc;
    }
    return  cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        isH5=YES;
    }else{
        isH5=NO;
    }
    [self clickphotoBtn];
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0||section==1) {
        return 54;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    UILabel *label = [[UILabel alloc] init] ;
    label.frame = CGRectMake(8, 4, CGRectGetWidth(tableView.frame)-16, 54);
    label.font = [UIFont systemFontOfSize:15];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    label.text = sectionTitle;
    label.numberOfLines=0;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 54)];
    [view addSubview:label];
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"手机版广告图片：";
    }else if(section==1){
        return @"电脑版广告图片：";
    }else{
        return nil;
    }
}

#pragma mark - 修改头像
-(void)clickphotoBtn{
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            case 1:
                [self pickImage];
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:isH5?@"选择手机版广告图片":@"选择电脑版广告图片"
                                  items:items] showWithBlock:completeBlock];
}

//拍照
- (void) snapImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"%@",info);
    UIImage *image= [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(SCREENSIZE.width-20, SCREENSIZE.height/3-28)];
    if(isH5){
        img_h5=theImage;
    }else{
        img_pc=theImage;
    }
    [myTable reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - navigation delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    UIColor * color = [UIColor blackColor];
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    navigationController.navigationBar.titleTextAttributes=dict;
    navigationController.navigationBar.tintColor=[UIColor blackColor];
}

#pragma mark - Request 请求

-(void)getAdvInfoRequest{
    NSDictionary *dict=@{
                         @"act":@"get_adv",
                         @"is_phone":@"1",
                         @"user_id":[AppUtils getValueWithKey:User_ID],
                         @"is_nosign":@"1"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"busines.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *url_h5=[NSString stringWithFormat:@"%@%@",ImageHost01,responseObject[@"retData"][@"business_poster_h5"]];
            NSString *url_pc=[NSString stringWithFormat:@"%@%@",ImageHost01,responseObject[@"retData"][@"business_poster_pc"]];
            NSData *data_h5=[NSData dataWithContentsOfURL:[NSURL URLWithString:url_h5]];
            NSData *data_pc=[NSData dataWithContentsOfURL:[NSURL URLWithString:url_pc]];
            img_h5=[UIImage sd_imageWithData:data_h5];
            img_pc=[UIImage sd_imageWithData:data_pc];
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)submitRequest{
    NSDictionary *dict=@{
                         @"is_nosign":@"1",
                         @"act":@"insert_adv",
                         @"is_phone":@"1",
                         @"user_id":[AppUtils getValueWithKey:User_ID],
                         @"adv_arr":@[UIImagePNGRepresentation(img_pc),UIImagePNGRepresentation(img_h5)]
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"busines.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"保存成功" inView:self.view];
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
