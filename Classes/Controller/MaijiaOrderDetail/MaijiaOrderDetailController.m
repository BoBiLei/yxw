//
//  MaijiaOrderDetailController.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaOrderDetailController.h"
#import "MaijiaOrderDetailFirstCell.h"
#import "MaijiaOrderDetailSecondCell.h"
#import "MaijiaOrderDetailThreeCell.h"
#import "MaijiaOrderDetailFourCell.h"
#import "CustomPickerView.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import "ShangjiaMacro.h"
#import "HttpRequests.h"
#import "MMGSign.h"
#import "Macro2.h"
@interface MaijiaOrderDetailController ()<UITableViewDataSource,UITableViewDelegate,SaveMaijiaOrderDetailDelegate,MaijiaOrderDetailStatusDelegate>

@end

@implementation MaijiaOrderDetailController{
    
    UITableView *myTable;
    
    NSString *goods_status;     //商品发货状态
    NSString *order_sn;         //订单编号
    NSString *add_time;         //（下单时间戳）
    NSString *pay_time;         //（支付时间戳）
    NSString *shipping_time;    //（发货时间戳）
    NSString *intro;            //（卖价备注）
    NSString *goods_img;        //（商品图片）
    NSString *user_name;        //（买家名字）
    NSString *jiesuan_price;    //（寄售价格）
    NSString *fenqi_fee;        // (每期要还的款)
    NSString *fenqi_interest;   //(利息总和)
    NSString *fenqi_service;    //(手续费总和)
    NSString *mobile;           //（买家手机号）
    NSString *address;          //（收货地址）
    NSString *order_msg;        //请尽快发货  （买家留言）
    NSString *fenqi_sum;        //（分期数）
    NSString *goods_name;       //（商品名字）
    NSString *js_status;        //（结算状态 1:待结算，2：已结算'）
    NSString *js_price;         //已结算金额
    NSString *kd_tongtai;       //快递动态
    NSString *kd_bianhao;       //快递编号
    NSString *kd_company;       //快递公司
    
    
    NSMutableArray *goodsStatusArr;
    NSString *seleGoodStatusID;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"订单详情"];
    
    [self setUpTableView];
    
    [self requestGoodsDetailWithGoodId:self.goodsId orderId:self.orderId];
}

#pragma mark - setUpTableView
-(void)setUpTableView{
    
    goodsStatusArr=[NSMutableArray array];
    for (int i=1; i<=3; i++) {
        SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
        model.typeId=[NSString stringWithFormat:@"%d",i];
        NSString *str;
        if (i==3) {
            str=@"商品已收货";
        }else if (i==2){
            str=@"商品已发货";
        }else{
            str=@"商品未发货";
        }
        model.typeName=str;
        [goodsStatusArr addObject:model];
    }
    
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return ![goods_status isEqualToString:@"3"]?4:3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
//            NSString *str=[AppUtils getValueWithKey:@"MaijiaOrderDetailFirstCell_Height"];
//            return str.floatValue;
            return 200;
        }
            break;
        case 1:
        {
            NSString *str=[AppUtils getValueWithKey:@"MaijiaOrderDetailSecondCell_Height"];
            return str.floatValue;
        }
            break;
        case 2:
        {
            NSString *str=[AppUtils getValueWithKey:@"MaijiaOrderDetailThreeCell_Height"];
            return str.floatValue;
        }
            break;
        default:
        {
            NSString *str=[AppUtils getValueWithKey:@"MaijiaOrderDetailFourCell_Height"];
            return str.floatValue;
        }
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if ([goods_status isEqualToString:@"3"]) {
        if (section==2) {
            return 20;
        }else{
            return 0.000000000001f;
        }
    }else{
        return 0.000000000001f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            MaijiaOrderDetailFirstCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell01"];
            if (!cell) {
                cell=[[MaijiaOrderDetailFirstCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell01"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushViewForOrderStatus:goods_status orderSn:order_sn addTime:add_time payTime:pay_time shippingTime:shipping_time liuyan:intro];
            cell.bzTextFieldBlock=^(NSString *tfText){
                intro=tfText;
            };
            return  cell;
        }
            break;
        case 1:
        {
            MaijiaOrderDetailSecondCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell02"];
            if (!cell) {
                cell=[[MaijiaOrderDetailSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell02"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushViewWithUser:user_name mobile:mobile address:address orderMsg:order_msg goodsImg:goods_img js_status:js_status goodsName:goods_name jiesuan_price:jiesuan_price js_price:js_price fenqiFee:fenqi_fee fenqi:fenqi_sum sxffee:fenqi_interest shuiFee:fenqi_service];
            return  cell;
        }
            break;
        case 2:
        {
            MaijiaOrderDetailThreeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell03"];
            if (!cell) {
                cell=[[MaijiaOrderDetailThreeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell03"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDateForStatus:goods_status kdCompany:kd_company kdNumber:kd_bianhao kdDongtai:kd_tongtai];
            cell.delegate=self;
            cell.kdCompanyBlock=^(NSString *tfText){
                kd_company=tfText;
            };
            cell.kdNumberBlock=^(NSString *tfText){
                kd_bianhao=tfText;
            };
            cell.kdDongtaiBlock=^(NSString *tfText){
                kd_tongtai=tfText;
            };
            return  cell;
        }
            break;
        default:
        {
            MaijiaOrderDetailFourCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell04"];
            if (!cell) {
                cell=[[MaijiaOrderDetailFourCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell04"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.delegate=self;
            return  cell;
        }
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击选择商品状态
-(void)clickGoodsStatusView{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:goodsStatusArr selectedTypeId:seleGoodStatusID];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleGoodStatusID=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Maijia_OrderDetail_GoodsStatusNoti object:model];
    }];
}

#pragma mark - 保存订单详情信息 Delegate
-(void)saveMaijiaOrderDetail{
    [AppUtils closeKeyboard];
    if ([intro isEqualToString:@""]||intro==nil) {
        [AppUtils showSuccessMessage:@"请输入卖家备注" inView:self.view];
    }else if ([kd_company isEqualToString:@""]||kd_company==nil){
        [AppUtils showSuccessMessage:@"请输入快递公司" inView:self.view];
    }else if ([kd_bianhao isEqualToString:@""]||kd_bianhao==nil){
        [AppUtils showSuccessMessage:@"请输入快递编号" inView:self.view];
    }else if ([kd_tongtai isEqualToString:@""]||kd_tongtai==nil){
        [AppUtils showSuccessMessage:@"请输入快递动态" inView:self.view];
    }else{
        [self saveRequest];
    }
}

#pragma mark - Request 请求
-(void)requestGoodsDetailWithGoodId:(NSString *)goodId orderId:(NSString *)orderId{
    
    [AppUtils showProgressInView:self.view];
    
    //dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"get_order_info" forKey:@"act"];
    [dict setObject:goodId forKey:@"goods_id"];
    [dict setObject:orderId forKey:@"order_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@sold_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            goods_status=responseObject[@"retData"][@"goods_status"];
            order_sn=responseObject[@"retData"][@"order_sn"];
            add_time=responseObject[@"retData"][@"add_time"];
            pay_time=responseObject[@"retData"][@"pay_time"];
            shipping_time=responseObject[@"retData"][@"shipping_time"];
            intro=responseObject[@"retData"][@"intro"];
            goods_img=responseObject[@"retData"][@"goods_img"];
            user_name=responseObject[@"retData"][@"user_name"];
            jiesuan_price=responseObject[@"retData"][@"jiesuan_price"];
            fenqi_fee=responseObject[@"retData"][@"fenqi_fee"];
            fenqi_interest=responseObject[@"retData"][@"fenqi_interest"];
            fenqi_service=responseObject[@"retData"][@"fenqi_service"];
            mobile=responseObject[@"retData"][@"mobile"];
            address=responseObject[@"retData"][@"address"];
            order_msg=responseObject[@"retData"][@"order_msg"];
            fenqi_sum=responseObject[@"retData"][@"fenqi"];
            goods_name=responseObject[@"retData"][@"goods_name"];
            js_status=responseObject[@"retData"][@"js_status"];
            js_price=responseObject[@"retData"][@"js_price"];
            kd_tongtai=responseObject[@"retData"][@"kd_tongdai"];
            kd_bianhao=responseObject[@"retData"][@"kd_bianhao"];
            kd_company=responseObject[@"retData"][@"kd_company"];
            
            seleGoodStatusID=goods_status;
        }
        [myTable reloadData];
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

-(void)saveRequest{
    
    [AppUtils showProgressInView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"save_order_info" forKey:@"act"];
    [dict setObject:_goodsId forKey:@"goods_id"];
    [dict setObject:intro forKey:@"intro"];
    [dict setObject:seleGoodStatusID forKey:@"goods_status"];
    [dict setObject:kd_company forKey:@"kd_company"];
    [dict setObject:kd_bianhao forKey:@"kd_bianhao"];
    [dict setObject:kd_tongtai forKey:@"kd_tongdai"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@sold_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"sccuess"]) {
            [AppUtils showSuccessMessage:@"保存信息成功" inView:self.view];
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"update_hadseller_list" object:nil];
            });
        }else{
            [AppUtils showSuccessMessage:@"保存信息失败" inView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

@end
