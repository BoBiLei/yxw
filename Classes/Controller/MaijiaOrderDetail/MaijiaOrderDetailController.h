//
//  MaijiaOrderDetailController.h
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface MaijiaOrderDetailController : BaseController

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *goodsId;

@end
