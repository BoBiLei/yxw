//
//  MMGSelectCityView.h
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMGSelectCityView : UIView

@property (nonatomic,assign) NSArray *provinceArr;
@property (nonatomic,assign) NSArray *cityArr;
@property (nonatomic,assign) NSArray *districtArr;
@property (nonatomic,assign) NSArray *selectedArray;

+(void)showPickerViewInView: (UIView *)view
        withProvinceStrings: (NSArray *)provinceStrings
            withCityStrings: (NSArray *)cityStrings
        withDistrictStrings: (NSArray *)districtStrings
                withOptions: (NSDictionary *)options
                 completion: (void(^)(NSString *selectedString))completion;

+(void)dismissWithCompletion: (void(^)(NSString *))completion;

@end
