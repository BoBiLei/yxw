//
//  MMGSelectCityView.m
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGSelectCityView.h"
#import "MMGCityModel.h"
#import "CityFmdb.h"

@interface MMGSelectCityView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *pickerContainerView;
@property (nonatomic, strong) UIView *bgnView;
@property (nonatomic, strong) UIView *pickerTopBarView;
@property (nonatomic, strong) UIImageView *pickerTopBarImageView;
@property (nonatomic, strong) UIToolbar *pickerViewToolBar;
@property (nonatomic, strong) UIBarButtonItem *pickerViewBarButtonItem;
@property (nonatomic, strong) UIButton *pickerDoneButton;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) NSArray *pickerViewArray;
@property (nonatomic, strong) UIColor *pickerViewTextColor;
@property (nonatomic, strong) UIFont *pickerViewFont;
@property (nonatomic, assign) CGFloat yValueFromTop;
@property (nonatomic, assign) NSInteger pickerViewTextAlignment;
@property (nonatomic, assign) BOOL pickerViewShowsSelectionIndicator;
@property (copy) void (^onDismissCompletion)(NSString *);
@property (copy) NSString *(^objectToStringConverter)(id object);

@end

@implementation MMGSelectCityView

#pragma mark - Singleton
+ (MMGSelectCityView*)sharedView {
    static dispatch_once_t once;
    static MMGSelectCityView *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[self alloc] init];
    });
    return sharedView;
}

#pragma mark - Show Methods

+(void)showPickerViewInView:(UIView *)view
        withProvinceStrings:(NSArray *)provinceStrings
            withCityStrings:(NSArray *)cityStrings
        withDistrictStrings:(NSArray *)districtStrings
                withOptions:(NSDictionary *)options
                 completion:(void (^)(NSString *))completion{
    
    [[self sharedView] initializePickerViewInView:view withProvinceStrings:provinceStrings
                                  withCityStrings:cityStrings withDistrictStrings:districtStrings
                                      withOptions:options];
    
    [[self sharedView] setPickerHidden:NO callBack:nil];
    [self sharedView].onDismissCompletion = completion;
    [view addSubview:[self sharedView]];
    
}

#pragma mark - Dismiss Methods

+(void)dismissWithCompletion:(void (^)(NSString *))completion{
    [[self sharedView] setPickerHidden:YES callBack:completion];
}

-(void)dismiss{
    [MMGSelectCityView dismissWithCompletion:self.onDismissCompletion];
}

+(void)removePickerView{
    [[self sharedView] removeFromSuperview];
}

#pragma mark - Show/hide PickerView methods

-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(NSString *))callBack; {
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_bgnView setAlpha:0.0];
                             [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
                         } else {
                             [_bgnView setAlpha:1.0];
                             [_pickerContainerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [MMGSelectCityView removePickerView];
                             callBack([self selectedObject]);
                         }
                     }];
}

#pragma mark - Initialize PickerView

-(void)initializePickerViewInView: (UIView *)view
              withProvinceStrings: (NSArray *)provinceStrings
                  withCityStrings: (NSArray *)cityStrings
              withDistrictStrings: (NSArray *)districtStrings
                      withOptions: (NSDictionary *)options {
    
    self.provinceArr = provinceStrings;
    self.cityArr = cityStrings;
    self.districtArr = districtStrings;
    
    id chosenObject = options[@"MMselectedObject"];
    NSInteger selectedRow;
    
    if (chosenObject!=nil) {
        selectedRow = [_pickerViewArray indexOfObject:chosenObject];
    }else{
        selectedRow = [[_pickerViewArray objectAtIndex:0] integerValue];
    }
    
    //Default value is NSTextAlignmentCenter
    _pickerViewTextAlignment = 1;
    
    
    [self setFrame: view.bounds];
    [self setBackgroundColor:[UIColor clearColor]];
    
    //半透明黑色背景
    _bgnView = [[UIView alloc] initWithFrame:view.bounds];
    [_bgnView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
    [_bgnView setAlpha:0.0];
    //添加手势
    [_bgnView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
    [self addSubview:_bgnView];
    
    //PickerView Container with top bar
    _pickerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _bgnView.height - 236.0, self.width, 236.0)];
    
    [self addSubview:_pickerContainerView];
    
    //Content of pickerContainerView
    
    //Top bar view
    _pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _pickerContainerView.width, 44.0)];
    [_pickerContainerView addSubview:_pickerTopBarView];
    [_pickerTopBarView setBackgroundColor:[UIColor whiteColor]];
    
    
    _pickerViewToolBar = [[UIToolbar alloc] initWithFrame:_pickerTopBarView.frame];
    [_pickerContainerView addSubview:_pickerViewToolBar];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.width-60, 4, 40, 40)];
    [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
    [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
    [closeBtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
    _pickerViewBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    _pickerViewToolBar.items = @[flexibleSpace, _pickerViewBarButtonItem];
    
    //Add pickerView
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, _pickerTopBarView.height, self.width, _pickerContainerView.height-_pickerTopBarView.height)];
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
    [_pickerView setShowsSelectionIndicator: _pickerViewShowsSelectionIndicator];
    [_pickerContainerView addSubview:_pickerView];
    
    [_pickerContainerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_pickerContainerView.frame))];
    
    //Set selected row
    [_pickerView selectRow:selectedRow inComponent:0 animated:YES];
}

#pragma mark - 点击透明背景hidden

- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_bgnView setAlpha:1.0];
                         [_pickerContainerView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.provinceArr.count;
    } else if (component == 1) {
        return self.cityArr.count;
    } else {
        return self.districtArr.count;
    }
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        MMGCityModel *model=[self.provinceArr objectAtIndex:row];
        return model.cityName;
    } else if (component == 1) {
        if(self.cityArr.count==1){
            return nil;
        }else{
            MMGCityModel *model=[self.cityArr objectAtIndex:row];
            return model.cityName;
        }
    } else {
        MMGCityModel *model=[self.districtArr objectAtIndex:row];
        return model.cityName;
    }
//    NSLog(@"%@",self.provinceArr);
    return @"faf";
}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 110;
    } else if (component == 1) {
        return 100;
    } else {
        return 110;
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    if (component == 0) {
        self.selectedArray =self.provinceArr;
        if (self.selectedArray.count > 0) {
            MMGCityModel *model=self.selectedArray[row];
            //得到一个当前省份的所有城市
            self.cityArr=[db getCityWithId:model.parent_id];
        } else {
            self.cityArr = nil;
        }
        
        if (self.cityArr.count > 0) {
            //得到一个选择市的全部区域
            MMGCityModel *model=self.cityArr[0];
            self.districtArr=[db getCityWithId:model.parent_id];
        } else {
            self.districtArr = nil;
        }
        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectedRowInComponent:2];
    }
    
    if (component == 1) {
        if (self.selectedArray.count > 0 && self.cityArr.count > 0) {
            MMGCityModel *model=self.cityArr[row];
            //得到一个市的全部区域
            self.districtArr=[db getCityWithId:model.parent_id];
        } else {
            self.districtArr = nil;
        }
        [pickerView selectRow:2 inComponent:2 animated:YES];
    }
    [pickerView reloadComponent:2];
}

- (id)selectedObject {
    return [_pickerViewArray objectAtIndex: [self.pickerView selectedRowInComponent:0]];
}

@end
