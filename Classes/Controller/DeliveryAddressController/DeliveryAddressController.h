//
//  DeliveryAddressController.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface DeliveryAddressController : BaseController

@property(nonatomic,copy) NSString *seleAdrId;

@property (nonatomic, strong) void (^kReceiverBlock)(id model);
@end
