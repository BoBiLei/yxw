//
//  AboutUsController.m
//  CatShopping
//
//  Created by mac on 15/9/28.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "AboutUsController.h"
#import "YHT_NJKWebViewProgress.h"
#import "YHT_NJKWebViewProgressView.h"
//#import "NJKWebViewProgressView.h"
//#import "NJKWebViewProgress.h"
//#import "Macro2.h"
@interface AboutUsController ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end

@implementation AboutUsController{
    UIWebView *_webView;
    NSString *urlStr;
    YHT_NJKWebViewProgressView *_progressView;
    YHT_NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"关于我们"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NewYXHost stringByAppendingString:urlStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[YHT_NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[YHT_NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    UIView *vv=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 2)];
    vv.backgroundColor=[UIColor redColor];
    _progressView.progressBarView=vv;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(YHT_NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
