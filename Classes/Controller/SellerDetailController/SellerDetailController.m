//
//  SellerDetailController.m
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SellerDetailController.h"
#import "SellerDetailCell.h"
#import "ProductDetailController.h"
#import "SellerDetailUserImgCell.h"
#import "SellerDetailUserImgModel.h"
#import "Macro2.h"
NSString *sdIdentifier=@"sellerdetailcell";
@interface SellerDetailController ()<UITableViewDataSource,UITableViewDelegate,SellerDetailDelegate>

@end

@implementation SellerDetailController{
    NSMutableArray *dataArr;
    NSMutableArray *userImgArr;
    UITableView *sellerTableview;
    
    UIImageView *sellerPhoto;
    UILabel *sellerName;
}


-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];

}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self seNavBar];
//    [self setCustomNavigationTitle:@""];
    
    [self requestHttpsForSellerDetail:_sellerId];
}


#pragma mark - init UI
-(void)setUpUi{
    
    userImgArr=[NSMutableArray array];
    
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.image=[UIImage imageNamed:@"login_baground"];
    bagView.userInteractionEnabled=YES;
    
    sellerTableview=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    sellerTableview.showsVerticalScrollIndicator=NO;
    sellerTableview.backgroundView=bagView;
    sellerTableview.dataSource=self;
    sellerTableview.delegate=self;
    [sellerTableview registerNib:[UINib nibWithNibName:@"SellerDetailCell" bundle:nil] forCellReuseIdentifier:sdIdentifier];
    [sellerTableview registerNib:[UINib nibWithNibName:@"SellerDetailUserImgCell" bundle:nil] forCellReuseIdentifier:@"imgcell"];
    sellerTableview.tableHeaderView=[self setUpHeaderView];
    [self.view addSubview:sellerTableview];
}

-(UIView *)setUpHeaderView{
    
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 70)];
    
    //卖家头像
    sellerPhoto=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 70, 70)];
    [headerView addSubview:sellerPhoto];
    
    //卖家昵称
    sellerName=[[UILabel alloc]initWithFrame:CGRectMake(sellerPhoto.frame.origin.x+sellerPhoto.frame.size.width+8, sellerPhoto.frame.origin.y, SCREENSIZE.width-164, 21)];
    sellerName.font=[UIFont systemFontOfSize:14];
    [headerView addSubview:sellerName];
    
    //喜欢按钮
    UIButton *likeBn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    likeBn.backgroundColor=[UIColor lightGrayColor];
    likeBn.layer.cornerRadius=3;
    [likeBn setTitle:@"喜欢" forState:UIControlStateNormal];
    likeBn.titleLabel.font=[UIFont systemFontOfSize:13];
    [likeBn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [likeBn addTarget:self action:@selector(clickLikeBtn) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:likeBn];
    
    likeBn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* likeBn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[likeBn(56)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(likeBn)];
    [NSLayoutConstraint activateConstraints:likeBn_h];
    
    NSArray* likeBn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[likeBn(24)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(likeBn)];
    [NSLayoutConstraint activateConstraints:likeBn_w];
    return headerView;
}

#pragma mark - 点击喜欢按钮
-(void)clickLikeBtn{
    NSLog(@"like click");
}

#pragma mark - TableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return userImgArr.count;
    }else{
        return dataArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        SellerDetailUserImgCell *cell=[tableView dequeueReusableCellWithIdentifier:@"imgcell"];
        if (!cell) {
            cell=[[SellerDetailUserImgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"imgcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        SellerDetailUserImgModel *model=userImgArr[indexPath.row];
        [cell setCellFrameForImgUrl:model];
        return cell.frame.size.height;
    }else{
        return 90;
    }
}

//自定义头部标题
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return nil;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 30)];
        view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 100, 30)];
        label.text = @"店长推荐";
        label.font=[UIFont systemFontOfSize:12];
        [view addSubview:label];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        SellerDetailUserImgCell *cell;
        cell=(SellerDetailUserImgCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (!cell) {
            cell=[[SellerDetailUserImgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"imgcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (indexPath.row<userImgArr.count) {
            SellerDetailUserImgModel *model=userImgArr[indexPath.row];
            [cell setCellFrameForImgUrl:model];
        }
        return cell;
    }else{
        SellerDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:sdIdentifier];
        if (!cell) {
            cell=[[SellerDetailCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        if (indexPath.row<dataArr.count) {
            SellerDetailModel *model=dataArr[indexPath.row];
            [cell reflushDataForModel:model];
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        
    }else{
        SellerDetailModel *model=dataArr[indexPath.row];
        ProductDetailController *productDetail=[[ProductDetailController alloc]init];
        productDetail.hidesBottomBarWhenPushed=YES;
        productDetail.goodId=model.goodsId;
        [self.navigationController pushViewController:productDetail animated:YES];
    }
}

#pragma mark - 店长推荐商品 delegate
-(void)joinShoppingCartForGoodId:(SellerDetailModel *)model{
    SingFMDB *db=[SingFMDB shareFMDB];
    BOOL isExit=[db searchData:model.goodsId];
    if (!isExit) {
        ShoppingCartModel *smodel=[[ShoppingCartModel alloc]init];
        smodel.goodsId=model.goodsId;
        smodel.goodsImgStr=model.goodsImgStr;
        smodel.goodsTitle=model.goodsTitleStr;
        smodel.goodsPrice=model.mmPrice;
        BOOL isOk=[db insertMyGood:smodel];
        if (isOk) {
            [[MMPopupWindow sharedWindow] cacheWindow];
            [MMPopupWindow sharedWindow].touchWildToHide = YES;
            
            MMAlertViewConfig *sheetConfig = [MMAlertViewConfig globalConfig];
            sheetConfig.buttonFontSize=15;
            sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
            
            MMPopupItemHandler block = ^(NSInteger index){
                switch (index) {
                    case 0:
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionGoodDetailGoToShoppingCart object:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
                        [self.navigationController popToRootViewControllerAnimated:NO];
                        break;
                    default:
                        break;
                }
            };
            
            MMPopupBlock completeBlock = ^(MMPopupView *popupView){
                
            };
            NSArray *items =
            @[MMItemMake(@"前往购物车", MMItemTypeNormal, block),
              MMItemMake(@"继续购物", MMItemTypeNormal, block)];
            
            [[[MMAlertView alloc] initWithTitle:@"提示" detail:@"商品成功添加到购物车！" items:items] showWithBlock:completeBlock];
        }
    }else{
        [AppUtils showSuccessMessage:@"购物车已有该商品！" inView:self.view];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForSellerDetail:(NSString *)sellerId{
    [AppUtils showActivityIndicatorView:self.view];
    dataArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"is_nosign":@"1",
                         @"act":@"show_detail",
                         @"id":sellerId
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self setUpUi];
        
        NSArray *imgStrArr=responseObject[@"retData"][@"img_list"];
        for (NSDictionary *imgStrDic in imgStrArr) {
            SellerDetailUserImgModel *model=[[SellerDetailUserImgModel alloc]init];
            [model jsonDataForDictionary:imgStrDic];
            [userImgArr addObject:model];
        }
        
        [self setCustomNavigationTitle:responseObject[@"retData"][@"show"][@"name"]];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost01,responseObject[@"retData"][@"show"][@"logo"]];
            [sellerPhoto setImageWithURL:[NSURL URLWithString:imgStr]];
            
            sellerName.text=responseObject[@"retData"][@"show"][@"name"];
            
            /*******
             店长推荐
             *******/
            NSArray *arr=responseObject[@"retData"][@"recommand_goods"];
            for (NSDictionary *dic in arr) {
                SellerDetailModel *model=[[SellerDetailModel alloc]init];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
        }
        [sellerTableview reloadData];
        [AppUtils dismissActivityIndicatorView:self.view];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
