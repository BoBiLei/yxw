//
//  SellerDetailController.h
//  CatShopping
//
//  Created by mac on 15/9/23.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface SellerDetailController : BaseController

@property (nonatomic,copy) NSString *sellerId;

@end
