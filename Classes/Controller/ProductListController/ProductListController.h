//
//  ProductListController.h
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

typedef NS_ENUM(NSUInteger, ProdectListRequestType) {
    HomePageSearchRequestType,
    HomePageCategoryRequestType,
    HomePageGoodsListRequestType,
    CategoryRequestType
};


@interface ProductListController : BaseController

@property(nonatomic ,copy) NSString *productListId;
@property(nonatomic ,copy) NSString *productListName;

@property(nonatomic ,copy) NSString *searchKeyWord;

@property (nonatomic) ProdectListRequestType requestType;

-(void)requestWithType:(ProdectListRequestType)requestType;

@end
