//
//  ProductListController.m
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProductListController.h"
#import "ProductListCell.h"
#import "ProductDetailController.h"
#import "FillterViewController.h"
#import "FillterIDModel.h"
#import "HttpRequests.h"
#import "MMGSign.h"
#import "Macro2.h"
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

NSString *goodListIdentifier=@"goodlistcell";
@interface ProductListController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,DOPDropDownMenuDataSource,DOPDropDownMenuDelegate>

@property (nonatomic, strong) NSArray *classifys;
@property (nonatomic, strong) NSArray *cates;
@property (nonatomic, strong) NSArray *movices;
@property (nonatomic, strong) NSArray *hostels;
@property (nonatomic, strong) NSArray *areas;

@property (nonatomic, strong) NSArray *sorts;
@property (nonatomic, weak) DOPDropDownMenu *menu;
@end

@implementation ProductListController{
    
    FillterViewController *fillterViewContr;
    MJRefreshAutoNormalFooter *footer;
    
    //筛选的
    UIButton *btn01;
    UIButton *btn02;
    UIButton *btn03;
    UIView *filScrolView;
    UIButton *seletedBtn;
    
    //
    NSMutableArray *dataArr;
    UICollectionView *collectView;
    
    UIView *filterView;
    CGFloat offset;
    
    NSString *moreDataStr; //保存下拉加载跟多的链接
    
    //赛选字段
    NSString *fkeywork;
    NSString *fCategory01;
    NSString *fCategory02;
    NSString *fBrand;
    NSString *fMin_price;
    NSString *fMax_price;
    NSString *forder;
    NSString *forder02;
}

-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    
        UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
        btnlab.text=_productListName;
    
        btnlab.textAlignment = NSTextAlignmentCenter;
        [btnlab setTextColor:[UIColor whiteColor]];
        [self.view addSubview:btnlab];
    
}

-(void)fanhui{
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFillterData:) name:@"REFLUSH_FILLTERDATA" object:nil];
    
    [self seNavBar];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(SCREENSIZE.width-60, 30, 60, 30);
    [rightBtn setTitle:@"筛选" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
   
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [rightBtn addTarget:self action:@selector(clickFilterBtn) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:rightBtn];
    
    fillterViewContr=[[FillterViewController alloc]init];
    fillterViewContr.isBusinessFillter=NO;
     
    [self setUpUI];
    
    [self requestWithType:self.requestType];
}

#pragma mark - Notification Method
-(void)updateFillterData:(NSNotification *)noti{
    [self setCustomNavigationTitle:@"商品筛选"];
    FillterIDModel *model=noti.object;
    [self requestHttpsFilterViewDataForModel:model];
}

//赛选请求
-(void)requestHttpsFilterViewDataForModel:(FillterIDModel *)model{
    if (dataArr.count>0) {
        [dataArr removeAllObjects];
    }
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"keywords":@"",
                         @"category":model.categoryId,
                         @"category2":model.subCategoryId,
                         @"brand":model.brandId,
                         @"min_price":model.min_priceId,
                         @"max_price":model.max_priceId,
                         @"sort":@"",
                         @"order":@""
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"search.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            fkeywork=responseObject[@"retData"][@"keywords"];
            fCategory01=responseObject[@"retData"][@"category"];
            fCategory02=responseObject[@"retData"][@"category2"];
            fBrand=responseObject[@"retData"][@"brand"];
            fMin_price=responseObject[@"retData"][@"min_price"];
            fMax_price=responseObject[@"retData"][@"max_price"];
//            forder=responseObject[@"retData"][@"order"];
        }
        [collectView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}


#pragma mark - clickFilterBtn
-(void)clickFilterBtn{
    [self.navigationController pushViewController:fillterViewContr animated:YES];
}


-(void)requestWithType:(ProdectListRequestType)requestType{
    switch (requestType) {
        case HomePageSearchRequestType:
            [self requestHttpsForSearchProductList:self.searchKeyWord];
            break;
        case HomePageCategoryRequestType:
            [self requestHttpsForHomePageCategory:self.productListId];
            break;
        case HomePageGoodsListRequestType:
            [self requestHttpsForHomePageGoodsList:self.productListId];
            break;
        case CategoryRequestType:
            [self requestHttpsForCategory:self.productListId];
            break;
        default:
            break;
    }
}

#pragma mark - init UI
-(void)setUpUI{
        forder=@"DESC";
        forder02=@"DESC";
        //赛选的View
        UIView *fillterView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 44)];
        [self.view addSubview:fillterView];
        
        UIView *fillterView_line=[[UIView alloc]initWithFrame:CGRectMake(0, 43.5f, SCREENSIZE.width, 0.5f)];
        fillterView_line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [fillterView addSubview:fillterView_line];
        
        //btn01
        btn01=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn01 setTitle:@"默认" forState:UIControlStateNormal];
        [btn01 setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
        btn01.titleLabel.font=[UIFont systemFontOfSize:15];
        btn01.frame=CGRectMake(0, 0, (SCREENSIZE.width-1)/3, 43.5f);
        [fillterView addSubview:btn01];
        btn01.tag=101;
        [btn01 addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *vline01=[[UIView alloc]initWithFrame:CGRectMake(btn01.frame.origin.x+btn01.frame.size.width, 8, 0.5f, 28)];
        vline01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [fillterView addSubview:vline01];
        
        //btn02
        btn02=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn02 setTitle:@"最新" forState:UIControlStateNormal];
        [btn02 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btn02.titleLabel.font=[UIFont systemFontOfSize:15];
        btn02.frame=CGRectMake(vline01.frame.origin.x+vline01.frame.size.width, 0, (SCREENSIZE.width-1)/3, 43.5f);
        [fillterView addSubview:btn02];
        btn02.tag=102;
        [btn02 addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *vline02=[[UIView alloc]initWithFrame:CGRectMake(btn02.frame.origin.x+btn02.frame.size.width, 8, 0.5f, 28)];
        vline02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [fillterView addSubview:vline02];
        
        //btn03
        btn03=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn03 setTitle:@"价格" forState:UIControlStateNormal];
        [btn03 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btn03.titleLabel.font=[UIFont systemFontOfSize:15];
        btn03.frame=CGRectMake(vline02.frame.origin.x+vline02.frame.size.width, 0, (SCREENSIZE.width-1)/3, 43.5f);
        [fillterView addSubview:btn03];
        btn03.tag=103;
        [btn03 addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        //filScrolView
        filScrolView=[[UIView alloc]initWithFrame:CGRectMake(0, 42.5f, btn01.frame.size.width, 1.5f)];
        filScrolView.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        [fillterView addSubview:filScrolView];
        
        //collectView
        dataArr=[NSMutableArray array];
        UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
        collectView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) collectionViewLayout:myLayout];
        [collectView registerNib:[UINib nibWithNibName:@"ProductListCell" bundle:nil] forCellWithReuseIdentifier:goodListIdentifier];
        collectView.backgroundColor=[UIColor clearColor];
        collectView.dataSource=self;
        collectView.delegate=self;
        collectView.scrollsToTop=YES;
        
        //================
        //集成上拉刷新控件
        //================
        //加载更多
        footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [self refreshForPullUp];
        }];
        footer.stateLabel.font = [UIFont systemFontOfSize:14];
        footer.stateLabel.textColor = [UIColor lightGrayColor];
        [footer setTitle:@"" forState:MJRefreshStateIdle];
        [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
        [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
        // 忽略掉底部inset
        footer.triggerAutomaticallyRefreshPercent=0.5;
        collectView.mj_footer = footer;
    
        
        UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
        bagView.userInteractionEnabled=YES;
        bagView.image=[UIImage imageNamed:@"login_baground"];
        collectView.backgroundView=bagView;
        [self.view addSubview:collectView];
}

#pragma mark - 点击筛选按钮（默认、最新、价格）
-(void)filterBtnClick:(id)sender{
    if (dataArr.count<=0) {
        
    }else{
        seletedBtn.backgroundColor=[UIColor whiteColor];
        [seletedBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [seletedBtn setImage:nil forState:UIControlStateNormal];
        seletedBtn.imageEdgeInsets=UIEdgeInsetsMake(0, 0, 0, 0);
        seletedBtn.enabled=YES;
        
        UIButton *btn=(UIButton *)sender;
        if (btn.tag==101) {
            btn.enabled=NO;
            [UIView animateWithDuration:.30 animations:^{
                filScrolView.frame=CGRectMake(0, 42.5f, btn01.frame.size.width, 1.5f);
                [btn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
            } completion:^(BOOL finished) {
                [self requestHttpsFilterForType:@"g.add_time" orderBy:@"DESC"];
            }];
        }else if (btn.tag==102){
            [UIView animateWithDuration:.30 animations:^{
                filScrolView.frame=CGRectMake(btn01.frame.origin.x+btn01.frame.size.width+0.5f, 42.5f, btn01.frame.size.width, 1.5f);
                [btn01 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"price_down"] forState:UIControlStateNormal];
                btn.imageEdgeInsets=UIEdgeInsetsMake(10, 86, 6, -2);
                btn.titleEdgeInsets=UIEdgeInsetsMake(0, -16, 0, 0);
            } completion:^(BOOL finished) {
                if ([forder isEqualToString:@"DESC"]) {
                    [self requestHttpsFilterForType:@"goods_id" orderBy:@"DESC"];
                    [btn setImage:[UIImage imageNamed:@"price_down"] forState:UIControlStateNormal];
                    forder=@"ASC";
                }else{
                    [self requestHttpsFilterForType:@"goods_id" orderBy:@"ASC"];
                    [btn setImage:[UIImage imageNamed:@"price_up"] forState:UIControlStateNormal];
                    forder=@"DESC";
                }
            }];
        }else{
            [UIView animateWithDuration:.30 animations:^{
                filScrolView.frame=CGRectMake(btn02.frame.origin.x+btn02.frame.size.width+0.5f, 42.5f, btn01.frame.size.width, 1.5f);
                [btn01 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:@"price_down"] forState:UIControlStateNormal];
                btn.imageEdgeInsets=UIEdgeInsetsMake(10, 86, 6, -2);
                btn.titleEdgeInsets=UIEdgeInsetsMake(0, -16, 0, 0);
            } completion:^(BOOL finished) {
                if ([forder02 isEqualToString:@"DESC"]) {
                    [self requestHttpsFilterForType:@"shop_price" orderBy:@"DESC"];
                    [btn setImage:[UIImage imageNamed:@"price_down"] forState:UIControlStateNormal];
                    forder02=@"ASC";
                }else{
                    [self requestHttpsFilterForType:@"shop_price" orderBy:@"ASC"];
                    [btn setImage:[UIImage imageNamed:@"price_up"] forState:UIControlStateNormal];
                    forder02=@"DESC";
                }
            }];
        }
        //用另一个button代替当前点击的button
        seletedBtn=btn;
    }
}

#pragma mark - 数据处理相关
#pragma mark 下拉刷新数据
- (void)refreshForPullDown{
    [self requestHttpsMoreProduct:moreDataStr];
    [collectView.mj_header endRefreshing];
}

- (void)refreshForPullUp{
    [self requestHttpsMoreProduct:moreDataStr];
    [collectView.mj_footer endRefreshing];
}

#pragma mark - Request(请求)
//分类请求
-(void)requestHttpsForCategory:(NSString *)productListId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:productListId forKey:@"category"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"search.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCategory:self.productListId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
                NSArray *arr=responseObject[@"retData"][@"good_list"];
                for (NSDictionary *dic in arr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                
                fkeywork=responseObject[@"retData"][@"keywords"];
                fCategory01=responseObject[@"retData"][@"category"];
                fCategory02=responseObject[@"retData"][@"category2"];
                fBrand=responseObject[@"retData"][@"brand"];
                fMin_price=responseObject[@"retData"][@"min_price"];
                fMax_price=responseObject[@"retData"][@"max_price"];
            }
            [collectView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//首页类别请求过来
-(void)requestHttpsForHomePageCategory:(NSString *)productListId{
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:productListId] method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            fkeywork=responseObject[@"retData"][@"keywords"];
            fCategory01=responseObject[@"retData"][@"category"];
            fCategory02=responseObject[@"retData"][@"category2"];
            fBrand=responseObject[@"retData"][@"brand"];
            fMin_price=responseObject[@"retData"][@"min_price"];
            fMax_price=responseObject[@"retData"][@"max_price"];
        }
        [collectView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//搜索请求过来
-(void)requestHttpsForSearchProductList:(NSString *)keywordStr{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:keywordStr forKey:@"keywords"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"search.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForSearchProductList:self.searchKeyWord];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
                NSArray *arr=responseObject[@"retData"][@"good_list"];
                for (NSDictionary *dic in arr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                fkeywork=responseObject[@"retData"][@"keywords"];
                fCategory01=responseObject[@"retData"][@"category"];
                fCategory02=responseObject[@"retData"][@"category2"];
                fBrand=responseObject[@"retData"][@"brand"];
                fMin_price=responseObject[@"retData"][@"min_price"];
                fMax_price=responseObject[@"retData"][@"max_price"];
            }
            [collectView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//首页商品列表的
-(void)requestHttpsForHomePageGoodsList:(NSString *)productListId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:productListId] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForHomePageGoodsList:self.productListId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
                NSArray *arr=responseObject[@"retData"][@"good_list"];
                for (NSDictionary *dic in arr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                fkeywork=responseObject[@"retData"][@"keywords"];
                fCategory01=responseObject[@"retData"][@"category"];
                fCategory02=responseObject[@"retData"][@"category2"];
                fBrand=responseObject[@"retData"][@"brand"];
                fMin_price=responseObject[@"retData"][@"min_price"];
                fMax_price=responseObject[@"retData"][@"max_price"];
            }
            [collectView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//更多请求
-(void)requestHttpsMoreProduct:(NSString *)moreHref{
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:moreHref] method:HttpRequestPosts parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            
            [collectView reloadData];
            
            //
            if (arr.count!=0) {
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
        }else{
            [footer endRefreshingWithNoMoreData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//赛选请求
-(void)requestHttpsFilterForType:(NSString *)type orderBy:(NSString *)orderStr{
    if (dataArr.count>0) {
        [dataArr removeAllObjects];
    }
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"keywords":fkeywork,
                         @"category":fCategory01,
                         @"category2":fCategory02,
                         @"brand":fBrand,
                         @"min_price":fMin_price,
                         @"max_price":fMax_price,
                         @"sort":type,
                         @"order":orderStr
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"search.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            fkeywork=responseObject[@"retData"][@"keywords"];
            fCategory01=responseObject[@"retData"][@"category"];
            fCategory02=responseObject[@"retData"][@"category2"];
            fBrand=responseObject[@"retData"][@"brand"];
            fMin_price=responseObject[@"retData"][@"min_price"];
            fMax_price=responseObject[@"retData"][@"max_price"];
        }
        [collectView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}


#pragma mark - 筛选View
-(void)setUpFilterView{
    filterView=[[UIView alloc]initWithFrame:CGRectMake(0, -SCREENSIZE.height-64, SCREENSIZE.width, SCREENSIZE.height-64)];
    filterView.backgroundColor=[UIColor whiteColor];
    filterView.hidden=YES;
    [self.view addSubview:filterView];
    
    //tap手势
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideBackGround)];
    [collectView addGestureRecognizer:tap];
}

-(void)hideBackGround{
//    [self hideMenu];
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:goodListIdentifier forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[ProductListCell alloc]init];
    }
    
    if (indexPath.row<dataArr.count) {
        ProductListModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREENSIZE.width)/2-14, IPHONE6_6P?265:IPHONE6?246:218);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 8, 0, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductListModel *model;
    if (indexPath.row<dataArr.count) {
        model=dataArr[indexPath.row];
    }
    ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
    prodectDetail.goodId=model.idStr;
    prodectDetail.goodTitle=model.titleStr;
    prodectDetail.goodImgStr=model.imgStr;
    prodectDetail.goodPrice=model.mmPriceStr;
    [self.navigationController pushViewController:prodectDetail animated:YES];
}

#pragma mark - dropmenu delegate
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu
{
    return 3;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column
{
    if (column == 0) {
        return self.classifys.count;
    }else if (column == 1){
        return self.areas.count;
    }else {
        return self.sorts.count;
    }
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 0) {
        return self.classifys[indexPath.row];
    } else if (indexPath.column == 1){
        return self.areas[indexPath.row];
    } else {
        return self.sorts[indexPath.row];
    }
}

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 0 || indexPath.column == 1) {
        return [NSString stringWithFormat:@"ic_filter_category_%ld",(long)indexPath.row];
    }
    return nil;
}

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 0 && indexPath.item >= 0) {
        return [NSString stringWithFormat:@"ic_filter_category_%ld",(long)indexPath.item];
    }
    return nil;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column{
    return 0;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath{
    return nil;
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
