//
//  SubmitOrderController.m
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SubmitOrderController.h"
#import "SubmitOrderCell.h"
#import "OrderPayController.h"
#import "SubmitOrderGoodListCell.h"
#import "ProductDetailController.h"
#import "MMPickerView.h"
#import "DeliveryAddressController.h"
#import "ReceiverModel.h"
#import "UseCouponModel.h"
#import "MMGUseCouponController.h"
#import "MMGCustomPickerView.h"
#import "SubmitOrderReceiverCell.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import "HttpRequests.h"
#import "SingFMDB.h"
#import "Macro2.h"
#import "WelcomeController.h"
@interface SubmitOrderController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SubmitOrderController{
    
    
    CGFloat sFeeFloat;               //计算后的税费
    
    DeliveryAddressController *deliveryAddress;
    MMGUseCouponController *useCoupon;
    
    NSArray *leftDataArr;
    NSMutableArray *rightDataArr;
    UITableView *myTable;
    
    NSString *receiverInfoStr;      //收件人信息
    NSString *couponStr;            //优惠券
    NSString *payTypeStr;           //支付方式
    
    NSMutableArray *addressArr;     //收货地址
    NSMutableArray *stagesTermArr;  //分期付款条件rr
    NSMutableArray *shippingArr;    //配送方式Arr
    NSMutableArray *couponArr;      //优惠券Arr
    NSMutableArray *payTypeArr;     //支付方式Arr
    
    ReceiverModel *addressModel; //用户的收货人信息model
    NSString *addressSeleId;
    UseCouponModel *MMGCouponModel;  //接收优惠券页面选择的model
    
    //==========================================
    //=============提交订单字段所用数据============
    //==========================================
    NSString *sub_stagesTermId;     //分期付款条件ID
    NSString *sub_shipTypeId;       //配送方式ID
    NSString *sub_receiverId;       //收货人ID
    NSString *sub_couponId;         //优惠券ID
    NSString *sub_couponMoney;      //优惠券金额
    NSString *sub_payTypeId;        //支付方式ID
    
    UILabel *payLabel;
    
    UIView *footView;
    CGRect oldFrame;
    TTTAttributedLabel *label01;
    TTTAttributedLabel *stageFeelabel;
    NSString *oldStageFee;  //原来还原分期手续费（选择优惠券时）
    TTTAttributedLabel *orderTotalLabel;
    TTTAttributedLabel *evePayLabel;    //每期应付
    TTTAttributedLabel *couponLabel;    //使用优惠券
    TTTAttributedLabel *shuiFeeLabel;
    
    //提交订单的优惠券id、金额
    NSString *subOrderCouponId;
    NSString *subOrderCouponMoney;
}



-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"提交订单";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self seNavBar];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
//    [self setCustomNavigationTitle:@"提交订单"];
    
    oldStageFee=self.stageFee;
    subOrderCouponId=@"0";
    subOrderCouponMoney=@"0";
    
    deliveryAddress=[[DeliveryAddressController alloc]init];
    
    useCoupon=[[MMGUseCouponController alloc]init];
    //请求完成后加载数据
    [self requestHttpsReceiverAndCouponInfo];
    
}

#pragma mark - Request（默认收件人信息、优惠券信息请求）
-(void)requestHttpsReceiverAndCouponInfo{
    addressArr=[NSMutableArray array];
    stagesTermArr=[NSMutableArray array];
    shippingArr=[NSMutableArray array];
    couponArr=[NSMutableArray array];
    payTypeArr=[NSMutableArray array];
    rightDataArr=[NSMutableArray array];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];

    NSDictionary *dict=@{
                         @"is_phone":@"1",
                        @"step":@"checkout2",
                        @"fenqi":self.stageSum,
                        @"id":self.goodsIDArr,
                        @"is_nosign":@"1",
                        @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@""
                         
                         };
    
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //分期付款条件
            for (NSDictionary *dic in responseObject[@"retData"][@"sh_array"]) {
                SubmitOrder_SeletedTypeModel *model=[[SubmitOrder_SeletedTypeModel alloc]init];
                model.typeId=dic[@"key"];
                model.typeName=dic[@"value"];
                [stagesTermArr addObject:model];
            }
            [rightDataArr addObject:stagesTermArr[0]];
            
            //配送方式
            for (NSDictionary *dic in responseObject[@"retData"][@"shipping_list"]) {
                SubmitOrder_SeletedTypeModel *model=[[SubmitOrder_SeletedTypeModel alloc]init];
                model.typeId=dic[@"shipping_id"];
                model.typeName=dic[@"shipping_name"];
                [shippingArr addObject:model];
            }
            
            //获取默认收货地址（没有则空）
            NSArray *defaultAdd=responseObject[@"retData"][@"default_consignee_t"];
            if (defaultAdd.count<=0) {
                
            }else{
                NSDictionary *defaultAddressDic=responseObject[@"retData"][@"default_consignee_t"];
                ReceiverModel *model=[[ReceiverModel alloc]init];
                model.receiverId=defaultAddressDic[@"address_id"];
                addressSeleId=[NSString stringWithFormat:@"%@",defaultAddressDic[@"address_id"]];
                model.receiverName=defaultAddressDic[@"consignee"];
                model.phoneNum=defaultAddressDic[@"mobile"];
                model.address=defaultAddressDic[@"address"];
                addressModel=[ReceiverModel new];
                addressModel=model;
            }
            
            //获取收货地址
            for (NSDictionary *addressDic in responseObject[@"retData"][@"consignee_list"]) {
                ReceiverModel *model=[[ReceiverModel alloc]init];
                [model jsonDataForDictionary:addressDic];
                [addressArr addObject:model];
            }
            
            NSString *defaultReceStr;
            if (addressArr.count<=0) {
                defaultReceStr=@"未添加";
            }else{
                defaultReceStr=@"";
            }
            if (![responseObject[@"retData"][@"shipping"] isKindOfClass:[NSNull class]]) {
                NSDictionary *shipDic=responseObject[@"retData"][@"shipping"];
                SubmitOrder_SeletedTypeModel *defaultShippingModel=[[SubmitOrder_SeletedTypeModel alloc]init];
                defaultShippingModel.typeId=shipDic[@"shipping_id"];
                defaultShippingModel.typeName=shipDic[@"shipping_name"];
                [rightDataArr addObject:@[defaultShippingModel,defaultReceStr]];
            }else{
                [rightDataArr addObject:@[shippingArr[0],defaultReceStr]];
            }
            
            
            //优惠券
            NSArray *couArr=responseObject[@"retData"][@"bonus_list"];
            if (couArr.count>0) {
                for (NSDictionary *dic in couArr) {
                    UseCouponModel *model=[[UseCouponModel alloc]init];
                    model.couponID=dic[@"bonus_id"];
                    model.couponMoney=dic[@"type_money"];
                    [couponArr addObject:model];
                }
            }else{
                UseCouponModel *model=[[UseCouponModel alloc]init];
                model.couponID=@"0";
                model.couponMoney=@"0";
                [couponArr addObject:model];
            }
            
            //支付方式
            for (NSDictionary *dic in responseObject[@"retData"][@"payment_list"]) {
                SubmitOrder_SeletedTypeModel *model=[[SubmitOrder_SeletedTypeModel alloc]init];
                model.typeId=dic[@"pay_id"];
                model.typeName=dic[@"pay_name"];
                [payTypeArr addObject:model];
            }
            
            UseCouponModel *defaultCouponModel=[[UseCouponModel alloc]init];
            defaultCouponModel.couponID=@"0";
            defaultCouponModel.couponMoney=@"不使用";
            
            SubmitOrder_SeletedTypeModel *defaultPayModel=[[SubmitOrder_SeletedTypeModel alloc]init];
            defaultPayModel.typeId=@"5";
            defaultPayModel.typeName=@"支付宝";
            [rightDataArr addObject:@[defaultCouponModel,defaultPayModel]];
            
            leftDataArr=@[@[@"分期付款条件"],@[@"配送方式",@"收货人信息"],@[@"优惠券",@"支付方式"],_receiveDataArr];
            [rightDataArr addObject:@[@"",@""]];
            
            [self setUpUI];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - Request提交订单请求
-(void)requestHttpsSubmitOrder{
    
    //下单成功删除购物车
    [[SingFMDB shareFMDB] clearAllCollection];
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
    
    //
    
    NSString *recId=addressModel.receiverId;
    
    NSString *goodCount=[NSString stringWithFormat:@"%lu",(unsigned long)self.goodsIDArr.count];
    
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                        @"is_phone":@"1",
                        @"step":@"done2",
                        @"fenqi":self.stageSum,
                        @"sel_goods":self.goodsIDArr,
                        @"trial_info":sub_stagesTermId,
                        @"shipping":sub_shipTypeId,
                        @"payment":sub_payTypeId,
                        @"total_product_price":self.totalPrice,
                        @"tt_per_price":goodCount,
                        @"fenqi_fee":self.stageFee,
                        @"consignee_id":recId,
                        @"bonus_money":subOrderCouponMoney,
                        @"bonus":subOrderCouponId,
                        @"surplus":@"0",
                        @"is_nosign":@"1",
                        @"yx_uid":[AppUtils getValueWithKey:User_ID],
                        @"yx_uname":uSr!=nil?uSr:@"",
                        @"yx_phone":pSr!=nil?pSr:@""
                        };
    NSLog(@"%@",dict);
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *str=responseObject[@"retData"][@"href"];
            NSMutableString *testStr=[NSMutableString stringWithString:str];
            NSRange range = [testStr rangeOfString:@"order="];
            if (range.location != NSNotFound) {
                NSString *str01=[testStr substringFromIndex:range.location+range.length];
                NSRange range01 = [str01 rangeOfString:@"&is_phone=1"];
                if (range01.location != NSNotFound) {
                    NSString * orderId = [str01 substringToIndex:range01.location];
                    OrderPayController *orderPay=[[OrderPayController alloc]init];
                    orderPay.orderId=orderId;
                    orderPay.selectedPayId=sub_payTypeId;
                    [self.navigationController pushViewController:orderPay animated:YES];
                }
            }
            
//            //下单成功删除购物车
//            [[SingFMDB shareFMDB] clearAllCollection];
//            [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"message"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - init View
-(void)setUpUI{
    
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 64, SCREENSIZE.width-16, 38)];
    imgv.contentMode=UIViewContentModeScaleToFill;
    imgv.image=[UIImage imageNamed:@"submit_order01"];
    [self.view addSubview:imgv];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 118, SCREENSIZE.width-16, SCREENSIZE.height-200) style:UITableViewStyleGrouped];
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"SubmitOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [myTable registerNib:[UINib nibWithNibName:@"SubmitOrderGoodListCell" bundle:nil] forCellReuseIdentifier:@"listcell"];
    [myTable registerNib:[UINib nibWithNibName:@"SubmitOrderReceiverCell" bundle:nil] forCellReuseIdentifier:@"addresscell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:myTable];
    
    //footView
    footView=[UIView new];
    CGRect footFrame=CGRectMake(0, 64, myTable.width, [_stageSum isEqualToString:@"1"]?88:112);
    footView.frame=footFrame;
    footView.backgroundColor=[UIColor whiteColor];
    myTable.tableFooterView=footView;
    oldFrame=footFrame;
    
    //总金额
    label01 = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(12, 6, footView.frame.size.width-24, 18)];
    NSString *label01TextStr=[NSString stringWithFormat:@"%@件商品，总金额：%@元",self.seleGoodSum,self.totalPrice];
    NSRange qwRange=[label01TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.totalPrice]];
    NSMutableAttributedString *hintLabel01 = [[NSMutableAttributedString alloc] initWithString:label01TextStr];
    [hintLabel01 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:qwRange];
    [label01 setText:hintLabel01];
    label01.lineSpacing = 8;
    label01.font=[UIFont systemFontOfSize:14];
    [label01 setBackgroundColor:[UIColor clearColor]];
    [label01 setTextAlignment:NSTextAlignmentRight];
    label01.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:label01];
    
    //分期手续费
    stageFeelabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18)];
    stageFeelabel.hidden=[_stageSum isEqualToString:@"1"]?YES:NO;
    NSString *label02TextStr=[NSString stringWithFormat:@"分期手续费：%@元",self.stageFee];
    NSRange label02Range=[label02TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.stageFee]];
    NSMutableAttributedString *hintLabel02 = [[NSMutableAttributedString alloc] initWithString:label02TextStr];
    [hintLabel02 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label02Range];
    [stageFeelabel setText:hintLabel02];
    stageFeelabel.lineSpacing = 8;
    stageFeelabel.font=[UIFont systemFontOfSize:14];
    [stageFeelabel setBackgroundColor:[UIColor clearColor]];
    [stageFeelabel setTextAlignment:NSTextAlignmentRight];
    stageFeelabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:stageFeelabel];
    
    //税费
    sFeeFloat=self.totalPrice.floatValue*_shuiFeeFloat;
    shuiFeeLabel = [TTTAttributedLabel new];
    CGRect shuiFeeFrame=[_stageSum isEqualToString:@"1"]?CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18):CGRectMake(12, stageFeelabel.frame.origin.y+stageFeelabel.frame.size.height+2, stageFeelabel.frame.size.width, 18);
    shuiFeeLabel.frame=shuiFeeFrame;
    if(sFeeFloat>=9000){
        sFeeFloat=9000;
    }
    NSString *sfTextStr=[NSString stringWithFormat:@"税 费：%.f元",sFeeFloat];
    NSRange sfRange=[sfTextStr rangeOfString:[NSString stringWithFormat:@"%.f元",sFeeFloat]];
    NSMutableAttributedString *hintlabelsf = [[NSMutableAttributedString alloc] initWithString:sfTextStr];
    [hintlabelsf addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:sfRange];
    [shuiFeeLabel setText:hintlabelsf];
    shuiFeeLabel.lineSpacing = 8;
    shuiFeeLabel.font=[UIFont systemFontOfSize:14];
    [shuiFeeLabel setBackgroundColor:[UIColor clearColor]];
    [shuiFeeLabel setTextAlignment:NSTextAlignmentRight];
    shuiFeeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:shuiFeeLabel];
    
    //订单总金额
    orderTotalLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(12, shuiFeeLabel.frame.origin.y+shuiFeeLabel.frame.size.height+2, shuiFeeLabel.frame.size.width, 18)];
    NSString *label03TextStr=[NSString stringWithFormat:@"订单总金额：%@元",self.orderPrice];
    NSRange label03Range=[label03TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.orderPrice]];
    NSMutableAttributedString *hintlabel03 = [[NSMutableAttributedString alloc] initWithString:label03TextStr];
    [hintlabel03 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label03Range];
    [orderTotalLabel setText:hintlabel03];
    orderTotalLabel.lineSpacing = 8;
    orderTotalLabel.font=[UIFont systemFontOfSize:14];
    [orderTotalLabel setBackgroundColor:[UIColor clearColor]];
    [orderTotalLabel setTextAlignment:NSTextAlignmentRight];
    orderTotalLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:orderTotalLabel];
    
    //每期应付
    evePayLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(12, orderTotalLabel.frame.origin.y+orderTotalLabel.frame.size.height+2, orderTotalLabel.frame.size.width, 18)];
    NSString *label04TextStr=[NSString stringWithFormat:@"分%@期(每期应付%@元)",self.stageSum,self.everyStagePrice];
    NSRange label04Range=[label04TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.everyStagePrice]];
    NSMutableAttributedString *hintlabel04 = [[NSMutableAttributedString alloc] initWithString:label04TextStr];
    [hintlabel04 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label04Range];
    [evePayLabel setText:hintlabel04];
    evePayLabel.lineSpacing = 8;
    evePayLabel.font=[UIFont systemFontOfSize:14];
    [evePayLabel setBackgroundColor:[UIColor clearColor]];
    [evePayLabel setTextAlignment:NSTextAlignmentRight];
    evePayLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:evePayLabel];
    
    //优惠券
    couponLabel = [TTTAttributedLabel new];
    couponLabel.font=[UIFont systemFontOfSize:14];
    [couponLabel setBackgroundColor:[UIColor clearColor]];
    [couponLabel setTextAlignment:NSTextAlignmentRight];
    couponLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [footView addSubview:couponLabel];
    
    //bottom view
    UIView *btmView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-64, SCREENSIZE.width, 48)];
    btmView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:btmView];
    
    //button
    UIButton *submitBtn=[[UIButton alloc]init];
    submitBtn.layer.cornerRadius=2;
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [submitBtn addTarget:self action:@selector(goOrderPayController) forControlEvents:UIControlEventTouchUpInside];
    [btmView addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* submitBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[submitBtn(110)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_h];
    
    NSArray* submitBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-7-[submitBtn]-7-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_w];
    
    //pay label
    payLabel=[[UILabel alloc]init];
    payLabel.font=[UIFont systemFontOfSize:13];
    payLabel.adjustsFontSizeToFitWidth=YES;
    payLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    
    CGFloat stageFeeFloat=self.stageFee.floatValue;
    CGFloat everystageFeeFloat=self.everyStagePrice.floatValue;
    CGFloat payFeeFloat=stageFeeFloat+everystageFeeFloat+sFeeFloat;
    NSString *payLStr=[_stageSum isEqualToString:@"1"]?[NSString stringWithFormat:@"本期应付：%.f元（含税费）",payFeeFloat]:[NSString stringWithFormat:@"本期应付：%.f元（含全部手续费、税费）",payFeeFloat];
    payLabel.text=payLStr;
    [btmView addSubview:payLabel];
    payLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* payLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[payLabel]-8-[submitBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(payLabel,submitBtn)];
    [NSLayoutConstraint activateConstraints:payLabel_h];
    
    NSArray* payLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[payLabel]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(payLabel)];
    [NSLayoutConstraint activateConstraints:payLabel_w];
    
}

#pragma mark - 点击提交订单
-(void)goOrderPayController{
  //  if(![AppUtils loginState])
    //{
//        WelcomeController *login=[[WelcomeController alloc]init];
//        login.isShop=YES;
//        UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
//        [loginNav.navigationBar setShadowImage:[UIImage new]];
//        //隐藏tabbar
//        login.hidesBottomBarWhenPushed = YES;
//        [((UINavigationController *)tabBarController.selectedViewController) presentViewController:loginNav animated:YES completion:nil];
   // }
    
    if (sub_stagesTermId==nil) {
        [AppUtils showSuccessMessage:@"请选择分期付款条件" inView:self.view];
    }else if (sub_shipTypeId==nil){
        [AppUtils showSuccessMessage:@"请选择配送方式" inView:self.view];
    }else if (addressModel.receiverId==nil){
        [AppUtils showSuccessMessage:@"收货人信息不能为空" inView:self.view];
    }else if (sub_payTypeId==nil){
        [AppUtils showSuccessMessage:@"请选择支付方式" inView:self.view];
    }else{
        [self requestHttpsSubmitOrder];
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return leftDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)leftDataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==3) {
        return 77;
    }
    else if (indexPath.section==1){
        if (indexPath.row==1) {
            SubmitOrderReceiverCell *cell=[tableView dequeueReusableCellWithIdentifier:@"addresscell"];
            if (!cell) {
                cell=[[SubmitOrderReceiverCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addresscell"];
            }
            [cell reflushDataForModel:addressModel];
            return cell.frame.size.height;
        }else{
            return 44;
        }
    }
    else{
        return 44;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==3) {
        return 34;
    }else{
        return 4;
    }
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==3) {
        UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 34)];
        headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        
        //订单编号
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width/2+20, headerSectionView.frame.size.height)];
        label.font=[UIFont systemFontOfSize:13];
        label.textColor=[UIColor colorWithHexString:@"#282828"];
        label.text=@"商品列表";
        [headerSectionView addSubview:label];
        
        return headerSectionView;
    }else{
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==3) {
        SubmitOrderGoodListCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"listcell"];
        if (!cell) {
            cell=[[SubmitOrderGoodListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"listcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        ShoppingCartModel *model=_receiveDataArr[indexPath.row];
        [cell reflushModeDate:model];
        return  cell;
    }
    if (indexPath.section==1) {
        if (indexPath.row==1) {
            SubmitOrderReceiverCell *cell;
            cell=(SubmitOrderReceiverCell *)[tableView cellForRowAtIndexPath:indexPath];
            if (!cell) {
                cell=[[SubmitOrderReceiverCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addresscell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDataForModel:addressModel];
            return  cell;
        }else{
            SubmitOrderCell *cell;
            cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (!cell) {
                cell=[[SubmitOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=leftDataArr[indexPath.section][indexPath.row];
            //默认配送方式
            SubmitOrder_SeletedTypeModel *model=rightDataArr[indexPath.section][indexPath.row];
            cell.rightLabel.text=model.typeName;
            sub_shipTypeId=model.typeId;
            return  cell;
        }
        
    }
    else if(indexPath.section==0){
        SubmitOrderCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[SubmitOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text=leftDataArr[indexPath.section][indexPath.row];

        //默认分期付款条件
        SubmitOrder_SeletedTypeModel *model=rightDataArr[indexPath.row];
        cell.rightLabel.text=model.typeName;
        sub_stagesTermId=model.typeId;
        return  cell;
    }else{
        SubmitOrderCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[SubmitOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text=leftDataArr[indexPath.section][indexPath.row];
        if(indexPath.row==0){
            UseCouponModel *model=rightDataArr[indexPath.section][indexPath.row];
            cell.rightLabel.text=model.couponMoney;
        }else{
            //默认支付方式
            SubmitOrder_SeletedTypeModel *model=rightDataArr[indexPath.section][indexPath.row];
            cell.rightLabel.text=model.typeName;
            sub_payTypeId=model.typeId;
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SubmitOrderCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    deliveryAddress.seleAdrId=addressSeleId;
    
    if (indexPath.section==3) {
        ShoppingCartModel *model=_receiveDataArr[indexPath.row];
            ProductDetailController *productDetail=[[ProductDetailController alloc]init];
        productDetail.goodId=model.goodsId;
        [self.navigationController pushViewController:productDetail animated:YES];
    }
    if (indexPath.section==0) {
        
        
        MMGCustomPickerView *pick=[[MMGCustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:stagesTermArr selectedTypeId:sub_stagesTermId];
        [pick showPickerViewCompletion:^(id selectedObject) {
            SubmitOrder_SeletedTypeModel *model=selectedObject;
            cell.rightLabel.text = model.typeName;
            sub_stagesTermId=[NSString stringWithFormat:@"%@",model.typeId];
        }];
    }else if (indexPath.section==1){
        if (indexPath.row==0) {
            MMGCustomPickerView *pick=[[MMGCustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:shippingArr selectedTypeId:sub_shipTypeId];
            [pick showPickerViewCompletion:^(id selectedObject) {
                SubmitOrder_SeletedTypeModel *model=selectedObject;
                cell.rightLabel.text = model.typeName;
                sub_shipTypeId=[NSString stringWithFormat:@"%@",model.typeId];
            }];
        }else{
            deliveryAddress.kReceiverBlock = ^(id model){
                addressModel=model;
                sub_receiverId=addressModel.receiverId;
                addressSeleId=addressModel.receiverId;
                [myTable reloadData];//刷新表格
            };
            [self.navigationController pushViewController:deliveryAddress animated:YES];
        }
    }else if (indexPath.section==2){
        if(indexPath.row==0){
            self.stageFee=oldStageFee;
            useCoupon.goodPrice=self.orderPrice;
            useCoupon.kCouponBlock = ^(id model){
                MMGCouponModel=(UseCouponModel *)model;
                cell.rightLabel.text=MMGCouponModel.couponMoney;
                [self upDatePriceDataForModel:MMGCouponModel];
            };
            [self.navigationController pushViewController:useCoupon animated:YES];
        }else{
            MMGCustomPickerView *pick=[[MMGCustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:payTypeArr selectedTypeId:sub_payTypeId];
            [pick showPickerViewCompletion:^(id selectedObject) {
                SubmitOrder_SeletedTypeModel *model=selectedObject;
                cell.rightLabel.text = model.typeName;
                sub_payTypeId=[NSString stringWithFormat:@"%@",model.typeId];
            }];
        }
    }
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}


#pragma mark - 更新当前应付金额
-(void)upDatePriceDataForModel:(UseCouponModel *)model{
    NSLog(@"%@--%@",model.couponID,model.couponMoney);
    if ([model.couponMoney isEqualToString:@"不使用"]) {
        couponLabel.frame=CGRectMake(0, 0, 0, 0);
        
        label01.frame=CGRectMake(12, 6, footView.frame.size.width-24, 18);
        
        stageFeelabel.frame = CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18);
        
        CGRect shuiFeeFrame=[_stageSum isEqualToString:@"1"]?CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18):CGRectMake(12, stageFeelabel.frame.origin.y+stageFeelabel.frame.size.height+2, stageFeelabel.frame.size.width, 18);
        shuiFeeLabel.frame=shuiFeeFrame;
        
        orderTotalLabel.frame = CGRectMake(12, shuiFeeLabel.frame.origin.y+shuiFeeLabel.frame.size.height+2, shuiFeeLabel.frame.size.width, 18);
        
        evePayLabel.frame = CGRectMake(12, orderTotalLabel.frame.origin.y+orderTotalLabel.frame.size.height+2, orderTotalLabel.frame.size.width, 18);
        
        CGRect newFrame=footView.frame;
        newFrame.size.height=[_stageSum isEqualToString:@"1"]?88:112;
        footView.frame=newFrame;
        
        //
        subOrderCouponId=@"0";
        subOrderCouponMoney=@"0";
        //
        NSString *label02TextStr=[NSString stringWithFormat:@"分期手续费：%@元",self.stageFee];
        NSRange label02Range=[label02TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.stageFee]];
        NSMutableAttributedString *hintLabel02 = [[NSMutableAttributedString alloc] initWithString:label02TextStr];
        [hintLabel02 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label02Range];
        [stageFeelabel setText:hintLabel02];
        
        //
        NSString *label03TextStr=[NSString stringWithFormat:@"订单总金额：%@元",self.orderPrice];
        NSRange label03Range=[label03TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.orderPrice]];
        NSMutableAttributedString *hintlabel03 = [[NSMutableAttributedString alloc] initWithString:label03TextStr];
        [hintlabel03 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label03Range];
        [orderTotalLabel setText:hintlabel03];
        
        //
        NSString *label04TextStr=[NSString stringWithFormat:@"分%@期(每期应付%@元)",self.stageSum,self.everyStagePrice];
        NSRange label04Range=[label04TextStr rangeOfString:[NSString stringWithFormat:@"%@元",self.everyStagePrice]];
        NSMutableAttributedString *hintlabel04 = [[NSMutableAttributedString alloc] initWithString:label04TextStr];
        [hintlabel04 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label04Range];
        [evePayLabel setText:hintlabel04];
        
        //
        CGFloat stageFeeFloat=self.stageFee.floatValue;
        CGFloat everystageFeeFloat=self.everyStagePrice.floatValue;
        CGFloat payFeeFloat=stageFeeFloat+everystageFeeFloat+sFeeFloat;
        NSString *payLStr=[_stageSum isEqualToString:@"1"]?[NSString stringWithFormat:@"本期应付：%.f元（含税费）",payFeeFloat]:[NSString stringWithFormat:@"本期应付：%.f元（含全部手续费、税费）",payFeeFloat];
        payLabel.text=payLStr;
    }else{
        //
        subOrderCouponId=model.couponID;
        CGFloat couponMoneyFloat=model.couponMoney.floatValue;;
        subOrderCouponMoney=[NSString stringWithFormat:@"%.f",couponMoneyFloat];
        NSLog(@"%@",subOrderCouponMoney);
        
        //
        label01.frame=CGRectMake(12, 6, footView.frame.size.width-24, 18);
        
        couponLabel.frame=CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18);
        
        stageFeelabel.frame = [_stageSum isEqualToString:@"1"]?CGRectMake(0, 0, 0, 0):CGRectMake(12, couponLabel.frame.origin.y+couponLabel.frame.size.height+2, couponLabel.frame.size.width, 18);
        
        CGRect shuiFeeFrame=[_stageSum isEqualToString:@"1"]?CGRectMake(12, couponLabel.frame.origin.y+couponLabel.frame.size.height+2, couponLabel.frame.size.width, 18):CGRectMake(12, stageFeelabel.frame.origin.y+stageFeelabel.frame.size.height+2, stageFeelabel.frame.size.width, 18);
        shuiFeeLabel.frame=shuiFeeFrame;
        
        orderTotalLabel.frame = CGRectMake(12, shuiFeeLabel.frame.origin.y+shuiFeeLabel.frame.size.height+2, shuiFeeLabel.frame.size.width, 18);
        
        evePayLabel.frame = CGRectMake(12, orderTotalLabel.frame.origin.y+orderTotalLabel.frame.size.height+2, orderTotalLabel.frame.size.width, 18);
        
        CGRect newFrame=footView.frame;
        newFrame.size.height=evePayLabel.frame.origin.y+evePayLabel.frame.size.height+8;
        footView.frame=newFrame;
        
        //重新设置frame
        /*
        couponLabel.frame=CGRectMake(12, label01.frame.origin.y+label01.frame.size.height+2, label01.frame.size.width, 18);
        
        
        stageFeelabel.frame = CGRectMake(12, couponLabel.frame.origin.y+couponLabel.frame.size.height+2, couponLabel.frame.size.width, 18);
        
        orderTotalLabel.frame = CGRectMake(12, stageFeelabel.frame.origin.y+stageFeelabel.frame.size.height+2, stageFeelabel.frame.size.width, 18);
        
        evePayLabel.frame = CGRectMake(12, orderTotalLabel.frame.origin.y+orderTotalLabel.frame.size.height+2, orderTotalLabel.frame.size.width, 18);
        
        CGRect newFrame=footView.frame;
        newFrame.size.height=evePayLabel.frame.origin.y+evePayLabel.frame.size.height+8;
        footView.frame=newFrame;*/
        
        
        couponLabel.text=subOrderCouponMoney; //
        
        //选择的优惠券金额
        CGFloat couponFeeFloat=model.couponMoney.floatValue;
        
        //分期手续费
        CGFloat stageFeeFloat;
        if([self.stageSum isEqualToString:@"1"]){
            stageFeeFloat=0;
        }else{
           stageFeeFloat=self.stageFee.integerValue-(couponFeeFloat*self.stageSum.integerValue*0.01);
        }
        
        //判断优惠券金额不能大于订单金额的10%
        CGFloat seleCouponMoneyFloat=subOrderCouponMoney.floatValue;
        CGFloat orderPriceFloat=self.orderPrice.floatValue;
        CGFloat orderPriceFV=orderPriceFloat*0.1;
        if(seleCouponMoneyFloat>orderPriceFV){
            subOrderCouponMoney=[NSString stringWithFormat:@"%.f",orderPriceFV];
        }
        
        //优惠券金额
        NSString *couponLabelTextStr=[NSString stringWithFormat:@"优惠券：- %@元",subOrderCouponMoney];
        NSRange couponLabelRange=[couponLabelTextStr rangeOfString:[NSString stringWithFormat:@"- %@元",subOrderCouponMoney]];
        NSMutableAttributedString *hintcouponLabel = [[NSMutableAttributedString alloc] initWithString:couponLabelTextStr];
        [hintcouponLabel addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:couponLabelRange];
        [couponLabel setText:hintcouponLabel];
        
        //重新设置分期手续费（提交订单用）
        
        self.stageFee=[NSString stringWithFormat:@"%.f",stageFeeFloat];
        NSString *stageFeeFloatStr=[NSString stringWithFormat:@"分期手续费：%.f元",stageFeeFloat];
        NSRange stRange=[stageFeeFloatStr rangeOfString:[NSString stringWithFormat:@"%.f元",stageFeeFloat]];
        NSMutableAttributedString *hintLabel02 = [[NSMutableAttributedString alloc] initWithString:stageFeeFloatStr];
        [hintLabel02 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:stRange];
        [stageFeelabel setText:hintLabel02];
        
        //刷新订单总金额
        CGFloat orderTotalFeeFloat=self.totalPrice.floatValue+stageFeeFloat-subOrderCouponMoney.floatValue;
        NSString *label03TextStr=[NSString stringWithFormat:@"订单总金额：%.f元",orderTotalFeeFloat];
        NSRange label03Range=[label03TextStr rangeOfString:[NSString stringWithFormat:@"%.f元",orderTotalFeeFloat]];
        NSMutableAttributedString *hintlabel03 = [[NSMutableAttributedString alloc] initWithString:label03TextStr];
        [hintlabel03 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label03Range];
        [orderTotalLabel setText:hintlabel03];
        
        //刷新每期应付
        NSInteger eveStagePayFloat=(self.totalPrice.floatValue-subOrderCouponMoney.floatValue)/self.stageSum.integerValue;
        NSLog(@"%ld",(long)eveStagePayFloat);
        NSString *eveStagePayStr=[NSString stringWithFormat:@"分%@期(每期应付%ld元)",self.stageSum,(long)eveStagePayFloat];
        NSRange label04Range=[eveStagePayStr rangeOfString:[NSString stringWithFormat:@"%ld元",(long)eveStagePayFloat]];
        NSMutableAttributedString *hintlabel04 = [[NSMutableAttributedString alloc] initWithString:eveStagePayStr];
        [hintlabel04 addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#EC0000"] CGColor] range:label04Range];
        [evePayLabel setText:hintlabel04];
        
        //刷新当前应付
        CGFloat payFeeFloat=stageFeeFloat+eveStagePayFloat+sFeeFloat;
        NSString *payLStr=[_stageSum isEqualToString:@"1"]?[NSString stringWithFormat:@"本期应付：%.f元（含税费）",payFeeFloat]:[NSString stringWithFormat:@"本期应付：%.f元（含全部手续费、税费）",payFeeFloat];
        payLabel.text=payLStr;
    }
    myTable.tableFooterView=footView;
}

@end
