//
//  CustomPickerView.m
//  MMG
//
//  Created by mac on 15/10/22.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGCustomPickerView.h"
#import "SubmitOrderController.h"

@interface MMGCustomPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIPickerView *customPickerView;
@property (nonatomic, strong) NSArray *dataArray;
@property (copy) void (^onDismissCompletion)(NSString *);
@property (copy) NSString *(^objectToStringConverter)(id object);

@end

@implementation MMGCustomPickerView


#pragma mark - Init PickerView
-(id)initWithPickerViewInView:(UIView *)view
                    withArray:(NSArray *)array
               selectedTypeId:(NSString *)typeId{
    self = [super init];
    if (self) {
        
        _dataArray = array;
        
        [self setFrame: view.bounds];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //半透明黑色背景
        _backgroundView = [[UIView alloc] initWithFrame:view.bounds];
        [_backgroundView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
        [_backgroundView setAlpha:0.0];
        [_backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
        [self addSubview:_backgroundView];
        
        //PickerView Container with top bar
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _backgroundView.height - 236.0, self.width, 236.0)];
        _containerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_containerView];
        
        //ToolbarBackgroundColor - Black
        UIColor *toolbarBackgroundColor = [[UIColor alloc] initWithCGColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8].CGColor];
        
        //Top bar view
        UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _containerView.width, 44.0)];
        [_containerView addSubview:topBarView];
        [topBarView setBackgroundColor:[UIColor whiteColor]];
        
        
        UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:topBarView.frame];
        [_containerView addSubview:pickerToolBar];
        
        CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        if (iOSVersion < 7.0) {
            pickerToolBar.tintColor = toolbarBackgroundColor;
        }else{
            [pickerToolBar setBackgroundColor:toolbarBackgroundColor];
            
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
            pickerToolBar.barTintColor = toolbarBackgroundColor;
#endif
        }
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.width-60, 4, 40, 40)];
        [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
        [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
        [closeBtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
        closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
        [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
        pickerToolBar.items = @[flexibleSpace, barButtonItem];
        
        //Add pickerView
        _customPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, topBarView.height, self.width, _containerView.height-topBarView.height)];
        [_customPickerView setDelegate:self];
        [_customPickerView setDataSource:self];
        [_containerView addSubview:_customPickerView];
        
        [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
        
        //Set selected row
        int seletedRow=0;
        for (int i=0; i<_dataArray.count; i++) {
            SubmitOrder_SeletedTypeModel *model=_dataArray[i];
            NSString *seleStr=[NSString stringWithFormat:@"%@",model.typeId];
            if ([seleStr isEqualToString:typeId]) {
                seletedRow=i;
            }
        }
        [_customPickerView selectRow:seletedRow inComponent:0 animated:YES];
        [view addSubview:self];
    }
    return self;
}

#pragma mark - Show Methods
-(void)showPickerViewCompletion:(void (^)(id))completion{
    [self setPickerHidden:NO callBack:nil];
    self.onDismissCompletion = completion;
}

-(void)showPickerView{
    [self setPickerHidden:NO callBack:nil];
}

#pragma mark - Dismiss Methods
-(void)dismissWithCompletion:(void (^)(NSString *))completion{
    [self setPickerHidden:YES callBack:completion];
}

-(void)dismiss{
    [self dismissWithCompletion:self.onDismissCompletion];
}

#pragma mark - Show/hide PickerView methods
-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(id))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_backgroundView setAlpha:0.0];
                             [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
                         } else {
                             [_backgroundView setAlpha:1.0];
                             [_containerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [self removeFromSuperview];
                             callBack([self selectedObject]);
                         }
                     }];
}

#pragma mark - 点击透明背景hidden
- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_backgroundView setAlpha:1.0];
                         [_containerView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

#pragma mark - UIPickerViewDataSource
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 36.0;
}

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component {
    return [_dataArray count];
}

- (NSString *)pickerView: (UIPickerView *)pickerView
             titleForRow: (NSInteger)row
            forComponent: (NSInteger)component {
    SubmitOrder_SeletedTypeModel *model=_dataArray[row];
    if (self.objectToStringConverter == nil){
        return model.typeName;
    } else{
        return (self.objectToStringConverter (model.typeName));
    }
}

- (id)selectedObject {
    return [_dataArray objectAtIndex: [self.customPickerView selectedRowInComponent:0]];
}

#pragma mark - UIPickerViewDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    /*
    //如果需要选择的时候就响应就要该代码
    if (self.objectToStringConverter == nil) {
        self.onDismissCompletion ([_dataArray objectAtIndex:row]);
    } else{
        self.onDismissCompletion (self.objectToStringConverter ([self selectedObject]));
    }*/
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UIView *customPickerView = view;
    
    UILabel *pickerViewLabel;
    
    if (customPickerView==nil) {
        
        CGRect frame = CGRectMake(0.0, 0.0, 292.0, 44.0);
        customPickerView = [[UIView alloc] initWithFrame: frame];
        
        CGRect labelFrame = CGRectMake(0.0, 3.0, 292.0, 35); // 35 or 44
        pickerViewLabel = [[UILabel alloc] initWithFrame:labelFrame];
        [pickerViewLabel setTag:1];
        [pickerViewLabel setBackgroundColor:[UIColor clearColor]];
        pickerViewLabel.textAlignment=NSTextAlignmentCenter;
        [pickerViewLabel setTextColor:[UIColor blackColor]];
        [pickerViewLabel setFont:[UIFont systemFontOfSize:16]];
        [customPickerView addSubview:pickerViewLabel];
    } else{
        
        for (UIView *view in customPickerView.subviews) {
            if (view.tag == 1) {
                pickerViewLabel = (UILabel *)view;
                break;
            }
        }
    }
    
    SubmitOrder_SeletedTypeModel *model=[_dataArray objectAtIndex:row];
    if (self.objectToStringConverter == nil){
        [pickerViewLabel setText:model.typeName];
    } else{
        [pickerViewLabel setText:(self.objectToStringConverter (model.typeName))];
    }
    return customPickerView;
}

@end
