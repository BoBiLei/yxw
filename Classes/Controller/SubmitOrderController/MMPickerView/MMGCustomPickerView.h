//
//  MMGCustomPickerView
//  MMG
//
//  Created by mac on 15/10/22.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubmitOrder_SeletedTypeModel.h"

@interface MMGCustomPickerView : UIView

-(id)initWithPickerViewInView:(UIView *)view
                    withArray:(NSArray *)array
               selectedTypeId:(NSString *)typeId;

-(void)showPickerViewCompletion:(void (^)(id))completion;

@end
