//
//  RegistController.m
//  CatShopping
//
//  Created by mac on 15/9/15.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "RegistController.h"
#import "Macro2.h"
#import "HttpRequests.h"
@interface RegistController ()

@end

#define VIEWCORNERRADIUS 4;

@implementation RegistController{
    int timerCount;
    NSString *receiverify;
    UITextField *globleTextFiled;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    timerCount = RESETTIME;
    [self setCustomNavigationTitle:@"用户注册"];
    [self setUILayerCorneradius];
    
}

#pragma mark - textfield代理
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    globleTextFiled = textField;
}

-(void)setUILayerCorneradius{
    _phoneLabel.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    _verifyText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    _pwdText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    _confirmPwdText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    _recomentText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    //设置cornerRadius
    self.acountView.layer.cornerRadius=VIEWCORNERRADIUS;
    self.verifyView.layer.cornerRadius=VIEWCORNERRADIUS;
    self.verifyBtn.layer.cornerRadius=VIEWCORNERRADIUS;
    self.registBtn.layer.cornerRadius=VIEW_LAYER_CORNERRADIUS;
    self.passwordView.layer.cornerRadius=VIEWCORNERRADIUS;
    self.confirmPassView.layer.cornerRadius=VIEWCORNERRADIUS;
    self.referralView.layer.cornerRadius=VIEWCORNERRADIUS;
    
    //设置propert
    [_phoneLabel addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
    _phoneLabel.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    _registBtn.backgroundColor=[UIColor lightGrayColor];
    _registBtn.enabled=NO;
    
    _verifyBtn.backgroundColor=[UIColor lightGrayColor];
    _verifyBtn.enabled=NO;
}

- (void)phoneDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if ([AppUtils checkPhoneNumber:field.text]) {
        _verifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        _verifyBtn.enabled=YES;
        _registBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        _registBtn.enabled=YES;
    }else{
        _verifyBtn.backgroundColor=[UIColor lightGrayColor];
        _verifyBtn.enabled=NO;
        _registBtn.backgroundColor=[UIColor lightGrayColor];
        _registBtn.enabled=NO;
    }
}

#pragma mark - Request 请求

//判断是否注册
-(void)requestHttpsIsRegist:(NSString *)phone{
    NSDictionary *dict=@{
                         @"act":@"check_mobile_phone",
                         @"is_phone":@"1",
                         @"mobile_phone":phone
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            _verifyBtn.backgroundColor=[UIColor lightGrayColor];
            _verifyBtn.enabled=NO;
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
            [self requestVerifyHttpsForPhone:_phoneLabel.text];
        }else{
            [AppUtils showSuccessMessage:@"此号码已被注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//获取验证码
-(void)requestVerifyHttpsForPhone:(NSString *)phone{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"send_sms_code2" forKey:@"act"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestVerifyHttpsForPhone:_phoneLabel.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                receiverify=responseObject[@"retData"][@"code"];
                NSLog(@"%@",responseObject[@"retData"][@"code"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//提交注册
-(void)registRequestHttpsForPhone:(NSString *)phone verify:(NSString *)verify password:(NSString *)password confirmPwd:(NSString *)confirmPwd recomment:(NSString *)recomment{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"act_register",
                         @"back_act":@"1",
                         @"is_nosign":@"1",
                         @"extend_field5":phone,
                         @"smscode":verify,
                         @"password":password,
                         @"confirm_password":confirmPwd,
                         @"recomment":recomment,
                         @"agreement":@"1"// 1勾选 0未勾选
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            //==============
            //保存注册账号密码
            //==============
            if (self.LoginUserNameBlock) {
                self.LoginUserNameBlock(phone);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error userInfo]);
    }];
}

#pragma mark - button event
//发送验证码按钮
- (IBAction)clickVerifiBtn:(id)sender {
    [AppUtils closeKeyboard];
    if(![AppUtils checkPhoneNumber:_phoneLabel.text]){
        [AppUtils showSuccessMessage:@"请输入正确手机号" inView:self.view];
    }else{
        [self requestHttpsIsRegist:_phoneLabel.text];
    }
}

-(void)setTimer:(NSTimer *)timer{
    [_verifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [_verifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _verifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        _verifyBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

#pragma mark - 点击注册
- (IBAction)clickRegistBtn:(id)sender {
    
    if ([_verifyText.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"验证码不能为空" inView:self.view];
    }else if ([_pwdText.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"密码不能为空" inView:self.view];
    }else{
        if ([_confirmPwdText.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"确认密码不能为空" inView:self.view];
        }else if (![_confirmPwdText.text isEqualToString:_pwdText.text]){
            [AppUtils showSuccessMessage:@"确认密码不一致" inView:self.view];
        }else{
            [self registRequestHttpsForPhone:_phoneLabel.text verify:_verifyText.text password:_pwdText.text confirmPwd:_confirmPwdText.text recomment:_recomentText.text];
        }
    }
}

@end
