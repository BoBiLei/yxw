//
//  FillterIDModel.h
//  MMG
//
//  Created by mac on 15/11/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FillterIDModel : NSObject

@property (nonatomic,copy) NSString *categoryId;
@property (nonatomic,copy) NSString *categoryName;
@property (nonatomic,copy) NSString *subCategoryId;
@property (nonatomic,copy) NSString *brandId;
@property (nonatomic,copy) NSString *min_priceId;
@property (nonatomic,copy) NSString *max_priceId;

@end
