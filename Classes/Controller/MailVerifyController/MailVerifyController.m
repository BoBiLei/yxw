//
//  MailVerifyController.m
//  CatShopping
//
//  Created by mac on 15/9/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MailVerifyController.h"

@interface MailVerifyController ()

@end

@implementation MailVerifyController{
    UITextField *verifyText;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"邮箱验证"];
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    if (_isVerify) {
        [self hadVerifyView];
    }else{
        [self notVerifyView];
    }
}

-(void)notVerifyView{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 214)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    //邮箱View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    verifyText=[[UITextField alloc]init];
    verifyText.clearButtonMode=UITextFieldViewModeWhileEditing;
    verifyText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [verifyText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    verifyText.placeholder=@"请输入您的邮箱";
    verifyText.font=[UIFont systemFontOfSize:15];
    verifyText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:verifyText];
    verifyText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* verifyText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[verifyText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText)];
    [NSLayoutConstraint activateConstraints:verifyText_h];
    
    NSArray* verifyText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[verifyText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText)];
    [NSLayoutConstraint activateConstraints:verifyText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"提交" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[button(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickSubmigBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    UILabel *tip01=[[UILabel alloc]init];
    tip01.text=@"验证邮箱后，您可以免费享受如下服务：";
    tip01.font=[UIFont systemFontOfSize:14];
    [topView addSubview:tip01];
    tip01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tip01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[tip01]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip01)];
    [NSLayoutConstraint activateConstraints:tip01_h];
    
    NSArray* tip01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button]-10-[tip01(18)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button,tip01)];
    [NSLayoutConstraint activateConstraints:tip01_w];
    
    //
    UILabel *tip02=[[UILabel alloc]init];
    tip02.numberOfLines=0;
    tip02.text=@"1、通过邮箱轻松找回密码。";
    tip02.textColor=[UIColor colorWithHexString:@"#282828"];
    tip02.font=[UIFont systemFontOfSize:13];
    [topView addSubview:tip02];
    tip02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tip02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[tip02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip02)];
    [NSLayoutConstraint activateConstraints:tip02_h];
    
    NSArray* tip02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tip01]-2-[tip02(32)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip01,tip02)];
    [NSLayoutConstraint activateConstraints:tip02_w];
    
    //
    UILabel *tip03=[[UILabel alloc]init];
    tip03.numberOfLines=0;
    tip03.text=@"2、可接收订单、退款、账户余额、在线寄售等变动提醒，保障您的账户及资金安全。";
    tip03.textColor=[UIColor colorWithHexString:@"#282828"];
    tip03.font=[UIFont systemFontOfSize:13];
    [topView addSubview:tip03];
    tip03.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tip03_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[tip03]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip03)];
    [NSLayoutConstraint activateConstraints:tip03_h];
    
    NSArray* tip03_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tip02]-2-[tip03(36)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip02,tip03)];
    [NSLayoutConstraint activateConstraints:tip03_w];
}

-(void)hadVerifyView{
    
}

#pragma mark - text值改变时
-(void)textDidChange{
    if ([AppUtils checkEmail:verifyText.text]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - button Event
-(void)clickSubmigBtn{
    SubmitMailVerifyController *subMail=[[SubmitMailVerifyController alloc]init];
    [self.navigationController pushViewController:subMail animated:YES];
}
@end


#pragma mark
#pragma mark
@interface SubmitMailVerifyController ()

@end

@implementation SubmitMailVerifyController{
    UITextField *verifyText;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"邮箱验证"];
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 184)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    //
    UILabel *topTip01=[[UILabel alloc]init];
    topTip01.text=@"已发送验证码至邮箱：";
    topTip01.font=[UIFont systemFontOfSize:14];
    [topView addSubview:topTip01];
    topTip01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* topTip01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[topTip01(140)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip01)];
    [NSLayoutConstraint activateConstraints:topTip01_h];
    
    NSArray* topTip01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[topTip01(21)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip01)];
    [NSLayoutConstraint activateConstraints:topTip01_w];
    
    //
    UILabel *mailLabel=[[UILabel alloc]init];
    mailLabel.text=@"1050765093@qq.com";
    mailLabel.font=[UIFont systemFontOfSize:14];
    [topView addSubview:mailLabel];
    mailLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* mailLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[topTip01]-2-[mailLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip01,mailLabel)];
    [NSLayoutConstraint activateConstraints:mailLabel_h];
    
    NSArray* mailLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[mailLabel(18)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(mailLabel)];
    [NSLayoutConstraint activateConstraints:mailLabel_w];
    
    //
    UILabel *topTip02=[[UILabel alloc]init];
    topTip02.numberOfLines=0;
    topTip02.text=@"验证邮件3天内有效，请登录您的邮箱，查询验证码。";
    topTip02.textColor=[UIColor lightGrayColor];
    topTip02.font=[UIFont systemFontOfSize:13];
    [topView addSubview:topTip02];
    topTip02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* topTip02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[topTip02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip02)];
    [NSLayoutConstraint activateConstraints:topTip02_h];
    
    NSArray* topTip02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topTip01]-6-[topTip02(18)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip01,topTip02)];
    [NSLayoutConstraint activateConstraints:topTip02_w];
    
    //邮箱View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topTip02]-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topTip02,cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    verifyText=[[UITextField alloc]init];
    verifyText.clearButtonMode=UITextFieldViewModeWhileEditing;
    verifyText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [verifyText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    verifyText.placeholder=@"请输入您的邮箱验证码";
    verifyText.font=[UIFont systemFontOfSize:15];
    verifyText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:verifyText];
    verifyText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* verifyText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[verifyText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText)];
    [NSLayoutConstraint activateConstraints:verifyText_h];
    
    NSArray* verifyText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[verifyText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText)];
    [NSLayoutConstraint activateConstraints:verifyText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"提交验证" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(40)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickSubmigBtn) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - text值改变时
-(void)textDidChange{
    if (![verifyText.text isEqualToString:@""]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - button Event
-(void)clickSubmigBtn{
    
}
@end
