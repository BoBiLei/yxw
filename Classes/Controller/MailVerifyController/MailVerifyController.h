//
//  MailVerifyController.h
//  CatShopping
//
//  Created by mac on 15/9/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface MailVerifyController : BaseController

@property(nonatomic,assign) BOOL isVerify;

@end


//提交邮箱验证页面
@interface SubmitMailVerifyController : BaseController

@end
