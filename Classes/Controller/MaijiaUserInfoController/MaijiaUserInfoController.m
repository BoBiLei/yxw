//
//  MaijiaUserInfoController.m
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaUserInfoController.h"
#import "MMGInfoPhotoCell.h"
#import "InfoLabelCell.h"
#import "EditMaijiaInfoController.h"
#import "MMGCityModel.h"
#import "CityFmdb.h"
#import "HttpRequests.h"
#import "MMGSign.h"
#import "Macro2.h"

@interface MaijiaUserInfoController ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

//view
@property (strong, nonatomic) UIPickerView *myPicker;
@property (strong, nonatomic) UIView *maskView;
@property (strong, nonatomic) UIView *pickerBgView;

@end

@implementation MaijiaUserInfoController{
    NSArray *dataArr;
    NSMutableArray *rightDataArr;
    UITableView *myTable;
    
    BOOL isUpErweima;      //上传的是否是二维码
    NSString *photoStr;
    NSString *businessName;
    NSString *businessPhone;
    NSString *businessAddressDetail;
    NSString *weiboStr;
    NSString *qqStr;
    NSString *weixinStr;
    NSString *erWeimaStr;
    
    
    //选择地区
    NSArray *provinceArray;         //省
    NSArray *cityArray;             //市
    NSArray *districtArray;         //区
    NSArray *selectedArray;
    
    NSString *provinceId;
    NSString *cityId;
    NSString *districtId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateCellLabel:) name:@"EditMaijiaInfoText" object:nil];
    
    [self setUpNavigation];
    
    [self getPickerData];
    
    [self setUpUI];
    
    [self getMaijiaInfoRequest];
}

-(void)upDateCellLabel:(NSNotification *)noti{
    NSArray *arr=noti.object;
    NSString *str=arr[0];
    NSIndexPath *indexPath=arr[1];
    if (indexPath.section==0) {
        if (indexPath.row==1) {
            businessName=str;
        }else if (indexPath.row==2){
            businessPhone=str;
        }else if (indexPath.row==4){
            businessAddressDetail=str;
        }
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        [dic setValue:photoStr forKey:@"pic"];
        [dic setValue:businessName forKey:@"business_name"];
        [dic setObject:businessPhone forKey:@"business_phone"];
        [dic setObject:provinceId forKey:@"province"];
        [dic setObject:cityId forKey:@"city"];
        [dic setObject:districtId forKey:@"area"];
        [dic setObject:businessAddressDetail forKey:@"business_address"];
        [rightDataArr replaceObjectAtIndex:indexPath.section withObject:dic];
    }else if (indexPath.section==1){
        weiboStr=str;
        [rightDataArr replaceObjectAtIndex:indexPath.section withObject:@{@"business_sina":weiboStr}];
    }else if (indexPath.section==2){
        qqStr=str;
        [rightDataArr replaceObjectAtIndex:indexPath.section withObject:@{@"business_qq":qqStr}];
    }else if (indexPath.section==3){
        if (indexPath.row==0) {
            weixinStr=str;
            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
            [dic setValue:weixinStr forKey:@"business_wechat_id"];
            [dic setObject:erWeimaStr forKey:@"business_wechat"];
            [rightDataArr replaceObjectAtIndex:indexPath.section withObject:dic];
        }
    }
//    NSLog(@"%@--%@\n%ld--%ld",str,indexPath,indexPath.section,indexPath.row);
    [myTable reloadData];
}

#pragma mark - setUpNavigation

-(void)setUpNavigation{
    [self setCustomNavigationTitle:@"详细资料"];
    UIButton *navRightBtn=[self setCustomRightBarButtonItemSetFrame:CGRectMake(0, 0, 40, 40) Text:@"保存"];
    [navRightBtn addTarget:self action:@selector(clickSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickSaveBtn:(id)sender{
    [self updateInfoRequest];
}

#pragma mark - get Addressdata

- (void)getPickerData {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    
    //得到一个省份的全部省份
    provinceArray=[db getProvince];
    
    //设置默认省份的市
    MMGCityModel *model001=provinceArray[0];
    cityArray=[db getCityWithId:model001.parent_id];
    
    //设置默认市的区
    MMGCityModel *model002=cityArray[0];
    districtArray=[db getCityWithId:model002.parent_id];
}

#pragma mark - init UI

-(void)setUpUI{
    
    //
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"MaijiaInfoPlist" ofType:@"plist"];
    dataArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"MMGInfoPhotoCell" bundle:nil] forCellReuseIdentifier:@"MMGInfoPhotoCell"];
    [myTable registerNib:[UINib nibWithNibName:@"InfoLabelCell" bundle:nil] forCellReuseIdentifier:@"InfoLabelCell"];
    [self.view addSubview:myTable];
    
    //init pickview
    self.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = 0;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker)]];
    
    self.pickerBgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 236)];
    self.pickerBgView.backgroundColor=[UIColor whiteColor];
    
    //tool view
    UIView *pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, SCREENSIZE.width, 44.0)];
    [self.pickerBgView addSubview:pickerTopBarView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:pickerTopBarView.frame];
    [pickerTopBarView addSubview:toolBar];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-60, 4, 40, 40)];
    [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
    [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
    [closeBtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [closeBtn addTarget:self action:@selector(hideMyPicker) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    toolBar.items = @[flexibleSpace, barButtonItem];
    
    //pickerview
    _myPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, pickerTopBarView.height, SCREENSIZE.width, _pickerBgView.height-pickerTopBarView.height)];
    [_myPicker setDelegate:self];
    [_myPicker setDataSource:self];
    _myPicker.userInteractionEnabled=YES;
    [self.pickerBgView addSubview:_myPicker];
    [self.pickerBgView bringSubviewToFront:_myPicker]; 
}

#pragma mark - private method
- (void)showMyPicker{
    CityFmdb *db=[CityFmdb shareCityFMDB];
    //Set selected row
    //Province
    int seletedProvinceRow=0;
    selectedArray=provinceArray;
    for (int i=0; i<provinceArray.count; i++) {
        MMGCityModel *model=provinceArray[i];
        NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
        if ([provinceId isEqualToString:seleStr]) {
            seletedProvinceRow=i;
            MMGCityModel *model=provinceArray[i];
            //得到一个当前省份的所有城市
            cityArray=[db getCityWithId:model.parent_id];
        }
    }
    [_myPicker reloadComponent:1];
    [_myPicker selectRow:seletedProvinceRow inComponent:0 animated:YES];
    
    //city
    int seletedCityRow=0;
    for (int i=0; i<cityArray.count; i++) {
        MMGCityModel *model=cityArray[i];
        NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
        if ([cityId isEqualToString:seleStr]) {
            seletedCityRow=i;
            MMGCityModel *model=cityArray[i];
            //得到一个当前城市的所有区域
            districtArray=[db getCityWithId:model.parent_id];
        }
    }
    [_myPicker reloadComponent:2];
    [_myPicker selectRow:seletedCityRow inComponent:1 animated:YES];
    
    //area
    int seletedAreaRow=0;
    for (int i=0; i<districtArray.count; i++) {
        MMGCityModel *model=districtArray[i];
        NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
        if ([districtId isEqualToString:seleStr]) {
            seletedAreaRow=i;
        }
    }
    [_myPicker selectRow:seletedAreaRow inComponent:2 animated:YES];
    
    //
    [AppUtils closeKeyboard];
    [self.navigationController.view addSubview:self.maskView];
    [self.navigationController.view addSubview:self.pickerBgView];
    self.maskView.alpha = 0;
    self.pickerBgView.top = self.navigationController.view.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0.3;
        self.pickerBgView.bottom = self.navigationController.view.height;
    }];
}

- (void)hideMyPicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0;
        self.pickerBgView.top = self.navigationController.view.height;
    } completion:^(BOOL finished) {
        
        MMGCityModel *model01=[provinceArray objectAtIndex:[self.myPicker selectedRowInComponent:0]];
        MMGCityModel *model02=[cityArray objectAtIndex:[self.myPicker selectedRowInComponent:1]];
        MMGCityModel *model03=[districtArray objectAtIndex:[self.myPicker selectedRowInComponent:2]];
        
        provinceId=model01.parent_id;
        cityId=model02.parent_id;
        districtId=model03.parent_id;
        
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        [dic setValue:photoStr forKey:@"pic"];
        [dic setValue:businessName forKey:@"business_name"];
        [dic setObject:businessPhone forKey:@"business_phone"];
        [dic setObject:provinceId forKey:@"province"];
        [dic setObject:cityId forKey:@"city"];
        [dic setObject:districtId forKey:@"area"];
        [dic setObject:businessAddressDetail forKey:@"business_address"];
        [rightDataArr replaceObjectAtIndex:0 withObject:dic];
        
        [self.maskView removeFromSuperview];
        [self.pickerBgView removeFromSuperview];
        
        [myTable reloadData];
    }];
}

#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 36.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return provinceArray.count;
    } else if (component == 1) {
        return cityArray.count;
    } else {
        return districtArray.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        MMGCityModel *model=[provinceArray objectAtIndex:row];
        return model.cityName;
    } else if (component == 1) {
        if(cityArray.count==1){
            return nil;
        }else{
            MMGCityModel *model=[cityArray objectAtIndex:row];
            return model.cityName;
        }
    } else {
        MMGCityModel *model=[districtArray objectAtIndex:row];
        return model.cityName;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 110;
    } else if (component == 1) {
        return 100;
    } else {
        return 110;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    if (component == 0) {
        selectedArray =provinceArray;
        if (selectedArray.count > 0) {
            MMGCityModel *model=selectedArray[row];
            //得到一个当前省份的所有城市
            cityArray=[db getCityWithId:model.parent_id];
        } else {
            cityArray = nil;
        }
        
        if (cityArray.count > 0) {
            //得到一个选择市的全部区域
            MMGCityModel *model=cityArray[0];
            districtArray=[db getCityWithId:model.parent_id];
        } else {
            districtArray = nil;
        }
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    
    if (component == 1) {
        if (selectedArray.count > 0 && cityArray.count > 0) {
            MMGCityModel *model=cityArray[row];
            //得到一个市的全部区域
            districtArray=[db getCityWithId:model.parent_id];
        } else {
            districtArray = nil;
        }
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    [pickerView reloadComponent:2];
}

//更改字体大小（重写方法）
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:17]];
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

#pragma mark - table delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)dataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 78;
        }else{
            return 46;
        }
    }else if(indexPath.section==dataArr.count-1){
        if (indexPath.row!=0) {
            return 78;
        }else{
            return 46;
        }
    }else{
        return 46;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            MMGInfoPhotoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MMGInfoPhotoCell"];
            if (cell==nil) {
                cell=[[MMGInfoPhotoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGInfoPhotoCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            [cell.imgv setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:@""]];
            return  cell;
        }else{
            InfoLabelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoLabelCell"];
            if (cell==nil) {
                cell=[[InfoLabelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoLabelCell"];
            }
            cell.separatorInset=UIEdgeInsetsMake(0, indexPath.row==3?92:0, 0, 0);
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            NSDictionary *dic=rightDataArr[indexPath.section];
            NSString *str=@"";
            if (indexPath.row==1) {
                str=dic[@"business_name"];
            }else if (indexPath.row==2){
                str=dic[@"business_phone"];
            }else if (indexPath.row==3){
                CityFmdb *db=[CityFmdb shareCityFMDB];
                for (int i=0; i<provinceArray.count; i++) {
                    MMGCityModel *model=provinceArray[i];
                    NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
                    if ([provinceId isEqualToString:seleStr]) {
                        str=[str stringByAppendingString:model.cityName];
                        MMGCityModel *model=provinceArray[i];
                        //得到一个当前省份的所有城市
                        cityArray=[db getCityWithId:model.parent_id];
                    }
                }
                for (int i=0; i<cityArray.count; i++) {
                    MMGCityModel *model=cityArray[i];
                    NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
                    if ([cityId isEqualToString:seleStr]) {
                        str=[str stringByAppendingString:model.cityName];
                        MMGCityModel *model=cityArray[i];
                        //得到一个当前城市的所有区域
                        districtArray=[db getCityWithId:model.parent_id];
                    }
                }
                for (int i=0; i<districtArray.count; i++) {
                    MMGCityModel *model=districtArray[i];
                    NSString *seleStr=[NSString stringWithFormat:@"%@",model.parent_id];
                    if ([districtId isEqualToString:seleStr]) {
                        str=[str stringByAppendingString:model.cityName];
                    }
                }
            }else if (indexPath.row==4){
                str=dic[@"business_address"];
            }
            cell.rightLabel.text=str;
            return  cell;
        }
    }else if(indexPath.section==3){
        if (indexPath.row==0) {
            InfoLabelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoLabelCell"];
            if (cell==nil) {
                cell=[[InfoLabelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoLabelCell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            NSDictionary *dic=rightDataArr[indexPath.section];
            cell.rightLabel.text=dic[@"business_wechat_id"];
            return  cell;
        }else{
            MMGInfoPhotoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MMGInfoPhotoCell"];
            if (cell==nil) {
                cell=[[MMGInfoPhotoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGInfoPhotoCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            cell.imgv.layer.cornerRadius=0;
            NSDictionary *dic=rightDataArr[indexPath.section];
            NSString *str=dic[@"business_wechat"];
            if ([str isEqualToString:@""]) {
                cell.imgv.image=[UIImage imageNamed:@"shangjia_sc_imgv"];
            }else{
                [cell.imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost01,str]]];
            }
            
            return  cell;
        }
    }else{
        InfoLabelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoLabelCell"];
        if (cell==nil) {
            cell=[[InfoLabelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoLabelCell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
        NSDictionary *dic=rightDataArr[indexPath.section];
        cell.rightLabel.text=indexPath.section==1?dic[@"business_sina"]:dic[@"business_qq"];
        return  cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if(indexPath.row==0){
            isUpErweima=NO;
            [self clickphotoBtn];
        }
        if (indexPath.row==1||indexPath.row==2||indexPath.row==4) {
            EditMaijiaInfoController *ctr=[EditMaijiaInfoController new];
            ctr.hidesBottomBarWhenPushed=YES;
            NSDictionary *dic=rightDataArr[indexPath.section];
            NSString *str;
            if (indexPath.row==1) {
                str=dic[@"business_name"];
            }else if (indexPath.row==2){
                str=dic[@"business_phone"];
            }else if (indexPath.row==4){
                str=dic[@"business_address"];
            }
            ctr.titleStr=str;
            ctr.indexPath=indexPath;
            [self.navigationController pushViewController:ctr animated:YES];
        }else if(indexPath.row==3){
            [self showMyPicker];
        }
    }else if(indexPath.section==1){
        EditMaijiaInfoController *ctr=[EditMaijiaInfoController new];
        ctr.hidesBottomBarWhenPushed=YES;
        NSDictionary *dic=rightDataArr[indexPath.section];
        ctr.titleStr=dic[@"business_sina"];
        ctr.indexPath=indexPath;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if(indexPath.section==2){
        EditMaijiaInfoController *ctr=[EditMaijiaInfoController new];
        ctr.hidesBottomBarWhenPushed=YES;
        NSDictionary *dic=rightDataArr[indexPath.section];
        ctr.titleStr=dic[@"business_qq"];
        ctr.indexPath=indexPath;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if(indexPath.section==3){
        if (indexPath.row==0) {
            EditMaijiaInfoController *ctr=[EditMaijiaInfoController new];
            ctr.hidesBottomBarWhenPushed=YES;
            NSDictionary *dic=rightDataArr[indexPath.section];
            ctr.titleStr=dic[@"business_wechat_id"];
            ctr.indexPath=indexPath;
            [self.navigationController pushViewController:ctr animated:YES];
        }else{
            isUpErweima=YES;
            [self clickphotoBtn];
        }
    }
}

#pragma mark - 修改头像
-(void)clickphotoBtn{
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            case 1:
                [self pickImage];
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:completeBlock];
}

#pragma mark - show ModifyPhoto View
-(void)showMdfView{
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            case 1:
                [self pickImage];
                break;
                
            default:
                break;
        }
    };
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=12;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    sheetConfig.itemNormalColor=[UIColor colorWithHexString:@"#0080c5"];
    
    NSArray *items =
    @[MMItemMake(@"拍 照", MMItemTypeNormal, block),
      MMItemMake(@"从相册上传", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"更新头像"
                                  items:items] showWithBlock:^(MMPopupView *popupView){
        
    }];
}

//拍照
- (void) snapImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image= [info objectForKey:@"UIImagePickerControllerEditedImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(200.0, 200.0)];
    
    NSData* imageData = UIImagePNGRepresentation(theImage);
    
    if(!isUpErweima){
        [self upPhotoImgsRequest:imageData];
    }else{
        [self upErweimaRequest:imageData];
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

//保存图片到document
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imageData writeToFile:fullPathToFile atomically:NO];
}

#pragma mark - navigation delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    UIColor * color = [UIColor blackColor];
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    navigationController.navigationBar.titleTextAttributes=dict;
    navigationController.navigationBar.tintColor=[UIColor blackColor];
}


#pragma mark Request 请求

-(void)getMaijiaInfoRequest{
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
	   NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"get_info",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"is_nosign":@"1"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"busines.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            provinceId=responseObject[@"retData"][@"arr_1"][@"province"];
            cityId=responseObject[@"retData"][@"arr_1"][@"city"];
            districtId=responseObject[@"retData"][@"arr_1"][@"area"];
            rightDataArr=[NSMutableArray arrayWithObjects:
                          responseObject[@"retData"][@"arr_1"],
                          responseObject[@"retData"][@"arr_2"],
                          responseObject[@"retData"][@"arr_3"],
                          responseObject[@"retData"][@"arr_4"], nil];
            photoStr=responseObject[@"retData"][@"arr_1"][@"pic"];
            businessName=responseObject[@"retData"][@"arr_1"][@"business_name"];
            businessPhone=responseObject[@"retData"][@"arr_1"][@"business_phone"];
            businessAddressDetail=responseObject[@"retData"][@"arr_1"][@"business_address"];
            weiboStr=responseObject[@"retData"][@"arr_2"][@"business_sina"];
            qqStr=responseObject[@"retData"][@"arr_3"][@"business_qq"];
            weixinStr=responseObject[@"retData"][@"arr_4"][@"business_wechat_id"];
            erWeimaStr=responseObject[@"retData"][@"arr_4"][@"business_wechat"];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)upPhotoImgsRequest:(NSData *)imgDate{
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"iso_upload_img",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"imgs":imgDate,
                         @"img_type":@"png",
                         @"is_nosign":@"1"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *newPhtotStr=responseObject[@"retData"][@"path"];
            [AppUtils saveValue:[NSString stringWithFormat:@"%@%@",ImageHost01,newPhtotStr] forKey:User_Photo];
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationUpdatePhoto object:nil];
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [myTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//二维码
-(void)upErweimaRequest:(NSData *)imgDate{
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
	   NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"insert_wechat",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"imgs":imgDate,
                         @"img_type":@"png",
                         @"is_nosign":@"1"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"busines.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"二维码保存成功" inView:self.view];
            erWeimaStr=responseObject[@"retData"][@"path"];
            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
            [dic setValue:weixinStr forKey:@"business_wechat_id"];
            [dic setObject:erWeimaStr forKey:@"business_wechat"];
            [rightDataArr replaceObjectAtIndex:3 withObject:dic];
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:1 inSection:3];
            [myTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//保存
-(void)updateInfoRequest{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
	   NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"insert_info" forKey:@"act"];
    [dict setObject:businessName forKey:@"business_name"];
    [dict setObject:businessPhone forKey:@"business_phone"];
    [dict setObject:businessAddressDetail forKey:@"business_address"];
    [dict setObject:qqStr forKey:@"business_qq"];
    [dict setObject:weiboStr forKey:@"business_sina"];
    [dict setObject:weixinStr forKey:@"business_wechat_id"];
    [dict setObject:provinceId forKey:@"province"];
    [dict setObject:cityId forKey:@"city"];
    [dict setObject:districtId forKey:@"area"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"busines.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]){
            [AppUtils showSuccessMessage:@"保存成功" inView:self.view];
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
