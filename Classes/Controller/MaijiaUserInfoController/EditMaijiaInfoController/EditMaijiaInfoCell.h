//
//  EditMaijiaInfoCell.h
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditMaijiaInfoDelegate <NSObject>

-(void)setNameTextfield:(NSString *)nameStr indexPath:(NSIndexPath *)indexPath;

@end

@interface EditMaijiaInfoCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic,copy) NSString *nameStr;

@property (nonatomic, weak) id<EditMaijiaInfoDelegate> delegate;

@end
