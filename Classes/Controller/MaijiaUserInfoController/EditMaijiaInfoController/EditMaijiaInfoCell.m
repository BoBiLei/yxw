//
//  EditMaijiaInfoCell.m
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "EditMaijiaInfoCell.h"

@implementation EditMaijiaInfoCell{
    UITextField *texTF;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        texTF=[[UITextField alloc] initWithFrame:CGRectMake(12, 0, SCREENSIZE.width-20, 44)];
        [texTF becomeFirstResponder];
        texTF.font=[UIFont systemFontOfSize:16];
        texTF.clearButtonMode=UITextFieldViewModeWhileEditing;
        texTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
        texTF.delegate=self;
        [texTF addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:texTF];
    }
    return self;
}

-(void)selectSell:(NSString *)nameStr{
    //获取当前的indexPath
    UITableView *tableView = (UITableView *)self.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.delegate setNameTextfield:nameStr indexPath:indexPath];
}

#pragma mark - textfield
- (void)phoneDidChange:(id) sender {
    UITextField *textFT=sender;
    [self selectSell:textFT.text];
}

#pragma mark - delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self selectSell:textField.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self selectSell:textField.text];
}

-(void)setNameStr:(NSString *)nameStr{
    texTF.placeholder=nameStr;
    texTF.text=nameStr;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
