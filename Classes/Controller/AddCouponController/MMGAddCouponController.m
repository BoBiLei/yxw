//
//  MMGAddCouponController.m
//  MMG
//
//  Created by mac on 15/10/15.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGAddCouponController.h"
#import "MMGSign.h"
#import "HttpRequests.h"
#import "Macro2.h"
@interface MMGAddCouponController ()<UITableViewDelegate>

@end

@implementation MMGAddCouponController{
    UITableView *myTable;
    
    UITextField *cartText;
    UITextField *passText;
    UIButton *button;
}

-(void)setNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    //    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    //UIColor *color1=[UIColor colorWithRed:0.0 green:127.0 blue:181.0 alpha:1];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    //UIColor *color2=[[UIColor alloc] initWithRed:2.0f green:116.0f blue:181.0f alpha:1];
    //[statusBarView setBackgroundColor:color1];
    
    [self.view addSubview:statusBarView];
    
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(138, 25, 120, 30)];
    btnlab.text=@"绑定新礼品卡";
    [btnlab setTextColor:[UIColor whiteColor]];
    
    btnlab.textAlignment=NSTextAlignmentCenter;

    [self.view addSubview:btnlab];
    
    
}
-(void)fanhui
{
    
    [self .navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setCustomNavigationTitle:@"绑定新礼品卡"];
    [self setNavBar];
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 64, SCREENSIZE.width-16, SCREENSIZE.height) style:UITableViewStyleGrouped];
    myTable.sectionHeaderHeight = 70;
    [myTable registerNib:[UINib nibWithNibName:@"CouponCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
    myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self setUpHeaderView];
}

-(void)setUpHeaderView{
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 234)];
    headView.backgroundColor=[UIColor whiteColor];
    myTable.tableHeaderView=headView;
    myTable.delegate=self;
    
    //标题
    UIView *titView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, headView.frame.size.width, 58)];
    titView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    [headView addSubview:titView];
    
    //
    UIView *bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, titView.frame.size.width, 14)];
    bgView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [titView addSubview:bgView];
    
    //
    UILabel *titLabel=[UILabel new];
    titLabel.text=@"绑定新礼品卡";
    titLabel.font=[UIFont systemFontOfSize:14];
    titLabel.textAlignment=NSTextAlignmentLeft;
    titLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [titView addSubview:titLabel];
    titLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* titLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[titLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titLabel,titLabel)];
    [NSLayoutConstraint activateConstraints:titLabel_h];
    
    NSArray* titLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[bgView][titLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bgView,titLabel)];
    [NSLayoutConstraint activateConstraints:titLabel_w];
    
    //top line
    UIView *line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [headView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titLabel][line(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titLabel,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //卡号View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [headView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line]-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    UILabel *cartLabel=[[UILabel alloc]init];
    cartLabel.textAlignment=NSTextAlignmentCenter;
    cartLabel.text=@"卡号：";
    cartLabel.font=[UIFont systemFontOfSize:14];
    cartLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartLabel];
    cartLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartLabel(45)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_h];
    
    NSArray* cartLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_w];
    
    //
    cartText=[[UITextField alloc]init];
    cartText.clearButtonMode=UITextFieldViewModeWhileEditing;
    cartText.secureTextEntry = NO;
    cartText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [cartText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    cartText.placeholder=@"礼品卡号";
    cartText.font=[UIFont systemFontOfSize:14];
    cartText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:cartText];
    cartText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cartLabel]-8-[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel,cartText)];
    [NSLayoutConstraint activateConstraints:cartText_h];
    
    NSArray* cartText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartText)];
    [NSLayoutConstraint activateConstraints:cartText_w];
    
    //密码View
    UIView *passView=[[UIView alloc]init];
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [headView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[passView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[[UILabel alloc]init];
    passLabel.textAlignment=NSTextAlignmentCenter;
    passLabel.text=@"密码：";
    passLabel.font=[UIFont systemFontOfSize:14];
    passLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLabel(45)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    //
    passText=[[UITextField alloc]init];
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.secureTextEntry = YES;
    passText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [passText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.placeholder=@"密码";
    passText.font=[UIFont systemFontOfSize:14];
    passText.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-8-[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"绑定" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [headView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[passView]-12-[button(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView,button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - text值改变时
-(void)textDidChange{
    if (![cartText.text isEqualToString:@""]&&
         ![passText.text isEqualToString:@""]
         ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - 点击绑定按钮
-(void)clickBtn{
    [AppUtils closeKeyboard];
    [self requestHttpsForCartNum:cartText.text pass:passText.text];
}

#pragma mark Request 请求
-(void)requestHttpsForCartNum:(NSString *)cartNum pass:(NSString *)pass{
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"bind_bonus" forKey:@"act"];
    [dict setObject:cartNum forKey:@"bonus_sn"];
    [dict setObject:pass forKey:@"bonus_passwd"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCartNum:cartText.text pass:passText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpDateCoupon" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:@"您输入的卡号或密码有误！" inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
