//
//  PhoneVerifyController.m
//  CatShopping
//
//  Created by mac on 15/9/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "PhoneVerifyController.h"
#import "Macro2.h"
#import "MMGSign.h"
#import "HttpRequests.h"
@interface PhoneVerifyController ()

@end

@implementation PhoneVerifyController{
    NSString *receiverify;
    
    int timerCount;
    UIButton *getVerifyBtn;
    UITextField *phoneTextFiedl;
    UITextField *verifyText;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
  
//    [self setCustomNavigationTitle:navTitle];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    timerCount = RESETTIME;
    [self setUpUI];
}
-(void)setNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    //    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    //UIColor *color1=[UIColor colorWithRed:0.0 green:127.0 blue:181.0 alpha:1];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    //UIColor *color2=[[UIColor alloc] initWithRed:2.0f green:116.0f blue:181.0f alpha:1];
    //[statusBarView setBackgroundColor:color1];
    
    [self.view addSubview:statusBarView];
    
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [fanHuiButton setBackgroundImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
   
    if ([self.receiverPhone isEqualToString:@""]) {
        btnlab.text=@"手机绑定";
    }else{
        btnlab.text=@"手机验证";
    }
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
    
}
-(void)fanhui
{
    
    [self .navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - init UI
-(void)setUpUI{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 72, SCREENSIZE.width-16, 214)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    CGFloat verify_Y=0.0f;
    
    if([self.receiverPhone isEqualToString:@""]){
        //卡号View
        UIView *phoneView=[[UIView alloc]initWithFrame:CGRectMake(8, 72, topView.width-16, 44)];
        phoneView.layer.borderWidth=0.5f;
        phoneView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
        [topView addSubview:phoneView];
        verify_Y=phoneView.frame.origin.y+phoneView.height+8;
        
        CGRect frame=topView.frame;
        frame.size.height=226;
        topView.frame=frame;
        
        //
        phoneTextFiedl=[[UITextField alloc]initWithFrame:CGRectMake(8, 0, phoneView.width-16, phoneView.height)];
        phoneTextFiedl.clearButtonMode=UITextFieldViewModeWhileEditing;
        phoneTextFiedl.tintColor=[UIColor colorWithHexString:@"ec6b00"];
        [phoneTextFiedl addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
        phoneTextFiedl.placeholder=@"请输入绑定手机号";
        phoneTextFiedl.font=[UIFont systemFontOfSize:15];
        phoneTextFiedl.textColor=[UIColor colorWithHexString:@"#282828"];
        [phoneView addSubview:phoneTextFiedl];
    }else{
        //已绑定手机
        UILabel *showPhone=[[UILabel alloc]initWithFrame:CGRectMake(8, 10, topView.width-16, 28)];
        NSString *str=@"已绑定手机：";
        showPhone.text=[NSString stringWithFormat:@"%@%@",str,_receiverPhone];
        showPhone.textColor=[UIColor lightGrayColor];
        showPhone.font=[UIFont systemFontOfSize:14];
        [topView addSubview:showPhone];
        verify_Y=showPhone.frame.origin.y+showPhone.height+8;
    }
    //卡号View
    UIView *cartView=[[UIView alloc]initWithFrame:CGRectMake(8, verify_Y, topView.width-16, 44)];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    
    //获取验证码按钮
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if([self.receiverPhone isEqualToString:@""]){
        getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
        getVerifyBtn.enabled=NO;
    }else{
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        getVerifyBtn.enabled=YES;
    }
    getVerifyBtn.layer.cornerRadius=2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    [cartView addSubview:getVerifyBtn];
    getVerifyBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* getVerifyBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[getVerifyBtn(75)]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_h];
    
    NSArray* getVerifyBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[getVerifyBtn]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_w];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    verifyText=[[UITextField alloc]init];
    verifyText.clearButtonMode=UITextFieldViewModeWhileEditing;
    verifyText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [verifyText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    verifyText.placeholder=@"请输入验证码";
    verifyText.font=[UIFont systemFontOfSize:15];
    verifyText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:verifyText];
    verifyText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* verifyText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[verifyText]-2-[getVerifyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText,getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:verifyText_h];
    
    NSArray* verifyText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[verifyText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(verifyText)];
    [NSLayoutConstraint activateConstraints:verifyText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"提交" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[button(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickSubmigBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    UILabel *tip01=[[UILabel alloc]init];
    tip01.text=@"注意事项：";
    tip01.font=[UIFont systemFontOfSize:14];
    [topView addSubview:tip01];
    tip01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tip01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[tip01]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip01)];
    [NSLayoutConstraint activateConstraints:tip01_h];
    
    NSArray* tip01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button]-10-[tip01(18)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button,tip01)];
    [NSLayoutConstraint activateConstraints:tip01_w];
    
    //
    UILabel *tip02=[[UILabel alloc]init];
    tip02.numberOfLines=0;
    tip02.text=@"若该手机号已丢失或无法接收验证短信，请拨打客服电话0755-22211222申诉更改验证手机。";
    tip02.textColor=[UIColor colorWithHexString:@"#282828"];
    tip02.font=[UIFont systemFontOfSize:13];
    [topView addSubview:tip02];
    tip02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tip02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[tip02]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip02)];
    [NSLayoutConstraint activateConstraints:tip02_h];
    
    NSArray* tip02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tip01]-2-[tip02(36)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tip01,tip02)];
    [NSLayoutConstraint activateConstraints:tip02_w];
}

#pragma mark - text值改变时

-(void)phoneTextDidChange{
    if ([AppUtils checkPhoneNumber:phoneTextFiedl.text]) {
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        getVerifyBtn.enabled=YES;
    }else{
        getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
        getVerifyBtn.enabled=NO;
    }
}

-(void)textDidChange{
    if (![verifyText.text isEqualToString:@""]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - 获取验证码按钮
-(void)clickVerifyBtn{
    
    [AppUtils closeKeyboard];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
    
    if ([self.receiverPhone isEqualToString:@""]) {
        [self requestVerifyHttpsForBandingPhone:phoneTextFiedl.text];
    }else{
        [self requestVerifyHttpsForPhone:[AppUtils getValueWithKey:User_Phone]];
    }
}

#pragma mark - request 获取验证码请求
//01
-(void)requestVerifyHttpsForPhone:(NSString *)phone{
    NSDictionary *dict=@{
                         @"act":@"send_sms_code_find_pw",
                         @"is_phone":@"1",
                         @"phone":phone
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            receiverify=responseObject[@"retData"][@"rcode"];
        }
        NSLog(@"%@",responseObject[@"retData"][@"rcode"]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//02
-(void)requestVerifyHttpsForBandingPhone:(NSString *)phone{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:@"send_sms_code3" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestVerifyHttpsForBandingPhone:phoneTextFiedl.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                receiverify=responseObject[@"retData"][@"code"];
            }
            NSLog(@"%@",responseObject[@"retData"][@"code"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)setTimer:(NSTimer *)timer{
    getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
    getVerifyBtn.enabled=NO;
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        getVerifyBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

#pragma mark - 点击提交按钮
-(void)clickSubmigBtn{
    [AppUtils closeKeyboard];
    NSString *verifyStr=[NSString stringWithFormat:@"%@",receiverify];
    if (![verifyStr isEqualToString:verifyText.text]) {
        [AppUtils showSuccessMessage:@"验证码有误" inView:self.view];
    }else{
        if([self.receiverPhone isEqualToString:@""]){
            [self checkPhoneIsRegist:phoneTextFiedl.text];
        }else{
            BoundNewPhoneController *boundPhone=[[BoundNewPhoneController alloc]init];
            boundPhone.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:boundPhone animated:YES];
        }
    }
}

#pragma mark - request查询手机是否已注册
-(BOOL)checkPhoneIsRegist:(NSString *)phone{
    __block BOOL res = NO;
    NSDictionary *dict=@{
                         @"act":@"check_mobile_phone",
                         @"is_phone":@"1",
                         @"mobile_phone":phone
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //=========================
        //error表示存在，succe表示没有
        //=========================
        NSString *str=responseObject[@"retMsg"];
        if ([str isEqualToString:@"success"]) {
            res=YES;
            [self requestVerifyAndPhoneForPhone:phoneTextFiedl.text verify:verifyText.text];
        }else{
            [AppUtils showSuccessMessage:@"该号码已被注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
    return res;
}

#pragma mark - request绑定手机号、验证码
-(void)requestVerifyAndPhoneForPhone:(NSString *)phone verify:(NSString *)verify{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
   
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"act_phone_edit_bind_act_2" forKey:@"act"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:verify forKey:@"smscode"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestVerifyAndPhoneForPhone:phoneTextFiedl.text verify:verifyText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //保存用户绑定手机号
                [AppUtils saveValue:phone forKey:User_Phone];
                
                //发送更新通知
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_BANDINGPHONE" object:nil];
                
                [AppUtils showSuccessMessage:@"绑定手机成功" inView:self.view];
                sleep(1);
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
                NSLog(@"%@",responseObject[@"retData"][@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end

#pragma mark
#pragma mark
#pragma mark - 绑定新号码Controller

@interface BoundNewPhoneController ()

@end

@implementation BoundNewPhoneController{
    int timerCount;
    NSString *receiverify;
    
    UITextField *cartText;
    UITextField *passText;
    UIButton *getVerifyBtn;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setCustomNavigationTitle:@"绑定新号码"];
    timerCount = RESETTIME;
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 72, SCREENSIZE.width-16, 180)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    //卡号View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    UILabel *cartLabel=[[UILabel alloc]init];
    cartLabel.textAlignment=NSTextAlignmentCenter;
    cartLabel.text=@"手机号：";
    cartLabel.font=[UIFont systemFontOfSize:15];
    cartLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartLabel];
    cartLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_h];
    
    NSArray* cartLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_w];
    
    //获取验证码按钮
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyBtn.enabled=YES;
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    getVerifyBtn.layer.cornerRadius=2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    [cartView addSubview:getVerifyBtn];
    getVerifyBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* getVerifyBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[getVerifyBtn(75)]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_h];
    
    NSArray* getVerifyBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[getVerifyBtn]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_w];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    cartText=[[UITextField alloc]init];
    cartText.clearButtonMode=UITextFieldViewModeWhileEditing;
    cartText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [cartText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    cartText.placeholder=@"请输入手机号码";
    cartText.font=[UIFont systemFontOfSize:15];
    cartText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:cartText];
    cartText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cartLabel]-4-[cartText]-2-[getVerifyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel,cartText,getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:cartText_h];
    
    NSArray* cartText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartText)];
    [NSLayoutConstraint activateConstraints:cartText_w];
    
    //密码View
    UIView *passView=[[UIView alloc]init];
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[passView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[[UILabel alloc]init];
    passLabel.textAlignment=NSTextAlignmentCenter;
    passLabel.text=@"验证码：";
    passLabel.font=[UIFont systemFontOfSize:15];
    passLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    //
    passText=[[UITextField alloc]init];
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [passText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.placeholder=@"请输入验证码";
    passText.font=[UIFont systemFontOfSize:15];
    passText.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-4-[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"绑定新号码" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(40)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - text值改变时
-(void)textDidChange{
    if ([AppUtils checkPhoneNumber:cartText.text]&&
        ![cartText.text isEqualToString:@""]&&
        ![passText.text isEqualToString:@""]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - 点击获取验证码按钮
-(void)clickVerifyBtn{
    [AppUtils closeKeyboard];
    if ([cartText.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入绑定新号码" inView:self.view];
    }else if (![AppUtils checkPhoneNumber:cartText.text]){
        [AppUtils showSuccessMessage:@"请输入正确手机号码" inView:self.view];
    }else{
        getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
        getVerifyBtn.enabled=NO;
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        [self requestVerifyHttpsForPhone:cartText.text];
    }
}

-(void)setTimer:(NSTimer *)timer{
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        getVerifyBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

#pragma mark - request 获取验证码请求
-(void)requestVerifyHttpsForPhone:(NSString *)phone{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:@"send_sms_code3" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestVerifyHttpsForPhone:cartText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                receiverify=responseObject[@"retData"][@"code"];
            }
            NSLog(@"%@",responseObject[@"retData"][@"code"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 提交按钮
/*
 *1、判断验证码与输入是否正确
 *2、查询该号码是否被注册过
 *3、没被注册则提交（否则提示错误）
 */
-(void)clickBtn{
    NSString *verifyStr=[NSString stringWithFormat:@"%@",receiverify];
    if (![verifyStr isEqualToString:passText.text]) {
        [AppUtils showSuccessMessage:@"验证码有误" inView:self.view];
    }else{
        [self checkPhoneIsRegist:cartText.text];
    }
}

#pragma mark - request查询手机是否已注册
-(BOOL)checkPhoneIsRegist:(NSString *)phone{
    __block BOOL res = NO;
    NSDictionary *dict=@{
                         @"act":@"check_mobile_phone",
                         @"is_phone":@"1",
                         @"mobile_phone":phone
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //=========================
        //error表示存在，succe表示没有
        //=========================
        NSString *str=responseObject[@"retMsg"];
        if ([str isEqualToString:@"success"]) {
            res=YES;
            [self requestVerifyAndPhoneForPhone:cartText.text verify:passText.text];
        }else{
            [AppUtils showSuccessMessage:@"该号码已被注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
    return res;
}

#pragma mark - request绑定手机号、验证码
-(void)requestVerifyAndPhoneForPhone:(NSString *)phone verify:(NSString *)verify{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"act_phone_edit_bind_act_2" forKey:@"act"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:verify forKey:@"smscode"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestVerifyAndPhoneForPhone:cartText.text verify:passText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //修改成功覆盖存储的账号
                [AppUtils saveValue:phone forKey:User_Account];
                
                [AppUtils saveValue:phone forKey:User_Phone];
                
                [AppUtils showSuccessMessage:@"验证成功" inView:self.view];
                sleep(1);
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
                NSLog(@"%@",responseObject[@"retData"][@"message"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
