//
//  AccounInfoController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "AccounInfoController.h"
#import "PassWordModifyController.h"
#import "Macro2.h"
#import "MMGLoginController.h"

#define TitleColor @"#292929"
#define ThisYellowColor @"#ec6b00"
@interface AccounInfoController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@end

@implementation AccounInfoController{
    UIImageView *userImg;
    UILabel *userPhone;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"账户资料"];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.userInteractionEnabled=YES;
    bagView.image=[UIImage imageNamed:@"login_baground"];
    [self.view addSubview:bagView];
    
    UIView *mainView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 150)];
    mainView.layer.cornerRadius=3;
    mainView.backgroundColor=[UIColor whiteColor];
    [bagView addSubview:mainView];
    
    //用户头像
    userImg=[[UIImageView alloc]init];
    [mainView addSubview:userImg];
    userImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* userImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[userImg(70)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userImg)];
    [NSLayoutConstraint activateConstraints:userImg_h];
    
    NSArray* userImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[userImg(70)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userImg)];
    [NSLayoutConstraint activateConstraints:userImg_w];
    if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
        userImg.image=[UIImage imageNamed:@"user_default"];
    }else{
        [userImg setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
    }
    
    //用户名
    UILabel *userName=[[UILabel alloc]init];
    userName.font=[UIFont systemFontOfSize:14];
    userName.textColor=[UIColor colorWithHexString:TitleColor];
    [mainView addSubview:userName];
    userName.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* userName_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[userImg]-12-[userName]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userImg,userName)];
    [NSLayoutConstraint activateConstraints:userName_h];
    
    NSArray* userName_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[userName(21)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userName)];
    [NSLayoutConstraint activateConstraints:userName_w];
    userName.text=[NSString stringWithFormat:@"用户名：%@",[AppUtils getValueWithKey:User_Name]];
    
    //手机号
    userPhone=[[UILabel alloc]init];
    userPhone.font=[UIFont systemFontOfSize:14];
    userPhone.textColor=[UIColor colorWithHexString:TitleColor];
    [mainView addSubview:userPhone];
    userPhone.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* userPhone_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[userImg]-12-[userPhone]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userImg,userPhone)];
    [NSLayoutConstraint activateConstraints:userPhone_h];
    
    NSArray* userPhone_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[userName]-4-[userPhone(21)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userName,userPhone)];
    [NSLayoutConstraint activateConstraints:userPhone_w];
    
    if([AppUtils checkPhoneNumber:[AppUtils getValueWithKey:User_Phone]]){
        //处理字符串
        NSRange phoneRange = NSMakeRange(3, 4);
        NSMutableString *handlePhoneNum = [NSMutableString stringWithString:[AppUtils getValueWithKey:User_Phone]];
        [handlePhoneNum replaceCharactersInRange:phoneRange withString:@"****"];
        userPhone.text=[NSString stringWithFormat:@"手机号：%@",handlePhoneNum];
    }else{
        userPhone.text=@"手机号：未绑定";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBangingPhone) name:@"UPDATE_BANDINGPHONE" object:nil];
    
    //line
    UIView *line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [mainView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[userImg]-8-[line(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userImg,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    
    //button 1
    UIButton *photoBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [photoBtn addTarget:self action:@selector(clickphotoBtn) forControlEvents:UIControlEventTouchUpInside];
    photoBtn.backgroundColor=[UIColor colorWithHexString:@"#ababab"];
    [photoBtn setTitle:@"修改头像" forState:UIControlStateNormal];
    photoBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [photoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mainView addSubview:photoBtn];
    photoBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* photoBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[photoBtn(90)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(photoBtn)];
    [NSLayoutConstraint activateConstraints:photoBtn_h];
    
    NSArray* photoBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line]-16-[photoBtn]-16-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,photoBtn)];
    [NSLayoutConstraint activateConstraints:photoBtn_w];
    photoBtn.layer.cornerRadius=3;
    
    //button 2
    UIButton *psdBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [psdBtn addTarget:self action:@selector(clickPsdBtn) forControlEvents:UIControlEventTouchUpInside];
    psdBtn.backgroundColor=[UIColor colorWithHexString:ThisYellowColor];
    [psdBtn setTitle:@"修改密码" forState:UIControlStateNormal];
    psdBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [psdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mainView addSubview:psdBtn];
    psdBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* psdBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[psdBtn(90)]-40-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(psdBtn)];
    [NSLayoutConstraint activateConstraints:psdBtn_h];
    
    NSArray* psdBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line]-16-[psdBtn]-16-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,psdBtn)];
    [NSLayoutConstraint activateConstraints:psdBtn_w];
    psdBtn.layer.cornerRadius=3;
    
    [self setLogoutView];
}

-(void)updateBangingPhone{
    //处理字符串
    NSRange phoneRange = NSMakeRange(3, 4);
    NSMutableString *handlePhoneNum = [NSMutableString stringWithString:[AppUtils getValueWithKey:User_Phone]];
    [handlePhoneNum replaceCharactersInRange:phoneRange withString:@"****"];
    userPhone.text=[NSString stringWithFormat:@"手机号：%@",handlePhoneNum];
}

#pragma mark - 退出登录
-(void)setLogoutView{
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-120, SCREENSIZE.width, 60)];
    UIButton *cencelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cencelBtn.frame=CGRectMake(12, 0, SCREENSIZE.width-28, 44);
    cencelBtn.layer.cornerRadius=4;
    [footView addSubview:cencelBtn];
    cencelBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    cencelBtn.backgroundColor=[UIColor redColor];
    [cencelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cencelBtn setTitle:@"退出当前账号" forState:UIControlStateNormal];
    [cencelBtn addTarget:self action:@selector(clickCencelBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:footView];
}

-(void)clickCencelBtn{
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=23;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self logout];
                break;
            default:
                break;
        }
    };
    NSArray *items =
    @[MMItemMake(@"退出登录", MMItemTypeHighlight, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"退出后不会删除任何历史数据，下次登录依然可以使用本账号"
                                  items:items] showWithBlock:nil];
    
}

#pragma mark - 退出处理
//=================
//弹出登录、清空密码、
//发送通知以改变tabbar选中状态、
//并pop当前controller
//=================
-(void)logout{
    self.app.isLogin=NO;
    LoginController *login=[[LoginController alloc]init];
    UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
    [self presentViewController:loginNav animated:YES completion:^{
        [AppUtils saveValue:@"" forKey:User_Pass];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLogOut object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - 修改头像
-(void)clickphotoBtn{
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:ThisYellowColor];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            case 1:
                [self pickImage];
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:completeBlock];
}

-(void)clickPsdBtn{
    PassWordModifyController *psdModify=[[PassWordModifyController alloc]init];
    [self.navigationController pushViewController:psdModify animated:YES];
}

//拍照
- (void) snapImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image= [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(120.0, 120.0)];
    NSData* imageData = UIImagePNGRepresentation(theImage);
    
    [self requestHttpsForImg:imageData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark--按比例压缩图片--
//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark 保存图片到document
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imageData writeToFile:fullPathToFile atomically:NO];
}

#pragma mark Request 请求
-(void)requestHttpsForImg:(NSData *)imgDate{
    NSLog(@"imgDate=%@",imgDate);
    NSDictionary *dict=@{
                         @"act":@"iso_upload_img",
                         @"is_phone":@"1",
                         @"user_id":[AppUtils getValueWithKey:User_ID],
                         @"imgs":imgDate,
                         @"img_type":@"png",
                         @"is_nosign":@"1"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *newPhtotStr=responseObject[@"retData"][@"path"];
            [AppUtils saveValue:[NSString stringWithFormat:@"%@%@",ImageHost01,newPhtotStr] forKey:User_Photo];
            [userImg setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationUpdatePhoto object:nil];
            
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
