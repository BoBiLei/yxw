//
//  NewGoodsAddedController.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "NewGoodsAddedController.h"
#import "GoodsAddedFirstCell.h"
#import "NewGoodsAddedSecondCell.h"
#import "NewGoodsAddedSecondBtmCell.h"
#import "NewGoodsAddedThreeTopCell.h"
#import "PictureCell.h"
#import "NewGoodsAddedFourCell.h"
#import "GoodsAddTypeCell.h"
#import "CustomPickerView.h"
#import "MMPickerView.h"
#import "SubmitOrder_SeletedTypeModel.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TZImagePickerController.h"
#import "TZImageManager.h"
#import <IDMPhotoBrowser.h>
#import "ShangjiaMacro.h"
#import "HttpRequests.h"
#import "Macro2.h"
@interface NewGoodsAddedController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,NewAddGoodsFirstDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GoodsTypeDelegate,SubmitGoodsDelegate,NewGoodsAddDelegate,TZImagePickerControllerDelegate>

@end

@implementation NewGoodsAddedController{
    
    UICollectionView *myCollectionView;
    
    NSMutableArray *fenleiArr;          //分类Array
    NSMutableArray *erjFlArr;           //二级分类Array
    NSMutableArray *branchArr;          //商品品牌Array
    NSMutableArray *goodTypeArr;        //商品类型
    NSMutableArray *paramArr;           //商品品牌Array
    NSMutableArray *useInfoArr;         //使用情况
    
    NSString *seleFenleiId;
    NSString *seleErjiFlId;             //提交的分类ID
    NSString *seleBranceId;             //提交的品牌ID
    NSString *seleuseInfoId;            //提交的使用情况ID
    NSString *ershouIdTag;              //标识二手选择的
    NSString *seleGoodTypeId;           //提交的商品类型ID
    NSString *selePraemItemId;
    NSString *subGoodsName;             //提交的商品名称
    NSString *subHopePrice;             //提交的期望价格
    NSString *subComment;               //提交的备注说明
    NSMutableArray *subParamIDArr;      //提交属性ID数组
    NSMutableArray *subParamValueArr;   //提交属性值数组
    
    NSMutableArray *upLoadImgArr;       //
    
    NSMutableArray *_selectedAssets;
    
    //
    CGFloat btmViewHeight;
}


-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:MMG_BLUECOLOR];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"上架商品";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self seNavBar];
    [self setUpUI];
    
    [self requestData];
}

-(void)setUpUI{
    
    subComment=@"";
    
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    myCollectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    myCollectionView.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    myCollectionView.dataSource=self;
    myCollectionView.delegate=self;
    [myCollectionView registerClass:[GoodsAddedFirstCell class] forCellWithReuseIdentifier:@"GoodsAddedFirstCell"];
    [myCollectionView registerClass:[GoodsAddTypeCell class] forCellWithReuseIdentifier:@"GoodsAddTypeCell"];
    [myCollectionView registerClass:[NewGoodsAddedSecondCell class] forCellWithReuseIdentifier:@"NewGoodsAddedSecondCell"];
    [myCollectionView registerClass:[NewGoodsAddedSecondBtmCell class] forCellWithReuseIdentifier:@"NewGoodsAddedSecondBtmCell"];
    [myCollectionView registerClass:[NewGoodsAddedThreeTopCell class] forCellWithReuseIdentifier:@"NewGoodsAddedThreeTopCell"];
    [myCollectionView registerClass:[PictureCell class] forCellWithReuseIdentifier:@"PictureCell"];
    [myCollectionView registerClass:[NewGoodsAddedFourCell class] forCellWithReuseIdentifier:@"NewGoodsAddedFourCell"];
    [self.view addSubview:myCollectionView];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 7;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0||section==1||section==3||section==4||section==6) {
        return 1;
    }else if (section==2){
        return paramArr.count;
    }else{
        return upLoadImgArr.count+1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        GoodsAddedFirstCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsAddedFirstCell" forIndexPath:indexPath];
        cell.delegate=self;
        cell.GoodsNameBlock=^(NSString *tfText){
            subGoodsName=tfText;
        };
        return cell;
        return cell;
    }else if (indexPath.section==1) {
        GoodsAddTypeCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsAddTypeCell" forIndexPath:indexPath];
        cell.delegate=self;
        return cell;
    }else if (indexPath.section==2) {
        NewGoodsAddedSecondCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedSecondCell" forIndexPath:indexPath];
        if (indexPath.row<paramArr.count) {
            SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
            cell.model=model;
            cell.delegate=self;
            cell.nameTF.tag=indexPath.row;
        }
        return cell;
    }else if (indexPath.section==3){
        NewGoodsAddedSecondBtmCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedSecondBtmCell" forIndexPath:indexPath];
        cell.hopePriceBlock=^(NSString *tfText){
            subHopePrice=tfText;
        };
        cell.commentTextBlock=^(NSString *tfText){
            subComment=tfText;
        };
        return cell;
    }else if (indexPath.section==4){
        NewGoodsAddedThreeTopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedThreeTopCell" forIndexPath:indexPath];
        return cell;
    }else if (indexPath.section==5){
        PictureCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PictureCell" forIndexPath:indexPath];
        if (indexPath.row!=upLoadImgArr.count) {
            UIImage *img=upLoadImgArr[indexPath.row];
            cell.imageView.image=img;
            cell.deleteBtn.hidden = NO;
        }else{
            cell.imageView.image = [UIImage imageNamed:@"shangjia_sc_imgv"];
            cell.deleteBtn.hidden = YES;
        }
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        NewGoodsAddedFourCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewGoodsAddedFourCell" forIndexPath:indexPath];
        cell.delegate=self;
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            return CGSizeMake(SCREENSIZE.width, 338);
        }
            break;
        case 1:
            return CGSizeMake(SCREENSIZE.width, 58);
            break;
        case 2:
            return CGSizeMake(SCREENSIZE.width, 58);
            break;
        case 3:
        {
            return CGSizeMake(SCREENSIZE.width, 232);
        }
            break;
        case 4:
            return CGSizeMake(SCREENSIZE.width, 72);
            break;
        case 5:
            return CGSizeMake((SCREENSIZE.width)/3-12, SCREENSIZE.width/3-12);
            break;
        default:
        {
            return CGSizeMake(SCREENSIZE.width, 148);
        }
            break;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==1||section==2||section==4) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else if(section==5){
        return UIEdgeInsetsMake(6, 6, 6, 6);
    }else{
        return UIEdgeInsetsMake(0, 0, 18, 0);
    }
}

//定义每个item间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==5) {
        return 6;
    }else{
        return 0;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==2) {
        /*
            重要方法
         */
        SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
        if ([model.input_type isEqualToString:@"1"]) {
            CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:model.paramArr_Item selectedTypeId:model.seleTag];
            [pick showPickerViewCompletion:^(id selectedObject) {
                SubmitOrder_SeletedTypeModel *seleModel=selectedObject;
                model.seleTag=[NSString stringWithFormat:@"%@",seleModel.typeId];
                model.typeName=seleModel.typeName;
                [paramArr replaceObjectAtIndex:indexPath.row withObject:model];
                //刷新行
                [paramArr replaceObjectAtIndex:indexPath.row withObject:model];
                [myCollectionView reloadItemsAtIndexPaths:@[indexPath]];
            }];
        }
    }else if (indexPath.section==5){
        if (indexPath.row==upLoadImgArr.count) {
            [[MMPopupWindow sharedWindow] cacheWindow];
            [MMPopupWindow sharedWindow].touchWildToHide = YES;
            
            MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
            sheetConfig.buttonFontSize=15;
            sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
            
            MMPopupItemHandler block = ^(NSInteger index){
                switch (index) {
                    case 0:
                        [self snapImage];
                        break;
                    case 1:
                        [self pickImage];
                        break;
                    default:
                        break;
                }
            };
            
            MMPopupBlock completeBlock = ^(MMPopupView *popupView){
                
            };
            NSArray *items =
            @[MMItemMake(@"拍照", MMItemTypeNormal, block),
              MMItemMake(@"从相册选择", MMItemTypeNormal, block)];
            
            [[[MMSheetView alloc] initWithTitle:nil
                                          items:items] showWithBlock:completeBlock];
        }else{
            UIImage *img=upLoadImgArr[indexPath.row];
            NSArray *photo=[IDMPhoto photosWithImages:@[img]];
            IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photo];
            browser.displayToolbar=NO;
            browser.displayCounterLabel = YES;
            browser.displayActionButton = NO;
            
            // Show
            [self presentViewController:browser animated:YES completion:nil];
        }
    }
}

#pragma mark - 点击图片删除按钮
- (void)deleteBtnClik:(UIButton *)sender {
    [upLoadImgArr removeObjectAtIndex:sender.tag];
//    [_selectedAssets removeObjectAtIndex:sender.tag];
    
    [myCollectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:5];
        [myCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [myCollectionView reloadData];
    }];
}

#pragma mark - 上架商品第一步 Delegate
//点击分类
-(void)clickFenlei{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:fenleiArr selectedTypeId:seleFenleiId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        
        [erjFlArr removeAllObjects];
        
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleFenleiId=model.typeId;
        //默认二级分类
        NSArray *ejflArr=model.ejFlArr;
        if (ejflArr.count!=0) {
            for (NSDictionary *dic in ejflArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getFenleiJsonDataForDictionary:dic];
                [erjFlArr addObject:model];
            }
        }else{
            SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
            model.typeId=@"0";
            model.typeName=@"0";
            [erjFlArr addObject:model];
        }
        
        SubmitOrder_SeletedTypeModel *erjflModel=erjFlArr[0];
        seleErjiFlId=erjflModel.typeId;
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:erjflModel];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Fenlei_Notification object:model];
    }];
}

//点击二级分类
-(void)clickErjiFenlei{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:erjFlArr selectedTypeId:seleErjiFlId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleErjiFlId=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:model];
    }];
}

//点击商品品牌
-(void)clickPinpai{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:branchArr selectedTypeId:seleBranceId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleBranceId=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Brance_Notification object:model];
    }];
}

//点击二手列表
-(void)clickErShouList{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:useInfoArr selectedTypeId:ershouIdTag];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleuseInfoId=[NSString stringWithFormat:@"%@",model.typeId];
        ershouIdTag=[NSString stringWithFormat:@"%@",model.typeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_UseInfo_Notification object:model];
    }];
}

//点击全新按钮
-(void)hadClickNewBtn{
    seleuseInfoId=@"1";
}

//点击二手按钮
-(void)hadClickErShouBtn{
    seleuseInfoId=ershouIdTag;
}

//点击商品类型
-(void)clickGoodsType{
    [AppUtils closeKeyboard];
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:goodTypeArr selectedTypeId:seleGoodTypeId];
    
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleGoodTypeId=[NSString stringWithFormat:@"%@",model.typeId];
        [self requestParamData:seleGoodTypeId];
        [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_GoodType_Notification object:model];
    }];
}

-(void)setNameTextfield:(NSString *)nameStr indexPath:(NSIndexPath *)indexPath{
    SubmitOrder_SeletedTypeModel *model=paramArr[indexPath.row];
    model.typeName=nameStr;
    
    model.input_type=[nameStr isEqualToString:@""]||nameStr==nil?@"0":@"3";
    [paramArr replaceObjectAtIndex:indexPath.row withObject:model];
    
    if (indexPath!=nil) {
        [myCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    }
}

//点击提交按钮
-(void)clickSubmitBtn{
    
    /*
     NSLog(@"\n商品名称=%@\n分类ID=%@\n品牌ID=%@\n使用情况ID=%@",subGoodsName,seleErjiFlId,seleBranceId,seleuseInfoId);
     */
    if (subGoodsName.length==0) {
        [AppUtils showSuccessMessage:@"请填写商品名称" inView:self.view];
    }else if([seleGoodTypeId isEqualToString:@""]||seleGoodTypeId==nil){
        [AppUtils showSuccessMessage:@"请选择商品类型" inView:self.view];
    }else{
        subParamIDArr=[NSMutableArray array];
        subParamValueArr=[NSMutableArray array];
        for (SubmitOrder_SeletedTypeModel *model in paramArr) {
            [subParamIDArr addObject:model.paramArr_ItemId];
            if(model.typeName==nil||[model.typeName isEqualToString:@""]){
                //[AppUtils showSuccessMessage:[NSString stringWithFormat:@"请填写%@",model.paramArr_ItemName] inView:self.view];
                //return;
                [subParamValueArr addObject:@""];
            }else{
                [subParamValueArr addObject:model.typeName];
            }
        }
        
        //
        if(subHopePrice.length==0){
            [AppUtils showSuccessMessage:@"请填写期望价格" inView:self.view];
        }else if(upLoadImgArr.count==0){
            [AppUtils showSuccessMessage:@"至少选择一张图片" inView:self.view];
        }else{
            [self submitRequest];
        }
    }
}

#pragma mark - 拍照
- (void)snapImage{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"请在iPhone的“设置-隐私-相机”选项中，允许猫猫购商户版访问您的相机" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.videoQuality = UIImagePickerControllerQualityTypeMedium;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing=YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark UIImagePickerController 拍照选择照片后回调 delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:@"public.image"]) {
        TZImagePickerController *pickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:30 delegate:nil];
        pickerVc.sortAscendingByModificationDate = YES;//拍照按拍摄时间排序
        [pickerVc showProgressHUD];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"%@",info);
        //保存图片，获取到asset
        [[TZImageManager manager] savePhotoWithImage:image completion:^{
            [[TZImageManager manager] getCameraRollAlbum:NO allowPickingImage:YES completion:^(TZAlbumModel *model) {
                [[TZImageManager manager] getAssetsFromFetchResult:model.result allowPickingVideo:NO allowPickingImage:YES completion:^(NSArray<TZAssetModel *> *models) {
                    [pickerVc hideProgressHUD];
                    TZAssetModel *assetModel = [models firstObject];
                    if (pickerVc.sortAscendingByModificationDate) {
                        assetModel = [models lastObject];
                    }
                    [_selectedAssets addObject:assetModel.asset];
                    [upLoadImgArr addObject:image];
                    [myCollectionView reloadData];
                }];
            }];
        }];
    }
}

#pragma mark - 从相册选择
- (void) pickImage{
    TZImagePickerController *pickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:30 delegate:nil];
    pickerVc.navigationBar.barTintColor = [UIColor colorWithHexString:@"#282828"];
    pickerVc.navigationBar.tintColor = [UIColor colorWithHexString:@"#282828"];//旁边按钮颜色
    pickerVc.barItemTextFont=[UIFont systemFontOfSize:17];
    NSDictionary * dict = [NSDictionary dictionaryWithObject:[UIColor colorWithHexString:@"#282828"] forKey:NSForegroundColorAttributeName];
    pickerVc.navigationBar.titleTextAttributes = dict;
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    pickerVc.isSelectOriginalPhoto = YES;    //原图
    
    // 1.如果你需要将拍照按钮放在外面，不要传这个参数
    pickerVc.selectedAssets = _selectedAssets; // optional, 可选的
    pickerVc.allowTakePicture = NO;         // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    pickerVc.navigationBar.barTintColor = [UIColor colorWithHexString:@"#ec6b00"];
    pickerVc.oKButtonTitleColorDisabled = [UIColor colorWithHexString:@"#ec6b00"];
    pickerVc.oKButtonTitleColorNormal = [UIColor colorWithHexString:@"#ec6b00"];
    
    // 3. 设置是否可以选择视频/图片/原图
    pickerVc.allowPickingVideo = NO;
    pickerVc.allowPickingImage = YES;
    pickerVc.allowPickingOriginalPhoto = YES;
    
    // 4. 照片排列按修改时间升序
    pickerVc.sortAscendingByModificationDate = YES;
    
    // 你可以通过block或者代理，来得到用户选择的照片.
    [pickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [upLoadImgArr addObjectsFromArray:photos];
        _selectedAssets = [NSMutableArray arrayWithArray:assets];
        [myCollectionView reloadData];
    }];
    
    [self presentViewController:pickerVc animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - TZImagePickerController Delegate
//选择相册照片后返回
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    NSLog(@"----------");
}

-(UIImage *) imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth
{
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = (targetWidth / width) * height;
    UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
    [sourceImage drawInRect:CGRectMake(0,0,targetWidth,  targetHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Request 数据请求

-(void)requestData{
    
    fenleiArr=[NSMutableArray array];
    erjFlArr=[NSMutableArray array];
    branchArr=[NSMutableArray array];
    goodTypeArr=[NSMutableArray array];
    useInfoArr=[NSMutableArray array];
    upLoadImgArr=[NSMutableArray array];
    seleuseInfoId=@"1";             //默认是全新=1
    ershouIdTag=@"2";
    for (int i=2; i<=6; i++) {
        NSString *nameStr;
        switch (i) {
            case 2:
                nameStr=@"99新";
                break;
            case 3:
                nameStr=@"95新";
                break;
            case 4:
                nameStr=@"9成新";
                break;
            case 5:
                nameStr=@"85新";
                break;
            default:
                nameStr=@"8成新";
                break;
        }
        SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
        model.typeId=[NSString stringWithFormat:@"%d",i];
        model.typeName=nameStr;
        [useInfoArr addObject:model];
    }
    /*
     for (int i=2; i<=13; i++) {
     NSString *nameStr;
     switch (i) {
     case 2:
     nameStr=@"99新";
     break;
     case 3:
     nameStr=@"98新";
     break;
     case 4:
     nameStr=@"97新";
     break;
     case 5:
     nameStr=@"96新";
     break;
     case 6:
     nameStr=@"95新";
     break;
     case 7:
     nameStr=@"94新";
     break;
     case 8:
     nameStr=@"93新";
     break;
     case 9:
     nameStr=@"92新";
     break;
     case 10:
     nameStr=@"91新";
     break;
     case 11:
     nameStr=@"9成新";
     break;
     case 12:
     nameStr=@"85新";
     break;
     case 13:
     nameStr=@"8成新";
     break;
     default:
     nameStr=@"8成新";
     break;
     }
     SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
     model.typeId=[NSString stringWithFormat:@"%d",i];
     model.typeName=nameStr;
     [useInfoArr addObject:model];
     }
     */
    
    //
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@""
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@up_goods.php?act=add",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //获取大分类属性
            NSArray *flArr=responseObject[@"retData"][@"cate_list"];
            for (NSDictionary *dic in flArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getFenleiJsonDataForDictionary:dic];
                [fenleiArr addObject:model];
            }
            SubmitOrder_SeletedTypeModel *flModel=fenleiArr[0];
            seleFenleiId=flModel.typeId;
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Fenlei_Notification object:flModel];
            
            //默认二级分类
            SubmitOrder_SeletedTypeModel *ejModel=fenleiArr[0];
            NSArray *ejflArr=ejModel.ejFlArr;
            for (NSDictionary *dic in ejflArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getFenleiJsonDataForDictionary:dic];
                [erjFlArr addObject:model];
            }
            SubmitOrder_SeletedTypeModel *erjflModel=erjFlArr[0];
            seleErjiFlId=erjflModel.typeId;
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_ErjiFl_Notification object:erjflModel];
            
            //获取品牌属性
            NSArray *ppArr=responseObject[@"retData"][@"brand_list"];
            for (NSDictionary *dic in ppArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getBranceJsonDataForDictionary:dic];
                [branchArr addObject:model];
            }
            SubmitOrder_SeletedTypeModel *branceModel=branchArr[0];
            seleBranceId=branceModel.typeId;
            [[NSNotificationCenter defaultCenter] postNotificationName:Shangjia_Brance_Notification object:branceModel];
            
            //获取商品类型
            NSArray *lxArr=responseObject[@"retData"][@"goods_type_list"];
            for (NSDictionary *dic in lxArr) {
                SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
                [model getGoodTypeDataForDictionary:dic];
                [goodTypeArr addObject:model];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)requestParamData:(NSString *)goodTypeId{
    paramArr=[NSMutableArray array];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"by_cate_attr",
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"goods_type":goodTypeId
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@up_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        //获取Param属性
        NSArray *prArr=responseObject[@"retData"];
        for (NSDictionary *dic in prArr) {
            SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
            [model getParamDataForDictionary:dic];
            [paramArr addObject:model];
        }
        [UIView animateWithDuration:0.0000000001 animations:^{
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:2];
            [myCollectionView reloadSections:indexSet];
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)submitRequest{
    [AppUtils showProgressInView:self.view];
    NSMutableArray *imgs=[NSMutableArray array];
    for (UIImage *img in upLoadImgArr) {
        NSData *imageData = UIImageJPEGRepresentation(img, 0.2);
        NSMutableData *pictrueData=[NSMutableData data];
        [pictrueData appendData:imageData];
        [imgs addObject:pictrueData];
    }
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"insert_done",
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@"",
                         @"goods_name":subGoodsName,
                         @"cat_id":seleErjiFlId,
                         @"brand_id":seleBranceId,
                         @"level":seleuseInfoId,
                         @"jiesuan_price":subHopePrice,
                         @"explain":subComment,
                         @"attr_id_list":subParamIDArr,
                         @"attr_value_list":subParamValueArr,
                         @"goods_type":seleGoodTypeId,
                         @"goods_img":imgs
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@up_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"添加商品成功！" inView:self.view];
            // 延迟加载
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Insert_success" object:nil];
            });
        }
        [myCollectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@-%@",error,error.localizedFailureReason);
    }];
}

@end
