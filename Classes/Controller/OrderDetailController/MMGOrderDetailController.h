//
//  MMGOrderDetailController.h
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface MMGOrderDetailController : BaseController

@property (nonatomic,copy) NSString *orderId;

@end
