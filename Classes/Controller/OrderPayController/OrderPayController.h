//
//  OrderPayController.h
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"
#import "WXApi.h"
@interface OrderPayController : BaseController

@property (nonatomic,copy) NSString *selectedPayId; //选择的支付方式ID
@property (nonatomic,copy) NSString *orderId;
@property(nonatomic,copy)NSString *wxTradeNo;
@property(nonatomic) id<WXApiDelegate> delegate;
@end
