//
//  OrderPayController.m
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderPayController.h"
#import "OrderPayCell.h"
#import "MMGPayTypeCell.h"
#import "MMGAliPay.h"
#import "PayFinishController.h"
#import "UserOrderController.h"
#import "ShoppingCartController.h"
#import "MMGOrderDetailController.h"
#import "Macro2.h"
#import "FilmWeiXinPayModel.h"
#import "PaySuccessController.h"
#import "TestController.h"

#define sectionTitle05 @"收款账户信息（注:暂不支持建设银行卡转账）";
#define sectionTitle06 @"账户名：深圳市游侠网国际旅行社有限公司";
#define sectionTitle07 @"开户银行：工商银行深圳分行宝民支行";
#define sectionTitle08 @"银行账号：40000 9250 9100 1208 78";
@interface OrderPayController ()<UITableViewDataSource,UITableViewDelegate,PayFinishDelegate,OrderPayCellDelegate,UIGestureRecognizerDelegate>

@end

@implementation OrderPayController{
    CGRect tableFrame;
    NSMutableArray *payMsgDicArr;
    NSMutableArray *payTypeArr;
    NSArray *dataArr;
    UITableView *myTable;
    NSInteger payType;
    NSIndexPath *selectedIndexPath;
    UIView *buyView;    //立即购买的view
    //
    NSString *orderNumber;
    NSString *orderShopDescription;
    NSString *payPrice;
    NSString *couponId;  //如果为0，说明没有优惠券
}



-(void)setNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    
    
    [self.view addSubview:statusBarView];
    
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(flagTurnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"订单支付";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
    
}
-(void)fanhui
{
    
    [self .navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //支付监听从后台进入前台的通知（重要哦）
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxSender:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self setNavBar];
    //    [self setCustomNavigationTitle:@"订单支付"];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    [self requestHttpsForOrderPay:self.orderId];
    
    [self setUpUI];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}

#pragma mark - 覆盖BaseController的setCustomNavigationTitle方法
-(void)setCustomNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 64 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = [UIFont systemFontOfSize:NAV_TITLE_FONT_SIZE];
    navTitle.textColor = [UIColor colorWithHexString:@"#282828"];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,22.f,22.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(flagTurnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
}

-(void)flagTurnBack{
    
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否放弃付款"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[UserOrderController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
            if ([controller isKindOfClass:[ShoppingCartController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    
    UIAlertAction*otherAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:otherAction];
    [self presentViewController:alertController animated:YES completion:nil];
}




#pragma mark - 查询订单状态
-(void)wxSender:(id)sender{
    [self payResultRequest];
//    BaseResp * resp = [sender object];
//    
//    if([resp isKindOfClass:[PayResp class]]){
//        
//        switch (resp.errCode) {
//            case WXSuccess:
//                NSLog(@"支付结果：成功！");
//                [self requestHttpsSetOrderStatusForOrderNum:orderNumber orderAmout:payPrice];
//                break;
//            default:
//                NSLog(@"支付结果：失败！");
//                break;
//        }
//    }
}

#pragma mark - 支付宝支付回调
//-(void)alipaySender:(id)sender{
//    NSDictionary *resultDic=[sender object];
//    NSString *statusStr=resultDic[@"resultStatus"];
//    int statusInt=statusStr.intValue;
//    switch (statusInt) {
//        case 9000:
//            NSLog(@"支付成功----");
//            [self payResultRequest];
//            break;
//        case 4000:
//            NSLog(@"订单支付失败----");
//            break;
//        case 6001:
//            NSLog(@"用户中途取消----");
//            break;
//        case 6002:
//            NSLog(@"网络连接出错----");
//            break;
//        default:
//            break;
//    }
//}



//查询订单支付状态
/*
 -(void)wxPayResultRequest{
 [AppUtils showProgressInView:self.view];
 //
 time_t now;
 time(&now);
 NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
 NSString *nonce_str	= [YXWSign randomNumber];
 NSMutableDictionary *dict=[NSMutableDictionary dictionary];
 [dict setObject:@"1" forKey:@"is_iso"];
 [dict setObject:@"m_order" forKey:@"mod"];
 [dict setObject:@"queryOrder" forKey:@"code"];
 [dict setObject:_orderId forKey:@"orderid"];
 [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
 [dict setObject:time_stamp forKey:@"stamp"];
 [dict setObject:nonce_str forKey:@"noncestr"];
 [dict setObject:@"1" forKey:@"is_nosign"];
 //    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
 //    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
 //    [dict setObject:sign forKey:@"sign"];
 
 [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
 
 } success:^(NSURLSessionDataTask *task, id responseObject) {
 [AppUtils dismissHUDInView:self.view];
 NSLog(@"%@",responseObject);
 NSLog(@"查询订单支付状态=%@",responseObject);
 
 if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
 NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"status"]];
 if ([str isEqualToString:@"2"]) {
 TourSuccessController *ctr=[TourSuccessController new];
 ctr.orderId=_orderId;
 [self.navigationController pushViewController:ctr animated:YES];
 }else if([str isEqualToString:@"1"]){
 WaitTourController *ctr=[WaitTourController new];
 ctr.orderId=_orderId;
 [self.navigationController pushViewController:ctr animated:YES];
 }else if([str isEqualToString:@"5"]){
 FilmTicketCloseController *ctr=[FilmTicketCloseController new];
 ctr.orderId=_orderId;
 [self.navigationController pushViewController:ctr animated:YES];
 }
 }else{
 [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
 }
 } failure:^(NSURLSessionDataTask *task, NSError *error) {
 [AppUtils dismissHUDInView:self.view];
 DSLog(@"%@",error);
 }];
 }
 */
#pragma mark - init UI
-(void)setUpUI{
    payType=0;
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"PayType2" ofType:@"plist"];
    payTypeArr = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    dataArr=@[@[@""],payTypeArr];
    
    payMsgDicArr=[NSMutableArray array];
    
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 72, SCREENSIZE.width-16, 38)];
    imgv.contentMode=UIViewContentModeScaleToFill;
    imgv.image=[UIImage imageNamed:@"submit_order02"];
    [self.view addSubview:imgv];
    
    tableFrame=CGRectMake(8, 64, SCREENSIZE.width-16, SCREENSIZE.height-64);
    myTable=[[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"OrderPayCell" bundle:nil] forCellReuseIdentifier:@"orderpaycell"];
    [myTable registerNib:[UINib nibWithNibName:@"MMGPayTypeCell" bundle:nil] forCellReuseIdentifier:@"MMGPayTypeCell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.sectionFooterHeight = 0;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:myTable];
    
    //footview
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 240)];
    footView.backgroundColor=[UIColor whiteColor];
    myTable.tableFooterView=footView;
    //
    UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, 6, footView.width-12, 36)];
    tipLabel.font=[UIFont systemFontOfSize:15];
    tipLabel.text=sectionTitle05;
    tipLabel.numberOfLines=0;
    [footView addSubview:tipLabel];
    //
    UILabel *accountLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, tipLabel.frame.origin.y+tipLabel.frame.size.height+6, footView.width-12, 16)];
    accountLabel.font=[UIFont systemFontOfSize:14];
    accountLabel.text=sectionTitle06;
    accountLabel.textColor=[UIColor lightGrayColor];
    [footView addSubview:accountLabel];
    //
    UILabel *bankLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, accountLabel.frame.origin.y+accountLabel.frame.size.height+6, footView.width-12, 16)];
    bankLabel.font=[UIFont systemFontOfSize:14];
    bankLabel.text=sectionTitle07;
    bankLabel.textColor=[UIColor lightGrayColor];
    [footView addSubview:bankLabel];
    //
    UILabel *bankAccount=[[UILabel alloc]initWithFrame:CGRectMake(6, bankLabel.frame.origin.y+bankLabel.frame.size.height+6, footView.width-12, 16)];
    bankAccount.font=[UIFont systemFontOfSize:14];
    bankAccount.text=sectionTitle08;
    bankAccount.textColor=[UIColor lightGrayColor];
    [footView addSubview:bankAccount];
    
    //
    //    UILabel *tip02=[[UILabel alloc]initWithFrame:CGRectMake(6, bankAccount.frame.origin.y+bankAccount.frame.size.height+8, footView.width-12, 16)];
    //    tip02.font=[UIFont systemFontOfSize:15];
    //    tip02.text=@"温馨提示：";
    //    [footView addSubview:tip02];
    //    //
    //    UILabel *tip02_1=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02.frame.origin.y+tip02.frame.size.height+4, footView.width-12, 36)];
    //    tip02_1.font=[UIFont systemFontOfSize:14];
    //    tip02_1.text=@"1. “订单提交成功”表明猫猫购收到了您的订单，只有您的订单审核通过后，才能确认订单生效。";
    //    tip02_1.numberOfLines=0;
    //    [footView addSubview:tip02_1];
    //    //
    //    UILabel *tip02_2=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02_1.frame.origin.y+tip02_1.frame.size.height+4, footView.width-12, 36)];
    //    tip02_2.font=[UIFont systemFontOfSize:14];
    //    tip02_2.text=@"2. 对于“猫猫购”售出的商品，我们为您提供7天退换货保障。";
    //    tip02_2.numberOfLines=0;
    //    [footView addSubview:tip02_2];
    //
    //    //
    //    UILabel *phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02_2.frame.origin.y+tip02_2.frame.size.height+6, footView.width-12, 16)];
    //    phoneLabel.font=[UIFont systemFontOfSize:14];
    //    phoneLabel.text=@"客服电话：0755-22211222";
    //    [footView addSubview:phoneLabel];
    //    UITapGestureRecognizer *tapgest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickPhoneLabel)];
    //    phoneLabel.userInteractionEnabled=YES;
    //    [phoneLabel addGestureRecognizer:tapgest];
    
    myTable.tableFooterView.hidden=YES;
    
    //立即购买的View
    buyView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-44, SCREENSIZE.width, 44)];
    buyView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:buyView];
    
    //button
    UIButton *submitBtn=[[UIButton alloc]init];
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"立即付款" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [submitBtn addTarget:self action:@selector(clickOrderPayBtn) forControlEvents:UIControlEventTouchUpInside];
    [buyView addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* submitBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[submitBtn]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_h];
    
    NSArray* submitBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[submitBtn]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_w];
}

-(void)clickPhoneLabel{
    UIWebView *webView;
    if (webView == nil) {
        webView = [[UIWebView alloc] init];
        webView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

#pragma mark - Request获取付款参数请求
-(void)requestHttpsForOrderPay:(NSString *)orderId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [dict setObject:@"pay" forKey:@"step"];
    [dict setObject:orderId forKey:@"order"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    //    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    //    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    //    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"》》》》》%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderPay:self.orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                couponId=responseObject[@"retData"][@"bonus_id"];
                
                orderNumber=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"order_sn"]];
                
                self.selectedPayId=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"pay_id"]];
                
                NSMutableString *orderShopDescriptionStr=[NSMutableString stringWithString:responseObject[@"retData"][@"good_names"]];
                NSRange range=[orderShopDescriptionStr rangeOfString:@","];
                if (range.location!=NSNotFound) {
                    NSString *newStr=[orderShopDescriptionStr substringToIndex:range.location];
                    orderShopDescription=newStr;
                }else{
                    orderShopDescription=orderShopDescriptionStr;
                }
                
                payPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"order_amount"]];
                
                [payMsgDicArr addObject:responseObject[@"retData"][@"order"]];
            }else{
                NSLog(@"%@",responseObject[@"retData"][@"msg"]);
            }
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}




//向服务器发起微信支付请求
-(void)weixinPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"pay_wx_iso" forKey:@"step"];
    [dict setObject:_orderId forKey:@"trade_no"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];

    [[NetWorkRequest defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"微信 ： %@",responseObject);
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"];
                if ([resObj isKindOfClass:[NSDictionary class]]) {
                    FilmWeiXinPayModel *weixinPayModel=[FilmWeiXinPayModel new];
                    [weixinPayModel jsonDataForDictioanry:resObj];
                    [self weixinPayWithModel:weixinPayModel];
                }
            }else
            {
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    }
            failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                [AppUtils dismissHUDInView:self.view];
                                                DSLog(@"%@",error);
                                            }];
}

// 向服务器发起支付宝请求
//-(void)aliPayRequest
//{
//    [AppUtils showProgressMessage:@"正在确认订单" inView:self.view];
//    time_t now;
//    time(&now);
//    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
//    NSString *nonce_str	= [YXWSign randomNumber];
//    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
//    NSString *uSr=[AppUtils getValueWithKey:User_Name];
//    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//    [dict setObject:@"1" forKey:@"is_iso"];
//    [dict setObject:@"1" forKey:@"is_nosign"];
//    [dict setObject:@"order" forKey:@"mod"];
//    [dict setObject:@"get_alipay" forKey:@"action"];
//    [dict setObject:_orderId forKey:@"id"];
//    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
//    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
//    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
//    [dict setObject:@"3" forKey:@"payid"];
//    [dict setObject:time_stamp forKey:@"stamp"];
//    [dict setObject:nonce_str forKey:@"noncestr"];
////    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
////    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
////    [dict setObject:sign forKey:@"sign"];
//    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
//
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        [AppUtils dismissHUDInView:self.view];
//        NSLog(@"ali ： %@",responseObject);
//
//        if ([responseObject isKindOfClass:[NSNull class]]||
//            responseObject==nil||
//            responseObject==NULL) {
//            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
//        }else{
//            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
//                id resObj=responseObject[@"retData"][@"result"];
//                if ([resObj isKindOfClass:[NSString class]]) {
//                    [self alliPayWithId:resObj];
//                }
//            }else{
//                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
//            }
//        }
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [AppUtils dismissHUDInView:self.view];
//        DSLog(@"%@",error);
//    }];
//
//
//}
//更改订单状态
//-(void)requestHttpsSetOrderStatusForOrderNum:(NSString *)orderNum orderAmout:(NSString *)orderAmout{
//    time_t now;
//    time(&now);
//    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
//    NSString *nonce_str	= [MMGSign randomNumber];
//    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
//    NSString *uSr=[AppUtils getValueWithKey:User_Name];
//    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//    [dict setObject:@"1" forKey:@"is_phone"];
//    [dict setObject:@"paylog" forKey:@"act"];
//    [dict setObject:orderNumber forKey:@"order_sn"];
//    [dict setObject:payPrice forKey:@"order_amount"];
//    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
//    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
//    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
//    [dict setObject:time_stamp forKey:@"stamp"];
//    [dict setObject:nonce_str forKey:@"noncestr"];
//        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//        NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//        [dict setObject:sign forKey:@"sign"];
//    NSLog(@"%@",dict);
//    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"iso_pay.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
//        
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
//        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
//        if ([codeStr isEqualToString:@"100"]) {
//            //创建队列
//            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            dispatch_group_t queueGroup = dispatch_group_create();
//            //1、重新获取signkey
//            dispatch_group_async(queueGroup, aQueue, ^{
//                [AppUtils requestHttpsSignKey];
//            });
//            //2、重新请求
//            dispatch_group_async(queueGroup, aQueue, ^{
//                [self requestHttpsSetOrderStatusForOrderNum:orderNumber orderAmout:payPrice];
//            });
//        }else{
//            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
//                [AppUtils showSuccessMessage:@"付款成功" inView:self.view];
//                // 延迟加载
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    PayFinishController *payFinish=[[PayFinishController alloc]init];
//                    payFinish.payPrice=payPrice;
//                    payFinish.orderNum=orderNumber;
//                    payFinish.goodName=orderShopDescription;
//                    [self.navigationController pushViewController:payFinish animated:YES];
//                });
//            }
//        }
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"%@",error);
//    }];
//}

#pragma mark - 点击付款按钮
//-(void)clickOrderPayBtn:(FilmWeiXinPayModel *)model{
//    PayFinishController *payFinish=[[PayFinishController alloc]init];
//    payFinish.hidesBottomBarWhenPushed=YES;
//    if(payType==0){
//        [AppUtils showSuccessMessage:@"请选择支付方式" inView:self.view];
//    }else{
//        if (![orderNumber isEqualToString:@""]||
//            ![payPrice isEqualToString:@""]) {
//            switch (payType) {
//                case 1:
//                    [self sendAliPay];
//                    break;
//                case 2:
//                    [self weixinPayWithModel:model];
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//}


-(void)clickOrderPayBtn{
    PayFinishController *payFinish=[[PayFinishController alloc]init];
    payFinish.hidesBottomBarWhenPushed=YES;
    if(payType==0){
        [AppUtils showSuccessMessage:@"请选择支付方式" inView:self.view];
    }else{
        if (![orderNumber isEqualToString:@""]||
            ![payPrice isEqualToString:@""]) {
            switch (payType) {
                case 1:
                    [self sendAliPay];
                    break;
                case 2:
                    [self sendWXPays];
                    break;
                default:
                    break;
            }
        }
    }
}

#pragma mark - 支付宝支付
-(void)sendAliPay{
    //    MMGAliPay *aLiPay=[[MMGAliPay alloc]init];
    //    aLiPay.payFinishDelegate=self;
    //    aLiPay.shopName=orderShopDescription;
    //    aLiPay.shopDescription=orderShopDescription;
    //    aLiPay.money=payPrice;
    //    aLiPay.orderNum=orderNumber;
    //    [aLiPay sendAliPay];
    
    [self aliyPayRequest];
}


-(void)alliPayWithId:(NSString *)strId{
    NSString *appScheme = @"youxiaalipay";
    //支付异步请求并回调
    [[AlipaySDK defaultService] payOrder:strId fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
//        [self payResultRequest];
    }];
}



//查询订单支付状态
-(void)payResultRequest{
    [AppUtils showProgressInView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"check_order_is_pay" forKey:@"step"];
    [dict setObject:_orderId forKey:@"trade_no"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:@"1" forKey:@"is_nosign"];
        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
        NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
        [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSLog(@"查询订单支付状态=%@",responseObject);
        
        //error未支付 success已支付
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"msg"]];
            if ([str isEqualToString:@"ok"]) {
                PayFinishController *pays=[[PayFinishController alloc]init];
                pays.goodName=_orderId;
                pays.payPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payMoney"]];
                id arrObj=responseObject[@"retData"][@"islandTitle"];
                if ([arrObj isKindOfClass:[NSArray class]]) {
                    NSDictionary *arrDic=arrObj[0];
                    pays.orderNum=[NSString stringWithFormat:@"%@",arrDic[@"goods_name"]];
                }
                [self.navigationController pushViewController:pays animated:YES];
            }
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - 微信支付
-(void)weixinPayWithModel:(FilmWeiXinPayModel *)model{
    [WXApi registerApp:model.appid];
    if([WXApi isWXAppInstalled]){
        
        PayReq* req    = [[PayReq alloc] init];
        req.openID     = model.appid;
        req.partnerId  = model.partnerid;           //商户号
        req.prepayId   = model.prepayid;            //预支付交易会话ID
        req.package    = model.package;             //扩展字段
        req.nonceStr   = model.noncestr;            //随机字符串
        req.timeStamp  = model.timestamp.intValue;  //时间戳（防止重发）
        req.sign       = model.sign;                //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)sendWXPays{
    if([WXApi isWXAppInstalled]){
        [self weixinPayRequest];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)dataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderPayCell *cell=[tableView dequeueReusableCellWithIdentifier:@"orderpaycell"];
        if (!cell) {
            cell=[[OrderPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderpaycell"];
        }
        for (NSDictionary *dic in payMsgDicArr) {
            [cell reflushDataForDictionary:dic];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell.frame.size.height;
    }else{
        return 44;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 34;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 34)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    
    //订单编号
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width/2+20, headerSectionView.frame.size.height)];
    label.font=[UIFont systemFontOfSize:13];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    [headerSectionView addSubview:label];
    if (section==0) {
        label.text=@"支付信息";
    }else{
        label.text=@"支付方式";
    }
    return headerSectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderPayCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"orderpaycell"];
        if (!cell) {
            cell=[[OrderPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderpaycell"];
        }
        for (NSDictionary *dic in payMsgDicArr) {
            [cell reflushDataForDictionary:dic];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        return  cell;
    }else{
        MMGPayTypeCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"MMGPayTypeCell"];
        if (!cell) {
            cell=[[MMGPayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGPayTypeCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell._payTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImgv"]]];
        cell._payTypeName.text=dataArr[indexPath.section][indexPath.row][@"payTypeName"];
        
        //设置选中的支付方式
        if ([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:self.selectedPayId]) {
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_sele"]]];
            selectedIndexPath=indexPath;
            if([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:@"2"]){
                CGRect newFrame=tableFrame;
                newFrame.size.height=SCREENSIZE.height-118;
                myTable.frame=newFrame;
                myTable.tableFooterView.hidden=NO;
                buyView.hidden=YES;
            }else if ([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:@"5"]){
                payType=1;
            }else{
                payType=2;
            }
        }else{
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_nor"]]];
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        if (selectedIndexPath) {
            MMGPayTypeCell *cell=(MMGPayTypeCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_nor"]]];
        }
        MMGPayTypeCell *cell=(MMGPayTypeCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_sele"]]];
        selectedIndexPath = indexPath;
        
        CGRect newFrame=tableFrame;
        newFrame.size.height=SCREENSIZE.height-118;
        switch (indexPath.row) {
            case 0:
                payType=1;
                myTable.tableFooterView.hidden=YES;
                buyView.hidden=NO;
                break;
            case 1:
                payType=2;
                myTable.tableFooterView.hidden=YES;
                buyView.hidden=NO;
                break;
            case 2:
                myTable.frame=newFrame;
                myTable.tableFooterView.hidden=NO;
                buyView.hidden=YES;
                break;
            default:
                break;
        }
    }
}

#pragma mark - orderpayCell delegate
-(void)tapOrderNumLabel:(NSString *)orderNum{
    MMGOrderDetailController *orderDetail=[[MMGOrderDetailController alloc]init];
    orderDetail.orderId=orderNum;
    [self.navigationController pushViewController:orderDetail animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - aliyPayRequest
-(void)aliyPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"pay_alipay_iso" forKey:@"step"];
    [dict setObject:_orderId forKey:@"trade_no"];
    
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
    //        NSDictionary *dict=@{
    //                             @"is_iso":@"1",
    //                             @"step":@"pay_alipay_iso",
    //                             @"trade_no":_orderId,
    //                             @"coupon_id":seleCouponModel.cid,
    //                             @"movie_amount":[NSString stringWithFormat:@"%.2f",payPrice],
    //                             @"credit_price":[NSString stringWithFormat:@"%.2f",totalPrice-payPrice],
    //                             @"phone":sourceModel.user_phone,
    //                             @"pay_type":subPayType,
    //                             @"user_id":[AppUtils getValueWithKey:User_ID]
    //                             };
    [[NetWorkRequest defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"支付宝 ： %@---%@",responseObject,responseObject[@"retData"][@"msg"]);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"][@"msg"];
                if ([resObj isKindOfClass:[NSString class]]) {
                    [self alliPayWithId:resObj];
                }
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - pay finish delegate
-(void)pushOrderDetailViewWithController{
    NSLog(@"1");
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
