//
//  MMGLoginController.m
//  CatShopping
//
//  Created by mac on 15/9/15.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "MMGLoginController.h"
#import "RegistController.h"
#import "ForPassController.h"
#import "MMGSign.h"
#import "HttpRequests.h"
#import "Macro2.h"
#define YELLOWCOLOR @"#ec6b00"
@interface MMGLoginController ()<UITextFieldDelegate>

@end

@implementation MMGLoginController{
    UITextField *userTextFiled;
    UITextField *passTextFiled;
    UIButton *submitBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"用户登录"];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    //bg
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.image=[UIImage imageNamed:@"login_baground"];
    bagView.userInteractionEnabled=YES;
    [self.view addSubview:bagView];
    
    //logo
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 220, 150)];
    logo.center=CGPointMake(SCREENSIZE.width/2, 150/2);
    logo.contentMode=UIViewContentModeScaleAspectFit;
    logo.image=[UIImage imageNamed:@"login_logo"];
    [bagView addSubview:logo];
    
    //userView
    UIView *userView=[[UIView alloc]initWithFrame:CGRectMake(12, logo.frame.size.height+10, SCREENSIZE.width-24, 44)];
    userView.backgroundColor=[UIColor whiteColor];
    userView.layer.cornerRadius=2;
    [bagView addSubview:userView];
    
    //
    UILabel *userLab=[[UILabel alloc]init];
    userLab.text=@"用户名";
    userLab.font=[UIFont systemFontOfSize:16];
    userLab.textColor=[UIColor colorWithHexString:@"#282828"];
    [userView addSubview:userLab];
    userLab.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* userLab_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[userLab(50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userLab)];
    [NSLayoutConstraint activateConstraints:userLab_h];
    NSArray* userLab_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[userLab]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userLab)];
    [NSLayoutConstraint activateConstraints:userLab_w];
    
    //
    userTextFiled=[UITextField new];
    userTextFiled.delegate=self;
    userTextFiled.returnKeyType=UIReturnKeyNext;
    userTextFiled.clearButtonMode=UITextFieldViewModeWhileEditing;
    userTextFiled.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    userTextFiled.font=[UIFont systemFontOfSize:16];
    userTextFiled.placeholder=@"请输入用户名";
    userTextFiled.textColor=[UIColor colorWithHexString:@"#282828"];
    [userView addSubview:userTextFiled];
    userTextFiled.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* userTextFiled_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[userLab]-8-[userTextFiled]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userLab,userTextFiled)];
    [NSLayoutConstraint activateConstraints:userTextFiled_h];
    NSArray* userTextFiled_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[userTextFiled]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(userTextFiled)];
    [NSLayoutConstraint activateConstraints:userTextFiled_w];
    
    //passView
    UIView *passView=[[UIView alloc]initWithFrame:CGRectMake(12, userView.frame.origin.y+userView.frame.size.height+20, SCREENSIZE.width-24, 44)];
    passView.backgroundColor=[UIColor whiteColor];
    passView.layer.cornerRadius=2;
    [bagView addSubview:passView];
    
    UILabel *passLab=[[UILabel alloc]init];
    passLab.text=@"密   码";
    passLab.font=[UIFont systemFontOfSize:16];
    passLab.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passLab];
    passLab.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLab_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLab(50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLab)];
    [NSLayoutConstraint activateConstraints:passLab_h];
    NSArray* passLab_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLab]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLab)];
    [NSLayoutConstraint activateConstraints:passLab_w];
    
    //
    passTextFiled=[UITextField new];
    passTextFiled.delegate=self;
    passTextFiled.returnKeyType=UIReturnKeyJoin;
    passTextFiled.clearButtonMode=UITextFieldViewModeWhileEditing;
    passTextFiled.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    passTextFiled.font=[UIFont systemFontOfSize:16];
    passTextFiled.placeholder=@"请输入密码";
    passTextFiled.secureTextEntry = YES;
    passTextFiled.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passTextFiled];
    passTextFiled.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passTextFiled_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLab]-8-[passTextFiled]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLab,passTextFiled)];
    [NSLayoutConstraint activateConstraints:passTextFiled_h];
    NSArray* passTextFiled_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passTextFiled]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passTextFiled)];
    [NSLayoutConstraint activateConstraints:passTextFiled_w];
    
    //button
    submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [submitBtn addTarget:self action:@selector(clickLoginBtn) forControlEvents:UIControlEventTouchUpInside];
    [submitBtn setTitle:@"登录" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bagView addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* submitBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[submitBtn]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_h];
    
    NSArray* submitBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[passView]-30-[submitBtn(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView,submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_w];
    submitBtn.layer.cornerRadius=2;
    submitBtn.backgroundColor=[UIColor colorWithHexString:YELLOWCOLOR];
    submitBtn.enabled=YES;
    
    //
    if (![[AppUtils getValueWithKey:User_Account] isEqualToString:@""]) {
        userTextFiled.text=[AppUtils getValueWithKey:User_Account];
    }
    
    //注册
    UILabel *registeLab=[[UILabel alloc]init];
    registeLab.text=@"注  册";
    registeLab.font=[UIFont systemFontOfSize:16];
    registeLab.textColor=[UIColor colorWithHexString:YELLOWCOLOR];
    [bagView addSubview:registeLab];
    registeLab.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* registeLab_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[registeLab(50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(registeLab)];
    [NSLayoutConstraint activateConstraints:registeLab_h];
    NSArray* registeLab_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[submitBtn]-16-[registeLab(21)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn,registeLab)];
    [NSLayoutConstraint activateConstraints:registeLab_w];
    UITapGestureRecognizer *tapRegist=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goRegistController)];
    registeLab.userInteractionEnabled=YES;
    [registeLab addGestureRecognizer:tapRegist];
    
    //忘记密码
    UILabel *fogetPassLab=[[UILabel alloc]init];
    fogetPassLab.text=@"忘记密码";
    fogetPassLab.textAlignment=NSTextAlignmentRight;
    fogetPassLab.font=[UIFont systemFontOfSize:16];
    fogetPassLab.textColor=[UIColor colorWithHexString:YELLOWCOLOR];
    [bagView addSubview:fogetPassLab];
    fogetPassLab.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* fogetPassLab_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[fogetPassLab(150)]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fogetPassLab)];
    [NSLayoutConstraint activateConstraints:fogetPassLab_h];
    NSArray* fogetPassLab_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[submitBtn]-16-[fogetPassLab(21)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn,fogetPassLab)];
    [NSLayoutConstraint activateConstraints:fogetPassLab_w];
    fogetPassLab.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapForgetPass=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goForgetPassController)];
    [fogetPassLab addGestureRecognizer:tapForgetPass];
}

-(void)turnBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - uitextfield delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==userTextFiled) {
        [passTextFiled becomeFirstResponder];
    }else{
        [self requestHttpsForUserName:userTextFiled.text passWord:passTextFiled.text];
    }
    return NO;
}

#pragma mark - 前往注册、忘记密码页面
-(void)goRegistController{
    RegistController *regist=[[RegistController alloc]init];
    regist.LoginUserNameBlock=^(NSString *tfText){
        [self resetLabel:tfText];
    };
    [self.navigationController pushViewController:regist animated:YES];
}

-(void)goForgetPassController{
    ForPassController *forgetPass=[[ForPassController alloc]init];
    [self.navigationController pushViewController:forgetPass animated:YES];
}

#pragma mark - NextViewControllerBlock method
- (void)resetLabel:(NSString *)textStr{
    userTextFiled.text = textStr;
}

#pragma mark - button event
- (void)clickLoginBtn{
    if([userTextFiled.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"请输入用户名！" inView:self.view];
    }else if ([passTextFiled.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"请输入密码！" inView:self.view];
    }else if ([AppUtils isStringIncludeSpace:passTextFiled.text]){
        [AppUtils showSuccessMessage:@"输入密码不能带有空格" inView:self.view];
    }else{
        [self requestHttpsForUserName:userTextFiled.text passWord:passTextFiled.text];
    }
}

#pragma mark Request 请求
//user.php?act=act_login&is_phone=1（请求参数：Username，password，back_act（登录后返回页面），captcha（验证码））
-(void)requestHttpsForUserName:(NSString *)userName passWord:(NSString *)pwd{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"act_login" forKey:@"act"];
    [dict setObject:userName forKey:@"username"];
    [dict setObject:pwd forKey:@"password"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    //[dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForUserName:userTextFiled.text passWord:passTextFiled.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"login_failure"] inView:self.view];
            }else{
                [self updateUIAndLoginStatusForDictionary:responseObject[@"retData"]];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 保存账号、保存密码。标识程序登录状态
//=====================================
//登录成功保存账号、保存密码。标识程序登录状态。
//保存用户的信息：id、头像、用户名、可用余额
//发送通知改变tabbar index
//=====================================
-(void)updateUIAndLoginStatusForDictionary:(NSDictionary *)dic{
    self.app.isLogin=YES;
    [AppUtils saveValue:userTextFiled.text forKey:User_Account];
    [AppUtils saveValue:passTextFiled.text forKey:User_Pass];
    [AppUtils saveValue:dic[@"user_id"] forKey:User_ID];
    [AppUtils saveValue:dic[@"user_pic"] forKey:User_Photo];
    [AppUtils saveValue:dic[@"user_name"] forKey:User_Name];
    [AppUtils saveValue:dic[@"user_email"] forKey:User_Email];
    [AppUtils saveValue:dic[@"user_phone"] forKey:User_Phone];
    [AppUtils saveValue:dic[@"user_money"] forKey:User_Money];
    //店铺的
    [AppUtils saveValue:dic[@"business_name"] forKey:Business_Name];
    [AppUtils saveValue:dic[@"business_info"][@"amount"] forKey:Business_Amoubt];
    [AppUtils saveValue:dic[@"business_info"][@"goods_count"] forKey:Business_GoodsCount];
    
    [AppUtils saveValue:dic[@"user_money"] forKey:User_Money];
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
}

@end
