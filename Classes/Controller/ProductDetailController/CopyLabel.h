//
//  CopyLabel.h
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CopyLabel : UILabel

@property (nonatomic, copy) NSString *showString;

@end
