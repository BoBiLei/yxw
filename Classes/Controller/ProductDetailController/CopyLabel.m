//
//  CopyLabel.m
//  MMG
//
//  Created by mac on 16/7/12.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "CopyLabel.h"

@implementation CopyLabel

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longShowMenu:)];
        UITapGestureRecognizer *tap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapShowMenu:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:longPressGestureRecognizer];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)longShowMenu:(UILongPressGestureRecognizer *)recoginzer{
    if (recoginzer.state == UIGestureRecognizerStateBegan) {
        [self becomeFirstResponder];  //成为第一响应
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setTargetRect:CGRectMake([recoginzer locationInView:self].x, [recoginzer locationInView:self].y, 0, 0) inView:self]; //设置menu显示位置
        [menu setMenuVisible:YES animated:YES];  //显示menu
    }
}

- (void)tapShowMenu:(UILongPressGestureRecognizer *)recoginzer{
    if (recoginzer.state == UIGestureRecognizerStateEnded) {
        [self becomeFirstResponder];  //成为第一响应
        UIMenuController *menu = [UIMenuController sharedMenuController];
        // 设置菜单内容，显示中文，所以要手动设置app支持中文
        menu.menuItems = @[
                           [[UIMenuItem alloc] initWithTitle:_showString action:@selector(myCopy:)]
                           ];
        [menu setTargetRect:CGRectMake([recoginzer locationInView:self].x, [recoginzer locationInView:self].y, 0, 0) inView:self]; //设置menu显示位置
        [menu setMenuVisible:YES animated:YES];  //显示menu
    }
}

/*!
 *  允许成为第一响应
 */
- (BOOL)canBecomeFirstResponder{
    return YES;
}

/*!
 *  用于控制哪些命令显示在编辑菜单中
 */
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if (action == @selector(myCopy:)) {
        return YES;
    }
    return NO;
}

- (void)myCopy:(id)sender{
    NSString *textStr=self.text;
    NSRange range01=[textStr rangeOfString:@"："];
    NSRange range02=[textStr rangeOfString:@":"];
    if (range01.location!=NSNotFound) {
        textStr=[textStr substringFromIndex:range01.location+range01.length];
    }
    if (range02.location!=NSNotFound) {
        textStr=[textStr substringFromIndex:range02.location+range02.length];
    }
    [UIPasteboard generalPasteboard].string = textStr;
}

@end
