//
//  ProductDetailController.h
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface ProductDetailController : UIViewController

@property(nonatomic,copy) NSString *goodId;
@property(nonatomic,copy) NSString *goodTitle;
@property(nonatomic,copy) NSString *goodImgStr;
@property(nonatomic,copy) NSString *goodPrice;

@end
