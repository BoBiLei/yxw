//
//  SeleGoDateView.h
//  youxia
//
//  Created by mac on 16/1/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeleGoDateView : UIView

-(id)initWithPickerViewInView:(UIView *)view whitYear:(NSString *)year month:(NSString *)month day:(NSString *)day;

-(void)showPickerViewCompletion:(void (^)(id))completion;

@end
