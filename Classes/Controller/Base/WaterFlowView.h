//
//  WaterFlowView.h
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    WaterFlowViewMarginTypeTop,
    WaterFlowViewMarginTypeLeft,
    WaterFlowViewMarginTypeBottom,
    WaterFlowViewMarginTypeRight,
    WaterFlowViewMarginTypeRow,
    WaterFlowViewMarginTypeColumn
    
}WaterFlowViewMarginType;

@class WaterFlowView, WaterFlowViewCell;

@protocol WaterFlowViewDataSource <NSObject>

@required
//返回cell的数目
- (NSInteger)numberOfCellsInWaterFlowView:(WaterFlowView *)waterFlowView;
//返回index位置对应的cell
- (WaterFlowViewCell *)waterFlowView:(WaterFlowView *)waterFlowView cellAtIndex:(NSUInteger)index;

@optional
//一共多少列
- (NSInteger)numberOfColumnsInWaterFlowView:(WaterFlowView *)waterFlowView;
@end


@protocol WaterFlowViewDelegate <UIScrollViewDelegate>

@optional
//返回index的cell的高度
- (CGFloat)waterFlowView:(WaterFlowView *)waterFlowView heightAtIndex:(NSUInteger)index;

//各种间距
- (CGFloat)waterFlowView:(WaterFlowView *)waterFlowView marginForType:(WaterFlowViewMarginType)type;

//返回被选中的（孩子~~）cell
- (WaterFlowViewCell *)waterFlowView:(WaterFlowView *)waterFlowView didSelectedAtIndex:(NSUInteger)index;

@end

@interface WaterFlowView : UIScrollView

@property (nonatomic, weak) id<WaterFlowViewDataSource> dataSource;

@property (nonatomic, weak) id<WaterFlowViewDelegate> delegate;

//刷新数据
- (void)reloadData;

//根据identifier去缓存池找到对应的cell
- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier;

//返回宽度，方便计算图片宽高比
- (CGFloat)cellWidth;
@end
