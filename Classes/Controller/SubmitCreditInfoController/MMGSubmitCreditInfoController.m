//
//  MMGSubmitCreditInfoController.m
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "MMGSubmitCreditInfoController.h"
#import "PersonInfoCell.h"
#import "WorkCell.h"
#import "UploadocumentCell.h"
#import <AVFoundation/AVFoundation.h>
#import "CityFmdb.h"
#import "FmdbCityModel.h"
#import "SelectSubCityView.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "MMGSecondCell.h"
#import "SubPhoneCell.h"
#import <UIButton+WebCache.h>
#import "UploadXybgCell.h"
#import "XybgListCell.h"

#import "IDMPhotoBrowser.h"
#import "IDMZoomingScrollView.h"
#import "pop/POP.h"
#import "Macro2.h"
#define YxColor_Blue @"#e56b02"
@interface MMGSubmitCreditInfoController ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ValidateInfoDelegate,ValidateXYKDelegate,SelectedCompanyAddressDelegate,SelectedHomeAddressDelegate,SaveContactDelegate,SaveContactInfoDelegate,SelectedPhoto01Delegate,SelectedPhoto02Delegate,ShowSelectContactDelegate,ABPeoplePickerNavigationControllerDelegate,DeleteImageDelegate,UploadXybgDelegate,XybgManagerDelegate>

//view
@property (strong, nonatomic) UIPickerView *myPicker;
@property (strong, nonatomic) UIView *maskView;
@property (strong, nonatomic) UIView *pickerBgView;

@end

@implementation MMGSubmitCreditInfoController{
    
    /*
     code_type=none  联通不需要短信或者图片验证码
     code_type=smsCode  短信验证码
     code_type=picCode  图片验证码
     */
    NSString *code_type;
    
    BOOL isUploadXybg;                      //是否是上传信用报告
    
    IdCartButton *personImgBtn01;           //第一张身份证img
    IdCartButton *personImgBtn02;           //第二张身份证img
    NSString *seleIdCartFlat;               //标志当前选择第一还是第二张身份证
    NSMutableDictionary *frontIdCartData;   //第一张身份证data
    NSMutableDictionary *backIdCartData;    //第二张身份证data
    UIButton *frontdeleBtn;                 //正面删除按钮
    UIButton *backdeleBtn;                  //反面删除按钮
    BOOL isHadDeleteBtn;                    //是否有删除身份证按钮
    BOOL isFrontImgHadata;                  //是否选择了正面图片
    BOOL isBackImgHadata;                   //是否选择了反面图片
    NSString *frontIdCartID;                //正面身份证ID
    NSString *backIdCartID;                 //反面身份证ID
    BOOL isFrontValidatSuccess;             //正面是否验证成功
    BOOL isBackValidatSuccess;              //反面是否验证成功
    NSString *receiveFrontStatus;           //正面验证状态
    NSString *receiveBackStatus;            //反面验证状态
    
    NSArray *provinceArray;
    NSArray *testCityArr001;
    NSArray *testDistrictArr001;
    
    NSString *volatPhone;
    
    NSString *provinceId;
    NSString *cityId;
    NSString *districtId;
    
    NSArray *selectedArray;
    
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    UIButton *submitBtn;
    
    SelectSubCityView *companyCityPick;
    SelectSubCityView *homeCityPick;
    
    //
    NSArray *comAdrArr;
    NSArray *homeAdrArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subSuccess) name:SubmitXYRZSuccess object:nil];
    
    [self setCustomNavigationTitle:@"填写认证信息"];
    
    [self getPickerData];
    
    if ([_submitBtnStatus isEqualToString:@"3"]) {
        [AppUtils saveValue:@"1" forKey:VIP_Flag];
    }
    
    if (_frontIdCartModel.idCartArray.count<=0) {
        receiveFrontStatus=@"0";
    }else{
        if (![_submitBtnStatus isEqualToString:@"3"]) {
            receiveFrontStatus=_frontIdCartModel.idCartStatus;
        }else{
            receiveFrontStatus=@"99";
        }
        
    }
    
    if (_backIdCartModel.idCartArray.count<=0) {
        receiveBackStatus=@"0";
    }else{
        if (![_submitBtnStatus isEqualToString:@"3"]) {
            receiveBackStatus=_backIdCartModel.idCartStatus;
        }else{
            receiveBackStatus=@"99";
        }
        
    }
    
    if (_frontIdCartModel.idCartStatus.intValue<4||
        _backIdCartModel.idCartStatus.intValue<4) {
        isHadDeleteBtn=NO;
    }else{
        isHadDeleteBtn=YES;
    }
    
    //正反面是否都验证成功
    if (_frontIdCartModel.idCartArray.count!=0&&
        _backIdCartModel.idCartArray.count!=0) {
        _isOk05=YES;
    }else{
        _isOk05=NO;
    }
    
    companyCityPick=[[SelectSubCityView alloc]initPickerViewWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    homeCityPick=[[SelectSubCityView alloc]initPickerViewWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    
    [self submitContact:[self test]];
    
    [self setUpUI];
}

#pragma mark - 创建联系人姓名、电话号码
-(NSMutableArray *)test{
    //这个变量用于记录授权是否成功，即用户是否允许我们访问通讯录
    int __block tip=0;
    //声明一个通讯簿的引用
    ABAddressBookRef addBook =nil;
    //创建通讯簿的引用
    addBook=ABAddressBookCreateWithOptions(NULL,NULL);
    
    dispatch_semaphore_t sema=dispatch_semaphore_create(0);
    
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool greanted,CFErrorRef error)        {
        
        if (!greanted) {
            tip=1;
        }
        
        dispatch_semaphore_signal(sema);
    });
    
    dispatch_semaphore_wait(sema,DISPATCH_TIME_FOREVER);
    
    NSMutableArray *contactArr=[NSMutableArray array];
    
    if (!tip) {
        //
        //获取所有联系人的数组
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addBook);
        
        //进行遍历
        for (NSInteger i=0; i<CFArrayGetCount(allPeople); i++) {
            
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //读取firstname
            NSString *firstNameStr=@"";
            NSString *firstName = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            if(firstName != nil){
                firstNameStr=firstName;
            }
            //读取lastname
            NSString *lastNameStr=@"";
            NSString *lastname = (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            if(lastname != nil){
                lastNameStr=lastname;
            }
            
            NSString *nameString;
            if (lastname==nil&&firstName == nil) {
                nameString=@"未填写";
            }else{
                nameString=[NSString stringWithFormat:@"%@%@",lastNameStr,firstNameStr];
            }
            
            NSString *phoneString=@"";
            
            //读取电话多值
            ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for (int k = 0; k<ABMultiValueGetCount(phone); k++){
                //获取該Label下的电话值
                NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phone, k);
                //去掉-
                NSMutableString *phoneStr=[NSMutableString stringWithString:personPhone];
                NSRange nameRang=[phoneStr rangeOfString:@"-"];
                while (nameRang.location!=NSNotFound) {
                    [phoneStr replaceCharactersInRange:nameRang withString:@""];
                    nameRang=[phoneStr rangeOfString:@"-"];
                }
                phoneString=[[NSString stringWithFormat:@"%@|",phoneString] stringByAppendingString:phoneStr];
            }
            [contactArr addObject:[NSString stringWithFormat:@"%@%@",nameString,phoneString]];
        }
    }
    return contactArr;
}


#pragma mark -
-(void)subSuccess{
    submitBtn.enabled=NO;
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"#00A651"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"审核中" forState:UIControlStateNormal];
    _submitBtnStatus=@"2";
}

#pragma mark - get Addressdata
- (void)getPickerData {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    
    //得到一个省份的全部省份
    provinceArray=[db getProvince];
    
    //设置默认省份的市
    FmdbCityModel *model001=provinceArray[0];
    testCityArr001=[db getCityWithId:model001.parent_id];
    
    //设置默认市的区
    FmdbCityModel *model002=testCityArr001[0];
    testDistrictArr001=[db getCityWithId:model002.parent_id];
}

-(void)setUpUI{
    NSLog(@"%@",_txdzArr);
    
    
    comAdrArr=_txdzArr[1];
    homeAdrArr=_txdzArr[5];
    
    //
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"PersonInfoCell" bundle:nil] forCellReuseIdentifier:@"PersonInfoCell"];
    [myTable registerNib:[UINib nibWithNibName:@"SubPhoneCell" bundle:nil] forCellReuseIdentifier:@"subphonecell"];
    [myTable registerNib:[UINib nibWithNibName:@"WorkCell" bundle:nil] forCellReuseIdentifier:@"workcell"];
    [myTable registerNib:[UINib nibWithNibName:@"UploadocumentCell" bundle:nil] forCellReuseIdentifier:@"uploadocumentcell"];
    [myTable registerNib:[UINib nibWithNibName:@"UploadXybgCell" bundle:nil] forCellReuseIdentifier:@"UploadXybgCell"];
    [myTable registerNib:[UINib nibWithNibName:@"XybgListCell" bundle:nil] forCellReuseIdentifier:@"XybgListCell"];
    [self.view addSubview:myTable];
    
    //
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    footView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    myTable.tableFooterView=footView;
    
    UIView *line01=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 0.6f)];
    line01.backgroundColor=[UIColor lightGrayColor];
    line01.alpha=0.7f;
    [footView addSubview:line01];
    
    UIView *buttonView=[[UIView alloc]initWithFrame:CGRectMake(0, 0.6f, SCREENSIZE.width, 63.1f)];
    buttonView.backgroundColor=[UIColor whiteColor];
    [footView addSubview:buttonView];
    
    UIView *line02=[[UIView alloc]initWithFrame:CGRectMake(0, footView.height-0.6f, SCREENSIZE.width, 0.6f)];
    line02.backgroundColor=[UIColor lightGrayColor];
    line02.alpha=0.7f;
    [footView addSubview:line02];
    
    submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame=CGRectMake(12, 9, footView.width-24, footView.height-18);
    submitBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    NSString *titStr=@"";
    if ([_submitBtnStatus isEqualToString:@"1"]) {
        if ([_status_msg isEqualToString:@"0"]||
            [_status_msg isEqualToString:@""]) {
            titStr=@"提交信息，获取分期购物额度";
        }else{
            titStr=@"重新提交审核信息";
        }
        
        submitBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    }else if ([_submitBtnStatus isEqualToString:@"2"]){
        submitBtn.enabled=NO;
        submitBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        titStr=@"审核中";
    }else{
        submitBtn.enabled=NO;
        submitBtn.backgroundColor=[UIColor colorWithHexString:@"#00A651"];
        titStr=@"已通过验证";
    }
    [submitBtn setTitle:titStr forState:UIControlStateNormal];
    
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    submitBtn.layer.cornerRadius=3;
    [submitBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:submitBtn];
}

#pragma mark - 提交信用验证
-(void)clickSubmitBtn{
    if(!_isOk01){
        [AppUtils showSuccessMessage:@"请先验证第一步" inView:self.view];
    }else if (!_isOk02){
        [AppUtils showSuccessMessage:@"请先验证第二步" inView:self.view];
    }else if (!_isOk03){
        [AppUtils showSuccessMessage:@"请先验证第三步" inView:self.view];
    }else if (!_isOk04){
        [AppUtils showSuccessMessage:@"请先验证第四步" inView:self.view];
    }
    else if (!_isOk05){
        [AppUtils showSuccessMessage:@"请按提示上传身份证" inView:self.view];
    }
    else{
        [self submitValidateRequest];
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_submitBtnStatus isEqualToString:@"3"]?4:6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section!=5) {
        return 1;
    }else{
        return _xybgArr.count+1;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        PersonInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PersonInfoCell"];
        if (!cell) {
            cell=[[PersonInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonInfoCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
//        [cell reflushDataForArray:_cxkArr];
        return cell.height;
    }else if (indexPath.section==1){
//        MMGSecondCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MMGSecondCell"];
//        if (!cell) {
//            cell=[[MMGSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGSecondCell"];
//        }
//        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        MMGSecondCell *cell=[[MMGSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGSecondCell"];
        [cell reflushDataForArray:self.phoneArr];
        return cell.height;
    }else if (indexPath.section==2){
        SubPhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"subphonecell"];
        if (!cell) {
            cell=[[SubPhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"subphonecell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForArray:_contaceArr validateStatus:_submitBtnStatus];
        return cell.height;
    }else if (indexPath.section==3){
        WorkCell *cell=[tableView dequeueReusableCellWithIdentifier:@"workcell"];
        if (!cell) {
            cell=[[WorkCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"workcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForArray:_txdzArr validateStatus:_submitBtnStatus];
        return cell.height;
    }else if (indexPath.section==4){
        if (IPHONE5) {
            return 673.040;
        }else{
            if (IPHONE6) {
                return 756.750;
            }else{
                return 816.108;
            }
        }
    }else{
        if (indexPath.row==0) {
            UploadXybgCell *cell=[tableView dequeueReusableCellWithIdentifier:@"UploadXybgCell"];
            if (!cell) {
                cell=[[UploadXybgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UploadXybgCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.status=_submitBtnStatus;
            cell.delegate=self;
            return  cell.height;
        }else{
            return 44;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        PersonInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PersonInfoCell"];
        if (!cell) {
            cell=[[PersonInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonInfoCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
//        [cell reflushDataForArray:_cxkArr];
        return  cell;
    }
    else if (indexPath.section==1) {
//        MMGSecondCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MMGSecondCell"];
//        if (!cell) {
//            cell=[[MMGSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGSecondCell"];
//        }
        MMGSecondCell *cell=[[MMGSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MMGSecondCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        volatPhone=self.phoneArr[0];
        [cell reflushDataForArray:self.phoneArr];
        return  cell;
    }
    else if (indexPath.section==2) {
        SubPhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"subphonecell"];
        if (!cell) {
            cell=[[SubPhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"subphonecell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        cell.selectContactDelegate=self;
        [cell reflushDataForArray:_contaceArr validateStatus:_submitBtnStatus];
        return  cell;
    }
    else if (indexPath.section==3){
        WorkCell *cell=[tableView dequeueReusableCellWithIdentifier:@"workcell"];
        if (!cell) {
            cell=[[WorkCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"workcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        cell.HomeDelegate=self;
        cell.saveDelegate=self;
        [cell reflushDataForArray:_txdzArr validateStatus:_submitBtnStatus];
        return  cell;
    }
    else  if (indexPath.section==4){
        UploadocumentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"uploadocumentcell"];
        if (!cell) {
            cell=[[UploadocumentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uploadocumentcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate01=self;
        cell.delegate02=self;
        cell.deleteDelegate=self;
        
        NSURL *url01;
        NSURL *url02;
        NSString *btnTip01;
        NSString *btnTip02;
        NSString *waitStr=@"待审核";
        NSString *succcssStr=@"审核中";
        BOOL btnTipHidden01;
        BOOL btnTipHidden02;
        
        //_submitBtnStatus=2 审核中状态
        if ([_submitBtnStatus isEqualToString:@"2"]) {
            
            isHadDeleteBtn=NO;
            cell.deleteBtn01.hidden=YES;
            cell.deleteBtn02.hidden=YES;
            cell.selectedBtn02.userInteractionEnabled=YES;
            
            //如果不为空显示正面图片
            if (_frontIdCartModel.idCartArray.count!=0) {
                frontIdCartID=_frontIdCartModel.idCartArray[0][@"id"];
                url01=[NSURL URLWithString:_frontIdCartModel.idCartArray[0][@"path"]];
                
                //正面验证结果tip
                if(receiveFrontStatus.intValue<4){
                    btnTipHidden01=NO;
                    btnTip01=waitStr;
                }else{
                    if ([receiveFrontStatus isEqualToString:@"99"]) {
                        btnTipHidden01=NO;
                        btnTip01=succcssStr;
                    }else{
                        btnTipHidden01=NO;
                        btnTip01=waitStr;
                    }
                }
            }else{
                btnTipHidden01=YES;
            }
            
            //如果不为空显示反面图片
            if (_backIdCartModel.idCartArray.count!=0) {
                backIdCartID=_backIdCartModel.idCartArray[0][@"id"];
                url02=[NSURL URLWithString:_backIdCartModel.idCartArray[0][@"path"]];
                
                //
                //反面验证结果tip
                if(receiveBackStatus.intValue<4){
                    btnTipHidden02=NO;
                    btnTip02=waitStr;
                }else{
                    if ([receiveBackStatus isEqualToString:@"99"]) {
                        btnTipHidden02=NO;
                        btnTip02=succcssStr;
                    }else{
                        btnTipHidden02=NO;
                        btnTip02=waitStr;
                    }
                }
            }else{
                btnTipHidden02=YES;
            }
        }
        //待审核或未提交审核、审核失败
        else{
            //如果不为空显示正面图片
            if (_frontIdCartModel.idCartArray.count!=0) {
                frontIdCartID=_frontIdCartModel.idCartArray[0][@"id"];
                url01=[NSURL URLWithString:_frontIdCartModel.idCartArray[0][@"path"]];
                cell.selectedBtn01.userInteractionEnabled=NO;
                isHadDeleteBtn=YES;
                cell.deleteBtn01.hidden=NO;
                
                //正面验证结果tip
                if ([receiveFrontStatus isEqualToString:@"99"]) {
                    btnTipHidden01=NO;
                    btnTip01=succcssStr;
                    
                    isHadDeleteBtn=NO;
                    cell.deleteBtn01.hidden=YES;
                }else{
                    btnTipHidden01=NO;
                    btnTip01=waitStr;
                }
            }else{
                btnTipHidden01=YES;
            }
            
            //如果不为空显示反面图片
            if (_backIdCartModel.idCartArray.count!=0) {
                backIdCartID=_backIdCartModel.idCartArray[0][@"id"];
                url02=[NSURL URLWithString:_backIdCartModel.idCartArray[0][@"path"]];
                cell.selectedBtn02.userInteractionEnabled=NO;
                isHadDeleteBtn=YES;
                cell.deleteBtn02.hidden=NO;
                
                //
                //反面验证结果tip
                if ([receiveBackStatus isEqualToString:@"99"]) {
                    btnTipHidden02=NO;
                    btnTip02=succcssStr;
                    
                    isHadDeleteBtn=NO;
                    cell.deleteBtn02.hidden=YES;
                }else{
                    btnTipHidden02=NO;
                    btnTip02=waitStr;
                }
            }else{
                btnTipHidden02=YES;
            }
        }
        
        if (url01!=nil) {
            [cell.selectedBtn01 sd_setImageWithURL:url01 forState:UIControlStateNormal];
        }
        
        if (url02!=nil) {
            [cell.selectedBtn02 sd_setImageWithURL:url02 forState:UIControlStateNormal];
        }
        
        cell.selectedBtn01.tipLabel.hidden=btnTipHidden01;
        cell.selectedBtn02.tipLabel.hidden=btnTipHidden02;
        cell.selectedBtn01.tipLabel.text=btnTip01;
        cell.selectedBtn02.tipLabel.text=btnTip02;
        
        //正反面是否都验证成功
        if (_frontIdCartModel.idCartArray.count!=0&&
            _backIdCartModel.idCartArray.count!=0) {
            _isOk05=YES;
        }else{
            _isOk05=NO;
        }
        
        personImgBtn01=cell.selectedBtn01;
        personImgBtn02=cell.selectedBtn02;
        frontdeleBtn=cell.deleteBtn01;
        backdeleBtn=cell.deleteBtn02;
        //如果有删除按钮，则发送通知改变frame
        if (!frontdeleBtn.hidden||!backdeleBtn.hidden) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isHadDeleteBtn" object:nil];
        }
        return  cell;
    }else{
        if (indexPath.row==0) {
            UploadXybgCell *cell=[tableView dequeueReusableCellWithIdentifier:@"UploadXybgCell"];
            if (!cell) {
                cell=[[UploadXybgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UploadXybgCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.status=_submitBtnStatus;
            cell.delegate=self;
            return  cell;
        }else{
            XybgListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"XybgListCell"];
            if (!cell) {
                cell=[[XybgListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"XybgListCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.delegate=self;
            XybgListModel *model=_xybgArr[indexPath.row-1];
            cell.lModel=model;
            [cell reflushModel:model validateStatus:_submitBtnStatus];
            return  cell;
        }
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1||section==4) {
        return 44;
    }else{
        return 60;
    }
}

//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGFloat hedheight=0;
    if (section==1||section==4) {
        hedheight=44;
    }else{
        hedheight=60;
    }
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, hedheight)];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 0.5f)];
    line.backgroundColor=[UIColor lightGrayColor];
    line.alpha=0.6f;
    [headerSectionView addSubview:line];
    
    //title
    UILabel *headTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, 0.5f, SCREENSIZE.width-16, headerSectionView.height-0.5f)];
    headTitle.font=[UIFont boldSystemFontOfSize:16];
    headTitle.numberOfLines=0;
    NSString *titleStr;
    NSMutableAttributedString *strM;
    switch (section) {
        case 0:
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
            headTitle.textColor=[UIColor colorWithHexString:@"e56b02"];
            titleStr=@"第一步：验证您最常用的银行储蓄卡，分期购物每月自动扣款";
            break;
        case 1:
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
            headTitle.textColor=[UIColor colorWithHexString:@"e56b02"];
            titleStr=@"第二步：验证您本人名下的手机号码";
            break;
        case 2:
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
            headTitle.textColor=[UIColor colorWithHexString:@"e56b02"];
            titleStr=@"第三步：您的紧急联系人信息，请选择亲属或好友";
            break;
        case 3:
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
            headTitle.textColor=[UIColor colorWithHexString:@"e56b02"];
            titleStr=@"第四步：您的通讯地址，我们将邮寄商品与分期购物合同给您";
            break;
        case 4:
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
            headTitle.textColor=[UIColor colorWithHexString:@"e56b02"];
            titleStr=@"第五步：验证您本人的身份证信息";
            break;
        default:
        {
            headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#73bd39"];
            headTitle.textColor=[UIColor whiteColor];
            NSTextAttachment *attachMent = [[NSTextAttachment alloc] init];
            
            attachMent.image = [UIImage imageNamed:@"isld_btmtip"];
            CGFloat height = headTitle.font.lineHeight;
            attachMent.bounds = CGRectMake(0, -3, height, height);
            
            NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:attachMent];
            
            strM = [[NSMutableAttributedString alloc] init];
            [strM appendAttributedString: [[NSAttributedString alloc] initWithString:@"如分期额度大于10万元，请提交更多信用证明资料。（可选）"]];
            [strM appendAttributedString:attrString];
        }
            break;
    }
    if (section!=5) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleStr];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:section==4?0:4];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleStr length])];
        headTitle.attributedText = attributedString;
    }else{
        headTitle.attributedText = strM;
    }
    [headerSectionView addSubview:headTitle];
    return headerSectionView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 信用报告 Delegate
-(void)clickUploadButton{
    isUploadXybg=YES;
    [self showAddPictureViewWithButton:nil];
}

-(void)scanBaogaoButton:(XybgListModel *)model{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:@[model.bgImgStr]];
    browser.displayToolbar=NO;
    browser.displayCounterLabel = YES;
    browser.displayActionButton = NO;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
}

-(void)deleteBaogaoButton:(XybgListModel *)model sender:(id)sender{
    
    //得到当前cell的数组下标
    NSInteger seleRow=[myTable indexPathForCell:((XybgListCell*)[[sender superview] superview])].row-1;
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否删除该资料信息" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        [self deleteXybgWithId:model andIndex:seleRow];
    }];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        
    }];
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - ShowAddPictureView
-(void)showAddPictureViewWithButton:(UIButton *)button{
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            default:
                [self pickImage];
                break;
        }
    };
    
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从手机相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:^(MMPopupView *popupView){
        
    }];
}

//拍照
- (void) snapImage{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"请在iPhone的“设置-隐私-相机”选项中，允许 猫猫购 访问你的相机" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //取得原始图片
    UIImage *image= [info objectForKey:@"UIImagePickerControllerEditedImage"];
    NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
    UIImage *newImage=[UIImage imageWithData:imageData];
    
    //判断类型（拍照还是从相册选取）
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        
        UIImageWriteToSavedPhotosAlbum([info objectForKey:@"UIImagePickerControllerOriginalImage"], nil, nil, nil);
    }else{
        
    }
    
    UIImage *theImage = [self imageWithImageSimple:newImage scaledToSize:CGSizeMake(700, 700)];
    
    NSData* upImgData = UIImagePNGRepresentation(theImage);
    NSString *imgByte=[NSString stringWithFormat:@"%lu",(unsigned long)upImgData.length];
    
    if (isUploadXybg) {
        [self UploadXybgWithData:upImgData type:@"png" size:imgByte];
    }else{
        if ([seleIdCartFlat isEqualToString:@"1"]) {
            isFrontImgHadata=YES;
            
            frontIdCartData=[NSMutableDictionary dictionary];
            [frontIdCartData setObject:upImgData forKey:@"zfl"];
            [frontIdCartData setObject:@"png" forKey:@"img_type"];
            [frontIdCartData setObject:imgByte forKey:@"size"];
            
            backIdCartData=[NSMutableDictionary dictionary];
            [backIdCartData setObject:@"" forKey:@"zfl"];
            [backIdCartData setObject:@"" forKey:@"img_type"];
            [backIdCartData setObject:@"" forKey:@"size"];
            
            //上传
            [self UploadPictureWithFrontData:frontIdCartData backData:backIdCartData];
        }else{
            isBackImgHadata=YES;
            
            frontIdCartData=[NSMutableDictionary dictionary];
            [frontIdCartData setObject:@"" forKey:@"zfl"];
            [frontIdCartData setObject:@"" forKey:@"img_type"];
            [frontIdCartData setObject:@"" forKey:@"size"];
            
            backIdCartData=[NSMutableDictionary dictionary];
            [backIdCartData setObject:upImgData forKey:@"zfl"];
            [backIdCartData setObject:@"png" forKey:@"img_type"];
            [backIdCartData setObject:imgByte forKey:@"size"];
            
            //上传
            [self UploadPictureWithFrontData:frontIdCartData backData:backIdCartData];
        }
    }
    
    if (isFrontImgHadata&&isBackImgHadata) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UpdateUploadImgCount object:nil];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 选择城市delegate
-(void)showSelectedCompanyAddressSheet;{
    [AppUtils closeKeyboard];
    [self.view addSubview:companyCityPick];
    [companyCityPick showPickerViewCompletion:^(id selectedObject) {
        comAdrArr=selectedObject;
        [[NSNotificationCenter defaultCenter] postNotificationName:SelectedCompanyAddressNotification object:selectedObject];
    }];
}

-(void)showSelectedHomeAddressSheet{
    [AppUtils closeKeyboard];
    [self.view addSubview:homeCityPick];
    [homeCityPick showPickerViewCompletion:^(id selectedObject) {
        homeAdrArr=selectedObject;
        [[NSNotificationCenter defaultCenter] postNotificationName:SelectedHomeAddressNotification object:selectedObject];
    }];
}

#pragma mark - 选择通讯录
-(void)clickShowSelectView{
    [self selectPeople];
}

#pragma mark - 储蓄卡信息 Delegate
-(void)clickValidateCXKWithName:(NSString *)name personId:(NSString *)personId cxkNumber:(NSString *)cxkNum phone:(NSString *)phone{
    [self requestCXKForName:name personId:personId cartNum:cxkNum phone:phone];
}

#pragma mark - 提交电话信息 Delegate

-(void)submitPhoneAndPass:(NSString *)phone password:(NSString *)passWord{
    if (_isOk01) {
        [self requestPhoneAndPass:phone passWord:passWord];
    }else{
        [AppUtils showSuccessMessage:@"请先验证第一步" inView:self.view];
    }
}

-(void)submitPhoneAndPassAndPicture:(NSString *)phone password:(NSString *)passWord picture:(NSString *)picture{
    if (_isOk01) {
        [self requestPhoneAndPassAndPicture:phone passWord:passWord picture:picture];
    }else{
        [AppUtils showSuccessMessage:@"请先验证第一步" inView:self.view];
    }
}

-(void)submitPhoneAndDuanxin:(NSString *)phone duanxin:(NSString *)duanxin{
    if (_isOk01) {
        [self requestPhoneAndDuanXin:phone duanxin:duanxin];
    }else{
        [AppUtils showSuccessMessage:@"请先验证第一步" inView:self.view];
    }
}

-(void)reGetValideCode:(NSString *)phone{
    [self reGetValideCodeRequest:phone];
}

#pragma mark - 保存紧急联系人 Delegate
-(void)saveContactInfoWithName:(NSString *)name phone:(NSString *)phone{
    [self saveContactWithName:name phone:phone];
}

#pragma mark - 保存通讯地址 Delegate
-(void)saveAddressWithCompany:(NSString *)company companyAddress:(NSString *)companyAddress companyDetailAdr:(NSString *)companyDetailAdr companyReceiver:(NSString *)companyReceiver companyPhone:(NSString *)companyPhone homeAddress:(NSString *)homeAddress homeDetailAdr:(NSString *)homeDetailAdr homeReceiver:(NSString *)homeReceiver homePhone:(NSString *)homePhone{
    
    [self saveTXDZWithCompany:company cmpAddress:companyAddress cmpDetailAdr:companyDetailAdr cmpReceiver:companyReceiver cmpPhone:companyPhone HomeAddress:homeAddress homeDetailAdr:homeDetailAdr homeReceiver:homeReceiver homePhone:homePhone];
}

-(void)selectPeople{
    
    int __block tip=0;
    
    ABAddressBookRef addBook =nil;
    
    addBook=ABAddressBookCreateWithOptions(NULL,NULL);
    
    dispatch_semaphore_t sema=dispatch_semaphore_create(0);
    
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool greanted,CFErrorRef error)        {
        
        if (!greanted) {
            tip=1;
        }
        
        dispatch_semaphore_signal(sema);
    });
    
    dispatch_semaphore_wait(sema,DISPATCH_TIME_FOREVER);
    if (tip) {
        
        NSString *tipStr=[NSString stringWithFormat:@"您没有访问联系人权限，您可以在\n设置-隐私-通讯录-游侠网\n中启用访问权限"];
        UIAlertView * alart = [[UIAlertView alloc]initWithTitle:@"提示"message:tipStr delegate:self cancelButtonTitle:@"知道了"otherButtonTitles:nil,nil];
        [alart show];
    }else{
        ABPeoplePickerNavigationController *peoplePicker= [[ABPeoplePickerNavigationController alloc] init];
        
        [peoplePicker.navigationBar setBarStyle:UIBarStyleBlack];
        
        
        [peoplePicker.view setBackgroundColor:[UIColor whiteColor]];
        
        peoplePicker.peoplePickerDelegate =self;
        
        NSArray *displayItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty],nil];
        
        peoplePicker.displayedProperties=displayItems;
        
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            peoplePicker.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
        }
        [self presentViewController:peoplePicker animated:YES completion:nil];
    }
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    
    //获取联系人电话
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSRange prang=[phoneNO rangeOfString:@"+86 "];
    if (prang.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang.location+prang.length];
    }
    
    NSRange prang02=[phoneNO rangeOfString:@"00 86 "];
    if (prang02.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang02.location+prang02.length];
    }
    
    NSRange prang03=[phoneNO rangeOfString:@"0086"];
    if (prang03.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang03.location+prang03.length];
    }
    
    NSRange prang04=[phoneNO rangeOfString:@"86 "];
    if (prang04.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang04.location+prang04.length];
    }
    
    NSRange prang01=[phoneNO rangeOfString:@"86"];
    if (prang01.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang01.location+prang01.length];
    }
    
    NSRange spaceRang=[phoneNO rangeOfString:@" "];
    if (spaceRang.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:spaceRang.location+spaceRang.length];
    }
    
    //获取联系人姓名
    NSString *ssstr = (__bridge NSString*)ABRecordCopyCompositeName(person);
    
    /*
     if ([AppUtils checkPhoneNumber:phoneNO]) {
     [peoplePicker dismissViewControllerAnimated:YES completion:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:SelectedContactNotification object:@[ssstr,phoneNO]];
     return;
     }else{
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"错误提示" message:@"请选择正确手机号" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
     [alertView show];
     }*/
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:SelectedContactNotification object:@[ssstr,phoneNO]];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0){
    ABPersonViewController *personViewController = [[ABPersonViewController alloc] init];
    personViewController.displayedPerson = person;
    [peoplePicker pushViewController:personViewController animated:YES];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person NS_DEPRECATED_IOS(2_0, 8_0){
    return YES;
}
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_DEPRECATED_IOS(2_0, 8_0){
    
    //获取联系人姓名
    NSString *name = (__bridge NSString*)ABRecordCopyCompositeName(person);
    NSLog(@"%@",name);
    
    //获取联系人电话
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    return YES;
}

#pragma mark - 点击删除身份证图片
-(void)deleteImageWithButton:(NSInteger)tag{
    if (tag==0) {
        [self deletePersonIdCart:@"1" picId:frontIdCartID];
    }else{
        [self deletePersonIdCart:@"2" picId:backIdCartID];
    }
}

#pragma mark 点击选择第一张身份证图片
-(void)showSelectedPhoto01WithButton:(UIButton *)button{
    if (!([_submitBtnStatus isEqualToString:@"2"]||
          [_submitBtnStatus isEqualToString:@"3"])) {
        seleIdCartFlat=@"1";
        [self showAddPictureViewWithButton:button];
    }
}

#pragma mark 点击选择第二张身份证图片
-(void)showSelectedPhoto02WithButton:(UIButton *)button{
    if (!([_submitBtnStatus isEqualToString:@"2"]||
          [_submitBtnStatus isEqualToString:@"3"])) {
        seleIdCartFlat=@"2";
        [self showAddPictureViewWithButton:button];
    }
}

//==========================================================
//==========================================================
//==========================================================
#pragma mark - Request
//储蓄卡验证
-(void)requestCXKForName:(NSString *)name personId:(NSString *)personId cartNum:(NSString *)cartNum phone:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_bank1_iso" forKey:@"code"];
    [dict setObject:name forKey:@"person_name"];
    [dict setObject:personId forKey:@"person_idcard"];
    [dict setObject:cartNum forKey:@"bank_1_number"];
    [dict setObject:phone forKey:@"bank_1_mobile"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@""forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@....",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestCXKForName:name personId:personId cartNum:cartNum phone:phone];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                _isOk01=YES;
                /*******
                 1、
                 2、
                 *******/
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                _cxkArr=@[name,personId,cartNum,phone,@"1"];
                [[NSNotificationCenter defaultCenter] postNotificationName:UpdateXCKNotification object:_cxkArr];
                [myTable reloadData];
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 验证手机号、服务密码 Request

-(void)requestPhoneAndPass:(NSString *)phone passWord:(NSString *)passWord{
    [AppUtils showProgressMessage:@"正在验证中，请耐心等待！" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t2" forKey:@"mod"];
    [dict setObject:@"save_phone_number_ios" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:passWord forKey:@"phone_pdw"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndPass:phone passWord:passWord];
            });
        }else{
            /*
             {
             errCode = 0;
             retData =     {
             "code_type" = smsCode;  none-联通或成功 smsCode-短信 smsCode-图片验证码
             error = 0;
             img = "";
             "is_code" = 1;
             "is_pwd" = 1;
             success = 0;
             };
             retMsg = success;
             }
             */
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //error = 0 服务码验证成功;
                //error = 1 不成功/或进入判断is_dx=1
                NSString *errorCode=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"error"]];
                if ([errorCode isEqualToString:@"0"]) {
                    
                    //success = 1 全部验证成功;
                    //success = 0 需要下一步
                    NSString *succStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"success"]];
                    if ([succStr isEqualToString:@"0"]) {
                        NSString *codeType=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"code_type"]];
                        code_type=codeType;
                        /*
                         codeType = smsCode 需要短信验证
                         codeType = picCode 需要图片验证
                         */
                        
                        //100 需要图片验证码
                        //200 短信验证码
                        if ([codeType isEqualToString:@"smsCode"]) {
                            _phoneArr=@[phone,@"200",passWord,responseObject[@"retData"][@"img"],@"",@""];
                            [myTable reloadData];
                        }else if([codeType isEqualToString:@"picCode"]){
                            _phoneArr=@[phone,@"100",passWord,responseObject[@"retData"][@"img"],@"",@""];
                            [myTable reloadData];
                        }else{
                            /*
                             目前联通的都不需要图片
                             只有移动的才需要
                             */
                        }
                    }else{
                        [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                        _isOk02=YES;
                        _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                        [myTable reloadData];
                    }
                }else{
                    NSString *dxStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"is_dx"]];
                    if ([dxStr isEqualToString:@"1"]) {
                        _isOk02=YES;
                    }
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark 验证手机号、服务密码 图片/短信验证码 Request

-(void)requestPhoneAndPassAndPicture:(NSString *)phone passWord:(NSString *)passWord picture:(NSString *)picture{
    
    //ttp://www.youxia.com/mgo/index.php?mod=me_t&code=save_phone_number_pwd_ios&is_iso=1&phone_number=13008828755&phone_pdw=168200&phone_code=图片验证码& is_code=1
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t2" forKey:@"mod"];
    [dict setObject:code_type forKey:@"code_type"];
    [dict setObject:@"save_phone_number_pwd_ios" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:passWord forKey:@"phone_pdw"];
    [dict setObject:picture forKey:@"phone_code"];
    [dict setObject:@"1" forKey:@"is_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@-%@",responseObject,responseObject[@"retData"][@"msg"]);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndPassAndPicture:phone passWord:passWord picture:picture];
            });
        }else{
            /*
             成功
             errCode = 0;
             retData =     {
             error = 0;
             "is_code" = 0;
             msg = "\U8bf7\U8f93\U5165\U624b\U673a\U77ed\U4fe1\U9a8c\U8bc1\U7801";
             success = 0;
             };
             retMsg = success;
             
             //失败
             errCode = 0;
             retData =     {
             error = 1;
             msg = "\U767b\U5f55\U5931\U8d25\Uff01\U7528\U6237\U540d\Uff0c\U5bc6\U7801\U6216\U9a8c\U8bc1\U7801\U8f93\U5165\U4e0d\U6b63\U786e\Uff01";
             };
             retMsg = error;
             */
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                /*
                 success = 1 全部验证成功;
                 success = 0 需要下一步短信验证
                 */
                NSString *succStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"success"]];
                if ([succStr isEqualToString:@"0"]) {
                    
                    NSString *msgSecond=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"msg2"]];
                    NSRange rang01=[msgSecond rangeOfString:@"请"];
                    if (rang01.location!=NSNotFound) {
                        msgSecond=[msgSecond substringFromIndex:rang01.location+rang01.length];
                    }
                    NSRange rang02=[msgSecond rangeOfString:@"秒"];
                    if (rang02.location!=NSNotFound) {
                        msgSecond=[msgSecond substringToIndex:rang02.location];
                    }
                    _phoneArr=@[phone,@"999",@"",@"",@"",msgSecond];
                    [myTable reloadData];
                }else{
                    NSLog(@"%@",responseObject[@"retData"][@"msg"]);
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                    volatPhone=phone;
                    _isOk02=YES;
                    _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                    [myTable reloadData];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"\n%@\n%@\n%@\n%@\n%@",error,error.localizedDescription,task.response,error.localizedFailureReason,error.userInfo);
    }];
}

#pragma mark 验证手机号、短信

-(void)requestPhoneAndDuanXin:(NSString *)phone duanxin:(NSString *)duanxin{
    //ttp://www.youxia.com/mgo/index.php?mod=me_t&code=save_phone_number_smscode_ios&is_iso=1&phone_number=13008828755&phone_sms_code=168200
    [AppUtils showProgressMessage:@"正在验证中，请耐心等待！" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t2" forKey:@"mod"];
    [dict setObject:code_type forKey:@"code_type"];
    [dict setObject:@"save_phone_number_smscode_ios" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:duanxin forKey:@"phone_sms_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndDuanXin:phone duanxin:duanxin];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"error"]];
                if ([codeStr isEqualToString:@"0"]) {
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                    volatPhone=phone;
                    _isOk02=YES;
                    _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                    [myTable reloadData];
                }else{
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark 重新获取短信验证码 Request

-(void)reGetValideCodeRequest:(NSString *)phone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t2" forKey:@"mod"];
    [dict setObject:@"get_phone_number_smscode_ios" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:@"1" forKey:@"is_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@=====---",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"短信已发送，请查收" inView:self.view];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

#pragma mark - 保存紧急联系人

-(void)saveContactWithName:(NSString *)name phone:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_jj_iso" forKey:@"code"];
    [dict setObject:name forKey:@"person_jj_name"];
    [dict setObject:phone forKey:@"person_jj_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self saveContactWithName:name phone:phone];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                _isOk03=YES;
                /*******
                 1、
                 2、
                 *******/
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                _contaceArr=@[name,phone,@"2"];
                [[NSNotificationCenter defaultCenter] postNotificationName:UpdateContactNotification object:_contaceArr];
                [myTable reloadData];
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 上传通讯地址

-(void)saveTXDZWithCompany:(NSString *)company cmpAddress:(NSString *)cmpAddress cmpDetailAdr:(NSString *)cmpDetailAdr cmpReceiver:(NSString *)cmpReceiver cmpPhone:(NSString *)cmpPhone HomeAddress:(NSString *)homeAddress homeDetailAdr:(NSString *)homeDetailAdr homeReceiver:(NSString *)homeReceiver homePhone:(NSString *)homePhone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_workandhomeinfo_iso" forKey:@"code"];
    [dict setObject:company forKey:@"work_company"];
    [dict setObject:@"" forKey:@"work_phone"];
    [dict setObject:cmpReceiver forKey:@"work_sjname"];
    [dict setObject:cmpDetailAdr forKey:@"work_address"];
    //    NSArray *cpna=_txdzArr[1];
    [dict setObject:comAdrArr[0] forKey:@"SelectProvince3"];
    [dict setObject:comAdrArr[1] forKey:@"SelectCity3"];
    [dict setObject:comAdrArr[2] forKey:@"SelectDistrict3"];
    [dict setObject:homeDetailAdr forKey:@"person_address_1"];
    [dict setObject:cmpPhone forKey:@"home_phone"];
    [dict setObject:@"" forKey:@"home_sjname"];
    //    NSArray *homea=_txdzArr[5];
    [dict setObject:homeAdrArr[0] forKey:@"SelectProvince"];
    [dict setObject:homeAdrArr[1] forKey:@"SelectCity"];
    [dict setObject:homeAdrArr[2] forKey:@"SelectDistrict"];
    
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self saveTXDZWithCompany:company cmpAddress:cmpAddress cmpDetailAdr:cmpDetailAdr cmpReceiver:cmpReceiver cmpPhone:cmpPhone HomeAddress:homeAddress homeDetailAdr:homeDetailAdr homeReceiver:homeReceiver homePhone:homePhone];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                _isOk04=YES;
                /*******
                 1、
                 2、
                 *******/
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                _txdzArr=@[company,comAdrArr,cmpDetailAdr,cmpReceiver,cmpPhone,homeAdrArr,homeDetailAdr,homeReceiver,homePhone,@"2"];
                [[NSNotificationCenter defaultCenter] postNotificationName:UpdateTXDZNotification object:_txdzArr];
                [myTable reloadData];
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 上传身份证 Request

-(void)UploadPictureWithFrontData:(NSDictionary *)frontDic backData:(NSDictionary *)backDic{
    
    [AppUtils showProgressMessage:@"正在上传…" inView:self.view];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_imgs_iso2" forKey:@"code"];
    [dict setObject:@"imgs_idcard" forKey:@"type"];
    [dict setObject:frontDic forKey:@"data0"];
    [dict setObject:backDic forKey:@"data1"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@------",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSLog(@"%@--%@",responseObject[@"retData"][@"front"][@"msg"],responseObject[@"retData"][@"back"][@"msg"]);
            //正面
            if ([seleIdCartFlat isEqualToString:@"1"]) {
                //处理提示的
                if ([responseObject[@"retData"][@"front"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"front"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"front"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"front"][@"is_lock"] isEqualToString:@"1"]) {
                    if ([responseObject[@"retData"][@"front"][@"is_del"] isEqualToString:@"1"]) {
                        isFrontValidatSuccess=YES;
                        receiveFrontStatus=@"5";
                        //=1表示可以删除（也就是有删除按钮，并且enable）
                        frontIdCartID=responseObject[@"retData"][@"front"][@"up_info"][@"data_id"];
                        NSURL *url=[NSURL URLWithString:responseObject[@"retData"][@"front"][@"up_info"][@"url"]];
                        [personImgBtn01 sd_setImageWithURL:url forState:UIControlStateNormal];
                        personImgBtn01.userInteractionEnabled=NO;
                        frontdeleBtn.userInteractionEnabled=YES;
                        //改变状态
                        _frontIdCartModel.idCartArray=@[@{
                                                            @"id":responseObject[@"retData"][@"front"][@"up_info"][@"data_id"],
                                                            @"path":responseObject[@"retData"][@"front"][@"up_info"][@"url"]}
                                                        ];
                        receiveFrontStatus=[NSString stringWithFormat:@"%@",@"6"];
                        isHadDeleteBtn=YES;
                        frontdeleBtn.hidden=NO;
                        personImgBtn01.tipLabel.hidden=NO;
                        personImgBtn01.tipLabel.text=@"待审核";
                    }else{
                        //=0表示不可以删除（也就是通过验证）
                        isFrontValidatSuccess=YES;
                        receiveFrontStatus=@"5";
                        NSURL *url=[NSURL URLWithString:responseObject[@"retData"][@"front"][@"up_info"][@"url"]];
                        [personImgBtn01 sd_setImageWithURL:url forState:UIControlStateNormal];
                        personImgBtn01.userInteractionEnabled=NO;
                        frontdeleBtn.userInteractionEnabled=NO;
                        isHadDeleteBtn=NO;
                        frontdeleBtn.hidden=YES;
                        
                        //改变状态
                        _frontIdCartModel.idCartArray=@[@{
                                                            @"id":responseObject[@"retData"][@"front"][@"up_info"][@"data_id"],
                                                            @"path":responseObject[@"retData"][@"front"][@"up_info"][@"url"]}
                                                        ];
                        receiveFrontStatus=[NSString stringWithFormat:@"%@",@"99"];
                        personImgBtn01.tipLabel.hidden=NO;
                        personImgBtn01.tipLabel.text=@"验证成功";
                    }
                }
                //正面不保存
                else{
                    isHadDeleteBtn=NO;
                    frontdeleBtn.hidden=YES;
                }
            }
            
            //反面
            else{
                //处理提示的
                if ([responseObject[@"retData"][@"back"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"back"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"back"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"back"][@"is_lock"] isEqualToString:@"1"]) {
                    if ([responseObject[@"retData"][@"back"][@"is_del"] isEqualToString:@"1"]) {
                        isBackValidatSuccess=YES;
                        receiveBackStatus=@"6";
                        //=1表示可以删除（也就是有删除按钮，并且enable）
                        backIdCartID=responseObject[@"retData"][@"back"][@"up_info"][@"data_id"];
                        NSURL *url=[NSURL URLWithString:responseObject[@"retData"][@"back"][@"up_info"][@"url"]];
                        [personImgBtn02 sd_setImageWithURL:url forState:UIControlStateNormal];
                        personImgBtn02.userInteractionEnabled=NO;
                        backdeleBtn.userInteractionEnabled=YES;
                        
                        //改变状态
                        _backIdCartModel.idCartArray=@[@{
                                                           @"id":responseObject[@"retData"][@"back"][@"up_info"][@"data_id"],
                                                           @"path":responseObject[@"retData"][@"back"][@"up_info"][@"url"]}
                                                       ];
                        receiveBackStatus=[NSString stringWithFormat:@"%@",@"6"];
                        
                        isHadDeleteBtn=YES;
                        backdeleBtn.hidden=NO;
                        personImgBtn02.tipLabel.hidden=NO;
                        personImgBtn02.tipLabel.text=@"待审核";
                    }else{
                        //=0表示不可以删除（也就是通过验证）
                        isBackValidatSuccess=YES;
                        receiveBackStatus=@"6";
                        NSURL *url=[NSURL URLWithString:responseObject[@"retData"][@"back"][@"up_info"][@"url"]];
                        [personImgBtn02 sd_setImageWithURL:url forState:UIControlStateNormal];
                        personImgBtn02.userInteractionEnabled=NO;
                        backdeleBtn.userInteractionEnabled=NO;
                        isHadDeleteBtn=NO;
                        backdeleBtn.hidden=YES;
                        
                        //
                        _backIdCartModel.idCartArray=@[@{
                                                           @"id":responseObject[@"retData"][@"back"][@"up_info"][@"data_id"],
                                                           @"path":responseObject[@"retData"][@"back"][@"up_info"][@"url"]}
                                                       ];
                        receiveBackStatus=[NSString stringWithFormat:@"%@",@"99"];
                        personImgBtn02.tipLabel.hidden=NO;
                        personImgBtn02.tipLabel.text=@"审核中";
                    }
                }
                //反面不保存
                else{
                    isHadDeleteBtn=NO;
                    backdeleBtn.hidden=YES;
                }
            }
            
            //正反面是否都验证成功
            if (_frontIdCartModel.idCartArray.count!=0&&
                _backIdCartModel.idCartArray.count!=0) {
                _isOk05=YES;
            }else{
                _isOk05=NO;
            }
            [myTable reloadData];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 删除身份证 Request

-(void)deletePersonIdCart:(NSString *)type picId:(NSString *)picId{
    [AppUtils showProgressMessage:@"正在删除…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    NSString *codeStr=[type isEqualToString:@"1"]?@"delvipidcardimgs_front_iso":@"delvipidcardimgs_back_iso";
    [dict setObject:codeStr forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:picId forKey:@"id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //正面
            if ([type isEqualToString:@"1"]) {
                personImgBtn01.tipLabel.hidden=YES;
                isFrontImgHadata=NO;
                frontdeleBtn.hidden=YES;
                
                [personImgBtn01 setImage:[UIImage imageNamed:@"add_picture.jpg"] forState:UIControlStateNormal];
                personImgBtn01.userInteractionEnabled=YES;
                
                if (_frontIdCartModel.idCartArray.count>0) {
                    _frontIdCartModel.idCartArray=nil;
                }
                receiveFrontStatus=@"0";
            }
            //反面
            else{
                personImgBtn02.tipLabel.hidden=YES;
                isBackImgHadata=NO;
                backdeleBtn.hidden=YES;
                
                [personImgBtn02 setImage:[UIImage imageNamed:@"add_picture.jpg"] forState:UIControlStateNormal];
                personImgBtn02.userInteractionEnabled=YES;
                
                if (_backIdCartModel.idCartArray.count>0) {
                    _backIdCartModel.idCartArray=nil;
                }
                receiveBackStatus=@"0";
            }
            _isOk05=NO;
            //
            if (frontdeleBtn.hidden&&backdeleBtn.hidden) {
                isHadDeleteBtn=NO;
                [myTable reloadData];
            }
            
            //
            //发送通知，上传照片按钮（没有选择全部图片）
            [[NSNotificationCenter defaultCenter] postNotificationName:UploadButtonIsNot object:nil];
        }else{
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 提交验证

-(void)submitValidateRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_info_iso3" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@---",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            /*******
             1、
             2、
             *******/
            [[NSNotificationCenter defaultCenter] postNotificationName:SubmitXYRZSuccess object:nil];
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            _phoneArr=@[volatPhone,@"1",@"",@"",@"",@""];
            _submitBtnStatus=@"2";  //表示审核中
            [myTable reloadData];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

/**
 上传联系人信息
 */
-(void)submitContact:(NSMutableArray *)arr{
    //is_nosign
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    
     NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_user_phons_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [dict setObject:arr forKey:@"data"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        //NSLog(@"%@",dict[@"data"]);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 上传信用报告 Request

-(void)UploadXybgWithData:(NSData *)data type:(NSString *)type size:(NSString *)size{
    [AppUtils showProgressMessage:@"正在上传…" inView:self.view];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_iso" forKey:@"code"];
    [dict setObject:@"xinyong_pdf" forKey:@"type"];
    [dict setObject:data forKey:@"zfl"];
    [dict setObject:type forKey:@"img_type"];
    [dict setObject:size forKey:@"size"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@------",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"data"];
            XybgListModel *model=[XybgListModel new];
            [model secondJsonDate:dic];
            model.tag=_xybgArr.count+1;
            [_xybgArr addObject:model];
            [myTable reloadData];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 删除身份证 Request

-(void)deleteXybgWithId:(XybgListModel *)model andIndex:(NSInteger)row{
    [AppUtils showProgressMessage:@"正在删除…" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"del_vipxinyong_pfd_ios" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:model.bgId forKey:@"id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retData"] isEqualToString:@"ok"]) {
            [_xybgArr removeObjectAtIndex:row];
            [myTable reloadData];
        }else{
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
