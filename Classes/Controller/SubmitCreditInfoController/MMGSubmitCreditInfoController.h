//
//  MMGSubmitCreditInfoController.h
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonIdCartModel.h"
#import "XybgListModel.h"

@interface MMGSubmitCreditInfoController : BaseController

@property (nonatomic, strong) void (^kReceiverBlock)(id model);

@property (nonatomic, copy) NSString *cartStatus_01;
@property (nonatomic, copy) NSString *cartStatus_02;
//
@property (nonatomic, retain) NSArray *cxkArr;
@property (nonatomic, retain) NSArray *phoneArr;
@property (nonatomic, retain) NSArray *contaceArr;
@property (nonatomic, retain) NSArray *txdzArr; //通讯地址
@property (nonatomic, retain) NSArray *idCartArr;
@property (nonatomic, retain) NSMutableArray *xybgArr; //信用bg

@property (nonatomic, retain) PersonIdCartModel *frontIdCartModel;      //身份证正面
@property (nonatomic, retain) PersonIdCartModel *backIdCartModel;       //身份证反面

@property (nonatomic, copy) NSString *status_msg;    //审核是否失败
/*
 *1--未审核
 *2--审核中
 *3--审核通过
 */
@property (nonatomic, copy) NSString *submitBtnStatus;    //总的验证状态

@property(nonatomic, assign) BOOL isOk01;
@property(nonatomic, assign) BOOL isOk02;
@property(nonatomic, assign) BOOL isOk03;
@property(nonatomic, assign) BOOL isOk04;
@property(nonatomic, assign) BOOL isOk05;

@end
