//
//  ShoppingCartController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "ShoppingCartController.h"
#import "ShoppingCartCell.h"
#import "SubmitOrderController.h"
#import "ProductDetailController.h"
#import "SingFMDB.h"
#import "Macro2.h"
#import "HttpRequests.h"
#import "MMGSign.h"
#import "LoginController.h"
#import "WelcomeController.h"
#define TotalPriceNotification @"TotalPriceNotification"

//ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
@interface ShoppingCartController ()<UITableViewDataSource,UITableViewDelegate,ShoppingCartDelegate>

@end

@implementation ShoppingCartController{
    UIButton *seletedBtn;//全选按钮
    
    UIView *nothingGoodView;
    UIView *hadGoodView;
    UIView *buyView;
    
    NSMutableArray *dataArr;
    UITableView *goodTableView;
    
    SingFMDB *db;
    
    UIButton *btn;
    NSString *btnStr;
    UIButton *allSeleBtn;
    BOOL isSelectAll;   //全选按钮是否选择;
    BOOL isSeleStageBtn;//是否选择分期数按钮;
    CGFloat allPrice;//总价格
    CGFloat allPriceFloat;//总价格
    TTTAttributedLabel *totalPriceLabel;
    TTTAttributedLabel *stageFeeLabel;
    TTTAttributedLabel *feelLabel;
    TTTAttributedLabel *orderPriceLabel;
    UIButton *buyBtn;
    
    CGFloat stageRate01;    //1期手续费率
    CGFloat stageRate03;    //3期手续费率
    CGFloat stageRate06;    //6期手续费率
    CGFloat stageRate09;    //9期手续费率
    CGFloat stageRate12;    //12期手续费率
    CGFloat shuiFee;        //税费
    
    NSString *remittancesFee01; //1期手续费
    NSString *remittancesFee03; //3期手续费
    NSString *remittancesFee06; //6期手续费
    NSString *remittancesFee09; //9期手续费
    NSString *remittancesFee12; //12期手续费
    
    NSString *orderRemittancesFee; //提交订单的手续费
    
    NSString *selectedStageSum; //选中的分期数
    NSMutableArray *selectedGoodsArr; //选择的商品
    CGFloat subMit_TotalPrice;
    CGFloat subMit_OrderPrice;
    CGFloat subMit_EveryStagePrice;
    NSString *seleTotalGoodSum;
    
    NSString *selectedGoodId;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self seNavBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpDateViewData) name:UpDateShoppingCart object:nil];
    
    [self setUpNothingGoodView];
    
    [self setUpHadGoodView];
    
    [self requestHttpsForStageRate];
    
    [self UpDateViewData];
}
-(void)seNavBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    //    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    //UIColor *color1=[UIColor colorWithRed:0.0 green:127.0 blue:181.0 alpha:1];
    
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    //UIColor *color2=[[UIColor alloc] initWithRed:2.0f green:116.0f blue:181.0f alpha:1];
    //[statusBarView setBackgroundColor:color1];
    
    [self.view addSubview:statusBarView];
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(flagTurnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"购物车";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}

-(void)flagTurnBack

{
       self.tabBarController.selectedIndex=0;

}
-(void)UpDateViewData{
    
    isSelectAll=YES;
    
    isSeleStageBtn=NO;
    
    allPrice=0;
    
    selectedGoodsArr=[NSMutableArray array];
    
    /*****************************
     *1、查询数据库
     *2、显示购物车（空或不空）时的页面
     *****************************/
    db=[SingFMDB shareFMDB];
    dataArr=[db searchAllCollectionGood];
    selectedGoodsArr=[NSMutableArray arrayWithArray:dataArr];
    if (dataArr.count<=0) {
        [self.view bringSubviewToFront:nothingGoodView];
    }else{
        [self.view bringSubviewToFront:hadGoodView];
    }
    
    //设置默认全选
    isSelectAll=YES;
    for (int i=0; i<dataArr.count; i++){
        ShoppingCartModel *model = dataArr[i];
        model.selectState = isSelectAll;
    }
    
    [self totalPrice];//总价格
    
    NSString *stStr=[NSString stringWithFormat:@"分期手续费:0元"];
    [stageFeeLabel setText:stStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"0元"] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    [goodTableView reloadData];
}

#pragma mark - Request(获取分期利率)
-(void)requestHttpsForStageRate{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"iso_fenqi_fee"
                         };
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *stageRateStr01=responseObject[@"retData"][@"fenqi"][@"1"];
            NSString *stageRateStr03=responseObject[@"retData"][@"fenqi"][@"3"];
            NSString *stageRateStr06=responseObject[@"retData"][@"fenqi"][@"6"];
            NSString *stageRateStr09=responseObject[@"retData"][@"fenqi"][@"9"];
            NSString *stageRateStr12=responseObject[@"retData"][@"fenqi"][@"12"];
            NSString *shuiFeeStr=responseObject[@"retData"][@"sw"];
            
            stageRate01=stageRateStr01.floatValue;
            stageRate03=stageRateStr03.floatValue;
            stageRate06=stageRateStr06.floatValue;
            stageRate09=stageRateStr09.floatValue;
            stageRate12=stageRateStr12.floatValue;
            shuiFee=shuiFeeStr.floatValue;
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark 商品为空时显示的View
-(void)setUpNothingGoodView{
    nothingGoodView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113)];
    nothingGoodView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:nothingGoodView];
    
    //购物车图片
    UIImageView *logoImgv=[[UIImageView alloc]initWithFrame:CGRectMake(114, 96, SCREENSIZE.width-228, SCREENSIZE.width-228)];
    [logoImgv setImage:[UIImage imageNamed:@"shopcart_logo"]];
    logoImgv.contentMode=UIViewContentModeScaleAspectFit;
    [nothingGoodView addSubview:logoImgv];
    
    //提示语
    UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(40, logoImgv.frame.origin.y+logoImgv.frame.size.height+16, SCREENSIZE.width-80, 21)];
    tipLabel.font=[UIFont systemFontOfSize:15];
    tipLabel.textColor=[UIColor lightGrayColor];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.text=@"购物车内暂时没有商品,你可以...";
    [nothingGoodView addSubview:tipLabel];
    
    //返回首页按钮
    UIButton *goHomePageBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    goHomePageBtn.frame=CGRectMake(SCREENSIZE.width/3-10, tipLabel.frame.origin.y+tipLabel.frame.size.height+16, SCREENSIZE.width/3+20, 34);
    goHomePageBtn.layer.cornerRadius=3;
    goHomePageBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    goHomePageBtn.titleLabel.textColor=[UIColor whiteColor];
    goHomePageBtn.titleLabel.font=[UIFont systemFontOfSize:15 ];
    [goHomePageBtn setTitle:@"返回首页挑选商品" forState:UIControlStateNormal];
    [goHomePageBtn addTarget:self action:@selector(goHomePageTabbar) forControlEvents:UIControlEventTouchUpInside];
    [nothingGoodView addSubview:goHomePageBtn];
}

-(void)goHomePageTabbar{
    self.tabBarController.selectedIndex=0;
}

#pragma mark - 商品不为空时显示的View
-(void)setUpHadGoodView{
    
    hadGoodView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113)];
    [self.view addSubview:hadGoodView];
    
    goodTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, hadGoodView.frame.size.height) style:UITableViewStylePlain];
    [hadGoodView addSubview:goodTableView];
    goodTableView.dataSource=self;
    goodTableView.delegate=self;
    [goodTableView registerNib:[UINib nibWithNibName:@"ShoppingCartCell" bundle:nil] forCellReuseIdentifier:@"cellId"];
    
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    bagView.userInteractionEnabled=YES;
    bagView.image=[UIImage imageNamed:@"login_baground"];
    goodTableView.backgroundView=bagView;
    
#pragma mark 结算的View
    //
    buyView=[[UIView alloc]init];
    buyView.backgroundColor=[UIColor whiteColor];
    [hadGoodView addSubview:buyView];
    buyView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* buyView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buyView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyView,buyView)];
    [NSLayoutConstraint activateConstraints:buyView_h];
    
    NSArray* buyView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buyView(90)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyView)];
    [NSLayoutConstraint activateConstraints:buyView_w];
    
    //全选按钮
    allSeleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    allSeleBtn.layer.cornerRadius=3;
    [allSeleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    allSeleBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [allSeleBtn setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
    allSeleBtn.imageEdgeInsets=UIEdgeInsetsMake(0, -11, 0, 0);
    [allSeleBtn setTitle:@"全选" forState:UIControlStateNormal];
    [allSeleBtn addTarget:self action:@selector(clickallSeleBtn:) forControlEvents:UIControlEventTouchUpInside];
    [buyView addSubview:allSeleBtn];
    allSeleBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* allSeleBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[allSeleBtn(64)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn)];
    [NSLayoutConstraint activateConstraints:allSeleBtn_h];
    
    NSArray* allSeleBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[allSeleBtn]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn)];
    [NSLayoutConstraint activateConstraints:allSeleBtn_w];
    
    __weak typeof(self) weakSelf = self;
    //结算按钮
    buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.layer.cornerRadius=3;
    buyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buyBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [buyBtn addTarget:self action:@selector(clickGoBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    [buyView addSubview:buyBtn];
    buyBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* buyBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[buyBtn(90)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyBtn)];
    [NSLayoutConstraint activateConstraints:buyBtn_h];
    
    NSArray* buyBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-23-[buyBtn]-23-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyBtn)];
    [NSLayoutConstraint activateConstraints:buyBtn_w];
    weakSelf.SelectedGoodSumBlock = ^(NSString *selectGoodSum){
        [self setBuyButtonTitle:selectGoodSum];
    };
    
    CGFloat labelHeight=84/4;
    //总金额
    totalPriceLabel=[TTTAttributedLabel new];
    totalPriceLabel.font=[UIFont systemFontOfSize:13];
    totalPriceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [buyView addSubview:totalPriceLabel];
    totalPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* totalPrice_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[allSeleBtn][totalPriceLabel]-2-[buyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn,totalPriceLabel,buyBtn)];
    [NSLayoutConstraint activateConstraints:totalPrice_h];
    
    NSString *ttpHStr=[NSString stringWithFormat:@"V:|-3-[totalPriceLabel(%f)]",labelHeight];
    NSArray* totalPrice_w = [NSLayoutConstraint constraintsWithVisualFormat:ttpHStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(totalPriceLabel)];
    [NSLayoutConstraint activateConstraints:totalPrice_w];
    
    //
    stageFeeLabel=[TTTAttributedLabel new];
    stageFeeLabel.font=[UIFont systemFontOfSize:13];
    stageFeeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [buyView addSubview:stageFeeLabel];
    stageFeeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* stageFeeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[allSeleBtn][stageFeeLabel]-2-[buyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn,stageFeeLabel,buyBtn)];
    [NSLayoutConstraint activateConstraints:stageFeeLabel_h];
    
    NSString *sttgHStr=[NSString stringWithFormat:@"V:[totalPriceLabel][stageFeeLabel(%f)]",labelHeight];
    NSArray* stageFeeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:sttgHStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(totalPriceLabel,stageFeeLabel)];
    [NSLayoutConstraint activateConstraints:stageFeeLabel_w];
    weakSelf.StageFeeBlock = ^(NSString *stageFeeStr){
        [self setStageFeeLabel:stageFeeStr];
    };
    
    //利息费
    feelLabel=[TTTAttributedLabel new];
    feelLabel.font=[UIFont systemFontOfSize:13];
    feelLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [buyView addSubview:feelLabel];
    feelLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* feelLLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[allSeleBtn][feelLabel]-2-[buyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn,feelLabel,buyBtn)];
    [NSLayoutConstraint activateConstraints:feelLLabel_h];
    
    NSString *stflHStr=[NSString stringWithFormat:@"V:[stageFeeLabel][feelLabel(%f)]",labelHeight];
    NSArray* feelLLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:stflHStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageFeeLabel,feelLabel)];
    [NSLayoutConstraint activateConstraints:feelLLabel_w];
    weakSelf.FeeLvBlock = ^(NSString *feeLvStr){
        [self setFeeLvLabel:feeLvStr];
    };
    
    //order总金额
    orderPriceLabel=[TTTAttributedLabel new];
    orderPriceLabel.font=[UIFont systemFontOfSize:13];
    orderPriceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [buyView addSubview:orderPriceLabel];
    orderPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* orderPriceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[allSeleBtn][orderPriceLabel]-2-[buyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(allSeleBtn,orderPriceLabel,buyBtn)];
    [NSLayoutConstraint activateConstraints:orderPriceLabel_h];
    
    NSArray* orderPriceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[feelLabel][orderPriceLabel]-3-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(feelLabel,orderPriceLabel)];
    [NSLayoutConstraint activateConstraints:orderPriceLabel_w];
    weakSelf.OrderTotalPriceBlock = ^(NSString *orderTotalPrice){
        [self setOrderTotalPriceLabel:orderTotalPrice];
    };
    
}

-(void)setStageFeeLabel:(NSString *)stageFee{
    NSLog(@"---------%@",stageFee);
    NSString *stStr=[NSString stringWithFormat:@"分期手续费:%@元",stageFee];
    [stageFeeLabel setText:stStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",stageFee] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
}

-(void)setFeeLvLabel:(NSString *)feeLvStr{
    if(feeLvStr.floatValue>=9000){
        feeLvStr=@"9000";
    }
    NSString *stStr=[NSString stringWithFormat:@"鉴定保正费:%@元",feeLvStr];
    [feelLabel setText:stStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",feeLvStr] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
}

-(void)setOrderTotalPriceLabel:(NSString *)orderTotalPrice{
    subMit_OrderPrice=orderTotalPrice.floatValue;
    NSString *ddStr=[NSString stringWithFormat:@"订单总金额:%@元",orderTotalPrice];
    [orderPriceLabel setText:ddStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",orderTotalPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
}

-(void)setBuyButtonTitle:(NSString *)seleGoodSum{
    seleTotalGoodSum=seleGoodSum;
    NSString *btnTitleStr=[NSString stringWithFormat:@"去结算（%@）",seleGoodSum];
    [buyBtn setTitle:btnTitleStr forState:UIControlStateNormal];
}

#pragma mark - 分期按钮的View
-(UIView *)setUpStageRateView{
    UIView *rateView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 249)];
    UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 12, SCREENSIZE.width, 21)];
    tipLabel.text=@"分期价";
    tipLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    tipLabel.font=[UIFont systemFontOfSize:14];
    [rateView addSubview:tipLabel];
    for (int i=0; i<5; i++) {
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(8, 36+i*36+i*6, SCREENSIZE.width-16, 36);
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ababab"].CGColor;
        btn.layer.cornerRadius=2;
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        CGFloat remittancesFee=0;  //手续费
        NSString *rateStr01=@"分1期 ";
        NSString *rateStr03=@"分3期 ";
        NSString *rateStr06=@"分6期 ";
        NSString *rateStr09=@"分9期 ";
        NSString *rateStr12=@"分12期 ";
        CGFloat stagePriceFloat=0;  //各个分期价格
        NSString *stagePriceStr;       //各个分期价格
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        if (i==0) {
            btn.tag=1;
            
            remittancesFee=allPriceFloat*stageRate01;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee01=remittancesFeeStr;
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/1;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr01,subStageStr];
            
            objc_setAssociatedObject(btn, "btnstagePriceStr",subStageStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
            
            [self defaultSelected];
            
        }else if (i==1){
            btn.tag=3;
            remittancesFee=allPriceFloat*stageRate03;
            
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee03=remittancesFeeStr;
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/3;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr03,subStageStr];
            objc_setAssociatedObject(btn, "btnstagePriceStr",subStageStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if (i==2){
            btn.tag=6;
            remittancesFee=allPriceFloat*stageRate06;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee06=remittancesFeeStr;
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/6;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr06,subStageStr];
            objc_setAssociatedObject(btn, "btnstagePriceStr",subStageStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if(i==3){
            btn.tag=9;
            remittancesFee=allPriceFloat*stageRate09;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee09=remittancesFeeStr;
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/9;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr09,subStageStr];
            objc_setAssociatedObject(btn, "btnstagePriceStr",subStageStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if(i==4){
            btn.tag=12;
            remittancesFee=allPriceFloat*stageRate12;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee12=remittancesFeeStr;
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/12;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr12,subStageStr];
            objc_setAssociatedObject(btn, "btnstagePriceStr",subStageStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }
        [btn setTitle:btnStr forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickRateBtn:) forControlEvents:UIControlEventTouchDown];
        __weak typeof(self) weakSelf = self;
        weakSelf.TotalPriceBlock = ^(CGFloat totalPrice){
            [self resetLabel:totalPrice tag:btn];
        };
        [rateView addSubview:btn];
    }
    subMit_EveryStagePrice=allPriceFloat;
    return rateView;
}


#pragma mark - 总价格eBlock method
- (void)resetLabel:(CGFloat)totalPrice tag:(id)sender{
    
    CGFloat remittancesFee=0;   //手续费
    NSString *rateStr01=@"分1期 ";
    NSString *rateStr03=@"分3期 ";
    NSString *rateStr06=@"分6期 ";
    NSString *rateStr09=@"分9期 ";
    NSString *rateStr12=@"分12期 ";
    CGFloat stagePriceFloat=0;  //各个分期价格
    NSString *stagePriceStr;       //各个分期价格
    UIButton *button=(UIButton *)sender;
    if (button.tag==0) {
        
        remittancesFee=allPriceFloat*stageRate01;
        
        NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
        NSRange rang=[remittancesFeeStr rangeOfString:@"."];
        if (rang.location != NSNotFound) {
            remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
        }
        NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
        
        //
        stagePriceFloat=allPriceFloat/1;
        NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
        NSRange range=[subStageStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            subStageStr=[subStageStr substringToIndex:range.location];
        }
        stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr01,subStageStr];
        //
        btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
    }else if (button.tag==1){
        remittancesFee=allPriceFloat*stageRate03;
        NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
        NSRange rang=[remittancesFeeStr rangeOfString:@"."];
        if (rang.location != NSNotFound) {
            remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
        }
        NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
        
        
        //
        stagePriceFloat=allPriceFloat/3;
        NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
        NSRange range=[subStageStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            subStageStr=[subStageStr substringToIndex:range.location];
        }
        stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr03,subStageStr];
        //
        btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
    }else if (button.tag==2){
        remittancesFee=allPriceFloat*stageRate06;
        NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
        NSRange rang=[remittancesFeeStr rangeOfString:@"."];
        if (rang.location != NSNotFound) {
            remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
        }
        NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
        
        
        //
        stagePriceFloat=allPriceFloat/6;
        NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
        NSRange range=[subStageStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            subStageStr=[subStageStr substringToIndex:range.location];
        }
        stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr06,subStageStr];
        //
        btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
    }else if(button.tag==3){
        remittancesFee=allPriceFloat*stageRate09;
        NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
        NSRange rang=[remittancesFeeStr rangeOfString:@"."];
        if (rang.location != NSNotFound) {
            remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
        }
        NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
        
        //
        stagePriceFloat=allPriceFloat/9;
        NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
        NSRange range=[subStageStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            subStageStr=[subStageStr substringToIndex:range.location];
        }
        stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr09,subStageStr];
        //
        btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
    }else if(button.tag==4){
        remittancesFee=allPriceFloat*stageRate12;
        NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
        NSRange rang=[remittancesFeeStr rangeOfString:@"."];
        if (rang.location != NSNotFound) {
            remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
        }
        NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
        
        //
        stagePriceFloat=allPriceFloat/12;
        NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
        NSRange range=[subStageStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            subStageStr=[subStageStr substringToIndex:range.location];
        }
        stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr12,subStageStr];
        //
        btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
    }
    [btn setTitle:btnStr forState:UIControlStateNormal];
}

#pragma mark - 默认选中第一个
-(void)defaultSelected{
    //默认选中第一个
    isSeleStageBtn=YES;
    seletedBtn.backgroundColor=[UIColor whiteColor];
    [seletedBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    seletedBtn.enabled=YES;
    //当前点击的button
    UIButton *sebtn=btn;
    switch (sebtn.tag) {
        case 1:
            orderRemittancesFee=remittancesFee01;
            break;
        case 3:
            orderRemittancesFee=remittancesFee03;
            break;
        case 6:
            orderRemittancesFee=remittancesFee06;
            break;
        case 9:
            orderRemittancesFee=remittancesFee09;
            break;
        case 12:
            orderRemittancesFee=remittancesFee12;
            break;
        default:
            break;
    }
    if (self.StageFeeBlock) {
        self.StageFeeBlock(orderRemittancesFee);
    }
    
    CGFloat flFloat=allPriceFloat*0.03;
    if (self.FeeLvBlock) {
        self.FeeLvBlock([NSString stringWithFormat:@"%.f",flFloat]);
    }
     
    
    CGFloat orderPriceFloat=orderRemittancesFee.floatValue+allPriceFloat;
    NSString *orderPriceStr=[NSString stringWithFormat:@"%.f",orderPriceFloat];
    if (self.OrderTotalPriceBlock) {
        self.OrderTotalPriceBlock(orderPriceStr);
    }
    //获取选中的分期数
    NSInteger seleInt=sebtn.tag;
    selectedStageSum=[NSString stringWithFormat:@"%ld",(long)seleInt];
    sebtn.backgroundColor=[UIColor colorWithHexString:@"#cf1316"];
    [sebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sebtn.enabled=NO;
    //用另一个button代替当前点击的button
    seletedBtn=sebtn;
    //
}

#pragma mark - 点击分期数按钮
-(void)clickRateBtn:(id)sender{
    
    isSeleStageBtn=YES;
    
    seletedBtn.backgroundColor=[UIColor whiteColor];
    [seletedBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    seletedBtn.enabled=YES;
    
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    NSString *btnStr1 = objc_getAssociatedObject(sebtn, "btnstagePriceStr");
    switch (sebtn.tag) {
        case 1:
            subMit_EveryStagePrice=btnStr1.floatValue;
            orderRemittancesFee=remittancesFee01;
            break;
        case 3:
            subMit_EveryStagePrice=btnStr1.floatValue;
            orderRemittancesFee=remittancesFee03;
            break;
        case 6:
            subMit_EveryStagePrice=btnStr1.floatValue;
            orderRemittancesFee=remittancesFee06;
            break;
        case 9:
            subMit_EveryStagePrice=btnStr1.floatValue;
            orderRemittancesFee=remittancesFee09;
            break;
        case 12:
            subMit_EveryStagePrice=btnStr1.floatValue;
            orderRemittancesFee=remittancesFee12;
            break;
        default:
            break;
    }
    
    if (self.StageFeeBlock) {
        self.StageFeeBlock(orderRemittancesFee);
    }
    
    CGFloat flFloat=allPriceFloat*0.03;
    if (self.FeeLvBlock) {
        self.FeeLvBlock([NSString stringWithFormat:@"%.f",flFloat]);
    }
    
    CGFloat orderPriceFloat=orderRemittancesFee.floatValue+allPriceFloat+flFloat;
    NSString *orderPriceStr=[NSString stringWithFormat:@"%.f",orderPriceFloat];
    if (self.OrderTotalPriceBlock) {
        self.OrderTotalPriceBlock(orderPriceStr);
    }
    
    //获取选中的分期数
    NSInteger seleInt=sebtn.tag;
    selectedStageSum=[NSString stringWithFormat:@"%ld",(long)seleInt];
    
    sebtn.backgroundColor=[UIColor colorWithHexString:@"#cf1316"];
    [sebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sebtn.enabled=NO;
    
    //用另一个button代替当前点击的button
    seletedBtn=sebtn;
}

#pragma mark - 全选
-(void)clickallSeleBtn:(UIButton *)sender{
    if (!isSelectAll){
        [sender setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
        selectedGoodsArr=[NSMutableArray arrayWithArray:dataArr];
    }else{
        [sender setImage:[UIImage imageNamed:@"allBtn_nor"] forState:UIControlStateNormal];
        
        [selectedGoodsArr removeAllObjects];
        
        isSeleStageBtn=NO;
    }
    isSelectAll=!isSelectAll;
    for (int i=0; i<dataArr.count; i++){
        ShoppingCartModel *model = dataArr[i];
        model.selectState = isSelectAll;
    }
    //计算价格
    [self totalPrice];
    //刷新表格
    [goodTableView reloadData];
}

#pragma mark - 点击去结算
/*
 *1、如果未登录，跳转到登陆页面
 *2、已登陆，跳转到提交订单页面
 */
-(void)clickGoBuyBtn{
    if (![AppUtils loginState]) {
        WelcomeController *login=[[WelcomeController alloc]init];
        login.isShop=YES;
//        LoginController *login=[[LoginController alloc]init];
//        UINavigationController *logNav=[[UINavigationController alloc]initWithRootViewController:login];
        [self presentViewController:login animated:YES completion:nil];
    }else{
        if (selectedGoodsArr.count<=0) {
            [AppUtils showSuccessMessage:@"未选择任何选择商品" inView:self.view];
        }else{
            if (isSeleStageBtn) {
                [self requestParamData];
            }else{
                [AppUtils showSuccessMessage:@"请选择分期数" inView:self.view];
            }
        }
    }
}

#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 87;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCartCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellId"];
    if (cell==nil) {
        cell=[[ShoppingCartCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellId"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    ShoppingCartModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    cell.delegate=self;
    return  cell;
}

//单元格选中事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCartModel *model=dataArr[indexPath.row];
    ProductDetailController *productDetail=[[ProductDetailController alloc]init];
    productDetail.goodId=model.goodsId;
    productDetail.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:productDetail animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([goodTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [goodTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([goodTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [goodTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - CellButton Event Delegate
//加入收藏夹
-(void)joinCollectionForModel:(ShoppingCartModel *)model{
    LoginController *login=[[LoginController alloc]init];
    UINavigationController *logNav=[[UINavigationController alloc]initWithRootViewController:login];
    selectedGoodId=model.goodsId;
    if ([AppUtils loginState]) {
        [self requestHttpsForGoodId:selectedGoodId];
    }else{
        [self presentViewController:logNav animated:YES completion:nil];
    }
}

//购物车删除
-(void)deleteGoodForModel:(ShoppingCartModel *)model{
    //配置一下
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMAlertViewConfig *sheetConfig = [MMAlertViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [dataArr removeObject:model];
                
                [db deleteMyGood:model.goodsId];
                
                [self UpDateViewData];
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"确定", MMItemTypeNormal, block),
      MMItemMake(@"取消", MMItemTypeNormal, block)];
    
    [[[MMAlertView alloc] initWithTitle:@"提示" detail:@"是否删除该商品" items:items] showWithBlock:completeBlock];
}

-(void)selectedGoodCellForModel:(ShoppingCartModel *)model indexPath:(NSIndexPath *)indexPath{
    
    isSeleStageBtn=NO;
    NSString *stStr=[NSString stringWithFormat:@"分期手续费:0元"];
    [stageFeeLabel setText:stStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"0元"] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    /**
     *  判断当期是否为选中状态，如果选中状态点击则更改成未选中，如果未选中点击则更改成选中状态
     */
    
    ShoppingCartModel *model1 =model;
    
    if (model1.selectState){
        model1.selectState = NO;
        [seletedBtn setImage:[UIImage imageNamed:@"allBtn_nor"] forState:UIControlStateNormal];
        [selectedGoodsArr removeObject:model1];
        [allSeleBtn setImage:[UIImage imageNamed:@"allBtn_nor"] forState:UIControlStateNormal];
    }
    else{
        model1.selectState = YES;
        [seletedBtn setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
        [selectedGoodsArr addObject:model1];
        [allSeleBtn setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
    }
    
    //根据选择数量设置全选按钮
    if (selectedGoodsArr.count<dataArr.count) {
        isSelectAll=NO;
        [allSeleBtn setImage:[UIImage imageNamed:@"allBtn_nor"] forState:UIControlStateNormal];
    }else{
        isSelectAll=YES;
        [allSeleBtn setImage:[UIImage imageNamed:@"allBtn_sele"] forState:UIControlStateNormal];
    }
    
    //刷新当前行
    [goodTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [self totalPrice];
    
    CGFloat seleGoodSumFloat=selectedGoodsArr.count;
    NSString *seleGoodSumStr=[NSString stringWithFormat:@"%.f",seleGoodSumFloat];
    if (self.SelectedGoodSumBlock) {
        self.SelectedGoodSumBlock(seleGoodSumStr);
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForGoodId:(NSString *)goodId{
    time_t now;
    time(&now);
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"collect" forKey:@"act"];
    [dict setObject:goodId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"yx_uid"];
    [dict setObject:uSr!=nil?uSr:@"" forKey:@"yx_uname"];
    [dict setObject:pSr!=nil?pSr:@"" forKey:@"yx_phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    [[HttpRequests defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"user.php?"] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForGoodId:selectedGoodId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:@"收藏成功" inView:self.view];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)requestParamData{
    [AppUtils showProgressInView:self.view];
    NSMutableArray *idArr=[NSMutableArray array];
    for (ShoppingCartModel *model in dataArr) {
        [idArr addObject:model.goodsId];
    }
    NSString *pSr=[AppUtils getValueWithKey:User_Phone];
    NSString *uSr=[AppUtils getValueWithKey:User_Name];
    NSDictionary *dict=@{
                         @"act":@"get_goods_num",
                         @"is_nosign":@"1",
                         @"is_phone":@"1",
                         @"goods_id_arr":idArr,
                         @"yx_uid":[AppUtils getValueWithKey:User_ID],
                         @"yx_uname":uSr!=nil?uSr:@"",
                         @"yx_phone":pSr!=nil?pSr:@""
                         };
    [[HttpRequests defaultClient] requestWithPath:[NSString stringWithFormat:@"%@public_goods.php?",ImageHost] method:HttpRequestPosts parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"sccuess"]) {
            NSArray *arr=responseObject[@"retData"];
            NSString *idStrrr=@"";
            for (NSString *str in arr) {
                idStrrr=[idStrrr stringByAppendingString:str];
            }
            NSRange rang=[idStrrr rangeOfString:@"1"];
            if (rang.location!=NSNotFound) {
                NSMutableArray *subOrderGoodIDArr=[NSMutableArray array];
                for (ShoppingCartModel *model in selectedGoodsArr) {
                    [subOrderGoodIDArr addObject:model.goodsId];
                }
                
                SubmitOrderController *submitOrder=[[SubmitOrderController alloc]init];
                submitOrder.shuiFeeFloat=shuiFee;
                submitOrder.stageSum=selectedStageSum;
                submitOrder.stageFee=orderRemittancesFee;
                submitOrder.goodsIDArr=subOrderGoodIDArr;
                submitOrder.receiveDataArr=selectedGoodsArr;
                submitOrder.totalPrice=[NSString stringWithFormat:@"%ld",(long)subMit_TotalPrice];
                submitOrder.orderPrice=[NSString stringWithFormat:@"%ld",(long)subMit_OrderPrice];
                submitOrder.everyStagePrice=[NSString stringWithFormat:@"%ld",(long)subMit_EveryStagePrice];
                submitOrder.seleGoodSum=seleTotalGoodSum;
                submitOrder.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:submitOrder animated:YES];
            }else{
                [AppUtils showSuccessMessage:@"所选商品已出售" inView:self.view];
            }
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"message"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

#pragma mark - 计算价格
-(void)totalPrice{
    //遍历整个数据源，然后判断如果是选中的商品，就计算价格（单价 * 商品数量）
    for ( int i =0; i<dataArr.count; i++){
        ShoppingCartModel *model = dataArr[i];
        if (model.selectState){
            allPrice = allPrice + [model.goodsPrice floatValue];
        }
    }
    
    subMit_TotalPrice=allPrice;
    
    //默认给总价文本赋值
    NSString *zjeStr=[NSString stringWithFormat:@"总金额:%ld元",(long)allPrice];
    NSString *zjeRangStr=[NSString stringWithFormat:@"%ld元",(long)allPrice];
    [totalPriceLabel setText:zjeStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:zjeRangStr options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    NSString *stStr=[NSString stringWithFormat:@"分期手续费:0元"];
    [stageFeeLabel setText:stStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"0元"] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    //
    CGFloat sfFloat=allPrice*0.03;
    if(sfFloat>=9000){
        sfFloat=9000;
    }
    NSString *sfStr=[NSString stringWithFormat:@"税费:%.f元",sfFloat];
    [feelLabel setText:sfStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%.f元",sfFloat] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    //
    NSString *ddStr=[NSString stringWithFormat:@"订单总金额:%ld元",(long)allPrice];
    NSString *opRangeStr=[NSString stringWithFormat:@"%ld元",(long)allPrice];
    [orderPriceLabel setText:ddStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:opRangeStr options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    
    CGFloat goodSum=selectedGoodsArr.count;
    seleTotalGoodSum=[NSString stringWithFormat:@"%.f",goodSum];
    NSString *btnTitleStr=[NSString stringWithFormat:@"去结算（%.f）",goodSum];
    [buyBtn setTitle:btnTitleStr forState:UIControlStateNormal];
    
    allPriceFloat=allPrice;
    if (self.TotalPriceBlock) {
        self.TotalPriceBlock(allPriceFloat);
    }
    
    NSString *totalPriceStr=[NSString stringWithFormat:@"%ld",(long)allPrice];
    [self hiddenRateViewWithPrice:totalPriceStr];
    
    //每次算完要重置为0，因为每次的都是全部循环算一遍
    allPrice = 0.0;
    
}

//总价格为0时隐藏TableFootView
-(void)hiddenRateViewWithPrice:(NSString *)totalPrice{
    if (![totalPrice isEqualToString:@"0"]) {
        goodTableView.tableFooterView=[self setUpStageRateView];
    }else{
        goodTableView.tableFooterView=[[UIView alloc]init];
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
