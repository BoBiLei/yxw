//
//  ShoppingCartController.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface ShoppingCartController : UIViewController

@property (nonatomic, strong) void (^TotalPriceBlock)(CGFloat totalPrice);
@property (nonatomic, strong) void (^StageFeeBlock)(NSString *stageFeeStr);
@property (nonatomic, strong) void (^FeeLvBlock)(NSString *feeLvStr);
@property (nonatomic, strong) void (^OrderTotalPriceBlock)(NSString *orderTotalPrice);
@property (nonatomic, strong) void (^SelectedGoodSumBlock)(NSString *selectGoodSum);

@end
