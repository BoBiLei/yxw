//
//  MMGUseCouponController.h
//  MMG
//
//  Created by mac on 15/10/22.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface MMGUseCouponController : BaseController

@property (nonatomic,copy) NSString *selectedCouponId; //选择的优惠券ID
@property(nonatomic,copy) NSString *goodPrice;
@property (nonatomic, strong) void (^kCouponBlock)(id model);

@end
