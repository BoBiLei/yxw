//
//  SystemInfoController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 游侠旅游. All rights reserved.
//

#import "SystemInfoController.h"

@interface SystemInfoController ()

@end

@implementation SystemInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"系统信息"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
