//
//  RootViewController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "MMGRootViewController.h"
#import "MMGHomePageController.h"
#import "CategoryController.h"
#import "ShoppingCartController.h"
#import "UserCenterController.h"
#import "MMGLoginController.h"
#import "Macro2.h"
#import "HomePageController.h"
#import "WelcomeController.h"
@interface MMGRootViewController ()<UITabBarControllerDelegate>

@end

@implementation MMGRootViewController{
    UINavigationController *shoppingCartNav;
    
    UserCenterController *userCenter;
    UINavigationController *userCenterNav;
}

- (void)viewDidLoad {
    [super viewDidLoad];

        /****************************
     1、接收通知
     2、登录成功后tabbar下标跳转到个人中心
    *****************************/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateSeleteIndex) name:NotifitionLoginSuccess object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateSeleteIndex) name:@"ShopLoginSuccess" object:nil];
    
    /*****************
     商品详情立即购买后跳转到购物车
     *****************/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoShoppingCartIndex) name:NotifitionGoodDetailGoToShoppingCart object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setMMGRootSeleindex) name:@"setMMG_root_seleindex" object:nil];
    
    self.delegate=self;
    [self setUpViewController];
   
}

-(void)upDateSeleteIndex{
    self.selectedIndex=3;
}

-(void)setMMGRootSeleindex{
    if (![AppUtils loginState]) {
        self.selectedIndex=0;
    }
}
    


-(void)gotoShoppingCartIndex{
    self.selectedIndex=2;
}

-(void)setUpViewController{
    MMGHomePageController *homePage=[[MMGHomePageController alloc]init];
    homePage.title=@"首页";
    [homePage.tabBarItem setImage:[UIImage imageNamed:@"tab_home"]];
    [homePage.tabBarItem setSelectedImage:[UIImage imageNamed:@"tab_home"]];
    UINavigationController *homePageNav=[[UINavigationController alloc]initWithRootViewController:homePage];
    homePageNav.navigationBar.shadowImage = [[UIImage alloc]init];
    
    CategoryController *category=[[CategoryController alloc]init];
    category.title=@"分类";
    [category.tabBarItem setImage:[UIImage imageNamed:@"tab_category"]];
    [category.tabBarItem setSelectedImage:[UIImage imageNamed:@"tab_category"]];
    UINavigationController *categoryNav=[[UINavigationController alloc]initWithRootViewController:category];
//    categoryNav.navigationBar.shadowImage = [[UIImage alloc]init];
    
    ShoppingCartController *shoppingCart=[[ShoppingCartController alloc]init];
    shoppingCart.title=@"购物车";
    [shoppingCart.tabBarItem setImage:[UIImage imageNamed:@"tab_shoppingcart"]];
    [shoppingCart.tabBarItem setSelectedImage:[UIImage imageNamed:@"tab_shoppingcart"]];
    shoppingCartNav=[[UINavigationController alloc]initWithRootViewController:shoppingCart];
//    shoppingCartNav.navigationBar.shadowImage = [[UIImage alloc]init];
    
    userCenter=[[UserCenterController alloc]init];
    userCenter.title=@"个人中心";
    [userCenter.tabBarItem setImage:[UIImage imageNamed:@"tab_usercenter"]];
    [userCenter.tabBarItem setSelectedImage:[UIImage imageNamed:@"tab_usercenter"]];
    userCenterNav=[[UINavigationController alloc]initWithRootViewController:userCenter];
//    userCenterNav.navigationBar.shadowImage = [[UIImage alloc]init];
    userCenterNav.tabBarItem.tag=3;

    
    self.viewControllers=@[homePageNav,categoryNav,shoppingCartNav,userCenterNav];
    //设置一条bar颜色
//    self.tabBar.barTintColor=[UIColor blueColor];
    //设置baritem颜色
   // [self.tabBar setTintColor:MMG_YELLOWCOLOR];
}

#pragma mark - delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
//    if (viewController==userCenter) {
//        if (![AppUtils loginState])
//        {
//            WelcomeController *login=[[WelcomeController alloc]init];
//            [self presentViewController:login animated:YES completion:nil];
//        }
//    }
    
    return YES;
}

/****************************
 *点击到购物车时，发送通知刷新数据
 ****************************/
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (viewController==shoppingCartNav) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
    }
    
    if (viewController==userCenterNav) {
        if (![AppUtils loginState]) {
            //跳到登录页面
            WelcomeController *login=[[WelcomeController alloc]init];
            login.isShop=YES;
            UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
            [loginNav.navigationBar setShadowImage:[UIImage new]];
            //隐藏tabbar
            login.hidesBottomBarWhenPushed = YES;
            [((UINavigationController *)tabBarController.selectedViewController) presentViewController:loginNav animated:YES completion:nil];
        }
    }
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
