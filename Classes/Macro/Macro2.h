//
//  PrefixHeader.pch
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#ifndef DZ100_Macro_h
#define DZ100_Macro_h
#import <Availability.h>

#ifndef __IPHONE_3_0
#warning "This project uses features only available in iOS SDK 3.0 and later."
#endif

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "HttpRequests.h"
#import <UIImageView+AFNetworking.h>
//#import <NJKWebViewProgress.h>
//#import <NJKWebViewProgressView.h>
//#import <JGActionSheet.h>
#import "SingFMDB.h"
#import "UIColor+Hex.h"
#import "UIView+RGSize.h"
#import "UIImage+LK.h"
#import "UIBarButtonItem+Badge.h"
#import "RootViewController.h"
#import "AppUtils.h"
#import "AppDelegate.h"
#import "LoginController.h"
#import "MJRefresh.h"
#import <MBProgressHUD.h>
#import "DOPDropDownMenu.h"
#import "MMSheetView.h"
#import "MMAlertView.h"
#import "MMPopupItem.h"
#import "MMPopupWindow.h"
#import "MMGSign.h"
#import <ShareSDK/ShareSDK.h>
#import <TTTAttributedLabel.h>
#import <XMLDictionary.h>

#define UM_KEY @"56404338e0f55aaa38001363"

#define QQ_APPID @"1104985158"
#define QQ_APPKEY @"CJnmDkuaghdB5s5c"

#define TestHost 0
#define TestDefineControl 1

#define ImageHost @"http://mall.youxia.com/m/"
#define ImageHost01 @"http://mall.youxia.com/"
#define ImageHost03 @"http://mall.youxia.com"
#if TestHost
    #define RONGCLOUD_IM_APPKEY @"y745wfm84z35v"
#else
    #define RONGCLOUD_IM_APPKEY @"y745wfm84z35v"
#endif
//TestFlight

#if TestDefineControl
#define TestFlightDebug 1
//#define NSLog(__FORMAT__, ...) TFLog((@"%s [Line %d] " __FORMAT__), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define UMengTestLog 1
#else
#define TestFlightDebug 0
#define NSLog(...)
#define UMengTestLog 0
#endif

//猫猫购橙色
//#define MMG_YELLOWCOLOR [UIColor colorWithHexString:@"#ea6b1f"]//1172bd
#define MMG_BLUECOLOR [UIColor colorWithHexString:@"#0274BC"]
//#define ISIPHONE5 ([UIScreen mainScreen].bounds.size.height>=568?YES:NO)
#define RongYunIM [NSString stringWithFormat:@"%@/apiforios/im",DefaultHost]

#define IOS7 [[[UIDevice currentDevice] systemVersion]floatValue]>=7
#define NAVIMAGE @"nav_background"
#define NAV_TITLE_FONT_SIZE 17 //导航栏title字体大小

#define SCREENSIZE [UIScreen mainScreen].bounds.size
#define ISIPHONE5 ([UIScreen mainScreen].bounds.size.height==568?YES:NO)
#define ISIPHONE6PLUB ([UIScreen mainScreen].bounds.size.height==736?YES:NO)

//===============
//保存用户信息的标识
//===============

#define User_ID @"userid"                           //用户ID
#define User_Photo @"userphoto"                     //用户头像
#define User_Name @"username"                       //用户名
#define User_Money @"usermoney"                     //用户余额
#define User_Email @"useremail"                     //用户邮箱
#define User_Phone @"userephone"                    //用户手机
#define VIP_Flag @"vipFlag"                         //标识vip

#define Business_Name @"Business_Name"              //
#define Business_Amoubt @"Business_Amoubt"          //
#define Business_GoodsCount @"Business_GoodsCount"  //

#define NotificationUpdatePhoto @"NotificationUpdatePhoto"                      //刷新用户头像

//======================
//保存用户登录的账号密码标识
//======================
#define User_Account @"useraccount"                 //用户账号
#define User_Pass @"userpass"                       //用户密码


#define DICVICE_TOKEN @"divicetoken"    //设备号

#define SERVICEPHONE @"0755-22211222"    //服务电话

#define RESETTIME 60

//iPhone判断方法
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IPHONE5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE6_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

//
#define ALERTVIEW_TITLE nil
#define ALERTVIEW_CANCLE @"取消"
#define ALERTVIEW_CONFIRM @"确定"

#define VIEW_LAYER_CORNERRADIUS 5
#define VIEW_LAYER_BORDERWIDTH 1.0

#define UpDateShoppingCart @"UpDateShoppingCart"
#define NotifitionLogOut @"LogOut"
#define NotifitionLoginSuccess @"LoginSuccess"
#define NotifitionNewsViewWillAppear @"NotifitionNewsViewWillAppear"
#define NotifitionHomePageViewWillAppear @"NotifitionHomePageViewWillAppear"
#define NotifitionShareViewWillAppear @"NotifitionShareViewWillAppear"
#define NotifitionChangeColectButtonState @"NotifitionChangeColectButtonState"

#define NotifitionReceiveIMMessage @"NotifitionReceiveIMMessage"

#define NotifitionPushCount @"NotifitionPushCount"


#define NotifitionGoodDetailGoToShoppingCart @"GoToShoppingCart"

#define PlaceHolderImage50x50 @"login_name"
#define DEFAULTImage @"img_default.png"
#define PlaceHolderImage375x240 @"placeHolder375x240"
#define PlaceHolderImage750x200 @"placeHolder750x200"
#define PlaceHolderImage750x240 @"placeHolder750x240"
#define PlaceHolderImage750x300 @"placeHolder750x300"
#define PlaceHolderImage750x350 @"placeHolder750x350"


//填写信用信息
#define UpdateXCKNotification @"UpdateXCKNotification"

#define UpdateXYKNotification @"UpdateXYKNotification"

#define UpdateContactNotification @"UpdateContactNotification"

#define UpdateTXDZNotification @"UpdateTXDZNotification"

#define UpdateUploadImgCount @"UpdateUploadImgCount"

#define UploadButtonIsNot @"UploadButtonIsNot"

#define SelectedContactNotification @"SelectedContactNotification"

#define SubmitXYRZSuccess @"SubmitXYRZSuccess"


//
#define SelectedCompanyAddressNotification @"SelectedAddressNotification"
#define SelectedHomeAddressNotification @"SelectedHomeAddressNotification"

#import "ShangjiaMacro.h"

/*
 Marion,
 Copperplate,
 "Heiti SC",
 "Iowan Old Style",
 "Courier New",
 "Apple SD Gothic Neo",
 "Heiti TC",
 "Gill Sans",
 "Marker Felt",
 Thonburi,
 "Avenir Next Condensed",
 "Tamil Sangam MN",
 "Helvetica Neue",
 "Gurmukhi MN",
 "Times New Roman",
 Georgia,
 "Apple Color Emoji",
 "Arial Rounded MT Bold",
 Kailasa,
 "Kohinoor Devanagari",
 "Sinhala Sangam MN",
 "Chalkboard SE",
 Superclarendon,
 "Gujarati Sangam MN",
 Damascus,
 Noteworthy,
 "Geeza Pro",
 Avenir,
 "Academy Engraved LET",
 Mishafi,
 Futura,
 Farah,
 "Kannada Sangam MN",
 "Arial Hebrew",
 Arial,
 "Party LET",
 Chalkduster,
 "Hiragino Kaku Gothic ProN",
 "Hoefler Text",
 Optima,
 Palatino,
 "Malayalam Sangam MN",
 "Lao Sangam MN",
 "Al Nile",
 "Bradley Hand",
 "Hiragino Mincho ProN",
 "Trebuchet MS",
 Helvetica,
 Courier,
 Cochin,
 "Devanagari Sangam MN",
 "Oriya Sangam MN",
 "Snell Roundhand",
 "Zapf Dingbats",
 "Bodoni 72",
 Verdana,
 "American Typewriter",
 "Avenir Next",
 Baskerville,
 "Khmer Sangam MN",
 Didot,
 "Savoye LET",
 "Bodoni Ornaments",
 Symbol,
 Menlo,
 "Bodoni 72 Smallcaps",
 "DIN Alternate",
 Papyrus,
 "Euphemia UCAS",
 "Telugu Sangam MN",
 "Bangla Sangam MN",
 Zapfino,
 "Bodoni 72 Oldstyle",
 "DIN Condensed"
 */

#endif

#endif
