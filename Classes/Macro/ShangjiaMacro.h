//
//  ShangjiaMacro.h
//  MMG
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#ifndef ShangjiaMacro_h
#define ShangjiaMacro_h

#define Shangjia_GoodName_Notification @"Shangjia_GoodName_Notification"
#define Shangjia_Fenlei_Notification @"Shangjia_Fenlei_Notification"
#define Shangjia_ErjiFl_Notification @"Shangjia_ErjiFl_Notification"
#define Shangjia_Brance_Notification @"Shangjia_Brance_Notification"
#define Shangjia_UseInfo_Notification @"Shangjia_UseInfo_Notification"
#define Shangjia_GoodType_Notification @"Shangjia_GoodType_Notification"
#define Shangjia_ParamItem_Notification @"Shangjia_ParamItem_Notification"
#define Shangjia_HopePrice_Notification @"Shangjia_HopePrice_Notification"
#define Shangjia_Comment_Notification @"Shangjia_Comment_Notification"

#define Maijia_OrderDetail_GoodsStatusNoti @"Maijia_OrderDetail_GoodsStatusNoti"

#endif /* ShangjiaMacro_h */
