//
//  SingFMDB.m
//  MMG
//
//  Created by LJH on 14-9-26.
//  Copyright (c) 2014年 LJH. All rights reserved.
//

#import "SingFMDB.h"
#import "CityModel.h"

static SingFMDB *singleDataBase=nil;
@implementation SingFMDB
+(SingFMDB *)shareFMDB{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[SingFMDB alloc]init];
        [singleDataBase setUpFMDB];
    });
    return singleDataBase;
}
-(void)setUpFMDB{
    NSString *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    //创建数据库名称
    path=[path stringByAppendingPathComponent:@"mmgdb.sqlite"];
//    NSLog(@"%@",path);
    //连接数据库
    self.db=[FMDatabase databaseWithPath:path];
    if (![self.db open]) {
        NSLog(@"不能打开dataBase !");
    }
    [self.db executeUpdate:@"create table if not exists MMGShoppingCart (good_id integer primary key autoincrement,imageStr text,title text,price text)"];
}

/***********************
 *******我的收藏*********
 ***********************/
#pragma mark - 查询单个
-(ShoppingCartModel *)searchGoodForId:(NSString *)goodId{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from MMGShoppingCart where good_id=?",[NSNumber numberWithInt:goodId.intValue]];
    ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
    while ([result next]) {
        model.goodsId=[result stringForColumn:@"good_id"];
        model.goodsImgStr=[result stringForColumn:@"imageStr"];
        model.goodsTitle=[result stringForColumn:@"title"];
        model.goodsPrice=[result stringForColumn:@"price"];
    }
    [self.db close];
    return model;
}

#pragma mark  查询所有收藏
-(NSMutableArray *)searchAllCollectionGood{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from MMGShoppingCart"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
        model.goodsId=[result stringForColumn:@"good_id"];
        model.goodsImgStr=[result stringForColumn:@"imageStr"];
        model.goodsTitle=[result stringForColumn:@"title"];
        model.goodsPrice=[result stringForColumn:@"price"];
//        NSLog(@"%@-%@-%@-%@",[result stringForColumn:@"good_id"],[result stringForColumn:@"imageStr"],[result stringForColumn:@"title"],[result stringForColumn:@"price"]);
        [arr addObject:model];
    }
    [self.db close];
    return arr;
}
#pragma mark  根据id查询单个
-(BOOL)searchData:(NSString *)goodid{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from MMGShoppingCart where good_id=?",[NSNumber numberWithInt:goodid.intValue]];
    if ([result next]) {
        [self.db close];
        return YES;
    }else{
        [self.db close];
        return NO;
    }
}
#pragma mark  添加数据
-(BOOL)insertMyGood:(NSObject *)model{
    [self.db open];
    ShoppingCartModel *scModel=(ShoppingCartModel *)model;
    [self.db executeUpdate:@"insert into MMGShoppingCart values(?,?,?,?)",
     [NSNumber numberWithInt:scModel.goodsId.intValue],
     scModel.goodsImgStr,
     scModel.goodsTitle,
     scModel.goodsPrice];
    [self.db close];
    return YES;
}
#pragma mark  删除
-(void)deleteMyGood:(NSString *)goodid{
    [self.db open];
    [self.db executeUpdate:@"delete from MMGShoppingCart where good_id=?",goodid];
    [self.db close];
}
#pragma mark  清空收藏
-(void)clearAllCollection{
    [self.db open];
    [self.db executeUpdate:@"delete from MMGShoppingCart"];
    [self.db close];
}
 
 #pragma mark - 查询购物车得到所有价格
-(NSMutableArray *)searchAllPrice{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select price from MMGShoppingCart"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        NSString *priceStr=[result stringForColumn:@"price"];
        [arr addObject:priceStr];
    }
    [self.db close];
    return arr;
}

@end
