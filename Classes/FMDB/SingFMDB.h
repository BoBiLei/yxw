//
//  SingFMDB.h
//  MMG
//
//  Created by LJH on 14-9-26.
//  Copyright (c) 2014年 LJH. All rights reserved.
//
/***************
    本地数据库
 ***************/
#import <Foundation/Foundation.h>
#import "FMDB.h"
#import "ShoppingCartModel.h"
@interface SingFMDB : NSObject
+(SingFMDB *)shareFMDB;

//------购物车-------

/**
 根据id查询一个商品
 */
-(ShoppingCartModel *)searchGoodForId:(NSString *)goodId;

/**
 查询购物车所有商品
 */
-(NSMutableArray *)searchAllCollectionGood;


-(BOOL)searchData:(NSString *)goodid;

/**
 插入一条商品到购物车
 */
-(BOOL)insertMyGood:(NSObject *)model;

/**
 根据id删除购物车商品
 */
-(void)deleteMyGood:(NSString *)goodid;

/**
 清空购物车
 */
-(void)clearAllCollection;

/**
 查询购物车得到所有价格
 */
-(NSMutableArray *)searchAllPrice;

@property (strong,nonatomic) FMDatabase *db;
@property (readonly) NSString *databasePath;
@end
