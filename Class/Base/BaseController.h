//
//  BaseController.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseController : UIViewController

@property (nonatomic,strong) AppDelegate *app;

@property (nonatomic,strong) UIButton *navReturnBtn;

//类方法
+ (instancetype)shareInstance;

-(void)setCustomRootViewNavigationTitle:(NSString *)title;
-(void)setCustomNavigationTitle:(NSString *)title;
-(void)setCustomNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view;
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text;
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image;
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text;
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image;

- (void)turnBack;

- (void)showTipView:(NSString *)message;
//清理webView内存泄漏
- (void)clearWebViewUrlCache;

@end
