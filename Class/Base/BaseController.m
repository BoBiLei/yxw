//
//  BaseController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface BaseController ()<UINavigationControllerDelegate>

@end

@implementation BaseController{
    UIView *callView;
    UIView *_tipView;
    UILabel *_tipLab;
}

-(instancetype)init{
    self=[super init];
    if (self) {
        _app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return self;
}

+ (instancetype)shareInstance {
    static BaseController * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BaseController alloc] init];
    });
    return instance;
}
-(void)setCustomRootViewNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = [UIFont systemFontOfSize:NAV_TITLE_FONT_SIZE];
    navTitle.textColor = [UIColor colorWithHexString:@"#282828"];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
}

-(void)setCustomNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = [UIFont systemFontOfSize:NAV_TITLE_FONT_SIZE];
    navTitle.textColor = [UIColor colorWithHexString:@"#282828"];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,22.f,25.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
}

//-(void)setCustomNavigationTitle:(NSString *)title rightBtn:(NSString *)rightText{
//    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
//    navTitle.backgroundColor = [UIColor clearColor];
//    navTitle.font = [UIFont systemFontOfSize:NAV_TITLE_FONT_SIZE];
//    navTitle.textColor = [UIColor colorWithHexString:@"#282828"];
//    navTitle.text = title;
//    navTitle.textAlignment=NSTextAlignmentCenter;
//    self.navigationItem.titleView = navTitle;
//    
//    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
//    back.frame=CGRectMake(-6, 6,22.f,25.f);
//    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
//    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
//    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
//    self.navReturnBtn=back;
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
//    self.navigationItem.leftBarButtonItem = backItem;
//    
//    //
//    UIButton *right = [UIButton buttonWithType:UIButtonTypeCustom];
//    right.frame=CGRectMake(-6, 6,22.f,25.f);
//    [right setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
//    right.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
//    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
//    self.navReturnBtn=back;
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:back];
//    self.navigationItem.leftBarButtonItem = rightItem;
//}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setCustomNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view{
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,22.f,25.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
    self.navigationItem.titleView = view;
}
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    [btn setTitle:text forState:UIControlStateNormal];
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = btnItem;
    return btn;
}
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image{
    UIButton *imgBtn=[[UIButton alloc]initWithFrame:frame];
    [imgBtn setImage:image forState:UIControlStateNormal];
    UIBarButtonItem *addLItem = [[UIBarButtonItem alloc] initWithCustomView:imgBtn];
    self.navigationItem.leftBarButtonItem = addLItem;
    return imgBtn;
}

-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    btn.titleLabel.font=[UIFont systemFontOfSize:16];
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = btnItem;
    return btn;
}
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image{
    UIButton *imgBtn=[[UIButton alloc]initWithFrame:frame];
    [imgBtn setImage:image forState:UIControlStateNormal];
    imgBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [imgBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -16)];
    UIBarButtonItem *addRItem = [[UIBarButtonItem alloc] initWithCustomView:imgBtn];
    self.navigationItem.rightBarButtonItem = addRItem;
    return imgBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    self.view.backgroundColor=[UIColor whiteColor];
    //滑动返回
//    [self setTabBarHidden:YES];
//    self.rdv_tabBarController.delegate = self;
    //滑动返回
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending){
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        }
    }
}
- (void)initTipView{
    if (!_tipView) {
        _tipView = [[UIView alloc] initWithFrame:CGRectMake(SCREENSIZE.width/2.0-70, SCREENSIZE.height/2.0-80, 140, 40)];
        [_tipView.layer setCornerRadius:5.0];
        _tipView.backgroundColor = [UIColor blackColor];
        [_tipView setAlpha:0];
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate.window addSubview:_tipView];
        [delegate.window bringSubviewToFront:_tipView];
    }
    if (!_tipLab) {
        _tipLab = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 130, 0)];
        _tipLab.text = @"";
        _tipLab.numberOfLines = 0;
        _tipLab.lineBreakMode= NSLineBreakByWordWrapping;
        _tipLab.font = [UIFont systemFontOfSize:13];
        _tipLab.textColor = [UIColor whiteColor];
        //[_tipLab setContentMode:UIViewContentModeScaleAspectFit];
        [_tipLab setTextAlignment:NSTextAlignmentCenter];
        [_tipView addSubview:_tipLab];
    }
}
-(CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [_tipLab.text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}
- (void)showTipView:(NSString *)message {
    
//    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [delegate showTipView:message];
    });
    
    //    if (message) {
    //        _tipLab.text = message;
    //        CGSize size = [self sizeWithFont:[UIFont systemFontOfSize:13] maxSize:CGSizeMake(130, MAXFLOAT)];
    //        _tipView.viewHeight = size.height+20;
    //        _tipLab.viewHeight = size.height;
    //    }
    //    [UIView animateWithDuration:0.5 animations:^{
    //        _tipView.alpha = 1;
    //    } completion:^(BOOL finished) {
    //        [UIView animateWithDuration:1 animations:^{
    //            _tipView.alpha = 0;
    //        }];
    //    }];
}
- (void)viewWillAppear:(BOOL)animated {
    //
    [super viewWillAppear:YES];
//    self.rdv_tabBarController.delegate = self;
    
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    
}

//在键盘上附加工具栏
//-(UIView *)inputAccessoryView{
//    CGRect accessFrame = CGRectMake(0, 0, SCREENSIZE.width, 44);
//    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:accessFrame];
//    UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-60, 4, 40, 40)];
//    [closeBtn setTitle:@"收起" forState:UIControlStateNormal];
//    [closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
//    [closeBtn addTarget:self action:@selector(closeBeyBoard) forControlEvents:UIControlEventTouchDown];
//    [toolbar addSubview:closeBtn];
//    return toolbar;
//}
-(void)closeBeyBoard{
    [AppUtils closeKeyboard];
}

-(void)tapBlackView{
    //    isSyBtnClick=YES;
    [UIView animateWithDuration:.2 animations:^{
        [callView removeFromSuperview];
        //        callView.frame=CGRectMake(0, -55, MAINSCREEN.width, 55);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    // Dispose of any resources that can be recreated.
}
- (void)clearWebViewUrlCache {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitOfflineWebApplicationCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - navigation delegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{

}
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

//- (UIInterfaceOrientationMask)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController NS_AVAILABLE_IOS(7_0){
//
//}
//- (UIInterfaceOrientation)navigationControllerPreferredInterfaceOrientationForPresentation:(UINavigationController *)navigationController NS_AVAILABLE_IOS(7_0){
//    
//}

@end
