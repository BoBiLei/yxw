//
//  UseCouponController.m
//  MMG
//
//  Created by mac on 15/10/22.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "UseCouponController.h"
#import "UseCopuonCell.h"

@interface UseCouponController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation UseCouponController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    NSIndexPath *selectedIndexPath;
    
    UITextField *numberText;
    UITextField *passText;
    UIButton *useBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self setCustomNavigationTitle:@"使用优惠券"];
    
    [self requestHttpsForCouponList];
}

#pragma mark - init UI
-(void)setUpUI{
    self.selectedCouponId=@"0";
    //table
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"UseCopuonCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:myTable];
    
    //添加新优惠券
    UIView *addCouonView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 186)];
    addCouonView.backgroundColor=[UIColor whiteColor];
    
    //
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, addCouonView.frame.size.width, 34)];
    topView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    [addCouonView addSubview:topView];
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width, topView.frame.size.height)];
    label.font=[UIFont systemFontOfSize:14];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    label.text=@"使用新优惠券";
    [topView addSubview:label];
    
    //line
    UIView *line=[UIView new];
    line.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [addCouonView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView][line(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topView,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //numberView
    UIView *numberView=[UIView new];
    numberView.layer.cornerRadius=2;
    numberView.layer.borderWidth=0.5f;
    numberView.layer.borderColor=[UIColor colorWithHexString:@"#e6e6e6"].CGColor;
    [addCouonView addSubview:numberView];
    numberView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* numberView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[numberView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberView)];
    [NSLayoutConstraint activateConstraints:numberView_h];
    
    NSArray* numberView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line]-8-[numberView(38)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,numberView)];
    [NSLayoutConstraint activateConstraints:numberView_w];
    
    //
    UILabel *numberLabel=[UILabel new];
    numberLabel.text=@"优惠券号";
    numberLabel.font=[UIFont systemFontOfSize:14];
    numberLabel.textColor=[UIColor lightGrayColor];
    [numberView addSubview:numberLabel];
    numberLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* numberLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-4-[numberLabel(58)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberLabel)];
    [NSLayoutConstraint activateConstraints:numberLabel_h];
    
    NSArray* numberLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[numberLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberLabel)];
    [NSLayoutConstraint activateConstraints:numberLabel_w];
    
    numberText=[UITextField new];
    numberText.placeholder=@"输入优惠券卡号";
    numberText.font=[UIFont systemFontOfSize:13];
    numberText.tag=0;
    numberText.clearButtonMode=UITextFieldViewModeWhileEditing;
    [numberText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    numberText.font=[UIFont systemFontOfSize:15];
    numberText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [numberView addSubview:numberText];
    numberText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* numberText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[numberLabel]-8-[numberText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberLabel,numberText)];
    [NSLayoutConstraint activateConstraints:numberText_h];
    
    NSArray* numberText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[numberText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberText)];
    [NSLayoutConstraint activateConstraints:numberText_w];
    
    //passView
    UIView *passView=[UIView new];
    passView.layer.cornerRadius=2;
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e6e6e6"].CGColor;
    [addCouonView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[numberView]-8-[passView(numberView)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(numberView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[UILabel new];
    passLabel.text=@"密    码";
    passLabel.font=[UIFont systemFontOfSize:14];
    passLabel.textColor=[UIColor lightGrayColor];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-4-[passLabel(58)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    passText=[UITextField new];
    passText.placeholder=@"输入卡号密码";
    passText.font=[UIFont systemFontOfSize:13];
    passText.tag=0;
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.secureTextEntry=YES;
    [passText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.font=[UIFont systemFontOfSize:15];
    passText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-8-[passText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //useBtn
    useBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    useBtn.layer.cornerRadius=2;
    [useBtn setTitle:@"使用新优惠券" forState:UIControlStateNormal];
    [useBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    useBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [addCouonView addSubview:useBtn];
    [useBtn addTarget:self action:@selector(clickUseBtn) forControlEvents:UIControlEventTouchUpInside];
    useBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* useBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[useBtn]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(useBtn)];
    [NSLayoutConstraint activateConstraints:useBtn_h];
    
    NSArray* useBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[passView]-8-[useBtn]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView,useBtn)];
    [NSLayoutConstraint activateConstraints:useBtn_w];
    useBtn.enabled=NO;
    useBtn.backgroundColor=[UIColor lightGrayColor];
    
    myTable.tableFooterView=addCouonView;
}

-(void)phoneDidChange{
    if (![numberText.text isEqualToString:@""]&&![passText.text isEqualToString:@""]) {
        useBtn.enabled=YES;
        useBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    }else{
        useBtn.enabled=NO;
        useBtn.backgroundColor=[UIColor lightGrayColor];
    }
}

#pragma mark - 点击使用新优惠券按钮事件
-(void)clickUseBtn{
    [AppUtils closeKeyboard];
    [self requestHttpsForAddNewCoupon:numberText.text pass:passText.text];
}

#pragma mark - TableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 86;
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 86)];
    headerSectionView.backgroundColor=[UIColor whiteColor];
    
    UIView *ttView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 10)];
    ttView.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [headerSectionView addSubview:ttView];
    //
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 10, headerSectionView.frame.size.width, 34)];
    topView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    [headerSectionView addSubview:topView];
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width, topView.frame.size.height)];
    label.font=[UIFont systemFontOfSize:14];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    label.text=@"已绑定优惠券";
    [topView addSubview:label];
    
    //line
    UIView *line=[UIView new];
    line.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [headerSectionView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView][line(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topView,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //label
    UILabel *topLabel=[UILabel new];
    topLabel.numberOfLines=0;
    topLabel.text=@"提示：每个订单可使用一张优惠券，且优惠券支付金额不能超过订单总价的10%";
    topLabel.textColor=[UIColor lightGrayColor];
    topLabel.font=[UIFont systemFontOfSize:13];
    [headerSectionView addSubview:topLabel];
    topLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* topLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[topLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topLabel)];
    [NSLayoutConstraint activateConstraints:topLabel_h];
    
    NSArray* topLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line][topLabel]-0.5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,topLabel)];
    [NSLayoutConstraint activateConstraints:topLabel_w];
    
    //bottom line
    UIView *btmLine=[UIView new];
    btmLine.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [headerSectionView addSubview:btmLine];
    btmLine.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* btmLine_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[btmLine]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btmLine)];
    [NSLayoutConstraint activateConstraints:btmLine_h];
    
    NSArray* btmLine_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLabel][btmLine(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(topLabel,btmLine)];
    [NSLayoutConstraint activateConstraints:btmLine_w];
        
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UseCopuonCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (!cell) {
        cell=[[UseCopuonCell alloc]init];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UseCouponModel *model=dataArr[indexPath.row];
    cell.couponId=model.couponID;
    if (indexPath.row==dataArr.count-1) {
        cell.title.text=[NSString stringWithFormat:@"%@",model.couponMoney];
        cell.validate.text=nil;
    }else{
        cell.title.text=[NSString stringWithFormat:@"面值：%@元",model.couponMoney];
        cell.validate.text=[NSString stringWithFormat:@"有效期%@",model.couponEndTime];
    }
    
    /*
    if (model.isSelectedCoupon) {
        [cell.img setImage:[UIImage imageNamed:@"allBtn_sele"]];
        selectedIndexPath=indexPath;
    }else{
        [cell.img setImage:[UIImage imageNamed:@"allBtn_nor"]];
    }*/
    if ([model.couponID isEqualToString:self.selectedCouponId]) {
        [cell.img setImage:[UIImage imageNamed:@"allBtn_sele"]];
        selectedIndexPath=indexPath;
    }else{
        [cell.img setImage:[UIImage imageNamed:@"allBtn_nor"]];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedIndexPath) {
        UseCopuonCell *cell=(UseCopuonCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
        [cell.img setImage:[UIImage imageNamed:@"allBtn_nor"]];
    }
    UseCopuonCell *cell=(UseCopuonCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell.img setImage:[UIImage imageNamed:@"allBtn_sele"]];
    selectedIndexPath = indexPath;
    UseCouponModel *model=dataArr[selectedIndexPath.row];
    self.selectedCouponId=model.couponID;
    if (self.kCouponBlock) {
        self.kCouponBlock(model);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request请求
//(优惠券列表)
-(void)requestHttpsForCouponList{
    dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"iso_check_bouns" forKey:@"step"];
    [dict setObject:self.goodPrice forKey:@"goods_price"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCouponList];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]){
                NSArray *arr=responseObject[@"retData"][@"bonus_list"];
                for (NSDictionary *dic in arr) {
                    UseCouponModel *model=[[UseCouponModel alloc]init];
                    [model jsonDataForDictionay:dic];
                    [dataArr addObject:model];
                }
            }
            
            [self setUpUI];
            
            //添加一个不使用的model
            UseCouponModel *model=[[UseCouponModel alloc]init];
            model.couponID=@"0";
            model.couponMoney=@"不使用";
            model.couponEndTime=@"0";
            model.isSelectedCoupon=YES;
            [dataArr addObject:model];
            
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//(使用新优惠券)
-(void)requestHttpsForAddNewCoupon:(NSString *)numberStr pass:(NSString *)pass{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"bind_bonus" forKey:@"act"];
    [dict setObject:numberStr forKey:@"bonus_sn"];
    [dict setObject:pass forKey:@"bonus_passwd"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForAddNewCoupon:numberText.text pass:passText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]){
                
                //使用优惠券成功后处理（并pop回到上一个ctr）
                UseCouponModel *model=[[UseCouponModel alloc]init];
                [model jsonDataForDictionay:responseObject[@"retData"]];
                self.selectedCouponId=model.couponID;
                [dataArr addObject:model];
                if (self.kCouponBlock) {
                    self.kCouponBlock(model);
                }
                [myTable reloadData];
                numberText.text=@"";
                passText.text=@"";
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
