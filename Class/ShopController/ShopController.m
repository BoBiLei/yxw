//
//  ShopController.m
//  MMG
//
//  Created by mac on 16/7/5.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "ShopController.h"
#import "ShopHeaderCell.h"
#import "ShopCell.h"
#import "ShopReusableView.h"
#import "ProductDetailController.h"
#import "ProductListController.h"
#import "FillterViewController.h"
#import "FillterIDModel.h"
#import "SectionHeadercCollectionViewLayout.h"
#import "ShopAthorReusableView.h"
#import "ShopMarginCell.h"
#import "NewUpGoodsCell.h"

@interface ShopController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ClickShopReusableDelegate>

@end

@implementation ShopController{
    
    UIButton *fillterButton;
    
    NSString *rasuTagStr;      //0-店铺首页  1-全部商品 2-商品上架
    
    FillterViewController *fillterViewContr; //筛选页面
    MJRefreshAutoNormalFooter *footer;
    
    NSString *moreDataStr; //保存下拉加载跟多的链接
    
    //赛选字段
    NSString *fkeywork;
    NSString *fCategory01;
    NSString *fCategory02;
    NSString *fBrand;
    NSString *fMin_price;
    NSString *fMax_price;
    
    //
    UIView *searchViw;
    UITextField *searchTextField;
    NSDictionary *headDataDic;
    UICollectionView *myCollection;
    SectionHeadercCollectionViewLayout *myLayout;
    
    //
    NSMutableArray *hotSellArr;     //热卖
    NSMutableArray *renqiArr;       //人气
    NSMutableArray *allGoodsArr;    //全部商品
    NSMutableArray *upNewGoodsArr;  //新品上架
    
    NSString *typeStr;
    NSString *orderStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFillterData:) name:@"Business_REFLUSH_FILLTERDATA" object:nil];
    
    [self setCustomNavigationTitleViewSetFrame:CGRectMake(0, 0, SCREENSIZE.width-140, 44) titleView:[self setUpSearchView]];
    
    [self setUpFillterBtn];
    
    fillterViewContr=[[FillterViewController alloc]init];
    fillterViewContr.isBusinessFillter=YES;
    
    [self setUpUI];
    
    [self shopHomepageRequest];
    
}

#pragma mark - Notification Method
-(void)updateFillterData:(NSNotification *)noti{
    FillterIDModel *model=noti.object;
    [self requestHttpsFilterViewDataForModel:model];
}

#pragma mark - setUp UI
-(void)setUpUI{
    rasuTagStr=@"0";
    fkeywork=@"";
    fBrand=@"0";
    fCategory01=@"0";
    fCategory02=@"0";
    fMin_price=@"0";
    fMax_price=@"0";
    
    typeStr=@"g.add_time";
    orderStr=@"DESC";
    
    //Collection
    myLayout= [[SectionHeadercCollectionViewLayout alloc]init];
    myLayout.floatSection=1;
    myCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:myLayout];
    [myCollection registerNib:[UINib nibWithNibName:@"ShopHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"ShopHeaderCell"];
    //
    [myCollection registerNib:[UINib nibWithNibName:@"ShopMarginCell" bundle:nil] forCellWithReuseIdentifier:@"ShopMarginCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"ShopCell" bundle:nil] forCellWithReuseIdentifier:@"ShopCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"NewUpGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"NewUpGoodsCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"ShopReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusablecell"];
    [myCollection registerNib:[UINib nibWithNibName:@"ShopAthorReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusablecell02"];
    myCollection.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    myCollection.dataSource=self;
    myCollection.delegate=self;
    [self.view addSubview:myCollection];
    myCollection.mj_footer = nil;
}

//搜索的View
-(UIView *)setUpSearchView{
    searchViw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-150, 44)];
    searchViw.backgroundColor=[UIColor clearColor];
    [self.view addSubview:searchViw];
    
    
    UIView *insideSearchView=[[UIView alloc]initWithFrame:CGRectMake(0,4, searchViw.width, 36)];
    insideSearchView.layer.borderWidth=.8;
    insideSearchView.layer.cornerRadius=1;
    insideSearchView.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    [searchViw addSubview:insideSearchView];
    
    //searBtn
    UIButton *searBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    searBtn.frame=CGRectMake(insideSearchView.width-40, 0, 40, insideSearchView.height);
    searBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [searBtn setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    searBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [insideSearchView addSubview:searBtn];
    [searBtn addTarget:self action:@selector(clickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //searchTextField
    searchTextField=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, insideSearchView.frame.size.width-50, insideSearchView.frame.size.height)];
    searchTextField.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    searchTextField.placeholder=@"Birkin";
    searchTextField.font=[UIFont systemFontOfSize:14];
    searchTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    [insideSearchView addSubview:searchTextField];
    
    return searchViw;
}

#pragma mark - 点击搜索按钮
-(void)clickSearchBtn{
    if ([searchTextField.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入搜索内容" inView:self.view];
    }else{
        ProductListController *prodContr=[[ProductListController alloc]init];
        prodContr.requestType=HomePageSearchRequestType;
        prodContr.searchKeyWord=searchTextField.text;
        prodContr.productListName=[NSString stringWithFormat:@"%@",searchTextField.text];
        prodContr.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:prodContr animated:YES];
    }
    [AppUtils closeKeyboard];
}

-(void)setUpFillterBtn{
    
    fillterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fillterButton.hidden=YES;
    fillterButton.frame = CGRectMake(0, 4, 55 , 36);
    fillterButton.backgroundColor = [UIColor colorWithHexString:@"#ec6b00"];
    [fillterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [fillterButton setTitle:@"筛选" forState:UIControlStateNormal];
    fillterButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [fillterButton addTarget:self action:@selector(clickFilterBtn) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -7;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:fillterButton];
    self.navigationItem.rightBarButtonItems = @[negativeSpacer, backItem];
}

#pragma mark - 点击筛选按钮 GO
-(void)clickFilterBtn{
    [self.navigationController pushViewController:fillterViewContr animated:YES];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if ([rasuTagStr isEqualToString:@"0"]) {
        return 4;
    }else{
        return 2;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else if(section==1){
        if ([rasuTagStr isEqualToString:@"0"]) {
            return 1;
        }else if ([rasuTagStr isEqualToString:@"1"]) {
            return allGoodsArr.count;
        }else{
            return upNewGoodsArr.count;
        }
    }else if(section==2){
        return hotSellArr.count;
    }else{
        return renqiArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ShopHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ShopHeaderCell" forIndexPath:indexPath];
        cell.model=headDataDic;
        return cell;
    }else if(indexPath.section==1){
        if([rasuTagStr isEqualToString:@"0"]){
            NewUpGoodsCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewUpGoodsCell" forIndexPath:indexPath];
            return cell;
        }
        else if([rasuTagStr isEqualToString:@"1"]){
            ShopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ShopCell" forIndexPath:indexPath];
            if (indexPath.row<allGoodsArr.count) {
                ProductListModel *model=allGoodsArr[indexPath.row];
                [cell reflushDataForModel:model];
            }
            return cell;
        }else{
            if (indexPath.row==0) {
                NewUpGoodsCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewUpGoodsCell" forIndexPath:indexPath];
                ProductListModel *model=upNewGoodsArr[0];
                [cell reflushDataForModel:model];
                return cell;
            }else{
                ShopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ShopCell" forIndexPath:indexPath];
                if (indexPath.row<upNewGoodsArr.count) {
                    ProductListModel *model=upNewGoodsArr[indexPath.row];
                    [cell reflushDataForModel:model];
                }
                return cell;
            }
        }
        
    }else if(indexPath.section==2){
        ShopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ShopCell" forIndexPath:indexPath];
        if (indexPath.row<hotSellArr.count) {
            ProductListModel *model=hotSellArr[indexPath.row];
            [cell reflushDataForModel:model];
        }
        return cell;
    }else{
        ShopCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"ShopCell" forIndexPath:indexPath];
        if (indexPath.row<hotSellArr.count) {
            ProductListModel *model=renqiArr[indexPath.row];
            [cell reflushDataForModel:model];
        }
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width*260/550+68);
    }else if(indexPath.section==1){
        if ([rasuTagStr isEqualToString:@"0"]) {
            return CGSizeMake(SCREENSIZE.width, 0.00000000001);
        }else if([rasuTagStr isEqualToString:@"1"]){
            return CGSizeMake((SCREENSIZE.width)/2-14, (SCREENSIZE.width)/2-14+88);
        }else{
            if(indexPath.row==0){
                return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width-58);
            }else{
                return CGSizeMake((SCREENSIZE.width)/2-14, (SCREENSIZE.width)/2-14+88);
            }
        }
    }else{
        return CGSizeMake((SCREENSIZE.width)/2-14, (SCREENSIZE.width)/2-14+88);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==0) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else if(section==1){
        if ([rasuTagStr isEqualToString:@"0"]) {
            return UIEdgeInsetsMake(0, 0, 0, 0);
        }else if([rasuTagStr isEqualToString:@"1"]){
            return UIEdgeInsetsMake(0, 9, 9, 9);
        }else{
            return UIEdgeInsetsMake(-10, 9, 9, 9);
            //            return UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }else{
        return UIEdgeInsetsMake(9, 9, 9, 9);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductListModel *model;
    if([rasuTagStr isEqualToString:@"0"]){
        if (indexPath.section==2) {
            model=hotSellArr[indexPath.row];
            ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
            prodectDetail.goodId=model.idStr;
            prodectDetail.goodTitle=model.titleStr;
            prodectDetail.goodImgStr=model.imgStr;
            prodectDetail.goodPrice=model.mmPriceStr;
            [self.navigationController pushViewController:prodectDetail animated:YES];
        }else if(indexPath.section==3){
            model=renqiArr[indexPath.row];
            ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
            prodectDetail.goodId=model.idStr;
            prodectDetail.goodTitle=model.titleStr;
            prodectDetail.goodImgStr=model.imgStr;
            prodectDetail.goodPrice=model.mmPriceStr;
            NSLog(@"%@--------",model.imgStr);
            [self.navigationController pushViewController:prodectDetail animated:YES];
        }
    }else if([rasuTagStr isEqualToString:@"1"]){
        if (indexPath.section==1) {
            model=allGoodsArr[indexPath.row];
            ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
            prodectDetail.goodId=model.idStr;
            prodectDetail.goodTitle=model.titleStr;
            prodectDetail.goodImgStr=model.imgStr;
            prodectDetail.goodPrice=model.mmPriceStr;
            [self.navigationController pushViewController:prodectDetail animated:YES];
        }
    }else if ([rasuTagStr isEqualToString:@"2"]){
        if (indexPath.section==1) {
            model=upNewGoodsArr[indexPath.row];
            ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
            prodectDetail.goodId=model.idStr;
            prodectDetail.goodTitle=model.titleStr;
            prodectDetail.goodImgStr=model.imgStr;
            prodectDetail.goodPrice=model.mmPriceStr;
            [self.navigationController pushViewController:prodectDetail animated:YES];
        }
    }
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return CGSizeMake(0, 0);
    }else if(section==1){
        if ([rasuTagStr isEqualToString:@"1"]) {
            return CGSizeMake(SCREENSIZE.width,118);
        }else{
            return CGSizeMake(SCREENSIZE.width,64);
        }
    }else if(section==2||section==3){
        if ([rasuTagStr isEqualToString:@"0"]) {
            return CGSizeMake(SCREENSIZE.width, 42);
        }else{
            return CGSizeMake(0, 0);
        }
    }else{
        return CGSizeMake(0, 0);
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section==1){
            ShopReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusablecell" forIndexPath:indexPath];
            hpReusableView.delegate=self;
            return hpReusableView;
        }else if(indexPath.section==2){
            if ([rasuTagStr isEqualToString:@"0"]) {
                ShopAthorReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusablecell02" forIndexPath:indexPath];
                NSString *showStr=@"当季新品";
                hpReusableView.showLabel.text=showStr;
                return hpReusableView;
            }else{
                ShopAthorReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusablecell02" forIndexPath:indexPath];
                return hpReusableView;
            }
        }else if(indexPath.section==3){
            if ([rasuTagStr isEqualToString:@"0"]) {
                ShopAthorReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusablecell02" forIndexPath:indexPath];
                NSString *showStr=@"人气商品";
                hpReusableView.showLabel.text=showStr;
                return hpReusableView;
            }else{
                ShopAthorReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusablecell02" forIndexPath:indexPath];
                return hpReusableView;
            }
        }else{
            return nil;
        }
    }
    return nil;
}

#pragma mark - reusbleview Delegate
-(void)clickBtnWithTag:(NSString *)str{
    rasuTagStr=str;
    if([str isEqualToString:@"0"]){
        myCollection.mj_footer = nil;
        fillterButton.hidden=YES;
        [self shopHomepageRequest];
    }else if ([str isEqualToString:@"1"]){
        //加载更多
        footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [self refreshForPullUp];
        }];
        footer.stateLabel.font = [UIFont systemFontOfSize:14];
        footer.stateLabel.textColor = [UIColor lightGrayColor];
        [footer setTitle:@"" forState:MJRefreshStateIdle];
        [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
        [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
        // 忽略掉底部inset
        footer.triggerAutomaticallyRefreshPercent=0.5;
        myCollection.mj_footer = footer;
        fillterButton.hidden=NO;
        [self allGoodsRequestForType:typeStr orderBy:orderStr];
    }else{
        myCollection.mj_footer = nil;
        fillterButton.hidden=YES;
        [self upNewGoodsRequest];
    }
}

- (void)refreshForPullUp{
    [self requestHttpsMoreProduct:moreDataStr];
}

-(void)clickFillterWithType:(NSString *)type sort:(NSString *)sort{
    typeStr=type;
    orderStr=sort;
    [self allGoodsRequestForType:typeStr orderBy:orderStr];
}

#pragma mark - Request
//店铺首页
-(void)shopHomepageRequest{
    [AppUtils showProgressInView:self.view];
    
    hotSellArr=[NSMutableArray array];
    renqiArr=[NSMutableArray array];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"shop_index" forKey:@"act"];
    [dict setObject:_dp_id forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"shop.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            headDataDic=responseObject[@"retData"][@"user_business"];
            
            NSArray *hotArr=responseObject[@"retData"][@"goods_list"][@"hot_goods_list"];
            if (hotArr.count>0) {
                for (NSDictionary *dic in hotArr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [hotSellArr addObject:model];
                }
            }
            
            NSArray *rqArr=responseObject[@"retData"][@"goods_list"][@"new_goods_list"];
            if (rqArr.count>0) {
                for (NSDictionary *dic in rqArr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [renqiArr addObject:model];
                }
            }
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissHUDInView:self.view];
    }];
}

//新品上架
-(void)upNewGoodsRequest{
    [AppUtils showProgressInView:self.view];
    
    upNewGoodsArr=[NSMutableArray array];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"new_goods" forKey:@"act"];
    [dict setObject:_dp_id forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"shop.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            headDataDic=responseObject[@"retData"][@"user_business"];
            
            NSArray *arr=responseObject[@"retData"][@"goods_list"];
            if (arr.count>0) {
                for (NSDictionary *dic in arr) {
                    ProductListModel *model=[[ProductListModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [upNewGoodsArr addObject:model];
                }
            }
        }
        if (upNewGoodsArr.count!=0) {
            myLayout.floatSection=1;
        }else{
            myLayout.floatSection=0;
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissHUDInView:self.view];
    }];
}

//更多请求
-(void)requestHttpsMoreProduct:(NSString *)moreHref{
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:moreHref] method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [allGoodsArr addObject:model];
            }
            
            if (arr.count!=0) {
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
        }else{
            [footer endRefreshingWithNoMoreData];
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//赛选请求（默认、最新、价格）
-(void)allGoodsRequestForType:(NSString *)type orderBy:(NSString *)orderBy{
    [AppUtils showProgressInView:self.view];
    allGoodsArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"keywords":fkeywork,
                         @"category":fCategory01,
                         @"category2":fCategory02,
                         @"brand":fBrand,
                         @"min_price":fMin_price,
                         @"max_price":fMax_price,
                         @"jicun_id":_dp_id,
                         @"sort":type,
                         @"order":orderBy
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"search.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            
            headDataDic=responseObject[@"retData"][@"business"];
            
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [allGoodsArr addObject:model];
            }
            fkeywork=responseObject[@"retData"][@"keywords"];
            fCategory01=responseObject[@"retData"][@"category"];
            fCategory02=responseObject[@"retData"][@"category2"];
            fBrand=responseObject[@"retData"][@"brand"];
            fMin_price=responseObject[@"retData"][@"min_price"];
            fMax_price=responseObject[@"retData"][@"max_price"];
        }
        if (allGoodsArr.count!=0) {
            myLayout.floatSection=1;
        }else{
            myLayout.floatSection=0;
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

//筛选页面返回时赛选请求
-(void)requestHttpsFilterViewDataForModel:(FillterIDModel *)model{
    [AppUtils showProgressInView:self.view];
    if (allGoodsArr.count>0) {
        [allGoodsArr removeAllObjects];
    }
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"keywords":fkeywork,
                         @"category":model.categoryId,
                         @"category2":model.subCategoryId,
                         @"brand":model.brandId,
                         @"min_price":model.min_priceId,
                         @"max_price":model.max_priceId,
                         @"jicun_id":_dp_id,
                         @"sort":typeStr,
                         @"order":orderStr
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"search.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            moreDataStr=responseObject[@"retData"][@"ajax_url_format"];
            NSArray *arr=responseObject[@"retData"][@"good_list"];
            for (NSDictionary *dic in arr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForDictionary:dic];
                [allGoodsArr addObject:model];
            }
            fkeywork=responseObject[@"retData"][@"keywords"];
            fCategory01=responseObject[@"retData"][@"category"];
            fCategory02=responseObject[@"retData"][@"category2"];
            fBrand=responseObject[@"retData"][@"brand"];
            fMin_price=responseObject[@"retData"][@"min_price"];
            fMax_price=responseObject[@"retData"][@"max_price"];
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
