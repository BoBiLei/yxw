//
//  UserCollectionController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "UserCollectionController.h"
#import "CollectionCell.h"
#import "ProductDetailController.h"

NSString *colleidentifier=@"collctioncell";
@interface UserCollectionController ()<UITableViewDataSource,UITableViewDelegate,CollectionDelegate>

@end

@implementation UserCollectionController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    CollectionModel *seleModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"我的收藏"];
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self requestHttpsForUserId:[AppUtils getValueWithKey:User_ID]];
}

#pragma mark - init UI
-(void)setUpUI{
    if (dataArr.count>0) {
        myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
        myTable.dataSource=self;
        myTable.delegate=self;
        [myTable registerNib:[UINib nibWithNibName:@"CollectionCell" bundle:nil] forCellReuseIdentifier:colleidentifier];
        [self.view addSubview:myTable];
        
        UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
        bagView.userInteractionEnabled=YES;
        bagView.image=[UIImage imageNamed:@"login_baground"];
        myTable.backgroundView=bagView;
        myTable.tableFooterView=[[UIView alloc]init];
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UILabel class]]) {
                [view removeFromSuperview];
            }
        }
    }else{
        UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 21)];
        tipLabel.text=@"我的收藏为空";
        tipLabel.textColor=[UIColor lightGrayColor];
        tipLabel.textAlignment=NSTextAlignmentCenter;
        tipLabel.font=[UIFont systemFontOfSize:16];
        tipLabel.center=CGPointMake(SCREENSIZE.width/2, (SCREENSIZE.height-113)/2);
        [self.view addSubview:tipLabel];
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UITableView class]]) {
                [view removeFromSuperview];
            }
        }
    }
}

#pragma mark - TableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 87;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CollectionCell *cell=[tableView dequeueReusableCellWithIdentifier:colleidentifier];
    if (!cell) {
        cell=[[CollectionCell alloc]init];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.delegate=self;
    
    //判断indexPath.row<dataArr.count增加健壮性
    if(indexPath.row<dataArr.count){
        CollectionModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CollectionModel *model=dataArr[indexPath.row];
    ProductDetailController *product=[[ProductDetailController alloc]init];
    product.goodId=model.goodsId;
    [self.navigationController pushViewController:product animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - 自定义delegate
//立即购买
-(void)buyForModel:(CollectionModel *)model{
    
    //点击后跳转到购物车
    //1、发送通知以改变tabbar 的 seleteIndex
    //2、popViewController
    //==============
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionGoodDetailGoToShoppingCart object:nil];
    SingFMDB *db=[SingFMDB shareFMDB];
    BOOL isExit=[db searchData:model.goodsId];
    if (!isExit) {
        [db insertMyGood:model];
    }else{
        NSLog(@"添加失败，购物车已有");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

//取消收藏
-(void)deleteForModel:(CollectionModel *)model{
    seleModel=model;
    [self requestHttpsForCollectionId:seleModel];
}

#pragma mark - Request 
//收藏列表
-(void)requestHttpsForUserId:(NSString *)userId{
    dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"collection_list" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForUserId:[AppUtils getValueWithKey:User_ID]];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSArray *arr=responseObject[@"retData"][@"goods_list"];
                for (NSDictionary *dic in arr) {
                    CollectionModel *model=[[CollectionModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                [self setUpUI];
            }
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//取消收藏请求
-(void)requestHttpsForCollectionId:(CollectionModel *)model{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"delete_collection" forKey:@"act"];
    [dict setObject:model.collectionId forKey:@"collection_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCollectionId:seleModel];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                /********************************
                 *正确remove model的方法
                 ********************************/
                //            NSArray *array=[NSArray arrayWithArray:dataArr];
                //            for (id obj in array) {
                //                [dataArr removeObject:obj];
                //            }
                [self requestHttpsForUserId:[AppUtils getValueWithKey:User_ID]];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
