//
//  SearchBar.m
//  UISearchBarCustom
//
//  Created by gamin on 15/4/30.
//  Copyright (c) 2015年 gamin. All rights reserved.
//

#import "SearchBar.h"

@implementation SearchBar
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.tintColor = [UIColor whiteColor];
        self.layer.cornerRadius=2;
        self.layer.borderWidth=0.5f;
        self.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    }

    return self;
}

-(void) layoutSubviews{
    [super layoutSubviews];
    
    UITextField *searchField;
    NSArray *subviewArr = self.subviews;
    for(int i = 0; i < subviewArr.count ; i++) {
        UIView *viewSub = [subviewArr objectAtIndex:i];
        NSArray *arrSub = viewSub.subviews;
        for (int j = 0; j < arrSub.count ; j ++) {
            id tempId = [arrSub objectAtIndex:j];
            if([tempId isKindOfClass:[UITextField class]]) {
                searchField = (UITextField *)tempId;
            }
        }
    }
    
    //自定义UISearchBar
    if(searchField) {
        searchField.placeholder = @"输入要查找的关键字";
        [searchField setBorderStyle:UITextBorderStyleRoundedRect];
        [searchField setBackgroundColor:[UIColor whiteColor]];
        [searchField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        [searchField setTextColor:[UIColor orangeColor]];
        
        //自己的搜索图标
        NSString *path = [[NSBundle mainBundle] pathForResource:@"search1" ofType:@"png"];
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
        [iView setFrame:CGRectMake(0.0, 0.0, 16.0, 16.0)];
        searchField.leftView = iView;
    }
    
    //外部背景
    UIView *outView = [[UIView alloc] initWithFrame:self.bounds];
    [outView setBackgroundColor:[UIColor whiteColor]];
    [self insertSubview:outView atIndex:1];

}

@end
