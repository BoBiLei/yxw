//
//  RegistController.h
//  CatShopping
//
//  Created by mac on 15/9/15.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface RegistController : BaseController
@property (weak, nonatomic) IBOutlet UIView *acountView;
@property (weak, nonatomic) IBOutlet UIView *verifyView;
@property (weak, nonatomic) IBOutlet UIButton *verifyBtn;

@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIView *confirmPassView;
@property (weak, nonatomic) IBOutlet UIView *referralView;
@property (weak, nonatomic) IBOutlet UIButton *registBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollMainView;
//label
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *verifyText;
@property (weak, nonatomic) IBOutlet UITextField *pwdText;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdText;
@property (weak, nonatomic) IBOutlet UITextField *recomentText;

@property (nonatomic, copy) void (^LoginUserNameBlock)(NSString *textStr);

@end
