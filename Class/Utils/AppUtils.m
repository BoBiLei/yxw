//
//  AppUtils.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "AppUtils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation AppUtils

+ (void)closeKeyboard{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

+ (NSString *)md5FromString:(NSString *)str{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (int)strlen(cStr), result);
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (UILabel *)tableViewsHeaderLabelWithMessage:(NSString *)message{
    UILabel *lb_headTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 320, 20)];
    lb_headTitle.font = [UIFont boldSystemFontOfSize:15.0];
    lb_headTitle.textColor = [UIColor darkGrayColor];
    lb_headTitle.textAlignment = NSTextAlignmentCenter;
    lb_headTitle.text = message;
    return lb_headTitle;
}

+ (void)showSuccessMessage:(NSString *)message inView:(UIView *)view{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = message;
    hud.margin = 12;
    hud.cornerRadius = 5;
    hud.labelFont = [UIFont systemFontOfSize:16];
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1.7];
}
+ (void)showProgressInView:(UIView *)view{
    [MBProgressHUD showHUDAddedTo:view animated:YES];
}

+ (void)showProgressMessage:(NSString *) message inView:(UIView *)view{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    hud.labelText = message;
    hud.labelFont=[UIFont systemFontOfSize:13];
    hud.margin = 12;
    [hud show:YES];
}

+ (void)dismissHUDInView:(UIView *)view{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

+ (BOOL)checkPhoneNumber:(NSString *)phoneNumber{
    /**
     
     * 手机号码
     
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,184,187,188,1705，170，177
     
     * 联通：130,131,132,152,155,156,185,186,176,145,1709
     
     * 电信：133,1349卫星通信,153,180,181,189,177,1700
     
     */
    NSString * MOBILE  = @"^1(3[0-9]|5[0-35-9]|8[025-9]|70|77)\\d{8}$";
    NSString *      CM = @"^1(34[0-8]|(3[5-9]|47|5[0127-9]|78|8[23478])\\d)\\d{7}$";
    NSString *      CU = @"^1(3[0-2]|5[256]|8[56]|76|45)\\d{8}$";
    NSString *      CT = @"^1((33|53|8[019]|77)[0-9]|349)\\d{7}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    BOOL                res1 = [regextestmobile evaluateWithObject:phoneNumber];
    BOOL                res2 = [regextestcm evaluateWithObject:phoneNumber];
    BOOL                res3 = [regextestcu evaluateWithObject:phoneNumber];
    BOOL                res4 = [regextestct evaluateWithObject:phoneNumber];
    
    return res1||res2||res3||res4 ? YES:NO;
}

+ (BOOL)checkPassworkLength:(NSString *)pass{
    if ([pass length]>=6&&[pass length]<=18) {
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL)checkPassworkSafty:(NSString *)pass{
    return YES;
}

+ (BOOL)checkEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(void)saveValue:(id) value forKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([value isKindOfClass:[NSNull class]]){
        [userDefaults setObject:@"" forKey:key];
    }else{
        [userDefaults setObject:value forKey:key];
    }
    [userDefaults synchronize];
}

+(id)getValueWithKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}

+(BOOL)boolValueWithKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:key];
}

#pragma mark------计算文字的Size-------
+(CGSize)getStringSize:(NSString*)strtemp withFont:(NSInteger)fontSize{
    /*
    NSDictionary *attribute = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]};
    CGSize size = [strtemp boundingRectWithSize:
                   CGSizeMake(SCREENSIZE.width*8, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine| NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:
                   attribute context:nil].size;*/
    //
    UIFont *fnt = [UIFont systemFontOfSize:fontSize];
    CGSize size;
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    size = [strtemp boundingRectWithSize:
                      CGSizeMake(0, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine| NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil] context:nil].size;
#else
    
    size = [strtemp sizeWithFont:fnt constrainedToSize:CGSizeMake(0, MAXFLOAT)];
    
#endif
    return size;
}
+ (BOOL)loginState {
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    return app.isLogin;
}
+ (NSString *)userIDStr {
    NSString *iDstr = [self getValueWithKey:User_ID];
    return iDstr;
}
+ (NSString *)userNameStr {
    NSString *nameStr = [self getValueWithKey:User_Name];
    return nameStr;
}
+ (NSString *)userPhotoStr {
    NSString *photoStr = [self getValueWithKey:User_Photo];
    return photoStr;
}

/********************* 判断字符串是否包含空格 space**********************/
+(BOOL)isStringIncludeSpace:(NSString *)string{
    NSRange range = [string rangeOfString:@" "];
    if (!(range.length>0)) {
        return NO;
    }
    return YES;
}

/********************* 判断字符串是否包含特殊字符 **********************/
+(BOOL)isStringIncludeSpecialChar:(NSString *)string{
    NSRange specialRange = [string rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @"~￥#&*<>《》()[]{}【】^@/￡¤￥|§¨「」『』￠￢￣~@#￥&*（）——+|《》$_€～"]];
    if (specialRange.location == NSNotFound){
        return NO;
    }
    return YES;
}

/********************* 判断字符串是否包数字 **********************/
+(BOOL)isStringIncludeDigitChar:(NSString *)string{
    NSRange digitRange = [string rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @"0123456789"]];
    if (digitRange.location==NSNotFound){
        return NO;
    }
    return YES;
}

/********************* 判断字符串是否全部包含数字 **********************/
+(BOOL)isStringPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}



+(void)drawLine{
    //获得处理的上下文
    CGContextRef
    context = UIGraphicsGetCurrentContext();
    //指定直线样式
    CGContextSetLineCap(context,
                        kCGLineCapSquare);
    //直线宽度
    CGContextSetLineWidth(context,
                          2.0);
    //设置颜色
    CGContextSetRGBStrokeColor(context,
                               0.314, 0.486, 0.859, 1.0);
    //开始绘制
    CGContextBeginPath(context);
    //画笔移动到点(31,170)
    CGContextMoveToPoint(context,
                         50, SCREENSIZE.width);
    //绘制完成
    CGContextStrokePath(context);
}

//获取当前版本
+ (NSString *)getCurrentVersion{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

//验证银行卡合法性
+ (BOOL)isCardNumber:(NSString*)cardNumber{
    int oddsum = 0;     //奇数求和
    int evensum = 0;    //偶数求和
    int allsum = 0;
    int cardNoLength = (int)[cardNumber length];
    int lastNum = 0;
    if (![cardNumber isEqualToString:@""]) {
        lastNum = [[cardNumber substringFromIndex:cardNoLength-1] intValue];
        cardNumber = [cardNumber substringToIndex:cardNoLength - 1];
        for (int i = cardNoLength -1 ; i>=1;i--) {
            NSString *tmpString = [cardNumber substringWithRange:NSMakeRange(i-1, 1)];
            int tmpVal = [tmpString intValue];
            if (cardNoLength % 2 ==1 ) {
                if((i % 2) == 0){
                    tmpVal *= 2;
                    if(tmpVal>=10)
                        tmpVal -= 9;
                    evensum += tmpVal;
                }else{
                    oddsum += tmpVal;
                }
            }else{
                if((i % 2) == 1){
                    tmpVal *= 2;
                    if(tmpVal>=10)
                        tmpVal -= 9;
                    evensum += tmpVal;
                }else{
                    oddsum += tmpVal;
                }
            }
        }
    }
    allsum = oddsum + evensum;
    allsum += lastNum;
    if((allsum % 10) == 0){
        return YES;
    }
    else{
        return NO;
    }
}

//判断密码强度
+ (NSString*)judgePasswordStrength:(NSString*)password{
    NSMutableArray* resultArray = [[NSMutableArray alloc] init];
    
    NSArray* termArray1 = [[NSArray alloc] initWithObjects:@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", nil];
    
    NSArray* termArray2 = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0", nil];
    
    NSArray* termArray3 = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    
    NSArray* termArray4 = [[NSArray alloc] initWithObjects:@"~",@"`",@"@",@"#",@"$",@"%",@"^",@"&",@"*",@"(",@")",@"-",@"_",@"+",@"=",@"{",@"}",@"[",@"]",@"|",@":",@";",@"“",@"'",@"‘",@"<",@",",@".",@">",@"?",@"/",@"、", nil];
    
    NSString* result1 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray1 Password:password]];
    
    NSString* result2 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray2 Password:password]];
    
    NSString* result3 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray3 Password:password]];
    
    NSString* result4 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray4 Password:password]];
    
    [resultArray addObject:[NSString stringWithFormat:@"%@",result1]];
    
    [resultArray addObject:[NSString stringWithFormat:@"%@",result2]];
    
    [resultArray addObject:[NSString stringWithFormat:@"%@",result3]];
    
    [resultArray addObject:[NSString stringWithFormat:@"%@",result4]];
    
    int intResult=0;
    for (int j=0; j<[resultArray count]; j++){
        if ([[resultArray objectAtIndex:j] isEqualToString:@"1"]){
            intResult++;
        }
    }
    NSString* resultString = [[NSString alloc] init];
    if (intResult <2){
        resultString = @"0";
        
    }else if (intResult == 2&&[password length]>=6){
        resultString = @"1";
        
    }
    if (intResult > 2&&[password length]>=6){
        resultString = @"2";
        
    }
    return resultString;
}

//是否包含
+ (BOOL)judgeRange:(NSArray*)termArray Password:(NSString*)password{
    NSRange range;
    BOOL result =NO;
    for(int i=0; i<[termArray count]; i++){
        range = [password rangeOfString:[termArray objectAtIndex:i]];
        if(range.location != NSNotFound){
            result =YES;
        }
    }
    return result;
}

+(void)showActivityIndicatorView:(UIView *)view{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.center = CGPointMake(view.width/2, view.height/2);
    [view addSubview:activityIndicator];
    activityIndicator.color = [UIColor lightGrayColor];
    [activityIndicator startAnimating];
    [activityIndicator setHidesWhenStopped:YES];
}


+(BOOL)dismissActivityIndicatorView:(UIView *)view{
    UIActivityIndicatorView *activityIndicator=[self AciIndViewForView:view];
    if (activityIndicator != nil) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        return YES;
    }
    return NO;
}

+ (UIActivityIndicatorView *)AciIndViewForView:(UIView *)view {
    NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:[UIActivityIndicatorView class]]) {
            return (UIActivityIndicatorView *)subview;
        }
    }
    return nil;
}


+(NSString *)getTimeStampWithString:(NSString *)str{
    //时间戳
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%@", str];
    return time_stamp;
}

//重新获取key
+(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
        }else{
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//身份证号
+ (BOOL) validateIdentityCard: (NSString *)identityCard{
    BOOL flag;
    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:identityCard];
}

/**
 *处理字符串变为*号
 */
+(NSString *)handleStringReplaceToStar:(NSString *)handleString atIndex:(NSInteger)atIndex{
    
    
    //
    char mychar[150];
    NSInteger strLength=handleString.length;
    memcpy(mychar, [handleString cStringUsingEncoding:NSUTF8StringEncoding], [handleString length]);
    for(NSInteger i=strLength-1; i>=atIndex; i--){
        mychar[i]='*';
    }
    return [NSString stringWithCString:mychar encoding:NSUTF8StringEncoding];
}

// 获取某个月的天数
+ (NSInteger)getSumOfDaysInYear:(NSString *)year month:(NSString *)month{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM"]; // 年-月
    
    NSString * dateStr = [NSString stringWithFormat:@"%@-%@",year,month];
    
    NSDate * date = [formatter dateFromString:dateStr];
    
    //
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]; // 指定日历的算法
    
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay
                                   inUnit: NSCalendarUnitMonth
                                  forDate:date];
    return range.length;
}

/**
 *字符串时间戳转换时间
 */
+ (NSString *)handleTimeStampStringToTime:(NSString *)stringStamp{
    //时间戳转换
    NSTimeInterval time=[stringStamp doubleValue];
    
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *valiStr = [dateFormatter stringFromDate: detaildate];
    return valiStr;
}

//从URL获取图片
+ (UIImage *) imageFromURLString: (NSString *) urlstring{
    return [UIImage imageWithData:[NSData
                                   dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
}


//+ (void)saveKeyChainValue:(NSString *)value forKey:(NSString *)key{
//    [SAMKeychain setPassword:value forService:key account:key];
//}
//
//+ (NSString *)getKeyChainValueForKey:(NSString *)key{
//    return [SAMKeychain passwordForService:key account:key];
//}

#pragma mark - 画虚线
+(void)drawXuXian:(UIView *)view withColor:(UIColor *)color firstPoint:(CGPoint)fPoint endPoint:(CGPoint)ePoint{
    //虚线
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    // 设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:[color CGColor]];
    
    // 3.0f设置虚线的宽度
    [shapeLayer setLineWidth:1.0f];
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    // 3=线的宽度 1=每条线的间距
    [shapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:4],
      [NSNumber numberWithInt:1],nil]];
    
    // Setup the path
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, fPoint.x, fPoint.y);
    CGPathAddLineToPoint(path, NULL, ePoint.x,ePoint.y);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [[view layer] addSublayer:shapeLayer];
}

+(void)drawZhiXian:(UIView *)view withColor:(UIColor *)color height:(CGFloat)height firstPoint:(CGPoint)fPoint endPoint:(CGPoint)ePoint{
    CAShapeLayer *shapeLayer =  [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setLineWidth:height];
    // 设置实线颜色为blackColor
    [shapeLayer setStrokeColor:[color CGColor]];
    
    // Setup the path
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, fPoint.x, fPoint.y);
    CGPathAddLineToPoint(path, NULL, ePoint.x,ePoint.y);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    [view.layer addSublayer:shapeLayer];
}

+ (NSData *)toJSONData:(id)theData{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}

@end
