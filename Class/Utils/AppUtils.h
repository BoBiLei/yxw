//
//  AppUtils.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtils : NSObject

/******* 关闭键盘 *******/

+ (void)closeKeyboard;

/******* 获取MD5加密后字符串 *******/

+ (NSString *)md5FromString:(NSString *)str;

/******* 返回UILabel作为UITableView的header *******/

+ (UILabel *)tableViewsHeaderLabelWithMessage:(NSString *)message;

/********************* 弹出提示框 **********************/

+ (void)showSuccessMessage:(NSString *)message inView:(UIView *)view;

/********************* 弹出无提示的进程框 **********************/

+ (void)showProgressInView:(UIView *)view;

/********************* 弹出有提示的进程框 **********************/

+ (void)showProgressMessage:(NSString *) message inView:(UIView *)view;

/********************* 提示框消失 **********************/

+ (void)dismissHUDInView:(UIView *)view;

/********************* 验证手机号码合法性（正则） **********************/

+ (BOOL)checkPhoneNumber:(NSString *)phoneNumber;

/********************* 验证密码长度（6-20位） **********************/

+ (BOOL)checkPassworkLength:(NSString *)pass;

/********************* 验证密码强弱 **********************/

+ (BOOL)checkPassworkSafty:(NSString *)pass;

/********************* 验证邮箱合法性 **********************/

+ (BOOL) checkEmail:(NSString *)email;

/********************* 存储用户名密码 **********************/

+(void)saveValue:(id) value forKey:(NSString *)key;

+(id)getValueWithKey:(NSString *)key;

+(BOOL)boolValueWithKey:(NSString *)key;

/********************* 获取String的size **********************/
+(CGSize)getStringSize:(NSString*)strtemp withFont:(NSInteger)fontSize;

/********************* 获取当前登录状态 **********************/
+ (BOOL)loginState;

/********************* 获取ID **********************/
+ (NSString *)userIDStr;
/********************* 获取用户名 **********************/
+ (NSString *)userNameStr;
/********************* 获取用户头像 **********************/
+ (NSString *)userPhotoStr;

/********************* 收藏+- **********************/
//typedef NS_ENUM(NSInteger, CollectionStatus) {
//    CollectionAdd,
//    CollectionSubtracter
//};
//+ (void)setCollectionSumWithStatus:(CollectionStatus)status andKey:(NSString *)key;
/********************* 分享+- **********************/
typedef NS_ENUM(NSInteger, ShareStatus) {
    ShareAdd,
    ShareSubtracter
};
/********************* 判断字符串是否包含空格**********************/
+(BOOL)isStringIncludeSpace:(NSString *)string;

/********************* 判断字符串是否包含特殊字符 **********************/
+(BOOL)isStringIncludeSpecialChar:(NSString *)string;

/********************* 判断字符串是否包数字 **********************/
+(BOOL)isStringIncludeDigitChar:(NSString *)string;

/********************* 判断字符串是否全部包含数字 **********************/
+(BOOL)isStringPureInt:(NSString *)string;

+(void)drawLine;

/********************* 获取当前版本 **********************/
+ (NSString *)getCurrentVersion;

/********************* 验证银行卡 **********************/
+ (BOOL)isCardNumber:(NSString*)cardNumber;

/********************* 验证身份证 **********************/
+ (BOOL)validateIdentityCard:(NSString *)identityCard;

/**判断密码强度 0：弱 1：一般 2：强*/
+ (NSString*)judgePasswordStrength:(NSString*)password;
+ (BOOL)judgeRange:(NSArray*)termArray Password:(NSString*)password;

+(void)showActivityIndicatorView:(UIView *)view;
+(BOOL)dismissActivityIndicatorView:(UIView *)view;


/**获取时间戳*/
+(NSString *)getTimeStampWithString:(NSString *)str;

/**
 *重新获取signkey
 */
+(void)requestHttpsSignKey;

/**
 *处理字符串变为*号
 */
+(NSString *)handleStringReplaceToStar:(NSString *)handleString atIndex:(NSInteger)atIndex;

/**
 *获取某个月的天数
 */
+ (NSInteger)getSumOfDaysInYear:(NSString *)year month:(NSString *)month;

/**
 *字符串时间戳转换时间
 */
+ (NSString *)handleTimeStampStringToTime:(NSString *)stringStamp;

/**
 *从URL获取图片
 */
+ (UIImage *) imageFromURLString: (NSString *) urlstring;


+ (NSData *)toJSONData:(id)theData;

#pragma mark - 画虚线
+(void)drawXuXian:(UIView *)view withColor:(UIColor *)color firstPoint:(CGPoint)fPoint endPoint:(CGPoint)ePoint;

+(void)drawZhiXian:(UIView *)view withColor:(UIColor *)color height:(CGFloat)height firstPoint:(CGPoint)fPoint endPoint:(CGPoint)ePoint;

@end
