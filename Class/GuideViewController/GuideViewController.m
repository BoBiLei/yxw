//
//  GuideViewController.m
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "GuideViewController.h"
#import "RootViewController.h"
@interface GuideViewController ()<UIScrollViewDelegate>

@end

@implementation GuideViewController{
    UIView *mainView;
    UIScrollView *myScrollView;
    UIPageControl *_pageControl;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //签名需要的（初始值）
    [AppUtils saveValue:@"a5(4&70^8p9f4h*8xgg%h3x0^7a27v" forKey:Sign_Key];
    [AppUtils saveValue:@"151201" forKey:Verson_Key];
    [AppUtils saveValue:@"1" forKey:@"new"];
    
    [NSThread sleepForTimeInterval:2.0];
    
    mainView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    [self.view addSubview:mainView];
    myScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    myScrollView.delegate=self;
    [mainView addSubview:myScrollView];
    
    [self initGuideView];
}

-(void)initGuideView{
    for (int i=0; i<3; i++) {
        UIImageView *_imageView=[[UIImageView alloc]initWithFrame:CGRectMake(i*SCREENSIZE.width, 0, SCREENSIZE.width, SCREENSIZE.height)];
        _imageView.backgroundColor=[UIColor orangeColor];
        _imageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"guide0%d.jpg",i]];
        [_imageView setUserInteractionEnabled:YES];
        [myScrollView addSubview:_imageView];
        if (i==2) {
            UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-96, SCREENSIZE.height-62, 90, 44)];
            btn.titleLabel.font = [UIFont systemFontOfSize:18];
            [btn setTitle:@"进入 >>>" forState:UIControlStateNormal];
            btn.layer.cornerRadius=5;
            [btn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(gotoMainView) forControlEvents:UIControlEventTouchDown];
            [_imageView addSubview:btn];
        }
    }
    myScrollView.contentSize=CGSizeMake(SCREENSIZE.width*3, 0);
    myScrollView.showsHorizontalScrollIndicator=NO;
    myScrollView.showsVerticalScrollIndicator=NO;
    myScrollView.pagingEnabled=YES;
    myScrollView.bounces=NO;
    UIView *aView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-30, SCREENSIZE.width, 20)];
    aView.backgroundColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.0];
    _pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(50, 0, 220, 20)];
    _pageControl.center=CGPointMake(aView.frame.size.width/2, aView.frame.size.height/2);
    _pageControl.numberOfPages=3;
    _pageControl.currentPageIndicatorTintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [_pageControl addTarget:self action:@selector(onPageChange:) forControlEvents:UIControlEventValueChanged];
    [aView addSubview:_pageControl];
    [mainView addSubview:aView];
}
-(void)onPageChange:(UIPageControl*)sender{
    int page=(int)sender.currentPage;
    [myScrollView setContentOffset:CGPointMake(page*SCREENSIZE.width, 0) animated:YES];
}

-(void)gotoMainView{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FirstLoad"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FirstGuide"];
    RootViewController *root=[[RootViewController alloc]init];
    root.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:root animated:YES completion:nil];
};


#pragma mark -------scrollview delegate-------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView==myScrollView) {
        CGPoint p=scrollView.contentOffset;
        int page=p.x/SCREENSIZE.width;
        _pageControl.currentPage=page;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
