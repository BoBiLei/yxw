//
//  OrderDetailController.m
//  MMG
//
//  Created by mac on 15/10/9.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderDetailController.h"
#import "OrderDetailAddressCell.h"
#import "OrderDetailPayDetailCell.h"

NSString *orderDetailAddIdentifier=@"orderdetailaddcell";
NSString *orderDetailPayDetailIdentifier=@"orderdetailpaydetailcell";
@interface OrderDetailController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation OrderDetailController{
    NSMutableArray *addressArr;
    NSMutableArray *stageArr;
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"订单详情"];
    
    [self requestHttpsForOrderDetail:self.orderId];
}

#pragma mark - init view
-(void)setUpUI{
    
    addressArr=[NSMutableArray array];
    
    stageArr=[NSMutableArray array];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.sectionHeaderHeight = 50;
    [myTable registerNib:[UINib nibWithNibName:@"OrderDetailAddressCell" bundle:nil] forCellReuseIdentifier:orderDetailAddIdentifier];
    [myTable registerNib:[UINib nibWithNibName:@"OrderDetailPayDetailCell" bundle:nil] forCellReuseIdentifier:orderDetailPayDetailIdentifier];
    [self.view addSubview:myTable];
    myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
}

#pragma mark - Request 请求
-(void)requestHttpsForOrderDetail:(NSString *)orderId{
    [AppUtils showActivityIndicatorView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"order_detail" forKey:@"act"];
    [dict setObject:orderId forKey:@"order_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderDetail:self.orderId];
            });
        }else{
            [self setUpUI];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //收货信息
                OrderDetailAddressModel *model=[[OrderDetailAddressModel alloc]init];
                [model jsonDataForDictionary:responseObject[@"retData"][@"order"]];
                [addressArr addObject:model];
                
                //付款详情
                NSArray *stArr=responseObject[@"retData"][@"fenqis"];
                for (NSDictionary *stageDic in stArr) {
                    OrderDetailStageModel *model=[[OrderDetailStageModel alloc]init];
                    [model jsonDataForDictionary:stageDic];
                    [stageArr addObject:model];
                }
            }
            [AppUtils dismissActivityIndicatorView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}


#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return addressArr.count;
    }else{
        return stageArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 112;
    }else{
        return 30;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderDetailAddressCell *cell=[tableView dequeueReusableCellWithIdentifier:orderDetailAddIdentifier];
        if (!cell) {
            cell=[[OrderDetailAddressCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderDetailAddressModel *model=addressArr[indexPath.row];
        [cell reflushDataForModel:model];
        return  cell;
    }else{
        OrderDetailPayDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:orderDetailPayDetailIdentifier];
        if (!cell) {
            cell=[[OrderDetailPayDetailCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderDetailStageModel *model=stageArr[indexPath.row];
        [cell reflushDataForModel:model indexPath:indexPath];
        return  cell;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1) {
        return 79;
    }else{
       return 48;
    }
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 48)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    CGRect frame=headerSectionView.frame;
    frame.size.height=79;
    
    UIImageView *imgv=[UIImageView new];
    imgv.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headerSectionView addSubview:imgv];
    imgv.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* imgv_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imgv]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgv)];
    [NSLayoutConstraint activateConstraints:imgv_h];
    
    NSArray* imgv_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgv(14)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgv)];
    [NSLayoutConstraint activateConstraints:imgv_w];
    
    //订单编号
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 14, SCREENSIZE.width/2+20, headerSectionView.frame.size.height-10)];
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [headerSectionView addSubview:titleLabel];
    
    //审核状态
    UILabel *statusLabel=[UILabel new];
    [headerSectionView addSubview:statusLabel];
    statusLabel.font=[UIFont systemFontOfSize:13];
    statusLabel.textAlignment=NSTextAlignmentRight;
    statusLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* statusLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[titleLabel]-8-[statusLabel]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel,statusLabel)];
    [NSLayoutConstraint activateConstraints:statusLabel_h];
    
    NSArray* statusLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-14-[statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(statusLabel)];
    [NSLayoutConstraint activateConstraints:statusLabel_w];
    if (section==0){
        titleLabel.text=@"收件信息";
    }
    else{
        titleLabel.text=@"付款详情";
        headerSectionView.frame=frame;
        //top line
        UIView *line=[[UIView alloc]init];
        line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [headerSectionView addSubview:line];
        line.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
        [NSLayoutConstraint activateConstraints:line_h];
    
        NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel][line(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel,line)];
        [NSLayoutConstraint activateConstraints:line_w];
        
        //白色view
        UIView *mView=[[UIView alloc]init];
        mView.backgroundColor=[UIColor whiteColor];
        [headerSectionView addSubview:mView];
        mView.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* mView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(mView)];
        [NSLayoutConstraint activateConstraints:mView_h];
        
        NSArray* mView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line][mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,mView)];
        [NSLayoutConstraint activateConstraints:mView_w];
        
        //期数
        UILabel *stageLabel=[[UILabel alloc]init];
        stageLabel.text=@"期数";
        stageLabel.font=[UIFont systemFontOfSize:14];
        stageLabel.textAlignment=NSTextAlignmentCenter;
        stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:stageLabel];
        stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[stageLabel(52)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
        [NSLayoutConstraint activateConstraints:stageLabel_h];
        
        NSArray* stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stageLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
        [NSLayoutConstraint activateConstraints:stageLabel_w];
        
        //line01
        UIView *line01=[[UIView alloc]init];
        line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [mView addSubview:line01];
        line01.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[stageLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel,line01)];
        [NSLayoutConstraint activateConstraints:line01_h];
        
        NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
        [NSLayoutConstraint activateConstraints:line01_w];
        
        //扣款日期
        UILabel *cutPayTimeLabel=[[UILabel alloc]init];
        cutPayTimeLabel.text=@"扣款日期";
        cutPayTimeLabel.font=[UIFont systemFontOfSize:14];
        cutPayTimeLabel.textAlignment=NSTextAlignmentCenter;
        cutPayTimeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:cutPayTimeLabel];
        cutPayTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line01][cutPayTimeLabel(110)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,cutPayTimeLabel)];
        [NSLayoutConstraint activateConstraints:cutPayTimeLabel_h];
        
        NSArray* cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cutPayTimeLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel)];
        [NSLayoutConstraint activateConstraints:cutPayTimeLabel_w];
        
        //line02
        UIView *line02=[[UIView alloc]init];
        line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [mView addSubview:line02];
        line02.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cutPayTimeLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel,line02)];
        [NSLayoutConstraint activateConstraints:line02_h];
        
        NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
        [NSLayoutConstraint activateConstraints:line02_w];
        
        //金额
        UILabel *priceLabel=[[UILabel alloc]init];
        priceLabel.text=@"金额";
        priceLabel.font=[UIFont systemFontOfSize:14];
        priceLabel.textAlignment=NSTextAlignmentCenter;
        priceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:priceLabel];
        priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line02][priceLabel(80)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,priceLabel)];
        [NSLayoutConstraint activateConstraints:priceLabel_h];
        
        NSArray* priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[priceLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(priceLabel)];
        [NSLayoutConstraint activateConstraints:priceLabel_w];
        
        //line03
        UIView *line03=[[UIView alloc]init];
        line03.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [mView addSubview:line03];
        line03.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line03_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[priceLabel][line03(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(priceLabel,line03)];
        [NSLayoutConstraint activateConstraints:line03_h];
        
        NSArray* line03_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line03]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03)];
        [NSLayoutConstraint activateConstraints:line03_w];
        
        //状态
        UILabel *statusLabel=[[UILabel alloc]init];
        statusLabel.text=@"状态";
        statusLabel.font=[UIFont systemFontOfSize:14];
        statusLabel.textAlignment=NSTextAlignmentCenter;
        stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:statusLabel];
        statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* statusLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line03][statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03,statusLabel)];
        [NSLayoutConstraint activateConstraints:statusLabel_h];
        
        NSArray* statusLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(statusLabel)];
        [NSLayoutConstraint activateConstraints:statusLabel_w];
    }
    
    return headerSectionView;
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.5f;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}
@end
