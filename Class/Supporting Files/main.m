//
//  main.m
//  YXW
//
//  Created by mac on 15/11/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
