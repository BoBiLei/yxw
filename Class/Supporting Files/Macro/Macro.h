//
//  Macro.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#ifndef Macro_h
#define Macro_h

#import "AppDelegate.h"

//======================================================================
//tabbar的五个Controller，跟RootviewController
//======================================================================
#import "RootViewController.h"
#import "HomePageController.h"
#import "IslandController.h"
#import "PhoneController.h"
#import "PersonalCenterController.h"

//======================================================================
//添加本地头文件
//======================================================================
#import "GuideViewController.h"
#import "YXWSign.h"
#import "NetWorkRequest.h"
#import "IslandDetailController.h"
#import "CustomPickerView.h"
#import "YouXiaStageController.h"
#import "StrategyDetailController.h"
#import "BaseNavigationBar.h"
#import "NetWorkStatus.h"
#import "TalkingData.h"
#import "SDCycleScrollView.h"
#import "CustomPickerView.h"
#import "AppUtils.h"

#define WEAKSELF typeof(self) __weak weakSelf = self;

//======================================================================
//十六进制颜色值
//======================================================================
#define Tabbar_BgColor @"#f4f4f4"
#define TabbarItem_TitleColor @"#1172bd"
#define YxColor_LightBlue @"#d4edff"
#define YxColor_Blue @"#1172bd"
#define YxColor_MBlue @"#1DAFED"
#define YxColor_Yellow @"#ec6b00"

#define IMAGEPLAY_HEIHGT SCREENSIZE.width*375/500
#define Themtic_IMAGEPLAY_HEIHGT SCREENSIZE.height/2+49
#define CATEGORY_HEIHGT (SCREENSIZE.width-60)/4.0*2+10
#define SERVICEPHONE @"400-164-0016"

#define DefaultHost @"https://m.youxia.com/"
#define YouxiaXiu @"https://m.youxia.com/"
#define NewYXHost @"https://www.youxia.com/"

#define HoteHost @"https://www.youxia.com/"

#define FilmHost @"https://www.youxia.com/mgo/index.php?"

#define FilsHost @"https://mall.youxia.com/mgo/index.php?"
//咨询链接
#define ZixunUrl @"https://m.youxia.com/index.php?m=wap&c=index&a=zxzx_iso"

//获取费率
#define RateUrl @"https://m.youxia.com/index.php?m=wap&c=index&a=fenqi_lv"

#define Image_Default @"default"

#endif
