//
//  NoficationMacro.h
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//  放通知中心相关的标识

#ifndef NoficationMacro_h
#define NoficationMacro_h

#define HomePageImageNoti @"HomePageImageNoti"
#define HomePageCategoryNoti @"HomePageCategoryNoti"
#define ThemitPageImageNoti @"ThemitPageImageNoti"
#define ThemitTJWImageNoti @"ThemitTJWImageNoti"
#define YouxiaStageImageNoti @"YouxiaStageImageNoti"

#define RegistSuccessNotification @"registsuccessnotification"
#define LoginSuccessNotifition @"loginsuccessnotifition"

#define UpDateUser_Photo @"UpDateUser_Photo"
#define UpDateUser_Name @"UpDateUser_Name"

#define SelectedCompanyAddressNotification @"SelectedAddressNotification"
#define SelectedHomeAddressNotification @"SelectedHomeAddressNotification"


#define kXMPP_HOST @"im.youxia.com"
#define kXMPP_PORT 5222
#define bXMPP_domain @"im.youxia.com"
#define bXMPP_resource @"YouXia_iOS"

#define kLOGIN_SUCCESS      @"kLOGIN_SUCCESS"
#define kREGIST_RESULT      @"kREGIST_RESULT"
#define kXMPP_ROSTER_CHANGE @"kXMPP_ROSTER_CHANGE"
#define kXMPP_MESSAGE_CHANGE @"kXMPP_MESSAGE_CHANGE"


//填写信用信息
#define UpdateXCKNotification @"UpdateXCKNotification"

#define UpdateXYKNotification @"UpdateXYKNotification"

#define UpdateContactNotification @"UpdateContactNotification"

#define UpdateTXDZNotification @"UpdateTXDZNotification"

#define UpdateUploadImgCount @"UpdateUploadImgCount"

#define UploadButtonIsNot @"UploadButtonIsNot"

#define SelectedContactNotification @"SelectedContactNotification"

#define SubmitXYRZSuccess @"SubmitXYRZSuccess"

//chat
/** 通知中心 */
#define YBNotificationCenter  [NSNotificationCenter defaultCenter]

#define YBKeyboardDidClickFuctionButtonNotification   @"YBKeyboardDidClickFuctionButtonNotification"

#define YBKeyboardDidClickEmojiNotification   @"YBKeyboardDidClickEmojiNotification"

#define YBKeyboardDeletedEmojiNotification   @"YBKeyboardDeletedEmojiNotification"

#define YBKeyboardShowSendButtonNotification   @"YBKeyboardShowSendButtonNotification"

#define YBKeyboardSendActionNotification  @"YBKeyboardSendActionNotification"

//电影票
#define FilmOrder_CancelNotifacation  @"FilmOrder_CancelNotifacation"
#define FilmOrder_SelectCoupon_Noti  @"FilmOrder_SelectCoupon_Noti"
#define SelectCinema_City_Notification @"SelectCinema_City_Notification"

#define FilmPay_Success_RefleshList_Noti  @"FilmPay_Success_RefleshList_Noti"

#endif /* NoficationMacro_h */
