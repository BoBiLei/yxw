//
//  APIStringMacro.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//  放一些第三方相关的

#ifndef VendersMacro_h
#define VendersMacro_h

//======================================================================
//添加第三方库头文件
//======================================================================
#import <IQKeyboardManager.h>
#import "MMAlertView.h"
#import "MMPopupCategory.h"
#import "MMPopupWindow.h"
#import "MMPopupView.h"
#import "MMPopupItem.h"
#import "MMSheetView.h"
#import <MBProgressHUD.h>
#import <AFNetworking.h>
#import <TTTAttributedLabel.h>
#import <MJRefresh.h>
#import <MJRefreshHeader.h>
#import <MJRefreshFooter.h>
#import <UIScrollView+MJRefresh.h>
#import <XMLDictionary.h>
#import "WebViewJavascriptBridge.h"
#import <AlipaySDK/AlipaySDK.h>
#import "DOPDropDownMenu.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIShareActionSheetCustomItem.h>
#import <JDStatusBarNotification.h>
#import <YYKit.h>
#import <SDImageCache.h>
#import <UIImageView+WebCache.h>
#import "UINavigationBar+Awesome.h"
#import <RDVTabBarController.h>
#import <RDVTabBarItem.h>
#import <FXBlurView.h>
#import <IDMPhotoBrowser.h>

//======================================================================
//添加Category头文件
//======================================================================
#import "UIBarButtonItem+Badge.h"
#import "UIColor+Hex.h"
#import "UIImage+LK.h"
#import "UIView+RGSize.h"
#import "UIWindow+SIUtils.h"
#import "NSData+THCategory.h"
#import "FilmNavigationBar.h"

#endif /* VendersMacro_h */
