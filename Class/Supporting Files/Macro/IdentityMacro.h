//
//  IdentityMacro.h
//  youxia
//
//  Created by mac on 16/7/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  NSUserDefault 标识相关的

#ifndef IdentityMacro_h
#define IdentityMacro_h

//===============
//标识签名
//===============
#define Sign_Key @"yx_signkey"                         //用户ID
#define Verson_Key @"yx_keyverson"
#define Sign_NewKey @"yx_Sign_NewKey"

//======================================================================
//标识UserDefault的
//======================================================================
#define User_Account @"yx_useraccount"            //用户账号
#define User_ID @"userid"                      //用户ID
#define User_Photo @"userphoto"                //用户头像
#define User_Name @"username"                  //用户名
#define User_Money @"usermoney"                //用户余额
#define User_Email @"useremail"                //用户邮箱
#define User_Phone @"userephone"               //用户手机
#define User_Pass @"yx_userepass"                 //
#define VIP_Flag @"vipFlag"                    //标识vip
#define Bind_User_Id @"Bind_User_Id"    

//第三方登陆是否是新密码
#define Is_NewPwd @"Is_NewPwd"                 //新密码vip

//===========标识聊天的==========
#define IsHadChat @"ishadchat"      //是否已聊过标识
#define ChatService @"chatservice"  //聊天客服标识

//第三方授权登录类型 微信、QQ、新浪
#define SSO_Login_UID @"SSO_Login_UID"
#define SSO_Login_Type @"SSO_Login_Type"

//登录类型  1：手机号登录   2：第三方登录
#define Login_Type @"Login_Type"

#define yx_wx_Code @"yx_wx_Code"

//电影院城市ID、城市名称
#define Cinema_City_Id @"Cinema_CityId"

#define Cinema_City_Name @"Cinema_CityName"

#endif /* IdentityMacro_h */
