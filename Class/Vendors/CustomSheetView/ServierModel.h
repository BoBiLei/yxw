//
//  ServierModel.h
//  youxia
//
//  Created by mac on 15/12/23.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServierModel : NSObject

@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *serviceEmail;
@property (nonatomic, copy) NSString *serviceMoblie;
@property (nonatomic, copy) NSString *serviceName;

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

@end
