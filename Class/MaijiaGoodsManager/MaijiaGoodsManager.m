//
//  MaijiaGoodsManager.m
//  MMG
//
//  Created by mac on 16/8/16.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "MaijiaGoodsManager.h"
#import "SoldOutGoodsCell.h"
#import "CustomIOSAlertView.h"
#import <TTTAttributedLabel.h>
#import "GoodsEditController.h"

@interface MaijiaGoodsManager ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,SoldOutGoodsDelegate>

@end

@implementation MaijiaGoodsManager{
    UITextField *searchTF;
    NSString *searKeywork;
    BOOL isSeleJishou;
    NSString *js_sum;               //寄售个数
    NSString *xj_sum;               //下架个数
    
    NSString *validateStr;          //输入的验证码
    NSString *productValidate;      //生成的验证码
    
    NSMutableArray *dataArr;
    UITableView *myTable;
    NSInteger pageInt;
    MJRefreshAutoNormalFooter *footer;
    
    CustomIOSAlertView *reShangjiaAlertView;
    CustomIOSAlertView *deleteAlertView;
    UIButton *confirmDeleteBtn;
    
    BOOL isXiajiaGoods;                     //是否选择下架
    BOOL isSelePiLiangBtn;                  //是否选择批量按钮
    UIButton *seleJisXiajBtn;               //寄售下架btn
    UIButton *jsBtn;
    UIButton *xjBtn;
    UIButton *pilBtn;                       //批量管理button
    UIButton *bottom_plBtn;
    
    UIView *fillBtmView;
    
    NSMutableArray *selectGoodsArr;         //已选择的商品
    MaijiaGoodsManagerModel *seleModel;
    
    BOOL isGoodsManageer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateView:) name:@"manager_eidt_success" object:nil];
    
    [self setCustomNavigationTitle:@"商品管理"];
    
    [self setUpSearchView];
    
    [self setUpTableView];
    
    [self setUpFillterBtmView];
    
    [self requestGoodsDetailWithKeywork:searKeywork];
}

-(void)upDateView:(NSNotification *)noti{
    pageInt=1;
    [dataArr removeAllObjects];
    [selectGoodsArr removeAllObjects];
    [self requestGoodsDetailWithKeywork:searKeywork];
}

-(void)setUpSearchView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
    [self.view addSubview:view];
    
#pragma mark 搜索View
    UIView *sView=[[UIView alloc] initWithFrame:CGRectMake(12, 14, SCREENSIZE.width-24, 36)];
    sView.backgroundColor=[UIColor whiteColor];
    [view addSubview:sView];
    
    searchTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, sView.width-sView.height-8, sView.height)];
    searchTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    searchTF.placeholder=@"Birkin";
    searchTF.font=[UIFont systemFontOfSize:15];
    searchTF.textColor=[UIColor colorWithHexString:@"#363636"];
    searchTF.returnKeyType=UIReturnKeySearch;
    searchTF.delegate=self;
    [searchTF addTarget:self action:@selector(searTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [sView addSubview:searchTF];
    
    //
    UIImageView *srImgv=[[UIImageView alloc] initWithFrame:CGRectMake(searchTF.origin.x+searchTF.width+4, 0, sView.height, sView.height)];
    srImgv.image=[UIImage imageNamed:@"maijia_search"];
    srImgv.userInteractionEnabled=YES;
    [sView addSubview:srImgv];
    UITapGestureRecognizer *stap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSearch)];
    [srImgv addGestureRecognizer:stap];
    
    [self setUpDateFillterViewAfterView:view];
}

#pragma mark - 点击搜索
-(void)clickSearch{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [selectGoodsArr removeAllObjects];
    [self requestGoodsDetailWithKeywork:searKeywork];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [selectGoodsArr removeAllObjects];
    [self requestGoodsDetailWithKeywork:searKeywork];
    return YES;
}

- (void)searTextDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    searKeywork=field.text;
}

#pragma mark - 商品筛选View
-(void)setUpDateFillterViewAfterView:(UIView *)view{
    UIView *fview=[[UIView alloc] initWithFrame:CGRectMake(0, view.origin.y+view.height, SCREENSIZE.width, view.height)];
    fview.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:fview];
    
    
    CGFloat plBtnWidth=SCREENSIZE.width/5;
    CGFloat btnY=13;
    CGFloat fillBtnWidth=(SCREENSIZE.width-plBtnWidth-40)/2;
    CGFloat fillBtnHeight=fview.height-btnY*2;
    CGFloat btnX=12;
    //
    for (int i=0; i<2; i++) {
        btnX=btnX+i*fillBtnWidth+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, fillBtnWidth, fillBtnHeight);
        btn.backgroundColor=i==0?[UIColor colorWithHexString:@"#ec6b00"]:[UIColor whiteColor];
        [btn setTitleColor:i==0?[UIColor whiteColor]:[UIColor colorWithHexString:@"#252525"] forState:UIControlStateNormal];
        [btn setTitle:i==0?@"寄售中商品":@"已下架商品" forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        btn.tag=i;
        btn.layer.cornerRadius=3;
        if (i==0) {
            jsBtn=btn;
            seleJisXiajBtn=btn;
        }else{
            xjBtn=btn;
        }
        [btn addTarget:self action:@selector(clickJisXiajBtn:) forControlEvents:UIControlEventTouchUpInside];
        [fview addSubview:btn];
    }
    
#pragma mark 批量管理
    pilBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    pilBtn.frame=CGRectMake(SCREENSIZE.width-12-plBtnWidth, btnY, plBtnWidth, fillBtnHeight);
    pilBtn.backgroundColor=[UIColor whiteColor];
    [pilBtn setTitleColor:[UIColor colorWithHexString:@"#252525"] forState:UIControlStateNormal];
    [pilBtn setTitle:@"批量管理" forState:UIControlStateNormal];
    pilBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    pilBtn.layer.cornerRadius=3;
    [pilBtn addTarget:self action:@selector(clickPiliangBtn) forControlEvents:UIControlEventTouchUpInside];
    [fview addSubview:pilBtn];
    
    [AppUtils drawZhiXian:fview withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:1 firstPoint:CGPointMake(0, fview.height) endPoint:CGPointMake(SCREENSIZE.width, fview.height)];
}

#pragma mark - 点击寄售中/已下架按钮
-(void)clickJisXiajBtn:(id)sender{
    
    [AppUtils closeKeyboard];
    
    pageInt=1;
    
    [dataArr removeAllObjects];
    
    [selectGoodsArr removeAllObjects];
    
    seleJisXiajBtn.enabled=YES;
    seleJisXiajBtn.backgroundColor=[UIColor whiteColor];
    [seleJisXiajBtn setTitleColor:[UIColor colorWithHexString:@"252525"] forState:UIControlStateNormal];
    
    UIButton *btn=sender;
    [bottom_plBtn setTitle:btn.tag==0?@"批量下架":@"批量上架" forState:UIControlStateNormal];
    isSeleJishou=btn.tag==0?YES:NO;
    isXiajiaGoods=btn.tag==0?NO:YES;
    btn.enabled=NO;
    btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    seleJisXiajBtn=btn;
    
    [self requestGoodsDetailWithKeywork:searKeywork];
}

#pragma mark - 点击批量管理
-(void)clickPiliangBtn{
    [UIView animateWithDuration:0.3 animations:^{
        if (isSelePiLiangBtn) {
            [pilBtn setTitle:@"批量管理" forState:UIControlStateNormal];
            pilBtn.backgroundColor=[UIColor whiteColor];
            [pilBtn setTitleColor:[UIColor colorWithHexString:@"#252525"] forState:UIControlStateNormal];
            fillBtmView.frame=CGRectMake(0, SCREENSIZE.height-64, SCREENSIZE.width, 54);
        }else{
            [pilBtn setTitle:@"取消管理" forState:UIControlStateNormal];
            pilBtn.backgroundColor=[UIColor colorWithHexString:@"#00a453"];
            [pilBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            fillBtmView.frame=CGRectMake(0, SCREENSIZE.height-118, SCREENSIZE.width, 54);
        }
        isSelePiLiangBtn=!isSelePiLiangBtn;
    } completion:^(BOOL finished) {
        
    }];
    [myTable reloadData];
}

#pragma mark - setUpTableView
-(void)setUpTableView{
    
    dataArr=[NSMutableArray array];
    
    searKeywork=@"";
    isXiajiaGoods=NO;
    isSeleJishou=YES;
    isSelePiLiangBtn=NO;
    
    pageInt=1;
    
    selectGoodsArr=[NSMutableArray array];
    
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 128, SCREENSIZE.width, SCREENSIZE.height-192) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //加载更多
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = [UIFont systemFontOfSize:14];
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (pageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestGoodsDetailWithKeywork:searKeywork];
        });
    }
}

-(void)setUpFillterBtmView{
    //
    fillBtmView=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-64, SCREENSIZE.width, 54)];
    fillBtmView.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:fillBtmView];
    
    CGFloat fbtnWidth=(SCREENSIZE.width-68)/3;
    CGFloat fbtnX=SCREENSIZE.width-(20+2*fbtnWidth);
    for (int i=0; i<2; i++) {
        fbtnX=fbtnX+i*fbtnWidth+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(fbtnX, 9, fbtnWidth, 36);
        btn.layer.cornerRadius=2;
        btn.backgroundColor=[UIColor colorWithHexString:i==0?@"#ea6b1f":@"#e13f3f"];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:i==0?@"批量下架":@"批量删除" forState:UIControlStateNormal];
        btn.tag=i;
        if (i==0) {
            bottom_plBtn=btn;
        }
        [btn addTarget:self action:@selector(clickBottomBtn:) forControlEvents:UIControlEventTouchUpInside];
        [fillBtmView addSubview:btn];
    }
}

#pragma mark - 点击底部批量下架/批量删除按钮
-(void)clickBottomBtn:(id)sender{
    if (selectGoodsArr.count<=0) {
        [AppUtils showSuccessMessage:@"您未选择任何商品!" inView:self.view];
    }else{
        UIButton *btn=sender;
        if (btn.tag==0) {
            reShangjiaAlertView = [[CustomIOSAlertView alloc] init];
            [reShangjiaAlertView setContainerView:[self createShangjiaAlertView:isSelePiLiangBtn]];
            [reShangjiaAlertView setButtonTitles:nil];
            
            [reShangjiaAlertView setUseMotionEffects:true];
            [reShangjiaAlertView show];
        }else{
            deleteAlertView = [[CustomIOSAlertView alloc] init];
            [deleteAlertView setContainerView:[self deleteGoodsAlertView:isSelePiLiangBtn]];
            [deleteAlertView setButtonTitles:nil];
            
            [deleteAlertView setUseMotionEffects:true];
            [deleteAlertView show];
        }
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str=[AppUtils getValueWithKey:@"SoldOutGoodsCell_Height"];
    return str.floatValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 4;
    }else{
        return 0.000000000001f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SoldOutGoodsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[SoldOutGoodsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.section<dataArr.count){
        MaijiaGoodsManagerModel *model=dataArr[indexPath.section];
        [cell reflushDataForModel:model andIsXiajia:isXiajiaGoods];
        cell.isSelePiliang=isSelePiLiangBtn;
        cell.delegate=self;
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Cell Delegate
//编辑商品
-(void)editGoods:(MaijiaGoodsManagerModel *)model{
    seleModel=model;
    GoodsEditController *ctr=[GoodsEditController new];
    ctr.goodsID=seleModel.goodsID;
    ctr.isGoodsManageer=isSeleJishou;
    [self.navigationController pushViewController:ctr animated:YES];
}

//重新上架
-(void)reShangjia:(MaijiaGoodsManagerModel *)model{
    seleModel=model;
    reShangjiaAlertView = [[CustomIOSAlertView alloc] init];
    [reShangjiaAlertView setContainerView:[self createShangjiaAlertView:isSelePiLiangBtn]];
    [reShangjiaAlertView setButtonTitles:nil];
    
    [reShangjiaAlertView setUseMotionEffects:true];
    [reShangjiaAlertView show];
}

//删除商品
-(void)deleteGoods:(MaijiaGoodsManagerModel *)model{
    seleModel=model;
    deleteAlertView = [[CustomIOSAlertView alloc] init];
    [deleteAlertView setContainerView:[self deleteGoodsAlertView:isSelePiLiangBtn]];
    [deleteAlertView setButtonTitles:nil];
    
    [deleteAlertView setUseMotionEffects:true];
    [deleteAlertView show];
}

-(void)selectedGoodCellForModel:(MaijiaGoodsManagerModel *)model indexPath:(NSIndexPath *)indexPath{
    
    /**
     *  判断当期是否为选中状态，如果选中状态点击则更改成未选中，如果未选中点击则更改成选中状态
     */
    MaijiaGoodsManagerModel *model1 =model;
    
    if (model1.selectState){
        model1.selectState = NO;
        [selectGoodsArr removeObject:model1];
    }
    else{
        model1.selectState = YES;
        [selectGoodsArr addObject:model1];
    }
    
    //刷新当前行
    [myTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - 创建重新上架的

-(UIView *)createShangjiaAlertView:(BOOL)isPiliang{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.layer.cornerRadius=6;
    view.layer.borderWidth=0.5f;
    view.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 24, view.width, 24)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    tipLabel.text=isXiajiaGoods?isPiliang?@"是否确认重新批量上架？":@"是否确认重新上架？":isPiliang?@"是否确认商品批量下架？":@"是否确认商品下架？";
    [view addSubview:tipLabel];
    
    //
    CGFloat lastYH=[self createShangjiaBtn:@[@"取消",isXiajiaGoods?@"确认上架":@"确认下架"] andToView:view afterView:tipLabel];
    
    if (isPiliang) {
        TTTAttributedLabel *ttt=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, lastYH+6, view.width, 19)];
        lastYH=ttt.origin.y+ttt.height;
        ttt.textAlignment=NSTextAlignmentCenter;
        ttt.font=[UIFont systemFontOfSize:13];
        ttt.textColor=[UIColor colorWithHexString:@"#252525"];
        [view addSubview:ttt];
        NSString *stageStr=[NSString stringWithFormat:@"（已选中%lu件商品）",(unsigned long)selectGoodsArr.count];
        [ttt setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%lu",(unsigned long)selectGoodsArr.count] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:14];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+24;
    view.frame=frame;
    
    return view;
}


-(CGFloat)createShangjiaBtn:(NSArray *)array andToView:(UIView *)view afterView:(UIView *)afterView{
    CGFloat lastYH=0;
    CGFloat btnX=36;
    CGFloat btnY=afterView.origin.y+afterView.height+17;
    CGFloat btnW=(view.width-36)/2;
    CGFloat btnHeight=40;
    for (int i=0; i<array.count; i++) {
        btnX=12+i*btnW+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        btn.layer.cornerRadius=2;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor colorWithHexString:i==array.count-1?@"e13f3f":@"#ea6b1f"];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikShangjiaBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        lastYH=btn.origin.y+btn.height+4;
    }
    return lastYH;
}

-(void)clcikShangjiaBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
            [reShangjiaAlertView close];
            break;
        default:
            if (isSelePiLiangBtn) {
                NSString *parStr=@"";
                for (int i=0; i<selectGoodsArr.count; i++) {
                    MaijiaGoodsManagerModel *model=selectGoodsArr[i];
                    NSString *str=model.goodsID;
                    parStr=[parStr stringByAppendingString:[NSString stringWithFormat:i==selectGoodsArr.count-1?@"%@":@"%@|",str]];
                }
                if (isXiajiaGoods) {
                    //批量上架
                    [self upGoodsRequestWithArray:parStr];
                }else{
                    //批量下架
                    [self offGoodsRequestWithArray:parStr];
                }
            }else{
                if (isXiajiaGoods) {
                    //单个上架
                    [self upGoodsRequestWithGoodsId:seleModel.goodsID];
                }else{
                    //单个下架
                    [self offGoodsRequestWithGoodsId:seleModel.goodsID];
                }
            }
            [reShangjiaAlertView close];
            break;
    }
}

#pragma mark - 创建删除商品

-(UIView *)deleteGoodsAlertView:(BOOL)isSelePil{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.layer.cornerRadius=6;
    view.layer.borderWidth=0.5f;
    view.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 24, view.width, 24)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    tipLabel.text=isSelePil?@"是否要批量删除商品？":@"是否要删除商品？";
    [view addSubview:tipLabel];
    
#pragma mark 验证码View
    UIView *volatileView=[[UIView alloc] initWithFrame:CGRectMake(view.width/4, tipLabel.origin.y+tipLabel.height+16, view.width*3/5, 40)];
    volatileView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    volatileView.center=CGPointMake(view.width/2, volatileView.center.y);
    volatileView.layer.borderWidth=0.5f;
    volatileView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [view addSubview:volatileView];
    
    UITextField *valiTF=[[UITextField alloc] initWithFrame:CGRectMake(2, 0, volatileView.width/2-4, volatileView.height)];
    valiTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    valiTF.placeholder=@"输入验证码";
    valiTF.font=[UIFont systemFontOfSize:15];
    valiTF.textColor=[UIColor colorWithHexString:@"#363636"];
    valiTF.keyboardType=UIKeyboardTypeNumberPad;
    valiTF.textAlignment=NSTextAlignmentCenter;
    [valiTF addTarget:self action:@selector(valiTFDidChange:) forControlEvents:UIControlEventEditingChanged];
    [volatileView addSubview:valiTF];
    
    UILabel *valiLabel=[[UILabel alloc] initWithFrame:CGRectMake(volatileView.width/2, 0, volatileView.width/2, volatileView.height)];
    valiLabel.backgroundColor=[UIColor colorWithHexString:@"#b7b7b7"];
    valiLabel.textAlignment=NSTextAlignmentCenter;
    valiLabel.font=[UIFont systemFontOfSize:17];
    valiLabel.textColor=[UIColor whiteColor];
    valiLabel.text=[self randomNumber];
    productValidate=[self randomNumber];
    [volatileView addSubview:valiLabel];
    
    //
    CGFloat lastYH=[self createDeleteBtn:@[@"取消",@"确认删除"] andToView:view afterView:volatileView];
    
    if (isSelePil) {
        TTTAttributedLabel *ttt=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, lastYH+6, view.width, 19)];
        lastYH=ttt.origin.y+ttt.height;
        ttt.textAlignment=NSTextAlignmentCenter;
        ttt.font=[UIFont systemFontOfSize:13];
        ttt.textColor=[UIColor colorWithHexString:@"#252525"];
        [view addSubview:ttt];
        NSString *stageStr=[NSString stringWithFormat:@"（已选中%lu件商品）",(unsigned long)selectGoodsArr.count];
        [ttt setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%lu",(unsigned long)selectGoodsArr.count] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:14];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+24;
    view.frame=frame;
    
    return view;
}


-(CGFloat)createDeleteBtn:(NSArray *)array andToView:(UIView *)view afterView:(UIView *)afterView{
    CGFloat lastYH=0;
    CGFloat btnX=36;
    CGFloat btnY=afterView.origin.y+afterView.height+17;
    CGFloat btnW=(view.width-36)/2;
    CGFloat btnHeight=38;
    for (int i=0; i<array.count; i++) {
        btnX=12+i*btnW+i*8;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        btn.layer.cornerRadius=2;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor colorWithHexString:i==array.count-1?@"e13f3f":@"#ea6b1f"];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        if (i==array.count-1) {
            btn.enabled=NO;
            btn.backgroundColor=[UIColor lightGrayColor];
            confirmDeleteBtn=btn;
        }
        lastYH=btn.origin.y+btn.height+4;
    }
    return lastYH;
}

-(void)clcikDeleteBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
            [deleteAlertView close];
            break;
        default:
        {
            if (![validateStr isEqualToString:productValidate]) {
                [AppUtils showSuccessMessage:@"请输入正确验证码" inView:self.view];
            }else{
                if (isSelePiLiangBtn) {
                    NSString *parStr=@"";
                    for (int i=0; i<selectGoodsArr.count; i++) {
                        MaijiaGoodsManagerModel *model=selectGoodsArr[i];
                        NSString *str=model.goodsID;
                        parStr=[parStr stringByAppendingString:[NSString stringWithFormat:i==selectGoodsArr.count-1?@"%@":@"%@|",str]];
                    }
                    [self deleteGoodsWithArray:parStr];
                }else{
                    [self deleteGoodsRequestWithGoodsId:seleModel.goodsID];
                }
            }
            [deleteAlertView close];
            break;
        }
    }
}

//生产随机数
-(NSString *)randomNumber{
    
    const int N = 4;
    
    NSString *sourceString = @"0123456789";
    
    NSMutableString *result = [[NSMutableString alloc] init];
    
    srand((int)time(0));
    
    for (int i = 0; i < N; i++){
        
        unsigned index = rand() % [sourceString length];
        
        NSString *s = [sourceString substringWithRange:NSMakeRange(index, 1)];
        
        [result appendString:[NSString stringWithFormat:@"%@",s]];
        
    }
    return result;
}

- (void)valiTFDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    validateStr=field.text;
    if (![field.text isEqualToString:@""]) {
        confirmDeleteBtn.backgroundColor=[UIColor colorWithHexString:@"e13f3f"];
        confirmDeleteBtn.enabled=YES;
    }else{
        confirmDeleteBtn.enabled=NO;
        confirmDeleteBtn.backgroundColor=[UIColor lightGrayColor];
    }
}

#pragma mark - Request 请求
-(void)requestGoodsDetailWithKeywork:(NSString *)keywork{
    
    /*
     on_goods_list 寄售中
     off_goods_list 下架
     */
    if (pageInt==1) {
        [AppUtils showProgressInView:self.view];
    }
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:isSeleJishou?@"on_goods_list":@"off_goods_list" forKey:@"act"];
    [dict setObject:keywork forKey:@"goods_name"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSString stringWithFormat:@"%ld",pageInt] forKey:@"page"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"goods_list"];
            NSArray *fqArr=responseObject[@"retData"][@"fenqi_fee"];
            for (NSDictionary *dic in arr) {
                MaijiaGoodsManagerModel *model=[MaijiaGoodsManagerModel new];
                [model jsonManagerDataForDictionary:dic];
                model.fenqi_fee=fqArr;
                model.selectState=NO;
                [dataArr addObject:model];
            }
            
            js_sum=responseObject[@"retData"][@"on_goods_count"];
            xj_sum=responseObject[@"retData"][@"off_goods_count"];
            [jsBtn setTitle:[NSString stringWithFormat:@"寄售中商品 %@",js_sum] forState:UIControlStateNormal];
            [xjBtn setTitle:[NSString stringWithFormat:@"下架中商品 %@",xj_sum] forState:UIControlStateNormal];
            
            //总页数
            NSString *totalPage=[responseObject[@"retData"][@"totalPageCount"] stringValue];
            if (pageInt<totalPage.integerValue) {
                pageInt++;
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
            
        }
        [myTable reloadData];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

-(void)deleteGoodsRequestWithGoodsId:(NSString *)goodsId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"del_a_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

-(void)deleteGoodsWithArray:(NSString *)goodsIds{
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"del_batch_goods" forKey:@"act"];
    [dict setObject:goodsIds forKey:@"goods_id_arr"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@-%@",responseObject,responseObject[@"retMsg"]);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//单个上架
-(void)upGoodsRequestWithGoodsId:(NSString *)goodsId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"up_a_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:@"商品上架成功" inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//批量上架
-(void)upGoodsRequestWithArray:(NSString *)goodsArr{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"up_batch_goods" forKey:@"act"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [dict setObject:goodsArr forKey:@"goods_id_arr"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:@"批量上架成功" inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//单个下架
-(void)offGoodsRequestWithGoodsId:(NSString *)goodsId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"off_a_goods" forKey:@"act"];
    [dict setObject:goodsId forKey:@"goods_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:@"商品下架成功" inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

//批量下架
-(void)offGoodsRequestWithArray:(NSString *)goodsArr{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"off_batch_goods" forKey:@"act"];
    [dict setObject:goodsArr forKey:@"goods_id_arr"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@manage_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([[responseObject[@"errCode"] stringValue] isEqualToString:@"0"]) {
            [AppUtils showSuccessMessage:@"批量下架成功" inView:self.view];
            // 延迟加载
            [dataArr removeAllObjects];
            [selectGoodsArr removeAllObjects];
            pageInt=1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestGoodsDetailWithKeywork:searKeywork];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retMsg"] inView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

@end
