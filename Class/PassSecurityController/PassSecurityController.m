//
//  PassSecurityController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "PassSecurityController.h"
#import "PassSecurityCell.h"
#import "PassWordModifyController.h"
#import "MailVerifyController.h"
#import "PhoneVerifyController.h"
#import "CancelCell.h"

#define EXPLAINSTRING01 @"互联网账号存在被盗的风险，建议您定期更改密码以保护账户安全。"
#define EXPLAINSTRING02 @"验证后，可用于快速找回登录密码及支付密码，接收账户余额变动提醒。"
@interface PassSecurityController ()<UITableViewDataSource,UITableViewDelegate,SafeCenterCancelDelegate>

@end

NSString *securityidentifier=@"securitycentercell";
@implementation PassSecurityController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    NSString *mailVerifyStr;
    NSString *phoneVerifyStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"安全中心"];
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self requestHttpsForUserId:[AppUtils getValueWithKey:User_ID]];
}

#pragma mark - init UI
-(void)setUpUI{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"securitycellid" bundle:nil] forCellReuseIdentifier:securityidentifier];
    [myTable registerNib:[UINib nibWithNibName:@"CancelCell" bundle:nil] forCellReuseIdentifier:@"CancelCell"];
    [self.view addSubview:myTable];
    myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section!=2) {
        static NSString *cellId=@"securitycellid";
        PassSecurityCell *cell=(PassSecurityCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (!cell) {
            cell=[[PassSecurityCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        //判断indexPath.row<行数增加健壮性
        if(indexPath.row<2){
            [cell reflushDataForIndexPath:indexPath verifyString:phoneVerifyStr];
            if (indexPath.section==1) {
                if (![phoneVerifyStr isEqualToString:@""]) {
                    cell.rightTitleLabel.text=phoneVerifyStr;
                }
            }
        }
        return  cell;
    }else{
        CancelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CancelCell"];
        if (!cell) {
            cell=[[CancelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CancelCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    UILabel *label = [[UILabel alloc] init] ;
    label.frame = CGRectMake(8, 14, CGRectGetWidth(tableView.frame)-16, 44);
    label.font = [UIFont systemFontOfSize:14];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    label.text = sectionTitle;
    label.numberOfLines=0;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 58)];
    [view addSubview:label];
    return view;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0||section==1) {
        return 58;
    }
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 0.0000001;
    }
    return 20;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return EXPLAINSTRING01;
    }else if(section==1){
        return EXPLAINSTRING02;
    }else{
        return nil;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PassWordModifyController *pwdModify=[[PassWordModifyController alloc]init];
    PhoneVerifyController *phoneVerify=[[PhoneVerifyController alloc]init];
    
    switch (indexPath.section) {
        case 0:
            [self.navigationController pushViewController:pwdModify animated:YES];
            break;
        case 1:
            phoneVerify.receiverPhone=phoneVerifyStr;
            [self.navigationController pushViewController:phoneVerify animated:YES];
            break;
        default:
            break;
    }
}

-(void)clickCancelButton{
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=23;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self logout];
                break;
            default:
                break;
        }
    };
    NSArray *items =
    @[MMItemMake(@"退出登录", MMItemTypeHighlight, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"退出后不会删除任何历史数据，下次登录依然可以使用本账号"
                                  items:items] showWithBlock:nil];
}

#pragma mark - 退出处理
//=================
//弹出登录、清空密码、
//发送通知以改变tabbar选中状态、
//并pop当前controller
//=================
-(void)logout{
    self.app.isLogin=NO;
    LoginController *login=[[LoginController alloc]init];
    UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
    [self presentViewController:loginNav animated:YES completion:^{
        [AppUtils saveValue:@"" forKey:User_Pass];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLogOut object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForUserId:(NSString *)userId{
    dataArr=[NSMutableArray array];
    [AppUtils showActivityIndicatorView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"safe_center" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissActivityIndicatorView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForUserId:[AppUtils getValueWithKey:User_ID]];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                if ([responseObject[@"retData"][@"hash_email"] isKindOfClass:[NSNull class]]) {
                    mailVerifyStr=@"";
                }else{
                    mailVerifyStr=responseObject[@"retData"][@"hash_email"];
                }
                if ([responseObject[@"retData"][@"hash_phone"] isKindOfClass:[NSNull class]]) {
                    phoneVerifyStr=@"";
                }else{
                    phoneVerifyStr=responseObject[@"retData"][@"hash_phone"];
                }
                
            }
            [self setUpUI];
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissActivityIndicatorView:self.view];
    }];
}

@end