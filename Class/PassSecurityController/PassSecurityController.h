//
//  PassSecurityController.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

//安全等级
typedef NS_ENUM(NSInteger, SecurityLevelType) {
    SecurityLevelLower = 0,
    SecurityLevelMedium,
    SecurityLevelHigher
};

@interface PassSecurityController : BaseController

@property (nonatomic, readonly) SecurityLevelType securityLevel;

@end
