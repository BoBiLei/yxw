//
//  AppDelegate.m
//  YXW
//
//  Created by mac on 15/11/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "AppDelegate.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import "JPUSHService.h"
#import "StrategyDetailController.h"

//
#import "UIView+TYLaunchAnimation.h"
#import "TYLaunchFadeScaleAnimation.h"
#import "UIImage+TYLaunchImage.h"

#import "YHTSdk.h"

#import <SDImageCache.h>
////////////////////////////////////
////////////////////////////////////
#define QQ_APPID @"101166591"
#define QQ_APPKEY @"9a93a747f8b916caf7d5fdfae1342027"

#define Sina_APPID @"3373841426"
#define Sina_APPKEY @"dcd8671717bbcd8c754a5657639d4f7f"
#define Sina_RedirectURL @"http://sns.whalecloud.com/sina2/callback"

#define WX_APPID @"wxc60eab93d2dba545"
#define WX_APPKEY @"6d033ba665bb11207b5c43fc13d06d5d"

@interface AppDelegate ()<WXApiDelegate,UIAlertViewDelegate>

@end

@implementation AppDelegate{
    
    //推送参数（包括type、id）
    NSString *tsTypeStr;
    NSString *tsTypeId;
    NSString *strateTitle;
    NSString *strateDescription;
    NSString *strateImg;
    
    RootViewController *rootViewCtr;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //配置IQKeyboardManager
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = YES;
    manager.shouldShowTextFieldPlaceholder=NO;
    
    //配置TallkingData
    [self configTallkingData];
    
    //初始化消息推送
    [self registerJPushWithOptions:launchOptions];
    
    //自动登录
    [self autoLogin];
    
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    [self customizeInterface];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    //
    [self initGuideviewController];
    
    
    //启动图片动画（必须 self.window.rootViewController = XXX之后）
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage ty_getLaunchImage]];
    [imageView showInWindowWithAnimation:[TYLaunchFadeScaleAnimation fadeScaleAnimation] completion:^(BOOL finished) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }];
    
    //注册share SDK
    [self registerShareSdk];
    
    return YES;
}

#pragma mark - 配置 TallkingData
-(void)configTallkingData{
    // App ID: 在 App Analytics 创建应用后，进入数据报表页中，在“系统设置”-“编辑应用”页面里查看App ID。
    // 渠道 ID: 是渠道标识符，可通过不同渠道单独追踪数据。
    [TalkingData setExceptionReportEnabled:YES];    //发送错误报告
    [TalkingData sessionStarted:@"D0EB77E52BF50E7BB4D36F898A61569F" withChannelId:@"AppStore"];
}

#pragma mark - 注册JPush
-(void)registerJPushWithOptions:(NSDictionary *)launchOptions{
    //[JPUSHService setDebugMode];      //打印调试bug
    //[JPUSHService setLogOFF];           //关闭日记
    //1.注册要处理的远程通知类型
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
         
                                              categories:nil];
    } else {
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }
    
    //2.启动SDK，（”apsForProduction“开发状态NO，生产状态YES）
    [JPUSHService setupWithOption:launchOptions appKey:@"74090705a0e19e8f1d165ce5" channel:@"AppStore" apsForProduction:YES];
    
}

#pragma mark 推送的

//应用接收到推送通知，
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
    DSLog(@"收到通知01:%@", userInfo);
    tsTypeStr=userInfo[@"type"];
    tsTypeId=userInfo[@"id"];
    // 应用在前台 或者后台开启状态下，不跳转页面，让用户选择。
    if (application.applicationState == UIApplicationStateActive || application.applicationState == UIApplicationStateBackground) {
        UIAlertView *alertView =[[UIAlertView alloc]initWithTitle:@"收到一条消息" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"查看", nil];
        [alertView show];
    }
    else{
        [self clickPush];
    }
}

// App 收到推送的通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    [JPUSHService handleRemoteNotification:userInfo];
    tsTypeStr=userInfo[@"type"];
    tsTypeId=userInfo[@"id"];
    
    // 应用在前台 或者后台开启状态下，不跳转页面，让用户选择。
    if (application.applicationState == UIApplicationStateActive || application.applicationState == UIApplicationStateBackground) {
        
        UIAlertView *alertView =[[UIAlertView alloc]initWithTitle:@"收到一条消息" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"查看", nil];
        [alertView show];
    }
    else{
        [self clickPush];
    }
}

//点击推送信息
-(void)clickPush{
    //重置脚标(jpush服务器的为0)
    [JPUSHService resetBadge];
    DSLog(@"\n%@--------%@",tsTypeStr,tsTypeId);
    
    StrategyDetailController *stratgeDetail=[[StrategyDetailController alloc]init];
    
    //（推送后台传递参数：1、type  2、id）
    switch (tsTypeStr.integerValue) {
            //====================
            //101-旅游攻略详情(参数：id/title/description/img)
            //102-岛屿详情
            //103-tab=2
            //
            //====================
        case 101:
            stratgeDetail.idStr=tsTypeId;
            stratgeDetail.strategyTitle=strateTitle;
            stratgeDetail.strategyDescription=strateDescription;
            stratgeDetail.strategyImgStr=strateImg;
//            [rootViewCtr.selectedViewController pushViewController:stratgeDetail animated:YES];
            break;
        case 102:
            rootViewCtr.selectedViewController.tabBarController.selectedIndex=2;
            break;
        case 103:
            rootViewCtr.selectedViewController.tabBarController.selectedIndex=3;
            break;
        default:
//            newMsg.msgId=tsTypeId;
//            [rootViewCtr.selectedViewController pushViewController:newMsg animated:YES];
            break;
    }
}

// 在 iOS8 系统中，还需要添加这个方法。通过新的 API 注册推送服务
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    [application registerForRemoteNotifications];
}

//获取deviceToken成功后，注册deviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    DSLog(@"获取DeviceToken成功");
    [JPUSHService registerDeviceToken:deviceToken];
}

// 当 DeviceToken 获取失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    DSLog(@"DeviceToken 获取失败，原因：%@",error);
}

//ios9推送
- (void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forLocalNotification:(UILocalNotification *)notification withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void(^)())completionHandler{
    DSLog(@"1111111");
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void(^)())completionHandler{
    DSLog(@"2222222");
}
//接收到本地推送
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}

#pragma mark - alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){
        [self clickPush];
    }
}

#pragma mark - 注册share SDK
-(void)registerShareSdk{
    /**
     *  设置ShareSDK的appKey，如果尚未在ShareSDK官网注册过App，请移步到http://mob.com/login 登录后台进行应用注册，
     *  在将生成的AppKey传入到此方法中。
     *  方法中的第二个第三个参数为需要连接社交平台SDK时触发，
     *  在此事件中写入连接代码。第四个参数则为配置本地社交平台时触发，根据返回的平台类型来配置平台信息。
     *  如果您使用的时服务端托管平台信息时，第二、四项参数可以传入nil，第三项参数则根据服务端托管平台来决定要连接的社交SDK。
     */
    [ShareSDK registerApp:@"ee2e81956992"
     
          activePlatforms:@[
                            @(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ),
                            @(SSDKPlatformTypeSinaWeibo)
                            ]
                 onImport:^(SSDKPlatformType platformType){
                     switch (platformType){
                         case SSDKPlatformTypeWechat:
                             [ShareSDKConnector connectWeChat:[WXApi class]];
                             break;
                         case SSDKPlatformTypeQQ:
                             [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                             break;
                         case SSDKPlatformTypeSinaWeibo:
                             [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                             break;
                         default:
                             break;
                     }
                 }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo){
              
              switch (platformType){
                  case SSDKPlatformTypeWechat:
                      [appInfo SSDKSetupWeChatByAppId:WX_APPID
                                            appSecret:WX_APPKEY];
                      break;
                  case SSDKPlatformTypeQQ:
                      [appInfo SSDKSetupQQByAppId:QQ_APPID
                                           appKey:QQ_APPKEY
                                         authType:SSDKAuthTypeBoth];
                      break;
                  case SSDKPlatformTypeSinaWeibo:
                      [appInfo SSDKSetupSinaWeiboByAppKey:Sina_APPID appSecret:Sina_APPKEY redirectUri:Sina_RedirectURL authType:SSDKAuthTypeBoth];
                      break;
                  default:
                      break;
              }
          }];
}

#pragma mark - 自动登录
-(void)autoLogin{
    /*
     *  lgTypeSTr=NULL      第一次安装或从未登录过
     *  lgTypeSTr=1         手机登录过
     *  lgTypeSTr=2         第三方登录过
     */
    NSString *lgTypeSTr=[AppUtils getValueWithKey:Login_Type];
    if (lgTypeSTr==NULL) {
        
    }else{
        if ([lgTypeSTr isEqualToString:@"1"]) {
            if (![[AppUtils getValueWithKey:User_Account] isEqualToString:@""]&&
                [AppUtils getValueWithKey:User_Account]!=NULL&&
                ![[AppUtils getValueWithKey:User_Pass] isEqualToString:@""]&&
                [AppUtils getValueWithKey:User_Pass]!=NULL){
                    [self requestForLoginName:[AppUtils getValueWithKey:User_Account] pass:[AppUtils getValueWithKey:User_Pass]];
            }
        }else if ([lgTypeSTr isEqualToString:@"2"]){
            
            if ([AppUtils getValueWithKey:SSO_Login_UID]!=NULL) {
                if (![[AppUtils getValueWithKey:SSO_Login_UID] isEqualToString:@""]) {
                    NSString *lgType=[AppUtils getValueWithKey:SSO_Login_Type];
                    if ([lgType isEqualToString:@"wxphone"]) {
                        //[self wxLoginRequest:[AppUtils getValueWithKey:yx_wx_Code]];
                    }else{
                        NSString *lgUid=[AppUtils getValueWithKey:SSO_Login_UID];
                        NSString *lgPhoto=[AppUtils getValueWithKey:User_Photo];
                        NSString *lgName=[AppUtils getValueWithKey:User_Name];
                        [self requestLoginWithType:lgType openId:lgUid faceImg:lgPhoto nickName:lgName];
                    }
                }
            }
        }
    }
}

#pragma mark - Request 请求
//手机登录
-(void)requestForLoginName:(NSString *)name pass:(NSString *)pass{
    
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"is_nosign":@"1",
                         @"mod":@"account_t",
                         @"code":@"Login",
                         @"op":@"done",
                         @"username":name,
                         @"password":pass
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            self.isLogin=YES;
            //Login_Type=1 手机登陆
            [AppUtils saveValue:@"1" forKey:Login_Type];
            [self updatePhotoUIAndLoginStatusWithDic:responseObject[@"retData"] name:name pass:pass];
        }else{
            self.isLogin=NO;
            [AppUtils showSuccessMessage:@"自动登录失败！" inView:self.window];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//保存账号、保存密码。标识程序登录状态
-(void)updatePhotoUIAndLoginStatusWithDic:(NSDictionary *)dic name:(NSString *)name pass:(NSString *)pass{
    self.isLogin=YES;
    [AppUtils saveValue:dic[@"user_id"] forKey:User_ID];
    [AppUtils saveValue:name forKey:User_Account];
    [AppUtils saveValue:name forKey:User_Name];
    [AppUtils saveValue:pass forKey:User_Pass];
    [AppUtils saveValue:dic[@"user_list"][@"face"] forKey:User_Photo];
    [AppUtils saveValue:dic[@"user_list"][@"phone"] forKey:User_Phone];
    
    NSString *roleStr=dic[@"user_list"][@"role_id"];
    if ([roleStr isEqualToString:@"11"]) {
        [AppUtils saveValue:@"1" forKey:VIP_Flag];
    }else{
        [AppUtils saveValue:@"0" forKey:VIP_Flag];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:nil];
}

//第三方登录
-(void)requestLoginWithType:(NSString *)loginType openId:(NSString *)openId faceImg:(NSString *)faceImg nickName:(NSString *)nickName{
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"is_nosign":@"1",
                         @"mod":@"account_t",
                         @"code":@"otherconn_login_ios",
                         @"type":loginType,
                         @"openid":openId,
                         @"face":faceImg,
                         @"nickname":nickName
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //DSLog(@"%@--",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            self.isLogin=YES;
            [AppUtils saveValue:loginType forKey:SSO_Login_Type];
            
            //Login_Type=2 第三方登陆
            [AppUtils saveValue:@"2" forKey:Login_Type];
            [self updateUIAndLoginStatusWithDic:responseObject[@"retData"]];
        }else{
            self.isLogin=NO;
            [AppUtils showSuccessMessage:@"自动登录失败！" inView:self.window];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)wxLoginRequest:(NSString *)code{
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"is_nosign":@"1",
                         @"mod":@"account_t",
                         @"code":@"thirdconn_login_iso",
                         @"wx_code":code,
                         @"type":@"wxphone"
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        //0516cFxR1uJZs71IF3xR1vIkxR16cFxO
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@--",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            [AppUtils saveValue:[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"unionid"]] forKey:SSO_Login_UID];
            [AppUtils saveValue:code forKey:yx_wx_Code];
            [AppUtils saveValue:@"wxphone" forKey:SSO_Login_Type];
            [AppUtils saveValue:@"2" forKey:Login_Type];
            [self updateUIAndLoginStatusWithDic:responseObject[@"retData"]];
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"result"] inView:self.window];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//=====================================
//登录成功保存账号、保存密码。标识程序登录状态。
//保存用户的信息：id、头像、用户名、可用余额
//发送通知改变tabbar index
//=====================================
-(void)updateUIAndLoginStatusWithDic:(NSDictionary *)dic{
    self.isLogin=YES;
    [AppUtils saveValue:dic[@"uid"] forKey:User_ID];
    [AppUtils saveValue:dic[@"truename"] forKey:User_Name];
    [AppUtils saveValue:dic[@"face"] forKey:User_Photo];
    
    [AppUtils saveValue:dic[@"phone"] forKey:User_Phone];
    [AppUtils saveValue:dic[@"bind_user_id"] forKey:Bind_User_Id];
    
    //0--未修改过的  1--修改过的
    [AppUtils saveValue:dic[@"is_new_password"] forKey:Is_NewPwd];
    
    
    NSString *roleStr=dic[@"user_list"][@"role_id"];
    if ([roleStr isEqualToString:@"11"]) {
        [AppUtils saveValue:@"1" forKey:VIP_Flag];
    }else{
        [AppUtils saveValue:@"0" forKey:VIP_Flag];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:nil];
}

#pragma mark - 设置全局导航条颜色
- (void)customizeInterface {
    UINavigationBar *barAppearance = [UINavigationBar appearance];
    UIImage *backgroundImage = nil;
    NSDictionary *textAttributes = nil;
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        backgroundImage = [UIImage imageNamed:@"checkad_head"];
        textAttributes = @{
                           NSFontAttributeName:kFontSize18,
                           NSForegroundColorAttributeName: [UIColor whiteColor],
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
        backgroundImage = [UIImage imageNamed:@"checkad_head"];
        textAttributes = @{
                           NSFontAttributeName:kFontSize18,
                           NSForegroundColorAttributeName: [UIColor whiteColor],
                           };
#endif
    }
    [barAppearance setBackgroundImage:backgroundImage
                                  forBarMetrics:UIBarMetricsDefault];
    //title颜色
    [barAppearance setTitleTextAttributes:textAttributes];
    //barbutton item颜色
    [barAppearance setTintColor:[UIColor whiteColor]];
    [barAppearance setBarTintColor:[UIColor colorWithHexString:@"0195ff"alpha:1.0f]];
}

#pragma mark - 初始化引导页
/*
 当第一次安装APP或是APP更新时（根据判断保存的版本号是否相同）
 */
-(void)initGuideviewController{
    if ([AppUtils getValueWithKey:@"HanUpdateVersion"]==nil||
        ![[AppUtils getValueWithKey:@"HanUpdateVersion"] isEqualToString:[AppUtils getCurrentVersion]]) {
        GuideViewController *guideView = [[GuideViewController alloc]init];
        self.window.rootViewController = guideView;
    }else{
        rootViewCtr=[[RootViewController alloc]init];
        self.window.rootViewController=rootViewCtr;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    //重置Jpush脚标(jpush服务器的为0)
    [JPUSHService resetBadge];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

#pragma mark -
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return [WXApi handleOpenURL:url delegate:self];
}

-(BOOL)application:(UIApplication*)app openURL:(NSURL*)url options:(NSDictionary<NSString *,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url
                                                  standbyCallback:^(NSDictionary *resultDic) {
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
                                                  }];
    }
    if ([url.host isEqualToString:@"platformapi"]){
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
        }];
    }
    return  [WXApi handleOpenURL:url delegate:self];
}

#pragma mark--配置支付宝客户端返回url处理方法--
// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url
                                                  standbyCallback:^(NSDictionary *resultDic) {
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
                                                  }];
    }
    if ([url.host isEqualToString:@"platformapi"]){
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
        }];
    }
    return  [WXApi handleOpenURL:url delegate:self];
}

    
#pragma mark - WinXin delegate
-(void) onResp:(BaseResp*)resp{
    NSLog(@"errCode=%d",resp.errCode);
    if ([resp isKindOfClass:[PayResp class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WXPAY_SENDER" object:resp];
    }
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *obj=(SendAuthResp *)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WXAuth_SUCCESS" object:obj];
    }
}

@end
