//
//  AppDelegate.h
//  YXW
//
//  Created by mac on 15/11/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) Boolean isLogin;      //用来判断用户是否登录

@property (nonatomic, assign) NSInteger hp_reflush_reusable;    //用来判断首页是否第一次
@property (nonatomic, assign) NSInteger md_reflush_reusable;    //用来判断首页是否第一次

@end

