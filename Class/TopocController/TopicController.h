//
//  TopicController.h
//  MMG
//
//  Created by mac on 15/10/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

typedef NS_ENUM(NSUInteger, TopicImgUrl) {
    ImgUrl01,
    ImgUrl02
};

@interface TopicController : BaseController

@property(nonatomic,copy) NSString *idStr;

@property (nonatomic) TopicImgUrl imgUrlType;

@end
