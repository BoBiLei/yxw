//
//  TopicController.m
//  MMG
//
//  Created by mac on 15/10/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "TopicController.h"
#import "ProductListCell.h"
#import "TopicCollectionHeaderView.h"
#import "TopicCell.h"
#import "ProductDetailController.h"
#import "ProductListController.h"

@interface TopicController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,TopicCollectionHeaderDelegate>

@end

@implementation TopicController{
    NSMutableArray *picArr;
    NSMutableArray *dataArr;
    NSDictionary *reusableviewDic;
    UICollectionView *collectView;
    TopicCollectionHeaderView *coHeaderView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@""];
    
    [self setUpUI];
    
    [self requestDataForId:self.idStr];
}

#pragma mark init UI
-(void)setUpUI{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    collectView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:myLayout];
    [collectView registerNib:[UINib nibWithNibName:@"ProductListCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [collectView registerNib:[UINib nibWithNibName:@"TopicCell" bundle:nil] forCellWithReuseIdentifier:@"topiccell"];
    collectView.backgroundColor=[UIColor clearColor];
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.userInteractionEnabled=YES;
    bagView.image=[UIImage imageNamed:@"login_baground"];
    collectView.backgroundView=bagView;
    collectView.dataSource=self;
    collectView.delegate=self;
    [collectView registerNib:[UINib nibWithNibName:@"TopicCollectionHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    /*
     *下拉刷新
     */
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestDataForId:self.idStr];
    }];
    header.arrowView.hidden=NO;
    // 设置字体
    header.stateLabel.font = [UIFont systemFontOfSize:12];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:11];
    collectView.mj_header = header;
    
    [self.view addSubview:collectView];
}


#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) {
        return picArr.count;
    }else{
        return dataArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        TopicCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"topiccell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[TopicCell alloc]init];
        }
        if (indexPath.row<picArr.count) {
            NSString *str=picArr[indexPath.row];
            NSRange rang=[str rangeOfString:@"m/"];
            if (rang.location != NSNotFound) {
                str=[str substringFromIndex:rang.location+rang.length];
            }
            [cell.imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost,str]]];
        }
        return cell;
    }else{
        ProductListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[ProductListCell alloc]init];
        }
        cell.layer.borderWidth=0.5f;
        cell.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
        if (indexPath.row<dataArr.count) {
            ProductListModel *model=dataArr[indexPath.row];
            [cell reflushTopicDataForModel:model];
        }
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, 160);
    }else{
        return CGSizeMake((SCREENSIZE.width)/2-14, 242);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if(section==0){
        return 0.0f;
    }else{
        return 12.0f;
    }
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==0) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else{
        return UIEdgeInsetsMake(8, 8, 0, 8);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        
    }else{
        ProductListModel *model=dataArr[indexPath.row];
        ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
        prodectDetail.goodId=model.idStr;
        prodectDetail.goodTitle=model.titleStr;
        prodectDetail.goodImgStr=model.imgStr;
        prodectDetail.goodPrice=model.mmPriceStr;
        [self.navigationController pushViewController:prodectDetail animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    if (indexPath.section==0) {
        if (kind == UICollectionElementKindSectionHeader){
            UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
            reusableview = headerView;
        }
    }else{
        coHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        coHeaderView.delegate=self;
        reusableview = coHeaderView;
    }
    return reusableview;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==0) {
        CGSize size=CGSizeMake(0, 0);
        return size;
    }else{
        CGSize size=CGSizeMake(SCREENSIZE.width, 45);
        return size;
    }
}

#pragma mark - Topic delegate
-(void)getMore:(NSString *)strId{
    ProductListController *productList=[[ProductListController alloc]init];
    productList.productListId=strId;
    productList.productListName=@"推荐商品";
    productList.requestType=HomePageGoodsListRequestType;
    [self.navigationController pushViewController:productList animated:YES];
}

#pragma mark - Request 请求
-(void)requestDataForId:(NSString *)topicId{
    
    picArr=[NSMutableArray array];
    
    dataArr=[NSMutableArray array];
    
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"topic_id":topicId
                         };
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"topic.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //
            NSString *titleStr=responseObject[@"retData"][@"page_title"];
            [self setCustomNavigationTitle:titleStr];
            //图片
            NSArray *picStrArr=responseObject[@"retData"][@"pic"];
            for (NSString *picStr in picStrArr) {
                [picArr addObject:picStr];
            }
            
            reusableviewDic=responseObject[@"retData"];
            [coHeaderView reflushDataForDictionary:reusableviewDic];
            //商品信息
            NSArray *goodStrArr=responseObject[@"retData"][@"sort_goods_arr"][@"all_goods"];
            for (NSDictionary *goodStrDic in goodStrArr) {
                ProductListModel *model=[[ProductListModel alloc]init];
                [model jsonDataForTopicDictionary:goodStrDic];
                [dataArr addObject:model];
            }
        }
        [collectView reloadData];
        [collectView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
