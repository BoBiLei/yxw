//
//  TopicCollectionHeaderView.h
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TopicCollectionHeaderDelegate <NSObject>

@optional
/**
 更多
 */
-(void)getMore:(NSString *)strId;
@end

@interface TopicCollectionHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (copy, nonatomic) NSString *rightButtonId;
@property (weak, nonatomic) id<TopicCollectionHeaderDelegate> delegate;

-(void)reflushDataForDictionary:(NSDictionary *)dic;

@end
