//
//  TopicCollectionHeaderView.m
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "TopicCollectionHeaderView.h"

@implementation TopicCollectionHeaderView

- (void)awakeFromNib {
    
}

-(void)reflushDataForDictionary:(NSDictionary *)dic{
    self.backgroundColor=[UIColor groupTableViewBackgroundColor];
    self.rightButtonId=dic[@"all_goods_more"];
//    self.leftLabel.text=dic[@"page_title"];
    self.leftLabel.text=@"推荐商品";
    self.leftLabel.font=[UIFont systemFontOfSize:16];
    self.leftLabel.textColor=[UIColor blackColor];
    [self.rightButton setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    [self.rightButton setTitle:@"MORE" forState:UIControlStateNormal];
    [self.rightButton setImage:[UIImage imageNamed:@"s_more"] forState:UIControlStateNormal];
    self.rightButton.titleEdgeInsets=UIEdgeInsetsMake(0,0,0,8);
    self.rightButton.imageEdgeInsets=UIEdgeInsetsMake(0,80,0,0);
}

- (IBAction)clickBtn:(id)sender {
     [self.delegate getMore:self.rightButtonId];
}

@end
