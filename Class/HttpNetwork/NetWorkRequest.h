//
//  HttpRequest.h
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 游侠旅游. All rights reserved.
//

#import "AFNetworking.h"

//Request请求类型
typedef NS_ENUM(NSInteger, HttpRequestType) {
    HttpRequestGet,
    HttpRequestPost,
};

/**
 *  请求开始前预处理Block
 */
typedef void(^PrepareExecuteBlock)(void);

/****************   RTHttpClient   ****************/
@interface NetWorkRequest : NSObject{
    
    BOOL isConnectNetWork;
}

+ (NetWorkRequest *)defaultClient;

/**
 *  HTTP请求（GET、POST）
 *
 *  @param path
 *  @param method     Request请求类型
 *  @param parameters 请求参数
 *  @param prepare    请求前预处理块
 *  @param success    请求成功处理块
 *  @param failure    请求失败处理块
 */
- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters
         prepareExecute:(PrepareExecuteBlock) prepare
                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
