//
//  NetWorkStatus.h
//  youxia
//
//  Created by mac on 16/6/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//
//
//  只需放在每个RootView中
//
//

#import <Foundation/Foundation.h>

typedef void (^NetWorkStatusBlock)(BOOL status);

@interface NetWorkStatus : NSObject

@property (nonatomic, copy) NetWorkStatusBlock NetWorkStatusBlock;

- (instancetype)init;

@end
