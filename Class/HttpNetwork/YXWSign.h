//
//  YXWSign.h
//  YXW
//
//  Created by mac on 15/11/10.
//  Copyright (c) 2015年 游侠旅游. All rights reserved.
//
/**
 ******************************
 ************签名方法***********
 ******************************
 **/
#import <Foundation/Foundation.h>

@interface YXWSign : NSObject

+(YXWSign *)shareSign;

+(NSString *)randomNumber;

-(NSString *)getSign:(NSString *)sec forDictionary:(NSDictionary *)dic;

@end
