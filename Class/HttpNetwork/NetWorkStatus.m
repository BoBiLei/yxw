//
//  NetWorkStatus.m
//  youxia
//
//  Created by mac on 16/6/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NetWorkStatus.h"

@implementation NetWorkStatus

- (instancetype)init{
    self=[super init];
    if (self) {
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        
        __block   BOOL network =  network ;
        
        __block   BOOL change =  change ;
        
        change = NO;
        
        network = NO;
        
        // 检测网络连接的单例,网络变化时的回调方法
        
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
         {
             switch (status) {
                 case AFNetworkReachabilityStatusNotReachable:
                     network = NO;
                     change = YES;
                     break;
                 case AFNetworkReachabilityStatusReachableViaWiFi:
                     network = YES;
                     change = YES;
                     break;
                 case AFNetworkReachabilityStatusReachableViaWWAN:
                     network = YES;
                     change = YES;
                     break;
                 default:
                     break;
             }
             
             if (network) {
                 [JDStatusBarNotification dismiss];
             }else{
                 [JDStatusBarNotification showWithStatus:@"网络似乎已断开，请检查网络设置！"];
             }
             
             if (self.NetWorkStatusBlock){
                 self.NetWorkStatusBlock(network);
             }
         }];
    }
    return self;
}

@end
