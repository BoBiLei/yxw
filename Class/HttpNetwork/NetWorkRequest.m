//
//  HttpRequest.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 游侠旅游. All rights reserved.
//
#import "NetWorkRequest.h"
#import <AFNetworkReachabilityManager.h>

typedef void(^SuccessBlock)(NSURLSessionDataTask *operation, id responseObject);
typedef void(^FailureBlock)(NSURLSessionDataTask *operation, NSError *error);

@interface NetWorkRequest(){
    AppDelegate *_delegate;
}

@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation NetWorkRequest

- (id)init{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configSession
        =[NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLCache *cache=[[NSURLCache alloc]initWithMemoryCapacity:3*1024*1024
                                                       diskCapacity:8*1024*1024
                                                           diskPath:nil];
        configSession.URLCache=cache;
        //设置缓存策略
        configSession.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        configSession.timeoutIntervalForRequest=300; //设置超时时间
        
        _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configSession];
        
        //证书模式
//        _manager.securityPolicy = [NetWorkRequest customSecurityPolicy];
        
        //设置response序列化器
        AFJSONResponseSerializer * response = [AFJSONResponseSerializer serializer];
        _manager.responseSerializer = response;
        
        //接受的类型
        [_manager.responseSerializer setAcceptableContentTypes:
         [NSSet setWithObjects:
          @"text/plain",
          @"application/json",
          @"text/json",
          @"text/javascript",
          @"text/html",
          nil]];
    }
    return self;
}

//支持https
+ (AFSecurityPolicy *)customSecurityPolicy{
    //AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    //allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    //如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    securityPolicy.validatesDomainName = YES;
    
    //导入证书，找到证书的路径
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"youxiaserver" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    NSSet *set = [[NSSet alloc] initWithObjects:certData, nil];
    securityPolicy.pinnedCertificates = set;
    
    return securityPolicy;
}

+ (NetWorkRequest *)defaultClient{
    static NetWorkRequest *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters
         prepareExecute:(PrepareExecuteBlock)prepareExecute
                success:(void (^)(NSURLSessionDataTask *, id))success
                failure:(void (^)(NSURLSessionDataTask *, NSError *))failure{
    
    _delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    //预处理（显示加载信息啥的）
    if (prepareExecute) {
        prepareExecute();
//        DSLog(@"\n请求参数：%@",parameters);
    }
    
    //检查地址中是否有中文
    NSString *urlStr=[NSURL URLWithString:url]?url:[self strUTF8Encoding:url];
    
    switch (method) {
        case HttpRequestGet:{
            [self openActivity];
            
            [_manager GET:urlStr parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(task,responseObject);
                [self closeActivity];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(task,error);
                if (error.code==-1001) {
                    [self showExceptionDialog:error];
                }
                [self closeActivity];
            }];
        }
            break;
            
        case HttpRequestPost:{
            [self openActivity];
            
            [_manager POST:urlStr parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(task,responseObject);
                [self closeActivity];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(task,error);
                if (error.code==-1001) {
                    [self showExceptionDialog:error];
                }
                [self closeActivity];
            }];
        }
            break;
        default:
            break;
    }
}

-(NSString *)strUTF8Encoding:(NSString *)str{
    return [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}

- (void)showExceptionDialog:(NSError *)error{
    [[[UIAlertView alloc] initWithTitle:@"提示"
                                    message:error.localizedDescription
                                   delegate:self
                          cancelButtonTitle:@"好的"
                          otherButtonTitles:nil, nil] show];
}

// 开启状态栏菊花
- (void)openActivity{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

// 关闭状态栏菊花
- (void)closeActivity{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
@end

