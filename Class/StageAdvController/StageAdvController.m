//
//  StageAdvController.m
//  MMG
//
//  Created by mac on 15/11/2.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "StageAdvController.h"
#import "AdvDetailCell.h"

@interface StageAdvController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation StageAdvController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"分期购物"];
    [self setUpUI];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"StageAdv" ofType:@"plist"];
    NSArray *plistArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    for (NSDictionary *dic in plistArr) {
        AdvDetailModel *model=[[AdvDetailModel alloc]init];
        [model jsonDataForDictionary:dic];
        [dataArr addObject:model];
    }
    [myTable reloadData];
}

#pragma mark - init UI
-(void)setUpUI{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [myTable registerNib:[UINib nibWithNibName:@"AdvDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
    myTable.tableFooterView=[[UIView alloc]init];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    AdvDetailModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    AdvDetailModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    return  cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
