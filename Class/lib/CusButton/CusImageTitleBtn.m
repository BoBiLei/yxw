//
//  CusImageTitleBtn.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CusImageTitleBtn.h"

@interface CusImageTitleBtn()

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIImage *image;

@end

@implementation CusImageTitleBtn

-(id)initWithFrame:(CGRect)frame title:(NSString *)title image:(UIImage *)image{
    self=[super initWithFrame:frame];
    if (self) {
        self.title=title;
        self.image=image;
        [self addSubview:self.imgv];
        [self addSubview:self.cusTitleLabel];
    }
    return self;
}

-(UIImageView *)imgv{
    if (!_imgv) {
        _imgv=[[UIImageView alloc] init];
        _imgv.image=self.image;
    }
    return _imgv;
}

-(UILabel *)cusTitleLabel{
    if (!_cusTitleLabel) {
        _cusTitleLabel=[[UILabel alloc] init];
        _cusTitleLabel.text=self.title;
        _cusTitleLabel.font=kFontSize15;
        _cusTitleLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _cusTitleLabel;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    _cusTitleLabel.frame=CGRectMake(0, self.height/3*2, self.width, self.height/3);
    _imgv.frame=CGRectMake((self.width-self.height/3*2)/2, 4, self.height/3*2, self.height/3*2);
    
}

@end
