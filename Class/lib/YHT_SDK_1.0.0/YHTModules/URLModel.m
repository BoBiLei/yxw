//
//  URLModel.m
//  CloudContract_SDK
//
//  Created by 吴清正 on 16/5/6.
//  Copyright © 2016年 dazheng_wu. All rights reserved.
//

#import "URLModel.h"

@implementation URLModel

NSString * const kSignContract_URL         = @"sdk/contract/sign";

NSString * const kInvalidContract_URL      = @"sdk/contract/invalid";

NSString * const kViewContract_URL         = @"sdk/contract/view";

NSString * const kViewSignature_URL        = @"sdk/sign/getSign";

NSString * const kGenerateSignature_URL    = @"sdk/sign/createSign";

NSString * const kDeleteSignature_URL      = @"sdk/sign/delSign";

NSString * const kViewWebContract_URL      = @"sdk/contract/mobile/view";

NSString * const kToken_URL                = @"sdkdemo/token";

NSString * const kTokenContract_URL        = @"sdkdemo/token_contract";

+ (NSString *)host {

    NSString *__returnURL = @"http://sdk.yunhetong.com";

#ifndef TEST_SERVICE
    __returnURL = @"http://sdk.yunhetong.com";

#else
    __returnURL = @"http://192.168.1.56:9999";
#endif

    return __returnURL;
}

+ (NSString *)urlByHost:(NSString *)path{
    NSURL *url = [[NSURL URLWithString:[self host]] URLByAppendingPathComponent:path];
//    DSLog(@"服务器url=%@",url.absoluteString);
    return url.absoluteString;
}

@end