//
//  YHTSignMadeViewController.m
//  CloudContract_SDK
//
//  Created by 吴清正 on 16/5/9.
//  Copyright © 2016年 dazheng_wu. All rights reserved.
//

#import "YHTSignMadeViewController.h"
#import "YHT_PPSSignatureView.h"
#import "UIButton+Wqz.h"
#import "NSData+Base64.h"

@interface YHTSignMadeViewController ()<YHTHttpRequestDelegate>

@property (nonatomic, strong) YHT_PPSSignatureView *signView;

@end

@implementation YHTSignMadeViewController

+ (instancetype)instanceWithDelegate:(id<YHTSignMadeViewDelegate>)delegate{
    YHTSignMadeViewController *__vc = [[YHTSignMadeViewController alloc] init];
    __vc.delegate = delegate;

    return __vc;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    if (_kIOS_VERSION >= 8.0) {
        CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
        [UIView animateWithDuration:duration animations:^{
            //View controller-based status bar appearance 设置为NO，否则状态栏不消失
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
            
            self.navigationController.view.transform = CGAffineTransformIdentity;
            self.navigationController.view.transform = CGAffineTransformMakeRotation(M_PI / 2);
            self.navigationController.view.bounds = CGRectMake(0, 0, _kIOS_SCREEN_HEIGHT, _kIOS_SCREEN_WIDTH);
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //显示状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [UIView animateWithDuration:duration animations:^{
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
        self.navigationController.view.transform = CGAffineTransformIdentity;
        self.navigationController.view.transform = [self accessToTheSizeOfTheRotatingAngle];
        self.navigationController.view.bounds = CGRectMake(0, 0, _kIOS_SCREEN_WIDTH, _kIOS_SCREEN_HEIGHT);
    }];
}

//获取需要旋转角度的大小
- (CGAffineTransform)accessToTheSizeOfTheRotatingAngle{
    //获取当前状态栏的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    //根据当前状态栏的方向，获取需要旋转的角度的大小
    if (orientation == UIInterfaceOrientationLandscapeLeft) {
        return CGAffineTransformMakeRotation(M_PI*1.5);
    } else if (orientation == UIInterfaceOrientationLandscapeRight) {
        return CGAffineTransformMakeRotation(M_PI/2);
    } else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
        return CGAffineTransformMakeRotation(-M_PI);
    } else {
        return CGAffineTransformIdentity;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    EAGLContext *context = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (_kIOS_VERSION >= 8.0) {
        self.signView = [[YHT_PPSSignatureView alloc] initWithFrame:CGRectMake(0, 64, _kIOS_SCREEN_HEIGHT, _kIOS_SCREEN_WIDTH-64) context:context];
    }else{
        self.signView = [[YHT_PPSSignatureView alloc] initWithFrame:CGRectMake(0, 64, _kIOS_SCREEN_WIDTH, _kIOS_SCREEN_HEIGHT-64) context:context];
    }
    
    [self.view addSubview:self.signView] ;
    self.view.userInteractionEnabled = YES;

    [self setUpCustomSearBar];
    
    UIButton *__clearButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [__clearButton setTitle:@"清除" forState:UIControlStateNormal];
    [__clearButton setTitle:@"清除" forState:UIControlStateHighlighted];
    [__clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [__clearButton setBackgroundColor:_kMAIN_COLOR];
    __clearButton.frame = CGRectMake(self.signView.frame.size.width - 70, self.signView.frame.size.height - 10, 55, 30);
    [__clearButton wqz_setButtonRadius:5.0f withColor:_kMAIN_COLOR withWidth:1.0f];
    [__clearButton addTarget:self action:@selector(clearSignEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:__clearButton];

    UIButton *__adopButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [__adopButton setTitle:@"采用" forState:UIControlStateNormal];
    [__adopButton setTitle:@"采用" forState:UIControlStateHighlighted];
    [__adopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [__adopButton setBackgroundColor:_kMAIN_COLOR];
    __adopButton.frame = CGRectMake(self.signView.frame.size.width - 70, self.signView.frame.size.height - 10 - 40, 55, 30);
    [__adopButton wqz_setButtonRadius:5.0f withColor:_kMAIN_COLOR withWidth:1.0f];
    [__adopButton addTarget:self action:@selector(saveSignEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:__adopButton];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.height, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.height-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"绘制签名";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)clickReturnBtn{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 清除
- (void)clearSignEvent:(id)sender {
    [self.signView erase];
}

#pragma mark - 采用
- (void)saveSignEvent:(id)sender {
    
    NSString *signStr = [UIImagePNGRepresentation(self.signView.signatureImage) base64EncodedString];

    NSString *baseString = (__bridge NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                         (CFStringRef)signStr,
                                                                                         NULL,
                                                                                         CFSTR(":/?#[]@!$&’()*+,;="),
                                                                                         kCFStringEncodingUTF8);

    DSLog(@"madeviewsignStr =%@",signStr);
    if (!baseString) {
        UIAlertView *_alertView = [[UIAlertView alloc] initWithTitle:@"签名不能为空"
                                                             message:@"签名不能为空"
                                                            delegate:self
                                                   cancelButtonTitle:@"确定"
                                                   otherButtonTitles:nil];
        [_alertView show];
        return;
    }

    [[YHTSignManager sharedManager] generateSignatureWithSignData:baseString
                                                           tag:@"GenerateSignature"
                                                      delegate:self];
}

//TGT-1707-KaOiRjebu0dhxfKbIjKvb9eCtF4MIkiYJIefSsU64P3dni0Spo-cas01.example.org
//TGT-1709-GHSNXnKdmwIqimruymwza7BH9Kwq25txdmhNMyFCI0PvjgDauZ-cas01.example.org
#pragma mark - 签名请求后的 Delegate
//成功
-(void)request:(YHTHttpRequest *)request didFinishLoadingWithResult:(id)result{
    [self.delegate onMadeSignSuccessed:self];
    if ([request.tag isEqualToString:@"GenerateSignature"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//失败
- (void)request:(YHTHttpRequest *)request didFailWithError:(NSError *)error{
    if ([request.tag isEqualToString:@"GenerateSignature"]) {
        DSLog(@"%@",error.localizedDescription);
    }
}

@end