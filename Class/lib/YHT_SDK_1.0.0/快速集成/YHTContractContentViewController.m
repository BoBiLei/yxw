//
//  YHTContractContentViewController.m
//  CloudContract_SDK
//
//  Created by 吴清正 on 16/5/10.
//  Copyright © 2016年 dazheng_wu. All rights reserved.
//

#import "YHTContractContentViewController.h"
#import "YHTContractWebView.h"
#import "YHTContractPartner.h"
#import "YHTContractOperateMenu.h"
#import "UIImage+Wqz.h"
#import "YHTContract.h"
@interface YHTContractContentViewController ()<YHTHttpRequestDelegate>

@property (nonatomic, strong)YHTContractPartner *partner;

@property (nonatomic, strong)YHTContractOperateMenu *operateMenu;

@property (nonatomic, strong)YHTContractWebView *webView;

@property (nonatomic, strong)NSNumber *contractID;

@property (nonatomic, strong)NSString *contractType;

@property (nonatomic, strong)NSString *orderId;

@property (nonatomic, assign) BOOL isxt;

@end

@implementation YHTContractContentViewController{
    UIButton *rightBtn;
}

+ (instancetype)instanceWithContractID:(NSNumber *)contractID contractType:(NSString *)contractType orderId:(NSString *)orderId isXieTong:(BOOL)isXieTong{
    
    YHTContractContentViewController *vc = [[YHTContractContentViewController alloc] init];
    vc.contractID = contractID;
    vc.contractType = contractType;
    vc.orderId = orderId;
    vc.isxt=isXieTong;
    return vc;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"签署合同页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavBar];
    
    self.webView = [YHTContractWebView instanceWithContractID:_contractID delegate:nil];
    [self.view addSubview:self.webView];
    
    //0.15秒后加载
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self webViewRefresh];
    });
}

-(void)setUpNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"签署合同";
    [self.view addSubview:navBar];
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.hidden=YES;
    rightBtn.frame=CGRectMake(navBar.width-64, 20, 64, 49);
    rightBtn.backgroundColor=[UIColor clearColor];
    rightBtn.titleLabel.font=kFontSize16;
    [rightBtn setTitle:@"操作" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:rightBtn];
}

#pragma mark - 刷新 webView
- (void)webViewRefresh{
    
    [self.webView refresh];
    
    [[YHTContractManager sharedManager] viewContactWithContractID:_contractID
                                                              tag:@"ViewContract"
                                                         delegate:self];
}

#pragma mark - 设置按钮是否显示
- (void)rightBarButtonItemRefresh{
    if (self.partner.invalid == NO && self.partner.sign == NO) {
        rightBtn.hidden=YES;
    }else{
        rightBtn.hidden=NO;
    }
}

- (void)buttonClick{
    [self.operateMenu show];
}

#pragma mark - Request Delegate
//请求成功
- (void)request:(YHTHttpRequest *)request didFinishLoadingWithResult:(id)result{
    if ([request.tag isEqualToString:@"ViewContract"]) {
        self.partner = [YHTContractPartner instanceWithDict:result[@"value"][@"partner"]];
        [self rightBarButtonItemRefresh];
    }else if ([request.tag isEqualToString:@"SignContract"]){
        [self submitHTDateToServer:self.orderId];
    }else if ([request.tag isEqualToString:@"InvalidContract"]){
        [self webViewRefresh];
        [self rightBarButtonItemRefresh];
    }
}

//请求失败
- (void)request:(YHTHttpRequest *)request didFailWithError:(NSError *)error{
    DSLog(@"%@",error.localizedDescription);
}

#pragma mark - GET/SET
- (YHTContractPartner *)partner{
    if (!_partner) {
        _partner = [[YHTContractPartner alloc] init];
    }
    
    return _partner;
}

- (YHTContractOperateMenu *)operateMenu{
    if (self.partner == nil || [self.partner titlesAndOperateTypes] == nil) {
        self.navigationItem.rightBarButtonItem = nil;
        return nil;
    }
    
    _operateMenu = [YHTContractOperateMenu instanceWithContract:self.partner delegate:self];
    
    return _operateMenu;
}

#pragma mark - 点击签名或作废 Delegate
- (void)didSelectedOperate:(ContractOperateType)__operateType{
    if(__operateType == ContractOperateType_Sign){
        
//        [self didPushWithViewController];
        
        [[YHTContractManager sharedManager] signContractWithContractID:self.contractID
                                                                   tag:@"SignContract"
                                                              delegate:self];
        
    }else if (__operateType == ContractOperateType_Invalid) {
        [[YHTContractManager sharedManager] invalidContractWithContractID:self.contractID
                                                                      tag:@"InvalidContract"
                                                                 delegate:self];
        
    }
}


-(void)didPushWithViewController{
    [self.navigationController pushViewController:[YHTSignMadeViewController instanceWithDelegate:self] animated:YES];
}

#pragma mark - 签名成功 Delegate
- (void)onMadeSignSuccessed:(id)__id{
    [[YHTContractManager sharedManager] signContractWithContractID:self.contractID
                                                               tag:@"SignContract"
                                                          delegate:self];
}

#pragma mark - 提交合同状态
-(void)submitHTDateToServer:(NSString *)orderId{
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"hetong" forKey:@"mod"];
//    [dict setObject:@"update_ht" forKey:@"code"];
    [dict setObject:self.isxt?@"update_ht2_xie":@"update_ht2" forKey:@"code"];
    [dict setObject:self.contractType forKey:@"type"];
    [dict setObject:orderId forKey:@"yxid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@--",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self submitHTDateToServer:orderId];
            });
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"error"]];
            if ([codeStr isEqualToString:@"0"]) {
                if ([self.contractType isEqualToString:@"1"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"JP_QianShu_Success" object:nil];
                }else if ([self.contractType isEqualToString:@"2"]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"JD_QianShu_Success" object:nil];
                }else if ([self.contractType isEqualToString:@"3"]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"FQ_QianShu_Success" object:nil];
                }else if ([self.contractType isEqualToString:@"4"]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"XT_QianShu_Success" object:nil];
                }
                [self webViewRefresh];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
