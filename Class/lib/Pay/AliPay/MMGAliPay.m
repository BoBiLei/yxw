//
//  MMGAliPay.m
//  MMG
//
//  Created by dingzhi100-03 on 15/5/14.
//  Copyright (c) 2015年 深圳市方云互联网有限公司. All rights reserved.
//

#import "MMGAliPay.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PartnerConfig.h"
#import "Order.h"
#import "DataSigner.h"

@interface MMGAliPay ()<UIAlertViewDelegate>

@end

@implementation MMGAliPay
-(void)sendAliPay{
    /*
     *商户的唯一的parnter和seller。
     *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
     */
    NSString *partner = PartnerID;
    NSString *seller = SellerID;
    NSString *privateKey = PartnerPrivKey;
    //partner和seller获取失败,提示
    if ([partner length] == 0 ||
        [seller length] == 0 ||
        [privateKey length] == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少partner或者seller或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    /****************
     *生成订单信息及签名
     ****************/
    Order *order = [[Order alloc] init];
    order.partner = partner;
    order.seller = seller;
    order.tradeNO = [self generateTradeNO];    //流水账号ID（由商家自行制定）
    order.productName = self.shopName;         //商品标题
    order.productDescription = self.shopDescription; //商品描述
    order.amount = [NSString stringWithFormat:@"%@",self.money]; //总金额
    order.notifyURL =  @"http://www.xxx.com";  //pc回调URL
    order.service = @"mobile.securitypay.pay"; //接口名称
    order.paymentType = @"1";                  //默认为1（商品购买）
    order.inputCharset = @"utf-8";             //固定为utf-8编码
    order.itBPay = @"60m";                     //交易超时时间
    order.showUrl = @"m.alipay.com";
    
    //将订单信息拼接成字符串
    NSString *orderSpec = [order description];
    DSLog(@"%@",orderSpec);
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"youxiaalipay";
    
    //获取私钥并将订单信息签名
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    
    /*
     app_id=2015120100894559&biz_content={"body":"order:2016121219214","subject":"2016121219214","out_trade_no":"2016121219214","total_amount":0.01,"product_code":"QUICK_MSECURITY_PAY"}&charset=UTF-8&format=json&method=alipay.trade.app.pay&notify_url=http://www.youxia.com/mgo/index.php?mod=movie_order_paydo&code=notice_alipay_forandroid&is_iso=1&sign_type=RSA&timestamp=2016-12-12 19:50:11&version=1.0&sign=Gp+Bh48t+NlOQsVOwfhtzWpE5FiD9aWcuLPFSyF55J4WpjzgOo3xsWWmla9ACQQ+vaTqKU1QBadvixyX+eMEZVWK9VB08k4Q8TBHTMW2IXdpIcESZQzsL1x+xgIVw5ei0DkOVcw16agJRvC5eKETqsXci+uv3I0rkKSphBLF06k=
     */
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        //支付异步请求并回调
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
            DSLog(@"resultDic=%@",resultDic);
        }];
    }
}

#pragma mark   ==============定义订单号==============
- (NSString *)generateTradeNO{
    const int N = 15;
    NSString *sourceString = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *result = [[NSMutableString alloc] init] ;
    srand((int)time(0));
    for (int i = 0; i < N; i++){
        unsigned index = rand() % [sourceString length];
        NSString *s = [sourceString substringWithRange:NSMakeRange(index, 1)];
        [result appendString:[NSString stringWithFormat:@"%@",s]];
    }
    return [NSString stringWithFormat:@"YXW%@",result];
}
@end
