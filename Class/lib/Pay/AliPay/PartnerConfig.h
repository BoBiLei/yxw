//
//  PartnerConfig.h
//  AlipaySdkDemo
//
//  Created by ChaoGanYing on 13-5-3.
//  Copyright (c) 2013年 RenFei. All rights reserved.
//
//  提示：如何获取安全校验码和合作身份者id
//  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
//  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
//  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
//

#ifndef MQPDemo_PartnerConfig_h
#define MQPDemo_PartnerConfig_h

//合作身份者id，以2088开头的16位纯数字
#define PartnerID @"2088911712038545"
//收款支付宝账号
#define SellerID  @"pay7@youxia.cn"

//（微信的）安全校验码（MD5）密钥，以数字和字母组成的32位字符
#define MD5_KEY @"s6U8c0Wy1O40hToJ34x0FbK4nzY8a4uq5"

//商户私钥 pkcs8格式
#define PartnerPrivKey @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALT5qLO215cbm3eNHdkLlr8+trcU0ujPm2ckUJw6HLH0y8lhaSqGUhTnasObqA8/kAh0qoOYanQDPCQHH+WR9MtjtF2P1UiRGKaY/8ObFWpCkxB096oeyS+ucL9/0bSJWp0oJIAgsnaDrqvo9f3+LEdX1jAZaSW8+NWjsjuH2I2jAgMBAAECgYEAjF8NhH+wz0shlmRFn4FGpwYs0Or9AIlvP1xCjjrg4QNAQe4llU9kQUSSYjDM9S7XOEfNro69G0kbD9La4cUJOp12SH+zBy9ZbqLyxxyLHRmrtGjmEjLF5WsGrlOkIN6BwTL9jpYnUtVeZ6W93W9C96nWia6Kqr5+ovAb3oiIemECQQDuN15IN5+3SVGXK1+2UDgm8VMFyq58h4g92hLT8tArwSbZbX8StGR4Z8Au/SSjzjEaQAxBWYtkUPArlVHVxnCxAkEAwnxWcIlax4FvCrdd61DrRrtRYwQieRX8LsBLy5xkkP6naSVBq1SpRkyFV4I7oHU6Zpbh+0LzFODMDFn6NPlYkwJAHKg0g2ONpzL+ybzCCuCjoKuvXScAzqJPVYGCelTfjiPyVH3Dp7Bj1chFamGxorMPXSUPVF09U5gZDGUuLxVsQQJAO+aB1Q1pCVODOOC7K5PzswhUi9OlLngoANo3PhIAI2xbcZYJANUFgeJlm0tARC52+0vzdCjtp3uJPTClUJWPwwJBAN5JmwNDpZo+xhmm8L25Mfs1SLb/vfpw9OoeCcWjU7pYIAMF75gHxScJb3tmuMLTmkMUhP6pLNbUuhvNAm7WQas="

//支付宝公钥
#define AlipayPubKey   @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB"
#endif
