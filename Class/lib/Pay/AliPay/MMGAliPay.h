//
//  MMGAliPay.h
//  MMG
//
//  Created by dingzhi100-03 on 15/5/14.
//  Copyright (c) 2015年 深圳市方云互联网有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *shop delegate
 **/
@protocol PayFinishDelegate <NSObject>
-(void)pushOrderDetailViewWithController;
@end

@interface MMGAliPay : NSObject
@property(nonatomic,copy) NSString *shopName;
@property(nonatomic,copy) NSString *shopDescription;
@property(nonatomic,copy) NSString *money;
@property(nonatomic,copy) NSString *orderNum;
@property id<PayFinishDelegate> payFinishDelegate;
-(void)sendAliPay;
@end
