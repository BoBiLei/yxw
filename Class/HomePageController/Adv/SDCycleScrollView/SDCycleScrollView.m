//
//  SDCycleScrollView.m
//  SDCycleScrollView
//
//  Created by aier on 15-3-22.
//  Copyright (c) 2015年 GSD. All rights reserved.
//

/**
 *******************************************************
 *                                                      *
 * 感谢您的支持， 如果下载的代码在使用过程中出现BUG或者其他问题    *
 * 您可以发邮件到gsdios@126.com 或者 到                       *
 * https://github.com/gsdios?tab=repositories 提交问题     *
 *                                                      *
 *******************************************************
 */

#import "SDCycleScrollView.h"
#import "SDCollectionViewCell.h"
#import "UIView+SDExtension.h"
#import "TAPageControl.h"
#import "NSData+SDDataCache.h"
#import "ImageModel.h"
#import <SDWebImageManager.h>

static NSString * const ID = @"cycleCell";

@interface SDCycleScrollView () <UICollectionViewDataSource, UICollectionViewDelegate>

@end

@implementation SDCycleScrollView{
    UICollectionView *imageCollectView; // 显示图片的collectionView
    UICollectionViewFlowLayout *flowLayout;
    NSInteger totalItemsCount;
    NSTimer *sdTimer;
    NSMutableArray *dataArr;
    UIControl *customPageControl;
    NSInteger imgCount;//图片数量
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI{
    _pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    _autoScrollTimeInterval = 2.0;
    _titleLabelTextColor = [UIColor whiteColor];
    _titleLabelTextFont= [UIFont systemFontOfSize:14];
    _titleLabelBackgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    _titleLabelHeight = 30;
    _autoScroll = YES;
    _infiniteLoop = YES;
    _showPageControl = YES;
    _pageControlDotSize = CGSizeMake(10, 10);
    _pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    self.backgroundColor = [UIColor lightGrayColor];
    
    flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = self.frame.size;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    imageCollectView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    imageCollectView.backgroundColor = [UIColor clearColor];
    imageCollectView.pagingEnabled = YES;
    imageCollectView.showsHorizontalScrollIndicator = NO;
    imageCollectView.showsVerticalScrollIndicator = NO;
    [imageCollectView registerClass:[SDCollectionViewCell class] forCellWithReuseIdentifier:ID];
    imageCollectView.dataSource = self;
    imageCollectView.delegate = self;
    [self addSubview:imageCollectView];
}

#pragma mark - properties
- (void)setPageControlDotSize:(CGSize)pageControlDotSize{
    _pageControlDotSize = pageControlDotSize;
    if ([customPageControl isKindOfClass:[TAPageControl class]]) {
        TAPageControl *pageContol = (TAPageControl *)customPageControl;
        pageContol.dotSize = pageControlDotSize;
    }
}

- (void)setShowPageControl:(BOOL)showPageControl{
    _showPageControl = showPageControl;
    
    customPageControl.hidden = !showPageControl;
}

- (void)setDotColor:(UIColor *)dotColor{
    _dotColor = dotColor;
    if ([customPageControl isKindOfClass:[TAPageControl class]]) {
        TAPageControl *pageControl = (TAPageControl *)customPageControl;
        pageControl.dotColor = dotColor;
    } else {
        UIPageControl *pageControl = (UIPageControl *)customPageControl;
        pageControl.currentPageIndicatorTintColor = dotColor;
    }
}

-(void)setAutoScroll:(BOOL)autoScroll{
    _autoScroll = autoScroll;
    [sdTimer invalidate];
    sdTimer = nil;
    
    if (_autoScroll) {
        [self setupTimer];
    }
}

- (void)setAutoScrollTimeInterval:(CGFloat)autoScrollTimeInterval{
    _autoScrollTimeInterval = autoScrollTimeInterval;
    
    [self setAutoScroll:self.autoScroll];
}

- (void)setPageControlStyle:(SDCycleScrollViewPageContolStyle)pageControlStyle{
    
    _pageControlStyle = pageControlStyle;
    
    [self setupPageControl];
}

#pragma mark - 设置imageURLStringGroup

- (void)setImageURLStringsGroup:(NSArray *)imageURLArr{
    _imageURLStringsGroup = imageURLArr;
    
    dataArr = [NSMutableArray array];
    
    for (NSDictionary *dic in imageURLArr) {
        ImageModel *model=[[ImageModel alloc]init];
        [model jsonDataForDictionary:dic];
        [dataArr addObject:model];
    }
    
    imgCount=dataArr.count;
    
    totalItemsCount = self.infiniteLoop ? imgCount * 100 : imgCount;
    
    if (dataArr.count != 1) {
        imageCollectView.scrollEnabled = YES;
        [self setAutoScroll:self.autoScroll];
    } else {
        imageCollectView.scrollEnabled = NO;
    }
    
    [self setupPageControl];
    
    [imageCollectView reloadData];
    
    /*
    for (int i=0; i<imageURLArr.count; i++) {
        ImageModel *model=[[ImageModel alloc]init];
        model.imgStr=imageURLArr[i];
        NSData *data = [NSData getDataCacheWithIdentifier:model.imgStr];
        if (data) {
            [dataArr setObject:[UIImage imageWithData:data] atIndexedSubscript:i];
        }
    }*/
}

- (void)setupPageControl{
    if (customPageControl) [customPageControl removeFromSuperview]; // 重新加载数据时调整
    
    switch (self.pageControlStyle) {
        case SDCycleScrollViewPageContolStyleAnimated:
        {
            TAPageControl *pageControl = [[TAPageControl alloc] init];
            pageControl.numberOfPages = imgCount;
            pageControl.dotColor = self.dotColor;
            [self addSubview:pageControl];
            customPageControl = pageControl;
        }
            break;
            
        case SDCycleScrollViewPageContolStyleClassic:
        {
            UIPageControl *pageControl = [[UIPageControl alloc] init];
            pageControl.numberOfPages = imgCount;
            pageControl.currentPageIndicatorTintColor = self.dotColor;
            [self addSubview:pageControl];
            customPageControl = pageControl;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - timer
- (void)setupTimer{
    sdTimer = [NSTimer scheduledTimerWithTimeInterval:self.autoScrollTimeInterval target:self selector:@selector(automaticScroll) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:sdTimer forMode:NSRunLoopCommonModes];
}

- (void)automaticScroll{
    if (0 == totalItemsCount) return;
    int currentIndex = imageCollectView.contentOffset.x / flowLayout.itemSize.width;
    int targetIndex = currentIndex + 1;
    if (targetIndex == totalItemsCount) {
        if (self.infiniteLoop) {
            targetIndex = totalItemsCount * 0.5;
        }else{
            targetIndex = 0;
        }
        [imageCollectView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:targetIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    }
    [imageCollectView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:targetIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}

#pragma mark - life circles

- (void)layoutSubviews{
    [super layoutSubviews];
    
    flowLayout.itemSize = self.frame.size;
    
    imageCollectView.frame = self.bounds;
    if (imageCollectView.contentOffset.x == 0 &&  totalItemsCount) {
        int targetIndex = 0;
        if (self.infiniteLoop) {
            targetIndex = totalItemsCount * 0.5;
        }else{
            targetIndex = 0;
        }
        [imageCollectView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:targetIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    }
    
    CGSize size = CGSizeZero;
    if ([customPageControl isKindOfClass:[TAPageControl class]]) {
        TAPageControl *pageControl = (TAPageControl *)customPageControl;
        size = [pageControl sizeForNumberOfPages:imgCount];
    } else {
        size = CGSizeMake(imgCount * self.pageControlDotSize.width * 1.2, self.pageControlDotSize.height);
    }
    CGFloat x = (self.sd_width - size.width) * 0.5;
    if (self.pageControlAliment == SDCycleScrollViewPageContolAlimentRight) {
        x = imageCollectView.sd_width - size.width - 10;
    }
    CGFloat y = imageCollectView.sd_height - size.height - 10;
    
    if ([customPageControl isKindOfClass:[TAPageControl class]]) {
        TAPageControl *pageControl = (TAPageControl *)customPageControl;
        [pageControl sizeToFit];
    }
    
    customPageControl.frame = CGRectMake(x, y, size.width, size.height);
    customPageControl.hidden = !_showPageControl;
    
}

//解决当父View释放时，当前视图因为被Timer强引用而不能释放的问题
- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (!newSuperview) {
        [sdTimer invalidate];
        sdTimer = nil;
    }
}

//解决当timer释放后 回调scrollViewDidScroll时访问野指针导致崩溃
- (void)dealloc {
    imageCollectView.delegate = nil;
    imageCollectView.dataSource = nil;
}

#pragma mark - public actions

- (void)clearCache{
    [NSData clearCache];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return totalItemsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SDCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    long itemIndex = indexPath.item % imgCount;
    ImageModel *model=dataArr[itemIndex];
    NSString *imgUrl=[NSString stringWithFormat:@"%@%@",ImageHost,model.imgStr];
    [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"placeholder_320x160"]];
    if (_titlesGroup.count) {
        cell.title = _titlesGroup[itemIndex];
    }
    
    if (!cell.hasConfigured) {
        cell.titleLabelBackgroundColor = self.titleLabelBackgroundColor;
        cell.titleLabelHeight = self.titleLabelHeight;
        cell.titleLabelTextColor = self.titleLabelTextColor;
        cell.titleLabelTextFont = self.titleLabelTextFont;
        cell.hasConfigured = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    long itemIndex = indexPath.item % imgCount;
    ImageModel *model=dataArr[itemIndex];
    if ([self.delegate respondsToSelector:@selector(cycleScrollView:didSelectItemAtIndex:andAdvType:)]) {
        [self.delegate cycleScrollView:self didSelectItemAtIndex:model.imgId andAdvType:model.advType];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int itemIndex = (scrollView.contentOffset.x + imageCollectView.sd_width * 0.5) / imageCollectView.sd_width;
    if (!imgCount) return; // 解决清除timer时偶尔会出现的问题
    int indexOnPageControl = itemIndex % imgCount;
    
    if ([customPageControl isKindOfClass:[TAPageControl class]]) {
        TAPageControl *pageControl = (TAPageControl *)customPageControl;
        pageControl.currentPage = indexOnPageControl;
    } else {
        UIPageControl *pageControl = (UIPageControl *)customPageControl;
        pageControl.currentPage = indexOnPageControl;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.autoScroll) {
        [sdTimer invalidate];
        sdTimer = nil;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (self.autoScroll) {
        [self setupTimer];
    }
}

@end
