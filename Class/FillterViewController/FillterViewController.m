//
//  FillterViewController.m
//  MMG
//
//  Created by mac on 15/11/17.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "FillterViewController.h"
#import "FillterHeaderView.h"
#import "FillterViewModel.h"
#import "FillterViewCell.h"
#import "FillterIDModel.h"
@interface FillterViewController ()<UITableViewDataSource,UITableViewDelegate,FillterViewDelegate,SubCategoryDelegate>

@end

@implementation FillterViewController{
    
    NSMutableArray *categoryDataArr;    //分类
    NSMutableArray *brandDataArr;       //品牌
    NSMutableArray *priceDataArr;       //价格
    
    NSMutableArray *dataArr;
    NSMutableArray *reflushDataArr;
    UITableView *myTable;
    
    BOOL isSele;
    NSInteger selectBtnTag;
    
    //选择的按钮ID
    NSString *selectCategoryId;
    NSString *selectSubCategoryId;
    NSString *selectBrandId;
    NSString *min_priceId;
    NSString *max_priceId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"筛选信息"];
    
    [self setUpRightBarButtonItem];
    
    [self setUpUI];
    
    [self requestHttpsFillterList];
}

-(void)setUpRightBarButtonItem{
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 60, 30);
    [rightBtn setTitle:@"确认" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(1, 20, -1, -20)];
    UIBarButtonItem *_rightViewItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    _rightViewItem.badgeBGColor = [UIColor redColor];
    _rightViewItem.badgeTextColor = [UIColor whiteColor];
    [rightBtn addTarget:self action:@selector(clickYesBtn) forControlEvents:UIControlEventTouchDown];
    self.navigationItem.rightBarButtonItem = _rightViewItem;
}

#pragma mark - UIBarButtonItem event
-(void)clickYesBtn{
    FillterIDModel *model=[[FillterIDModel alloc]init];
    model.categoryId=selectCategoryId;
    model.subCategoryId=selectSubCategoryId;
    model.brandId=selectBrandId;
    model.min_priceId=min_priceId;
    model.max_priceId=max_priceId;
    [[NSNotificationCenter defaultCenter] postNotificationName:_isBusinessFillter?@"Business_REFLUSH_FILLTERDATA":@"REFLUSH_FILLTERDATA" object:model];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init UI
-(void)setUpUI{
    //默认的ID
    selectCategoryId=@"0";
    selectSubCategoryId=@"0";
    selectBrandId=@"0";
    min_priceId=@"0";
    max_priceId=@"0";
    //
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.bounces=NO;
    myTable.sectionFooterHeight = 1.0;
    [self.view addSubview:myTable];
    myTable.tableFooterView=[[UIView alloc]init];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        if (!isSele) {
            return 0;
        }else{
            return 1;
        }
    }else{
        return 0;
    }
}

//一共多少组
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

//组头高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    FillterHeaderView *headerView=[[FillterHeaderView alloc]init];
    if (section==0) {
        headerView.dataArr=categoryDataArr;
        [headerView relushCategoryDataForArray];
    }
    else{
        if (section==1) {
            headerView.dataArr=brandDataArr;
        }else{
            headerView.dataArr=priceDataArr;
        }
        [headerView relushOtherDataForArray];
    }
    return headerView.frame.size.height;
}

//返回组头的VIew
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    FillterHeaderView *headerView=[[FillterHeaderView alloc]init];
    headerView.backgroundColor=[UIColor whiteColor];
    if (section==0) {
        headerView.dataArr=categoryDataArr;
        headerView.leftLabel.text=@"分类";
        headerView.selectBtnTag=selectBtnTag;
        [headerView relushCategoryDataForArray];
        headerView.selectedBtnType=tyFillterCategory;
    }
    else{
        if (section==1) {
            headerView.leftLabel.text=@"品牌";
            headerView.dataArr=brandDataArr;
            headerView.selectedBtnType=tyFillterBrand;
        }else{
            headerView.leftLabel.text=@"价格";
            headerView.dataArr=priceDataArr;
            headerView.selectedBtnType=tyFillterPrice;
        }
        [headerView relushOtherDataForArray];
    }
    headerView.delegate=self;
    return  headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    FillterViewCell *cell=[[FillterViewCell alloc]init];
    if (indexPath.section==0) {
        cell.dataArr=reflushDataArr;
    }
    [cell relushDataForArray];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FillterViewCell *cell=[[FillterViewCell alloc]init];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (indexPath.section==0) {
        cell.dataArr=reflushDataArr;
    }
    [cell relushDataForArray];
    cell.delegate=self;
    selectSubCategoryId=@"0"; //每次刷新后都会跳到全部，重新设为0
    return  cell;
}

#pragma mark - fillterview delegate
-(void)didSelectedButtonForModel:(NSArray *)array andSelectButton:(UIButton *)button andSelectId:(NSString *)selectedId{
    selectCategoryId=selectedId;
    selectBtnTag=button.tag;
    reflushDataArr=[NSMutableArray arrayWithArray:array];
    if(array.count<=1){
        isSele=NO;
    }else{
        isSele=YES;
    }
    //刷新对应的组
    [myTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.enabled=NO;
}
-(void)didSelectedBrandButtonForSelectId:(NSString *)selectedId{
    selectBrandId=selectedId;
}

-(void)didSelectedPriceButtonForSelectId:(NSString *)selectedId{
    if ([selectedId isEqualToString:@"0"]) {
        min_priceId=@"0";
        max_priceId=@"0";
    }else if ([selectedId isEqualToString:@"1"]){
        min_priceId=@"0";
        max_priceId=@"10000";
    }else if ([selectedId isEqualToString:@"2"]){
        min_priceId=@"10001";
        max_priceId=@"50000";
    }else if ([selectedId isEqualToString:@"3"]){
        min_priceId=@"50001";
        max_priceId=@"100000";
    }else{
        min_priceId=@"100001";
        max_priceId=@"100000000";
    }
    
}
-(void)didSelectedSubButtonForModel:(FillterViewModel *)model{
    selectSubCategoryId=model.categoryId;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsFillterList{
    categoryDataArr=[NSMutableArray array];
    brandDataArr=[NSMutableArray array];
    priceDataArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"advanced_search_filter"
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"search.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject[@"retData"][@"categories"]);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //分类的
            NSArray *categoryArr=responseObject[@"retData"][@"categories"];
            FillterViewModel *categoryModel=[[FillterViewModel alloc]init];
            categoryModel.categoryId=@"0";
            categoryModel.categoryName=@"全部";
            categoryModel.categoryArray=[NSArray array];
            [categoryDataArr addObject:categoryModel];
            for (NSDictionary *categoryDic in categoryArr) {
                FillterViewModel *model=[[FillterViewModel alloc]init];
                [model jsonDataForDictionary:categoryDic];
                [categoryDataArr addObject:model];
            }
            
            //品牌的
            NSArray *brandArr=responseObject[@"retData"][@"brand_list"];
            FillterViewModel *brandModel=[[FillterViewModel alloc]init];
            brandModel.categoryId=@"0";
            brandModel.categoryName=@"全部";
            [brandDataArr addObject:brandModel];
            for (NSDictionary *brandDic in brandArr) {
                FillterViewModel *model=[[FillterViewModel alloc]init];
                [model brandJsonDataForDictionary:brandDic];
                [brandDataArr addObject:model];
            }
            
            //价格的
            NSArray *priceArr=@[@{@"id":@"0",@"name":@"全部"},
                                @{@"id":@"1",@"name":@"10000以下"},
                                @{@"id":@"2",@"name":@"10001-50000"},
                                @{@"id":@"3",@"name":@"50001-100000"},
                                @{@"id":@"4",@"name":@"100001以上"}
                                ];
            for (NSDictionary *priceDic in priceArr) {
                FillterViewModel *model=[[FillterViewModel alloc]init];
                [model brandJsonDataForDictionary:priceDic];
                [priceDataArr addObject:model];
            }
            
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
