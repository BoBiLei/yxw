//
//  CouponController.m
//  CatShopping
//
//  Created by mac on 15/9/15.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "CouponController.h"
#import "CouponCell.h"
#import "AddCouponController.h"

NSString *couponIdentifier=@"couponcell";
@interface CouponController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation CouponController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    NSIndexPath *selectedIndexPath;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"优惠券"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestHttpsForCoupon) name:@"UpDateCoupon" object:nil];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 60, 30);
    [rightBtn setTitle:@"添加" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(1, 20, -1, -20)];
    UIBarButtonItem *_rightViewItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    _rightViewItem.badgeBGColor = [UIColor redColor];
    _rightViewItem.badgeTextColor = [UIColor whiteColor];
    [rightBtn addTarget:self action:@selector(clickAddCouponBtn) forControlEvents:UIControlEventTouchDown];
    self.navigationItem.rightBarButtonItem = _rightViewItem;
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self requestHttpsForCoupon];
}

#pragma mark - 点击添加按钮
-(void)clickAddCouponBtn{
    AddCouponController *addCoupon=[[AddCouponController alloc]init];
    addCoupon.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:addCoupon animated:YES];
}

#pragma mark - init view
-(void)setUpUI{
    if (dataArr.count>0) {
        myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
        myTable.dataSource=self;
        myTable.delegate=self;
        myTable.sectionHeaderHeight = 50;
        [myTable registerNib:[UINib nibWithNibName:@"CouponCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self.view addSubview:myTable];
        myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UILabel class]]) {
                [view removeFromSuperview];
            }
        }
    }else{
        UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 21)];
        tipLabel.center=CGPointMake(SCREENSIZE.width/2, (SCREENSIZE.height-113)/2);
        tipLabel.textAlignment=NSTextAlignmentCenter;
        tipLabel.text=@"您暂时没有优惠券";
        tipLabel.font=[UIFont systemFontOfSize:16];
        tipLabel.textColor=[UIColor lightGrayColor];
        [self.view addSubview:tipLabel];
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UITableView class]]) {
                [view removeFromSuperview];
            }
        }
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 32;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CouponCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[CouponCell alloc]init];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row<dataArr.count){
        CouponModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return  cell;
    
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 83;
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 48)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    CGRect frame=headerSectionView.frame;
    frame.size.height=83;
    
    UIImageView *imgv=[UIImageView new];
    imgv.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headerSectionView addSubview:imgv];
    imgv.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* imgv_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imgv]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgv)];
    [NSLayoutConstraint activateConstraints:imgv_h];
    
    NSArray* imgv_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgv(14)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgv)];
    [NSLayoutConstraint activateConstraints:imgv_w];
    
    //
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 14, SCREENSIZE.width/2+20, headerSectionView.frame.size.height-10)];
    titleLabel.font=[UIFont systemFontOfSize:15];
    titleLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [headerSectionView addSubview:titleLabel];
    
    //
    
    titleLabel.text=@"已绑定礼品卡";
    headerSectionView.frame=frame;
    //top line
    UIView *line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [headerSectionView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel][line(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //白色view
    UIView *mView=[[UIView alloc]init];
    mView.backgroundColor=[UIColor whiteColor];
    [headerSectionView addSubview:mView];
    mView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* mView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(mView)];
    [NSLayoutConstraint activateConstraints:mView_h];
    
    NSArray* mView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line][mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,mView)];
    [NSLayoutConstraint activateConstraints:mView_w];
    
    //
    UILabel *stageLabel=[[UILabel alloc]init];
    stageLabel.text=@"礼品卡号";
    stageLabel.font=[UIFont systemFontOfSize:14];
    stageLabel.textAlignment=NSTextAlignmentCenter;
    stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [mView addSubview:stageLabel];
    stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *stageStr=[NSString stringWithFormat:@"H:|[stageLabel(%f)]",(SCREENSIZE.width-16)/3+6];
    NSArray* stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:stageStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
    [NSLayoutConstraint activateConstraints:stageLabel_h];
    
    NSArray* stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stageLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
    [NSLayoutConstraint activateConstraints:stageLabel_w];
    
    //line01
    UIView *line01=[[UIView alloc]init];
    line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [mView addSubview:line01];
    line01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[stageLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel,line01)];
    [NSLayoutConstraint activateConstraints:line01_h];
    
    NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
    [NSLayoutConstraint activateConstraints:line01_w];
    
    //
    UILabel *cutPayTimeLabel=[[UILabel alloc]init];
    cutPayTimeLabel.text=@"面值";
    cutPayTimeLabel.font=[UIFont systemFontOfSize:14];
    cutPayTimeLabel.textAlignment=NSTextAlignmentCenter;
    cutPayTimeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [mView addSubview:cutPayTimeLabel];
    cutPayTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line01][cutPayTimeLabel(64)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:cutPayTimeLabel_h];
    
    NSArray* cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cutPayTimeLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:cutPayTimeLabel_w];
    
    //line02
    UIView *line02=[[UIView alloc]init];
    line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [mView addSubview:line02];
    line02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cutPayTimeLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel,line02)];
    [NSLayoutConstraint activateConstraints:line02_h];
    
    NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
    [NSLayoutConstraint activateConstraints:line02_w];
    
    //有效期
    UILabel *priceLabel=[[UILabel alloc]init];
    priceLabel.text=@"有效期";
    priceLabel.font=[UIFont systemFontOfSize:14];
    priceLabel.textAlignment=NSTextAlignmentCenter;
    priceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [mView addSubview:priceLabel];
    priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *validStr=[NSString stringWithFormat:@"H:[line02][priceLabel(%f)]",(SCREENSIZE.width-16)/4+10];
    NSArray* priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:validStr options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,priceLabel)];
    [NSLayoutConstraint activateConstraints:priceLabel_h];
    
    NSArray* priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[priceLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(priceLabel)];
    [NSLayoutConstraint activateConstraints:priceLabel_w];
    
    //line03
    UIView *line03=[[UIView alloc]init];
    line03.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [mView addSubview:line03];
    line03.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line03_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[priceLabel][line03(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(priceLabel,line03)];
    [NSLayoutConstraint activateConstraints:line03_h];
    
    NSArray* line03_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line03]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03)];
    [NSLayoutConstraint activateConstraints:line03_w];
    
    //状态
    UILabel *statusLabel=[[UILabel alloc]init];
    statusLabel.text=@"状态";
    statusLabel.font=[UIFont systemFontOfSize:14];
    statusLabel.textAlignment=NSTextAlignmentCenter;
    stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [mView addSubview:statusLabel];
    statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* statusLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line03][statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line03,statusLabel)];
    [NSLayoutConstraint activateConstraints:statusLabel_h];
    
    NSArray* statusLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[statusLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(statusLabel)];
    [NSLayoutConstraint activateConstraints:statusLabel_w];
    
    return headerSectionView;
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.5f;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Request 请求
-(void)requestHttpsForCoupon{
    dataArr=[NSMutableArray array];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"bonus" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCoupon];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSArray *arr=responseObject[@"retData"][@"bonus"];
                for (NSDictionary *dic in arr) {
                    CouponModel *model=[[CouponModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                [self setUpUI];
            }
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
