//
//  HotelDetailPriceCell.m
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelDetailPriceCell.h"

@implementation HotelDetailPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.buyBtn.layer.cornerRadius=4;
    self.buyBtn.layer.masksToBounds=YES;
    self.imgv.layer.cornerRadius=4;
    self.imgv.layer.masksToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickBuyBtn:(id)sender {
    [self.delegate clickBuyBtn:self.model];
}

-(void)reflushModel:(HotelDetailPriceModel *)model{
    self.model=model;
    self.nameLabel.text=model.RatePlanName;
    self.priceLabel.text=[NSString stringWithFormat:@"￥%@起",model.TotalPrice];
    if ([model.RoomStatus isEqualToString:@"1"]) {
        self.buyBtn.enabled=YES;
        self.buyBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        [self.buyBtn setTitle:@"预定" forState:UIControlStateNormal];
    }else{
        self.buyBtn.enabled=NO;
        self.buyBtn.backgroundColor=[UIColor colorWithHexString:@"ababab"];
        [self.buyBtn setTitle:@"已售完" forState:UIControlStateNormal];
    }
}

@end
