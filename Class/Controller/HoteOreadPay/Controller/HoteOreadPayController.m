//
//  HoteOreadPayController.m
//  youxia
//
//  Created by mac on 2017/3/28.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteOreadPayController.h"
#import "InformationCell.h"
#import "PSCHotelOrderDetail.h"
#import "HotelOrderList.h"

@interface HoteOreadPayController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation HoteOreadPayController
{
    UITableView *myTable;
    UIScrollView *myScrolview;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self seNavBar];
    [self setTupTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)seNavBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    
    //UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectZero];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 100, 30)];
    btnlab.text=@"支付成功";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(SCREENSIZE.width-60, 30, 60, 30);
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [rightBtn addTarget:self action:@selector(clickwanc:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:rightBtn];
}
-(void)fanhui{
    for (NSInteger i=self.navigationController.viewControllers.count-1; i>=0; i--) {
        UIViewController *controller=self.navigationController.viewControllers[i];
        if ([controller isKindOfClass:[HotelOrderList class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reflush_hotellist_noti" object:nil];
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
}

-(void)clickwanc:(id)send{
    for (NSInteger i=self.navigationController.viewControllers.count-1; i>=0; i--) {
        UIViewController *controller=self.navigationController.viewControllers[i];
        if ([controller isKindOfClass:[HotelOrderList class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reflush_hotellist_noti" object:nil];
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
}

#pragma mark - init UI
-(void)setTupTableView{
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"InformationCell" bundle:nil] forCellReuseIdentifier:@"InformationCell"];
    [self.view addSubview:myTable];
    
    
    UIView *paysuccess=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 220)];
    [paysuccess setBackgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1]];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(SCREENSIZE.width/2-40,20, 80, 120);
    imageView.image = [UIImage imageNamed:@"圆形-3"];
    [paysuccess addSubview:imageView];
    
    UILabel *asd=[[UILabel alloc]initWithFrame:CGRectMake(SCREENSIZE.width/2-35, imageView.size.height+imageView.origin.y+10, 100, 30)];
    asd.text=@"支付成功";
    
    [paysuccess addSubview:asd];
    UILabel *dind=[[UILabel alloc]initWithFrame:CGRectMake(SCREENSIZE.width/2-50, asd.size.height+asd.origin.y+5, 200, 44)];
    dind.textColor=[UIColor colorWithRed:171.0/255 green:171.0/255 blue:171.0/255 alpha:1];
    dind.font=[UIFont systemFontOfSize:14];
    dind.text=@"订单处理中,请稍等";
    [paysuccess addSubview:dind];
    
    myTable.tableHeaderView=paysuccess;
    
    myTable.tableFooterView=[UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        UIView *bview=[[UIView alloc]initWithFrame:CGRectMake(0, 13, 3, 20)];
        [bview setBackgroundColor:[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1]];
        [cell addSubview:bview];
        cell.titleName.text=@"金额 :";
        cell.moneytit.text=[NSString stringWithFormat:@"%@元",_paymoney];
        cell.titleName.textColor=[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1];
        cell.moneytit.textColor=[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    
    }else if (indexPath.row==1)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.moneytit.text=_payee;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        return cell;

    }else if (indexPath.row==2)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.titleName.text=@"商品";
        cell.moneytit.text=_product;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        return cell;

    
    }else if (indexPath.row==3)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.titleName.text=@"交易时间";
        cell.moneytit.text=_payTime;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        return cell;

    
    }else if (indexPath.row==4)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.titleName.text=@"付款方式";
        cell.moneytit.text=_payType;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        return cell;

    }else if (indexPath.row==5)
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.titleName.text=@"交易单号";
        cell.moneytit.text=_trade_no;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;

    
    }else
    {
        InformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if (!cell) {
            cell=[[InformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
            return cell;
        }
        cell.titleName.text=@"订单号";
        cell.moneytit.text=_orderid;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        return cell;

    
    }


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//        //假设跳的酒店订单;
//    HotelOrderController *ss=[[HotelOrderController alloc]init];
//    [self.navigationController pushViewController:ss animated:YES];
}
#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}


@end
