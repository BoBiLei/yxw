//
//  HoteOreadPayController.h
//  youxia
//
//  Created by mac on 2017/3/28.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteOreadPayController : UIViewController
@property(nonatomic,copy)NSString *paymoney;
@property(nonatomic,copy)NSString *payee;
@property(nonatomic,copy)NSString *product;
@property(nonatomic,copy)NSString *payTime;
@property(nonatomic,copy)NSString *payType;
@property(nonatomic,copy)NSString *orderid;
@property(nonatomic,copy)NSString *trade_no;

@end
