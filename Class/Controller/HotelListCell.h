//
//  HotelListCell.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *grogshopimg;
@property (weak, nonatomic) IBOutlet UILabel *grogshoptitle;
@property (weak, nonatomic) IBOutlet UILabel *grogshopsite;
@property (weak, nonatomic) IBOutlet UILabel *grogshopprice;
@property (weak, nonatomic) IBOutlet UIButton *sshibtn;

@end
