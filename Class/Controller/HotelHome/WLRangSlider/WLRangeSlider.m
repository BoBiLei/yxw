#import "WLRangeSlider.h"
#import "WLSliderThumbLayer.h"
#import "WLTrackLayer.h"
#import <QuartzCore/QuartzCore.h>
#import "JWTrackLayer.h"


@interface WLRangeSlider()

@property (nonatomic,strong) WLSliderThumbLayer *leftThumbLayer;
@property (nonatomic,strong) WLSliderThumbLayer *rightThumbLayer;
@property (nonatomic,strong) WLSliderThumbLayer *leftThumbLayer1;
@property (nonatomic,strong) WLSliderThumbLayer *rightThumbLayer1;
@property (nonatomic,strong) WLTrackLayer *trackLayer;
@property(nonatomic,strong)JWTrackLayer*trackLayer1;
@property (nonatomic) CGPoint previousLoction;
@property (nonatomic, strong) CATextLayer *leftLabel;
@property (nonatomic, strong) CATextLayer *rightLabel;
@property (nonatomic, strong) CATextLayer *leftLabel1;
@property (nonatomic, strong) CATextLayer *rightLabel1;

@end

@implementation WLRangeSlider

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initLayers];
    }
    return self;
}

- (void)initLayers{
    
    _maxValue =2000;
    
    _minValue = 0;
    _leftValue = 0;
    _rightValue = 2000;
    _thumbColor = [UIColor colorWithRed:71/255.0 green:159/255.0 blue:236/255.0 alpha:1];
    _trackHighlightTintColor = [UIColor colorWithRed:71/255.0 green:159/255.0 blue:236/255.0 alpha:1];
    _trackColor = [UIColor colorWithWhite:0.9 alpha:0.5];
    _cornorRadiusScale = 1.0  ;
    
    _trackLayer = [WLTrackLayer layer];
    _trackLayer.contentsScale = [UIScreen mainScreen].scale;
    _trackLayer.rangeSlider = self;
    [self.layer addSublayer:_trackLayer];
    
    _trackLayer1=[JWTrackLayer layer];
    _trackLayer1.contentsScale=[UIScreen mainScreen].scale;
    _trackLayer1.rangeSlider=self;
    [self.layer addSublayer:_trackLayer1];
    
    //左边圆圈
    _leftThumbLayer = [WLSliderThumbLayer layer];
    _leftThumbLayer.contentsScale = [UIScreen mainScreen].scale;
    _leftThumbLayer.highlighted=YES;
    _leftThumbLayer.rangeSlider = self;
    [self.layer addSublayer:_leftThumbLayer];
    
    //右边圆圈
    _rightThumbLayer = [WLSliderThumbLayer layer];
    _rightThumbLayer.contentsScale = [UIScreen mainScreen].scale;
    _leftThumbLayer.highlighted=NO;
    _rightThumbLayer.rangeSlider = self;
    [self.layer addSublayer:_rightThumbLayer];
    
    [self initLabels];
    
    [self updateLayerFrames];
    [self updateLabelFrame];
}
-(void)initLabels
{
    //shang   0：00
    self.leftLabel1 = [[CATextLayer alloc] init];
    self.leftLabel1.alignmentMode = kCAAlignmentCenter;
    self.leftLabel1.fontSize = 12.0f;
    self.leftLabel1.frame = CGRectMake(0, 10, 120, 16);
    self.leftLabel1.contentsScale = [UIScreen mainScreen].scale;
    self.leftLabel1.contentsScale = [UIScreen mainScreen].scale;
    self.leftLabel1.string=@"最低";
    self.leftLabel1.foregroundColor=[[UIColor colorWithHexString:@"ec6b00"] CGColor];
    [self.layer addSublayer:_leftLabel1];
    
    //左边label   0：00
    self.leftLabel = [[CATextLayer alloc] init];
    self.leftLabel.alignmentMode = kCAAlignmentCenter;
    self.leftLabel.fontSize = 12.0f;
    self.leftLabel.frame = CGRectMake(0, 10, 120, 16);
    self.leftLabel.contentsScale = [UIScreen mainScreen].scale;
    self.leftLabel.contentsScale = [UIScreen mainScreen].scale;
    self.leftLabel.string=[self changeStr:[NSString stringWithFormat:@"￥%f",self.leftValue]];
    self.leftLabel.foregroundColor=[[UIColor colorWithHexString:@"ec6b00"] CGColor];
    [self.layer addSublayer:_leftLabel];
    
    //right
    self.rightLabel1=[[CATextLayer alloc]init];
    self.rightLabel1.alignmentMode=kCAAlignmentCenter;
    self.rightLabel1.fontSize=12.0f;
    self.rightLabel1.frame=CGRectMake(0, 10, 120, 16);
    self.rightLabel1.contentsScale = [UIScreen mainScreen].scale;
    self.rightLabel1.string=@"最高";
    self.rightLabel1.foregroundColor=[[UIColor colorWithHexString:@"ec6b00"] CGColor];
    [self.layer addSublayer:_rightLabel1];
    
    //right
    self.rightLabel=[[CATextLayer alloc]init];
    self.rightLabel.alignmentMode=kCAAlignmentCenter;
    self.rightLabel.fontSize=12.0f;
    self.rightLabel.frame=CGRectMake(0, 10, 120, 16);
    self.rightLabel.contentsScale = [UIScreen mainScreen].scale;
    self.rightLabel.string=[self changeStr:[NSString stringWithFormat:@"￥%f",self.rightValue]];
    self.rightLabel.foregroundColor=[[UIColor colorWithHexString:@"ec6b00"] CGColor];
    [self.layer addSublayer:_rightLabel];
}

#pragma mark - Setters

- (void)setMinValue:(CGFloat)minValue{
    _minValue = minValue;
    [self updateLayerFrames];
}

- (void)setMaxValue:(CGFloat)maxValue{
    _maxValue = maxValue;
    [self updateLayerFrames];
}

- (void)setLeftValue:(CGFloat)leftValue{
    _leftValue = leftValue;
    [self updateLayerFrames];
//    [self updateLabelFrame];
    self.leftLabel.string=[self changeStr:[NSString stringWithFormat:@"￥%f",self.leftValue]];
   
}

- (void)setRightValue:(CGFloat)rightValue{
        _rightValue = rightValue;
        [self updateLayerFrames];
//    [self updateLabelFrame];
    self.rightLabel.string=[self changeStr:[NSString stringWithFormat:@"￥%f",self.rightValue]];
}

- (void)setThumbColor:(UIColor *)thumbColor{
    _thumbColor = thumbColor;
    [_leftThumbLayer setNeedsDisplay];
    [_rightThumbLayer setNeedsDisplay];
}

- (void)setTrackColor:(UIColor *)trackColor{
    _trackColor = trackColor;
    [_trackLayer setNeedsDisplay];
}

- (void)setTrackHighlightTintColor:(UIColor *)trackHighlightTintColor{
    _trackHighlightTintColor = trackHighlightTintColor;
    [_trackLayer setNeedsDisplay];
}

- (void)setCornorRadiusScale:(CGFloat)cornorRadiusScale{
    _cornorRadiusScale = cornorRadiusScale;
    [_leftThumbLayer setNeedsDisplay];
    [_rightThumbLayer setNeedsDisplay];
    [_trackLayer setNeedsDisplay];
}

#pragma mark - Utils Methods
- (CGFloat)thumbWidth{
    return CGRectGetHeight(self.bounds);
}

- (void)updateLayerFrames{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    
    CGFloat leftThumbCenter = [self positionForValue:_leftValue];
    CGFloat rightThumbCenter = [self positionForValue:_rightValue];
    _trackLayer.frame = CGRectInset(self.bounds, 0, [self thumbWidth]/3+1);
    _trackLayer1.frame=CGRectInset(self.bounds, 0, [self thumbWidth]/3+1);
        [_trackLayer setNeedsDisplay];
        [_trackLayer1 setNeedsDisplay];
        _leftThumbLayer.frame = CGRectMake(leftThumbCenter - [self thumbWidth] / 2.0, 0.0, [self thumbWidth], [self thumbWidth]);
        _rightThumbLayer.frame = CGRectMake(rightThumbCenter - [self thumbWidth] / 2.0, 0.0, [self thumbWidth], [self thumbWidth]);
        [_leftThumbLayer setNeedsDisplay];
        [_rightThumbLayer setNeedsDisplay];
    [CATransaction commit];
    
}

-(void)updateLabelFrame{

        self.leftLabel.position=CGPointMake(CGRectGetMidX(_leftThumbLayer.frame), 34);
        self.leftLabel1.position=CGPointMake(CGRectGetMidX(_leftThumbLayer.frame)+2, -12);
        self.rightLabel.position=CGPointMake(CGRectGetMidX(_rightThumbLayer.frame)-4, 34);
        self.rightLabel1.position=CGPointMake(CGRectGetMidX(_rightThumbLayer.frame)-2, -12);
}

- (CGFloat)positionForValue:(CGFloat)value{
    return (CGRectGetWidth(self.bounds) - [self thumbWidth]) * (value - _minValue) / (_maxValue - _minValue) + [self thumbWidth] / 2.0;
}

- (CGFloat)boundaryForValue:(CGFloat)value minValue:(CGFloat)minValue maxValue:(CGFloat)maxValue{
    CGFloat num = MIN(MAX(value, minValue), maxValue);
    NSLog(@"++++++++=%f",num);
    return num;
}

#pragma mark - Override Methods
- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    _previousLoction = [touch locationInView:self];
    if (CGRectContainsPoint(_leftThumbLayer.frame, _previousLoction)) {
        _leftThumbLayer.highlighted = YES;
        
    }else if(CGRectContainsPoint(_rightThumbLayer.frame, _previousLoction)){
        _rightThumbLayer.highlighted = YES;
    }

    return _rightThumbLayer.highlighted || _leftThumbLayer.highlighted;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    CGPoint point = [touch locationInView:self];
    CGFloat deltaX = point.x - _previousLoction.x;
    CGFloat deltaValue = (_maxValue - _minValue) * deltaX / (CGRectGetWidth(self.bounds) - [self thumbWidth]);
    _previousLoction = point;
    if (deltaValue>0) {
        if (_leftThumbLayer.highlighted && _rightValue-_leftValue>100) {
            self.leftValue += deltaValue;
            self.leftValue = [self boundaryForValue:_leftValue minValue:_minValue maxValue:_rightValue];
        }else if (_rightThumbLayer.highlighted ){
            self.rightValue += deltaValue;
            self.rightValue = [self boundaryForValue:_rightValue minValue:_leftValue maxValue:_maxValue];
        }
    }else{
        if (_leftThumbLayer.highlighted) {
            self.leftValue += deltaValue;
            self.leftValue = [self boundaryForValue:_leftValue minValue:_minValue maxValue:_rightValue];
        }else if (_rightThumbLayer.highlighted && _rightValue-_leftValue>100){
            self.rightValue += deltaValue;
            self.rightValue = [self boundaryForValue:_rightValue minValue:_leftValue maxValue:_maxValue];
        }
    }
    
    if (self.leftValue==0) {
        if (self.rightValue<6.5) {
            self.rightValue=6.5;
        }
    }else if (self.leftValue>0&&self.leftValue<6){
        if (self.rightValue-self.leftValue<3) {
            self.rightValue=self.leftValue+3;
        }
    }
    [self sendActionsForControlEvents: UIControlEventValueChanged];
   
    return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    _leftThumbLayer.highlighted = NO;
   
}

- (void)cancelTrackingWithEvent:(UIEvent *)event{
    _leftThumbLayer.highlighted = NO;
    _rightThumbLayer.highlighted = NO;
}
#pragma mark StrChanged
-(NSString*)changeStr:(NSString*)myStr
{
    NSString*str=nil;
    NSString*fenStr=[NSString stringWithFormat:@"0.%@",[myStr componentsSeparatedByString:@"."][1]];
    NSString*fen=[self notRounding:[fenStr floatValue]*60  afterPoint:0];
    
    
    if ([fen substringFromIndex:1]==nil||[[fen substringFromIndex:1]isEqualToString:@""]) {
        fen=[NSString stringWithFormat:@"0%@",fen];
    }
    
    str=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[myStr componentsSeparatedByString:@"."][0]]];
    NSLog(@"str--%@",str);
    return str;
}

-(NSString *)notRounding:(float)price afterPoint:(int)position{
    
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *ouncesDecimal;
    
    NSDecimalNumber *roundedOunces;
    
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    
    return [NSString stringWithFormat:@"%@",roundedOunces];
}


@end
