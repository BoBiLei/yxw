//
//  HoteapartmentCell.h
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteapartmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *hotelab;
@property (weak, nonatomic) IBOutlet UILabel *counts;

@end
