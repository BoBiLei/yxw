//
//  HotelHomeCell.h
//  youxia
//
//  Created by mac on 2017/3/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelHomeModel.h"
@interface HotelHomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgico;
@property (weak, nonatomic) IBOutlet UILabel *titileare;
@property (weak, nonatomic) IBOutlet UIImageView *btnjt;
@property (weak, nonatomic) IBOutlet UIButton *dw;
@property (weak, nonatomic) IBOutlet UIButton *wz;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property(nonatomic,weak) HotelHomeModel *model;
@end
