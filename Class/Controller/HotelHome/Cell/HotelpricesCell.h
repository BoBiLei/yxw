//
//  HotelpricesCell.h
//  youxia
//
//  Created by mac on 2017/3/22.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelpricesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *star;

@property (weak, nonatomic) IBOutlet UILabel *price;
@end
