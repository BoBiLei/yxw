//
//  HotelSelectChildAgeCell.h
//  youxia
//
//  Created by mac on 2017/5/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelSelectChildAgeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

-(void)reflushData:(NSString *)age atIndexPach:(NSIndexPath *)indexPach;

@end
