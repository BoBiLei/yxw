//
//  HotelSelectChildAgeCell.m
//  youxia
//
//  Created by mac on 2017/5/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelSelectChildAgeCell.h"

@implementation HotelSelectChildAgeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(NSString *)age atIndexPach:(NSIndexPath *)indexPach{
    self.rightLabel.text=[age isEqualToString:@""]?[NSString stringWithFormat:@"请选择儿童%ld年龄",indexPach.row+1]:age;
}

@end
