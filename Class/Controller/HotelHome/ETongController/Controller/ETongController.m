//
//  ETongController.m
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ETongController.h"
#import "ETongCell.h"
#import "HotelSumChildCell.h"
#import "HotelSelectChildAgeCell.h"
#import "HotelHomeController.h"
#import "NSMutableAttributedString+TY.h"

@interface ETongController ()<UITableViewDelegate,UITableViewDataSource,HotelInSumDelegate,HotelInSumChildDelegate>

@property (nonatomic ,strong) UIView *deliverView; //底部View
@property (nonatomic ,strong) UIView *BGView; //遮罩

@end

@implementation ETongController{
    UITableView *myTable;
    
    NSInteger seleRow;
    
    NSMutableArray *childArr;
    
    NSInteger adultSum;         //成人数
    NSInteger childSum;         //儿童数
    UIButton *seleBtn;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self seNavBar];
    
    adultSum=1;
    childSum=0;
    
    [self setTablew];
}

-(void)seNavBar{
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(155, 25, 200, 30)];
    btnlab.text=@"入住人数";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}

-(void)fanhui{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 点击确定
-(void)Btnque:(NSString *)send{
    if (childSum==0) {
        _selectEtongDateBlock([NSString stringWithFormat:@"%ld",adultSum],[NSString stringWithFormat:@"%ld",childSum],@"1");
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        for (int i=0;i<childArr.count;i++) {
            NSString *str=childArr[i];
            if ([str isEqualToString:@""]) {
                [AppUtils showSuccessMessage:[NSString stringWithFormat:@"请选择儿童%d年龄",i+1] inView:self.view];
                return;
            }
        }
        _selectEtongDateBlock([NSString stringWithFormat:@"%ld",adultSum],[NSString stringWithFormat:@"%ld",childArr.count],@"1");
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)setTablew{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"ETongCell" bundle:nil] forCellReuseIdentifier:@"ETongCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HotelSumChildCell" bundle:nil] forCellReuseIdentifier:@"HotelSumChildCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HotelSelectChildAgeCell" bundle:nil] forCellReuseIdentifier:@"HotelSelectChildAgeCell"];
    [self.view addSubview:myTable];
    
    UIView *fview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 200)];
    
    UILabel *tip01=[[UILabel alloc]init];
    tip01.text=@"温馨提示";
    tip01.font=kFontSize16;
    tip01.frame=CGRectMake(16, 10, 100, 40);
    [fview addSubview:tip01];
    myTable.tableFooterView=fview;
    
    UILabel *tip02=[[UILabel alloc]init];
    tip02.text=@"入住人数和儿童人数的变化,价格可能会变化,请按实际入住情况选择人数";
    tip02.numberOfLines = 0;
    tip02.frame=CGRectMake(tip01.origin.x, tip01.size.height+tip01.origin.y, SCREENSIZE.width-tip01.origin.x*2, 40);
    tip02.font = [UIFont systemFontOfSize:14];
    tip02.textColor=[UIColor grayColor];
    [fview addSubview:tip02];

    UIButton *cfBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cfBtn.frame = CGRectMake(10, tip02.size.height+tip02.origin.y+16, SCREEN_WIDTH-20, 44);
    cfBtn.backgroundColor=[UIColor colorWithRed:70/255.0 green:161/255.0 blue:236/255.0 alpha:1];
    [cfBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cfBtn.layer.cornerRadius = 3.0;
    [cfBtn setTitle:@"确定" forState:UIControlStateNormal];
    [cfBtn addTarget:self action:@selector(Btnque:) forControlEvents:UIControlEventTouchUpInside];
    [fview addSubview:cfBtn];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

//第section分区一共有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else if(section==1){
        return 1;
    }else{
        return childSum;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ETongCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ETongCell"];
        if (!cell) {
            cell = [[ETongCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ETongCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:adultSum];
        cell.delegate=self;
        return cell;
    }else if(indexPath.section==1){
        HotelSumChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelSumChildCell"];
        if (!cell) {
            cell = [[HotelSumChildCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelSumChildCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushSumData:childSum];
        cell.delegate=self;
        return cell;
    }else{
        HotelSelectChildAgeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelSelectChildAgeCell"];
        if (!cell) {
            cell=[[HotelSelectChildAgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelSelectChildAgeCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text=[NSString stringWithFormat:@"儿童%ld",indexPath.row+1];
        [cell reflushData:childArr[indexPath.row] atIndexPach:indexPath];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==2) {
        seleRow=indexPath.row;
        [self showSelectAgeView];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?16:0.000000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?16:0.0000001f;
}

#pragma mark - 点击 成人加

-(void)clickAddBtnAtIndexPatch:(NSIndexPath *)indexPatch{
    adultSum++;
    [UIView animateWithDuration:0.0000001 animations:^{
        [myTable reloadSection:0 withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 点击 成人减

-(void)clickJianBtnAtIndexPatch:(NSIndexPath *)indexPatch{
    adultSum--;
    [UIView animateWithDuration:0.0000001 animations:^{
        [myTable reloadSection:0 withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 点击 儿童加
-(void)clickAddChildBtnAtIndexPatch:(NSIndexPath *)indexPatch{
    childSum++;
    childArr=[NSMutableArray array];
    for (int i=0; i<childSum; i++) {
        [childArr addObject:@""];
    }
    [UIView animateWithDuration:0.0000001 animations:^{
        [myTable reloadData];
    }];
}

#pragma mark - 点击 儿童减
-(void)clickJianChildBtnAtIndexPatch:(NSIndexPath *)indexPatch{
    childSum--;
    childArr=[NSMutableArray array];
    for (int i=0; i<childSum; i++) {
        [childArr addObject:@""];
    }
    [UIView animateWithDuration:0.0000001 animations:^{
        [myTable reloadData];
    }];
}


#pragma mark - showSelectAgeView
- (void)showSelectAgeView {
    // ------全屏遮罩
    self.BGView                 = [[UIView alloc] init];
    self.BGView.frame           = [[UIScreen mainScreen] bounds];
    self.BGView.tag             = 100;
    self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.BGView.opaque = NO;
    
    //--UIWindow的优先级最高，Window包含了所有视图，在这之上添加视图，可以保证添加在最上面
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    [appWindow addSubview:self.BGView];
    
    // ------给全屏遮罩添加的点击事件
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAgeView)];
    gesture.numberOfTapsRequired = 1;
    gesture.cancelsTouchesInView = NO;
    [self.BGView addGestureRecognizer:gesture];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    }];
    
    // ------底部弹出的View
    self.deliverView                 = [[UIView alloc] init];
    self.deliverView.frame           = CGRectMake(0, SCREEN_WIDTH, SCREEN_WIDTH, SCREEN_WIDTH+100);
    self.deliverView.backgroundColor = [UIColor whiteColor];
    
    [appWindow addSubview:self.deliverView];
    
    // ------View出现动画
    self.deliverView.transform = CGAffineTransformMakeTranslation(0.01, SCREEN_HEIGHT);
    [UIView animateWithDuration:0.3 animations:^{
        self.deliverView.transform = CGAffineTransformMakeTranslation(0.01, 0.01);
    }];
    UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(16, 16, 200, 21)];
    lab.text=@"年龄筛选";
    lab.textColor=[UIColor colorWithHexString:@"707070"];
    lab.font=kFontSize16;
    [_deliverView addSubview:lab];
    NSArray *ageArr=@[@"<1岁",@"1岁",@"2岁",@"3岁",@"4岁",@"5岁",@"6岁",@"7岁",@"8岁",@"9岁",@"10岁",@"11岁",@"12岁",@"13岁",@"14岁",@"15岁",@"16岁",@"17岁"];
    CGFloat lastYH=0;
    CGFloat btnWidth=(SCREENSIZE.width-lab.origin.x*2-24)/4;
    CGFloat btnHeight=38;
    CGFloat btnY=lab.origin.y+lab.height+10;
    CGFloat btnX=lab.origin.x;
    for (int i=0; i<ageArr.count; i++) {
        
        btnX=lab.origin.x+(i%4)*btnWidth+8*(i%4);
        btnY=(i/4)*btnHeight+(i/4)*8+lab.origin.y+lab.height+10;
        
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(btnX,btnY, btnWidth, btnHeight)];
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
        [btn.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
        [btn.layer setBorderWidth:1.2f];
        [btn.layer setMasksToBounds:YES];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitle:ageArr[i] forState:UIControlStateNormal];
        btn.tag =i;
        
        [btn addTarget:self action:@selector(tagButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_deliverView addSubview:btn];
        if(i==0){
            seleBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
            btn.enabled=NO;
            [btn.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
        }
        if (i==ageArr.count-1) {
            lastYH=btn.origin.y+btn.height+20;
        }
    }
    
    CGRect frame=_deliverView.frame;
    frame.size.height=lastYH;
    frame.origin.y=SCREENSIZE.height-frame.size.height;
    _deliverView.frame=frame;
}

#pragma mark - dismiss
- (void)dismissAgeView{
    [UIView animateWithDuration:0.3 animations:^{
        self.deliverView.transform = CGAffineTransformMakeTranslation(0.01, SCREEN_HEIGHT);
        self.deliverView.alpha = 0.2;
        self.BGView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.BGView removeFromSuperview];
        [self.deliverView removeFromSuperview];
    }];
}

- (void)tagButtonClick:(UIButton *)sender{
    
    [sender setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    [sender.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
    sender.enabled=NO;
    
    [seleBtn setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
    [seleBtn.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
    seleBtn.enabled=YES;
    
    seleBtn= sender;
    
    //
    if (childArr.count!=0) {
        [childArr removeObjectAtIndex:seleRow];
    }
    
    [childArr insertObject:sender.titleLabel.text atIndex:seleRow];
    
    [self dismissAgeView];
    [myTable reloadData];
}

@end
