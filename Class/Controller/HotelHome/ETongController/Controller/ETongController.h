//
//  ETongController.h
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETongController : UIViewController

@property(nonatomic,copy) void(^selectEtongDateBlock)(NSString *chNum,NSString *adNum,NSString *etage);

@end
