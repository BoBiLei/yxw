//
//  SelectHotelModel.m
//  YXJR_SH
//
//  Created by mac on 2016/11/17.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "SelectHotelModel.h"

@implementation SelectHotelModel

-(void)jsonDataForDictioanry:(NSDictionary *)dic{
    self.hId=dic[@"id"];
    self.hName=dic[@"name"];
    self.hAddress=dic[@"address"];
}

@end
