//
//  SelectHotelModel.h
//  YXJR_SH
//
//  Created by mac on 2016/11/17.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectHotelModel : NSObject

@property (nonatomic, copy) NSString *hId;
@property (nonatomic, copy) NSString *hName;
@property (nonatomic, copy) NSString *hAddress;

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

@end
