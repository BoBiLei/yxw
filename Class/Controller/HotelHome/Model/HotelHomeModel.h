//
//  HotelHomeModel.h
//  youxia
//
//  Created by mac on 2017/3/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelHomeModel : NSObject



@property (nonatomic,copy) NSString *istypeId;
@property (nonatomic,copy) NSString *fildId;

@property (nonatomic,copy) NSString *type;  //表明搜索类型:
@property (nonatomic,copy) NSString *position; //CountryCode:CityCode
@property (nonatomic,copy) NSString *order; //价格排序
@property (nonatomic,copy) NSString *star;   //酒店星级
@property (nonatomic,copy) NSString *price; //价格区间
@property (nonatomic,copy) NSString *checkin; //入住日期
@property (nonatomic,copy) NSString *checkout; // 退房日期
@property (nonatomic,copy) NSString *searchKey;      //搜索关键词
@property (nonatomic,copy) NSString *page;      //分页数
@property (nonatomic,copy) NSString *adult; //成人
@property (nonatomic,copy) NSString *child; //儿童
@property (nonatomic,copy) NSString *childage; //儿童年龄
-(void)jsonDataForDictioanry:(NSDictionary *)model;

@end
