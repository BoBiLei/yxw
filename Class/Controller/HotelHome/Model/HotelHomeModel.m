//
//  HotelHomeModel.m
//  youxia
//
//  Created by mac on 2017/3/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelHomeModel.h"

@implementation HotelHomeModel
-(void)jsonDataForDictioanry:(NSDictionary *)model{
    self.type=model[@"type"];
    self.position=model[@"position"];
    self.order=model[@"order"];
    self.star=model[@"star"];
    self.price=model[@"price"];
    self.checkin=model[@"checkin"];
    self.checkout=model[@"checkout"];
    self.searchKey=model[@"searchKey"];
    self.page=model[@"page"];
    self.adult=model[@"adult"];
    self.child=model[@"child"];
    self.childage=model[@"childAge"];
}
@end
