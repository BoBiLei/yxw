//
//  SelectCtripCityController.m
//  YXJR_SH
//
//  Created by mac on 2016/11/18.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "SelectCtripCityController.h"
#import "CountyDB.h"

//////
#import "NewHotelCityDB.h"
//////

#define Flight_chinacity_star_noti @"flight_chinacity_star_noti"
#define Flight_chinacity_end_noti @"flight_chinacity_end_noti"
#define Flight_overseascity_star_noti @"flight_overseascity_star_noti"
#define Flight_overseascity_end_noti @"flight_overseascity_end_noti"
@interface SelectCtripCityController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@end

@implementation SelectCtripCityController{
    
    NSString *keyWord;
    UITextField *searchTf;
    
    UIView *btLine;
    UIButton *seleFilterBtn;
    UITableView *myTable;
    
    NSDictionary *sortedNameDict;
    //首字母
    NSArray *indexArray;
    
    NSInteger seleTag;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpTopBtnForArray:@[@"国内城市",@"国际城市"]];
    
    [self setUpUI];
}

-(void)setNavBar{
    keyWord=@"";
    //
    UIView *nav=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [self.view addSubview:nav];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"nav_turnback02"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [nav addSubview:back];
    
    //
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(back.origin.x+back.width+1, 26, SCREENSIZE.width-(back.origin.x*2+back.width+8), 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=5;
    searchView.layer.borderWidth=0.8;
    searchView.layer.borderColor=[UIColor colorWithHexString:@"#e4e4e4"].CGColor;
    [self.view addSubview:searchView];
    
    //放大镜图片
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"nav_magnifier"];
    [searchView addSubview:tipImg];
    
    //textField
    searchTf=[[UITextField alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    searchTf.font=[UIFont systemFontOfSize:15];
    searchTf.placeholder=_selectCityType==ChinaCityType?@"搜索国内城市":@"搜索国际城市";
    [searchTf setValue:[UIColor colorWithHexString:@"#71808c"] forKeyPath:@"_placeholderLabel.textColor"];
    searchTf.delegate=self;
    searchTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTf.textColor=[UIColor blackColor];
    searchTf.tintColor=[UIColor colorWithHexString:@"55b2f0"];
    searchTf.returnKeyType=UIReturnKeySearch;
    [searchTf addTarget:self action:@selector(keyWordDidChange:) forControlEvents:UIControlEventEditingChanged];
    [searchView addSubview:searchTf];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - setUp 筛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=64;
    for (int i=0; i<arr.count; i++) {
        
        NSString *name=arr[i];
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i;
        btn.frame=CGRectMake(i*btnWidth, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=[UIFont systemFontOfSize:16];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"595959"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if (i==_selectCityType) {
            lineX=btn.origin.x;
            seleFilterBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        }
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(btn.origin.x+btn.width+0.5f,72,0.5f,28)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [self.view addSubview:line];
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(_selectCityType==0?lineX:btnWidth, btnY+btnHeihgt-2.0f, btnWidth, 2.5f)];
    btLine.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:btLine];
    
    UIView *barLine=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 0.5f)];
    barLine.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [self.view addSubview:barLine];
}

#pragma mark - 点击筛选按钮
-(void)clickFBtn:(id)sender{
    
    [AppUtils closeKeyboard];
    
    [seleFilterBtn setTitleColor:[UIColor colorWithHexString:@"595959"] forState:UIControlStateNormal];
    seleFilterBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    seleTag=btn.tag;
    [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    //设置蓝色横条位置
    CGRect frame=btLine.frame;
    frame.origin.x=btn.origin.x;
    
    [AppUtils showProgressInView:self.view];
    
    [UIView animateWithDuration:0.20 animations:^{
        btLine.frame=frame;
        
        
        if (btn.tag==0) {
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_china"];
            sortedNameDict=[db getHotelCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_oversea"];
            sortedNameDict = [db getHotelCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                if ([obj1 integerValue] > [obj2 integerValue]) {
                    return [obj2 compare:obj1];
                }
                return [obj1 compare:obj2];
            }];
        }
        NSLog(@"%@--",indexArray)
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
    } completion:^(BOOL finished) {
        if (btn.tag==0) {
            searchTf.placeholder=@"搜索国内城市";
        }else{
            searchTf.placeholder=@"搜索国际城市";
        }
        [myTable reloadData];
        [AppUtils dismissHUDInView:self.view];
    }];
    
    //用一个button代替当前选择的
    seleFilterBtn=btn;
}

#pragma mark - init UI
-(void)setUpUI{
    
    seleTag=_selectCityType;
    
    
    if (_selectCityType==0) {
        NewHotelCityDB *db=[NewHotelCityDB shareDB];
        [db openDBForPath:@"hotelcity_china"];
        sortedNameDict=[db getHotelCity];
        indexArray = [sortedNameDict allKeys];
        indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return [obj1 compare:obj2];
        }];
    }else{
        NewHotelCityDB *db=[NewHotelCityDB shareDB];
        [db openDBForPath:@"hotelcity_oversea"];
        sortedNameDict = [db getHotelCity];
        indexArray = [sortedNameDict allKeys];
        indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return [obj1 compare:obj2];
        }];
    }
    
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) style:UITableViewStylePlain];
    [myTable setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSectionIndexColor:[UIColor colorWithHexString:@"777777"]];    //字母颜色
    [myTable setSectionIndexBackgroundColor:[UIColor clearColor]];//清空section颜色
    [self.view addSubview:myTable];
    myTable.tableFooterView=[UIView new];
}

#pragma mark - UITableView
//section
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [sortedNameDict allKeys].count;
}
//row
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = [sortedNameDict objectForKey:indexArray[section]];
    return array.count;
}
//height
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}
//初始化cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *ID1 = @"cellIdentifier1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID1];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID1];
    }
    cell.textLabel.adjustsFontSizeToFitWidth=YES;
    
    if (indexPath.row<[sortedNameDict allKeys].count) {
        NewHotelCityModel *model = [[sortedNameDict objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        [cell.textLabel setText:model.cityName_cn];
    }
    /////////////////////////////////
    /////////////////////////////////
    return cell;
}

//返回的字母
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return indexArray;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 24)];
    headerView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, SCREENSIZE.width-24, headerView.height)];
    label.textColor = [UIColor colorWithHexString:@"#000000"];
    label.font=[UIFont systemFontOfSize:15];
    label.text=indexArray[section];
    [headerView addSubview:label];
    
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
    if(seleTag==0){
        CountyCityModel *model = [[sortedNameDict objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        model.seleCityType=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"flight_chinacity_star_noti" object:model];
    }else{
        CountyCityModel *model = [[sortedNameDict objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        model.seleCityType=1;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"flight_Oversea_City_noti" object:model];
    }
    [self.navigationController popViewControllerAnimated:YES];
     */
    
    
    ////////////////////////
    ////////////////////////
    if(seleTag==0){
        NewHotelCityModel *model = [[sortedNameDict objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        model.seleCityType=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"flight_chinacity_star_noti" object:model];
    }else{
        NewHotelCityModel *model = [[sortedNameDict objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        model.seleCityType=1;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"flight_Oversea_City_noti" object:model];
    }
    [self.navigationController popViewControllerAnimated:YES];
    ////////////////////////
    ////////////////////////
}

#pragma mark - KeyWordDidChange

- (void)keyWordDidChange:(id) sender {
    /*
    UITextField *field = (UITextField *)sender;
    keyWord=field.text;
    CountyDB *db=[CountyDB shareDB];
    [db openDBForPath:@"county"];
    if ([keyWord isEqualToString:@""]) {
        if (seleTag==0) {
            [db closeDB];
            sortedNameDict=[db getCtripChinaCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            [db closeDB];
            sortedNameDict = [db getCtripOverSeaCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
        [AppUtils closeKeyboard];
    }else{
        if (seleTag==0) {
            [db closeDB];
            sortedNameDict=[db searchChinaCtripCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            [db closeDB];
            sortedNameDict = [db searchOverseasCtripCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
    }
    [myTable reloadData];
     */
    
    ////////////////////////
    ////////////////////////
    UITextField *field = (UITextField *)sender;
    keyWord=field.text;
    if ([keyWord isEqualToString:@""]) {
        if (seleTag==0) {
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_china"];
            sortedNameDict=[db getHotelCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_oversea"];
            sortedNameDict = [db getHotelCity];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
        [AppUtils closeKeyboard];
    }else{
        if (seleTag==0) {
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_china"];
            sortedNameDict=[db searchCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_oversea"];
            sortedNameDict = [db searchCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
    }
    [myTable reloadData];
    ////////////////////////
    ////////////////////////
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    /*
    [AppUtils closeKeyboard];
    if (textField.text.length>0) {
        CountyDB *db=[CountyDB shareDB];
        [db openDBForPath:@"county"];
        if (seleTag==0) {
            [db closeDB];
            sortedNameDict=[db searchChinaCtripCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            [db closeDB];
            sortedNameDict = [db searchOverseasCtripCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
    }
    [myTable reloadData];
    return YES;
     */
    
    ////////////////////////
    ////////////////////////
    [AppUtils closeKeyboard];
    if (textField.text.length>0) {
        if (seleTag==0) {
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_china"];
            sortedNameDict=[db searchCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }else{
            NewHotelCityDB *db=[NewHotelCityDB shareDB];
            [db openDBForPath:@"hotelcity_oversea"];
            sortedNameDict = [db searchCityWithKeyWord:keyWord];
            indexArray = [sortedNameDict allKeys];
            indexArray = [indexArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
    }
    [myTable reloadData];
    return YES;
    ////////////////////////
    ////////////////////////
}

@end
