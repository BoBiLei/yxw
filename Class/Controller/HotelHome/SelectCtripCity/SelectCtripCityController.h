//
//  SelectCtripCityController.h
//  YXJR_SH
//
//  Created by mac on 2016/11/18.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "BaseController.h"

typedef NS_ENUM(NSUInteger, SelectFlightCityType) {
    ChinaCityType=0,
    OverseaType=1
};

@interface SelectCtripCityController : BaseController

@property (nonatomic) SelectFlightCityType selectCityType;

@end
