//
//  HotelHomeController.m
//  youxia
//
//  Created by mac on 2017/3/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelHomeController.h"
#import "HomepageoptionsCell.h"
#import "HotelHomeModel.h"
#import "HotelHomeCell.h"
#import "HoteldateCell.h"
#import "OneofCell.h"
#import "HotelpricesCell.h"
#import "BeginsearchCell.h"
#import "SelectCheckDateViewController.h"
#import "HotelListController.h"
#import "HoteapartmentCell.h"
#import "HMSegmentedControl.h"
#import "ETongController.h"
#import "HoteSDCell.h"
#import "WLRangeSlider.h"
#import <QuartzCore/QuartzCore.h>
#import "SelectCtripCityController.h"
#import "CountyCityModel.h"
#import "CtripChinaCityModel.h"
#import "CtripOverseaCityModel.h"

////////////
#import "NewHotelCityModel.h"
////////////

@interface HotelHomeController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *imgArr;
}
@property (nonatomic ,strong) UIView *deverView; //底部View
@property (nonatomic ,strong) UIView *bgView; //遮罩
@property (nonatomic,strong)WLRangeSlider *rangeSlider;

@property(nonatomic,copy)NSString *pricers;
@property(nonatomic,copy)NSString *minPricers;
@end

@implementation HotelHomeController{
    
    NSString *adultSum;                 //成人数量
    NSString *childSum;                 //儿童数量
    NSString *childAge;                 //儿童年龄
    
    NSArray *starArr;                   //星级Arr
    
    NSString *xuanzhe; //选择的类型;
    
    NSString *Tprice; //价格;
    UITableView *myTable;
    UILabel *Citynames;
    
    UIScrollView *myScrolview;
    
    NSString *ruzhuDate;            //入住时间
    NSString *endDate;              //入住时间
    NSString *totalDays;            //天数
    int as;
    
    NSString *ruDate;               //入住时间
    NSString *eDate;                //入住时间
    NSString *tDays;                //天数
    
    NSString *starAndPriceStr;      //显示性价价格
    NSInteger filterTag;
    
    UILabel *chengNum;
    UILabel *ertongNum;
    NSString *typeCN;
    NSString *typeINT;
    UITextField *guoneifield;
    UITextField *guijifield;
    NSString *chinaCity;                //国内城市
    NSString *chinaCityParam;           //国内城市参数
    NSString *overseaCity;              //国际城市
    NSString *overseaCityParam;         //国际城市参数
    NSString *pd2;
    NSString *s;
    NSMutableArray * choiceStarArray;
    NSString * pos;
    UIButton *seleBtn;
    NSString * wordPos;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    choiceStarArray = [[NSMutableArray alloc]init];
    [self.view setBackgroundColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receNoti:) name:@"flight_chinacity_star_noti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receOverseaNoti:) name:@"flight_Oversea_City_noti" object:nil];
    
    filterTag=0;
    chinaCity=@"深圳市";
    overseaCity=@"深圳市";
    adultSum=@"1";
    childSum=@"0";
    childAge=@"0";
    starAndPriceStr=@"";
    switch (filterTag) {
        case 0:
            typeCN=@"CN";
            break;
        default:
            typeCN=@"INT";
            break;
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;//解决table便宜问题
    pos=@"CN:3354";
    [self optiondate];
    
    [self dataRequest];
}

-(void)receNoti:(NSNotification *)noti{
    
    /*
    CountyCityModel *model=noti.object;
    chinaCity=model.cityName_CN;
    
    chinaCityParam=[NSString stringWithFormat:@"%@:%@",model.countryCode,model.cityCode];
    pos=chinaCityParam;
    if (model.seleCityType==0) {
        [self onChinaBtn];
    }else{
        [self onOverseaBtn];
    }
     */
    
    /////////
    /////////
    NewHotelCityModel *model=noti.object;
    chinaCity=model.cityName_cn;
    
    chinaCityParam=[NSString stringWithFormat:@"%@:%@",model.countryCode,model.cityCode];
    pos=chinaCityParam;
    if (model.seleCityType==0) {
        [self onChinaBtn];
    }else{
        [self onOverseaBtn];
    }
    /////////
    /////////
}

-(void)receOverseaNoti:(NSNotification *)noti{
    /*
    CountyCityModel *model=noti.object;
    overseaCity=model.cityName_CN;
    
    overseaCityParam=[NSString stringWithFormat:@"%@:%@",model.countryCode,model.cityCode];
    wordPos=overseaCityParam;
    if (model.seleCityType==0) {
        [self onChinaBtn];
    }else{
        [self onOverseaBtn];
    }
     */
    
    NewHotelCityModel *model=noti.object;
    overseaCity=model.cityName_cn;
    
    overseaCityParam=[NSString stringWithFormat:@"%@:%@",model.countryCode,model.cityCode];
    wordPos=overseaCityParam;
    if (model.seleCityType==0) {
        [self onChinaBtn];
    }else{
        [self onOverseaBtn];
    }
}


//获取当前时间和后天时间
-(void)optiondate{
    NSDate *  senddate=[NSDate date];//当前时间
    NSDate *nextDay = [NSDate dateWithTimeInterval:24*60*60 sinceDate:senddate];//后一天
    
    NSDate *nextnextDay=[NSDate dateWithTimeInterval:(24*60*60)*2 sinceDate:senddate];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    
    NSString *  locationString=[dateformatter stringFromDate:nextDay];
    NSString *nextString=[dateformatter stringFromDate:nextnextDay];
    ruDate=locationString;
    ruzhuDate=locationString;
    endDate=nextString;
    eDate=nextString;
    totalDays=@"1";
    tDays=@"1";
}

-(void)seNavBar{
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SCREENSIZE.width, 44)];
    btnlab.text=@"酒店预订";
    btnlab.textAlignment=NSTextAlignmentCenter;
    [btnlab setTextColor:[UIColor whiteColor]];
    [cusBar addSubview:btnlab];
    
    UIButton *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [cusBar addSubview:fanHuiButton];
}

-(void)fanhui{
    [self .navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     _minPricers = @"0";
    _pricers = @"2000";
    s = @"0";
    [TalkingData trackPageBegin:@"酒店首页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"酒店首页"];
}

#pragma mark - SetUpScrollview
-(void)setUpMainScrollview{
    myScrolview=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 250)];
    [myScrolview setBounces:NO];
    myScrolview.showsHorizontalScrollIndicator=NO;
    myScrolview.scrollEnabled=YES;
    myScrolview.pagingEnabled=YES;
    [self.view addSubview:myScrolview];
    for (int j=0; j<imgArr.count; j++) {
        myScrolview.contentSize=CGSizeMake(SCREENSIZE.width*imgArr.count, 0);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(j*SCREENSIZE.width, 0, SCREENSIZE.width, myScrolview.height)];
        imageView.tag = 100 + j;
        NSURL * url = [NSURL URLWithString:imgArr[j]];  //字符串转URL
        [imageView setImageWithURL:url placeholder:nil]; //设置图片
        imageView.userInteractionEnabled=YES;//与用户交互
        [myScrolview setCanCancelContentTouches:YES];
        [myScrolview addSubview:imageView];
    }
}

#pragma mark - Request 星级与价格接
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    imgArr=[NSMutableArray array];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"starPrice" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:typeCN forKey:@"loc"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@============================================",responseObject);
        
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]) {
            _pricers=responseObject[@"retData"][@"priceCeil"];
            
            starArr=responseObject[@"retData"][@"star"];
            
            for(NSString *arrs in responseObject[@"retData"][@"slideshow"]){
                [imgArr addObject:arrs];
            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
        [self setUpMainScrollview];
        [self setTupTableView];
        [self seNavBar];
    }failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - init tableview
-(void)setTupTableView{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(12, 200, SCREENSIZE.width-24, SCREENSIZE.height-272) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"HotelHomeCell" bundle:nil] forCellReuseIdentifier:@"HotelHomeCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteldateCell" bundle:nil] forCellReuseIdentifier:@"HoteldateCell"];
    [myTable registerNib:[UINib nibWithNibName:@"OneofCell" bundle:nil] forCellReuseIdentifier:@"OneofCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HotelpricesCell" bundle:nil] forCellReuseIdentifier:@"HotelpricesCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteapartmentCell" bundle:nil] forCellReuseIdentifier:@"HoteapartmentCell"];
    [myTable registerNib:[UINib nibWithNibName:@"BeginsearchCell" bundle:nil] forCellReuseIdentifier:@"BeginsearchCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteSDCell" bundle:nil] forCellReuseIdentifier:@"HoteSDCell"];
    
    myTable.layer.cornerRadius = 7;
    myTable.layer.masksToBounds = YES;
    myTable.scrollEnabled =NO;
    [self.view addSubview:myTable];
    
    myTable.tableFooterView=[UIView new];
}

#pragma mark - Table Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(filterTag==0){
        if(indexPath.row==0){
            HotelHomeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelHomeCell"];
            if (!cell) {
                cell=[[HotelHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelHomeCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.cityLabel.text=chinaCity;
            return cell;
            
        }else if(indexPath.row==1){
            HoteldateCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteldateCell"];
            if (!cell) {
                cell=[[HoteldateCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteldateCell"];
            }
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            
            [cell refluStarDate:ruzhuDate endDate:endDate totalDays:totalDays];
            return  cell;
        }else if(indexPath.row==2){
            HotelpricesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelpricesCell"];
            if (!cell) {
                cell=[[HotelpricesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelpricesCell"];
            }
            
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
            }
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            UILabel *starLabel=[[UILabel alloc]initWithFrame:CGRectMake(45, 0, SCREENSIZE.width-90, 44)];
            starLabel.font=kFontSize16;
            starLabel.text=[starAndPriceStr isEqualToString:@""]?@"星级/价格":starAndPriceStr;
            starLabel.textColor=[UIColor grayColor];
            [cell addSubview:starLabel];
            
            return  cell;
            
        }else if(indexPath.row==3) {
            OneofCell *cell=[tableView dequeueReusableCellWithIdentifier:@"OneofCell"];
            if (!cell) {
                cell=[[OneofCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OneofCell"];
            }
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    [view removeFromSuperview];
                }
            }
            guoneifield =[[UITextField alloc]initWithFrame:CGRectMake(45, 0, SCREENSIZE.width-90, 44)];
            guoneifield.font=kFontSize16;
            guoneifield.placeholder=@"关键词/酒店";
            guoneifield.borderStyle=UITextBorderStyleNone;
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            
            [cell addSubview:guoneifield];
            
            return cell;
        }else{
            BeginsearchCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BeginsearchCell"];
            if (!cell) {
                cell=[[BeginsearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BeginsearchCell"];
                return cell;
            }
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UIButton class]]) {
                    [view removeFromSuperview];
                }
            }
            UIButton *btnsearch = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnsearch.frame=CGRectMake(10, 20, SCREENSIZE.width-42, 44);
            btnsearch.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            [btnsearch setTitle:@"开始搜索" forState:UIControlStateNormal];
            btnsearch.titleLabel.textAlignment = NSTextAlignmentCenter;
            [btnsearch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnsearch addTarget:self action:@selector(btnsearchclicked:) forControlEvents:UIControlEventTouchUpInside];
            [btnsearch setBackgroundColor:[UIColor colorWithRed:71/255.0 green:159/255.0 blue:236/255.0 alpha:1]];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            cell.separatorInset = UIEdgeInsetsMake(0, SCREENSIZE.width, 0, 0);
            btnsearch.layer.cornerRadius = 4.0;
            [cell addSubview:btnsearch];
            [cell.layer setMasksToBounds:YES];
            cell.layer.cornerRadius=5;
            return cell;
        }
    }else {
        if(indexPath.row==0){
            HotelHomeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelHomeCell"];
            if (!cell) {
                cell=[[HotelHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelHomeCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.cityLabel.text=overseaCity;
            return cell;
        }else if(indexPath.row==1){
            HoteSDCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteSDCell"];
            if (!cell) {
                cell=[[HoteSDCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteSDCell"];
            }
            [cell refluStarD:ruDate endDate:eDate totalDays:tDays];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            return  cell;
        }else if(indexPath.row==2){
            HotelpricesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelpricesCell"];
            if (!cell) {
                cell=[[HotelpricesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelpricesCell"];
            }
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            UILabel *starLabel=[[UILabel alloc]initWithFrame:CGRectMake(45, 0, SCREENSIZE.width-90, 44)];
            starLabel.font=kFontSize16;
            starLabel.text=[starAndPriceStr isEqualToString:@""]?@"星级/价格":starAndPriceStr;
            starLabel.textColor=[UIColor grayColor];
            [cell addSubview:starLabel];
            return  cell;
        }else if(indexPath.row==3){
            HoteapartmentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteapartmentCell"];
            if (!cell){
                cell=[[HoteapartmentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteapartmentCell"];
            }
            UILabel *labe=[[UILabel alloc]initWithFrame:CGRectMake(45, 0, 40, 44)];
            labe.font=kFontSize16;
            labe.text=@"1间,";
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
            }
            chengNum=[[UILabel alloc]initWithFrame:CGRectMake(labe.size.width+labe.origin.x, 0, 92, 44)];
            chengNum.font=kFontSize16;
            chengNum.text=[NSString stringWithFormat:@"每间%d成人，",0+[adultSum intValue]];
            ertongNum=[[UILabel alloc]initWithFrame:CGRectMake(chengNum.origin.x+chengNum.size.width+5, 0, 100, 44)];

            ertongNum.font=kFontSize16;

            ertongNum.text=[NSString stringWithFormat:@"%d儿童",0+[childSum intValue]];

            [cell addSubview:chengNum];
            [cell addSubview:ertongNum];
            [cell addSubview:labe];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            return cell;
        }else if (indexPath.row==4){
            OneofCell *cell=[tableView dequeueReusableCellWithIdentifier:@"OneofCell"];
            if (!cell) {
                cell=[[OneofCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OneofCell"];
            }
            
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    [view removeFromSuperview];
                }
            }
            guijifield =[[UITextField alloc]initWithFrame:CGRectMake(45, 0, 300, 44)];
            guijifield.font=kFontSize16;
            guijifield.placeholder=@"关键词/酒店/地名";
            guijifield.borderStyle=UITextBorderStyleNone;
            [cell addSubview:guijifield];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            
            return cell;
        }else{
            BeginsearchCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BeginsearchCell"];
            if (!cell){
                cell=[[BeginsearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BeginsearchCell"];
            }
            UIButton *btnsearch = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnsearch.frame=CGRectMake(10, 20, SCREENSIZE.width-42, 44);
            btnsearch.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            [btnsearch setTitle:@"开始搜索" forState:UIControlStateNormal];
            btnsearch.titleLabel.textAlignment = NSTextAlignmentCenter;
            [btnsearch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnsearch addTarget:self action:@selector(btnsearchclicked:) forControlEvents:UIControlEventTouchUpInside];
            [btnsearch setBackgroundColor:[UIColor colorWithRed:71/255.0 green:159/255.0 blue:236/255.0 alpha:1]];
            btnsearch.layer.cornerRadius = 4.0;
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            [cell addSubview:btnsearch];
            [cell.layer setMasksToBounds:YES];
            cell.layer.cornerRadius=5;
            cell.separatorInset = UIEdgeInsetsMake(0, SCREENSIZE.width, 0, 0);
            return cell;
        }
    }
}

//第section分区一共有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (filterTag==0) {
        return 5;
    }else
        return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 46;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell ==nil) {
        cell=[[UITableViewCell
               alloc] initWithStyle:UITableViewCellStyleDefault
              reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UIButton *chinaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    chinaBtn.frame = CGRectMake(0, 2.0f, SCREENSIZE.width/2, 44.0f);
    [chinaBtn setTitle:@"国内·港澳台" forState:UIControlStateNormal];
    [chinaBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    [chinaBtn addTarget:self action:@selector(onChinaBtn) forControlEvents:UIControlEventTouchUpInside];
    chinaBtn.titleLabel.font    = [UIFont systemFontOfSize: 17];
    [cell addSubview:chinaBtn];
    
    UIButton *overseaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    overseaBtn.frame = CGRectMake(SCREENSIZE.width/2, 2.0f, SCREENSIZE.width/2-20, 44.0f);
    [overseaBtn setTitle:@"国际酒店" forState:UIControlStateNormal];
    overseaBtn.titleLabel.font = [UIFont systemFontOfSize: 17];
    [overseaBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [overseaBtn addTarget:self action:@selector(onOverseaBtn) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:overseaBtn];
    
    if (filterTag==0) {
        [chinaBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        [overseaBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }else{
        [chinaBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [overseaBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    }
    
    UIView *vline=[[UIView alloc] initWithFrame:CGRectMake(SCREENSIZE.width/2, 16, 0.5, 16)];
    vline.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [cell addSubview:vline];
    
    UIView *hline=[[UIView alloc] initWithFrame:CGRectMake(0, 45.5, SCREENSIZE.width, 0.5f)];
    hline.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [cell addSubview:hline];
    
    return cell;
}

-(void)onChinaBtn{
    typeCN=@"CN";
    filterTag=0;

    CGRect frame=myTable.frame;
    frame.size.height=SCREENSIZE.height-272;
    myTable.frame=frame;
    
    if (Citynames.text>=0) {
        [myTable reloadData];
    }
}

-(void)onOverseaBtn{
    typeCN=@"INT";
    filterTag=1;
    
    CGRect frame=myTable.frame;
    
    frame.size.height=SCREENSIZE.height-230;
    myTable.frame=frame;
    
    if (Citynames.text>=0) {
        [myTable reloadData];
    }
}

#pragma mark - 点击开始搜索按钮
-(void)btnsearchclicked:(HotelHomeModel *)model{
    if (filterTag==0) {
        HotelListController *listCon=[[HotelListController alloc]init];
        listCon.cityName=Citynames.text;
        listCon.hotetype=typeCN;
        int aa=[s intValue];
        if (aa==0) {
            as=0;
        }else{
            as=aa+1;
        }
        listCon.sta=s==nil||s==NULL||[s isEqualToString:@""]?@"0":[NSString stringWithFormat:@"%d",as];
        
        listCon.pos=pos;
        listCon.pri=Tprice==nil||Tprice==NULL||[Tprice isEqualToString:@""]?@"0:2000":Tprice;
        listCon.chein=ruzhuDate;
        listCon.cheout=endDate;
        listCon.sea=guoneifield.text;
        listCon.numdays=totalDays;
        listCon.adult=@"1";
        listCon.child=@"0";
        listCon.childage=@"";
        listCon.dzarrs=starArr;
        listCon.Hprice=_pricers;
        listCon.pricers = _pricers;
        listCon.minPricers = _minPricers;
        [self.navigationController pushViewController:listCon animated:YES];
    }else{
        HotelListController *listCon=[[HotelListController alloc]init];
        listCon.cityName=Citynames.text;
        listCon.hotetype=typeCN;int aa=[s intValue];
        if (aa==0) {
            as=0;
        }else{
            as=aa+1;
        }
        listCon.sta=s==nil||s==NULL||[s isEqualToString:@""]?@"0":[NSString stringWithFormat:@"%d",as];
        if (wordPos==nil || [wordPos isEqualToString:@""]) {
            listCon.pos = @"CN:3354";;
        }else{
            listCon.pos=wordPos;
        }
        listCon.pri=Tprice==nil||Tprice==NULL||[Tprice isEqualToString:@""]?@"0:2000":Tprice;
        listCon.chein=ruDate;
        listCon.cheout=eDate;
        listCon.adult=adultSum;
        listCon.child=childSum;
        listCon.childage=childAge;
        listCon.sea=guijifield.text;
        listCon.dzarrs=starArr;
        listCon.pricers = _pricers;
        listCon.minPricers = _minPricers;
        [self.navigationController pushViewController:listCon animated:YES];
    }
}


//一共有多少个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//选中了UITableView的某一行
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (filterTag==0){
        if (indexPath.row==0){
            SelectCtripCityController *ctr=[SelectCtripCityController new];
            ctr.selectCityType=ChinaCityType;
            [self.navigationController pushViewController:ctr animated:YES];
        }
        else if(indexPath.row==1){
            SelectCheckDateViewController  *vc=[[SelectCheckDateViewController alloc]init];
            vc.ruzhuDate = ruzhuDate;
            vc.endDate = endDate;
            vc.allDays = totalDays;
            [vc setSelectCheckDateBlock:^(NSString *startDateStr, NSString *endDateStr, NSString *daysStr) {
                ruzhuDate=startDateStr;
                endDate=endDateStr;
                totalDays=daysStr;
                [myTable reloadRow:1 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            }];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if(indexPath.row==2){
            [self showStarAndPriceView];
        }
        else if(indexPath.row==3){
            
        }
        else{
            NSLog(@"cell点击");
        }
    }
    else{
        if (indexPath.row==0){
            SelectCtripCityController *ctr=[SelectCtripCityController new];
            ctr.selectCityType=OverseaType;
            [self.navigationController pushViewController:ctr animated:YES];
        }else if (indexPath.row==1){
            SelectCheckDateViewController  *vc=[[SelectCheckDateViewController alloc]init];
            [vc setSelectCheckDateBlock:^(NSString *startDateStr, NSString *endDateStr, NSString *daysStr) {
                ruDate=startDateStr;
                eDate=endDateStr;
                tDays=daysStr;
                [myTable reloadRow:1 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            }];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row==2){
            [self showStarAndPriceView];
        }
        else if (indexPath.row==3){
            ETongController *etong=[[ETongController alloc]init];
            [etong setSelectEtongDateBlock:^(NSString *crNum , NSString *etNum,NSString *etage){
                adultSum=crNum;
                childSum=etNum;
                childAge=etage;
                [myTable reloadRow:3 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            }];
            [self.navigationController pushViewController:etong animated:YES];
        }
        else if(indexPath.row==4){
            NSLog(@"ss");
        }
        else{
            NSLog(@"开始搜索");
        }
    }
}

//某一行的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(filterTag==0){
        if (indexPath.row==0) {
            return 72;
        }else if(indexPath.row==1){
            return 105;
        }else if(indexPath.row==2){
            return 44;
        }else if(indexPath.row==3){
            return 44;
        }else{
            return 84;
        }
    }
    else{
        if (indexPath.row==0) {
            return 72;
        }else if(indexPath.row==1)
        {
            return 105;
        }else if(indexPath.row==2||indexPath.row==3||indexPath.row==4)
        {
            return 44;
        }else{
            return 84;
        }
    }
}

-(NSString *)notRounding:(float)price afterPoint:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *ouncesDecimal;
    
    NSDecimalNumber *roundedOunces;
    
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    
    return [NSString stringWithFormat:@"%@",roundedOunces];
}

#pragma mark - 价格赛选变动
- (void)valueChanged:(WLRangeSlider *)slider{
    NSLog(@"slider.value----%f=====%f",_rangeSlider.leftValue,_rangeSlider.rightValue);
    int b;int c;
    b=ceilf(_rangeSlider.leftValue*1.0);
    c=ceilf(_rangeSlider.rightValue*1.0);
    Tprice=[NSString stringWithFormat:@"%d:%d",b,c];
    _pricers = [NSString stringWithFormat:@"%d",c];
    _minPricers = [NSString stringWithFormat:@"%d",b];
    //    NSLog(@"----zhi%@",leftValuetringWithFormat:@"%f",_rangeSlider.leftValue ] componentsSeparatedByString:@"."][1]);
    [self changeStr:[NSString stringWithFormat:@"%f",_rangeSlider.leftValue]];
    
}

-(NSString*)changeStr:(NSString*)myStr{
    NSString*str=nil;
    NSString*fenStr=[NSString stringWithFormat:@"0.%@",[myStr componentsSeparatedByString:@"."][1]];
    NSString*fen=[self notRounding:[fenStr floatValue]*60  afterPoint:0];
    
    str=[NSString stringWithFormat:@"%@:%@",[NSString stringWithFormat:@"%@",[myStr componentsSeparatedByString:@"."][0]],fen];
    NSLog(@"str--%@",str);
    return str;
}

#pragma mark - 弹出星级价格View
- (void)showStarAndPriceView {
    // ------全屏遮罩
    self.bgView                 = [[UIView alloc] init];
    self.bgView.frame           = [[UIScreen mainScreen] bounds];
    self.bgView.tag             = 100;
    self.bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.bgView.opaque = NO;
    
    //--UIWindow的优先级最高，Window包含了所有视图，在这之上添加视图，可以保证添加在最上面
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    [appWindow addSubview:self.bgView];
    
    // ------给全屏遮罩添加的点击事件
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitClick)];
    gesture.numberOfTapsRequired = 1;
    gesture.cancelsTouchesInView = NO;
    [self.bgView addGestureRecognizer:gesture];

    [UIView animateWithDuration:0.3 animations:^{
        self.bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        
    }];
    
    // ------底部弹出的View
    self.deverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREENSIZE.height)];
    self.deverView.backgroundColor = [UIColor whiteColor];
    [appWindow addSubview:self.deverView];
    
    UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(16, 18, 100, 21)];
    lab.text=@"价格筛选";
    lab.textColor=[UIColor colorWithHexString:@"707070"];
    lab.font = [UIFont systemFontOfSize:16];
    [_deverView addSubview:lab];
    
    _rangeSlider=[[WLRangeSlider alloc]initWithFrame:CGRectMake(lab.origin.x+4, lab.size.height+lab.origin.y+26, SCREENSIZE.width-(lab.origin.x+4)*2, 24)];
    _rangeSlider.leftValue = _minPricers.floatValue;
    _rangeSlider.rightValue = _pricers.floatValue;
    [_rangeSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [_deverView addSubview:_rangeSlider];
    [self valueChanged:_rangeSlider];
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab.origin.x,lab.origin.y+lab.height+80, lab.width, lab.height)];
    lab2.text=@"星级筛选";
    lab2.textColor=[UIColor colorWithHexString:@"707070"];
    lab2.font = [UIFont systemFontOfSize:16];
    [_deverView addSubview:lab2];
    
    //arr=@[@"不限", @"二星及以下",@"三星/舒适",@"四星/高档",@"五星/豪华"];
    CGFloat lastYH=0;
    CGFloat btnWidth=(SCREENSIZE.width-lab.origin.x*2-24)/4;
    CGFloat btnHeight=38;
    CGFloat btnY=lab2.origin.y+lab2.height+10;
    CGFloat btnX=lab.origin.x;
    for (int i=0; i<starArr.count; i++) {
        btnX=lab.origin.x+(i%4)*btnWidth+8*(i%4);
        btnY=(i/4)*btnHeight+(i/4)*8+lab2.origin.y+lab2.height+10;
        
       UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(btnX,btnY, btnWidth, btnHeight)];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
        [button.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
        [button.layer setBorderWidth:1.2f];
        [button.layer setMasksToBounds:YES];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:starArr[i] forState:UIControlStateNormal];
        button.tag =i;
        [choiceStarArray addObject:button];
        [button addTarget:self action:@selector(tagButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_deverView addSubview:button];
        if (s.intValue==0) {
            if(i==0){
                seleBtn=button;
                [button setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
                [button.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
            }
        }else{
            if(i==(s.intValue)){
                seleBtn=button;
                [button setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
                [button.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
            }
        }
        
        if (i==starArr.count-1) {
            lastYH=button.origin.y+button.height+20;
        }
    }
    
    UIButton *cancelBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, lastYH, self.deverView.width/2, 48)];
    cancelBtn.backgroundColor=[UIColor whiteColor];
    [cancelBtn setTitle:@"清空选择" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font=kFontSize16;
    cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancelBtn.layer.borderWidth=0.8;
    cancelBtn.layer.borderColor=[[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]CGColor];
    [cancelBtn setTitleColor:[UIColor colorWithRed:155/255.0 green:155/255.0 blue:155/255.0 alpha:1] forState:UIControlStateNormal];
    [self.deverView addSubview: cancelBtn];
    
    UIButton *deterBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.deverView.width/2, cancelBtn.origin.y, cancelBtn.width, cancelBtn.height)];
    [deterBtn setTitle:@"确 定" forState:UIControlStateNormal];
    deterBtn.titleLabel.font=kFontSize16;
    [deterBtn setBackgroundColor:[UIColor colorWithRed:70/255.0 green:161/255.0 blue:236/255.0 alpha:1]];
    deterBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [deterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deterBtn addTarget:self action:@selector(deterBrtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.deverView addSubview: deterBtn];
    
    CGRect frame=self.deverView.frame;
    frame.size.height=deterBtn.origin.y+deterBtn.height;
    frame.origin.y=SCREENSIZE.height-frame.size.height;
    self.deverView.frame=frame;
    
    // ------View出现动画
    self.deverView.transform = CGAffineTransformMakeTranslation(0.01, SCREENSIZE.height);
    [UIView animateWithDuration:0.3 animations:^{
        self.deverView.transform = CGAffineTransformMakeTranslation(0.01, 0.01);
    }];
}

#pragma mark - 点击价格赛选确定
-(void)deterBrtn:(NSString *)send{
   
        starAndPriceStr=[NSString stringWithFormat:@"%@/%@元",xuanzhe==nil||xuanzhe==NULL||[xuanzhe isEqualToString:@""]?@"不限":xuanzhe,Tprice];
        [self exitClick];
        [myTable reloadRow:2 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
    
}

-(void)cancelView{
    s=@"0";
    _minPricers = @"0";
    _pricers   = @"2000";
    Tprice  = @"0:2000";
    xuanzhe = @"不限";
    starAndPriceStr=[NSString stringWithFormat:@"不限/%d元",2000];
    [myTable reloadRow:2 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
    [self exitClick];
}

- (void)exitClick {
    [UIView animateWithDuration:0.3 animations:^{
        self.deverView.transform = CGAffineTransformMakeTranslation(0.01, SCREEN_HEIGHT);
        self.deverView.alpha = 0.2;
        self.bgView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.bgView removeFromSuperview];
        [self.deverView removeFromSuperview];
    }];
}

- (void)tagButtonClick:(UIButton *)sender{
    
//    [sender setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
//    [sender.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
//    sender.enabled=NO;
//    
//    [seleBtn setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
//    [seleBtn.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
//    seleBtn.enabled=YES;
//    seleBtn= sender;
    
    for (UIButton * selectButton in choiceStarArray) {
        if (selectButton.tag == sender.tag) {
            [sender setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
            [sender.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
        }else{
            [selectButton setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
            [selectButton.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
        }
    }
    
    s = [NSString stringWithFormat:@"%ld",sender.tag];
    xuanzhe=sender.titleLabel.text;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
