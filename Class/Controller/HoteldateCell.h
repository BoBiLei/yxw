//
//  HoteldateCell.h
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HoteldateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *hoteDateone;
@property (weak, nonatomic) IBOutlet UILabel *hoteDatetwo;
@property (weak, nonatomic) IBOutlet UILabel *Today;
@property (weak, nonatomic) IBOutlet UILabel *tomorrow;
@property (weak, nonatomic) IBOutlet UILabel *Hotecount;

-(void)refluStarDate:(NSString *)starStr endDate:(NSString *)endDate totalDays:(NSString *)days;

@end
