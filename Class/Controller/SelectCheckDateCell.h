//
//  SelectCheckDateCell.h
//  DFCalendar
//
//  Created by Macsyf on 16/12/7.
//  Copyright © 2016年 ZhouDeFa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MouthModel.h"

//typedef void(^SelectedDay)(NSString *yearMonth,NSArray *days);
typedef void(^SelectedDay)(NSInteger yearMonth);

@interface SelectCheckDateCell : UITableViewCell

-(void)fullCellWithModel:(MouthModel *)model;

@property(nonatomic,copy)SelectedDay selectedDay;

-(void)selectedDay:(SelectedDay)selectedDay;

-(void)setToday:(BOOL )isToday;

@end
