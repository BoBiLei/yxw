//
//  RoomSumChildAgeCell.h
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomSumChildAgeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UITextField *childAgeTF;

@property (nonatomic, copy) void (^RoomSumChildAgeBlock)(NSString *text);

@end
