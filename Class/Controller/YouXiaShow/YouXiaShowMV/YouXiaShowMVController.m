//
//  YouXiaShowMVController.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowMVController.h"
#import "YouXiaShowAloneMVCell.h"
#import "YouXiaShowMVDetailController.h"

@interface YouXiaShowMVController ()<DOPDropDownMenuDataSource,DOPDropDownMenuDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YouXiaShowMVController{
    
    DOPDropDownMenu *menu;
    NSString *distId;
    NSString *hotNewId;
    NSArray *distArr;
    NSArray *hotNewArr;
    
    
    NSMutableArray *dataArr;
    UICollectionView *myCollection;
    
    NSInteger page;
    
    MJRefreshAutoNormalFooter *footer;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"游侠秀MV页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"游侠秀MV页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"游侠秀MV";
    [self.view addSubview:navBar];
    
    [self initCollectionView];
    
    [self setUpDopMenu];
}

#pragma mark - init CollectionView
-(void)initCollectionView{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    
    myCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) collectionViewLayout:myLayout];
    [myCollection registerNib:[UINib nibWithNibName:@"YouXiaShowAloneMVCell" bundle:nil] forCellWithReuseIdentifier:@"YouXiaShowAloneMVCell"];
    myCollection.backgroundColor=[UIColor whiteColor];
    myCollection.dataSource=self;
    myCollection.delegate=self;
    [self.view addSubview:myCollection];
    
    //
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = kFontSize13;
    footer.stateLabel.textColor = [UIColor colorWithHexString:@"#282828"];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    myCollection.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (page!=1) {
        [self fillterKezhaoRequestDistingceId:distId ntId:hotNewId andPage:page];
    }
}

-(void)setUpDopMenu{
    
    distId=@"all";
    hotNewId=@"zx";
    
    // 数据
    NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"yxs_distingce" ofType:@"plist"];
    distArr = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
    
    NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"yxs_newhot" ofType:@"plist"];
    hotNewArr = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
    
    // 添加下拉菜单
    menu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    menu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    menu.textColor=[UIColor colorWithHexString:@"#686868"];
    menu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    menu.delegate = self;
    menu.dataSource = self;
    [menu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:menu];
    
    // 创建menu 第一次显示 不会调用点击代理，可以用这个手动调用
    [menu selectDefalutIndexPath];
}

#pragma mark -DOPMenu Delegate
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu{
    return 2;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column{
    if (column == 0) {
        return distArr.count;
    }else{
        return hotNewArr.count;
    }
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath{
    if (indexPath.column == 0) {
        return distArr[indexPath.row][@"name"];
    }else {
        return hotNewArr[indexPath.row][@"name"];
    }
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath{
    
    dataArr=[NSMutableArray array];
    
    page=1;  //默认第一页（每次点击都把page设置为第一页）
    
    if (indexPath.column == 0) {
        distId=distArr[indexPath.row][@"id"];
    }else{
        hotNewId=hotNewArr[indexPath.row][@"id"];
    }
    [self fillterKezhaoRequestDistingceId:distId ntId:hotNewId andPage:page];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YouXiaShowAloneMVCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"YouXiaShowAloneMVCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[YouXiaShowAloneMVCell alloc]init];
    }
    
    if (dataArr.count!=0) {
        YouXiaShowMVModel *model=dataArr[indexPath.row];
        cell.model=model;
    }
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREENSIZE.width-16, SCREENSIZE.width*2/3-20);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//每个item之间的间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10.0f;
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 8, 10, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YouXiaShowMVModel *model=dataArr[indexPath.row];
    YouXiaShowMVDetailController *ctr=[YouXiaShowMVDetailController new];
    ctr.titleName=@"游侠秀MV详情";
    ctr.photoId=model.mvId;
    ctr.cid=model.cId;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - 筛选请求
//筛选请求
-(void)fillterKezhaoRequestDistingceId:(NSString *)disId ntId:(NSString *)ntId andPage:(NSInteger)cpage{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"m":@"wap",
                         @"a":@"list_video_v6_ios",
                         @"page":[NSString stringWithFormat:@"%ld",(long)cpage],
                         @"mudi":disId,
                         @"order":ntId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:@"https://m.youxia.com/index.php?" method:HttpRequestPost parameters:dict prepareExecute:^{
        DSLog(@"%@",dict);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"data"];
            for (NSDictionary *dic in arr) {
                YouXiaShowMVModel *model=[[YouXiaShowMVModel alloc]init];
                model.model=dic;
                [dataArr addObject:model];
            }
            
            if (dataArr.count<=0) {
                page=1;
                [footer setTitle:@"" forState:MJRefreshStateIdle];
            }else{
                page++;
            }
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
