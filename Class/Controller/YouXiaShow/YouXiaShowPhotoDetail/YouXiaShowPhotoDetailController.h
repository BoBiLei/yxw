//
//  YouXiaShowPhotoDetailController.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    yxxPhoto,
    yxxMV,
} YXSDetailType;

@interface YouXiaShowPhotoDetailController : UIViewController

@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSString *photoId;
@property (nonatomic, copy) NSString *cid;

@property (nonatomic, assign) YXSDetailType type;

@end
