//
//  YouXiaShowPhotoDetailController.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowPhotoDetailController.h"
#import <WebKit/WebKit.h>

@interface YouXiaShowPhotoDetailController ()<WKNavigationDelegate, WKUIDelegate>

@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) UIProgressView *progressView;

@end

@implementation YouXiaShowPhotoDetailController{
    UINavigationBar *cusBar;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self deleteWebCache];
    
    [TalkingData trackPageBegin:@"游侠秀详情页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"游侠秀详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"游侠秀详情";
    [self.view addSubview:navBar];
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    _webView.navigationDelegate = self;
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.view addSubview:_webView];
    
    _progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(0, 61, CGRectGetWidth(self.view.frame),2)];
    _progressView.trackTintColor=[UIColor colorWithHexString:@"#0080c5"];
    _progressView.progressTintColor=[UIColor whiteColor];
    [self.view addSubview:_progressView];
    
    NSString *urlStr;
    if (self.type==yxxPhoto) {
        urlStr=[NSString stringWithFormat:@"%@index.php?m=wap&c=index&a=show&catid=%@&id=%@&typeid=12&is_v6=1&is_iso=1",DefaultHost,self.cid,self.photoId];
    }else{
        urlStr=[NSString stringWithFormat:@"%@index.php?m=wap&c=index&a=show&catid=%@&id=%@&is_v6=1&is_iso=1",DefaultHost,self.cid,self.photoId];
    }
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
}


#pragma mark - webview delegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
}

// 计算进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == _webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            _progressView.hidden = YES;
            [_progressView setProgress:0 animated:NO];
        }else {
            _progressView.hidden = NO;
            [_progressView setProgress:_webView.estimatedProgress animated:YES];
        }
    }
}

#pragma mark - 清除缓存
- (void)deleteWebCache {
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
        
        NSSet *websiteDataTypes
        
        = [NSSet setWithArray:@[
                                
                                //                                WKWebsiteDataTypeDiskCache,
                                
                                //                                WKWebsiteDataTypeOfflineWebApplicationCache,
                                
                                //                                WKWebsiteDataTypeMemoryCache,
                                
                                //                                WKWebsiteDataTypeLocalStorage,
                                
                                WKWebsiteDataTypeCookies,
                                
                                //                                WKWebsiteDataTypeSessionStorage,
                                
                                //                                WKWebsiteDataTypeIndexedDBDatabases,
                                
                                //                                WKWebsiteDataTypeWebSQLDatabases
                                
                                ]];
        
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
            // Done
        }];
    } else {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        
        NSError *errors;
        
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        
    }
}

- (void)dealloc {
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
}
@end
