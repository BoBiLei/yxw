//
//  YouXiaShowMVModel.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouXiaShowMVModel : NSObject

@property (nonatomic, copy) NSString *mvId;
@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *mvImg;
@property (nonatomic, copy) NSString *mvTitle;
@property (nonatomic, copy) NSString *mvTime;
@property (nonatomic, copy) NSString *mvUrl;

@property (nonatomic, retain) NSDictionary *model;

@end
