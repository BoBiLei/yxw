//
//  YouXiaShowMVModel.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowMVModel.h"

@implementation YouXiaShowMVModel

-(void)setModel:(NSDictionary *)model{
    self.mvId=model[@"id"];
    self.cId=model[@"catid"];
    self.mvImg=model[@"thumb"];
    self.mvTitle=model[@"title"];
    self.mvTime=model[@"inputtime"];
    self.mvUrl=model[@"video_m_url"];
}

@end
