//
//  YouXiaShowXCModel.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowXCModel.h"

@implementation YouXiaShowXCModel

-(void)setModel:(NSDictionary *)model{
    self.photoId=model[@"id"];
    self.cId=model[@"catid"];
    self.photoImg=model[@"thumb"];
    self.photoTitle=model[@"title"];
    self.photoTime=model[@"inputtime"];
}

@end
