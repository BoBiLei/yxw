//
//  YouXiaShowXCModel.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouXiaShowXCModel : NSObject

@property (nonatomic, copy) NSString *photoId;
@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *photoImg;
@property (nonatomic, copy) NSString *photoTitle;
@property (nonatomic, copy) NSString *photoTime;

@property (nonatomic, retain) NSDictionary *model;

@end
