//
//  YouXiaShowMVDetailController.h
//  youxia
//
//  Created by mac on 16/5/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouXiaShowMVDetailController : UIViewController

@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSString *photoId;
@property (nonatomic, copy) NSString *cid;

@end
