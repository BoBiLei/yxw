//
//  YouXiaShowMVDetailController.m
//  youxia
//
//  Created by mac on 16/5/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowMVDetailController.h"

@interface YouXiaShowMVDetailController ()<UIWebViewDelegate,WKNavigationDelegate>

@property WebViewJavascriptBridge* bridge;
@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) UIProgressView *progressView;

@end

@implementation YouXiaShowMVDetailController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self deleteWebCache];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=self.titleName;
    [self.view addSubview:navBar];
    
    [self setUpUI];
}

-(void)setUpUI{
    
    if (_bridge) { return; }
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    _webView.backgroundColor=[UIColor whiteColor];
    _webView.navigationDelegate = self;
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.view addSubview:_webView];
    
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 63, SCREENSIZE.width, 1.5)];
    _progressView.trackTintColor=[UIColor colorWithHexString:@"#0080c5"];
    _progressView.progressTintColor=[UIColor whiteColor];
    [self.view addSubview:_progressView];
    
    _bridge = [WebViewJavascriptBridge bridgeForWebView:_webView];
    [_bridge setWebViewDelegate:self];
    
    [_bridge registerHandler:@"ObjcCallback" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self handleCallBackForData:data];
    }];
    
    NSString *urlStr=[NSString stringWithFormat:@"%@index.php?m=wap&c=index&a=show&catid=%@&id=%@&is_v6=1&is_iso=1",YouxiaXiu,self.cid,self.photoId];
    NSLog(@"%@",urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - 处理js返回数据
-(void)handleCallBackForData:(id)data{
    self.cid=data[@"catid"];
    self.photoId=data[@"id"];
    NSString *urlStr=[NSString stringWithFormat:@"%@index.php?m=wap&c=index&a=show&catid=%@&id=%@&is_v6=1&is_iso=1",YouxiaXiu,self.cid,self.photoId];
    NSURL *url = [NSURL URLWithString:urlStr];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - webview delegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
}

// 计算进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == _webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            _progressView.hidden = YES;
            [_progressView setProgress:0 animated:NO];
        }else {
            _progressView.hidden = NO;
            [_progressView setProgress:_webView.estimatedProgress animated:YES];
        }
    }
}

#pragma mark - 清除缓存
- (void)deleteWebCache {
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
        
        NSSet *websiteDataTypes
        
        = [NSSet setWithArray:@[
                                
                                //                                WKWebsiteDataTypeDiskCache,
                                
                                //                                WKWebsiteDataTypeOfflineWebApplicationCache,
                                
                                //                                WKWebsiteDataTypeMemoryCache,
                                
                                //                                WKWebsiteDataTypeLocalStorage,
                                
                                WKWebsiteDataTypeCookies,
                                
                                //                                WKWebsiteDataTypeSessionStorage,
                                
                                //                                WKWebsiteDataTypeIndexedDBDatabases,
                                
                                //                                WKWebsiteDataTypeWebSQLDatabases
                                
                                ]];
        
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
            // Done
        }];
    } else {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        
        NSError *errors;
        
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        
    }
}

// 记得取消监听
- (void)dealloc {
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

@end
