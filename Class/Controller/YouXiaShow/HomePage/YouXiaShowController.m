//
//  YouXiaShowController.m
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowController.h"
#import "NewThemticHeaderCell.h"
#import "HomePageReusableView.h"
#import "SearchViewController.h"
#import "StrategyDetailController.h"
#import "NewTSHeaderCell.h"     //新游记攻略顶部
#import "NewTSItem.h"           //新游记攻略item
#import "IslandOnlineController.h"
#import "NewMaldiThematiController.h"
#import "PalauDistingceController.h"
#import "PalauThemticController.h"
#import "YouXiaShowPhotoCell.h"
#import "YouXiaShowXCCell.h"
#import "YouXiaShowMVCell.h"
#import "YouXiaShowKezhaoController.h"
#import "YouXiaShowXCController.h"
#import "YouXiaShowPhotoDetailController.h"
#import "YouXiaShowMVController.h"
#import "ThemticStragoryController.h"

#define cellWidth (SCREENSIZE.width-32)/3
#define NavigaBar_Height 64
@interface YouXiaShowController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HomePageReusableViewDelegate,ThemticBarnerEventDelegate,ThemticRecommendDelegate>

@end

@implementation YouXiaShowController{
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    UIImageView *phImgv;
    CGFloat topContentInset;
    
    UIWebView *phoneView;
    NSURLRequest *phoneRequest;
    
    //
    NSMutableArray *kezhaoArr;          //客照
    NSMutableArray *xcArr;              //相册
    NSMutableArray *mvArr;              //MV
    NSMutableArray *straHeaderArr;      //游记攻略header
    NSMutableArray *straArr;            //游记攻略
    
    
    UICollectionView *homePageCollection;
    
    HomePageReusableView *hpReusableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    topContentInset = IMAGEPLAY_HEIHGT;
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initCollectionView];
    
    [self setUpCustomSearBar];
    
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestHttpsForYouXiaShow];
        }
    };
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    CGFloat phoneWidth=44;
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    
    searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-phoneWidth-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"目的地/岛屿/酒店";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    //
    phImgv=[[UIImageView alloc] initWithFrame:CGRectMake(cusBar.width-searchView.height-10, searchView.origin.y, searchView.height, searchView.height)];
    phImgv.userInteractionEnabled=YES;
    phImgv.image=[UIImage imageNamed:@"search_phone"];
    phImgv.userInteractionEnabled=YES;
    [self.view addSubview:phImgv];
    UITapGestureRecognizer *phTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [phImgv addGestureRecognizer:phTap];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

-(void)callPhone{
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - init CollectionView
-(void)initCollectionView{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    
    homePageCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    homePageCollection.hidden=YES;
    homePageCollection.showsVerticalScrollIndicator=NO;
    [homePageCollection registerNib:[UINib nibWithNibName:@"YouXiaShowMVCell" bundle:nil] forCellWithReuseIdentifier:@"YouXiaShowMVCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"YouXiaShowXCCell" bundle:nil] forCellWithReuseIdentifier:@"YouXiaShowXCCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSItem" bundle:nil] forCellWithReuseIdentifier:@"NewTSItem"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NewTSHeaderCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewThemticHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"headercell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"YouXiaShowPhotoCell" bundle:nil] forCellWithReuseIdentifier:@"YouXiaShowPhotoCell"];
    homePageCollection.backgroundColor=[UIColor groupTableViewBackgroundColor];
    homePageCollection.dataSource=self;
    homePageCollection.delegate=self;
    
    [homePageCollection registerNib:[UINib nibWithNibName:@"HomePageReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview"];
    
    [self.view addSubview:homePageCollection];
}


#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 6;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else if (section==1){
        return kezhaoArr.count;
    }else if (section==2){
        return xcArr.count;
    }else if (section==3){
        return mvArr.count;
    }else if (section==4){
        return straHeaderArr.count;
    }else{
        return straArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        NewThemticHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"headercell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewThemticHeaderCell alloc]init];
        }
        cell.barnerClickDelegate=self;
        cell.themticRecommendDelegate=self;
        return cell;
    }else if (indexPath.section==1){
        YouXiaShowPhotoCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"YouXiaShowPhotoCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[YouXiaShowPhotoCell alloc]init];
        }
        YouXiaShowPhotoModel *model=kezhaoArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==2){
        YouXiaShowXCCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"YouXiaShowXCCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[YouXiaShowXCCell alloc]init];
        }
        YouXiaShowXCModel *model=xcArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==3){
        YouXiaShowMVCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"YouXiaShowMVCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[YouXiaShowMVCell alloc]init];
        }
        YouXiaShowMVModel *model=mvArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==4){
        NewTSHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSHeaderCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSHeaderCell alloc]init];
        }
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        cell.model=model;
        return cell;
    }else{
        NewTSItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSItem" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSItem alloc]init];
        }
        HomepageStrategyModel *model=straArr[indexPath.row];
        cell.model=model;
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, Themtic_IMAGEPLAY_HEIHGT);
    }else if(indexPath.section==1){
        return CGSizeMake((SCREENSIZE.width)/2-14, IS_IPHONE5?130:150);
    }else if(indexPath.section==2){
        return CGSizeMake((SCREENSIZE.width-32)/3, SCREENSIZE.width/2);
    }else if (indexPath.section==3){
        return CGSizeMake((SCREENSIZE.width)/2-14, IS_IPHONE5?130:150);
    }else if (indexPath.section==4){
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3+29);
    }else{
        return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width-32)/3-28);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//每个item之间的间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==4||section==5) {
        return 0.0f;
    }else{
        return 9.0f;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    switch (section) {
        case 0:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        case 1:
            return UIEdgeInsetsMake(10, 9, 10, 9);
            break;
        case 2:
            return UIEdgeInsetsMake(8, 8, 8, 8);
            break;
        case 3:
            return UIEdgeInsetsMake(10, 9, 10, 9);
            break;
        case 4:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        default:
            return UIEdgeInsetsMake(0, 0, 12, 0);
            break;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==1) {
        YouXiaShowPhotoModel *model=kezhaoArr[indexPath.row];
        YouXiaShowPhotoDetailController *ctr=[YouXiaShowPhotoDetailController new];
        ctr.titleName=@"游侠秀客照";
        ctr.photoId=model.photoId;
        ctr.cid=model.cId;
        ctr.type=yxxPhoto;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (indexPath.section==2){
        YouXiaShowXCModel *model=xcArr[indexPath.row];
        YouXiaShowPhotoDetailController *ctr=[YouXiaShowPhotoDetailController new];
        ctr.titleName=@"游侠秀相册";
        ctr.photoId=model.photoId;
        ctr.cid=model.cId;
        ctr.type=yxxPhoto;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (indexPath.section==3){
        YouXiaShowMVModel *model=mvArr[indexPath.row];
        YouXiaShowPhotoDetailController *ctr=[YouXiaShowPhotoDetailController new];
        ctr.titleName=@"游侠秀MV";
        ctr.photoId=model.mvId;
        ctr.cid=model.cId;
        ctr.type=yxxMV;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (indexPath.section==4){
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if (indexPath.section==5){
        HomepageStrategyModel *model=straArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview" forIndexPath:indexPath];
    [hpReusableView reflushYXSDataForIndexPath:indexPath];
    hpReusableView.delegate=self;
    reusableview = hpReusableView;
    return reusableview;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (section==0||section==5) {
        return CGSizeMake(0, 0);
    }else{
        return CGSizeMake(SCREENSIZE.width, 56);
    }
}

#pragma mark - HeaderviewCategory delegate
-(void)clickCollectionItem:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击Barner广告 Delegate
//广告
-(void)clickThemticBarner:(ImageModel *)model{
    if ([model.advType isEqualToString:@"maldives"]) {
        NewMaldiThematiController *malCtr=[NewMaldiThematiController new];
        [self.navigationController pushViewController:malCtr animated:YES];
    }else if ([model.advType isEqualToString:@"palau"]) {
        PalauThemticController *plCtr=[PalauThemticController new];
        [self.navigationController pushViewController:plCtr animated:YES];
    }else if ([model.advType isEqualToString:@"show"]) {
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"zxxd"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"links"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",model.imgId]]];
    }else if ([model.advType isEqualToString:@"strategy_maldives"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_palau"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi"]) {
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_show"]) {
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.imgId;
        strCtr.cId=@"234";
        strCtr.strategyTitle=@"title";
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi_list"]) {
        
    }
}

//推荐
-(void)clickThemticRecommend:(ThemticRecommendModel *)model{
    
    if ([model.name isEqualToString:@"客照样片"]) {
        YouXiaShowKezhaoController *ctr=[YouXiaShowKezhaoController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"相册"]){
        YouXiaShowXCController *ctr=[YouXiaShowXCController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"游侠秀mv"]){
        YouXiaShowMVController *ctr=[YouXiaShowMVController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else{
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=model.url;
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}


#pragma mark - Reusableview delegate
-(void)clickMore:(NSString *)strId{
    DSLog(@"%@",strId);
    if ([strId isEqualToString:@"11"]) {
        YouXiaShowKezhaoController *ctr=[YouXiaShowKezhaoController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"22"]){
        YouXiaShowXCController *ctr=[YouXiaShowXCController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"33"]){
        YouXiaShowMVController *ctr=[YouXiaShowMVController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else{
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=@"https://m.youxia.com/index.php?m=wap&c=index&a=list_strategy_v6_ios&is_iso=1";
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offsetY = scrollView.contentOffset.y+homePageCollection.contentInset.top;
    
    if (offsetY<0) {
        searchView.hidden=YES;
        phImgv.hidden=YES;
    }else{
        searchView.hidden=NO;
        phImgv.hidden=NO;
    }
    
    if (offsetY<-44) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((Themtic_IMAGEPLAY_HEIHGT-NavigaBar_Height - offsetY) / topContentInset));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForYouXiaShow{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        kezhaoArr=[NSMutableArray array];
        xcArr=[NSMutableArray array];
        mvArr=[NSMutableArray array];
        straHeaderArr=[NSMutableArray array];
        straArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&c=index&a=index_yxx_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            DSLog(@"%@",responseObject);
            [AppUtils dismissHUDInView:self.view];
            homePageCollection.hidden=NO;
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //banner
                NSArray *imgArr=responseObject[@"retData"][@"banner"];
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitPageImageNoti object:imgArr];
                
                //tuijianwei
                NSArray *tjwArr=responseObject[@"retData"][@"tuijianwei"];
                NSMutableArray *headTjwArr=[NSMutableArray array];
                for (NSDictionary *dic in tjwArr) {
                    ThemticRecommendModel *model=[ThemticRecommendModel new];
                    model.model=dic;
                    [headTjwArr addObject:model];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitTJWImageNoti object:headTjwArr];
                
                //游侠秀客照
                NSArray *hsArr=responseObject[@"retData"][@"kezhao"];
                for (NSDictionary *dic in hsArr) {
                    YouXiaShowPhotoModel *model=[YouXiaShowPhotoModel new];
                    model.model=dic;
                    [kezhaoArr addObject:model];
                }
                
                //相册
                NSArray *jdArr=responseObject[@"retData"][@"xiangce"];
                DSLog(@"%ld",(unsigned long)jdArr.count);
                for (NSDictionary *dic in jdArr) {
                    YouXiaShowXCModel *model=[YouXiaShowXCModel new];
                    model.model=dic;
                    [xcArr addObject:model];
                }
                
                //MV
                NSArray *dyArr=responseObject[@"retData"][@"video"];
                for (NSDictionary *dic in dyArr) {
                    YouXiaShowMVModel *model=[YouXiaShowMVModel new];
                    model.model=dic;
                    [mvArr addObject:model];
                }
                
                //游记攻略
                NSArray *glArr=responseObject[@"retData"][@"strategy_data"];
                for (int i=0; i<glArr.count; i++) {
                    NSDictionary *dic=glArr[i];
                    if (i==0) {
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straHeaderArr addObject:model];
                    }else{
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straArr addObject:model];
                    }
                }
                
            }
            [homePageCollection reloadData];
            [homePageCollection.mj_header endRefreshing];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
            [self requestHttpsForYouXiaShow];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
