//
//  YouXiaShowAloneXCCell.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowAloneXCCell.h"

#define IMGWidth (SCREENSIZE.width-24)/2
#define IMGHeight SCREENSIZE.width*2/3
@implementation YouXiaShowAloneXCCell{
    UIImageView *islandImg;
    UILabel *titleLabel;
    TTTAttributedLabel *timeLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.width=IMGWidth;
    frame.size.height=IMGHeight;
    self.frame=frame;
    
    //图片
    islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGWidth, self.height)];
    [self addSubview:islandImg];
    
    //
    UIView *labelView=[[UIView alloc] initWithFrame:CGRectMake(0, self.height-44, self.width, 44)];
    labelView.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    labelView.alpha=0.85f;
    [self addSubview:labelView];
    
    //
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, 2, islandImg.width-8, 22)];
    titleLabel.font=kFontSize14;
    titleLabel.textColor=[UIColor whiteColor];
    [labelView addSubview:titleLabel];
    
    //timeLabel
    timeLabel=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height, titleLabel.width, 20)];
    timeLabel.font=kFontSize12;
    timeLabel.textColor=[UIColor whiteColor];
    [labelView addSubview:timeLabel];
}

-(void)setModel:(YouXiaShowXCModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.photoImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.photoImg];
        islandImg.image=image;
    }else{
        [islandImg sd_setImageWithURL:[NSURL URLWithString:model.photoImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x307"]];
    }
    
    titleLabel.text=model.photoTitle;
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.photoTime];
    timeLabel.text=timeStr;
}

@end
