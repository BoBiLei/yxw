//
//  YouXiaShowPhotoCell.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowPhotoCell.h"

#define IMGHEIGHT (SCREENSIZE.width)/2-14
@implementation YouXiaShowPhotoCell{
    UIImageView *islandImg;
    UILabel *titleLabel;
    UILabel *timeLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.height=IS_IPHONE5?130:150;
    
    //图片
    islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGHEIGHT, IS_IPHONE5?00:100)];
    [self addSubview:islandImg];
    
    //
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, islandImg.height+5, IMGHEIGHT-8, 19)];
    titleLabel.font=kFontSize13;
    titleLabel.textColor=[UIColor colorWithHexString:@"#555555"];
    [self addSubview:titleLabel];
    
    //timeLabel
    timeLabel=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height+2, titleLabel.width, 19)];
    timeLabel.font=kFontSize12;
    timeLabel.textColor=[UIColor colorWithHexString:@"#989898"];
    [self addSubview:timeLabel];
}

-(void)setModel:(YouXiaShowPhotoModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.photoImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.photoImg];
        islandImg.image=image;
    }else{
        [islandImg sd_setImageWithURL:[NSURL URLWithString:model.photoImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x172"]];
    }
    titleLabel.text=model.photoTitle;
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.photoTime];
    timeLabel.text=timeStr;
}

@end
