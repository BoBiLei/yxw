//
//  YouXiaShowXCCell.m
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowXCCell.h"

#define IMGHEIGHT (SCREENSIZE.width-32)/3
@implementation YouXiaShowXCCell{
    UIImageView *islandImg;
    UILabel *titleLabel;
    UILabel *timeLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.width=IMGHEIGHT;
    frame.size.height=SCREENSIZE.width/2;
    self.frame=frame;
    
    //图片
    islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGHEIGHT, self.height-44)];
    [self addSubview:islandImg];
    
    //
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, islandImg.height+5, islandImg.width-8, 18)];
    titleLabel.font=kFontSize13;
    titleLabel.textColor=[UIColor colorWithHexString:@"#555555"];
    [self addSubview:titleLabel];
    
    //timeLabel
    timeLabel=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height+1, titleLabel.width, 18)];
    timeLabel.font=kFontSize12;
    timeLabel.textColor=[UIColor colorWithHexString:@"#989898"];
    [self addSubview:timeLabel];
}

-(void)setModel:(YouXiaShowXCModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.photoImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.photoImg];
        islandImg.image=image;
    }else{
        [islandImg sd_setImageWithURL:[NSURL URLWithString:model.photoImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x307"]];
    }
    
    titleLabel.text=model.photoTitle;
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.photoTime];
    timeLabel.text=timeStr;
}


@end
