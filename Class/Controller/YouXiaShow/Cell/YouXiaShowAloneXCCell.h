//
//  YouXiaShowAloneXCCell.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  游侠Show独立

#import <UIKit/UIKit.h>
#import "YouXiaShowXCModel.h"

@interface YouXiaShowAloneXCCell : UICollectionViewCell

@property (nonatomic, retain) YouXiaShowXCModel *model;

@end
