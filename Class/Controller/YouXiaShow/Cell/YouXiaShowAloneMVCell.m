//
//  YouXiaShowAloneMVCell.m
//  youxia
//
//  Created by mac on 16/5/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowAloneMVCell.h"

#define IMGWidth SCREENSIZE.width-16
#define IMGHeight SCREENSIZE.width*2/3-20

@implementation YouXiaShowAloneMVCell{
    UIImageView *islandImg;
    UILabel *titleLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.width=IMGWidth;
    frame.size.height=IMGHeight;
    self.frame=frame;
    
    //图片
    islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGWidth, self.height)];
    [self addSubview:islandImg];
    
    UIImageView *playImg=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 76, 76)];
    playImg.image=[UIImage imageNamed:@"yxshow_play"];
    playImg.center=islandImg.center;
    [self addSubview:playImg];
    
    //
    UIView *labelView=[[UIView alloc] initWithFrame:CGRectMake(0, self.height-44, self.width, 44)];
    labelView.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    labelView.alpha=0.85f;
    [self addSubview:labelView];
    
    //
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, 2, islandImg.width-8, 40)];
    titleLabel.font=kFontSize14;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.numberOfLines=0;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    [labelView addSubview:titleLabel];
    
}

-(void)setModel:(YouXiaShowMVModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.mvImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.mvImg];
        islandImg.image=image;
    }else{
        [islandImg sd_setImageWithURL:[NSURL URLWithString:model.mvImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x316"]];
    }
    
    titleLabel.text=model.mvTitle;
}

@end
