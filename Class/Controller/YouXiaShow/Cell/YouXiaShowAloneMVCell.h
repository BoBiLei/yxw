//
//  YouXiaShowAloneMVCell.h
//  youxia
//
//  Created by mac on 16/5/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YouXiaShowMVModel.h"

@interface YouXiaShowAloneMVCell : UICollectionViewCell

@property (nonatomic, retain) YouXiaShowMVModel *model;

@end
