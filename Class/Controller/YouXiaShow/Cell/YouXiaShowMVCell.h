//
//  YouXiaShowMVCell.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  游侠Show MV

#import <UIKit/UIKit.h>
#import "YouXiaShowMVModel.h"

@interface YouXiaShowMVCell : UICollectionViewCell

@property (nonatomic, retain) YouXiaShowMVModel *model;

@end
