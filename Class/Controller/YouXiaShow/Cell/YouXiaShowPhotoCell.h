//
//  YouXiaShowPhotoCell.h
//  youxia
//
//  Created by mac on 16/5/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  客照

#import <UIKit/UIKit.h>
#import "YouXiaShowPhotoModel.h"
@interface YouXiaShowPhotoCell : UICollectionViewCell

@property (nonatomic, retain) YouXiaShowPhotoModel *model;

@end
