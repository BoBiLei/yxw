//
//  HotePricesController.m
//  Example
//
//  Created by sq on 2017/1/6.
//
//

#import "HotePricesController.h"
#import "Masonry.h"
#import "LiuXSlider.h"
/////
#define AniDuration   (0.25f)

#define BaseWindow    ([UIApplication sharedApplication].keyWindow)
#define PopViewSize   ([UIApplication sharedApplication].keyWindow.bounds.size)
#define PopViewBounds ([UIApplication sharedApplication].keyWindow.bounds)

#define PopViewInitBackColor ([UIColor colorWithWhite:0.0f alpha:0.0f])
#define PopViewPopBackColor  ([UIColor colorWithWhite:0.0f alpha:0.4])

#define ContentViewHt        (500*(ScreenWd/375.0))
#define ContentViewInitFrame (CGRectMake(0, PopViewSize.height, PopViewSize.width, ContentViewHt))
#define ContentViewPopFrame  (CGRectMake(0, PopViewSize.height-ContentViewHt, PopViewSize.width, ContentViewHt))

/////
#define ScreenWd ([[UIScreen mainScreen] bounds].size.width)
#define ScreenHt ([[UIScreen mainScreen] bounds].size.height)
#define Ratio    (ScreenWd/375.0)
#define UIColorFromRGBA(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(rgbValue & 0x0000FF))/255.0 alpha:1]

////
#define InfoViewHt  (106.5*Ratio)

////
#define SizeViewHt      ([self popSizeViewHt])
#define SizeItemWd      (68*Ratio)
#define SizeItemHt      (28*Ratio)
#define SizeLineSpacing (12*Ratio)
#define SizeHeaderHt    (52*Ratio)
#define SizeFooterHt    (20*Ratio)

///
#define ColorViewHt      ([self popColorViewHt])
#define ColorItemWd      (68*Ratio)
#define ColorItemHt      (28*Ratio)
#define ColorLineSpacing (12*Ratio)
#define ColorHeaderHt    (52*Ratio)
#define ColorFooterHt    (20*Ratio)

///
#define NumViewHt        (153*Ratio)




#define SelectViewBgColor   [UIColor colorWithRed:9/255.0 green:170/255.0 blue:238/255.0 alpha:1]
#define defaultViewBgColor  [UIColor lightGrayColor]

#define LiuXSlideWidth      (self.bounds.size.width)
#define LiuXSliderHight     (self.bounds.size.height)

#define LiuXSliderTitle_H   (LiuXSliderHight*.3)

#define CenterImage_W       26.0

#define LiuXSliderLine_W    (LiuXSlideWidth-CenterImage_W)
#define LiuXSLiderLine_H    6.0
#define LiuXSliderLine_Y    (LiuXSliderHight-LiuXSliderTitle_H)


#define CenterImage_Y       (LiuXSliderLine_Y+(LiuXSLiderLine_H/2))

////////
@interface UICollectionItemViewCell : UICollectionViewCell
{

    
}

@property (nonatomic, weak) UILabel *textLabel;
@property (nonatomic,assign)CGFloat defaultIndx;

/**
 *  必传，传入节点数组
 */
@property (nonatomic,strong)NSArray *titleArray;

/**
 *  首，末位置的title
 */
@property (nonatomic,strong)NSArray *firstAndLastTitles;
/**
 *  传入图片
 */
@property (nonatomic,strong)UIImage *sliderImage;

@property (strong,nonatomic)UIView *selectView;
@property (strong,nonatomic)UIView *defaultView;
@property (strong,nonatomic)UIImageView *centerImage;
@end

@implementation UICollectionItemViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        self.clipsToBounds = YES;
        self.layer.cornerRadius = SizeItemHt/2;
        self.backgroundColor = UIColorFromRGBA(0xF2F2F2);
        
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.textColor = UIColorFromRGBA(0x666666);
        textLabel.font = [UIFont systemFontOfSize:14*Ratio];
        textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:textLabel];
        [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self);
            make.size.mas_equalTo(self);
        }];
        self.textLabel = textLabel;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.textLabel.textColor = selected? UIColorFromRGBA(0xFFFFFF): UIColorFromRGBA(0x666666);
    self.backgroundColor = selected? UIColorFromRGBA(0xF40C53): UIColorFromRGBA(0xF2F2F2);
}

@end


////////
@interface HotePricesController () <UICollectionViewDataSource, UICollectionViewDelegate>

//PopContentView
@property (nonatomic) UIView *popContentView;

//info
@property (nonatomic, weak) UIImageView *headerImageView;
@property (nonatomic, weak) UILabel *priceLabel;
@property (nonatomic, weak) UILabel *goodsNoLabel;
@property (nonatomic, weak) UISlider *SLider;

//size
@property (nonatomic) NSArray *sizeData;
@property (nonatomic, weak) UICollectionView *sizeCollectionView;

//color
@property (nonatomic) NSArray *colorData;
@property (nonatomic, weak) UICollectionView *colorCollectionView;

@end

@implementation HotePricesController
{
    CGFloat _pointX;
    NSInteger _sectionIndex;//当前选中的那个
    CGFloat _sectionLength;//根据数组分段后一段的长度
    UILabel *_selectLab;
    UILabel *_leftLab;
    UILabel *_rightLab;


}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self popShow];
    [self initData];
//    [self initView];
}

- (void)initData {
    
//   LiuXSlider *slider=[[LiuXSlider alloc] initWithFrame:CGRectMake(50, 50, 300, 50) titles:@[@"￥0",@"￥150",@"￥300",@"￥450",@"￥600",@"￥1000",@"不限"] firstAndLastTitles:@[@"0",@"不限"] defaultIndex:1 sliderImage:[UIImage imageNamed:@"日历"]];
//   [self.view addSubview:slider];
//    slider.block=^(int index){
//        NSLog(@"当前index==%d",index);
//    };
//
   // self.sizeData = @[@"￥0", @"M", @"L", @"XL", @"XXL",];
    self.colorData = @[@"不限", @"客栈公寓", @"快捷连锁", @"二星及以下",@"三星/舒适",@"四星/高档",@"五星/豪华"];
}

- (void)initView {
    UIButton *openButton = [[UIButton alloc] init];
    openButton.backgroundColor = [UIColor grayColor];
    [openButton setTitle:@"打开" forState:UIControlStateNormal];
    openButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [openButton addTarget:self action:@selector(didOpen) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:openButton];
    [openButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(100.0f, 44.0f));
    }];
}

- (void)didOpen {
    NSLog(@"打开");
    [self popShow];
}

#pragma mark - PopView
- (void)popShow {
    UIView *popBackView = [[UIView alloc] init];
    popBackView.frame = PopViewBounds;
    popBackView.backgroundColor = PopViewInitBackColor;
    [BaseWindow addSubview:popBackView];
    
    self.popContentView.frame = ContentViewInitFrame;
    [popBackView addSubview:self.popContentView];
    
    [UIView animateWithDuration:AniDuration animations:^{
        popBackView.backgroundColor = PopViewPopBackColor;
        self.popContentView.frame = ContentViewPopFrame;
    }];
    
  //  点击空白下落
//    [popBackView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popDismiss)]];
}

- (UIView *)popContentView {
    if(!_popContentView) {
        
        UIView *popContentView = [[UIView alloc] init];
        popContentView.backgroundColor = [UIColor whiteColor];
        _popContentView = popContentView;
        
        //
        UIView *infoView = [[UIView alloc] init];
        [popContentView addSubview:infoView];
        [infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(popContentView);
            make.width.mas_equalTo(popContentView);
            make.top.mas_equalTo(popContentView);
            make.height.mas_equalTo(InfoViewHt);
        }];
        
//        UIImageView *headerImageView = [[UIImageView alloc] init];
//        headerImageView.backgroundColor = [UIColor redColor];
//        headerImageView.layer.cornerRadius = 5*Ratio;
//        [infoView addSubview:headerImageView];
//        [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(infoView).offset(16*Ratio);
//            make.top.mas_equalTo(infoView).offset(-33.5*Ratio);
//            make.size.mas_equalTo(CGSizeMake(120*Ratio, 120*Ratio));
//        }];
    //    self.headerImageView = headerImageView;
//        
//        UILabel *priceLabel = [[UILabel alloc] init];
//        priceLabel.text = @"¥ 567.00";
//        priceLabel.font = [UIFont systemFontOfSize:16*Ratio weight:UIFontWeightMedium];
//        priceLabel.textColor = UIColorFromRGBA(0xFF4400);
//        [infoView addSubview:priceLabel];
//        [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(headerImageView.mas_right).offset(9.5*Ratio);
//            make.top.mas_equalTo(infoView).offset(25.5*Ratio);
//        }];
//        self.priceLabel = priceLabel;
        
        
        LiuXSlider *slider=[[LiuXSlider alloc] initWithFrame:CGRectMake(50, 50, 300, 50) titles:@[@"￥0",@"￥150",@"￥300",@"￥450",@"￥600",@"￥1000",@"不限"] firstAndLastTitles:@[@"0",@"不限"] defaultIndex:1 sliderImage:[UIImage imageNamed:@"日历"]];
        [infoView addSubview:slider];
        [slider mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(infoView).offset(16*Ratio);
                        make.top.mas_equalTo(infoView).offset(33.5*Ratio);
                        make.size.mas_equalTo(CGSizeMake(300*Ratio, 120*Ratio));
                    }];
//            self.SLider = slider;

            slider.block=^(int index){
            NSLog(@"当前index==%d",index);
        };

//
//        UILabel *goodsNoLabel = [[UILabel alloc] init];
//        goodsNoLabel.text = @"商品货号：G76978";
//        goodsNoLabel.textColor = UIColorFromRGBA(0x000000);
//        goodsNoLabel.font = [UIFont systemFontOfSize:14*Ratio weight:UIFontWeightLight];
//        [infoView addSubview:goodsNoLabel];
//        [goodsNoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(priceLabel);
//            make.top.mas_equalTo(infoView).offset(54*Ratio);
//        }];
//        self.goodsNoLabel = goodsNoLabel;
        
        UIButton *closeButton = [[UIButton alloc] init];
        closeButton.backgroundColor = [UIColor redColor];
        [closeButton addTarget:self action:@selector(popDismiss) forControlEvents:UIControlEventTouchUpInside];
        [infoView addSubview:closeButton];
        [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(infoView).offset(7.5*Ratio);
            make.right.mas_equalTo(infoView).offset(-7.5*Ratio);
            make.size.mas_equalTo(CGSizeMake(28.5*Ratio, 28.5*Ratio));
        }];
        
        UIView *infoBottomBorder = [[UIView alloc] init];
        infoBottomBorder.backgroundColor = UIColorFromRGBA(0xD8D8D8);
        [infoView addSubview:infoBottomBorder];
        [infoBottomBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(infoView);
            make.width.mas_equalTo(infoView);
            make.bottom.mas_equalTo(infoView);
            make.height.mas_equalTo(0.5);
        }];
        
        //
        UIButton *confirmButton = [[UIButton alloc] init];
        confirmButton.backgroundColor = UIColorFromRGBA(0xF40C53);
        [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [confirmButton setTitleColor:UIColorFromRGBA(0xFFFFFF) forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont systemFontOfSize:16*Ratio];
        [confirmButton addTarget:self action:@selector(popConfirm) forControlEvents:UIControlEventTouchUpInside];
        [popContentView addSubview:confirmButton];
        [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(popContentView);
            make.width.mas_equalTo(popContentView);
            make.bottom.mas_equalTo(popContentView);
            make.height.mas_equalTo(48*Ratio);
        }];
        
        //
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.contentSize = CGSizeMake(ScreenWd, InfoViewHt+SizeViewHt+ColorViewHt+NumViewHt);
        [popContentView addSubview:scrollView];
        [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(popContentView);
            make.width.mas_equalTo(popContentView);
            make.top.mas_equalTo(infoView.mas_bottom);
            make.bottom.mas_equalTo(confirmButton.mas_top);
        }];
        
        //
        UIView *sizeView = [[UIView alloc] init];
        [scrollView addSubview:sizeView];
        [sizeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(scrollView);
            make.width.mas_equalTo(scrollView);
            make.top.mas_equalTo(scrollView);
            make.height.mas_equalTo(SizeViewHt);
        }];
        
        UICollectionViewFlowLayout *sizeLayout = [[UICollectionViewFlowLayout alloc] init];
        sizeLayout.itemSize = CGSizeMake(SizeItemWd, SizeItemHt);
        sizeLayout.minimumLineSpacing = SizeLineSpacing;
        sizeLayout.minimumInteritemSpacing = 12*Ratio;
        sizeLayout.headerReferenceSize = CGSizeMake(308*Ratio, SizeHeaderHt);
        
        UICollectionView *sizeCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:sizeLayout];
        sizeCollectionView.dataSource = self;
        sizeCollectionView.delegate = self;
        sizeCollectionView.backgroundColor = [UIColor clearColor];
        [sizeCollectionView registerClass:UICollectionItemViewCell.class forCellWithReuseIdentifier:@"UICollectionViewCellSize"];
        [sizeCollectionView registerClass:UICollectionReusableView.class
               forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UICollectionReusableViewHeader"];
        [sizeView addSubview:sizeCollectionView];
        [sizeCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(sizeView);
            make.bottom.mas_equalTo(sizeView);
            make.left.mas_equalTo(sizeView).offset(16*Ratio);
            make.width.mas_equalTo(308*Ratio);
        }];
        self.sizeCollectionView = sizeCollectionView;
        
        UIView *sizeBottomBorder = [[UIView alloc] init];
        sizeBottomBorder.backgroundColor = UIColorFromRGBA(0xD8D8D8);
        [sizeView addSubview:sizeBottomBorder];
        [sizeBottomBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(sizeView).offset(16*Ratio);
            make.right.mas_equalTo(sizeView).offset(-16*Ratio);
            make.bottom.mas_equalTo(sizeView);
            make.height.mas_equalTo(0.5);
        }];
        
        //
        UIView *colorView = [[UIView alloc] init];
        [scrollView addSubview:colorView];
        [colorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(scrollView);
            make.width.mas_equalTo(scrollView);
            make.top.mas_equalTo(sizeView.mas_bottom);
            make.height.mas_equalTo(ColorViewHt);
        }];
        
        UICollectionViewFlowLayout *colorLayout = [[UICollectionViewFlowLayout alloc] init];
        colorLayout.itemSize = CGSizeMake(ColorItemWd, ColorItemHt);
        colorLayout.minimumLineSpacing = ColorLineSpacing;
        colorLayout.minimumInteritemSpacing = 12*Ratio;
        colorLayout.headerReferenceSize = CGSizeMake(308*Ratio, ColorHeaderHt);
        
        UICollectionView *colorCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:colorLayout];
        colorCollectionView.dataSource = self;
        colorCollectionView.delegate = self;
        colorCollectionView.backgroundColor = [UIColor clearColor];
        [colorCollectionView registerClass:UICollectionItemViewCell.class forCellWithReuseIdentifier:@"UICollectionViewCellColor"];
        [colorCollectionView registerClass:UICollectionReusableView.class
                forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UICollectionReusableViewHeader"];
        [colorView addSubview:colorCollectionView];
        [colorCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(colorView);
            make.bottom.mas_equalTo(colorView);
            make.left.mas_equalTo(colorView).offset(16*Ratio);
            make.width.mas_equalTo(308*Ratio);
        }];
        self.colorCollectionView = colorCollectionView;
        
        UIView *colorBottomBorder = [[UIView alloc] init];
        colorBottomBorder.backgroundColor = UIColorFromRGBA(0xD8D8D8);
        [colorView addSubview:colorBottomBorder];
        [colorBottomBorder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(colorView).offset(16*Ratio);
            make.right.mas_equalTo(colorView).offset(-16*Ratio);
            make.bottom.mas_equalTo(colorView);
            make.height.mas_equalTo(0.5);
        }];
        
        //
        UIView *numView = [[UIView alloc] init];
        [scrollView addSubview:numView];
        [numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(scrollView);
            make.width.mas_equalTo(scrollView);
            make.top.mas_equalTo(colorView.mas_bottom);
            make.height.mas_equalTo(NumViewHt);
        }];
        
        
        
     }
    return _popContentView;
}

- (void)popDismiss {
    if (self.popContentView) {
        UIView *popBackView = self.popContentView.superview;
        if(popBackView) {
            [UIView animateWithDuration:AniDuration animations:^{
                popBackView.backgroundColor = PopViewInitBackColor;
                self.popContentView.frame = ContentViewInitFrame;
            } completion:^(BOOL finished) {
                [self.popContentView removeFromSuperview];
                [popBackView removeFromSuperview];
            }];
        }
    }
}

- (CGFloat)popSizeViewHt {
    NSInteger lines = self.sizeData.count/4+(self.sizeData.count%4==0? 0: 1);
    return lines*SizeItemHt+SizeHeaderHt+SizeFooterHt+MAX(0, lines-1)*SizeLineSpacing;
}

- (CGFloat)popColorViewHt {
    NSInteger lines = self.colorData.count/4+(self.colorData.count%4==0? 0: 1);
    return lines*ColorItemHt+ColorHeaderHt+ColorFooterHt+MAX(0, lines-1)*ColorLineSpacing;
}

- (void)popConfirm {
    [self popDismiss];
    NSLog(@"点击确定");
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger num = 0;
    if(collectionView == self.sizeCollectionView) {
        num = self.sizeData.count;
    } else if(collectionView == self.colorCollectionView) {
        num = self.colorData.count;
    }
    return num;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *rCell = nil;
    if(collectionView == self.sizeCollectionView) {
        
        UICollectionItemViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UICollectionViewCellSize" forIndexPath:indexPath];
        cell.textLabel.text = self.sizeData[indexPath.row];
        rCell = cell;
    } else if(collectionView == self.colorCollectionView) {
        
        UICollectionItemViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UICollectionViewCellColor" forIndexPath:indexPath];
        cell.textLabel.text = self.colorData[indexPath.row];
        rCell = cell;
    }
    return rCell;
}

#pragma mark - UICollectionViewDelegate
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *rCell = nil;
    if(kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *headerCell = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UICollectionReusableViewHeader" forIndexPath:indexPath];
        UILabel *titleLabel = [headerCell viewWithTag:1111];
        if(!titleLabel) {
            titleLabel = [[UILabel alloc] init];
            titleLabel.textColor = UIColorFromRGBA(0x000000);
            titleLabel.font = [UIFont systemFontOfSize:14*Ratio];
            [headerCell addSubview:titleLabel];
            [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(headerCell);
                make.top.mas_equalTo(headerCell).offset(25*Ratio);
            }];
        }
        
        if(collectionView == self.sizeCollectionView) {
            titleLabel.text = @"价格筛选";
        } else if(collectionView == self.colorCollectionView) {
            titleLabel.text = @"星级筛选";
        }
        
        rCell = headerCell;
    }
    return rCell;
}
@end
