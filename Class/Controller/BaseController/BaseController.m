//
//  BaseController.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "BaseController.h"
#import "TTTTView.h"
#import "XHPopMenu.h"
//www.youxia.cn/mgo/index.php?mod=index&code=top_nav_iso
@interface BaseController ()<UINavigationControllerDelegate>
//@property (nonatomic, strong) XHPopMenu *popMenu;
@end

@implementation BaseController{
    NSArray *dataArr;
    NSURLRequest *phoneRequest;
    UIScrollView *fillTable;
    CGRect origFrame;
    UIView *bgView;
    BOOL hidden;
}

-(instancetype)init{
    self=[super init];
    if (self) {
        _app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return self;
}

+ (instancetype)shareInstance {
    static BaseController * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BaseController alloc] init];
    });
    return instance;
}

//
-(void)setGlobalNavigationView:(BOOL)isHadLogo{
    //
    if (isHadLogo) {
        UIButton *btn=[self setCustomLeftBarButtonItemSetFrame:CGRectMake(0, 0, IS_IPHONE5?88:101, IS_IPHONE5?34:36) image:[UIImage imageNamed:@"logo"]];
        [btn addTarget:self action:@selector(clickLogo) forControlEvents:UIControlEventTouchUpInside];
    }else{
        _navReturnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _navReturnBtn.frame=CGRectMake(-6, 6,52.f,44.f);
        [_navReturnBtn setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
        _navReturnBtn.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
        self.navReturnBtn=_navReturnBtn;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:_navReturnBtn];
        self.navigationItem.leftBarButtonItem = backItem;
    }
    
    
    //
    UIView *titleiew=[[UIView alloc]initWithFrame:CGRectMake(10, 0, SCREENSIZE.width-142, 31)];
    titleiew.backgroundColor=[UIColor whiteColor];
    titleiew.layer.cornerRadius=2.6f;
    self.navigationItem.titleView=titleiew;
    UITapGestureRecognizer *titTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickSearch)];
    titleiew.userInteractionEnabled=YES;
    [titleiew addGestureRecognizer:titTap];
    
    //searchTf
    UITextField *searchTf=[UITextField new];
    searchTf.enabled=NO;
    searchTf.placeholder=@"请输入关键字";
    searchTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTf.textColor=[UIColor blackColor];
    searchTf.font=kFontSize15;
    [titleiew addSubview:searchTf];
    searchTf.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* searchTf_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-4-[searchTf]-33-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searchTf)];
    [NSLayoutConstraint activateConstraints:searchTf_h];
    NSArray* searchTf_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchTf]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searchTf)];
    [NSLayoutConstraint activateConstraints:searchTf_w];
    
    //
    UIButton *imgBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [imgBtn setImage:[UIImage imageNamed:@"nav_magnifier"] forState:UIControlStateNormal];
    [imgBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [titleiew addSubview:imgBtn];
    imgBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* imgBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[searchTf]-2-[imgBtn]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searchTf,imgBtn)];
    [NSLayoutConstraint activateConstraints:imgBtn_h];
    NSArray* imgBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgBtn]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgBtn)];
    [imgBtn addTarget:self action:@selector(clickSearch) forControlEvents:UIControlEventTouchDown];
    [NSLayoutConstraint activateConstraints:imgBtn_w];
}

#pragma mark - button event
-(void)clickSearch{
//    SearchViewController *searCtr=[[SearchViewController alloc]init];
//    searCtr.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:searCtr animated:YES];
}


#pragma mark - Propertys

//- (XHPopMenu *)popMenu {
//    if (!_popMenu) {
//        NSMutableArray *popMenuItems = [NSMutableArray array];
//        for (int i = 0; i < 9; i ++) {
//            NSString *imageName;
//            NSString *title;
//            switch (i) {
//                case 0: {
//                    imageName = @"contacts_add_newmessage";
//                    title = @"首页";
//                    break;
//                }
//                case 1: {
//                    imageName = @"contacts_add_friend";
//                    title = @"马尔代夫";
//                    break;
//                }
//                case 2: {
//                    imageName = @"contacts_add_scan";
//                    title = @"巴厘岛";
//                    break;
//                }
//                case 3: {
//                    imageName = @"contacts_add_photo";
//                    title = @"帕劳";
//                    break;
//                }
//                case 4: {
//                    imageName = @"contacts_add_voip";
//                    title = @"精选热卖";
//                    break;
//                }
//                case 5: {
//                    imageName = @"contacts_add_voip";
//                    title = @"视频蜜月";
//                    break;
//                }
//                case 6: {
//                    imageName = @"contacts_add_voip";
//                    title = @"游侠分期";
//                    break;
//                }
//                case 7: {
//                    imageName = @"contacts_add_voip";
//                    title = @"旅游攻略";
//                    break;
//                }
//                case 8: {
//                    imageName = @"contacts_add_voip";
//                    title = @"游侠秀";
//                    break;
//                }
//                default:
//                    break;
//            }
//            XHPopMenuItem *popMenuItem = [[XHPopMenuItem alloc] initWithImage:[UIImage imageNamed:imageName] title:title];
//            [popMenuItems addObject:popMenuItem];
//        }
//        
//        _popMenu = [[XHPopMenu alloc] initWithMenus:popMenuItems];
//        _popMenu.popMenuDidSlectedCompled = ^(NSInteger index, XHPopMenuItem *popMenuItems) {
//        };
//    }
//    return _popMenu;
//}


-(void)setNavigationView_TitleView:(NSString *)title isHaveTurnBackBtn:(BOOL)isHadReturnBtn showFillterBtn:(BOOL)isShowFillterBtn{
    
    TTTTView *titleView=[[TTTTView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 44)];
    titleView.tag=100;
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[TTTTView class]]) {
            [view removeFromSuperview];
        }
    }
    [self.view addSubview:titleView];
    
    UILabel *titLabel=[[UILabel alloc]initWithFrame:CGRectMake(50, 0, SCREENSIZE.width-100, titleView.height)];
    titLabel.tag=101;
    titLabel.font=kFontSize16;
    titLabel.textAlignment=NSTextAlignmentCenter;
    titLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    titLabel.text=title;
    [titleView addSubview:titLabel];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, titleView.height-0.5f, titleView.width, 0.5f)];
    line.tag=102;
    line.backgroundColor=[UIColor lightGrayColor];
    [titleView addSubview:line];
    
    if (isHadReturnBtn) {
        //return按钮
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.tag=103;
        back.frame=CGRectMake(6, 5,titleView.height+14,titleView.height-10);
        [back setImageEdgeInsets:UIEdgeInsetsMake(0, -50, 0, -80)];
        [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback02"] forState:UIControlStateNormal];
        [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:back];
    }
    
    if (isShowFillterBtn) {
        //return按钮
        UIButton *fillterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        fillterBtn.tag=104;
        fillterBtn.frame=CGRectMake(titleView.width-40, 9,26,26);
        [fillterBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -50, 0, -80)];
        [fillterBtn setBackgroundImage:[UIImage imageNamed:@"nav_hidden"] forState:UIControlStateNormal];
        [fillterBtn addTarget:self action:@selector(seleFillterBtn) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:fillterBtn];
        
        dataArr=@[@"首页",@"马尔代夫",@"巴厘岛",@"帕劳",@"精选热卖",@"视频蜜月",@"游侠分期",@"旅游攻略",@"游侠秀"];
        
        bgView=[[UIView alloc]initWithFrame:self.view.bounds];
        bgView.userInteractionEnabled=YES;
        [bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBgView)]];
        
        //
        origFrame=CGRectMake(SCREENSIZE.width-110, 107, 108, 0);
        fillTable=[[UIScrollView alloc]initWithFrame:origFrame];
        fillTable.alpha=0.90f;
        fillTable.backgroundColor=[UIColor whiteColor];
        fillTable.layer.cornerRadius=1.5f;
        fillTable.alwaysBounceVertical=YES;
        [self.navigationController.view addSubview:fillTable];
        
        
        CGFloat btnHeight=40;
        CGFloat btnWidth=108;
        for (int i=0; i<dataArr.count; i++) {
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(0, i*btnHeight, btnWidth, btnHeight);
            btn.tag=i;
            [btn setTitle:dataArr[i] forState:UIControlStateNormal];
            btn.titleLabel.font=kFontSize15;
            [btn setTitleColor:[UIColor colorWithHexString:@"#282828"] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(clickFillterBtn:) forControlEvents:UIControlEventTouchUpInside];
            [fillTable addSubview:btn];
            
            UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, btn.height-0.5f, btn.width, 0.5f)];
            line.backgroundColor=[UIColor lightGrayColor];
            [btn addSubview:line];
        }
    }
}

-(void)clickFillterBtn:(id)sender{
    
    
    YouXiaStageController *yxStage=[[YouXiaStageController alloc]init];
    
    if (hidden) {
        [UIView animateWithDuration:0.12
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fillTable.frame=origFrame;
                         } completion:^(BOOL completed) {
                             [bgView removeFromSuperview];
                         }];
    }
    hidden=!hidden;
    UIButton *btn=sender;
    NSInteger seleTag=btn.tag;
    if (seleTag==0) {
        self.tabBarController.selectedIndex=0;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (seleTag==1){
        
    }else if (seleTag==2){
        
    }else if (seleTag==3){
        
    }else if (seleTag==4){
        
    }else if (seleTag==5){
        
    }else if (seleTag==6){
        if (![self isKindOfClass:[YouXiaStageController class]]) {
            [self.navigationController pushViewController:yxStage animated:YES];
        }
    }else if (seleTag==7){
        
    }else if (seleTag==8){
        
    }
}

#pragma mark - 点击透明背景hidden
- (void)hideBgView{
    if (hidden) {
        [UIView animateWithDuration:0.12
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fillTable.frame=origFrame;
                         } completion:^(BOOL completed) {
                             [bgView removeFromSuperview];
                         }];
    }
    hidden=!hidden;
}

-(void)seleFillterBtn{
    
    if(hidden){
        [UIView animateWithDuration:0.12
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fillTable.frame=origFrame;
                         } completion:^(BOOL completed) {
                             
                             [bgView removeFromSuperview];
                         }];
    }else{
        [UIView animateWithDuration:0.22
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fillTable.frame=CGRectMake(SCREENSIZE.width-110, 107, 108, 360);
                         } completion:^(BOOL completed) {
                             [self.navigationController.view addSubview:bgView];
                             [self.navigationController.view bringSubviewToFront:fillTable];
                         }];
    }
    
    hidden=!hidden;
}

#pragma mark -
-(void)setCustomRootViewNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = kFontSize18;
    navTitle.textColor = [UIColor blackColor];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
}

-(void)setCustomNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = kFontSize18;
    navTitle.textColor = [UIColor whiteColor];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,52.f,44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)turnBack{
    [AppUtils closeKeyboard];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view{
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(0, 2, 42,42);
    [back setImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back setImageEdgeInsets:UIEdgeInsetsMake(0, -22, 0, 0)];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
    self.navigationItem.titleView = view;
}

-(void)setCustomNavigationSearhcViewFrame:(CGRect)frame searhcView:(UIView *)view{
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(0, 2, 42,42);
    [back setImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back setImageEdgeInsets:UIEdgeInsetsMake(0, -22, 0, 0)];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
    self.navigationItem.titleView = view;
    
    UIView *titleiew=[[UIView alloc]initWithFrame:frame];
    titleiew.backgroundColor=[UIColor whiteColor];
    titleiew.layer.cornerRadius=5;
    self.navigationItem.titleView=titleiew;
}

-(void)setCustomNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view{
    UIView *titleiew=[[UIView alloc]initWithFrame:frame];
    titleiew.backgroundColor=[UIColor whiteColor];
    titleiew.layer.cornerRadius=5;
    self.navigationItem.titleView=titleiew;
}
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    [btn setTitle:text forState:UIControlStateNormal];
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = btnItem;
    return btn;
}
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image{
    UIButton *imgBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    imgBtn.frame=frame;
    imgBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    imgBtn.imageEdgeInsets=UIEdgeInsetsMake(3, -16, 3, 0);
    [imgBtn setImage:image forState:UIControlStateNormal];
    UIBarButtonItem *addLItem = [[UIBarButtonItem alloc] initWithCustomView:imgBtn];
    self.navigationItem.leftBarButtonItem = addLItem;
    return imgBtn;
}

-(void)clickLogo{
    self.tabBarController.selectedIndex=0;
    [self.navigationController popToRootViewControllerAnimated:YES];

}

-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    btn.titleLabel.font=kFontSize16;
    [btn setTitle:text forState:UIControlStateNormal];
    btn.titleLabel.textAlignment=NSTextAlignmentRight;
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = btnItem;
    return btn;
}
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image{
    UIButton *imgBtn=[[UIButton alloc]initWithFrame:frame];
    [imgBtn setImage:image forState:UIControlStateNormal];
    imgBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [imgBtn setImageEdgeInsets:UIEdgeInsetsMake(2, 0, 2, -10)];
    UIBarButtonItem *addRItem = [[UIBarButtonItem alloc] initWithCustomView:imgBtn];
    self.navigationItem.rightBarButtonItem = addRItem;
    return imgBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    self.view.backgroundColor=[UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    //
    [super viewWillAppear:YES];
    
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    
}

//在键盘上附加工具栏
-(UIView *)inputAccessoryView{
    CGRect accessFrame = CGRectMake(0, 0, SCREENSIZE.width, 44);
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:accessFrame];
    UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-50, 2, 40, 40)];
    [closeBtn setTitle:@"完成" forState:UIControlStateNormal];
    [closeBtn setTitleColor:[UIColor colorWithHexString:YxColor_Blue] forState:UIControlStateNormal];
    closeBtn.titleLabel.font=[UIFont boldSystemFontOfSize:16];
    [closeBtn addTarget:self action:@selector(closeBeyBoard) forControlEvents:UIControlEventTouchDown];
    [toolbar addSubview:closeBtn];
    return toolbar;
}
-(void)closeBeyBoard{
    [AppUtils closeKeyboard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}
- (void)clearWebViewUrlCache {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitOfflineWebApplicationCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - navigation delegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

#pragma mark - TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YouXiaStageController *yxStage=[[YouXiaStageController alloc]init];
    
    
    if (hidden) {
        [UIView animateWithDuration:0.12
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fillTable.frame=origFrame;
                         } completion:^(BOOL completed) {
                             [bgView removeFromSuperview];
                         }];
    }
    hidden=!hidden;
    NSInteger seleTag=indexPath.row;
    if (seleTag==0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.tabBarController.selectedIndex=0;
    }else if (seleTag==1){
        
    }else if (seleTag==2){
        
    }else if (seleTag==3){
        
    }else if (seleTag==4){
        
    }else if (seleTag==5){
        
    }else if (seleTag==6){
        [self.navigationController pushViewController:yxStage animated:YES];
    }else if (seleTag==7){
        
    }else if (seleTag==8){
        
    }
}

@end
