//
//  BaseNavigationBar.m
//  youxia
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BaseNavigationBar.h"

@implementation BaseNavigationBar{
    UIButton *myTitleLabel;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self setShadowImage:[UIImage new]];
        UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
        [self lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
        
        //返回按钮
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.frame=CGRectMake(16, 20, 56.f, 44.f);
        [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
        back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:back];
        
        myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
        myTitleLabel.backgroundColor=[UIColor clearColor];
        [self addSubview:myTitleLabel];
    }
    return self;
}


-(void)setTitleStr:(NSString *)titleStr{
    [myTitleLabel setTitle:titleStr forState:UIControlStateNormal];
}


-(void)turnBack{
    [[self viewController].navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end
