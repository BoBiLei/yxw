//
//  FilmNavigationBar.m
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmNavigationBar.h"

@implementation FilmNavigationBar{
    UIButton *myTitleLabel;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        UIColor * color = [[UIColor colorWithHexString:@"ffffff"] colorWithAlphaComponent:1];
        [self lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
        
        //返回按钮
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.frame=CGRectMake(16, 20, 64, 44);
        [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
        back.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:back];
        
        myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
        myTitleLabel.backgroundColor=[UIColor clearColor];
        [myTitleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
        [self addSubview:myTitleLabel];
        
        self.shadowImage = [[UIImage alloc]init];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-0.5f, SCREENSIZE.width, 0.5f)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [self addSubview:line];
    }
    return self;
}


-(void)setTitleStr:(NSString *)titleStr{
    [myTitleLabel setTitle:titleStr forState:UIControlStateNormal];
}


-(void)turnBack{
    [[self viewController].navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end
