//
//  FilmNavigationBar.h
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmNavigationBar : UINavigationBar

@property (nonatomic, strong) NSString *titleStr;

@end
