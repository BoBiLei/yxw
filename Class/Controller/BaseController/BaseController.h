//
//  BaseController.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

//海岛主题类型
typedef NS_ENUM(NSUInteger, ThematicType) {
    MaldivesType,
    BaliType,
    PalauType
};

@interface BaseController : UIViewController

@property (nonatomic,strong) AppDelegate *app;

@property (nonatomic,strong) UIButton *navReturnBtn;

@property (nonatomic,assign) BOOL isHadTitle;

//类方法
+ (instancetype)shareInstance;

/**带有logo、搜索、电话的导航栏*/
-(void)setGlobalNavigationView:(BOOL)isHadLogo;

/**导航栏下面的Title View*/
-(void)setNavigationView_TitleView:(NSString *)title isHaveTurnBackBtn:(BOOL)isHadReturnBtn showFillterBtn:(BOOL)isShowFillterBtn;

-(void)setCustomRootViewNavigationTitle:(NSString *)title;
-(void)setCustomNavigationTitle:(NSString *)title;
-(void)setNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view;

-(void)setCustomNavigationTitleViewSetFrame:(CGRect)frame titleView:(UIView *)view;
-(void)setCustomNavigationSearhcViewFrame:(CGRect)frame searhcView:(UIView *)view;
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text;
-(UIButton *)setCustomLeftBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image;
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame Text:(NSString *)text;
-(UIButton *)setCustomRightBarButtonItemSetFrame:(CGRect)frame image:(UIImage *)image;

- (void)turnBack;

//清理webView内存泄漏
- (void)clearWebViewUrlCache;

@end
