//
//  BaseNavigationBar.h
//  youxia
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationBar : UINavigationBar

@property (nonatomic, strong) NSString *titleStr;

@end
