//
//  HoteldetailsModel.m
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteldetailsModel.h"

@implementation HoteldetailsModel
-(void)jsonDataForDictioanry:(NSDictionary *)model
{
    self.hoteID=model[@"HotelID"];
    self.ruzhuDate=model[@"CheckInDate"];
    self.endDate=model[@"CheckOutDate"];
    self.crnum=model[@"adult"];
    self.etnum=model[@"child"];
    self.etage=model[@"childAge"];
    
    
    self.bedtype=model[@"BedType"];
    self.breakfastype=model[@"BreakfastType"];
    self.currency=model[@"Currency"];
    self.kucun=model[@"InventoryCount"];
    self.maxqccpancy=model[@"MaxOccupancy"];
    self.rateplanid=model[@"RatePlanID"];
    self.rateplaname=model[@"RatePlanName"];
    self.pricelist=model[@"PriceList"];
    self.roomstatus=model[@"RoomStatus"];
    self.tprice=model[@"TotalPrice"];
    self.name_cn=model[@"Name_CN"];
    self.roomimg=model[@"RoomImg"];

}
-(void)setModel:(NSDictionary *)model
{
//    self.hoteID=model[@"HotelID"];
//    self.ruzhuDate=model[@"CheckInDate"];
//    self.endDate=model[@"CheckOutDate"];
//    self.crnum=model[@"adult"];
//    self.etnum=model[@"child"];
//    self.etage=model[@"childAge"];
    
    if ([model[@"BedType"] isKindOfClass:[NSNull class]]) {
        self.bedtype=@"";
    }else{
        self.bedtype=[NSString stringWithFormat:@"%@",model[@"BedType"]];
    }
    if ([model[@"BreakfastType"] isKindOfClass:[NSNull class]]) {
        self.bedtype=@"";
    }else{
        self.breakfastype=model[@"BreakfastType"];
    }
    if ([model[@"Currency"] isKindOfClass:[NSNull class]]) {
        self.currency=@"";
    }else{
        self.currency=model[@"Currency"];
    }
    if ([model[@"InventoryCount"] isKindOfClass:[NSNull class]]) {
        self.kucun=@"";
    }else{
        self.kucun=model[@"InventoryCount"];
    }
    if ([model[@"MaxOccupancy"] isKindOfClass:[NSNull class]]) {
        self.maxqccpancy=@"";
    }else{
        self.maxqccpancy=model[@"MaxOccupancy"];
    }
    if ([model[@"RatePlanID"] isKindOfClass:[NSNull class]]) {
        self.rateplanid=@"";
    }else{
        self.rateplanid=model[@"RatePlanID"];
    }
    
    
    if ([model[@"RatePlanName"] isKindOfClass:[NSNull class]]) {
        self.rateplaname=@"";
    }else{
        self.rateplaname=model[@"RatePlanName"];
    }
    
    if ([model[@"PriceList"] isKindOfClass:[NSNull class]]) {
        self.pricelist=@[];
    }else{
        self.pricelist=model[@"PriceList"];
    }
    
    if ([model[@"RoomStatus"] isKindOfClass:[NSNull class]]) {
        self.roomstatus=@"";
    }else{
        self.roomstatus=model[@"RoomStatus"];
    }
    
    if ([model[@"TotalPrice"] isKindOfClass:[NSNull class]]) {
        self.tprice=@"";
    }else{
        self.tprice=model[@"TotalPrice"];
    }
    
    if ([model[@"Name_CN"] isKindOfClass:[NSNull class]]) {
        self.name_cn=@"";
    }else{
        self.name_cn=model[@"Name_CN"];
    }
    if ([model[@"RoomImg"] isKindOfClass:[NSNull class]]) {
        self.roomimg=@[];
    }else{
    self.roomimg=model[@"RoomImg"];
    }
}
@end
