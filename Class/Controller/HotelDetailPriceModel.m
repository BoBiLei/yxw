//
//  HotelDetailPriceModel.m
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelDetailPriceModel.h"

@implementation HotelDetailPriceModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"CancelPolicy"] isKindOfClass:[NSNull class]]) {
        self.CancelPolicy=@[];
    }else{
        self.CancelPolicy=dic[@"CancelPolicy"];
    }
    
    //
    if ([dic[@"Currency"] isKindOfClass:[NSNull class]]) {
        self.Currency=@"";
    }else{
        self.Currency=[NSString stringWithFormat:@"%@",dic[@"Currency"]];
    }
    
    //
    if ([dic[@"InventoryCount"] isKindOfClass:[NSNull class]]) {
        self.InventoryCount=@"";
    }else{
        self.InventoryCount=[NSString stringWithFormat:@"%@",dic[@"InventoryCount"]];
    }
    
    //
    if ([dic[@"MaxOccupancy"] isKindOfClass:[NSNull class]]) {
        self.MaxOccupancy=@"";
    }else{
        self.MaxOccupancy=[NSString stringWithFormat:@"%@",dic[@"MaxOccupancy"]];
    }
    
    //
    if ([dic[@"MaxChild"] isKindOfClass:[NSNull class]]) {
        self.MaxChild=@"0";
    }else{
        self.MaxChild=[NSString stringWithFormat:@"%@",dic[@"MaxChild"]];
    }
    
    //
    if ([dic[@"PriceList"] isKindOfClass:[NSNull class]]) {
        self.PriceList=@[];
    }else{
        self.PriceList=dic[@"PriceList"];
    }
    
    //
    if ([dic[@"RatePlanID"] isKindOfClass:[NSNull class]]) {
        self.RatePlanID=@"";
    }else{
        self.RatePlanID=[NSString stringWithFormat:@"%@",dic[@"RatePlanID"]];
    }
    
    //
    if ([dic[@"RatePlanName"] isKindOfClass:[NSNull class]]) {
        self.RatePlanName=@"";
    }else{
        self.RatePlanName=[NSString stringWithFormat:@"%@",dic[@"RatePlanName"]];
    }
    
    //
    if ([dic[@"RoomImg"] isKindOfClass:[NSNull class]]) {
        self.RoomImg=@[];
    }else{
        self.RoomImg=dic[@"RoomImg"];
    }
    
    //
    if ([dic[@"RoomStatus"] isKindOfClass:[NSNull class]]) {
        self.RoomStatus=@"";
    }else{
        self.RoomStatus=[NSString stringWithFormat:@"%@",dic[@"RoomStatus"]];
    }
    
    //
    if ([dic[@"TotalPrice"] isKindOfClass:[NSNull class]]) {
        self.TotalPrice=@"";
    }else{
        self.TotalPrice=[NSString stringWithFormat:@"%@",dic[@"TotalPrice"]];
    }
}

@end
