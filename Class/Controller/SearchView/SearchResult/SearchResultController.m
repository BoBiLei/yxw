//
//  SearchResultController.m
//  youxia
//
//  Created by mac on 16/5/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchResultController.h"
#import "SearchHistoryFmdb.h"
#import <TTTAttributedLabel.h>
#import "NewIslandOnlineCell.h"

#define  NiocellHeight SCREENSIZE.width/3-12
@interface SearchResultController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SearchResultController{
    NSInteger page;
    MJRefreshAutoNormalFooter *footer;
    
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"搜索结果页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"搜索结果页"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    //插入历史记录，并发送通知
    SearchHistoryFmdb *db=[SearchHistoryFmdb shareFmdb];
    if (self.isUpdateHist) {
        [db insertSearchHistoryDate:self.keyWord];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"insertschkeyword" object:nil];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpTableView];
    
    [self requestForKeyWord:self.keyWord andPage:page];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width;
    TTTAttributedLabel *title=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(sViewX, 20, SCREENSIZE.width-2*sViewX, 44)];
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor whiteColor];
    title.font=kFontSize16;
    title.numberOfLines=0;
    [self.view addSubview:title];
    NSString *stageStr=[NSString stringWithFormat:@"%@\n搜索结果",self.keyWord];
    [title setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"搜索结果" options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize11;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init TableView
-(void)setUpTableView{
    
    page=1;  //默认第一页
    
    dataArr=[NSMutableArray array];
    
    //
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    myTable.hidden=YES;
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = kFontSize13;
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"没有更多数据" forState:MJRefreshStateNoMoreData];
    myTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (page!=1) {
        [self requestForKeyWord:self.keyWord andPage:page];
    }
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return NiocellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[NewIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    DistinceShowModel *model=dataArr[indexPath.row];
    cell.model=model;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DistinceShowModel *model=dataArr[indexPath.row];
    
    IslandDetailController *islDetail=[[IslandDetailController alloc] init];
    if ([model.islandYwName isEqualToString:@"帕劳"]) {
        islDetail.detail_landType=PalauIslandType;
    }else{
        islDetail.detail_landType=MaldivesIslandType;
    }
    islDetail.islandName=model.islandTitle;
    islDetail.islandId=model.islandId;
    islDetail.islandImg=model.islandImg;
    islDetail.islandPId=model.pId;
    islDetail.mdId=model.mdId;
    islDetail.islandDescript=model.islandTitle;
    [self.navigationController pushViewController:islDetail animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 搜索请求
-(void)requestForKeyWord:(NSString *)keyWord andPage:(NSInteger)cpage{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"typeid"];
    [dict setObject:keyWord forKey:@"keword"];
    [dict setObject:@"wap" forKey:@"m"];
    [dict setObject:@"index" forKey:@"c"];
    [dict setObject:@"search" forKey:@"a"];
    [dict setObject:@"1" forKey:@"is_v6"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)cpage] forKey:@"page"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            myTable.hidden=NO;
            //
            NSArray *arr=responseObject[@"retData"][@"list"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                model.islandYwName=dic[@"dy"];
                [dataArr addObject:model];
            }
            
            if (dataArr.count!=0) {
                page++;
            }else{
                [footer setState:MJRefreshStateNoMoreData];
            }
            [footer endRefreshing];
            [myTable reloadData];
            
            
        }else{
            [footer endRefreshingWithNoMoreData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
