//
//  SearchResultController.h
//  youxia
//
//  Created by mac on 16/5/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultController : UIViewController

@property (nonatomic, assign) BOOL isUpdateHist;
@property (nonatomic, copy) NSString *keyWord;

@end
