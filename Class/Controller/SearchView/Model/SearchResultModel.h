//
//  SearchResultModel.h
//  youxia
//
//  Created by mac on 16/1/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResultModel : NSObject

@property (nonatomic,copy) NSString *catId;
@property (nonatomic,copy) NSString *pId;
@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,copy) NSString *imgStr;
@property (nonatomic,copy) NSString *islandName;
@property (nonatomic,copy) NSString *islandDescript;
@property (nonatomic,copy) NSString *en_Name;
@property (nonatomic,copy) NSString *starLevel;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *stagePrice;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
