//
//  SearchResultModel.m
//  youxia
//
//  Created by mac on 16/1/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchResultModel.h"

@implementation SearchResultModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.catId=dic[@"catid"];
    self.pId=dic[@"pid"];
    self.islandId=dic[@"id"];
    self.imgStr=dic[@"thumb"];
    self.islandName=dic[@"title"];
    self.islandDescript=dic[@"description"];
    self.en_Name=dic[@"daoyu_ennm"];
    self.starLevel=dic[@"xingji"];
    self.price=dic[@"p_price"];
    self.stagePrice=dic[@"p_f_price"];
}

@end
