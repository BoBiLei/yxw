//
//  SearchViewController.m
//  youxia
//
//  Created by mac on 16/1/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultCell.h"
#import "SearchHistoryFmdb.h"
#import "SearchReusableReusableView.h"
#import "SearchItem01Cell.h"
#import "SearchResultController.h"

#define X_Space 12
#define Btn_Height 30
#define HotSearView_Height 28

@interface SearchViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,SearchReusableDelegate>

@end

@implementation SearchViewController{
    UILabel *tipLabel;
    
    UITextField *searchTf;
    
    //
    NSMutableArray *hisArr;             //历史记录Arr
    NSArray *tjArr;                     //热门推荐Arr
    NSMutableArray *distArr;            //全部热门目的地
    NSMutableArray *distShowArr;        //要显示的目的地
    
    //
    UICollectionView *historyView;
    UIButton *seletedKeyWordBtn;
    NSString *seletedKeyWordStr;
    
    //
    SearchReusableReusableView *myRsbView;
    
    //
    NSMutableArray *dataArr;
    UITableView *resultTable;
    
    //
    BOOL _wasKeyboardManagerEnabled;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"搜索页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    [[IQKeyboardManager sharedManager] setEnable:_wasKeyboardManagerEnabled];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"搜索页"];
}

#pragma mark - viewDidAppear
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用滑动返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    _wasKeyboardManagerEnabled = [[IQKeyboardManager sharedManager] isEnabled];
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

-(instancetype)init{
    self=[super init];
    if (self) {
        [self setUpCustomSearBar];      //自定义NavigationBar
        
        [self setUpHistoryView];        //热门搜索、搜索历史
    }
    return self;
}

#pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateHisArr) name:@"insertschkeyword" object:nil];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
 
    //
    hisArr=[[SearchHistoryFmdb shareFmdb] getAllHistory];
    
    tjArr=@[@"马尔代夫",@"白马庄园"];
    
    distArr=[NSMutableArray arrayWithObjects:@"马尔代夫",@"巴厘岛",@"香港",@"帕劳",@"毛里求斯",@"普吉岛",@"塞班",@"大溪地",@"日本",@"新加坡",@"塞舌尔",@"苏梅岛",@"清迈",@"曼谷+芭提雅",@"韩国",nil];
    distShowArr=[NSMutableArray array];
    for (NSString *str in distArr) {
        if (distShowArr.count<8) {
            [distShowArr addObject:str];
        }
    }
}

-(void)updateHisArr{
    hisArr=[[SearchHistoryFmdb shareFmdb] getAllHistory];
    [historyView reloadData];
}

#pragma mark - setUpCustomSearBar
-(void)setUpCustomSearBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    CGFloat phoneWidth=44;
    
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar lt_setBackgroundColor:[UIColor colorWithHexString:@"#ffffff"]];
    [self.view addSubview:cusBar];
    
    UIImageView *shadowImg=[[UIImageView alloc] initWithFrame:CGRectMake(0, 63.3, cusBar.width, 0.7)];
    shadowImg.backgroundColor=[UIColor colorWithHexString:@"#eeeeee"];
    [self.view addSubview:shadowImg];
    
    //
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(12, 26, SCREENSIZE.width-24-phoneWidth, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=5;
    searchView.layer.borderWidth=0.8;
    searchView.layer.borderColor=[UIColor colorWithHexString:@"#e4e4e4"].CGColor;
    [self.view addSubview:searchView];
    
    //放大镜图片
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    //textField
    searchTf=[[UITextField alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    searchTf.font=kFontSize14;
    searchTf.placeholder=@"目的地/岛屿/酒店";
    [searchTf setValue:[UIColor colorWithHexString:@"#71808c"] forKeyPath:@"_placeholderLabel.textColor"];
    searchTf.delegate=self;
    searchTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTf.textColor=[UIColor blackColor];
    searchTf.tintColor=[UIColor colorWithHexString:YxColor_Blue];
    searchTf.returnKeyType=UIReturnKeySearch;
    [searchTf becomeFirstResponder];
    [searchTf addTarget:self action:@selector(keyWordDidChange:) forControlEvents:UIControlEventEditingChanged];
    [searchView addSubview:searchTf];
    
    //取消按钮
    UIButton *cencleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cencleBtn.frame=CGRectMake(cusBar.width-searchView.height-12, searchView.origin.y, searchView.height, searchView.height);
    cencleBtn.titleLabel.font=kFontSize15;
    [cencleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cencleBtn setTitleColor:[UIColor colorWithHexString:@"#a0a0a0"] forState:UIControlStateNormal];
    [cencleBtn addTarget:self action:@selector(cancelNav) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cencleBtn];
}

-(void)cancelNav{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - 初始化热门搜索、搜索历史View
-(void)setUpHistoryView{
    
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    historyView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:myLayout];
    historyView.backgroundColor=[UIColor whiteColor];
    historyView.dataSource=self;
    historyView.delegate=self;
    historyView.showsVerticalScrollIndicator=NO;
    historyView.bounces=YES;
    [historyView registerNib:[UINib nibWithNibName:@"SearchItem01Cell" bundle:nil] forCellWithReuseIdentifier:@"cell01"];
    [historyView registerNib:[UINib nibWithNibName:@"SearchReusableReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusable"];
    [self.view addSubview:historyView];
    
}

#pragma mark - textfield
- (void)keyWordDidChange:(id) sender {
    [self.view bringSubviewToFront:resultTable];
    UITextField *field = (UITextField *)sender;
    NSString *str=field.text;
    if (str.length>0) {
        historyView.hidden=YES;
    }else{
        historyView.hidden=NO;
    }
}

#pragma mark - 添加热门关键词

-(CGFloat)createKeyWorkForlArray:(NSArray *)modelArr headview:(UIView *)headview{
    
    CGFloat bottomYH=0;
    CGFloat btnX=0;
    CGFloat btnY=HotSearView_Height+14;
    CGFloat btnWidth=(SCREENSIZE.width-X_Space*5)/4;
    CGFloat btnHeight=Btn_Height;
    
    for (int i=0; i<modelArr.count; i++) {
        NSString *model=modelArr[i];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.cornerRadius=3;
        btn.layer.borderWidth=0.6f;
        btn.layer.borderColor=[UIColor grayColor].CGColor;
        btnX=X_Space*i+btnWidth*i+X_Space;
        if (i>=4) {
            int kXishu=i/4+1;       //有几行的系数
            
            btnX=X_Space*(i-4)+(i-4)*btnWidth+X_Space;
            btnY=HotSearView_Height+Btn_Height*(kXishu-1)+11*kXishu;
            
//            DSLog(@"%d--%d--%f",i,kXishu,btnY);
        }
        
        btn.frame=CGRectMake(btnX, btnY, btnWidth, btnHeight);
        
        [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth=YES;
        [btn setTitle:model forState:UIControlStateNormal];
        objc_setAssociatedObject(btn, "SearKeyWord",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        btn.titleLabel.font=kFontSize13;
        [btn addTarget:self action:@selector(clickKeyWordBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [headview addSubview:btn];
        
        bottomYH=btn.origin.y+btn.height+X_Space;
        
    }
    return bottomYH;
}

#pragma mark - 点击热门关键字
-(void)clickKeyWordBtn:(id)sender{
    [AppUtils closeKeyboard];
    //
    seletedKeyWordBtn.backgroundColor=[UIColor whiteColor];
    [seletedKeyWordBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    seletedKeyWordBtn.enabled=YES;
    
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    NSString *model = objc_getAssociatedObject(sebtn, "SearKeyWord");
    seletedKeyWordStr=model;
    [[SearchHistoryFmdb shareFmdb] insertSearchHistoryDate:seletedKeyWordStr];
    searchTf.text=seletedKeyWordStr;
    
    sebtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [sebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sebtn.enabled=NO;
    
    //用另一个button代替当前点击的button
    seletedKeyWordBtn=sebtn;
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section==0){
        return hisArr.count==0?1:hisArr.count;
    }else if(section==1){
        return tjArr.count;
    }else{
        return distShowArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchItem01Cell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell01" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[SearchItem01Cell alloc]init];
    }
    
    switch (indexPath.section) {
        case 0:
        {
            cell.dataArr=hisArr;
            if (hisArr.count!=0) {
                cell.hotItemStr=hisArr[indexPath.row];
            }
        }
            break;
        case 1:
        {
            if (tjArr.count!=0) {
                cell.hotItemStr=tjArr[indexPath.row];
            }
        }
            break;
        case 2:
        {
            if (distArr.count!=0) {
                cell.hotItemStr=distArr[indexPath.row];
            }
        }
            break;
            
        default:
            break;
    }
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(hisArr==0?SCREENSIZE.width:(SCREENSIZE.width-54)/4, 30);
    }else{
        return CGSizeMake((SCREENSIZE.width-54)/4, 30);
    }
}


//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 8;
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==0) {
        if (hisArr.count<=0) {
            return UIEdgeInsetsMake(0, 0, 0, 0);
        }else{
            return UIEdgeInsetsMake(8, 12, 8, 12);
        }
    }else{
        return UIEdgeInsetsMake(10, 12, 10, 12);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchResultController *resultCtr=[SearchResultController new];
    
    NSString *kStr;
    BOOL isUpdate;
    if (indexPath.section==0) {
        if (hisArr.count!=0) {
            isUpdate=NO;
            kStr=hisArr[indexPath.row];
            resultCtr.isUpdateHist=isUpdate;
            resultCtr.keyWord=kStr;
            [self.navigationController pushViewController:resultCtr animated:YES];
        }
    }else if (indexPath.section==1){
        isUpdate=YES;
        kStr=tjArr[indexPath.row];
        resultCtr.isUpdateHist=isUpdate;
        resultCtr.keyWord=kStr;
        [self.navigationController pushViewController:resultCtr animated:YES];
    }else if (indexPath.section==2){
        isUpdate=YES ;
        kStr=distArr[indexPath.row];
        resultCtr.isUpdateHist=isUpdate;
        resultCtr.keyWord=kStr;
        [self.navigationController pushViewController:resultCtr animated:YES];
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        myRsbView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusable" forIndexPath:indexPath];
        switch (indexPath.section) {
            case 0:
                myRsbView.reusableId=@"00";
                myRsbView.imgv.image=[UIImage imageNamed:@"search_r01"];
                [myRsbView.button setTitle:@"清除" forState:UIControlStateNormal];
                myRsbView.title.text=@"历史搜索";
                break;
            case 1:
                myRsbView.reusableId=@"11";
                myRsbView.imgv.image=[UIImage imageNamed:@"search_r02"];
                [myRsbView.button setTitle:@"" forState:UIControlStateNormal];
                myRsbView.title.text=@"热门推荐";
                break;
            case 2:
                myRsbView.reusableId=@"22";
                myRsbView.imgv.image=[UIImage imageNamed:@"search_r03"];
                [myRsbView.button setTitle:@"换一换" forState:UIControlStateNormal];
                myRsbView.title.text=@"热门目的地";
                break;
            default:
                break;
        }
        myRsbView.delegate=self;
        return myRsbView;
    }
    return nil;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(SCREENSIZE.width, 36);
}

#pragma mark - 点击reusable 按钮
-(void)clickSearchReusableBtnWithId:(NSString *)reuId{
    if ([reuId isEqualToString:@"00"]) {
        if (hisArr.count!=0) {
            [[SearchHistoryFmdb shareFmdb] clearHistory];
            hisArr=[[SearchHistoryFmdb shareFmdb] getAllHistory];
            [historyView reloadData];
        }
    }else if ([reuId isEqualToString:@"22"]){
        //随机重新排列
        for (int i=0; i<distArr.count; i++) {
            int  rand = arc4random() % distArr.count;
            NSString *str=distArr[rand];
            if (distShowArr.count<8) {
                [distShowArr addObject:str];
                [distShowArr removeObjectAtIndex:rand];
            }
            [distArr addObject:str];
            [distArr removeObjectAtIndex:rand];
        }
        [historyView reloadData];
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [AppUtils closeKeyboard];
    if (textField.text.length>0) {
        SearchResultController *resultCtr=[SearchResultController new];
        resultCtr.isUpdateHist=YES;
        resultCtr.keyWord=textField.text;
        [self.navigationController pushViewController:resultCtr animated:YES];
    }
    return YES;
}

@end
