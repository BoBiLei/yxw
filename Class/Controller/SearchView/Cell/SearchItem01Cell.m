//
//  SearchItem01Cell.m
//  youxia
//
//  Created by mac on 16/5/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchItem01Cell.h"

@implementation SearchItem01Cell{
    UILabel *label;
    UILabel *tipLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    CGRect frame=self.frame;
    frame.size.width=(SCREENSIZE.width-54)/4;
    frame.size.height=30;
    self.frame=frame;
    
    //
    label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
    label.layer.cornerRadius=2;
    label.layer.borderWidth=0.6f;
    label.layer.borderColor=[UIColor lightGrayColor].CGColor;
    label.textAlignment=NSTextAlignmentCenter;
    label.font=kFontSize14;
    label.textColor=[UIColor colorWithHexString:@"#848484"];
    [self addSubview:label];
    
    //
    tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 8, SCREENSIZE.width, 22)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=kFontSize14;
    tipLabel.textColor=[UIColor lightGrayColor];
    tipLabel.text=@"暂无历史搜索记录";
    tipLabel.hidden=YES;
    [self addSubview:tipLabel];
}

-(void)setDataArr:(NSArray *)dataArr{
    if (dataArr.count==0) {
        CGRect frame=self.frame;
        frame.size.width=SCREENSIZE.width;
        self.frame=frame;
        
        //
        label.hidden=YES;
        tipLabel.hidden=NO;
    }else{
        CGRect frame=self.frame;
        frame.size.width=(SCREENSIZE.width-54)/4;
        self.frame=frame;
        
        //
        label.hidden=NO;
        tipLabel.hidden=YES;
    }
}

-(void)setHotItemStr:(NSString *)hotItemStr{
    //
    label.text=hotItemStr;
}

@end
