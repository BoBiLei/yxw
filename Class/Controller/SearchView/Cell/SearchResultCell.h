//
//  SearchResultCell.h
//  youxia
//
//  Created by mac on 16/1/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultModel.h"

@interface SearchResultCell : UITableViewCell

-(void)reflushDataForModel:(SearchResultModel *)model;

@end
