//
//  SearchResultCell.m
//  youxia
//
//  Created by mac on 16/1/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchResultCell.h"

@implementation SearchResultCell{
    UIImageView *imgv;
    UILabel *titleLabel;
    UILabel *en_Label;
    UILabel *erjLabel;
    UILabel *price;
    UILabel *stagePrice;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    //
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, (SCREENSIZE.width-24)/3, 80)];
    [self addSubview:imgv];
    
    frame.size.height=imgv.origin.y*2+imgv.height;
    self.frame=frame;
    
    erjLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, imgv.height-21, imgv.width, 21)];
    erjLabel.backgroundColor=[UIColor colorWithHexString:@"#6DCFF6"];
    erjLabel.font=kFontSize13;
    erjLabel.adjustsFontSizeToFitWidth = YES;
    erjLabel.textColor=[UIColor whiteColor];
    erjLabel.textAlignment=NSTextAlignmentCenter;
    [imgv addSubview:erjLabel];
    
    //
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+8, imgv.origin.y, SCREENSIZE.width-(imgv.origin.x+imgv.width+16), 40)];
    titleLabel.font=kFontSize15;
    titleLabel.numberOfLines=0;
    [self addSubview:titleLabel];
    
    //
    stagePrice=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, self.height-29, titleLabel.width, 21)];
    stagePrice.textColor=[UIColor colorWithHexString:YxColor_Yellow];
    stagePrice.textAlignment=NSTextAlignmentRight;
    stagePrice.font=kFontSize14;
    [self addSubview:stagePrice];
    //
    price=[UILabel new];
    price.font=kFontSize14;
    price.textColor=[UIColor colorWithHexString:YxColor_Yellow];
    price.textAlignment=NSTextAlignmentRight;
    [self addSubview:price];
    price.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat priceWidth=stagePrice.width/2-20;
    NSString *imghFor=[NSString stringWithFormat:@"[price(%.f)]-8-|",priceWidth];
    NSArray* imgBig_h = [NSLayoutConstraint constraintsWithVisualFormat:imghFor options:0 metrics:nil views:NSDictionaryOfVariableBindings(price)];
    [NSLayoutConstraint activateConstraints:imgBig_h];
    
    NSArray* imgBig_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[price][stagePrice]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(price,stagePrice)];
    [NSLayoutConstraint activateConstraints:imgBig_w];
    
    //
    en_Label=[UILabel new];
    en_Label.font=kFontSize13;
    en_Label.textColor=[UIColor colorWithHexString:@"#282828"];
    en_Label.adjustsFontSizeToFitWidth=YES;
    en_Label.textAlignment=NSTextAlignmentLeft;
    [self addSubview:en_Label];
    en_Label.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *en_LabelFor=[NSString stringWithFormat:@"[en_Label][price]"];
    NSArray* enLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:en_LabelFor options:0 metrics:nil views:NSDictionaryOfVariableBindings(en_Label,price)];
    [NSLayoutConstraint activateConstraints:enLabel_h];
    
    NSArray* enLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[en_Label]-2-[stagePrice]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(en_Label,stagePrice)];
    [NSLayoutConstraint activateConstraints:enLabel_w];
}

-(void)reflushDataForModel:(SearchResultModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.imgStr]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.imgStr];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.imgStr] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
    
    erjLabel.text=model.starLevel;
    titleLabel.text=model.islandName;
//    en_Label.text=model.en_Name;
    price.text=[NSString stringWithFormat:@"￥%@/人",model.price];
    stagePrice.text=[NSString stringWithFormat:@"【分期价】￥%@x9期",model.stagePrice];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
