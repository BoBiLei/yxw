//
//  SearchReusableReusableView.m
//  youxia
//
//  Created by mac on 16/5/20.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchReusableReusableView.h"

@implementation SearchReusableReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor colorWithHexString:@"#ffffff"];
    self.title.textColor=[UIColor colorWithHexString:@"#515151"];
    [self.button setTitleColor:[UIColor colorWithHexString:@"#348fcb"] forState:UIControlStateNormal];
    
    //
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(12, self.height-0.8, SCREENSIZE.width-24, 0.8)];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line];
}

- (IBAction)clickBtn:(id)sender {
    [self.delegate clickSearchReusableBtnWithId:self.reusableId];
}


@end
