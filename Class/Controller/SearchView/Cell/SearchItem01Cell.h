//
//  SearchItem01Cell.h
//  youxia
//
//  Created by mac on 16/5/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchItem01Cell : UICollectionViewCell

@property (nonatomic, retain) NSArray *dataArr;

@property (nonatomic, copy) NSString *hotItemStr;

@end
