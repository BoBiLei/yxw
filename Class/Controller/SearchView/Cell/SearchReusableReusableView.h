//
//  SearchReusableReusableView.h
//  youxia
//
//  Created by mac on 16/5/20.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchReusableDelegate <NSObject>

-(void)clickSearchReusableBtnWithId:(NSString *)reuId;

@end

@interface SearchReusableReusableView : UICollectionReusableView
@property (copy, nonatomic) NSString *reusableId;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (nonatomic, assign) BOOL isHadTopLine;

@property (nonatomic,weak) id<SearchReusableDelegate> delegate;

@end
