//
//  RootViewController.h
//  YXW
//
//  Created by mac on 15/11/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RDVTabBarController.h>
#import <RDVTabBarItem.h>

@interface RootViewController : RDVTabBarController

//@interface RootViewController : UITabBarController

@end
