//
//  RootViewController.m
//  YXW
//
//  Created by mac on 15/11/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "RootViewController.h"
#import "WelcomeController.h"
#import "BindingPhoneController.h"
#import "FilmController.h"

@interface RootViewController ()<RDVTabBarControllerDelegate>

@end

#define Tabbar_NorColor @"1172bd"
#define Tabbar_SeleColor @"ef662f"
@implementation RootViewController{
    PersonalCenterController * myVc;
    UINavigationController *personalNav;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    /****************************
     1、接收通知
     2、登录成功后tabbar下标跳转到个人中心
     *****************************/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateSeletePersonalIndex) name:LoginSuccessNotifition object:nil];
    self.delegate=self;
    [self initViewControllers];
}

-(void)upDateSeletePersonalIndex{
    self.selectedIndex=4;
}

#pragma mark - init ViewControllers
-(void)initViewControllers{
    
    HomePageController * vc = [[HomePageController alloc] init];
    vc.title=@"首页";
    UINavigationController * homePageNav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    //
    IslandController *isLandCtr=[[IslandController alloc]init];
    isLandCtr.title=@"目的地";
    UINavigationController *isLandNav=[[UINavigationController alloc]initWithRootViewController:isLandCtr];
    isLandNav.navigationBar.shadowImage = [[UIImage alloc]init];
    
    [isLandCtr.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:Tabbar_NorColor],NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [isLandCtr.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:Tabbar_SeleColor],NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    //
    FilmController *consultCtr=[[FilmController alloc]init];
    consultCtr.title=@"游侠电影";
    UINavigationController * consultNav = [[UINavigationController alloc] initWithRootViewController:consultCtr];
    
    //
    PhoneController *phoneCtr=[[PhoneController alloc]init];
    phoneCtr.title=@"咨询";
    UINavigationController *phoneNav=[[UINavigationController alloc]initWithRootViewController:phoneCtr];
    phoneNav.navigationBar.shadowImage = [[UIImage alloc]init];
    
    [phoneCtr.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:Tabbar_NorColor],NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [phoneCtr.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:Tabbar_SeleColor],NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    //
    myVc = [[PersonalCenterController alloc] init];
    myVc.title=@"我的";
    personalNav = [[UINavigationController alloc] initWithRootViewController:myVc];
    
    self.viewControllers=@[homePageNav,isLandNav,consultNav,phoneNav,personalNav];
    
    [self customizeTabBarForController:self];
    
}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    UIImage *seleImage = [UIImage imageNamed:@"tabbar_selected_background"];
    UIImage *unSeleImage = [UIImage imageNamed:@"tabbar_normal_background"];
    NSArray *tabBarItemImages = @[@"first", @"second", @"three", @"four", @"five"];
    
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setSelectedTitleAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], NSForegroundColorAttributeName,
          [UIFont fontWithName:@"AmericanTypewriter" size:10.0], NSFontAttributeName,
          nil]];
        [item setUnselectedTitleAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], NSForegroundColorAttributeName,
          [UIFont fontWithName:@"AmericanTypewriter" size:10.0], NSFontAttributeName,
          nil]];
        
        
        [item setBackgroundSelectedImage:seleImage withUnselectedImage:unSeleImage];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
}

#pragma mark - delegate
- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    if (viewController==personalNav) {
        if (![AppUtils loginState]) {
            //跳到登录页面
            WelcomeController *login=[[WelcomeController alloc]init];
            UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
            [loginNav.navigationBar setShadowImage:[UIImage new]];
            //隐藏tabbar
            login.hidesBottomBarWhenPushed = YES;
            [((UINavigationController *)tabBarController.selectedViewController) presentViewController:loginNav animated:YES completion:nil];
        }else{
            if (([[AppUtils getValueWithKey:User_Phone] isEqualToString:@""]&&
                 [[AppUtils getValueWithKey:Bind_User_Id] isEqualToString:@""])||
                ([[AppUtils getValueWithKey:User_Phone] isEqualToString:@""]&&
                 [[AppUtils getValueWithKey:Bind_User_Id] isEqualToString:@"0"])) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"为了账户安全，建议您先绑定手机号！"preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                        
                    }];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        [myVc.navigationController pushViewController:[BindingPhoneController new] animated:YES];
                    }];
                    [alertController addAction:cancelAction];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
        }
    }else{
    }
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
