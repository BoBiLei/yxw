//
//  RefundDetailSecondCell.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelRefundDetailModel.h"

@interface RefundDetailSecondCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;

-(void)reflushData:(HotelRefundDetailModel *)model;

@end
