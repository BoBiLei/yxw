//
//  HotelRefundDetailHeaderCell.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelRefundDetailHeaderCell.h"

@implementation HotelRefundDetailHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushModel:(HotelRefundDetailModel *)model{
    self.topLabel.text=model.statusInfo;
    self.bottomLabel.text=model.statusComment;
}

@end
