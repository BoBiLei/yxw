//
//  HotelRefundDetailHeaderCell.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelRefundDetailModel.h"

@interface HotelRefundDetailHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

-(void)reflushModel:(HotelRefundDetailModel *)model;

@end
