//
//  HotelRefundDetailLastCell.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelRefundDetailLastCell.h"

@implementation HotelRefundDetailLastCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.stageLabel.layer.cornerRadius=10;
    self.stageLabel.layer.masksToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(HotelRefundDetailOperateModel *)model atIndexPatch:(NSIndexPath *)indexPath{
    self.titleLabel.text=model.operate;
    self.detailLabel.text=model.comment;
    self.stageLabel.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
    self.stageLabel.backgroundColor=[model.operateStatus isEqualToString:@"1"]?[UIColor colorWithHexString:@"FA8D00"]:[UIColor colorWithHexString:@"ABABAB"];
    switch (indexPath.row) {
        case 0:
            self.line01.hidden=YES;
            self.line02.hidden=NO;
            break;
        case 1:
            self.line01.hidden=NO;
            self.line02.hidden=NO;
            break;
        case 2:
            self.line01.hidden=NO;
            self.line02.hidden=NO;
            break;
        default:
            self.line01.hidden=NO;
            self.line02.hidden=YES;
            break;
    }
}

@end
