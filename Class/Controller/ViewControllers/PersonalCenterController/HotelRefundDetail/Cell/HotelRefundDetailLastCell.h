//
//  HotelRefundDetailLastCell.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelRefundDetailOperateModel.h"

@interface HotelRefundDetailLastCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *line01;
@property (weak, nonatomic) IBOutlet UIView *line02;
@property (weak, nonatomic) IBOutlet UILabel *stageLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

-(void)reflushData:(HotelRefundDetailOperateModel *)model atIndexPatch:(NSIndexPath *)indexPath;

@end
