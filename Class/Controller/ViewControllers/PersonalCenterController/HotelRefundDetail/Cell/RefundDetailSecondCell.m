//
//  RefundDetailSecondCell.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RefundDetailSecondCell.h"

@implementation RefundDetailSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(HotelRefundDetailModel *)model{
    self.moneyLabel.text=[NSString stringWithFormat:@"￥%@",model.refundMoney];
    self.accountLabel.text=model.payType;
}

@end
