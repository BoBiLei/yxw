//
//  HotelRefundDetail.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelRefundDetail : UIViewController

+(id)initWithOrderId:(NSString *)orderId;

@end
