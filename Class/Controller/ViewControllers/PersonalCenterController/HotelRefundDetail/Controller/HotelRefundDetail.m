//
//  HotelRefundDetail.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelRefundDetail.h"
#import "HotelRefundDetailHeaderCell.h"
#import "RefundDetailSecondCell.h"
#import "HotelRefundDetailLastCell.h"
#import "HotelRefundDetailModel.h"
#import "HotelRefundDetailOperateModel.h"

@interface HotelRefundDetail ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, strong) UITableView *myTable;

@property (nonatomic, retain) NSMutableArray *dataArr;

@end

@implementation HotelRefundDetail{
    HotelRefundDetailModel *model;
}

+(id)initWithOrderId:(NSString *)orderId{
    HotelRefundDetail *vc=[HotelRefundDetail new];
    vc.orderId=orderId;
    return vc;
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr=[NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"退款详情";
    [self.view addSubview:navBar];
    
    [self dataRequest];
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"HotelRefundDetailHeaderCell" bundle:nil] forCellReuseIdentifier:@"HotelRefundDetailHeaderCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"RefundDetailSecondCell" bundle:nil] forCellReuseIdentifier:@"RefundDetailSecondCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HotelRefundDetailLastCell" bundle:nil] forCellReuseIdentifier:@"HotelRefundDetailLastCell"];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [self.view addSubview:_myTable];
    }
    return  _myTable;
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section==2?4:1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 80;
    }else if (indexPath.section==1){
        return 90;
    }else{
        return 76;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0||section==1?16:54;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 0.0000001f;
    }else{
        return 8;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0||section==1) {
        return nil;
    }else{
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 54)];
        view.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        
        UIView *mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 10, SCREENSIZE.width, 44)];
        mainView.backgroundColor=[UIColor whiteColor];
        [view addSubview:mainView];
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(14, 0, mainView.width-28, mainView.height)];
        titleLabel.font=kFontSize16;
        titleLabel.text=@"退款流程";
        [mainView addSubview:titleLabel];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 53.5, SCREENSIZE.width, 0.5f)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [view addSubview:line];
        
        return view;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        HotelRefundDetailHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelRefundDetailHeaderCell"];
        if (!cell) {
            cell=[[HotelRefundDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelRefundDetailHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushModel:model];
        return  cell;
    }else if (indexPath.section==1){
        RefundDetailSecondCell *cell=[tableView dequeueReusableCellWithIdentifier:@"RefundDetailSecondCell"];
        if (!cell) {
            cell=[[RefundDetailSecondCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RefundDetailSecondCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else{
        HotelRefundDetailLastCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelRefundDetailLastCell"];
        if (!cell) {
            cell=[[HotelRefundDetailLastCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelRefundDetailLastCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        HotelRefundDetailOperateModel *rModel=_dataArr[indexPath.row];
        [cell reflushData:rModel atIndexPatch:indexPath];
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"refundOrderInfo",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            model=[HotelRefundDetailModel new];
            [model jsonToModel:responseObject[@"retData"]];
            if (model.operateList.count!=0) {
                for (NSDictionary *dic in model.operateList) {
                    HotelRefundDetailOperateModel *rModel=[HotelRefundDetailOperateModel new];
                    [rModel jsonToModel:dic];
                    [self.dataArr addObject:rModel];
                }
            }
            [self.myTable reloadData];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
