//
//  HotelRefundDetailModel.h
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelRefundDetailModel : NSObject

@property (nonatomic, copy) NSString *cancelTime;
@property (nonatomic, retain) NSArray *operateList;
@property (nonatomic, copy) NSString *payType;
@property (nonatomic, copy) NSString *refundMoney;
@property (nonatomic, copy) NSString *statusInfo;
@property (nonatomic, copy) NSString *statusComment;

-(void)jsonToModel:(NSDictionary *)dic;

@end
