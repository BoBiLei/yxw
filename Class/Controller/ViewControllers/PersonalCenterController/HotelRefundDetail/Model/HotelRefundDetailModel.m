//
//  HotelRefundDetailModel.m
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelRefundDetailModel.h"

@implementation HotelRefundDetailModel

-(void)jsonToModel:(NSDictionary *)dic{
    //
    if ([dic[@"cancelTime"] isKindOfClass:[NSNull class]]) {
        self.cancelTime=@"";
    }else{
        self.cancelTime=dic[@"cancelTime"];
    }
    
    //
    if ([dic[@"operateList"] isKindOfClass:[NSNull class]]) {
        self.operateList=@[];
    }else{
        self.operateList=dic[@"operateList"];
    }
    
    //
    if ([dic[@"payType"] isKindOfClass:[NSNull class]]) {
        self.payType=@"";
    }else{
        self.payType=dic[@"payType"];
    }
    
    //
    if ([dic[@"refundMoney"] isKindOfClass:[NSNull class]]) {
        self.refundMoney=@"";
    }else{
        self.refundMoney=dic[@"refundMoney"];
    }
    
    //
    if ([dic[@"statusComment"] isKindOfClass:[NSNull class]]) {
        self.statusComment=@"";
    }else{
        self.statusComment=dic[@"statusComment"];
    }
    
    //
    if ([dic[@"statusInfo"] isKindOfClass:[NSNull class]]) {
        self.statusInfo=@"";
    }else{
        self.statusInfo=dic[@"statusInfo"];
    }
}

@end
