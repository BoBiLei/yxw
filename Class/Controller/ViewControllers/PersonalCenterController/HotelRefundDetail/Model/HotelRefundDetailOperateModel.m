//
//  HotelRefundDetailOperateModel.m
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelRefundDetailOperateModel.h"

@implementation HotelRefundDetailOperateModel

-(void)jsonToModel:(NSDictionary *)dic{
    //
    if ([dic[@"comment"] isKindOfClass:[NSNull class]]) {
        self.comment=@"";
    }else{
        self.comment=dic[@"comment"];
    }
    
    //
    if ([dic[@"operate"] isKindOfClass:[NSNull class]]) {
        self.operate=@"";
    }else{
        self.operate=dic[@"operate"];
    }
    
    //
    if ([dic[@"operateStatus"] isKindOfClass:[NSNull class]]) {
        self.operateStatus=@"";
    }else{
        self.operateStatus=[NSString stringWithFormat:@"%@",dic[@"operateStatus"]];
    }
}

@end
