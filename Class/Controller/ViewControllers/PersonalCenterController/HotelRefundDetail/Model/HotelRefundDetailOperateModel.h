//
//  HotelRefundDetailOperateModel.h
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelRefundDetailOperateModel : NSObject

@property (nonatomic, copy) NSString *comment;
@property (nonatomic, copy) NSString *operate;
@property (nonatomic, copy) NSString *operateStatus;

-(void)jsonToModel:(NSDictionary *)dic;

@end
