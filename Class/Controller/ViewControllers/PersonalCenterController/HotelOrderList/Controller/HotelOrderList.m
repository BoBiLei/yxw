//
//  HotelOrderList.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelOrderList.h"
#import "HotelOrderListCell.h"
#import "PSCHotelOrderDetail.h"
#import "HotelOrderListModel.h"
#import "CancelHotelOrder.h"

@interface HotelOrderList ()<UITableViewDataSource,UITableViewDelegate,HotelOrderListDelegate>

@end

@implementation HotelOrderList{
    UIView *btLine;
    UIButton *seleBtn;
    
    UITableView *myTable;
    NSMutableArray *dataArr;
    
    NSInteger seleHeaderTag;       //标识 全部、代付款、处理中、预约成功、已成交
    
    NSInteger pageInt;
    NSString *requestType;
    MJRefreshNormalHeader *refleshHeader;
    MJRefreshAutoNormalFooter *refleshFooter;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRequest) name:@"reflush_hotellist_noti" object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"酒店订单";
    [self.view addSubview:navBar];
    
    [self setUpTopBtnForArray:@[@"全部",@"待付款",@"处理中",@"预约成功",@"已成交"]];
    
    [self setUpTable];
    
    [AppUtils showProgressInView:self.view];
    [self dataRequestWithType:requestType];
}

-(void)updateRequest{
    [AppUtils showProgressInView:self.view];
    pageInt=1;
    dataArr=[NSMutableArray array];
    [self dataRequestWithType:requestType];
}

#pragma mark - setUp 赛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    requestType=@"99";
    dataArr=[NSMutableArray array];
    pageInt=1;
    seleHeaderTag=1;
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=64;
    for (int i=0; i<arr.count; i++) {
        NSString *name=arr[i];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i+1;
        btn.frame=CGRectMake(i*btnWidth, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if (i==0) {
            lineX=btn.origin.x;
            seleBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(0, btnY+btnHeihgt-2.5f, btnWidth, 3.0f)];
    btLine.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:btLine];
    
    UIView *btmLine=[[UIView alloc] initWithFrame:CGRectMake(0, btLine.origin.y+btLine.height, SCREENSIZE.width-1, 0.5f)];
    btmLine.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [self.view addSubview:btmLine];
}

#pragma mark - 点击赛选订单按钮
-(void)clickFBtn:(id)sender{
    pageInt=1;
    [seleBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
    seleBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    seleHeaderTag=btn.tag-2;
    switch (btn.tag) {
        case 1:
            requestType=@"99";
            break;
        case 2:
            requestType=@"0";
            break;
        case 3:
            requestType=@"1";
            break;
        case 4:
            requestType=@"2";
            break;
        case 5:
            requestType=@"5";
            break;
        default:
            break;
    }
    
    [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    [UIView animateWithDuration:0.20 animations:^{
        CGRect frame=btLine.frame;
        frame.origin.x=btn.origin.x;
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        dataArr=[NSMutableArray array];
        [AppUtils showProgressInView:self.view];
        [self dataRequestWithType:requestType];
    }];
    seleBtn=btn;
}

-(void)setUpTable{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 109, SCREENSIZE.width, SCREENSIZE.height-109) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"HotelOrderListCell" bundle:nil] forCellReuseIdentifier:@"HotelOrderListCell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            pageInt=1;
            dataArr=[NSMutableArray array];
            [self dataRequestWithType:requestType];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    
    // 设置字体
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    myTable.mj_header = refleshHeader;
    
    ////////////////////
    //加载更多
    refleshFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self hadingFilmRefreshForPullUp];
    }];
    refleshFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    refleshFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [refleshFooter setTitle:@"" forState:MJRefreshStateIdle];
    [refleshFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [refleshFooter setTitle:@"没有更多" forState:MJRefreshStateNoMoreData];
    refleshFooter.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = refleshFooter;
}

#pragma mark - 加载更多
- (void)hadingFilmRefreshForPullUp{
    if (pageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dataRequestWithType:requestType];
        });
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 197;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?16:8;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==dataArr.count-1?8:8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotelOrderListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelOrderListCell"];
    if (!cell) {
        cell=[[HotelOrderListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelOrderListCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.section<dataArr.count){
        HotelOrderListModel *model=dataArr[indexPath.section];
        [cell reflushData:model];
        cell.delegate=self;
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击Cell右边按钮
-(void)clickRightBtn:(HotelOrderListModel *)model{
    [self.navigationController pushViewController:[PSCHotelOrderDetail initWithOrderId:model.orderid] animated:YES];
}

#pragma mark - 点击Cell左边按钮
-(void)clickLeftBtn:(HotelOrderListModel *)model{
    switch (model.status.integerValue) {
        case 0:
        {
            [self.navigationController pushViewController:[CancelHotelOrder initWithOrderId:model.orderid] animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - request
-(void)dataRequestWithType:(NSString *)type{
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"userOrderList",
                         @"type":type,
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"page":[NSString stringWithFormat:@"%ld",pageInt]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@--%ld",responseObject,pageInt);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"][@"orderList"];
            if ([retObj isKindOfClass:[NSArray class]]) {
                NSArray *arr=retObj;
                for (NSDictionary *dic in arr) {
                    HotelOrderListModel *model=[HotelOrderListModel new];
                    [model jsonToModel:dic];
                    [dataArr addObject:model];
                }
                
                NSString *totalPage=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"totalPage"]];
                if (pageInt<totalPage.integerValue) {
                    pageInt++;
                    [refleshFooter endRefreshing];
                }else{
                    [refleshFooter endRefreshingWithNoMoreData];
                }
                [myTable.mj_header endRefreshing];
                [myTable reloadData];
            }
        }else{
            [myTable.mj_header endRefreshing];
            [refleshFooter endRefreshingWithNoMoreData];
            [myTable reloadData];
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [myTable.mj_header endRefreshing];
        [refleshFooter endRefreshing];
        [myTable reloadData];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
