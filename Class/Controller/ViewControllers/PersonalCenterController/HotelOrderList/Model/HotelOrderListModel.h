//
//  HotelOrderListModel.h
//  youxia
//
//  Created by mac on 2017/5/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelOrderListModel : NSObject

@property (nonatomic, copy) NSString *checkin;
@property (nonatomic, copy) NSString *checkout;
@property (nonatomic, copy) NSString *defaultImg;
@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, copy) NSString *nights;
@property (nonatomic, copy) NSString *orderid;
@property (nonatomic, copy) NSString *paymoney;
@property (nonatomic, copy) NSString *paystatus;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusInfo;

-(void)jsonToModel:(NSDictionary *)dic;

@end
