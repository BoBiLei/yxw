//
//  HotelOrderListModel.m
//  youxia
//
//  Created by mac on 2017/5/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelOrderListModel.h"

@implementation HotelOrderListModel

-(void)jsonToModel:(NSDictionary *)dic{
    if (dic[@"checkin"]) {
        self.checkin=dic[@"checkin"];
    }else{
        self.checkin=@"";
    }
    //
    if (dic[@"checkout"]) {
        self.checkout=dic[@"checkout"];
    }else{
        self.checkout=@"";
    }
    //
    if (dic[@"defaultImg"]) {
        self.defaultImg=dic[@"defaultImg"];
    }else{
        self.defaultImg=@"";
    }
    //
    if (dic[@"hotelName"]) {
        self.hotelName=dic[@"hotelName"];
    }else{
        self.hotelName=@"";
    }
    //
    if (dic[@"nights"]) {
        self.nights=dic[@"nights"];
    }else{
        self.nights=@"";
    }
    //
    if (dic[@"orderid"]) {
        self.orderid=dic[@"orderid"];
    }else{
        self.orderid=@"";
    }
    //
    if (dic[@"paymoney"]) {
        self.paymoney=dic[@"paymoney"];
    }else{
        self.paymoney=@"";
    }
    //
    if (dic[@"paystatus"]) {
        self.paystatus=dic[@"paystatus"];
    }else{
        self.paystatus=@"";
    }
    //
    if (dic[@"status"]) {
        self.status=dic[@"status"];
    }else{
        self.status=@"";
    }
    //
    if (dic[@"statusInfo"]) {
        self.statusInfo=dic[@"statusInfo"];
    }else{
        self.statusInfo=@"";
    }
}

@end
