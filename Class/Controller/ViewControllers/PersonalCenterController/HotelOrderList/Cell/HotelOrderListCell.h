//
//  HotelOrderListCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelOrderListModel.h"

@protocol HotelOrderListDelegate <NSObject>

-(void)clickRightBtn:(HotelOrderListModel *)model;

-(void)clickLeftBtn:(HotelOrderListModel *)model;

@end

@interface HotelOrderListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *statusInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *hotelImgv;
@property (weak, nonatomic) IBOutlet UILabel *hotelNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalNightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalpriceLabel;

@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;

@property (strong, nonatomic) HotelOrderListModel *model;

@property (weak, nonatomic) id<HotelOrderListDelegate> delegate;

-(void)reflushData:(HotelOrderListModel *)model;

@end
