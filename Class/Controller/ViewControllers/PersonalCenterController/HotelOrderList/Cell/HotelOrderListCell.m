//
//  HotelOrderListCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelOrderListCell.h"

@implementation HotelOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _rightBtn.layer.borderWidth=0.7f;
    _leftBtn.layer.borderWidth=0.7f;
    _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
    _leftBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickRightBtn:(id)sender {
    [self.delegate clickRightBtn:self.model];
}
- (IBAction)clickLeftBtn:(id)sender {
    [self.delegate clickLeftBtn:self.model];
}

-(void)reflushData:(HotelOrderListModel *)model{
    self.model=model;
    self.statusInfoLabel.text=[NSString stringWithFormat:@"订单状态：%@",model.statusInfo];
    [self.hotelImgv sd_setImageWithURL:[NSURL URLWithString:model.defaultImg] placeholderImage:[UIImage imageNamed:Image_Default]];
    self.hotelNameLabel.text=model.hotelName;
    self.timeLabel.text=[NSString stringWithFormat:@"入住:%@ 离店:%@",model.checkin,model.checkout];
    self.totalNightLabel.text=[NSString stringWithFormat:@"共计：%@晚",model.nights];
    self.totalpriceLabel.text=[NSString stringWithFormat:@"合计：￥%@",model.paymoney];
    //0-待付款 1-处理中 2-预定成功 5-已成交
    switch (model.status.integerValue) {
        case 0:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"fa8d00"];
            self.leftBtn.hidden=NO;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"fa8d00"].CGColor;
            [_rightBtn setTitleColor:[UIColor colorWithHexString:@"fa8d00"] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
            break;
        case 1:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            self.leftBtn.hidden=YES;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
            [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"查看" forState:UIControlStateNormal];
            break;
        case 2:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            self.leftBtn.hidden=YES;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
            [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"查看" forState:UIControlStateNormal];
            break;
        case 5:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            self.leftBtn.hidden=YES;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
            [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"查看" forState:UIControlStateNormal];
            break;
        case 6:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            self.leftBtn.hidden=YES;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
            [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"查看" forState:UIControlStateNormal];
            break;
        case 7: case 8:
            self.statusInfoLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            self.leftBtn.hidden=YES;
            self.rightBtn.hidden=NO;
            _rightBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
            [_rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_rightBtn setTitle:@"查看" forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

@end
