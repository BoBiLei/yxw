//
//  ForgetPassController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ForgetPassController.h"
#import "ResetPassController.h"
#import "JDStatusBarNotification.h"

#define RESETTIME 60

static NSString *const SBStyle1 = @"SBStyle1";
static NSString *const SBStyle2 = @"SBStyle2";

@interface ForgetPassController ()

@end

@implementation ForgetPassController{
    UIView *mainView;
    
    int timerCount;
    NSString *receiverify;
    
    UITextField *phoneTf;
    UITextField *validTf;
    UIButton *getVerifyBtn;
    UIButton *loginBtn;
}

-(instancetype)init{
    self=[super init];
    if (self) {
        [JDStatusBarNotification addStyleNamed:SBStyle1
                                       prepare:^JDStatusBarStyle *(JDStatusBarStyle *style) {
                                           style.barColor = [UIColor colorWithRed:0.797 green:0.000 blue:0.662 alpha:1.000];
                                           style.textColor = [UIColor whiteColor];
                                           style.animationType = JDStatusBarAnimationTypeFade;
                                           style.font = [UIFont fontWithName:@"SnellRoundhand-Bold" size:17.0];
                                           style.progressBarColor = [UIColor colorWithRed:0.986 green:0.062 blue:0.598 alpha:1.000];
                                           style.progressBarHeight = 20.0;
                                           return style;
                                       }];
        
        [JDStatusBarNotification addStyleNamed:SBStyle2
                                       prepare:^JDStatusBarStyle *(JDStatusBarStyle *style) {
                                           style.barColor = [UIColor cyanColor];
                                           style.textColor = [UIColor colorWithRed:0.056 green:0.478 blue:0.998 alpha:1.000];
                                           style.animationType = JDStatusBarAnimationTypeBounce;
                                           style.progressBarColor = style.textColor;
                                           style.progressBarHeight = 5.0;
                                           style.progressBarPosition = JDStatusBarProgressBarPositionTop;
                                           if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                                               style.font = [UIFont fontWithName:@"DINCondensed-Bold" size:15.0];
                                               style.textVerticalPositionAdjustment = 2.0;
                                           } else {
                                               style.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:15.0];
                                           }
                                           return style;
                                       }];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NavigationBar
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"忘记密码";
    [self.view addSubview:navBar];
    
    //MainView
    mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    mainView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainView];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    
    
    //
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(30, 40, SCREENSIZE.width-60, 32)];
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTf.keyboardType=UIKeyboardTypeNumberPad;
    [phoneTf addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
    phoneTf.placeholder=@"请输入手机号码";
    phoneTf.font=kFontSize16;
    [phoneTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [mainView addSubview:phoneTf];
    
    UIView *line01=[[UIView alloc] initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+1, phoneTf.width, 0.7f)];
    line01.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [mainView addSubview:line01];
    
    CGFloat btnWidth=96;
    //
    validTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, line01.origin.y+line01.height+22, phoneTf.width-btnWidth, phoneTf.height)];
    [validTf addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    validTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    validTf.keyboardType=UIKeyboardTypeNumberPad;
    validTf.placeholder=@"验证码";
    validTf.font=kFontSize16;
    [validTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    validTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [mainView addSubview:validTf];
    
    UIView *line02=[[UIView alloc] initWithFrame:CGRectMake(line01.origin.x, 127, line01.width, line01.height)];
    line02.backgroundColor=line01.backgroundColor;
    [mainView addSubview:line02];
    
    //获取验证码按钮
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyBtn.frame=CGRectMake(line02.origin.x+line02.width-btnWidth, validTf.origin.y, btnWidth, validTf.height-3);
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    getVerifyBtn.layer.cornerRadius=getVerifyBtn.height/2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=kFontSize14;
    [mainView addSubview:getVerifyBtn];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    //button
    loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    loginBtn.frame=CGRectMake(0, 0, SCREENSIZE.width/3+20, 42);
    loginBtn.center=CGPointMake(SCREENSIZE.width/2, line02.center.y+70);
    loginBtn.layer.cornerRadius=loginBtn.height/2;
    loginBtn.enabled=NO;
    [loginBtn setTitle:@"找回密码" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBtn.titleLabel.font=kFontSize16;
    [loginBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:loginBtn];
}

#pragma mark - text值改变时
-(void)phoneTextDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]) {
        getVerifyBtn.enabled=YES;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    }else{
        getVerifyBtn.enabled=NO;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    }
}

-(void)textDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]&&
        ![validTf.text isEqualToString:@""]
        ) {
        loginBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        loginBtn.enabled=YES;
    }else{
        loginBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        loginBtn.enabled=NO;
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    NSString *verifyStr=[NSString stringWithFormat:@"%@",receiverify];
    if (![verifyStr isEqualToString:validTf.text]) {
        [AppUtils showSuccessMessage:@"验证码有误" inView:mainView];
    }else{
        ResetPassController *resetPwd=[[ResetPassController alloc]init];
        resetPwd.phone=phoneTf.text;
        resetPwd.verify=receiverify;
        [self.navigationController pushViewController:resetPwd animated:YES];
    }
}

#pragma mark - 点击获取验证码->查询是否注册过->发送验证码
-(void)clickVerifyBtn{
    [AppUtils closeKeyboard];
    [self checkPhoneIsRegist:phoneTf.text];
}

#pragma mark - request查询手机是否已注册
-(void)checkPhoneIsRegist:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:mainView];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"getrandcode_getpassword_iso" forKey:@"code"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:mainView];
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:mainView];
        }else{
            receiverify=responseObject[@"retData"][@"code"];
            DSLog(@"%@",receiverify);
            timerCount=RESETTIME;
            phoneTf.enabled=NO;
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)setTimer:(NSTimer *)timer{
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        getVerifyBtn.enabled=YES;
        phoneTf.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

@end
