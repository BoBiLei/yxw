//
//  ResetPassController.h
//  youxia
//
//  Created by mac on 16/1/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

@interface ResetPassController : UIViewController

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *verify;

@end
