//
//  ResetPassController.m
//  youxia
//
//  Created by mac on 16/1/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ResetPassController.h"

@interface ResetPassController ()

@end

@implementation ResetPassController{
    
    UIView *mainView;
    
    UITextField *cartText;
    UITextField *passText;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NavigationBar
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"修改密码";
    [self.view addSubview:navBar];
    
    //MainView
    mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    mainView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:mainView];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    
    //
    cartText=[[UITextField alloc]initWithFrame:CGRectMake(30, 40, SCREENSIZE.width-60, 32)];
    cartText.clearButtonMode=UITextFieldViewModeWhileEditing;
    cartText.placeholder=@"请输入密码";
    cartText.secureTextEntry=YES;
    cartText.font=kFontSize16;
    [cartText setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    cartText.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [cartText addTarget:self action:@selector(phoneTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainView addSubview:cartText];
    
    UIView *line01=[[UIView alloc] initWithFrame:CGRectMake(cartText.origin.x, cartText.origin.y+cartText.height, cartText.width, 0.6f)];
    line01.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [mainView addSubview:line01];
    
    
    //
    passText=[[UITextField alloc]initWithFrame:CGRectMake(cartText.origin.x, line01.origin.y+line01.height+24, cartText.width, cartText.height)];
    passText.placeholder=@"请再次输入密码";
    passText.secureTextEntry=YES;
    passText.font=kFontSize16;
    [passText setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    passText.tag=1;
    passText.secureTextEntry=YES;
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [passText addTarget:self action:@selector(phoneTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainView addSubview:passText];
    
    UIView *line02=[[UIView alloc] initWithFrame:CGRectMake(passText.origin.x, passText.origin.y+passText.height, passText.width, 0.6f)];
    line02.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [mainView addSubview:line02];
    
    
    //button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(0, 0, SCREENSIZE.width/3+20, 42);
    button.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    button.center=CGPointMake(SCREENSIZE.width/2, line02.center.y+70);
    button.layer.cornerRadius=button.height/2;
    button.enabled=NO;
    [button setTitle:@"立即修改" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=kFontSize16;
    [button addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:button];
}

#pragma mark - text值改变时
-(void)phoneTextDidChange:(id)sender{
    if ([cartText.text isEqualToString:@""]||[passText.text isEqualToString:@""]) {
        button.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        button.enabled=NO;
    }else{
        button.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        button.enabled=YES;
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    if ([AppUtils checkPassworkLength:cartText.text]) {
        if (![cartText.text isEqualToString:passText.text]) {
            [AppUtils showSuccessMessage:@"输入密码不一致" inView:mainView];
        }else{
            NSString *str=[AppUtils judgePasswordStrength:cartText.text];
            if ([str isEqualToString:@"0"]) {
                [AppUtils showSuccessMessage:@"您输入的密码太过简单" inView:self.view];
            }else{
                [self submitRequestWithPass:cartText.text confirmPwd:passText.text];
            }
        }
    }else{
        [AppUtils showSuccessMessage:@"至少输入6位密码" inView:self.view];
    }
}

#pragma mark - 提交请求
-(void)submitRequestWithPass:(NSString *)pass confirmPwd:(NSString *)confirmPwd{
    [AppUtils showProgressMessage:@"Loading…" inView:mainView];
    NSDictionary *dict=@{
                         @"mod":@"get_password",
                         @"code":@"set_user_password_iso",
                         @"is_iso":@"1",
                         @"phone":_phone,
                         @"reg_verify_code":_verify,
                         @"pwd":pass,
                         @"ckpwd":confirmPwd
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:mainView];
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:mainView];
        }else{
            [AppUtils saveValue:_phone forKey:User_Name];
            [AppUtils showSuccessMessage:@"重设密码成功！"inView:self.navigationController.view];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ResetPassWordSuccess" object:nil];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
