//
//  OrderListController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderListController.h"
#import "OrderListCell.h"
#import "OrderDetailController.h"
#import "IslandDetailController.h"
#import "ApprovedOrderController.h"
#import "PaySuccessController.h"

#import "TestController.h"

#define RefleshTitle_Font 13
@interface OrderListController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation OrderListController{
    
    UIView *btLine;
    UIButton *hadFilmBtn;
    UIButton *willFilmBtn;
    
    UIScrollView *tableScroller;
    NSInteger filmTableIndex;
    
    NSMutableArray *dataArr01;
    MJRefreshNormalHeader *rHeader01;
    UITableView *table01;       //我的订单
    
    NSMutableArray *dataArr02;
    MJRefreshNormalHeader *rHeader02;
    UITableView *table02;       //协同订单
    
    NSString *moreDataStr;
    UILabel *tipLabel;
    
    
    NSMutableArray *rateArr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"我的订单页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"我的订单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"旅游订单";
    [self.view addSubview:navBar];
    
    [self setUpTopBtnForArray:@[@"我的订单",@"协同订单"]];
    
    [self setUpUI];
    
    [self orderListRequest];
    [self getStageRate];
}

#pragma mark - setUp 正在热映筛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    CGFloat btnWidth=SCREENSIZE.width/2;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=64;
    for (int i=0; i<arr.count; i++) {
        NSString *name=arr[i];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i+1;
        btn.frame=CGRectMake(i*btnWidth, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if (i==0) {
            lineX=btn.origin.x;
            hadFilmBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        }else{
            willFilmBtn=btn;
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(0, btnY+btnHeihgt-2.5f, btnWidth, 3.0f)];
    btLine.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:btLine];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 108) endPoint:CGPointMake(SCREENSIZE.width, 108)];
}

#pragma mark - 点击我的订单/协同订单
-(void)clickFBtn:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==1) {
        
        filmTableIndex=1;
//
//        isSelectWillPlay=NO;
        
        [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        hadFilmBtn.enabled=NO;

        [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        willFilmBtn.enabled=YES;

        [self orderListRequest];
    }else{
        
        filmTableIndex=2;
//
//        isSelectWillPlay=YES;
        
        [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        hadFilmBtn.enabled=YES;
        
        [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        willFilmBtn.enabled=NO;
        
        [self orderListRequest];
    }
    
    //设置蓝色横条位置
    [UIView animateWithDuration:0.20 animations:^{
        CGRect frame=btLine.frame;
        if (btn.tag==1) {
            frame.origin.x=0;
        }else{
            frame.origin.x=SCREENSIZE.width/2;
        }
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        [tableScroller setContentOffset:CGPointMake((btn.tag-1)*SCREENSIZE.width, 0) animated:NO];
    }];
}

#pragma mark - init UI
-(void)setUpUI{
    //
    tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 21)];
    tipLabel.text=@"您暂无订单";
    tipLabel.textColor=[UIColor lightGrayColor];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.center=CGPointMake(SCREENSIZE.width/2, (SCREENSIZE.height-113)/2);
//    [self.view addSubview:tipLabel];
    
    //
    
    filmTableIndex=1;
    
    tableScroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108)];
    tableScroller.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    tableScroller.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    tableScroller.bounces=NO;
    tableScroller.showsHorizontalScrollIndicator=NO;
    tableScroller.showsVerticalScrollIndicator=NO;
    tableScroller.scrollEnabled=NO;
    tableScroller.pagingEnabled=NO;
    [self.view addSubview:tableScroller];
    
#pragma mark table01
    dataArr01=[NSMutableArray array];
    table01=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, tableScroller.height) style:UITableViewStyleGrouped];
    table01.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    [table01 registerNib:[UINib nibWithNibName:@"OrderListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    table01.dataSource=self;
    table01.delegate=self;
    table01.sectionFooterHeight = 8.0f;
    [tableScroller addSubview:table01];
    
    //下拉刷新
    rHeader01 = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self orderListRequest];
        });
    }];
    [rHeader01 setMj_h:64];//设置高度
    [rHeader01 setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [rHeader01.stateLabel setMj_y:13];
    rHeader01.stateLabel.font = [UIFont systemFontOfSize:RefleshTitle_Font];
    rHeader01.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:RefleshTitle_Font];
    table01.mj_header = rHeader01;
    
#pragma mark table02
    dataArr02=[NSMutableArray array];
    table02=[[UITableView alloc]initWithFrame:CGRectMake(SCREENSIZE.width, 0, SCREENSIZE.width, tableScroller.height) style:UITableViewStyleGrouped];
    table02.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    [table02 registerNib:[UINib nibWithNibName:@"OrderListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    table02.dataSource=self;
    table02.delegate=self;
    table02.sectionFooterHeight = 8.0f;
    [tableScroller addSubview:table02];
    
    //下拉刷新
    rHeader02 = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self orderListRequest];
        });
    }];
    [rHeader02 setMj_h:64];//设置高度
    [rHeader02 setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [rHeader02.stateLabel setMj_y:13];
    rHeader02.stateLabel.font = [UIFont systemFontOfSize:RefleshTitle_Font];
    rHeader02.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:RefleshTitle_Font];
    table02.mj_header = rHeader02;
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return tableView==table01?dataArr01.count:dataArr02.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
    OrderListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell=[[OrderListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (tableView==table01) {
        if (indexPath.row<dataArr01.count) {
            OrderListModel *model=dataArr01[indexPath.section];
            [cell reflushDataForModel:model];
        }
    }else{
        if (indexPath.row<dataArr02.count) {
            OrderListModel *model=dataArr02[indexPath.section];
            [cell reflushDataForModel:model];
        }
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListModel *model=tableView==table01?dataArr01[indexPath.section]:dataArr02[indexPath.section];
    IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
    islandDetail.islandId=model.mdId;
    islandDetail.mdId=model.mdId;
    islandDetail.islandImg=model.islandImg;
    islandDetail.islandPId=model.islandId;
    islandDetail.islandName=model.islandTitle;
    islandDetail.islandDescript=model.islandDescript;
    if ([model.headerTitle isEqualToString:@"马尔代夫"]) {
        islandDetail.detail_landType=MaldivesIslandType;
    }else if ([model.headerTitle isEqualToString:@"巴厘岛"]){
        islandDetail.detail_landType=BaliIslandType;
    }else{
        islandDetail.detail_landType=PalauIslandType;
    }
    [self.navigationController pushViewController:islandDetail animated:YES];
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    OrderListModel *model=tableView==table01?dataArr01[section]:dataArr02[section];
    if ([model.stageSum isEqualToString:@"1"]) {
        return 75;
    }else{
        return 100;
    }
}

//自定义组尾标题
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    OrderListModel *model;
    if (tableView==table01) {
        if(section<dataArr01.count){
            model=dataArr01[section];
        }
    }else{
        if(section<dataArr02.count){
            model=dataArr02[section];
        }
    }
    
    
    UIView *sectionFootView=[UIView new];
    sectionFootView.backgroundColor=[UIColor colorWithHexString:@"#ffffff"];
    
    //立即付款button
    UIButton *payBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    payBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    payBtn.layer.cornerRadius=2;
    [payBtn setTitle:@"立即付款" forState:UIControlStateNormal];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    payBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    //向button传递对象
    objc_setAssociatedObject(payBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [payBtn addTarget:self action:@selector(clickPayBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:payBtn];
    
    
    //查看详情button
    UIButton *seeDetailBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    seeDetailBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    seeDetailBtn.layer.cornerRadius=2;
    [seeDetailBtn setTitle:@"查看详情" forState:UIControlStateNormal];
    [seeDetailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    seeDetailBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    //向button传递对象
    objc_setAssociatedObject(seeDetailBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [seeDetailBtn addTarget:self action:@selector(clickSeeDetailBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:seeDetailBtn];
    
    
    //付款button
    UIButton *cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.backgroundColor=[UIColor lightGrayColor];
    cancelBtn.layer.cornerRadius=2;
    [cancelBtn setTitle:@"取消订单" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    //向button传递对象
    objc_setAssociatedObject(cancelBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [cancelBtn addTarget:self action:@selector(clickCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:cancelBtn];
    
    //
    //label01
    TTTAttributedLabel *label01=[TTTAttributedLabel new];
    label01.textAlignment=NSTextAlignmentRight;
    label01.textColor=[UIColor colorWithHexString:@"#f36700"];
    label01.font=[UIFont systemFontOfSize:15];
    NSString *label01Str=[NSString stringWithFormat:@"总金额：￥%@元",model.totalShowPrice];
    [label01 setText:label01Str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        return mutableAttributedString;
    }];
    [sectionFootView addSubview:label01];
    
    //
    UILabel *label02=[UILabel new];
    label02.textAlignment=NSTextAlignmentRight;
    label02.textColor=[UIColor colorWithHexString:@"#282828"];
    label02.font=[UIFont systemFontOfSize:15];
    label02.text=[NSString stringWithFormat:@"分%@期（每期%@元，首付%@元）",model.stageSum,model.price_everyStage,model.payMoney];
    [sectionFootView addSubview:label02];
    
    CGFloat btnY=0.0f;
    CGFloat btnWidth=72;
    CGFloat btnHeight=32;
    if ([model.stageSum isEqualToString:@"1"]) {
        btnY=75-41;
//        label01.hidden=YES;
        label02.hidden=YES;
    }else{
        btnY=100-41;
//        label01.hidden=NO;
        label02.hidden=NO;
    }
    
    label01.frame=CGRectMake(8, 7, SCREENSIZE.width-16, 21);
    label02.frame=CGRectMake(label01.origin.x, label01.origin.y+label01.height+2, SCREENSIZE.width-16, label01.height);
    
    if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
        
        if (filmTableIndex==1) {
            //
            payBtn.hidden=NO;
            payBtn.frame=CGRectMake(SCREENSIZE.width-84, btnY, btnWidth, btnHeight);
            //
            seeDetailBtn.hidden=NO;
            seeDetailBtn.frame=CGRectMake(payBtn.origin.x-84, btnY, btnWidth, btnHeight);
            
            //
            cancelBtn.hidden=NO;
            cancelBtn.frame=CGRectMake(seeDetailBtn.origin.x-84, btnY, btnWidth, btnHeight);
        }else{
            cancelBtn.hidden=YES;
            seeDetailBtn.hidden=NO;
            payBtn.hidden=YES;
            seeDetailBtn.frame=CGRectMake(SCREENSIZE.width-84, btnY, btnWidth, btnHeight);
        }
    }else{
        cancelBtn.hidden=YES;
        seeDetailBtn.hidden=NO;
        payBtn.hidden=YES;
        seeDetailBtn.frame=CGRectMake(SCREENSIZE.width-84, btnY, btnWidth, btnHeight);
    }
    
    
    return sectionFootView;
}

#pragma mark - Section Foot ButtonEvent
-(void)clickPayBtn:(id)sender{
    OrderListModel *model=(OrderListModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    if ([model.payStatus isEqualToString:@"1"]) {
        PaySuccessController *paySucce=[[PaySuccessController alloc]init];
        [self.navigationController pushViewController:paySucce animated:YES];
    }else{
        if ([model.kfStatus isEqualToString:@"1"]) {
            ApprovedOrderController *appr=[[ApprovedOrderController alloc]init];
            appr.orderId=model.orderId;
            appr.rateArr=rateArr;
            [self.navigationController pushViewController:appr animated:YES];
        }else{
            
            TestController *orderPay=[[TestController alloc]init];
            orderPay.orderId=model.orderId;
            [self.navigationController pushViewController:orderPay animated:YES];
        }
    }
}

-(void)clickSeeDetailBtn:(id)sender{
    
    //获取button传递过来的对象
    OrderListModel *model=(OrderListModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    
    OrderDetailController *orderDetail=[[OrderDetailController alloc]init];
    orderDetail.orderId=model.orderId;
    orderDetail.isXieTong=filmTableIndex==1?NO:YES;
    orderDetail.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:orderDetail animated:YES];
    
}

-(void)clickCancelBtn:(id)sender{
    OrderListModel *model=(OrderListModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否取消订单"preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        
    }];
    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        [self cancleOrderRequestWithOrderId:model.orderId];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:yesAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - request请求
-(void)orderListRequest{
    [AppUtils showProgressInView:self.view];
    if (dataArr01.count>0) {
        [dataArr01 removeAllObjects];
    }
    if (dataArr02.count>0) {
        [dataArr02 removeAllObjects];
    }
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:filmTableIndex==1?@"order_iso":@"order_xz_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            table01.hidden=NO;
            table02.hidden=NO;
            NSArray *olArr=responseObject[@"retData"][@"order_list"];
            if (olArr.count<=0) {
                [self.view bringSubviewToFront:tipLabel];
            }else{
                [self.view bringSubviewToFront:table01];
            }
            
            if (filmTableIndex==1) {
                for (NSDictionary *dic in olArr) {
                    OrderListModel *model=[[OrderListModel alloc]init];
                    [model jsonDataForDictioanry:dic];
                    [dataArr01 addObject:model];
                }
                [table01 reloadData];
                [table01.mj_header endRefreshing];
            }else{
                for (NSDictionary *dic in olArr) {
                    OrderListModel *model=[[OrderListModel alloc]init];
                    [model jsonDataForDictioanry:dic];
                    [dataArr02 addObject:model];
                }
                [table02 reloadData];
                [table02.mj_header endRefreshing];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//取消订单
-(void)cancleOrderRequestWithOrderId:(NSString *)orderId{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"cancel_iso" forKey:@"code"];
    [dict setObject:orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
//        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self cancleOrderRequestWithOrderId:orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [self orderListRequest];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

//获取费率
-(void)getStageRate{
    rateArr=[NSMutableArray array];
    [[NetWorkRequest defaultClient] requestWithPath:RateUrl method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"费率%@---",responseObject);
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"];
            for (NSDictionary *dic in arr) {
                NSString *rateStr=dic[@"feilv"];
                [rateArr addObject:rateStr];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
