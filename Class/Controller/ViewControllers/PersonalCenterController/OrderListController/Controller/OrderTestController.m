//
//  OrderTestController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderTestController.h"
#import "OrderTestCell.h"
#import "OrderDetailController.h"
#import "IslandDetailController.h"
#import "ApprovedOrderController.h"
#import "PaySuccessController.h"
#import "NewTravelPlanController.h"
#import "TestController.h"
#import <UIView+WZLBadge.h>

#define SeleColor @"#0080c5"
#define DefaultColor @"#656565"
#define grateColor @"#a0a0a0"
@interface OrderTestController ()<UITableViewDataSource,UITableViewDelegate,OrderListLeftButtonDelegate,OrderListRightButtonDelegate>

@end

@implementation OrderTestController{
    CGFloat btnWidth;
    UIButton *waitPayBtn;
    UIButton *seleFilterBtn;
    UIView *btLine;
    NSInteger numOfWaitPay;     //等待付款数；
    
    BOOL isSelectWaitPay;
    
    NSMutableArray *allListArr;
    NSMutableArray *waitPayArr;
    NSMutableArray *hadPayArr;
    NSMutableArray *hadCancelArr;
    NSMutableArray *dataArr;
    UITableView *myTable;
    NSString *moreDataStr;
    UILabel *tipLabel;
    
    NSMutableArray *rateArr;
    
    MJRefreshAutoNormalFooter *footer;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"我的订单页"];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"我的订单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"我的订单";
    [self.view addSubview:navBar];
    
    [self setUpUI];
    [self orderListRequest];
    [self getStageRate];
}


#pragma mark - init UI
-(void)setUpUI{
    
    [self setUpTopBtnForArray:@[@"全部订单",@"待付款",@"已付款",@"已取消"]];
    
    allListArr=[NSMutableArray array];
    waitPayArr=[NSMutableArray array];
    hadPayArr=[NSMutableArray array];
    hadCancelArr=[NSMutableArray array];
    dataArr=[NSMutableArray array];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-157) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#f9f9f9"];
    [self.view addSubview:myTable];
    
    //
    tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 21)];
    tipLabel.text=@"暂无数据";
    tipLabel.textColor=[UIColor lightGrayColor];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=kFontSize16;
    tipLabel.center=CGPointMake(SCREENSIZE.width/2, (myTable.height+myTable.origin.y)/2);
    tipLabel.hidden=YES;
    [self.view insertSubview:tipLabel aboveSubview:myTable];
    

    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = kFontSize14;
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (![moreDataStr isEqualToString:@""]) {
        //        [self requestHttpsMoreProduct:moreDataStr];
    }
    [myTable.mj_footer endRefreshing];
}

#pragma mark - setUp 筛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    
    btnWidth=SCREENSIZE.width/arr.count;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=64;
    for (int i=0; i<arr.count; i++) {
        
        NSString *name=arr[i];
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i;
        btn.frame=CGRectMake(i*btnWidth, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=kFontSize15;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:DefaultColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if (i==0) {
            lineX=btn.origin.x;
            seleFilterBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
            
            waitPayBtn=btn;
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(lineX, btnY+btnHeihgt-2.0f, btnWidth, 2.5f)];
    btLine.backgroundColor=[UIColor colorWithHexString:SeleColor];
    [self.view addSubview:btLine];
}

#pragma mark - 点击筛选按钮
-(void)clickFBtn:(id)sender{
    
    [seleFilterBtn setTitleColor:[UIColor colorWithHexString:DefaultColor] forState:UIControlStateNormal];
    seleFilterBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
        {
            isSelectWaitPay=NO;
            dataArr=allListArr;
        }
            break;
        case 1:
        {
            isSelectWaitPay=YES;
            [self getWaitPayArray:allListArr];
        }
            break;
        case 2:
        {
            isSelectWaitPay=NO;
            [self getHadPayArray:allListArr];
        }
            break;
            
        default:
        {
            isSelectWaitPay=NO;
            [self getHadCancelArray:allListArr];
        }
            break;
    }
    [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    //设置蓝色横条位置
    CGRect frame=btLine.frame;
    frame.origin.x=btn.origin.x;
    [UIView animateWithDuration:0.20 animations:^{
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        waitPayBtn.badgeCenterOffset = CGPointMake(btnWidth-21, 14);
        waitPayBtn.badgeBgColor=[UIColor colorWithHexString:@"#f36700"];
        [waitPayBtn showBadgeWithStyle:WBadgeStyleNumber value:numOfWaitPay animationType:WBadgeAnimTypeNone];
        [myTable reloadData];
        if (dataArr.count<=0) {
            tipLabel.hidden=NO;
        }else{
            tipLabel.hidden=YES;
        }
    }];
    
    //用一个button代替当前选择的
    seleFilterBtn=btn;
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREENSIZE.width/2+20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
    OrderTestCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell=[[OrderTestCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (indexPath.row<dataArr.count) {
        OrderListModel *model=dataArr[indexPath.section];
        [cell reflushDataForModel:model];
        cell.model=model;
        cell.leftButtonDelegate=self;
        cell.rightButtonDelegate=self;
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderListModel *model=dataArr[indexPath.section];
    IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
    islandDetail.islandId=model.mdId;
    islandDetail.mdId=model.mdId;
    islandDetail.islandImg=model.islandImg;
    islandDetail.islandPId=model.islandId;
    islandDetail.islandName=model.islandTitle;
    islandDetail.islandDescript=model.islandDescript;
    if ([model.headerTitle isEqualToString:@"马尔代夫"]) {
        islandDetail.detail_landType=MaldivesIslandType;
    }else if ([model.headerTitle isEqualToString:@"巴厘岛"]){
        islandDetail.detail_landType=BaliIslandType;
    }else{
        islandDetail.detail_landType=PalauIslandType;
    }
    [self.navigationController pushViewController:islandDetail animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 22;
    }
    return 0.0000000001;
}


#pragma mark - Section Foot ButtonEvent
-(void)clickPayBtn:(id)sender{
    OrderListModel *model=(OrderListModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    if ([model.payStatus isEqualToString:@"1"]) {
        PaySuccessController *paySucce=[[PaySuccessController alloc]init];
        [self.navigationController pushViewController:paySucce animated:YES];
    }else{
        if ([model.kfStatus isEqualToString:@"1"]) {
            ApprovedOrderController *appr=[[ApprovedOrderController alloc]init];
            appr.orderId=model.orderId;
            appr.rateArr=rateArr;
            [self.navigationController pushViewController:appr animated:YES];
        }else{
            //            OrderPayController *orderPay=[[OrderPayController alloc]init];
            //            orderPay.orderId=model.orderId;
            //            [self.navigationController pushViewController:orderPay animated:YES];
            
            TestController *orderPay=[[TestController alloc]init];
            orderPay.rateArr=rateArr;
            orderPay.orderId=model.orderId;
            [self.navigationController pushViewController:orderPay animated:YES];
        }
    }
    
}

-(void)clickSeeDetailBtn:(id)sender{
    
    //获取button传递过来的对象
    OrderListModel *model=(OrderListModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    
    OrderDetailController *orderDetail=[[OrderDetailController alloc]init];
    orderDetail.orderId=model.orderId;
    orderDetail.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:orderDetail animated:YES];
    
}

-(void)clickCancelBtn:(id)sender{
    
    
}

#pragma mark - 订单下面两个按钮 点击
-(void)clickOrderLeftButtonWithModel:(OrderListModel *)model{
    if ([model.orderStatus isEqualToString:@"1"]) {
        if ([model.orderType isEqualToString:@"prize"]) {
            //payStatusStr=@"";
        }else{
            if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                //payStatusStr=@" 等待付款";
                //rightBtnTitle=@"立即支付";
                //leftBtnTitle=@"取消订单";
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否取消订单"preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                    
                }];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    [self cancleOrderRequestWithOrderId:model.orderId];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                //payStatusStr=@"等待卖家发货";
            }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                //payStatusStr=@"已发货";
            }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                //payStatusStr=@"交易完成";
            }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                //payStatusStr=@"已创建";
            }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                //payStatusStr=@"已付款";
                //rightBtnTitle=@"申请退款";
                //leftBtnTitle=@"订单详情";
            }
        }
    }else{
        if ([model.orderStatus isEqualToString:@"0"]) {
            //payStatusStr=@"已取消";
            //rightBtnTitle=@"重新购买";
            //leftBtnTitle=@"删除订单";
        }else if ([model.orderStatus isEqualToString:@"2"]){
            //payStatusStr=@"已过期";
        }else if ([model.orderStatus isEqualToString:@"3"]){
            //payStatusStr=@"失败";
        }else if ([model.orderStatus isEqualToString:@"4"]){
            //payStatusStr=@"已经退款";
        }else if ([model.orderStatus isEqualToString:@"5"]){
            //payStatusStr=@"已申请退款";
        }
    }
    DSLog(@"%@",model.orderId);
}

-(void)clickOrderRightButtonWithModel:(OrderListModel *)model{
    if ([model.orderStatus isEqualToString:@"1"]) {
        if ([model.orderType isEqualToString:@"prize"]) {
            //payStatusStr=@"";
        }else{
            if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                //payStatusStr=@" 等待付款";
                //rightBtnTitle=@"立即支付";
                //leftBtnTitle=@"取消订单";
                if ([model.kfStatus isEqualToString:@"1"]) {
                    ApprovedOrderController *appr=[[ApprovedOrderController alloc]init];
                    appr.orderId=model.orderId;
                    appr.rateArr=rateArr;
                    [self.navigationController pushViewController:appr animated:YES];
                }else{
                    //            OrderPayController *orderPay=[[OrderPayController alloc]init];
                    //            orderPay.orderId=model.orderId;
                    //            [self.navigationController pushViewController:orderPay animated:YES];
                    
                    TestController *orderPay=[[TestController alloc]init];
                    orderPay.rateArr=rateArr;
                    orderPay.orderId=model.orderId;
                    [self.navigationController pushViewController:orderPay animated:YES];
                }
            }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                //payStatusStr=@"等待卖家发货";
            }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                //payStatusStr=@"已发货";
            }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                //payStatusStr=@"交易完成";
            }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                //payStatusStr=@"已创建";
            }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                //payStatusStr=@"已付款";
                //rightBtnTitle=@"申请退款";
                //leftBtnTitle=@"订单详情";
            }
        }
    }else{
        if ([model.orderStatus isEqualToString:@"0"]) {
            //payStatusStr=@"已取消";
            //rightBtnTitle=@"重新购买";
            //leftBtnTitle=@"删除订单";
            NewTravelPlanController *travelPlan=[[NewTravelPlanController alloc]init];
            travelPlan.pId=model.islandId;
            travelPlan.mdId=model .mdId;
            travelPlan.islandId=model.islandId;
            travelPlan.islandName=model.islandTitle;
            [self.navigationController pushViewController:travelPlan animated:YES];
        }else if ([model.orderStatus isEqualToString:@"2"]){
            //payStatusStr=@"已过期";
        }else if ([model.orderStatus isEqualToString:@"3"]){
            //payStatusStr=@"失败";
        }else if ([model.orderStatus isEqualToString:@"4"]){
            //payStatusStr=@"已经退款";
        }else if ([model.orderStatus isEqualToString:@"5"]){
            //payStatusStr=@"已申请退款";
        }
    }
}

#pragma mark - request请求

//获取费率
-(void)getStageRate{
    rateArr=[NSMutableArray array];
    [[NetWorkRequest defaultClient] requestWithPath:RateUrl method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"];
            for (NSDictionary *dic in arr) {
                NSString *rateStr=dic[@"feilv"];
                [rateArr addObject:rateStr];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)orderListRequest{
    
    [AppUtils showProgressMessage:@"Loading…" inView:myTable];
    if (allListArr.count>0) {
        [allListArr removeAllObjects];
    }
    dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:@"order_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:myTable];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            numOfWaitPay=0;
            myTable.hidden=NO;
            NSArray *olArr=responseObject[@"retData"][@"order_list"];
            for (NSDictionary *dic in olArr) {
                OrderListModel *model=[[OrderListModel alloc]init];
                [model jsonDataForDictioanry:dic];
                numOfWaitPay=numOfWaitPay+[self numOfWaitPayWithModel:model];
                [allListArr addObject:model];
            }
            if (isSelectWaitPay) {
                [self getWaitPayArray:allListArr];
            }else{
                dataArr=allListArr;
            }
            if (dataArr.count<=0) {
                tipLabel.hidden=NO;
            }else{
                tipLabel.hidden=YES;
            }
            waitPayBtn.badgeCenterOffset = CGPointMake(btnWidth-21, 14);
            waitPayBtn.badgeBgColor=[UIColor colorWithHexString:@"#f36700"];
            [waitPayBtn showBadgeWithStyle:WBadgeStyleNumber value:numOfWaitPay animationType:WBadgeAnimTypeNone];
            [myTable reloadData];
            
            //
            if (olArr.count!=0) {
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
            [myTable.mj_header endRefreshing];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//取消订单
-(void)cancleOrderRequestWithOrderId:(NSString *)orderId{
    [AppUtils showProgressMessage:@"Loading…" inView:myTable];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"cancel_iso" forKey:@"code"];
    [dict setObject:orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:myTable];
        //        DSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self cancleOrderRequestWithOrderId:orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [self orderListRequest];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}



-(NSInteger)numOfWaitPayWithModel:(OrderListModel *)model{
    NSInteger wpNum=0;
    if ([model.orderStatus isEqualToString:@"1"]) {
        if ([model.orderType isEqualToString:@"prize"]) {
            
        }else{
            if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                //@" 等待付款";
                //@"立即支付";
                //@"取消订单";
                wpNum=1;
            }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                //@"等待卖家发货";
            }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                //@"已发货";
            }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                //@"交易完成";
            }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                //@"已创建";
            }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                //@"已付款";
                //@"申请退款";
                //@"订单详情";
            }
        }
    }else{
        if ([model.orderStatus isEqualToString:@"0"]) {
            //@"已取消";
            //@"重新购买";
            //@"删除订单";
            wpNum=0;
        }else if ([model.orderStatus isEqualToString:@"2"]){
            //@"已过期";
        }else if ([model.orderStatus isEqualToString:@"3"]){
            //@"失败";
        }else if ([model.orderStatus isEqualToString:@"4"]){
            //@"已经退款";
        }else if ([model.orderStatus isEqualToString:@"5"]){
            //@"已申请退款";
        }
    }
    return wpNum;
}

-(void)getWaitPayArray:(NSMutableArray *)array{
    [waitPayArr removeAllObjects];
    for (OrderListModel *model in array) {
        if ([model.orderStatus isEqualToString:@"1"]) {
            if ([model.orderType isEqualToString:@"prize"]) {
                
            }else{
                if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                    //@" 等待付款";
                    //@"立即支付";
                    //@"取消订单";
                    [waitPayArr addObject:model];
                }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                    //@"等待卖家发货";
                }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                    //@"已发货";
                }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                    //@"交易完成";
                }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                    //@"已创建";
                }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                    //@"已付款";
                    //@"申请退款";
                    //@"订单详情";
                }
            }
        }else{
            if ([model.orderStatus isEqualToString:@"0"]) {
                //@"已取消";
                //@"重新购买";
                //@"删除订单";
            }else if ([model.orderStatus isEqualToString:@"2"]){
                //@"已过期";
            }else if ([model.orderStatus isEqualToString:@"3"]){
                //@"失败";
            }else if ([model.orderStatus isEqualToString:@"4"]){
                //@"已经退款";
            }else if ([model.orderStatus isEqualToString:@"5"]){
                //@"已申请退款";
            }
        }
    }
    dataArr=waitPayArr;
}

-(void)getHadPayArray:(NSMutableArray *)array{
    [hadPayArr removeAllObjects];
    for (OrderListModel *model in array) {
        if ([model.orderStatus isEqualToString:@"1"]) {
            if ([model.orderType isEqualToString:@"prize"]) {
                
            }else{
                if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                    //@" 等待付款";
                    //@"立即支付";
                    //@"取消订单";
                }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                    //@"等待卖家发货";
                }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                    //@"已发货";
                }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                    //@"交易完成";
                }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                    //@"已创建";
                }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                    //@"已付款";
                    //@"申请退款";
                    //@"订单详情";
                    [hadPayArr addObject:model];
                }
            }
        }else{
            if ([model.orderStatus isEqualToString:@"0"]) {
                //@"已取消";
                //@"重新购买";
                //@"删除订单";
            }else if ([model.orderStatus isEqualToString:@"2"]){
                //@"已过期";
            }else if ([model.orderStatus isEqualToString:@"3"]){
                //@"失败";
            }else if ([model.orderStatus isEqualToString:@"4"]){
                //@"已经退款";
            }else if ([model.orderStatus isEqualToString:@"5"]){
                //@"已申请退款";
            }
        }
    }
    dataArr=hadPayArr;
}

-(void)getHadCancelArray:(NSMutableArray *)array{
    [hadCancelArr removeAllObjects];
    for (OrderListModel *model in array) {
        if ([model.orderStatus isEqualToString:@"1"]) {
            if ([model.orderType isEqualToString:@"prize"]) {
                
            }else{
                if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                    //@" 等待付款";
                    //@"立即支付";
                    //@"取消订单";
                }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                    //@"等待卖家发货";
                }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                    //@"已发货";
                }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                    //@"交易完成";
                }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                    //@"已创建";
                }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                    //@"已付款";
                    //@"申请退款";
                    //@"订单详情";
                }
            }
        }else{
            if ([model.orderStatus isEqualToString:@"0"]) {
                //@"已取消";
                //@"重新购买";
                //@"删除订单";
                [hadCancelArr addObject:model];
            }else if ([model.orderStatus isEqualToString:@"2"]){
                //@"已过期";
            }else if ([model.orderStatus isEqualToString:@"3"]){
                //@"失败";
            }else if ([model.orderStatus isEqualToString:@"4"]){
                //@"已经退款";
            }else if ([model.orderStatus isEqualToString:@"5"]){
                //@"已申请退款";
            }
        }
    }
    dataArr=hadCancelArr;
}

@end
