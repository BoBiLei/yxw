//
//  OrderListModel.m
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderListModel.h"

@implementation OrderListModel

-(void)jsonDataForDictioanry:(NSDictionary *)dic{
    self.orderId=dic[@"orderid"];
    self.orderTime=dic[@"buytime"];
    self.headerTitle=dic[@"product"][@"category"];
    self.orderStatus=dic[@"status"];
    self.orderType=dic[@"product"][@"type"];
    self.orderProcess=dic[@"process"];
    self.mdId=dic[@"mdid"];
    self.islandId=dic[@"productid"];
    self.islandImg=dic[@"hsrc"];
    
    
    self.islandDescript=dic[@"description"];
    
    //
    self.islandTitle=dic[@"product"][@"name"];
    NSMutableString *titleStr=[NSMutableString stringWithString:_islandTitle];
    NSRange titRang=[titleStr rangeOfString:@"&amp;"];
    while (titRang.location!=NSNotFound) {
        [titleStr replaceCharactersInRange:titRang withString:@" "];
        titRang=[titleStr rangeOfString:@"&amp;"];
    }
    self.islandTitle=titleStr;
    
    //
    //
    self.stagePrice=dic[@"fenqi_price"];
    NSRange stpRang=[_stagePrice rangeOfString:@"."];
    if (stpRang.location!=NSNotFound) {
        _stagePrice=[_stagePrice substringToIndex:stpRang.location];
    }
    self.totalPrice=dic[@"totalprice"];
    self.couponPrice=dic[@"paycashcard"];
    self.yxcredit_price=dic[@"yxcredit"];
    //
    CGFloat vipPriceFloat=0.0f;
    CGFloat totalPirceFloat=self.totalPrice.floatValue;
    CGFloat couponFeeFloat=self.couponPrice.floatValue;
    CGFloat stageFeeFloat=self.stagePrice.floatValue;
    CGFloat totalShowPrice=0.0f;    //要显示的总计金额
    
    
    if ([_yxcredit_price isEqualToString:@"0"]) {
        totalShowPrice=totalPirceFloat-couponFeeFloat+stageFeeFloat-vipPriceFloat;
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }else if ([_yxcredit_price isEqualToString:@"1"]){
        totalShowPrice=(totalPirceFloat-couponFeeFloat)*0.98;
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }else{
        totalShowPrice=totalPirceFloat-couponFeeFloat;
        if (totalShowPrice>=50000) {
            totalShowPrice=totalShowPrice-3000;
        }else if (totalShowPrice>=30000){
            totalShowPrice=totalShowPrice-1000;
        }else if (totalShowPrice>=10000){
            totalShowPrice=totalShowPrice-300;
        }
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }
    
    self.stageSum=dic[@"stage"];
    
    //
    _price_everyStage=dic[@"per_price"];
    NSRange everyStageRang=[_price_everyStage rangeOfString:@"."];
    if (everyStageRang.location!=NSNotFound) {
        _price_everyStage=[_price_everyStage substringToIndex:everyStageRang.location];
    }
    
    self.payMoney=dic[@"paymoney"];
    NSRange pmRang=[_payMoney rangeOfString:@"."];
    if (pmRang.location!=NSNotFound) {
        _payMoney=[_payMoney substringToIndex:pmRang.location];
    }
    
    self.payStatus=dic[@"pay"];
    self.kfStatus=dic[@"is_kf_status"];
}

-(void)jsonApproveDataForDictionary:(NSDictionary *)dic{
    self.cardType=dic[@"order"][@"card_type"];
    self.orderId=dic[@"order"][@"orderid"];
    
    //
    self.islandTitle=dic[@"order"][@"product_data"][@"product"][@"name"];
    NSMutableString *titleStr=[NSMutableString stringWithString:_islandTitle];
    NSRange titRang=[titleStr rangeOfString:@"&amp;"];
    while (titRang.location!=NSNotFound) {
        [titleStr replaceCharactersInRange:titRang withString:@" "];
        titRang=[titleStr rangeOfString:@"&amp;"];
    }
    self.islandTitle=titleStr;
    
    //
    self.roomType=dic[@"order"][@"product_data"][@"rooms"][@"product_name"];
    NSMutableString *roomStr=[NSMutableString stringWithString:_roomType];
    NSRange roomRang=[roomStr rangeOfString:@"&nbsp;"];
    while (roomRang.location!=NSNotFound) {
        [roomStr replaceCharactersInRange:roomRang withString:@" "];
        roomRang=[roomStr rangeOfString:@"&nbsp;"];
    }
    self.roomType=roomStr;
    
    //
    self.flightInfo=dic[@"order"][@"product_data"][@"flight"][@"product_name"];
    self.kfUrl=dic[@"lxkf"];
    
    //
    self.stageSum=dic[@"order"][@"stage"];
    self.stageFee=dic[@"order"][@"stage"];
    
    //
    self.stagePrice=dic[@"order"][@"fenqi_price"];
    NSRange stpRang=[_stagePrice rangeOfString:@"."];
    if (stpRang.location!=NSNotFound) {
        _stagePrice=[_stagePrice substringToIndex:stpRang.location];
    }
    self.totalPrice=dic[@"order"][@"totalprice"];
    self.couponPrice=dic[@"order"][@"paycashcard"];
    self.yxcredit_price=dic[@"order"][@"yxcredit"];
    
    //
    self.payMoney=dic[@"order"][@"paymoney"];
    NSRange pmRang=[_payMoney rangeOfString:@"."];
    if (pmRang.location!=NSNotFound) {
        _payMoney=[_payMoney substringToIndex:pmRang.location];
    }
    
    //
    CGFloat vipPriceFloat=0.0f;
    CGFloat totalPirceFloat=self.totalPrice.floatValue;
    CGFloat couponFeeFloat=self.couponPrice.floatValue;
    CGFloat stageFeeFloat=self.stagePrice.floatValue;
    CGFloat totalShowPrice=0.0f;    //要显示的总计金额
    
    
    if ([_yxcredit_price isEqualToString:@"0"]) {
        totalShowPrice=totalPirceFloat-couponFeeFloat+stageFeeFloat-vipPriceFloat;
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }else if ([_yxcredit_price isEqualToString:@"1"]){
        totalShowPrice=(totalPirceFloat-couponFeeFloat)*0.98;
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }else{
        totalShowPrice=totalPirceFloat-couponFeeFloat;
        if (totalShowPrice>=50000) {
            totalShowPrice=totalShowPrice-3000;
        }else if (totalShowPrice>=30000){
            totalShowPrice=totalShowPrice-1000;
        }else if (totalShowPrice>=10000){
            totalShowPrice=totalShowPrice-300;
        }
        _totalShowPrice=[NSString stringWithFormat:@"%f",totalShowPrice];
        NSRange rang=[_totalShowPrice rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            _totalShowPrice=[_totalShowPrice substringToIndex:rang.location];
        }
    }
    
    NSString *everyStageStr=dic[@"order"][@"per_price"];
    NSRange everyStageRang=[everyStageStr rangeOfString:@"."];
    if (everyStageRang.location!=NSNotFound) {
        everyStageStr=[everyStageStr substringToIndex:everyStageRang.location];
    }
    _price_everyStage=everyStageStr;
    NSRange evepRang=[_price_everyStage rangeOfString:@"."];
    if (evepRang.location!=NSNotFound) {
        _price_everyStage=[_price_everyStage substringToIndex:evepRang.location];
    }
}

@end
