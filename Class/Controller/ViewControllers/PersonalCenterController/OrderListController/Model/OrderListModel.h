//
//  OrderListModel.h
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderListModel : NSObject

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *headerTitle;
@property (nonatomic, copy) NSString *orderStatus;
@property (nonatomic, copy) NSString *orderType;
@property (nonatomic, copy) NSString *orderProcess;
@property (nonatomic, copy) NSString *mdId;
@property (nonatomic, copy) NSString *islandId;
@property (nonatomic, copy) NSString *islandImg;
@property (nonatomic, copy) NSString *islandTitle;
@property (nonatomic, copy) NSString *islandDescript;
@property (nonatomic, copy) NSString *orderPrice;
@property (nonatomic, copy) NSString *orderTime;

//
@property (nonatomic, copy) NSString *cardType;        //
@property (nonatomic, copy) NSString *kfUrl;        //联系客服URL
@property (nonatomic, copy) NSString *payStatus;    //付款状态
@property (nonatomic, copy) NSString *kfStatus;     //客服状态
@property (nonatomic, copy) NSString *roomType;     //酒店房型
@property (nonatomic, copy) NSString *flightInfo;   //机票信息
@property (nonatomic, copy) NSString *stageSum;     //分期数
@property (nonatomic, copy) NSString *stageFee;     //分期率
@property (nonatomic, copy) NSString *stagePrice;   //分期金额
@property (nonatomic, copy) NSString *totalPrice;   //
@property (nonatomic, copy) NSString *couponPrice;  //优惠金额
@property (nonatomic, copy) NSString *yxcredit_price;  //VIP信用

@property (nonatomic, copy) NSString *totalShowPrice;  //显示总计金额
@property (nonatomic, copy) NSString *price_everyStage; //每期应付

@property (nonatomic, copy) NSString *payMoney;         //应付金额

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

//客服核定
-(void)jsonApproveDataForDictionary:(NSDictionary *)dic;

@end
