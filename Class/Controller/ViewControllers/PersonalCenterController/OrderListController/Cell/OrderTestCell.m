//
//  OrderListCell.m
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderTestCell.h"

#define ButtonWidth 74
#define ButtonHeight 34
@implementation OrderTestCell{
    UIImageView *bgImgv;        //背景图片
    UILabel *orderNum;          //订单号
    UIView *centerView;
    UILabel *payStatusLabel;    //支付状态
    UIImageView *islandImgv;    //岛屿图片
    UILabel *islandTitle;       //标题
    TTTAttributedLabel *sfPriceLabel;      //实付金额
    UIButton *leftBtn;
    UIButton *rightBtn;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //背景图片
        bgImgv=[UIImageView new];
        bgImgv.userInteractionEnabled=YES;
        bgImgv.image=[UIImage imageNamed:@"orderList_cell_bg"];
        
        //订单号
        orderNum=[UILabel new];
        orderNum.textColor=[UIColor grayColor];
        orderNum.font=kFontSize14;
        orderNum.numberOfLines=0;
        orderNum.adjustsFontSizeToFitWidth=YES;
        
        //付款状态
        payStatusLabel=[UILabel new];
        payStatusLabel.textAlignment=NSTextAlignmentRight;
        payStatusLabel.textColor=[UIColor colorWithHexString:@"#f36700"];
        payStatusLabel.font=kFontSize14;
        
        centerView=[UIView new];
        centerView.backgroundColor=[UIColor colorWithHexString:@"#f9f9f9"];
        
        //海岛图片
        islandImgv=[UIImageView new];
        
        //标题
        islandTitle=[UILabel new];
        islandTitle.font=kFontSize15;
        islandTitle.numberOfLines=0;
        
        //分期价
        sfPriceLabel=[TTTAttributedLabel new];
        sfPriceLabel.textAlignment=NSTextAlignmentLeft;
        sfPriceLabel.font=kFontSize14;
        sfPriceLabel.numberOfLines=0;
        
        leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        leftBtn.titleLabel.font=kFontSize15;
        leftBtn.layer.cornerRadius=3;
        leftBtn.layer.borderWidth=0.6f;
        leftBtn.layer.borderColor=[UIColor colorWithHexString:@"#858585"].CGColor;
        [leftBtn addTarget:self action:@selector(clickLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.titleLabel.font=kFontSize15;
        rightBtn.layer.cornerRadius=3;
        [rightBtn addTarget:self action:@selector(clickRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    bgImgv.frame=CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width/2+20);
    [self.contentView addSubview:bgImgv];
    
    orderNum.frame=CGRectMake(12, 4, SCREENSIZE.width/2+40, 30);
    [bgImgv addSubview:orderNum];
    
    payStatusLabel.frame=CGRectMake(SCREENSIZE.width-92, orderNum.origin.y, 80, orderNum.height);
    [bgImgv addSubview:payStatusLabel];
    
    centerView.frame=CGRectMake(0, orderNum.origin.y*2+orderNum.height, SCREENSIZE.width, bgImgv.height/2+8);
    [bgImgv addSubview:centerView];
    
    CGFloat islImgHeight=centerView.height-20;
    islandImgv.frame=CGRectMake(12, 10, islImgHeight+32, islImgHeight);
    [centerView addSubview:islandImgv];
    
    islandTitle.frame=CGRectMake(islandImgv.origin.x+islandImgv.width+12, islandImgv.origin.y, SCREENSIZE.width-(islandImgv.origin.x+islandImgv.width+24), 42);
    NSString *titStr=_model.islandTitle;
    CGSize titSize=[AppUtils getStringSize:titStr withFont:15];
    CGRect titLabelFrame=islandTitle.frame;
    CGFloat titLabelY=titLabelFrame.origin.y;
    CGFloat titLabelH=0.0f;
    
    if (titSize.width<SCREENSIZE.width-(56+(SCREENSIZE.width/2+20)/2)) {
        titLabelH=21;
        titLabelFrame.origin.y=titLabelY+3;
        titLabelFrame.size.height=titLabelH;
        islandTitle.frame=titLabelFrame;
    }
    [centerView addSubview:islandTitle];
    
    CGFloat btnWidth=76;
    CGFloat btnViewHeight=bgImgv.height-(centerView.origin.y+centerView.height);
    
    
    rightBtn.frame=CGRectMake(0, 0, btnWidth, btnViewHeight/2+3);
    rightBtn.center=CGPointMake(bgImgv.width-(12+btnWidth/2), centerView.origin.y+centerView.height+btnViewHeight/2-2);
    [bgImgv addSubview:rightBtn];
    
    leftBtn.frame=CGRectMake(0, 0, rightBtn.width, rightBtn.height);
    leftBtn.center=CGPointMake(rightBtn.center.x-rightBtn.width-12, rightBtn.center.y);
    [bgImgv addSubview:leftBtn];
    
    sfPriceLabel.frame=CGRectMake(12, leftBtn.origin.y, bgImgv.width/2, leftBtn.height);
    [bgImgv addSubview:sfPriceLabel];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)reflushDataForModel:(OrderListModel *)model{
    
    objc_setAssociatedObject(leftBtn, "leftBtnModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(rightBtn, "rightBtnModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    _orderId=model.orderId;
    
    NSString *payStatusStr;
    
    NSString *rightBtnTitle;
    UIColor *rightBtnBackGround;
    UIColor *rightBtnTitleColor;
    
    NSString *leftBtnTitle;
    UIColor *leftBtnBackGround=[UIColor whiteColor];
    UIColor *leftBtnTitleColor=[UIColor colorWithHexString:@"#858585"];
    if ([model.orderStatus isEqualToString:@"1"]) {
        if ([model.orderType isEqualToString:@"prize"]) {
            payStatusStr=@"";
        }else{
            
            islandTitle.textColor=[UIColor colorWithHexString:@"#484848"];
            orderNum.textColor=[UIColor colorWithHexString:@"#484848"];
            sfPriceLabel.textColor=[UIColor colorWithHexString:@"#f36700"];
            
            
            if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                
                payStatusStr=@" 等待付款";
                
                rightBtnTitle=@"立即支付";
                rightBtnBackGround=[UIColor colorWithHexString:@"#f36700"];
                rightBtnTitleColor=[UIColor whiteColor];
                
                leftBtnTitle=@"取消订单";
                
                payStatusLabel.textColor=[UIColor colorWithHexString:@"#f36700"];
                
            }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                payStatusStr=@"等待卖家发货";
            }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                payStatusStr=@"已发货";
            }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                
                payStatusStr=@"交易完成";
                
                rightBtnTitle=@"申请退款";
                rightBtnBackGround=[UIColor whiteColor];
                rightBtnTitleColor=[UIColor colorWithHexString:@"#858585"];
                
                leftBtnTitle=@"订单详情";
                rightBtn.layer.borderWidth=0.6f;
                rightBtn.layer.borderColor=[UIColor colorWithHexString:@"#858585"].CGColor;
            }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                payStatusStr=@"已创建";
            }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                
                payStatusStr=@"已付款";
                
                rightBtnTitle=@"申请退款";
                rightBtnBackGround=[UIColor whiteColor];
                rightBtnTitleColor=[UIColor colorWithHexString:@"#858585"];
                
                leftBtnTitle=@"订单详情";
            }
        }
    }else{
        islandTitle.textColor=[UIColor lightGrayColor];
        orderNum.textColor=[UIColor lightGrayColor];
        sfPriceLabel.textColor=[UIColor lightGrayColor];
        payStatusLabel.textColor=[UIColor lightGrayColor];
        
        rightBtn.layer.borderWidth=0;
        rightBtn.layer.borderColor=[UIColor clearColor].CGColor;
        if ([model.orderStatus isEqualToString:@"0"]) {
            
            payStatusStr=@"已取消";
            
            rightBtnTitle=@"重新购买";
            rightBtnBackGround=[UIColor colorWithHexString:@"#f36700"];
            rightBtnTitleColor=[UIColor whiteColor];
            
            leftBtnTitle=@"删除订单";
            
        }else if ([model.orderStatus isEqualToString:@"2"]){
            payStatusStr=@"已过期";
        }else if ([model.orderStatus isEqualToString:@"3"]){
            payStatusStr=@"失败";
        }else if ([model.orderStatus isEqualToString:@"4"]){
            payStatusStr=@"已经退款";
        }else if ([model.orderStatus isEqualToString:@"5"]){
            payStatusStr=@"已申请退款";
        }
    }
    payStatusLabel.text=payStatusStr;
    
    rightBtn.backgroundColor=rightBtnBackGround;
    [rightBtn setTitleColor:rightBtnTitleColor forState:UIControlStateNormal];
    [rightBtn setTitle:rightBtnTitle forState:UIControlStateNormal];
    
    leftBtn.backgroundColor=leftBtnBackGround;
    [leftBtn setTitleColor:leftBtnTitleColor forState:UIControlStateNormal];
    [leftBtn setTitle:leftBtnTitle forState:UIControlStateNormal];
    
    //
    
    NSString *titStr=model.islandTitle;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titStr];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:1.3];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titStr length])];
    islandTitle.attributedText = attributedString;
    
    
    orderNum.text=[NSString stringWithFormat:@"订单编号:%@",model.orderId];
    
    //
    NSString *valiStr = [NSString stringWithFormat:@"实付金额:￥%@元",model.stagePrice];
    [sfPriceLabel setText:valiStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.stagePrice options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize17;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    //
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        islandImgv.image=image;
    }else{
        [islandImgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_120x93"]];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)clickLeftBtn:(id)sender{
    UIButton *btn=sender;
    OrderListModel *model = objc_getAssociatedObject(btn, "leftBtnModel");
    [self.leftButtonDelegate clickOrderLeftButtonWithModel:model];
}

-(void)clickRightBtn:(id)sender{
    UIButton *btn=sender;
    OrderListModel *model = objc_getAssociatedObject(btn, "rightBtnModel");
    [self.rightButtonDelegate clickOrderRightButtonWithModel:model];
}

@end
