//
//  OrderTestCell.h
//  youxia
//
//  Created by mac on 16/7/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListModel.h"

@protocol OrderListLeftButtonDelegate <NSObject>

-(void)clickOrderLeftButtonWithModel:(OrderListModel *)model;

@end

@protocol OrderListRightButtonDelegate <NSObject>

-(void)clickOrderRightButtonWithModel:(OrderListModel *)model;

@end

@interface OrderTestCell : UITableViewCell

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, weak) id<OrderListLeftButtonDelegate> leftButtonDelegate;
@property (nonatomic, weak) id<OrderListRightButtonDelegate> rightButtonDelegate;

@property (nonatomic, retain) OrderListModel *model;
-(void)reflushDataForModel:(OrderListModel *)model;

@end
