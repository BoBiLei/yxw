//
//  OrderListCell.h
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListModel.h"

@interface OrderListCell : UITableViewCell

@property (nonatomic, copy) NSString *orderId;

-(void)reflushDataForModel:(OrderListModel *)model;

@end
