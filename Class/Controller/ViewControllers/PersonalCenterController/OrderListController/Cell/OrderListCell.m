//
//  OrderListCell.m
//  youxia
//
//  Created by mac on 15/12/16.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderListCell.h"

#define ButtonWidth 74
#define ButtonHeight 34
@implementation OrderListCell{
    UILabel *hdvRightLabel;
    UILabel *islandTitle;
    UILabel *orderNum;
    UILabel *orderTime;
    UIImageView *imgv;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 44)];
    headView.backgroundColor=[UIColor whiteColor];
    [self addSubview:headView];
    
    //付款状态
    hdvRightLabel=[[UILabel alloc]initWithFrame:CGRectMake(SCREENSIZE.width-62, 6, 64, headView.height-12)];
    hdvRightLabel.textAlignment=NSTextAlignmentCenter;
    hdvRightLabel.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    hdvRightLabel.textColor=[UIColor whiteColor];
    hdvRightLabel.font=kFontSize14;
    hdvRightLabel.layer.cornerRadius=3;
    hdvRightLabel.layer.masksToBounds=YES;
    [headView addSubview:hdvRightLabel];
    
    //海岛图片
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, headView.height+6, 98, 71)];
    imgv.contentMode=UIViewContentModeScaleAspectFill;
    imgv.layer.masksToBounds = YES;
    [self addSubview:imgv];
    
    //title
    islandTitle=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+8, hdvRightLabel.origin.y+hdvRightLabel.height+8, SCREENSIZE.width-(imgv.origin.x+imgv.width+16), 38)];
    islandTitle.font=kFontSize15;
    islandTitle.numberOfLines=0;
    islandTitle.opaque=YES;
    islandTitle.layer.shouldRasterize = YES;
    islandTitle.layer.rasterizationScale = [UIScreen mainScreen].scale;
    [self addSubview:islandTitle];
    
    //订单编号
    orderNum=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x, imgv.origin.y+imgv.height+2, (SCREENSIZE.width-24)/2, 24)];
    orderNum.textColor=[UIColor grayColor];
    orderNum.font=kFontSize14;
    orderNum.numberOfLines=0;
    orderNum.opaque=YES;
    orderNum.layer.shouldRasterize = YES;
    orderNum.layer.rasterizationScale = [UIScreen mainScreen].scale;
    orderNum.adjustsFontSizeToFitWidth=YES;
    [self addSubview:orderNum];
    
    //订单编号
    orderTime=[[UILabel alloc]initWithFrame:CGRectMake(orderNum.origin.x+orderNum.width+8, orderNum.origin.y, orderNum.width, orderNum.height)];
    orderTime.textColor=[UIColor grayColor];
    orderTime.font=kFontSize14;
    orderTime.numberOfLines=0;
    orderTime.opaque=YES;
    orderTime.layer.shouldRasterize = YES;
    orderTime.layer.rasterizationScale = [UIScreen mainScreen].scale;
    orderTime.adjustsFontSizeToFitWidth=YES;
    [self addSubview:orderTime];
}

-(void)reflushDataForModel:(OrderListModel *)model{
    
    _orderId=model.orderId;
    
    NSString *hdvStr;
    if ([model.orderStatus isEqualToString:@"1"]) {
        if ([model.orderType isEqualToString:@"prize"]) {
            hdvStr=@"";
        }else{
            
            islandTitle.textColor=[UIColor lightGrayColor];
            orderNum.textColor=[UIColor grayColor];
            orderTime.textColor=[UIColor grayColor];
            
            if ([model.orderProcess isEqualToString:@"WAIT_BUYER_PAY"]) {
                hdvStr=@" 等待付款";
                
                hdvRightLabel.textColor=[UIColor whiteColor];
                hdvRightLabel.backgroundColor=[UIColor colorWithHexString:@"#f36700"];
                
            }else if ([model.orderProcess isEqualToString:@"WAIT_SELLER_SEND_GOODS"]){
                hdvStr=@"等待卖家发货";
            }else if ([model.orderProcess isEqualToString:@"WAIT_BUYER_CONFIRM_GOODS"]){
                hdvStr=@"已发货";
            }else if ([model.orderProcess isEqualToString:@"TRADE_FINISHED"]){
                hdvStr=@"交易完成";
            }else if ([model.orderProcess isEqualToString:@"__CREATE__"]){
                hdvStr=@"已创建";
            }else if ([model.orderProcess isEqualToString:@"__PAY_YET__"]){
                hdvStr=@"已付款";
            }
        }
    }else{
        islandTitle.textColor=[UIColor lightGrayColor];
        orderNum.textColor=[UIColor lightGrayColor];
        orderTime.textColor=[UIColor lightGrayColor];
        hdvRightLabel.textColor=[UIColor whiteColor];
        hdvRightLabel.backgroundColor=[UIColor lightGrayColor];
        if ([model.orderStatus isEqualToString:@"0"]) {
            hdvStr=@"已取消";
        }else if ([model.orderStatus isEqualToString:@"2"]){
            hdvStr=@"已过期";
        }else if ([model.orderStatus isEqualToString:@"3"]){
            hdvStr=@"失败";
        }else if ([model.orderStatus isEqualToString:@"4"]){
            hdvStr=@"已经退款";
        }else if ([model.orderStatus isEqualToString:@"5"]){
            hdvStr=@"已申请退款";
        }
    }
    
    hdvRightLabel.text=hdvStr;
    
    //
    NSString *titStr=model.islandTitle;
    CGSize titSize=[AppUtils getStringSize:titStr withFont:15];
    CGRect titLabelFrame=islandTitle.frame;
    CGFloat titLabelY=titLabelFrame.origin.y;
    CGFloat titLabelH=0.0f;
    if (titSize.width<titLabelFrame.size.width) {
        titLabelH=21;
        titLabelFrame.origin.y=titLabelY+2;
        titLabelFrame.size.height=titLabelH;
        islandTitle.frame=titLabelFrame;
    }
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titStr];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:1.3];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titStr length])];
    islandTitle.attributedText = attributedString;
    
    
    orderNum.text=[NSString stringWithFormat:@"订单编号:%@",model.orderId];
    
    //
    //时间戳转换
    NSTimeInterval time=[model.orderTime doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *valiStr = [dateFormatter stringFromDate: detaildate];
    orderTime.text=valiStr;
    
    //
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_120x93"]];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
