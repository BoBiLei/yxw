//
//  IDCardApprove.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "IDCardApprove.h"
#import <AVFoundation/AVFoundation.h>

@interface IDCardApprove ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic, strong) IDCardApproveModel *model;

@end

@implementation IDCardApprove{
    NSString *seleIdCartFlat;
    
    NSMutableDictionary *frontIdCartData;
    NSMutableDictionary *backIdCartData;
    
    BOOL isFrontImgHadata;
    BOOL isBackImgHadata;
    
    UITextField *nameTF;
    UITextField *idCardTF;
    UIButton *idCardBtn01;
    UIButton *idCardBtn02;
    UIButton *subBtn;
}

+(id)initWithModel:(IDCardApproveModel *)model{
    IDCardApprove *vc=[IDCardApprove new];
    vc.model=model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpUI];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"身份证验证" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    UITableView *table=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-108)];
    [self.view addSubview:table];
    
    UIView *footView=[[UIView alloc] initWithFrame:table.frame];
    table.tableFooterView=footView;
    
#pragma mark 姓名
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, 24, 88, 42)];
    nameLabel.font=kFontSize16;
    nameLabel.text=@"姓 名:";
    [footView addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, footView.width-(nameLabel.origin.x*2+nameLabel.width+8), nameLabel.height)];
    nameView.layer.cornerRadius=4;
    nameView.layer.borderWidth=0.5f;
    nameView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    nameView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:nameView];
    
    nameTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, nameView.width-10, nameView.height)];
    nameTF.font=kFontSize15;
    nameTF.placeholder=@"请输入姓名";
    nameTF.text=_model.name;
    if ([_model.status isEqualToString:@"2"]) {
        nameTF.enabled=NO;
    }else{
        nameTF.enabled=YES;
    }
    nameTF.textColor=[UIColor colorWithHexString:@"70635a"];
    nameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:nameTF];
    
#pragma mark 身份证号码
    UILabel *idCardLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, 88, 42)];
    idCardLabel.font=kFontSize16;
    idCardLabel.text=@"身份证号码:";
    [footView addSubview:idCardLabel];
    
    UIView *idCardView=[[UIView alloc] initWithFrame:CGRectMake(idCardLabel.origin.x+idCardLabel.width+8, idCardLabel.origin.y, nameView.width, nameView.height)];
    idCardView.layer.cornerRadius=4;
    idCardView.layer.borderWidth=0.5f;
    idCardView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    idCardView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:idCardView];
    
    idCardTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, idCardView.width-10, idCardView.height)];
    idCardTF.font=kFontSize15;
    idCardTF.placeholder=@"请输入身份证号码";
    idCardTF.text=_model.idCard;
    if ([_model.status isEqualToString:@"2"]) {
        idCardTF.enabled=NO;
    }else{
        idCardTF.enabled=YES;
    }
    idCardTF.textColor=[UIColor colorWithHexString:@"70635a"];
    idCardTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [idCardView addSubview:idCardTF];
    
    /////////////////
    /////////////////
    UILabel *tip01=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, idCardLabel.origin.y+idCardLabel.height+26, SCREENSIZE.width-2*nameLabel.origin.x, 32)];
    tip01.text=@"请上传您的身份证的正反面照片";
    tip01.font=kFontSize16;
    tip01.textColor=[UIColor colorWithHexString:@"#282828"];
    tip01.textAlignment=NSTextAlignmentLeft;
    tip01.numberOfLines=0;
    [footView addSubview:tip01];
    
    //
    UILabel *upLoadTip01=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, tip01.origin.y+tip01.height, SCREENSIZE.width-24, 26)];
    upLoadTip01.text=@"注：JPG、PNG格式图片，大小不超过2M";
    upLoadTip01.font=kFontSize14;
    upLoadTip01.textColor=[UIColor redColor];
    upLoadTip01.textAlignment=NSTextAlignmentLeft;
    upLoadTip01.numberOfLines=0;
    [footView addSubview:upLoadTip01];
    
#pragma mark 显示身份证
    //显示身份证的
    idCardBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    idCardBtn01.frame=CGRectMake(16, upLoadTip01.origin.y+upLoadTip01.height+10, (SCREENSIZE.width-48)/2, (SCREENSIZE.width-48)/2);
    idCardBtn01.layer.borderWidth=0.7f;
    idCardBtn01.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [idCardBtn01 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    idCardBtn01.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [idCardBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    idCardBtn01.titleLabel.font=kFontSize15;
    [idCardBtn01 addTarget:self action:@selector(clickBtn01:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:idCardBtn01];
    if (![_model.idFrontImg isEqualToString:@""]||_model.idFrontImg!=NULL) {
        [idCardBtn01 setImageWithURL:[NSURL URLWithString:_model.idFrontImg] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"add_picture"]];
        isFrontImgHadata=YES;
    }else{
        [idCardBtn01 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    }
    
    
    
    idCardBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    idCardBtn02.frame=CGRectMake(idCardBtn01.origin.x+idCardBtn01.width+12, idCardBtn01.origin.y, idCardBtn01.width, idCardBtn01.height);
    idCardBtn02.layer.borderWidth=0.7f;
    idCardBtn02.imageView.contentMode=UIViewContentModeScaleAspectFit;
    idCardBtn02.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [idCardBtn02 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    [idCardBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    idCardBtn02.titleLabel.font=kFontSize15;
    [idCardBtn02 addTarget:self action:@selector(clickBtn02:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:idCardBtn02];
    if (![_model.idBackImg isEqualToString:@""]||_model.idBackImg!=NULL||![_model.idBackImg isEqualToString:@" "]) {
        [idCardBtn02 setImageWithURL:[NSURL URLWithString:_model.idBackImg] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"add_picture"]];
        isBackImgHadata=YES;
    }else{
        [idCardBtn02 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    }
    
    if ([_model.status isEqualToString:@"2"]) {
        [idCardBtn01 setUserInteractionEnabled:NO];
        [idCardBtn02 setUserInteractionEnabled:NO];
    }else{
        [idCardBtn01 setUserInteractionEnabled:YES];
        [idCardBtn02 setUserInteractionEnabled:YES];
    }
    
    //身份证照片左上角（正反面提示）
    UIImageView *zmImg=[UIImageView new];
    zmImg.image=[UIImage imageNamed:@"uploadimg_zm"];
    [idCardBtn01 addSubview:zmImg];
    zmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* zmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_h];
    NSArray* zmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_w];
    
    UIImageView *fmImg=[UIImageView new];
    fmImg.image=[UIImage imageNamed:@"uploadimg_fm"];
    [idCardBtn02 addSubview:fmImg];
    fmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* fmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_h];
    NSArray* fmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_w];
    
#pragma mark 提交按钮
    subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    subBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    subBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    subBtn.titleLabel.font=kFontSize17;
    [subBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:subBtn];
    if (![_model.status isEqualToString:@"2"]) {
        subBtn.hidden=NO;
    }else{
        subBtn.hidden=YES;
    }
}

-(void)clickBtn01:(id)sender{
    seleIdCartFlat=@"1";
    [self showAddPictureView];
}

-(void)clickBtn02:(id)sender{
    seleIdCartFlat=@"2";
    [self showAddPictureView];
}

-(void)clickSubBtn{
    if ([nameTF.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入姓名" inView:self.view];
    }else if ([idCardTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"请输入身份证号码" inView:self.view];
    }else if (!isFrontImgHadata){
        [AppUtils showSuccessMessage:@"请上传身份证正面照片" inView:self.view];
    }else if (!isBackImgHadata){
        [AppUtils showSuccessMessage:@"请上传身份证反面照片" inView:self.view];
    }else{
        [self submitForName:nameTF.text personId:idCardTF.text];
    }
}

#pragma mark - ShowAddPictureView
-(void)showAddPictureView{
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            default:
                [self pickImage];
                break;
        }
    };
    
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从手机相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:^(MMPopupView *popupView){
        
    }];
}

//拍照
- (void) snapImage{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"请在iPhone的“设置-隐私-相机”选项中，允许 游侠旅行 访问你的相机" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    ipc.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    ipc.navigationBar.barTintColor = [UIColor colorWithRed:20.f/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1];
    
    ipc.navigationBar.tintColor = [UIColor whiteColor];
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //取得原始图片
    UIImage *image= [info objectForKey:@"UIImagePickerControllerEditedImage"];
    NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
    UIImage *newImage=[UIImage imageWithData:imageData];
    
    //判断类型（拍照还是从相册选取）
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        
        UIImageWriteToSavedPhotosAlbum([info objectForKey:@"UIImagePickerControllerOriginalImage"], nil, nil, nil);
    }else{
        
    }
    
    UIImage *theImage = [self imageWithImageSimple:newImage scaledToSize:CGSizeMake(700, 700)];
    
    NSData* upImgData = UIImagePNGRepresentation(theImage);
    NSString *imgByte=[NSString stringWithFormat:@"%lu",(unsigned long)upImgData.length];
    
    if ([seleIdCartFlat isEqualToString:@"1"] ) {
        isFrontImgHadata=YES;
        
        frontIdCartData=[NSMutableDictionary dictionary];
        
        [frontIdCartData setObject:upImgData forKey:@"zfl"];
        [frontIdCartData setObject:@"png" forKey:@"img_type"];
        [frontIdCartData setObject:imgByte forKey:@"size"];
        
        backIdCartData=[NSMutableDictionary dictionary];
        
        [backIdCartData setObject:@"" forKey:@"zfl"];
        [backIdCartData setObject:@"" forKey:@"img_type"];
        [backIdCartData setObject:@"" forKey:@"size"];
        
        //上传
        [self uploadFrontData:frontIdCartData backData:backIdCartData];
        
    }
    
    
    else{
        isBackImgHadata=YES;
        
        frontIdCartData=[NSMutableDictionary dictionary];
        [frontIdCartData setObject:@"" forKey:@"zfl"];
        [frontIdCartData setObject:@"" forKey:@"img_type"];
        [frontIdCartData setObject:@"" forKey:@"size"];
        
        backIdCartData=[NSMutableDictionary dictionary];
        [backIdCartData setObject:upImgData forKey:@"zfl"];
        [backIdCartData setObject:@"png" forKey:@"img_type"];
        [backIdCartData setObject:imgByte forKey:@"size"];
        
        //上传
        [self uploadFrontData:frontIdCartData backData:backIdCartData];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationBar *navigationBar = navigationController.navigationBar;
    [navigationBar lt_setBackgroundColor:[UIColor colorWithHexString:@"0080c5"]];
    navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

#pragma mark  Request
//提交
-(void)submitForName:(NSString *)name personId:(NSString *)personId{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_person_info_new" forKey:@"code"];
    [dict setObject:name forKey:@"person_name"];
    [dict setObject:personId forKey:@"person_idcard"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            nameTF.enabled=NO;
            idCardTF.enabled=NO;
            idCardBtn01.userInteractionEnabled=NO;
            idCardBtn02.userInteractionEnabled=NO;
            subBtn.hidden=YES;
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)uploadFrontData:(NSDictionary *)frontDic backData:(NSDictionary *)backDic{
    [AppUtils showProgressMessage:@"正在上传图片…" inView:self.view];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_idcard_new" forKey:@"code"];
    [dict setObject:@"imgs_idcard" forKey:@"type"];
    [dict setObject:frontDic forKey:@"data0"];
    [dict setObject:backDic forKey:@"data1"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //正面
            if ([seleIdCartFlat isEqualToString:@"1"]) {
                //处理提示的
                if ([responseObject[@"retData"][@"front"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"front"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"front"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"front"][@"is_lock"] isEqualToString:@"1"]) {
                    NSString *imgStr=responseObject[@"retData"][@"front"][@"up_info"][@"url"];
                    [idCardBtn01 setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholder:nil];
                    [idCardBtn01 setUserInteractionEnabled:NO];
                }
                //正面不保存
                else{
                    
                }
            }
            
            //反面
            else{
                //处理提示的
                if ([responseObject[@"retData"][@"back"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"back"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"back"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"back"][@"is_lock"] isEqualToString:@"1"]) {
                    NSString *imgStr=responseObject[@"retData"][@"back"][@"up_info"][@"url"];
                    [idCardBtn02 setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholder:nil];
                    [idCardBtn02 setUserInteractionEnabled:NO];
                }
                //反面不保存
                else{
                    
                }
            }
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
