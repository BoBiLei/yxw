//
//  IDCardApproveModel.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDCardApproveModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *idCard;

@property (nonatomic, copy) NSString *idFrontImg;     //正面
@property (nonatomic, copy) NSString *idBackImg;      //反面

@property (nonatomic, copy) NSString *status;           //总的status

-(void)jsonToModel:(NSDictionary *)dic;

@end
