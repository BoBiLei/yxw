//
//  IDCardApproveModel.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "IDCardApproveModel.h"

@implementation IDCardApproveModel

-(void)jsonToModel:(NSDictionary *)dic{
    if (dic[@"person_name"]&&![dic[@"person_name"] isKindOfClass:[NSNull class]]) {
        self.name=dic[@"person_name"];
    }else{
        self.name=@"";
    }
    
    if (dic[@"person_idcard"]&&![dic[@"person_idcard"] isKindOfClass:[NSNull class]]) {
        self.idCard=dic[@"person_idcard"];
    }else{
        self.idCard=@"";
    }
    
    if (dic[@"img_idcard_front"]&&![dic[@"img_idcard_front"] isKindOfClass:[NSNull class]]&&![dic[@"img_idcard_front"] isEqualToString:@""]) {
        self.idFrontImg=dic[@"img_idcard_front"];
    }else{
        self.idFrontImg=@"";
    }
    
    if (dic[@"img_idcard_back"]&&![dic[@"img_idcard_back"] isKindOfClass:[NSNull class]]
        &&![dic[@"img_idcard_back"] isEqualToString:@""]) {
        self.idBackImg=dic[@"img_idcard_back"];
    }else{
        self.idBackImg=@"";
    }
    
    if (dic[@"person_status"]&&![dic[@"person_status"] isKindOfClass:[NSNull class]]) {
        self.status=dic[@"person_status"];
    }else{
        self.status=@"";
    }
}

@end
