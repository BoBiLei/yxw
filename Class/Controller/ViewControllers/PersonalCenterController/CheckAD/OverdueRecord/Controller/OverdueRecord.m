//
//  OverdueRecord.m
//  youxia
//
//  Created by mac on 2017/1/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "OverdueRecord.h"
#import "OverdueHeaderView.h"
#import "OverdueCell.h"
#import "OverdueLayout.h"
#import "OverdueFirstCell.h"
#import "OverdueRecordModel.h"
#import "CheckADController.h"
#import "NewApproveInfo.h"

@interface OverdueRecord ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation OverdueRecord{
    NSMutableArray *dataArr;
    UICollectionView *myCollection;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    
    [self setUpCustomSearBar];
    
    [self setUpUI];
    
    [self dataRequest];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#55b2f0"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"央行信用报告";
    [self.view addSubview:titleLabel];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(SCREENSIZE.width-108, 22, 94, 44);
    [rightBtn setTitle:@"更新报告" forState:UIControlStateNormal];
    rightBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=kFontSize16;
    [rightBtn addTarget:self action:@selector(clickReflush) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightBtn];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - 点击刷新更新报告
-(void)clickReflush{
    //点击刷新
    CheckADController *ctr=[CheckADController new];
    ctr.loginName=_loginName;
    ctr.loginPass=_loginPass;
    [self.navigationController pushViewController:ctr animated:YES];
}

-(void)setUpUI{
    OverdueLayout *myLayout= [[OverdueLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    myCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:myLayout];
    myCollection.backgroundColor=[UIColor clearColor];
    myCollection.dataSource=self;
    myCollection.delegate=self;
    [myCollection registerNib:[UINib nibWithNibName:@"OverdueFirstCell" bundle:nil] forCellWithReuseIdentifier:@"OverdueFirstCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"OverdueCell" bundle:nil] forCellWithReuseIdentifier:@"OverdueCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"OverdueHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"OverdueHeaderView"];
    myCollection.hidden=YES;
    [self.view addSubview:myCollection];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    OverdueCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"OverdueCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[OverdueCell alloc]init];
    }
    if(dataArr.count!=0){
        OverdueRecordModel *model=dataArr[indexPath.section];
        [cell reflushAtIndexPath:indexPath whitModel:model];
    }
    return cell;
}

- (CGFloat)minimumInteritemSpacing {
    return 0;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREENSIZE.width/4, SCREENSIZE.width/4);
}

//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 16, 0);
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        OverdueHeaderView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"OverdueHeaderView" forIndexPath:indexPath];
        cell.titleLabel.text=indexPath.section==0?@"信用卡":indexPath.section==1?@"房贷":indexPath.section==2?@"其他贷款":@"公众记录";
        [cell.imgv setImage:[UIImage imageNamed:indexPath.section==0?@"wyyq_xyk":indexPath.section==1?@"wyyq_fd":indexPath.section==2?@"wyyq_qt":@"wyyq_jl"] forState:UIControlStateNormal];
        return cell;
    }
    return nil;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(SCREENSIZE.width, 40);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"get_overdue",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
            NSDictionary *xykDic=responseObject[@"retData"][@"credit"][@"creditCard"];
            NSDictionary *fdDic=responseObject[@"retData"][@"credit"][@"purchaseLoan"];
            NSDictionary *otherDic=responseObject[@"retData"][@"credit"][@"otherLoans"];
            OverdueRecordModel *model01=[OverdueRecordModel new];
            OverdueRecordModel *model02=[OverdueRecordModel new];
            OverdueRecordModel *model03=[OverdueRecordModel new];
            [model01 jsonDataForDictionary:xykDic];
            [model02 jsonDataForDictionary:fdDic];
            [model03 jsonDataForDictionary:otherDic];
            [dataArr addObject:model01];
            [dataArr addObject:model02];
            [dataArr addObject:model03];
            myCollection.hidden=NO;
            [myCollection reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
