//
//  OverdueRecord.h
//  youxia
//
//  Created by mac on 2017/1/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverdueRecord : UIViewController

@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *loginPass;

@end
