//
//  OverdueCell.m
//  youxia
//
//  Created by mac on 2017/1/13.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "OverdueCell.h"

@implementation OverdueCell{
    TTTAttributedLabel *titleLabel;
    UIView *line;
    
    UIView *bottomLine;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //
    titleLabel=[TTTAttributedLabel new];
    titleLabel.font=[UIFont systemFontOfSize:17];
    titleLabel.numberOfLines=0;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* imgBig_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[titleLabel]-1-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel)];
    [NSLayoutConstraint activateConstraints:imgBig_h];
    NSArray* imgBig_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[titleLabel]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel)];
    [NSLayoutConstraint activateConstraints:imgBig_w];
    
    //
    line=[UIView new];
    line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    line.hidden=NO;
    [self addSubview:line];
    
    //
    bottomLine=[UIView new];
    bottomLine.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [self addSubview:bottomLine];
    bottomLine.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* bottomLine_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomLine]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bottomLine)];
    [NSLayoutConstraint activateConstraints:bottomLine_h];
    NSArray* bottomLine_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomLine(1)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bottomLine)];
    [NSLayoutConstraint activateConstraints:bottomLine_w];
}

-(void)reflushAtIndexPath:(NSIndexPath *)indexPath whitModel:(OverdueRecordModel *)model{
    if (indexPath.row==3) {
        line.hidden=YES;
    }else{
        line.hidden=NO;
    }
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line(1.5)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[line]-22-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //
    if (indexPath.section==0) {
        NSString *stageStr=indexPath.row==0?[NSString stringWithFormat:@"%@\n信用卡数量",model.accountNumber]:indexPath.row==1?[NSString stringWithFormat:@"%@\n账户逾期次数",model.delayAccountNumber]:indexPath.row==2?[NSString stringWithFormat:@"%@\n为他人担保数",model.guaranteeQuantity]:[NSString stringWithFormat:@"%@\n未销户账户数",model.unSettlement];
        [titleLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:indexPath.row==0?@"信用卡数量":indexPath.row==1?@"账户逾期次数":indexPath.row==2?@"为他人担保数":@"未销户账户数" options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"949494"] CGColor] range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:12];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else if (indexPath.section==1){
        NSString *stageStr=indexPath.row==0?[NSString stringWithFormat:@"%@\n购房贷款数",model.accountNumber]:indexPath.row==1?[NSString stringWithFormat:@"%@\n账户逾期次数",model.delayAccountNumber]:indexPath.row==2?[NSString stringWithFormat:@"%@\n为他人担保数",model.guaranteeQuantity]:[NSString stringWithFormat:@"%@\n未销户账户数",model.unSettlement];
        [titleLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:indexPath.row==0?@"购房贷款数":indexPath.row==1?@"账户逾期次数":indexPath.row==2?@"为他人担保数":@"未销户账户数" options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"949494"] CGColor] range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:12];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else if (indexPath.section==2){
        NSString *stageStr=indexPath.row==0?[NSString stringWithFormat:@"%@\n贷款账户数",model.accountNumber]:indexPath.row==1?[NSString stringWithFormat:@"%@\n账户逾期次数",model.delayAccountNumber]:indexPath.row==2?[NSString stringWithFormat:@"%@\n为他人担保数",model.guaranteeQuantity]:[NSString stringWithFormat:@"%@\n未销户账户数",model.unSettlement];
        [titleLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:indexPath.row==0?@"贷款账户数":indexPath.row==1?@"账户逾期次数":indexPath.row==2?@"为他人担保数":@"未销户账户数" options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"949494"] CGColor] range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:12];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
}

@end
