//
//  OverdueFirstCell.m
//  youxia
//
//  Created by mac on 2017/1/13.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "OverdueFirstCell.h"

@implementation OverdueFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 74, 24)];
    tipLabel.center=CGPointMake(SCREENSIZE.width/2, 28);
    tipLabel.layer.cornerRadius=3;
    tipLabel.layer.masksToBounds=YES;
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.textColor=[UIColor whiteColor];
    tipLabel.font=kFontSize15;
    tipLabel.backgroundColor=[UIColor colorWithHexString:@"669933"];
    tipLabel.text=@"尚无逾期";
    [self addSubview:tipLabel];
    
    TTTAttributedLabel *titleLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(12, tipLabel.origin.y+tipLabel.height+8, SCREENSIZE.width-24, 58)];
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.adjustsFontSizeToFitWidth=YES;
    titleLabel.textColor=[UIColor colorWithHexString:@"595959"];
    titleLabel.numberOfLines=0;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    NSString *titStr=@"您从未产生逾期，请继续保持，同事建议您（申请）使用信用卡，并按时还款，提高自己的信用。";
    [titleLabel setText:titStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"尚无逾期" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
        //背景色
        [mutableAttributedString addAttribute:kTTTBackgroundFillColorAttributeName
                                        value:[UIColor colorWithHexString:@"#55b2f0"]
                                        range:sumRange];
        [mutableAttributedString addAttribute:kTTTBackgroundStrokeColorAttributeName
                                        value:[UIColor colorWithHexString:@"#55b2f0"]
                                        range:sumRange];
        //
        [mutableAttributedString addAttribute:kTTTBackgroundCornerRadiusAttributeName
                                        value:@(2)
                                        range:sumRange];
        UIFont *boldSystemFont = [UIFont systemFontOfSize:13];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        //调整行间距
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:12];
        [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titStr length])];
        return mutableAttributedString;
    }];
    [self addSubview:titleLabel];
}

@end
