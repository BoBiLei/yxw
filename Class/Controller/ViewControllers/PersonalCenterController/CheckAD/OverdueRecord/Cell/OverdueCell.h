//
//  OverdueCell.h
//  youxia
//
//  Created by mac on 2017/1/13.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverdueRecordModel.h"

@interface OverdueCell : UICollectionViewCell

-(void)reflushAtIndexPath:(NSIndexPath *)indexPath whitModel:(OverdueRecordModel *)model;

@end
