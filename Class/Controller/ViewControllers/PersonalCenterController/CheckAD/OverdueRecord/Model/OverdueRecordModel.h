//
//  OverdueRecordModel.h
//  youxia
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OverdueRecordModel : NSObject

@property (nonatomic, copy) NSString *accountNumber;    //账户数量
@property (nonatomic, copy) NSString *delayAccountNumber;//发生过逾期的账户数
@property (nonatomic, copy) NSString *delayOver90DayAccountNumber;//逾期90天的账户数
@property (nonatomic, copy) NSString *guaranteeQuantity;//为他人担保笔数
@property (nonatomic, copy) NSString *unSettlement;     //未销户账户数

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
