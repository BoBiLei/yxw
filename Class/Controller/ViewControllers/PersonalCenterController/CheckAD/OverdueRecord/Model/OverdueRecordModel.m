//
//  OverdueRecordModel.m
//  youxia
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "OverdueRecordModel.h"

@implementation OverdueRecordModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.accountNumber=dic[@"accountNumber"];
    self.delayAccountNumber=dic[@"delayAccountNumber"];
    self.delayOver90DayAccountNumber=dic[@"delayOver90DayAccountNumber"];
    self.guaranteeQuantity=dic[@"guaranteeQuantity"];
    self.unSettlement=dic[@"unSettlement"];
}

@end
