//
//  ADputRHValidateController.m
//  youxia
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADputRHValidateController.h"
#import "OverdueRecord.h"
#import "CheckADController.h"
#import "CheckADLoginRegistCell.h"
#import "NewApproveInfo.h"

#define Headview_Height 130

@interface ADputRHValidateController ()<UITableViewDataSource,UITableViewDelegate,TTTAttributedLabelDelegate>

@end

@implementation ADputRHValidateController{
    UILabel *showName;
    UIImageView *tipPicture;
    
    NSString *yhValidateStr;    //央行验证码
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    yhValidateStr=@"";
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpHeaderView];
    
    [self setUpTableView];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#55b2f0"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"央行信用报告";
    [self.view addSubview:titleLabel];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

-(void)setUpHeaderView{
    UIImageView *headimgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, Headview_Height)];
    headimgv.image=[UIImage imageNamed:@"checkad_head"];
    [self.view addSubview:headimgv];
    
    //白线
    UIImageView *wLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headimgv.width, 1.5f)];
    wLine.center=CGPointMake(SCREENSIZE.width/2, headimgv.centerY-5);
    wLine.image=[UIImage imageNamed:@"cad_line"];
    [self.view addSubview:wLine];
    
    //
    UIImageView *leftImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 78, 76)];
    leftImgv.center=CGPointMake(SCREENSIZE.width+5, wLine.centerY-26);
    leftImgv.image=[UIImage imageNamed:@"check_head_right"];
    [self.view addSubview:leftImgv];
    
    //显示名称
    showName=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 21)];
    showName.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY+19);
    showName.font=kFontSize15;
    showName.textColor=[UIColor whiteColor];
    showName.textAlignment=NSTextAlignmentCenter;
    showName.text=@"获取信用信息";
    [self.view addSubview:showName];
    
    //小白点
    UIImageView *domImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    domImgv.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY);
    domImgv.image=[UIImage imageNamed:@"check_head_dom"];
    [self.view addSubview:domImgv];
    
    UIImageView *dom_center=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    dom_center.center=CGPointMake((SCREENSIZE.width-48)/4+25, wLine.centerY);
    dom_center.image=[UIImage imageNamed:@"check_dom_zq"];
    [self.view addSubview:dom_center];
    
    UIImageView *dom_left=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    dom_left.center=CGPointMake(31, wLine.centerY);
    dom_left.image=[UIImage imageNamed:@"check_dom_zq"];
    [self.view addSubview:dom_left];
    
    //
    tipPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52, 52)];
    tipPicture.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY-tipPicture.height/2-3);
    tipPicture.image=[UIImage imageNamed:@"check_head_xybg"];
    [self.view addSubview:tipPicture];
    
    UIImageView *cenPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
    cenPicture.center=CGPointMake(dom_center.centerX, wLine.centerY-cenPicture.height/2-2);
    cenPicture.image=[UIImage imageNamed:@"check_head_center"];
    [self.view addSubview:cenPicture];
    
    UIImageView *leftPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
    leftPicture.center=CGPointMake(dom_left.centerX, wLine.centerY-leftPicture.height/2-2);
    leftPicture.image=[UIImage imageNamed:@"check_head_left"];
    [self.view addSubview:leftPicture];
}

#pragma mark - SetUp Table
-(void)setUpTableView{
    UITableView *myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, Headview_Height+64, SCREENSIZE.width, SCREENSIZE.height-(Headview_Height+64+48)) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.scrollEnabled=NO;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"CheckADLoginRegistCell" bundle:nil] forCellReuseIdentifier:@"CheckADLoginRegistCell"];
    [self.view addSubview:myTable];
    
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 400)];
    myTable.tableFooterView=footView;
    
    UIImageView *bgImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width*299/500)];
    bgImgv.image=[UIImage imageNamed:@"check_bottom_bg"];   //500x299
    [footView addSubview:bgImgv];
    
//    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(18, bgImgv.origin.y+bgImgv.height, footView.width-36, 74)];
//    tipLabel.textColor=[UIColor colorWithHexString:@"ababab"];
//    tipLabel.font=kFontSize16;
//    tipLabel.numberOfLines=0;
//    NSString *labelTtt=@"征信中心会在24小时内,将申请结果以短信发送到您注册征信账户时预留的手机上,请收到验证码后返回获取征信报告。";
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle setLineSpacing:4];//调整行间距
//    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
//    tipLabel.attributedText = attributedString;
//    [footView addSubview:tipLabel];
    
    //
    TTTAttributedLabel *tipLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(18, bgImgv.origin.y+bgImgv.height+4, footView.width-36, 74)];
    tipLabel.font=[UIFont systemFontOfSize:15];
    tipLabel.numberOfLines=0;
    tipLabel.textColor=[UIColor colorWithHexString:@"ababab"];
    tipLabel.delegate=self;
    [footView addSubview:tipLabel];
    NSString *stageStr=@"您的报告已生成,快去看看吧！\n24小时仍未收到？点击重新获取";
    __block NSRange sumRange;
    [tipLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        sumRange = [[mutableAttributedString string] rangeOfString:@"点击重新获取" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"55b2f0"] CGColor] range:sumRange];
        //调整行间距
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [stageStr length])];
        return mutableAttributedString;
    }];
    //NO 不显示下划线
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
    [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    tipLabel.linkAttributes = linkAttributes;
    [tipLabel addLinkToURL:[NSURL URLWithString:@""] withRange:sumRange];
    
    //
    UIButton *nextBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    nextBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    nextBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(clickNext) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextBtn];
}

-(void)clickNext{
    if ([yhValidateStr isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入央行验证码" inView:self.view];
    }else{
        [self submitRequest];
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CheckADLoginRegistCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADLoginRegistCell"];
    if (!cell) {
        cell=[[CheckADLoginRegistCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADLoginRegistCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.leftBtn setImage:[UIImage imageNamed:@"czx_valide"] forState:UIControlStateNormal];
    cell.putTextField.placeholder=@"请输入央行验证码";
    cell.CheckADTextChangeBlock=^(NSString *text){
        yhValidateStr=text;
    };
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - TTTAttributedLabelDelegate 点击服务协议

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    [self regetValidateRequest];
}

#pragma mark - 点击重新获取验证码
-(void)clickReGetValidate{
    [self regetValidateRequest];
}

#pragma mark - Request
//提交
-(void)submitRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"idcheck",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"login_name":_loginName,
                         @"id_code":yhValidateStr
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
            [self getCreditRequest];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)getCreditRequest{
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"get_credit_data",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@------",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"msg"]]preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[CheckADController class]]) {
                        [self.navigationController popToViewController:controller animated:YES];
                    }
                }
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            [self.navigationController pushViewController:[OverdueRecord new] animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//重新获取验证码
-(void)regetValidateRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"update_idcode",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@------",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            
        }else{
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[CheckADController class]]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PutRH_noti_success" object:nil];
                    [self.navigationController popToViewController:controller animated:YES];
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
