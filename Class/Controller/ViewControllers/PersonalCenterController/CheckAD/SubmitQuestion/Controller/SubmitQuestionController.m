//
//  SubmitQuestionController.m
//  youxia
//
//  Created by mac on 2017/1/6.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "SubmitQuestionController.h"
#import "ADApplyResutlController.h"
#import "SubmitQuestionCell.h"
#import "NewApproveInfo.h"


#define Headview_Height 130

@interface SubmitQuestionController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SubmitQuestionController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    NSMutableDictionary *indexDictionary;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpHeaderView];
    
    [self setUpUI];
    
    [self dataRequest];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#55b2f0"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"央行信用报告";
    [self.view addSubview:titleLabel];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

-(void)setUpHeaderView{
    UIImageView *headimgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, Headview_Height)];
    headimgv.image=[UIImage imageNamed:@"checkad_head"];
    [self.view addSubview:headimgv];
    
    //白线
    UIImageView *wLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headimgv.width, 1.5f)];
    wLine.center=CGPointMake(SCREENSIZE.width/2, headimgv.centerY-5);
    wLine.image=[UIImage imageNamed:@"cad_line"];
    [self.view addSubview:wLine];
    
    //
    UIImageView *leftImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 78, 76)];
    leftImgv.center=CGPointMake(SCREENSIZE.width+5, wLine.centerY-26);
    leftImgv.image=[UIImage imageNamed:@"check_head_right"];
    [self.view addSubview:leftImgv];
    
    //显示名称
    UILabel *showName=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 21)];
    showName.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY+19);
    showName.font=kFontSize15;
    showName.textColor=[UIColor whiteColor];
    showName.textAlignment=NSTextAlignmentCenter;
    showName.text=@"验证身份";
    [self.view addSubview:showName];
    
    //小白点
    UIImageView *domImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    domImgv.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY);
    domImgv.image=[UIImage imageNamed:@"check_head_dom"];
    [self.view addSubview:domImgv];
    
    UIImageView *dom_center=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    dom_center.center=CGPointMake(SCREENSIZE.width/4-2, wLine.centerY);
    dom_center.image=[UIImage imageNamed:@"check_dom_zq"];
    [self.view addSubview:dom_center];
    
    //
    UIImageView *tipPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52, 52)];
    tipPicture.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY-tipPicture.height/2-3);
    tipPicture.image=[UIImage imageNamed:@"check_head_xybg"];
    [self.view addSubview:tipPicture];
    
    UIImageView *cenPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
    cenPicture.center=CGPointMake(dom_center.centerX, wLine.centerY-cenPicture.height/2-2);
    cenPicture.image=[UIImage imageNamed:@"check_head_left"];
    [self.view addSubview:cenPicture];
}

-(void)setUpUI{
    indexDictionary=[NSMutableDictionary dictionary];
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64+Headview_Height, SCREENSIZE.width, SCREENSIZE.height-(64+Headview_Height+48)) style:UITableViewStyleGrouped];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"SubmitQuestionCell" bundle:nil] forCellReuseIdentifier:@"SubmitQuestionCell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary *dic=dataArr[section];
    NSArray *arr=dic[@"queArr"];
    return arr.count-1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic=dataArr[indexPath.section];
    NSArray *arr=dic[@"queArr"];
    NSString *str=arr[indexPath.row+1];
    CGSize size=[AppUtils getStringSize:str withFont:15];
    if (size.width>SCREENSIZE.width-49) {
        return 66;
    }else{
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSDictionary *dic=dataArr[section];
    NSArray *textArr=dic[@"queArr"];
    NSString *str=textArr[0];
    CGSize size=[AppUtils getStringSize:str withFont:15];
    if (size.width>SCREENSIZE.width-24) {
        return section==0?72:60;
    }else{
        return section==0?48:36;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSDictionary *dic=dataArr[section];
    NSArray *textArr=dic[@"queArr"];
    NSString *str=textArr[0];
    CGSize size=[AppUtils getStringSize:str withFont:15];
    if (size.width>SCREENSIZE.width-24) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, section==0?72:60)];
        headerView.backgroundColor=[UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, section==0?8:0, SCREENSIZE.width-24, section==0?72:60)];
        label.numberOfLines=0;
        label.textColor = [UIColor colorWithHexString:@"1a1a1a"];
        label.font=[UIFont systemFontOfSize:15];
        label.adjustsFontSizeToFitWidth=YES;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:4];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        label.attributedText = attributedString;
        [headerView addSubview:label];
        
        return headerView;
    }else{
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, section==0?48:36)];
        headerView.backgroundColor=[UIColor whiteColor];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, section==0?8:0, SCREENSIZE.width-24, section==0?48:36)];
        label.numberOfLines=0;
        label.textColor = [UIColor colorWithHexString:@"1a1a1a"];
        label.font=[UIFont systemFontOfSize:15];
        label.text=str;
        [headerView addSubview:label];
        
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==dataArr.count-1?16:4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SubmitQuestionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SubmitQuestionCell"];
    if (!cell) {
        cell=[[SubmitQuestionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SubmitQuestionCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSDictionary *dic=dataArr[indexPath.section];
    NSArray *arr=dic[@"queArr"];
    NSString *str=arr[indexPath.row+1];
    CGSize size=[AppUtils getStringSize:str withFont:14];
    if (size.width>SCREENSIZE.width-24) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:4];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        cell.titleLabel.attributedText = attributedString;
    }else{
        cell.titleLabel.text=str;
    }
    cell.titleLabel.adjustsFontSizeToFitWidth=YES;
    //
    NSString *seleRowStr=dic[@"seleRow"];
    if (seleRowStr.integerValue==indexPath.row) {
        cell.leftImgv.image=[UIImage imageNamed:@"film_pay_sele"];
    }else{
        cell.leftImgv.image=[UIImage imageNamed:@"traveller_nor"];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic=dataArr[indexPath.section];
    NSMutableDictionary *muDic=[NSMutableDictionary dictionaryWithDictionary:dic];
    [muDic setObject:[NSString stringWithFormat:@"%ld",indexPath.row] forKey:@"seleRow"];
    [dataArr replaceObjectAtIndex:indexPath.section withObject:muDic];
    [UIView animateWithDuration:0.01 animations:^{
        [myTable reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 点击提交
-(void)clickSubmit{
    for (int i=0; i<dataArr.count; i++) {
        NSDictionary *dic=dataArr[i];
        NSString *kValue=dic[@"seleRow"];
        if ([kValue isEqualToString:@"-1"]) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"请选择第(%d)道题的答案",i+1] inView:self.view];
            return;
        }
    }
    
    [self submitQuestionRequest];
}

#pragma mark - Request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"get_questions",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            id obj=responseObject[@"retData"][@"questions"];
            if ([obj isKindOfClass:[NSArray class]]) {
                NSArray *arr=obj;
                for (NSArray *ddArr in arr) {
                    NSDictionary *dic=@{@"seleRow":@"-1",@"queArr":ddArr};
                    [dataArr addObject:dic];
                }
                [myTable reloadData];
            }
            //
            UIButton *submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            submitBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
            submitBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
            submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
            [submitBtn setTitle:@"提交问题" forState:UIControlStateNormal];
            [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [submitBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:submitBtn];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitQuestionRequest{
    [AppUtils showProgressInView:self.view];
    
    //NSLog(@"%@",dataArr);
    
    NSDictionary *firstDic=dataArr[0];
    NSString *firstStr=[NSString stringWithFormat:@"%@",firstDic[@"seleRow"]];
    NSString *first_sub=firstStr.integerValue==0?@"1":firstStr.integerValue==1?@"2":firstStr.integerValue==2?@"3":firstStr.integerValue==3?@"4":@"5";
    
    NSDictionary *secondDic=dataArr[1];
    NSString *secondStr=[NSString stringWithFormat:@"%@",secondDic[@"seleRow"]];
    NSString *second_sub=secondStr.integerValue==0?@"1":secondStr.integerValue==1?@"2":secondStr.integerValue==2?@"3":secondStr.integerValue==3?@"4":@"5";
    
    NSDictionary *threeDic=dataArr[2];
    NSString *threeStr=[NSString stringWithFormat:@"%@",threeDic[@"seleRow"]];
    NSString *three_sub=threeStr.integerValue==0?@"1":threeStr.integerValue==1?@"2":threeStr.integerValue==2?@"3":threeStr.integerValue==3?@"4":@"5";
    
    NSDictionary *fourDic=dataArr[3];
    NSString *fourStr=[NSString stringWithFormat:@"%@",fourDic[@"seleRow"]];
    NSString *four_sub=fourStr.integerValue==0?@"1":fourStr.integerValue==1?@"2":fourStr.integerValue==2?@"3":fourStr.integerValue==3?@"4":@"5";
    
    NSDictionary *fiveDic=dataArr[4];
    NSString *fiveStr=[NSString stringWithFormat:@"%@",fiveDic[@"seleRow"]];
    NSString *five_sub=fiveStr.integerValue==0?@"1":fiveStr.integerValue==1?@"2":fiveStr.integerValue==2?@"3":fiveStr.integerValue==3?@"4":@"5";
    
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"post_answer",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"q_first":first_sub,
                         @"q_second":second_sub,
                         @"q_third":three_sub,
                         @"q_fouth":four_sub,
                         @"q_fivth":five_sub
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示" message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                [self.navigationController pushViewController:[ADApplyResutlController new] animated:YES];
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
