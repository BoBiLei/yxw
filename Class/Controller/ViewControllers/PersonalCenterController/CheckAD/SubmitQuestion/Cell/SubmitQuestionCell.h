//
//  SubmitQuestionCell.h
//  youxia
//
//  Created by mac on 2017/1/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitQuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *leftImgv;


@end
