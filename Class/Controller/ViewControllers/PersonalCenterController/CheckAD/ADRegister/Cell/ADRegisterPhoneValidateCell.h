//
//  ADRegisterPhoneValidateCell.h
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADRegisterModel.h"

@protocol ADRegisterPhoneDelegate <NSObject>

-(void)clickGetValidateBtn;

@end

@interface ADRegisterPhoneValidateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIButton *leftImgv;
@property (weak, nonatomic) IBOutlet UITextField *myTextField;
@property (weak, nonatomic) IBOutlet UIButton *validateBtn;
@property (nonatomic, copy) void (^ADRegisterTextChangeBlock)(NSString *text);

@property (weak,nonatomic) id<ADRegisterPhoneDelegate> delegate;

-(void)reflushDataForDictionary:(ADRegisterModel *)model indexPath:(NSIndexPath *)indexPath;
@end
