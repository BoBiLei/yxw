//
//  ADRegisterPhoneValidateCell.m
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADRegisterPhoneValidateCell.h"

@implementation ADRegisterPhoneValidateCell{
    NSInteger timerCount;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotifacation) name:@"ADR_SendValidate_Success" object:nil];
    self.mainView.layer.cornerRadius=4;
    self.mainView.layer.borderWidth=0.8f;
    self.mainView.layer.borderColor=[UIColor colorWithHexString:@"eeeeee"].CGColor;
    self.validateBtn.layer.cornerRadius=3;
    
    [_myTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)updateNotifacation{
    timerCount=60;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
}

-(void)setTimer:(NSTimer *)timer{
    _validateBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    _validateBtn.enabled=NO;
    [_validateBtn setTitle:[NSString stringWithFormat:@"重发(%ldS)",(long)timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [_validateBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _validateBtn.backgroundColor=[UIColor colorWithHexString:@"#55b2f0"];
        _validateBtn.enabled=YES;
        timerCount = 60;
        return;
    }
    timerCount--;
}

#pragma mark - textField
- (void)textChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.ADRegisterTextChangeBlock) {
        self.ADRegisterTextChangeBlock(field.text);
    }
}

-(void)reflushDataForDictionary:(ADRegisterModel *)model indexPath:(NSIndexPath *)indexPath{
    _myTextField.text=model.phoneValidate;
}

- (IBAction)clickValidateBtn:(id)sender {
    [self.delegate clickGetValidateBtn];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
