//
//  ADRegisterController.m
//  youxia
//
//  Created by mac on 2017/1/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADRegisterController.h"
#import "CheckADLoginRegistCell.h"
#import "ADRegisterModel.h"
#import "ADRegisterPhoneValidateCell.h"
#import "CheckADController.h"
#import "NewApproveInfo.h"

@interface ADRegisterController ()<UITableViewDataSource,UITableViewDelegate,ADRegisterPhoneDelegate>

@end

@implementation ADRegisterController{
    ADRegisterModel *regModel;
    NSMutableArray *dataArr;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    //
    regModel=[ADRegisterModel new];
    regModel.realName=_model.realName;
    regModel.personId=_model.personId;
    regModel.regValidate=_model.validate;
    regModel.loginAccount=@"";
    regModel.loginPass=@"";
    regModel.phoneValidate=@"";
    dataArr=[NSMutableArray array];
    [dataArr addObject:regModel];
    
    UITableView *myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.scrollEnabled=NO;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"CheckADLoginRegistCell" bundle:nil] forCellReuseIdentifier:@"CheckADLoginRegistCell"];
    [myTable registerNib:[UINib nibWithNibName:@"ADRegisterPhoneValidateCell" bundle:nil] forCellReuseIdentifier:@"ADRegisterPhoneValidateCell"];
    [self.view addSubview:myTable];
    
    UIView  *btmView01=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 100)];
    myTable.tableFooterView=btmView01;
    
    UIButton *loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame=CGRectMake(18, 18, SCREENSIZE.width-36, 44);
    loginBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    loginBtn.layer.cornerRadius=4;
    loginBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [loginBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
    [btmView01 addSubview:loginBtn];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"用户注册" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - 点击下一步
-(void)clickSubmit{
    [AppUtils closeKeyboard];
    if ([regModel.loginAccount isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"登录名不能为空" inView:self.view];
    }else if ([regModel.loginPass isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"登陆不能为空" inView:self.view];
    }else if ([regModel.confirmPass isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"确认密码不能为空" inView:self.view];
    }else if ([regModel.phone isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"手机号不能为空" inView:self.view];
    }else if ([regModel.phoneValidate isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"手机验证码不能为空" inView:self.view];
    }else{
        [self submitRequest];
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?18:0.0000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 18;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==4) {
        ADRegisterPhoneValidateCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ADRegisterPhoneValidateCell"];
        if (!cell) {
            cell=[[ADRegisterPhoneValidateCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ADRegisterPhoneValidateCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.leftImgv setImage:[UIImage imageNamed:@"czx_valide"] forState:UIControlStateNormal];
        ADRegisterModel *model=dataArr[0];
        [cell reflushDataForDictionary:model indexPath:indexPath];
        cell.delegate=self;
        cell.ADRegisterTextChangeBlock=^(NSString *text){
            model.phoneValidate=text;
        };
        [dataArr replaceObjectAtIndex:0 withObject:model];
        return  cell;
    }else{
        CheckADLoginRegistCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADLoginRegistCell"];
        if (!cell) {
            cell=[[CheckADLoginRegistCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADLoginRegistCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.leftBtn setImage:[UIImage imageNamed:indexPath.section==0?@"czx_user":indexPath.section==1?@"czx_pass":indexPath.section==2?@"czx_pass":@"czx_phone"] forState:UIControlStateNormal];
        cell.putTextField.placeholder=indexPath.section==0?@"请输入登录名,1-16位数字字母":indexPath.section==1?@"请输入登录密码,6-20位数字字母组合":indexPath.section==2?@"请再次输入登录密码":@"请输入手机号";
        ADRegisterModel *model=dataArr[0];
        [cell reflushADRegister:model indexPath:indexPath];
        cell.CheckADTextChangeBlock=^(NSString *text){
            switch (indexPath.section) {
                case 0:
                    model.loginAccount=text;
                    break;
                case 1:
                    model.loginPass=text;
                    break;
                case 2:
                    model.confirmPass=text;
                    break;
                case 3:
                    model.phone=text;
                    break;
                default:
                    break;
            }
        };
        [dataArr replaceObjectAtIndex:0 withObject:model];
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击获取验证码
-(void)clickGetValidateBtn{
    [AppUtils closeKeyboard];
    if ([regModel.loginAccount isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"登录名不能为空" inView:self.view];
    }else if ([regModel.loginPass isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"登陆不能为空" inView:self.view];
    }else if ([regModel.confirmPass isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"确认密码不能为空" inView:self.view];
    }else if ([regModel.phone isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"手机号不能为空" inView:self.view];
    }else{
        [self getValidateRequest];
    }
}

#pragma mark - request
-(void)getValidateRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"get_smscode",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"phone_no":regModel.phone,
                         @"cardno":regModel.personId,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ADR_SendValidate_Success" object:nil];
            [AppUtils showSuccessMessage:@"发送验证码成功！" inView:self.view];
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"post_userinfo",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"name":regModel.realName,
                         @"sms_code":regModel.phoneValidate,
                         @"cardno":regModel.personId,
                         @"login_name":regModel.loginAccount,
                         @"passwd":regModel.loginPass,
                         @"phone_no":regModel.phone,
                         @"email":@""
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"注册成功"preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[CheckADController class]]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckADRegist_Success" object:nil];
                        [self.navigationController popToViewController:controller animated:YES];
                    }
                }
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        //
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
