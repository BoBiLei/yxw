//
//  ADRegisterController.h
//  youxia
//
//  Created by mac on 2017/1/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckADRegModel.h"

@interface ADRegisterController : UIViewController

@property (nonatomic, strong) CheckADRegModel *model;

@end
