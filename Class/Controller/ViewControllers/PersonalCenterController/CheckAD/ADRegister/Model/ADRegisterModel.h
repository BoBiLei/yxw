//
//  ADRegisterModel.h
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADRegisterModel : NSObject

@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *regValidate;
@property (nonatomic, copy) NSString *personId;

@property (nonatomic, copy) NSString *loginAccount;
@property (nonatomic, copy) NSString *loginPass;
@property (nonatomic, copy) NSString *confirmPass;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *phoneValidate;

@end
