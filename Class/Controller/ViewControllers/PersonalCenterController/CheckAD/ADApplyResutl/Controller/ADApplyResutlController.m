//
//  ADApplyResutlController.m
//  youxia
//
//  Created by mac on 2017/1/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADApplyResutlController.h"
#import "ADApplyResutlCell.h"
#import "CheckADController.h"
#import "NewApproveInfo.h"

@interface ADApplyResutlController ()<UITableViewDataSource,UITableViewDelegate,ADApplyResutlDelegate>

@end

@implementation ADApplyResutlController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    //
    UITableView *myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:myTable];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#55b2f0"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"申请结果";
    [self.view addSubview:titleLabel];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREENSIZE.height-64;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ADApplyResutlCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ADApplyResutlCell"];
    if (!cell) {
        cell=[[ADApplyResutlCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ADApplyResutlCell"];
    }
    cell.delegate=self;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击知道了
-(void)clickYES{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

@end
