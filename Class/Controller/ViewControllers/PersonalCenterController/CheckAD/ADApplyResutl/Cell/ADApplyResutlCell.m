//
//  ADApplyResutlCell.m
//  youxia
//
//  Created by mac on 2017/1/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADApplyResutlCell.h"

@implementation ADApplyResutlCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        
        UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 76, 76)];
        imgv.center=CGPointMake(SCREENSIZE.width/2, 76);
        imgv.contentMode=UIViewContentModeScaleAspectFit;
        imgv.image=[UIImage imageNamed:@"czx_resutl"];
        [self addSubview:imgv];
        
        //
        UILabel *label01=[[UILabel alloc] initWithFrame:CGRectMake(0, imgv.origin.y+imgv.height+14, SCREENSIZE.width, 24)];
        label01.font=[UIFont systemFontOfSize:16];
        label01.text=@"报告申请中";
        label01.textAlignment=NSTextAlignmentCenter;
        [self addSubview:label01];
        
        //
        UILabel *label02=[[UILabel alloc] initWithFrame:CGRectMake(18, label01.origin.y+label01.height+10, SCREENSIZE.width-36, 66)];
        label02.numberOfLines=0;
        label02.textColor=[UIColor colorWithHexString:@"949494"];
        label02.font=[UIFont systemFontOfSize:14];
        NSString *labelTtt=@"征信中心将在24小时内，\n将申请结果短信发送到您注册征信账号预留的手机上\n请收到验证码后放回获取征信报告";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment=NSTextAlignmentCenter;
        [paragraphStyle setLineSpacing:3];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        label02.attributedText = attributedString;
        [self addSubview:label02];
        
        //知道了按钮
        UIButton *loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        loginBtn.frame=CGRectMake(18, label02.origin.y+label02.height+36, SCREENSIZE.width-36, 44);
        loginBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        loginBtn.layer.cornerRadius=4;
        loginBtn.titleLabel.font=[UIFont systemFontOfSize:16];
        [loginBtn setTitle:@"知道了" forState:UIControlStateNormal];
        [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [loginBtn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:loginBtn];
    }
    return self;
}

-(void)clickSubmit{
    [self.delegate clickYES];
}

@end
