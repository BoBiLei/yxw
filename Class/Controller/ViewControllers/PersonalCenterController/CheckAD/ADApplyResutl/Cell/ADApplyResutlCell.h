//
//  ADApplyResutlCell.h
//  youxia
//
//  Created by mac on 2017/1/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADApplyResutlDelegate <NSObject>

-(void)clickYES;

@end

@interface ADApplyResutlCell : UITableViewCell

@property (weak, nonatomic) id<ADApplyResutlDelegate> delegate;

@end
