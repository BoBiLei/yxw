//
//  ADValidatePhone.m
//  youxia
//
//  Created by mac on 2017/2/9.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADValidatePhone.h"
#import "ADValidatePhoneCell.h"
#import "ADApplyResutlController.h"
#import "NewApproveInfo.h"


#define Headview_Height 130

@interface ADValidatePhone ()<UITableViewDataSource,UITableViewDelegate,ADValidatePhoneBtnDelegate>

@end

@implementation ADValidatePhone{
    BOOL isRefleshBtn;
    UITableView *myTable;
    
    UILabel *showName;
    UIImageView *tipPicture;
    
    NSString *yhValidateStr;    //央行验证码
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    yhValidateStr=@"";
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpHeaderView];
    
    [self setUpTableView];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#55b2f0"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"央行信用报告";
    [self.view addSubview:titleLabel];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

-(void)setUpHeaderView{
    UIImageView *headimgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, Headview_Height)];
    headimgv.image=[UIImage imageNamed:@"checkad_head"];
    [self.view addSubview:headimgv];
    
    //白线
    UIImageView *wLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headimgv.width, 1.5f)];
    wLine.center=CGPointMake(SCREENSIZE.width/2, headimgv.centerY-5);
    wLine.image=[UIImage imageNamed:@"cad_line"];
    [self.view addSubview:wLine];
    
    //
    UIImageView *leftImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 78, 76)];
    leftImgv.center=CGPointMake(SCREENSIZE.width+5, wLine.centerY-26);
    leftImgv.image=[UIImage imageNamed:@"check_head_right"];
    [self.view addSubview:leftImgv];
    
    //显示名称
    showName=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 21)];
    showName.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY+19);
    showName.font=kFontSize15;
    showName.textColor=[UIColor whiteColor];
    showName.textAlignment=NSTextAlignmentCenter;
    showName.text=@"验证身份";
    [self.view addSubview:showName];
    
    //小白点
    UIImageView *domImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    domImgv.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY);
    domImgv.image=[UIImage imageNamed:@"check_head_dom"];
    [self.view addSubview:domImgv];
    
    UIImageView *dom_center=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    dom_center.center=CGPointMake(SCREENSIZE.width/4-2, wLine.centerY);
    dom_center.image=[UIImage imageNamed:@"check_dom_zq"];
    [self.view addSubview:dom_center];
    
    //
    tipPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52, 52)];
    tipPicture.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY-tipPicture.height/2-3);
    tipPicture.image=[UIImage imageNamed:@"check_head_xybg"];
    [self.view addSubview:tipPicture];
    
    UIImageView *cenPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
    cenPicture.center=CGPointMake(dom_center.centerX, wLine.centerY-cenPicture.height/2-2);
    cenPicture.image=[UIImage imageNamed:@"check_head_left"];
    [self.view addSubview:cenPicture];
}

#pragma mark - SetUp Table
-(void)setUpTableView{
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, Headview_Height+64, SCREENSIZE.width, SCREENSIZE.height-(Headview_Height+64+48)) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.scrollEnabled=NO;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"ADValidatePhoneCell" bundle:nil] forCellReuseIdentifier:@"ADValidatePhoneCell"];
    [self.view addSubview:myTable];
    
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 400)];
    myTable.tableFooterView=footView;
    
    UIImageView *bgImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width*299/500)];
    bgImgv.image=[UIImage imageNamed:@"check_bottom_phonebg"];   //500x299
    [footView addSubview:bgImgv];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(18, bgImgv.origin.y+bgImgv.height, footView.width-36, 54)];
    tipLabel.textColor=[UIColor colorWithHexString:@"ababab"];
    tipLabel.font=kFontSize16;
    tipLabel.numberOfLines=0;
    NSString *labelTtt=@"您的账户安全等级较高,可直接通过注册绑定的手机号码验证安全身份。";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    tipLabel.attributedText = attributedString;
    [footView addSubview:tipLabel];
    
    //
    UIButton *nextBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    nextBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    nextBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [nextBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(clickNext) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextBtn];
}

-(void)clickNext{
    if ([yhValidateStr isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入央行验证码" inView:self.view];
    }else{
        [AppUtils closeKeyboard];
        [self submitQueryCodeRequest];
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 12;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ADValidatePhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ADValidatePhoneCell"];
    if (!cell) {
        cell=[[ADValidatePhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ADValidatePhoneCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.leftImgv setImage:[UIImage imageNamed:@"czx_valide"] forState:UIControlStateNormal];
    [cell reflushDataForString:yhValidateStr isRefleshBtn:isRefleshBtn];
    cell.delegate=self;
    cell.ADValidatePhoneBlock=^(NSString *text){
        yhValidateStr=text;
    };
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击获取验证码
-(void)clickGetValidateBtn{
    [self getQueryCodeRequest];
}

#pragma mark - Request
//获取快捷查询短信的
-(void)getQueryCodeRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"get_query_code",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            [AppUtils showSuccessMessage:@"已成功短信发送,请注意查收" inView:self.view];
            isRefleshBtn=YES;
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//提交快捷查询
-(void)submitQueryCodeRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"post_query_code",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"sms_code":yhValidateStr,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
            [self.navigationController pushViewController:[ADApplyResutlController new] animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
