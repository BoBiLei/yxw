//
//  ADValidatePhoneCell.h
//  youxia
//
//  Created by mac on 2017/2/9.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADValidatePhoneBtnDelegate <NSObject>

-(void)clickGetValidateBtn;

@end

@interface ADValidatePhoneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *leftImgv;
@property (weak, nonatomic) IBOutlet UIButton *valideBtn;
@property (weak, nonatomic) IBOutlet UITextField *myTextField;

@property (nonatomic, copy) void (^ADValidatePhoneBlock)(NSString *text);
@property (weak,nonatomic) id<ADValidatePhoneBtnDelegate> delegate;

-(void)reflushDataForString:(NSString *)validate isRefleshBtn:(BOOL)isReflesh;

@end
