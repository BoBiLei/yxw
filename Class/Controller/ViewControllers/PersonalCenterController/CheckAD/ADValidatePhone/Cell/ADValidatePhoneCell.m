//
//  ADValidatePhoneCell.m
//  youxia
//
//  Created by mac on 2017/2/9.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ADValidatePhoneCell.h"

#define RESETTIME 60
@implementation ADValidatePhoneCell{
    NSInteger timerCount;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.valideBtn.layer.cornerRadius=3;
    
    [_myTextField setValue:[UIColor colorWithHexString:@"ababab"]forKeyPath:@"_placeholderLabel.textColor"];
    [_myTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, self.height, SCREENSIZE.width-36, 0.7f)];
    line.backgroundColor=[UIColor colorWithHexString:@"ababab"];
    [_mainView addSubview:line];
}

-(void)reflushDataForString:(NSString *)validate isRefleshBtn:(BOOL)isReflesh{
    _myTextField.text=validate;
    if (isReflesh) {
        timerCount=RESETTIME;
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
    }
}

-(void)setTimer:(NSTimer *)timer{
    _valideBtn.backgroundColor=[UIColor colorWithHexString:@"#ababab"];
    _valideBtn.enabled=NO;
    [_valideBtn setTitle:[NSString stringWithFormat:@"重发(%ldS)",(long)timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [_valideBtn setTitle:@"获取手机短信" forState:UIControlStateNormal];
        _valideBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        _valideBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

#pragma mark - textField
- (void)textChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.ADValidatePhoneBlock) {
        self.ADValidatePhoneBlock(field.text);
    }
}

- (IBAction)clickBtn:(id)sender {
    [AppUtils closeKeyboard];
    [self.delegate clickGetValidateBtn];
}

@end
