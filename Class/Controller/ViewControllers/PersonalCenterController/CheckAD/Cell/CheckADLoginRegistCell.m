//
//  CheckADLoginRegistCell.m
//  youxia
//
//  Created by mac on 2017/1/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CheckADLoginRegistCell.h"

@implementation CheckADLoginRegistCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_putTextField setValue:[UIColor colorWithHexString:@"ababab"]forKeyPath:@"_placeholderLabel.textColor"];
    [_putTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, self.height, SCREENSIZE.width-36, 0.7f)];
    line.backgroundColor=[UIColor colorWithHexString:@"ababab"];
    [_mainView addSubview:line];
}

#pragma mark - textField
- (void)textChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.CheckADTextChangeBlock) {
        self.CheckADTextChangeBlock(field.text);
    }
}

-(void)reflushLoginWithModel:(CheckADLoginModel *)model indexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            _putTextField.text=model.loginName;
            break;
        case 1:
            _putTextField.text=model.loginPass;
            break;
        default:
            break;
    }
}

-(void)reflushDataForDictionary:(CheckADRegModel *)model indexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            _putTextField.text=model.realName;
            break;
        case 1:
            _putTextField.text=model.personId;
            break;
        default:
            break;
    }
}

-(void)reflushADRegister:(ADRegisterModel *)model indexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            _putTextField.text=model.loginAccount;
            break;
        case 1:
            _putTextField.text=model.loginPass;
            break;
        case 2:
            _putTextField.text=model.confirmPass;
            break;
        case 3:
            _putTextField.text=model.phone;
            break;
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
