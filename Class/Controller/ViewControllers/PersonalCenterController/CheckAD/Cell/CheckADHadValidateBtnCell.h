//
//  CheckADHadValidateBtnCell.h
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckADRegModel.h"
#import "CheckADLoginModel.h"

@protocol CheckADHadValidateBtnDelegate <NSObject>

-(void)clickGetValidateBtn;

@end

@interface CheckADHadValidateBtnCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *leftImgv;
@property (weak, nonatomic) IBOutlet UITextField *myTextField;
@property (weak, nonatomic) IBOutlet UIButton *valideBtn;

@property (weak,nonatomic) id<CheckADHadValidateBtnDelegate> delegate;

@property (nonatomic, copy) void (^CheckADTextChangeBlock)(NSString *text);

-(void)reflushLoginWithModel:(CheckADLoginModel *)model indexPath:(NSIndexPath *)indexPath;

-(void)reflushDataForDictionary:(CheckADRegModel *)model indexPath:(NSIndexPath *)indexPath;

@end
