//
//  CheckADHadValidateBtnCell.m
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CheckADHadValidateBtnCell.h"

@implementation CheckADHadValidateBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.valideBtn.layer.cornerRadius=3;
    
    [_myTextField setValue:[UIColor colorWithHexString:@"ababab"]forKeyPath:@"_placeholderLabel.textColor"];
    [_myTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, self.height, SCREENSIZE.width-36, 0.7f)];
    line.backgroundColor=[UIColor colorWithHexString:@"ababab"];
    [_mainView addSubview:line];
}

#pragma mark - textField
- (void)textChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.CheckADTextChangeBlock) {
        self.CheckADTextChangeBlock(field.text);
    }
}

-(void)reflushLoginWithModel:(CheckADLoginModel *)model indexPath:(NSIndexPath *)indexPath{
    _myTextField.text=model.validate;
    if (![model.imgUrl isEqualToString:@""]) {
        _valideBtn.backgroundColor=[UIColor clearColor];
        [_valideBtn setTitle:@"" forState:UIControlStateNormal];
        [_valideBtn setBackgroundImageWithURL:[NSURL URLWithString:model.imgUrl] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"category_bg"]];
    }
}

-(void)reflushDataForDictionary:(CheckADRegModel *)model indexPath:(NSIndexPath *)indexPath{
    _myTextField.text=model.validate;
    if (![model.imgUrl isEqualToString:@""]) {
        _valideBtn.backgroundColor=[UIColor clearColor];
        [_valideBtn setTitle:@"" forState:UIControlStateNormal];
        [_valideBtn setBackgroundImageWithURL:[NSURL URLWithString:model.imgUrl] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"category_bg"]];
    }
}

- (IBAction)clickBtn:(id)sender {
    [self.delegate clickGetValidateBtn];
}

@end
