//
//  CheckADLoginRegistCell.h
//  youxia
//
//  Created by mac on 2017/1/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckADRegModel.h"
#import "ADRegisterModel.h"
#import "CheckADLoginModel.h"

@interface CheckADLoginRegistCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UITextField *putTextField;

@property (nonatomic, copy) void (^CheckADTextChangeBlock)(NSString *text);

-(void)reflushLoginWithModel:(CheckADLoginModel *)model indexPath:(NSIndexPath *)indexPath;

-(void)reflushDataForDictionary:(CheckADRegModel *)model indexPath:(NSIndexPath *)indexPath;

-(void)reflushADRegister:(ADRegisterModel *)model indexPath:(NSIndexPath *)indexPath;

@end
