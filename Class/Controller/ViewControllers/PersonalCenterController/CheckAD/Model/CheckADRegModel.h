//
//  CheckADRegModel.h
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckADRegModel : NSObject

@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *personId;
@property (nonatomic, copy) NSString *validate;
@property (nonatomic, copy) NSString *imgUrl;

@end
