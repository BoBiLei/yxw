//
//  CheckADLoginModel.h
//  youxia
//
//  Created by mac on 2017/1/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckADLoginModel : NSObject

@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *loginPass;
@property (nonatomic, copy) NSString *validate;
@property (nonatomic, copy) NSString *imgUrl;

@end
