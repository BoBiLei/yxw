//
//  CheckADController.h
//  youxia
//
//  Created by mac on 2016/12/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckADLoginModel.h"


@interface CheckADController : UIViewController

@property (nonatomic, assign) BOOL isHadLogin;
@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *loginPass;
@property (nonatomic, copy) NSString *imgUrl;

@end
