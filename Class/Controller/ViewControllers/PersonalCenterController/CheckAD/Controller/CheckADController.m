//
//  CheckADController.m
//  youxia
//
//  Created by mac on 2016/12/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CheckADController.h"
#import "CheckADLoginRegistCell.h"
#import "CheckADHadValidateBtnCell.h"
#import "ADRegisterController.h"
#import "CheckADRegModel.h"
#import "CheckADLoginModel.h"
#import "CustomIOSAlertView.h"
#import "ADputRHValidateController.h"
#import "OverdueRecord.h"
#import "SubmitQuestionController.h"
#import "ADValidatePhone.h"
#import "NewApproveInfo.h"

#define Headview_Height 130
#define RESETTIME 60

@interface CheckADController ()<UITableViewDataSource,UITableViewDelegate,CheckADHadValidateBtnDelegate,TTTAttributedLabelDelegate>

@end

@implementation CheckADController{
    
    BOOL isShowAlert;
    UITextField *valiTF;        //登录账号验证码
    
    UILabel *showName;
    UIImageView *tipPicture;
    
    //
    UIButton *agreeBtn;
    BOOL isAgree;
    
    CustomIOSAlertView *validaAlertView;
    UIButton *validaBtn;
    NSInteger timerCount;
    NSString *validateStr;
    
    UISegmentedControl *mySegment;
    UIScrollView *tableScroller;
    
    BOOL isRester;
    
    //
    UITableView *loginTable;
    CheckADLoginModel *loginModel;
    NSMutableArray *loginDataArr;
    
    //
    UITableView *registerTable;
    CheckADRegModel *registModel;
    NSMutableArray *registDataArr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSegment) name:@"CheckADRegist_Success" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateShowAlert) name:@"PutRH_noti_success" object:nil];
    
    [self setNavBar];
    
    [self setUpHeaderView];
    
    [self setUpSegment];
    
    [self setUpScrollerView];
    
    if (_isHadLogin) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShowAlert=YES;
            [self showAlert];
        });
    }
}

-(void)updateShowAlert{
    isShowAlert=NO;
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"央行信用报告" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[NewApproveInfo class]]) {
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
}

#pragma mark - NSNotificationCenter Method
-(void)updateSegment{
    [mySegment setSelectedSegmentIndex:0];
    showName.text=@"登录央行账号";
    tipPicture.image=[UIImage imageNamed:@"check_head_login"];
    loginModel.loginName=@"";
    loginModel.loginPass=@"";
    loginModel.validate=@"";
    [loginTable reloadSection:2 withRowAnimation:UITableViewRowAnimationNone];
}

-(void)setUpHeaderView{
    UIImageView *headimgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, Headview_Height)];
    headimgv.image=[UIImage imageNamed:@"checkad_head"];
    [self.view addSubview:headimgv];
    
    //白线
    UIImageView *wLine=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headimgv.width, 1.5f)];
    wLine.center=CGPointMake(SCREENSIZE.width/2, headimgv.centerY-5);
    wLine.image=[UIImage imageNamed:@"cad_line"];
    [self.view addSubview:wLine];
    
    //
    UIImageView *leftImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 78, 76)];
    leftImgv.center=CGPointMake(SCREENSIZE.width+5, wLine.centerY-26);
    leftImgv.image=[UIImage imageNamed:@"check_head_right"];
    [self.view addSubview:leftImgv];
    
    //显示名称
    showName=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 21)];
    showName.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY+19);
    showName.font=kFontSize15;
    showName.textColor=[UIColor whiteColor];
    showName.textAlignment=NSTextAlignmentCenter;
    showName.text=@"登录央行账号";
    [self.view addSubview:showName];
    
    //小白点
    UIImageView *domImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    domImgv.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY);
    domImgv.image=[UIImage imageNamed:@"check_head_dom"];
    [self.view addSubview:domImgv];
    
    //
    tipPicture=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 52, 52)];
    tipPicture.center=CGPointMake(SCREENSIZE.width/2, wLine.centerY-tipPicture.height/2-3);
    tipPicture.image=[UIImage imageNamed:@"check_head_login"];
    [self.view addSubview:tipPicture];
}

-(void)setUpSegment{
    mySegment=[[UISegmentedControl alloc] initWithItems:@[@"登 录",@"注 册"]];
    mySegment.layer.cornerRadius = 5;
    mySegment.layer.masksToBounds = YES;
    //修改字体的默认颜色与选中颜色
    [mySegment setTitleTextAttributes:@{
                                        NSForegroundColorAttributeName:
                                            [UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:16]}
                             forState:UIControlStateSelected];
    [mySegment setTitleTextAttributes:@{NSForegroundColorAttributeName:
                                            [UIColor colorWithHexString:@"55b2f0"],
                                        NSFontAttributeName:[UIFont systemFontOfSize:16]}
                             forState:UIControlStateNormal];
    mySegment.frame=CGRectMake(18, Headview_Height+70, SCREENSIZE.width-36, 34);
    mySegment.selectedSegmentIndex=0;
    mySegment.backgroundColor=[UIColor whiteColor];
    mySegment.tintColor=[UIColor colorWithHexString:@"55b2f0"];
    [mySegment addTarget:self action:@selector(switchingView:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySegment];
}

#pragma mark - SelectedSegment
-(void)switchingView:(id)sender{
    [AppUtils closeKeyboard];
    UISegmentedControl *segment=sender;
    isRester=segment.selectedSegmentIndex==0?NO:YES;
    showName.text=segment.selectedSegmentIndex==0?@"登录央行账号":@"注册央行账号";
    tipPicture.image=[UIImage imageNamed:segment.selectedSegmentIndex==0?@"check_head_login":@"check_head_regist"];
    [tableScroller setContentOffset:CGPointMake(segment.selectedSegmentIndex*SCREENSIZE.width, 0) animated:NO];
}

-(void)setUpScrollerView{
    tableScroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, mySegment.origin.y+mySegment.height, SCREENSIZE.width, SCREENSIZE.height-(mySegment.origin.y+mySegment.height))];
    tableScroller.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    tableScroller.bounces=NO;
    tableScroller.showsHorizontalScrollIndicator=NO;
    tableScroller.showsVerticalScrollIndicator=NO;
    tableScroller.scrollEnabled=NO;
    tableScroller.pagingEnabled=NO;
    [self.view addSubview:tableScroller];
    
    isRester=NO;
    
    //登录
    loginModel=[CheckADLoginModel new];
    loginModel.loginName=_loginName;
    loginModel.loginPass=_loginPass;
    loginModel.imgUrl=_imgUrl;
    loginDataArr=[NSMutableArray array];
    [loginDataArr addObject:loginModel];
    
    loginTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 14, tableScroller.width, tableScroller.height-18-48) style:UITableViewStyleGrouped];
    loginTable.backgroundColor=[UIColor whiteColor];
    loginTable.dataSource=self;
    loginTable.delegate=self;
    loginTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [loginTable registerNib:[UINib nibWithNibName:@"CheckADLoginRegistCell" bundle:nil] forCellReuseIdentifier:@"CheckADLoginRegistCell"];
    [loginTable registerNib:[UINib nibWithNibName:@"CheckADHadValidateBtnCell" bundle:nil] forCellReuseIdentifier:@"CheckADHadValidate"];
    [tableScroller addSubview:loginTable];
    
    UIView  *btmView01=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 100)];
    loginTable.tableFooterView=btmView01;
    
    UIButton *loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame=CGRectMake(0, tableScroller.height-48, SCREENSIZE.width, 48);
    loginBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    loginBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(clickLogin) forControlEvents:UIControlEventTouchUpInside];
    [tableScroller addSubview:loginBtn];
    
    //注册
    registModel=[CheckADRegModel new];
    registModel.realName=@"";
    registModel.personId=@"";
    registModel.validate=@"";
    registModel.imgUrl=@"";
    registDataArr=[NSMutableArray array];
    [registDataArr addObject:registModel];
    
    registerTable=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 14, tableScroller.width, tableScroller.height-18) style:UITableViewStyleGrouped];
    registerTable.backgroundColor=[UIColor whiteColor];
    registerTable.dataSource=self;
    registerTable.delegate=self;
    registerTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [registerTable registerNib:[UINib nibWithNibName:@"CheckADLoginRegistCell" bundle:nil] forCellReuseIdentifier:@"CheckADLoginRegistCell"];
    [registerTable registerNib:[UINib nibWithNibName:@"CheckADHadValidateBtnCell" bundle:nil] forCellReuseIdentifier:@"CheckADHadValidateBtnCell"];
    [tableScroller addSubview:registerTable];
    
    UIView  *btmView02=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 100)];
    registerTable.tableFooterView=btmView02;
    
    agreeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    agreeBtn.frame=CGRectMake(18, 16, 21, 21);
    [agreeBtn setImage:[UIImage imageNamed:@"czx_agree"] forState:UIControlStateNormal];
    [agreeBtn addTarget:self action:@selector(clickAgree) forControlEvents:UIControlEventTouchUpInside];
    [btmView02 addSubview:agreeBtn];
    
    TTTAttributedLabel *xyLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(agreeBtn.origin.x+agreeBtn.width+4, agreeBtn.origin.y, 200, agreeBtn.height)];
    xyLabel.font=[UIFont systemFontOfSize:15];
    xyLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    xyLabel.delegate=self;
    [btmView02 addSubview:xyLabel];
    NSString *stageStr=@"我已阅读并同意《服务协议》";
    __block NSRange sumRange;
    [xyLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        sumRange = [[mutableAttributedString string] rangeOfString:@"《服务协议》" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"55b2f0"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    //NO 不显示下划线
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
    [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    xyLabel.linkAttributes = linkAttributes;
    
    [xyLabel addLinkToURL:[NSURL URLWithString:@""] withRange:sumRange];
    
    //
    UIButton *nextBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame=CGRectMake(SCREENSIZE.width, tableScroller.height-48, SCREENSIZE.width, 48);
    nextBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    nextBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn addTarget:self action:@selector(clickNext) forControlEvents:UIControlEventTouchUpInside];
    [tableScroller addSubview:nextBtn];
}

#pragma mark - 点击注册下一步
-(void)clickNext{
    [AppUtils closeKeyboard];
    if ([registModel.realName isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"真实姓名不能为空" inView:self.view];
    }else if ([registModel.personId isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"真实身份证号不能为空" inView:self.view];
    }else if ([registModel.validate isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"验证码不能为空" inView:self.view];
    }else if(!isAgree){
        [AppUtils showSuccessMessage:@"您未同意服务协议" inView:self.view];
    }else{
        [self registerNextRequest];
    }
}

#pragma mark - 点击登录
-(void)clickLogin{
    [AppUtils closeKeyboard];
    if ([loginModel.loginName isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"用户名不能为空" inView:self.view];
    }else if ([loginModel.loginPass isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"密码不能为空" inView:self.view];
    }
    else{
        [self getLoginValidateRequest];
    }
}

-(void)showAlert{
    [AppUtils closeKeyboard];
    validaAlertView = [[CustomIOSAlertView alloc] init];
    validaAlertView.containerView.layer.cornerRadius=10;
    [validaAlertView setContainerView:[self createAlertView]];
    [validaAlertView setButtonTitles:nil];
    [validaAlertView setUseMotionEffects:true];
    [validaAlertView show];
}

#pragma mark - 创建弹出验证码提示框

-(UIView *)createAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-108, 300)];
    view.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    view.layer.cornerRadius=8;
    
    //
    UIImageView *logo=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    logo.center=CGPointMake(view.width/2, logo.width/2+24);
    logo.image=[UIImage imageNamed:@"checkad_logo"];
    [view addSubview:logo];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, logo.origin.y+logo.height+14, view.width, 24)];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.textColor=[UIColor colorWithHexString:@"000000"];
    tipLabel.text=@"请输入验证码";
    [view addSubview:tipLabel];
    
#pragma mark x按钮
    UIButton *closeImg=[UIButton buttonWithType:UIButtonTypeCustom] ;
    closeImg.frame=CGRectMake(view.width-40, 4, 36, 36);
    [closeImg setImage:[UIImage imageNamed:@"getpass_delete"] forState:UIControlStateNormal];
    [closeImg addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeImg];
    
#pragma mark 验证码View
    UIView *volatileView=[[UIView alloc] initWithFrame:CGRectMake(22, tipLabel.origin.y+tipLabel.height+14, view.width-44, 40)];
    volatileView.layer.cornerRadius=3;
    volatileView.layer.borderWidth=0.5f;
    volatileView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [view addSubview:volatileView];
    
    valiTF=[[UITextField alloc] initWithFrame:CGRectMake(8, 0, (volatileView.width-16)/2, volatileView.height)];
    valiTF.tintColor=[UIColor colorWithHexString:@"#55b2f0"];
    valiTF.placeholder=@"输入验证码";
    valiTF.font=[UIFont systemFontOfSize:15];
    valiTF.textColor=[UIColor colorWithHexString:@"#949494"];
    valiTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [valiTF addTarget:self action:@selector(valiTFDidChange:) forControlEvents:UIControlEventEditingChanged];
    [volatileView addSubview:valiTF];
    
#pragma mark 验证码按钮
    validaBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    validaBtn.frame=CGRectMake(6+(volatileView.width-12)/2, 4, (volatileView.width-12)/2,volatileView.height-8);
    validaBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    //picValidStr
    [validaBtn setBackgroundImageWithURL:[NSURL URLWithString:loginModel.imgUrl] forState:UIControlStateNormal placeholder:nil];
    [validaBtn addTarget:self action:@selector(clickGetValidate) forControlEvents:UIControlEventTouchUpInside];
    [volatileView addSubview:validaBtn];
    
    //
    CGFloat lastYH=[self createBottomBtn:@[@"确认"] andToView:view afterView:volatileView];
    
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+1;
    view.frame=frame;
    
    return view;
}

-(void)clickGetValidate{
    [self getLoginValidateRequest];
}

-(void)setTimer:(NSTimer *)timer{
    validaBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    validaBtn.enabled=NO;
    [validaBtn setTitle:[NSString stringWithFormat:@"重发(%ldS)",(long)timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [validaBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        validaBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        validaBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

-(void)clickCloseBtn{
    isShowAlert=NO;
    [validaAlertView close];
}

- (void)valiTFDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    validateStr=field.text;
    loginModel.validate=field.text;
}

-(CGFloat)createBottomBtn:(NSArray *)array andToView:(UIView *)view afterView:(UIView *)afterView{
    
    CGFloat lastYH=0;
    CGFloat btnX=37;
    CGFloat btnY=afterView.origin.y+afterView.height+18;
    CGFloat btnW=view.width;
    CGFloat btnHeight=52;
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, btnY, view.width, 0.5f)];
    line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [view addSubview:line];
    
    for (int i=0; i<array.count; i++) {
        btnX=2;
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnX, btnY+1, btnW-4, btnHeight);
        [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:17];
        [btn setTitle:array[i] forState:UIControlStateNormal];
        btn.backgroundColor=[UIColor whiteColor];
        btn.tag=i;
        [btn addTarget:self action:@selector(clcikConfrimBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        lastYH=btn.origin.y+btn.height+1;
    }
    return lastYH;
}

-(void)clcikConfrimBtn:(id)sender{
    if ([valiTF.text isEqualToString:@""]||valiTF.text==nil) {
        [AppUtils showSuccessMessage:@"请输入验证码" inView:validaAlertView.containerView];
    }else{
        [self submitLoginRequest];
    }
}

#pragma mark - TTTAttributedLabelDelegate 点击服务协议

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"点击服务协议");
}

#pragma mark - 点击同意
-(void)clickAgree{
    //czx_noagree  czx_agree
    if (isAgree) {
        [agreeBtn setImage:[UIImage imageNamed:@"czx_agree"] forState:UIControlStateNormal];
    }else{
        [agreeBtn setImage:[UIImage imageNamed:@"czx_noagree"] forState:UIControlStateNormal];
    }
    isAgree=!isAgree;
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView==loginTable) {
        return 2;
    }
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==loginTable) {
        if (indexPath.section==2) {
            CheckADHadValidateBtnCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADHadValidate"];
            if (!cell) {
                cell=[[CheckADHadValidateBtnCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADHadValidate"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.leftImgv setImage:[UIImage imageNamed:@"czx_valide"] forState:UIControlStateNormal];
            CheckADLoginModel *model=loginDataArr[0];
            [cell reflushLoginWithModel:model indexPath:indexPath];
            cell.delegate=self;
            cell.CheckADTextChangeBlock=^(NSString *text){
                loginModel.validate=text;
            };
            [loginDataArr replaceObjectAtIndex:0 withObject:loginModel];
            return  cell;
        }else{
            CheckADLoginRegistCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADLoginRegistCell"];
            if (!cell) {
                cell=[[CheckADLoginRegistCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADLoginRegistCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.leftBtn setImage:[UIImage imageNamed:indexPath.section==0?@"czx_user":indexPath.section==1?@"czx_pass":@"czx_valide"] forState:UIControlStateNormal];
            cell.putTextField.placeholder=indexPath.section==0?@"请输入人行征信用户名":indexPath.section==1?@"请输入人行征信密码":@"请输入验证码";
            cell.putTextField.secureTextEntry=indexPath.section==1?YES:NO;
            CheckADLoginModel *model=loginDataArr[0];
            [cell reflushLoginWithModel:model indexPath:indexPath];
            cell.CheckADTextChangeBlock=^(NSString *text){
                switch (indexPath.section) {
                    case 0:
                        loginModel.loginName=text;
                        break;
                    case 1:
                        loginModel.loginPass=text;
                        break;
                    default:
                        break;
                }
            };
            [loginDataArr replaceObjectAtIndex:0 withObject:loginModel];
            return  cell;
        }
    }else{
        if (indexPath.section==2) {
            CheckADHadValidateBtnCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADHadValidateBtnCell"];
            if (!cell) {
                cell=[[CheckADHadValidateBtnCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADHadValidateBtnCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.leftImgv setImage:[UIImage imageNamed:@"czx_valide"] forState:UIControlStateNormal];
            CheckADRegModel *model=registDataArr[0];
            [cell reflushDataForDictionary:model indexPath:indexPath];
            cell.delegate=self;
            cell.CheckADTextChangeBlock=^(NSString *text){
                registModel.validate=text;
            };
            [registDataArr replaceObjectAtIndex:0 withObject:registModel];
            return  cell;
        }else{
            CheckADLoginRegistCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CheckADLoginRegistCell"];
            if (!cell) {
                cell=[[CheckADLoginRegistCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckADLoginRegistCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.leftBtn setImage:[UIImage imageNamed:indexPath.section==0?@"czx_user":indexPath.section==1?@"czx_pid":@"czx_valide"] forState:UIControlStateNormal];
            cell.putTextField.placeholder=indexPath.section==0?@"请输入您的真实姓名":indexPath.section==1?@"请输入您的真实身份证号":@"请输入验证码";
            CheckADRegModel *model=registDataArr[0];
            [cell reflushDataForDictionary:model indexPath:indexPath];
            cell.CheckADTextChangeBlock=^(NSString *text){
                switch (indexPath.section) {
                    case 0:
                        registModel.realName=text;
                        break;
                    case 1:
                        registModel.personId=text;
                        break;
                    default:
                        break;
                }
            };
            [registDataArr replaceObjectAtIndex:0 withObject:registModel];
            return  cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - （登录/注册）点击获取验证码
-(void)clickGetValidateBtn{
    [AppUtils closeKeyboard];
    if (isRester) {
        if ([registModel.realName isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"真实姓名不能为空" inView:self.view];
        }else if ([registModel.personId isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"真实身份证号不能为空" inView:self.view];
        }else if (![AppUtils validateIdentityCard:registModel.personId]) {
            [AppUtils showSuccessMessage:@"输入身份证号有误" inView:self.view];
        }else{
            [self getRegisterValidateRequest];
        }
    }else{
        if ([loginModel.loginName isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"用户名不能为空" inView:self.view];
        }else if ([loginModel.loginPass isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"密码能为空" inView:self.view];
        }else{
            [self getLoginValidateRequest];
        }
    }
    
}

#pragma mark - request

-(void)getLoginValidateRequest{
    [AppUtils showProgressInView:isShowAlert?validaAlertView.containerView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_credit" forKey:@"mod"];
    [dict setObject:@"get_login_code" forKey:@"code"];
    [dict setObject:loginModel.loginName forKey:@"login_name"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"uid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:isShowAlert?validaAlertView.containerView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            loginModel.imgUrl=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"data"][@"imgUrl"]];
            _imgUrl=loginModel.imgUrl;
            if (isShowAlert) {
                [validaBtn setBackgroundImageWithURL:[NSURL URLWithString:loginModel.imgUrl] forState:UIControlStateNormal placeholder:nil];
            }else{
                isShowAlert=YES;
                [self showAlert];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:isShowAlert?validaAlertView.containerView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)getValidateCode2{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_credit" forKey:@"mod"];
    [dict setObject:@"get_login_code" forKey:@"code"];
    [dict setObject:loginModel.loginName forKey:@"login_name"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"uid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:isShowAlert?validaAlertView.containerView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            
        }else{
            loginModel.imgUrl=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"data"][@"imgUrl"]];
            _imgUrl=loginModel.imgUrl;
            [validaBtn setBackgroundImageWithURL:[NSURL URLWithString:loginModel.imgUrl] forState:UIControlStateNormal placeholder:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)submitLoginRequest{
    [AppUtils showProgressInView:validaAlertView.containerView];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_credit" forKey:@"mod"];
    [dict setObject:@"login" forKey:@"code"];
    [dict setObject:loginModel.loginName forKey:@"login_name"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"uid"];
    [dict setObject:loginModel.loginPass forKey:@"passwd"];
    [dict setObject:loginModel.validate forKey:@"capcha"];
    [dict setObject:@"" forKey:@"idcode"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:validaAlertView.containerView];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [self getValidateCode2];
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:validaAlertView.containerView];
        }else{
            //idcode_status=1已获取过短信
            //canExpressQuery=1 1代表用户可通过快捷查询道获取身份验证码（跳到获取快捷查询短信），否则(为0时)通过安全问题获取身份验证码（跳到获取问题）
            //idcode_status=1&&status=1跳到征信报告
            [validaAlertView close];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSString *idCode_status=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"idcode_status"]];
                if ([idCode_status isEqualToString:@"1"]) {
                    NSString *status=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"status"]];
                    if ([status isEqualToString:@"1"]) {
                        //跳到征信报告
                        [self.navigationController pushViewController:[OverdueRecord new] animated:YES];
                    }else{
                        //跳到填写人行验证码
                        ADputRHValidateController *vc=[ADputRHValidateController new];
                        vc.loginName=loginModel.loginName;
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }else{
                    NSString *canExpress=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"canExpressQuery"]];
                    if ([canExpress isEqualToString:@"0"]) {
                        SubmitQuestionController *vc=[SubmitQuestionController new];
                        vc.loginName=loginModel.loginName;
                        [self.navigationController pushViewController:vc animated:YES];
                    }else if ([canExpress isEqualToString:@"1"]){
                        [self.navigationController pushViewController:[ADValidatePhone new] animated:YES];
                    }
                }
            });
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:validaAlertView.containerView];
        DSLog(@"%@",error);
    }];
}

-(void)getRegisterValidateRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"register_code",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"cardno":registModel.personId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            registModel.imgUrl=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"imgUrl"]];
            registModel.validate=@"";
        }
        [registerTable reloadSection:2 withRowAnimation:UITableViewRowAnimationNone];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)registerNextRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"m_credit",
                         @"code":@"check_real_name",
                         @"is_iso":@"1",
                         @"uid":[AppUtils getValueWithKey:User_ID],
                         @"name":registModel.realName,
                         @"cardno":registModel.personId,
                         @"capcha":registModel.validate,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            registModel.validate=@"";
            [registerTable reloadSection:2 withRowAnimation:UITableViewRowAnimationNone];
        }else{
            ADRegisterController *vc=[ADRegisterController new];
            vc.model=registModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
