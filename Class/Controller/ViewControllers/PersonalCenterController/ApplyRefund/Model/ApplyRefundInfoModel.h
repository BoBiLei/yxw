//
//  ApplyRefundInfoModel.h
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplyRefundInfoModel : NSObject

@property (nonatomic, copy) NSString *hotelLogo;
@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, copy) NSString *checkin;
@property (nonatomic, copy) NSString *checkout;
@property (nonatomic, copy) NSString *nights;
@property (nonatomic, copy) NSString *payMoney;
@property (nonatomic, copy) NSString *cancelMoney;
@property (nonatomic, retain) NSArray *refundReason;

-(void)jsonToModel:(NSDictionary *)dic;

@end
