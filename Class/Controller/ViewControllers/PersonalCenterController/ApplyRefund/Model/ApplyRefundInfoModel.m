//
//  ApplyRefundInfoModel.m
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundInfoModel.h"

@implementation ApplyRefundInfoModel

-(void)jsonToModel:(NSDictionary *)dic{
    //
    if ([dic[@"defaultImg"] isKindOfClass:[NSNull class]]) {
        self.hotelLogo=@"";
    }else{
        self.hotelLogo=dic[@"defaultImg"];
    }
    
    //
    if ([dic[@"hotelName"] isKindOfClass:[NSNull class]]) {
        self.hotelName=@"";
    }else{
        self.hotelName=dic[@"hotelName"];
    }
    
    //
    if ([dic[@"checkin"] isKindOfClass:[NSNull class]]) {
        self.checkin=@"";
    }else{
        self.checkin=dic[@"checkin"];
    }
    
    //
    if ([dic[@"checkout"] isKindOfClass:[NSNull class]]) {
        self.checkout=@"";
    }else{
        self.checkout=dic[@"checkout"];
    }
    
    //
    if ([dic[@"nights"] isKindOfClass:[NSNull class]]) {
        self.nights=@"";
    }else{
        self.nights=dic[@"nights"];
    }
    
    //
    if ([dic[@"refundMoney"] isKindOfClass:[NSNull class]]) {
        self.payMoney=@"0";
    }else{
        self.payMoney=dic[@"refundMoney"];
    }
    
    //
    if ([dic[@"cancelMoney"] isKindOfClass:[NSNull class]]) {
        self.cancelMoney=@"0";
    }else{
        self.cancelMoney=dic[@"cancelMoney"];
    }
    
    //
    if ([dic[@"refundReason"] isKindOfClass:[NSNull class]]) {
        self.refundReason=@[];
    }else{
        self.refundReason=dic[@"refundReason"];
    }
}

@end
