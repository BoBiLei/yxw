//
//  ApplyRefundReasonModel.m
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundReasonModel.h"

@implementation ApplyRefundReasonModel

-(void)jsonToModel:(NSDictionary *)dic{
    //
    if ([dic[@"ID"] isKindOfClass:[NSNull class]]) {
        self.reasonId=@"";
    }else{
        self.reasonId=[NSString stringWithFormat:@"%@",dic[@"ID"]];
    }
    
    //
    if ([dic[@"text"] isKindOfClass:[NSNull class]]) {
        self.reasonName=@"";
    }else{
        self.reasonName=dic[@"text"];
    }
}

@end
