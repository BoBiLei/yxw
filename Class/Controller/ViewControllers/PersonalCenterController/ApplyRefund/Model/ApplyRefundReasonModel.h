//
//  ApplyRefundReasonModel.h
//  youxia
//
//  Created by mac on 2017/5/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplyRefundReasonModel : NSObject

@property (nonatomic, copy) NSString *reasonId;
@property (nonatomic, copy) NSString *reasonName;

-(void)jsonToModel:(NSDictionary *)dic;

@end
