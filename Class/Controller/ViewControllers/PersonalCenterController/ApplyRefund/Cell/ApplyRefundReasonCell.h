//
//  ApplyRefundReasonCell.h
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplyRefundReasonModel.h"

@interface ApplyRefundReasonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *seleBtn;

-(void)reflushData:(ApplyRefundReasonModel *)model;

@end
