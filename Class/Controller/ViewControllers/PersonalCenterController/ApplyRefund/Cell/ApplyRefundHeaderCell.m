//
//  ApplyRefundHeaderCell.m
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundHeaderCell.h"

@implementation ApplyRefundHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(ApplyRefundInfoModel *)model{
    [self.hotelImgv sd_setImageWithURL:[NSURL URLWithString:model.hotelLogo] placeholderImage:[UIImage imageNamed:Image_Default]];
    self.nameLabel.text=model.hotelName;
    self.timeLabel.text=[NSString stringWithFormat:@"入住:%@ 离店:%@",model.checkin,model.checkout];
}

@end
