//
//  ApplyRefundReasonCell.m
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundReasonCell.h"

@implementation ApplyRefundReasonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        [self.seleBtn setImage:[UIImage imageNamed:@"hotelorder_sele"] forState:UIControlStateNormal];
    }else{
        [self.seleBtn setImage:[UIImage imageNamed:@"hotelorder_nor"] forState:UIControlStateNormal];
    }
}

-(void)reflushData:(ApplyRefundReasonModel *)model{
    self.titleLabel.text=model.reasonName;
}

@end
