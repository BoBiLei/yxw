//
//  ApplyRefundPriceCell.h
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplyRefundInfoModel.h"

@interface ApplyRefundPriceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UILabel *payMoneyLabel;
@property (weak, nonatomic) IBOutlet UIView *line;

-(void)reflushData:(ApplyRefundInfoModel *)model atIndexPatch:(NSIndexPath *)indexPatch;

@end
