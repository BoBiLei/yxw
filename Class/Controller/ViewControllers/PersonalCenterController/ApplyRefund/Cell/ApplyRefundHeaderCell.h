//
//  ApplyRefundHeaderCell.h
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplyRefundInfoModel.h"

@interface ApplyRefundHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *hotelImgv;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nightsLabel;

-(void)reflushData:(ApplyRefundInfoModel *)model;

@end
