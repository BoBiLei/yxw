//
//  ApplyRefundPriceCell.m
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundPriceCell.h"

@implementation ApplyRefundPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(ApplyRefundInfoModel *)model atIndexPatch:(NSIndexPath *)indexPatch{
    if (indexPatch.row==0) {
        self.payMoneyLabel.text=[NSString stringWithFormat:@"￥%@",model.payMoney];
        self.line.hidden=NO;
        self.leftLabel.text=@"退款金额";
    }else{
        self.payMoneyLabel.text=[NSString stringWithFormat:@"￥%@",model.cancelMoney];
        self.line.hidden=YES;
        self.leftLabel.text=@"退款手续费";
    }
    
}

@end
