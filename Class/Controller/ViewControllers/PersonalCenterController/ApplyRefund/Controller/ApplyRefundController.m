//
//  ApplyRefundController.m
//  youxia
//
//  Created by mac on 2017/4/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ApplyRefundController.h"
#import "ApplyRefundHeaderCell.h"
#import "ApplyRefundPriceCell.h"
#import "ApplyRefundTypeCell.h"
#import "ApplyRefundReasonCell.h"
#import "HotelRefundDetail.h"
#import "ApplyRefundInfoModel.h"
#import "ApplyRefundReasonModel.h"

@interface ApplyRefundController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, strong) UITableView *myTable;
@property (nonatomic, retain) NSMutableArray *dataArr;

@end

@implementation ApplyRefundController{
    ApplyRefundInfoModel *model;
    NSString *seleReasonId;
}

+(id)initWithOrderId:(NSString *)orderId{
    ApplyRefundController *vc=[ApplyRefundController new];
    vc.orderId=orderId;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"申请退款";
    [self.view addSubview:navBar];
    
    [self dataRequest];
}

-(NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr=[NSMutableArray array];
    }
    return _dataArr;
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-108) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"ApplyRefundHeaderCell" bundle:nil] forCellReuseIdentifier:@"ApplyRefundHeaderCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"ApplyRefundPriceCell" bundle:nil] forCellReuseIdentifier:@"ApplyRefundPriceCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"ApplyRefundTypeCell" bundle:nil] forCellReuseIdentifier:@"ApplyRefundTypeCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"ApplyRefundReasonCell" bundle:nil] forCellReuseIdentifier:@"ApplyRefundReasonCell"];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [_myTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self.view addSubview:_myTable];
        
        //
        UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-46, SCREENSIZE.width, 46)];
        [self.view addSubview:footView];
        
        //
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        btn.frame=CGRectMake(0, 0, footView.width, footView.height);
        btn.titleLabel.font=kFontSize16;
        [btn setTitle:@"确认退款" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickRefundBtn) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:btn];
    }
    return _myTable;
}

#pragma mark - 点击确认退款按钮
-(void)clickRefundBtn{
    if (seleReasonId==nil||seleReasonId==NULL) {
        [AppUtils showSuccessMessage:@"请选择退款原因" inView:self.view];
    }else{
        [self submitRequest:seleReasonId];
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==1) {
        return 2;
    }else if (section==3){
        return self.dataArr.count;
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 118;
    }else if (indexPath.section==1){
        return 48;
    }else if (indexPath.section==2){
        return 80;
    }
    return 48;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ApplyRefundHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ApplyRefundHeaderCell"];
        if (!cell) {
            cell=[[ApplyRefundHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ApplyRefundHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else if (indexPath.section==1){
        ApplyRefundPriceCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ApplyRefundPriceCell"];
        if (!cell) {
            cell=[[ApplyRefundPriceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ApplyRefundPriceCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model atIndexPatch:indexPath];
        return  cell;
    }else if (indexPath.section==2){
        ApplyRefundTypeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ApplyRefundTypeCell"];
        if (!cell) {
            cell=[[ApplyRefundTypeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ApplyRefundTypeCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }else{
        ApplyRefundReasonCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ApplyRefundReasonCell"];
        if (!cell) {
            cell=[[ApplyRefundReasonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ApplyRefundReasonCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        ApplyRefundReasonModel *rModel=self.dataArr[indexPath.row];
        [cell reflushData:rModel];
        return  cell;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0||section==1?16:54;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 0.0000001f;
    }else if (section==3){
        return 32;
    }else{
        return 8;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0||section==1) {
        return nil;
    }else{
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 54)];
        view.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        
        UIView *mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 10, SCREENSIZE.width, 44)];
        mainView.backgroundColor=[UIColor whiteColor];
        [view addSubview:mainView];
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(14, 0, mainView.width-28, mainView.height)];
        titleLabel.font=kFontSize16;
        titleLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
        NSString *titStr=section==2?@"退款方式":section==3?@"退款原因":@"订单遇到问题？";
        titleLabel.text=titStr;
        [mainView addSubview:titleLabel];
        
        return view;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==3) {
        ApplyRefundReasonModel *rModel=self.dataArr[indexPath.row];
        seleReasonId=rModel.reasonId;
    }
}

#pragma mark - request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"refundOrder",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            model=[ApplyRefundInfoModel new];
            [model jsonToModel:responseObject[@"retData"]];
            if (model.refundReason.count!=0) {
                for (NSDictionary *dic in model.refundReason) {
                    ApplyRefundReasonModel *rModel=[ApplyRefundReasonModel new];
                    [rModel jsonToModel:dic];
                    [self.dataArr addObject:rModel];
                }
            }
            [self.myTable reloadData];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitRequest:(NSString *)reasonId{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"refundOrderCheck",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId,
                         @"reason":reasonId,
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [self.navigationController pushViewController:[HotelRefundDetail initWithOrderId:_orderId] animated:YES];
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
            if (codeStr.integerValue==3) {
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
