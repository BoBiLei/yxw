//
//  UserInfoCell.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "UserInfoCell.h"

@implementation UserInfoCell{
    UILabel *namaLabel;
    UIImageView *imgv;
    UIImageView *vipimg;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateName:) name:UpDateUser_Name object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDatePhoto:) name:UpDateUser_Photo object:nil];
    
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(16, 12, self.height-24, self.height-24)];
    imgv.layer.borderWidth=0.7f;
    imgv.layer.borderColor=[UIColor lightGrayColor].CGColor;
    imgv.layer.masksToBounds=YES;
    imgv.layer.cornerRadius=imgv.height/2;
    

    
    
    if (![[AppUtils getValueWithKey:User_Photo] isEqualToString:@""]) {
        SDImageCache *imgCache=[SDImageCache sharedImageCache];
        if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
            UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
            imgv.image=image;
        }else{
            [imgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
        }
        
    }else{
        imgv.image=[UIImage imageNamed:Image_Default];
    }
    [self addSubview:imgv];
    
    //vip 标识
    vipimg=[[UIImageView alloc]init];
    vipimg.image=[UIImage imageNamed:@"yx_vip_icon"];
    if ([[AppUtils getValueWithKey:VIP_Flag] isEqualToString:@"1"]) {
        vipimg.hidden=NO;
    }else{
        vipimg.hidden=YES;
    }
    [imgv addSubview:vipimg];
    vipimg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* vipimg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[vipimg(28)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vipimg)];
    [NSLayoutConstraint activateConstraints:vipimg_h];
    NSArray* vipimg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[vipimg(28)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vipimg)];
    [NSLayoutConstraint activateConstraints:vipimg_w];
    
    
    //名称
    namaLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+12,20, SCREENSIZE.width-(imgv.origin.x+imgv.width+12)*2, 21)];
    NSString *nameStr;
    if (![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]) {
        nameStr=[AppUtils getValueWithKey:User_Name];
    }
    namaLabel.text=nameStr;
    namaLabel.font=kFontSize16;
    namaLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    [self addSubview:namaLabel];
    
    UILabel * yuElabel = [[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+12, 40, 80, self.height-50)];
    yuElabel.text = @"余额：";
    yuElabel.font=kFontSize16;
    yuElabel.textColor = [UIColor colorWithHexString:@"999999"];
    [self.contentView addSubview:yuElabel];
    
    self.balanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(yuElabel.origin.x+yuElabel.width+88, 40, self.height-50-164, self.height-50)];
    
    self.balanceLabel.textColor = [UIColor colorWithHexString:@"ff5742"];
    self.balanceLabel.font=kFontSize16;
    [self.contentView addSubview:self.balanceLabel];

//    UIImage * chongZhiImage = [UIImage imageNamed:@"chongZhi"];
//    _rechargeButton = [[UIButton alloc]initWithFrame:CGRectMake(self.width-50, 47, chongZhiImage.size.width/2, chongZhiImage.size.height/2)];
//    [_rechargeButton setBackgroundImage:chongZhiImage forState:UIControlStateNormal];
//    
//    [self.contentView addSubview:_rechargeButton];
    
 }

-(void)upDatePhoto:(NSNotification *)noti{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
}

-(void)clickBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 100:
            [self.delegate clickUserInfoButtonWith:ModifyPhotoType];
            break;
        case 101:
            [self.delegate clickUserInfoButtonWith:ModifyPasswordType];
            break;
        default:
            break;
    }
}

-(void)upDateName:(NSNotification *)noti{
    NSString *nameStr;
    if (![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]) {
        nameStr=[AppUtils getValueWithKey:User_Name];
    }
    namaLabel.text=nameStr;
    
    
    if ([[AppUtils getValueWithKey:VIP_Flag] isEqualToString:@"1"]) {
        vipimg.hidden=NO;
    }else{
        vipimg.hidden=YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
