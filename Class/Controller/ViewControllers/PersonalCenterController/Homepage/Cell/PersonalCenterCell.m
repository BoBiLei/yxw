//
//  PersonalCenterCell.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "PersonalCenterCell.h"

@implementation PersonalCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _titleLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
//    _titleLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?16:17];
    _rightLabel.textColor=[UIColor lightGrayColor];
//    _rightLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
