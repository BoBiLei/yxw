//
//  UserInfoCell.h
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UserInfoButtonType) {
    ModifyPhotoType,
    ModifyPasswordType
};

@protocol UserInfoCellDelegate <NSObject>

-(void)clickUserInfoButtonWith:(UserInfoButtonType)type;

@end

@interface UserInfoCell : UITableViewCell
@property (nonatomic,retain)UILabel * balanceLabel; //余额
@property (nonatomic,retain)UIButton * rechargeButton; // 充值
@property(nonatomic) id<UserInfoCellDelegate> delegate;

@end
