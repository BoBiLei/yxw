//
//  PersonCenterOrderCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonCenterOrderDelegate <NSObject>

-(void)clickPersonCenterOrderBtn:(NSInteger)tag;

@end

@interface PersonCenterOrderCell : UITableViewCell

@property (nonatomic, weak) id<PersonCenterOrderDelegate> delegate;

@end
