//
//  PersonCenterOrderCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PersonCenterOrderCell.h"
#import "CusImageTitleBtn.h"

@implementation PersonCenterOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *tip=[[UILabel alloc] initWithFrame:CGRectMake(15, 2, SCREENSIZE.width-30, 38)];
        tip.font=kFontSize16;
        tip.text=@"我的订单";
        [self addSubview:tip];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, tip.origin.y+tip.height, SCREENSIZE.width, 0.5f)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [self addSubview:line];
        
        CGFloat btnWidth=SCREENSIZE.width/4;
        for (int i=0; i<4; i++) {
            NSString *titStr=i==0?@"旅游":i==1?@"酒店":i==2?@"电影票":@"游侠商城";
            NSString *imgStr=i==0?@"psc_travelorder":i==1?@"psc_hotelorder":i==2?@"psc_filmorder":@"psc_shoporder";
            CusImageTitleBtn *btn=[[CusImageTitleBtn alloc] initWithFrame:CGRectMake(btnWidth*i, line.origin.y+line.height, btnWidth, btnWidth-18) title:titStr image:[UIImage imageNamed:imgStr]];
            btn.cusTitleLabel.textColor=[UIColor colorWithHexString:@"515151"];
            btn.tag=i;
            [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
        }
    }
    return self;
}

-(void)clickBtn:(id)sende{
    UIButton *btn=sende;
    [self.delegate clickPersonCenterOrderBtn:btn.tag];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
