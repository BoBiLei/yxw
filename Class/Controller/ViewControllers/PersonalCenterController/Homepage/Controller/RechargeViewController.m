//
//  RechargeViewController.m
//  youxia
//
//  Created by lx on 2017/6/1.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RechargeViewController.h"
#import "Macro2.h"
#import "FilmWeiXinPayModel.h"
#import "WXApi.h"
#import "PayTypeButton.h"
#define myDotNumbers     @"0123456789.\n"
#define myNumbers          @"0123456789\n"
@interface RechargeViewController ()<UITextFieldDelegate>

@end

@implementation RechargeViewController{
    UITextField * myTextField;
    NSMutableArray * zhiButtonArray;
    UILabel * totalShowLabel;
    NSString * _type;
    NSString * _price;
    UIButton *rightBtn;
    NSString * usrMony;
    UILabel* yuELabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCustomSearBar];
    [self CGrectVC];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    _type = @"3";
    _price = @"0";
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [self setUpBottomView];
    [self getBalanceParamRequest];
}
#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 20, SCREENSIZE.width-100, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"充值";
    [self.view addSubview:titleLabel];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [cusBar addSubview:fanHuiButton];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1]];
}
-(void)fanhui{
    [self .navigationController popViewControllerAnimated:YES];
}
-(void)CGrectVC{
    zhiButtonArray = [[NSMutableArray alloc]init];
    UIView * bgHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width, 260)];
    [bgHeaderView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgHeaderView];
    
   UIImageView *  headerImgv=[[UIImageView alloc]initWithFrame:CGRectMake(126.5*W_UNIT,43, 120, 120)];
    headerImgv.layer.borderWidth=0.7f;
    headerImgv.layer.borderColor=[UIColor lightGrayColor].CGColor;
    headerImgv.layer.masksToBounds=YES;
    headerImgv.layer.cornerRadius=headerImgv.height/2;
    
    if (![[AppUtils getValueWithKey:User_Photo] isEqualToString:@""]) {
        SDImageCache *imgCache=[SDImageCache sharedImageCache];
        if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
            UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
            headerImgv.image=image;
        }else{
            [headerImgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
        }
        
    }else{
        headerImgv.image=[UIImage imageNamed:Image_Default];
    }
    [bgHeaderView addSubview:headerImgv];
    
   UILabel* namaLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,headerImgv.origin.y+headerImgv.size.height+15, SCREENSIZE.width, 21)];
    NSString *nameStr;
    if (![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]) {
        nameStr=[AppUtils getValueWithKey:User_Name];
    }
    namaLabel.text=nameStr;
    namaLabel.font=kFontSize16;
    namaLabel.textAlignment = 1;
    namaLabel.textColor=[UIColor blackColor];
    [bgHeaderView addSubview:namaLabel];
    
    yuELabel=[[UILabel alloc]initWithFrame:CGRectMake(0,namaLabel.origin.y+namaLabel.size.height+10, SCREENSIZE.width, 21)];
//    NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
    
    yuELabel.font=kFontSize16;
    yuELabel.textAlignment = 1;
    [bgHeaderView addSubview:yuELabel];
    
    
    UIView * chongZHiView = [[UIView alloc]initWithFrame:CGRectMake(15, bgHeaderView.size.height+15+bgHeaderView.origin.y, SCREEN_WIDTH-30, 65)];
    [chongZHiView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:chongZHiView];
    
    UIImage * RMBImage = [UIImage imageNamed:@"rmbChongZhi"];
    UIImageView *  RMBImgv=[[UIImageView alloc]initWithFrame:CGRectMake(30,21, RMBImage.size.width/2, RMBImage.size.height/2)];
    [RMBImgv setImage:RMBImage];
    [chongZHiView addSubview:RMBImgv];
    
    UILabel* tiShiLabel=[[UILabel alloc]initWithFrame:CGRectMake(RMBImgv.origin.x+RMBImgv.size.width+10,18,90, 30)];
    tiShiLabel.text = @"充值金额：";
    tiShiLabel.font=kFontSize16;
    tiShiLabel.textColor = [UIColor blackColor];
     [chongZHiView addSubview:tiShiLabel];
    
    myTextField = [[UITextField alloc]initWithFrame:CGRectMake(tiShiLabel.origin.x+tiShiLabel.size.width, 19, SCREEN_WIDTH-tiShiLabel.origin.x-tiShiLabel.size.width-65, 30)];
    myTextField.placeholder =@"0.00";
    myTextField.delegate = self;
    myTextField.textAlignment = 2;
    myTextField.textColor =[UIColor colorWithHexString:@"ff5742"];
    myTextField.backgroundColor = [UIColor whiteColor];
    [myTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [chongZHiView addSubview:myTextField];
    
    UILabel* yuanLabel=[[UILabel alloc]initWithFrame:CGRectMake(myTextField.origin.x+myTextField.size.width+5,18,30, 30)];
    yuanLabel.text = @"元";
    yuanLabel.font=kFontSize16;
    yuanLabel.textColor = [UIColor colorWithHexString:@"ff5742"];
    [chongZHiView addSubview:yuanLabel];
    
    totalShowLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,chongZHiView.size.height+chongZHiView.origin.y,SCREEN_WIDTH, 65)];
    [totalShowLabel setBackgroundColor:[UIColor clearColor]];
    totalShowLabel.font=kFontSize30;
    totalShowLabel.text= @"0.00元";
    totalShowLabel.textAlignment = 1;
    totalShowLabel.textColor = [UIColor blackColor];
    [self.view addSubview:totalShowLabel];
    
    for (int i=0; i<2; i++) {
        UIView* bgZhiFULabel=[[UIView alloc]initWithFrame:CGRectMake(0,totalShowLabel.size.height+totalShowLabel.origin.y+i*58*H_UNIT,SCREEN_WIDTH, 48*H_UNIT)];
        [bgZhiFULabel setBackgroundColor:[UIColor whiteColor]];
        [bgZhiFULabel setUserInteractionEnabled:YES];
        [self.view addSubview:bgZhiFULabel];
        UIImage * zhiButtonImage = [UIImage imageNamed:@"toursuccess_icon"];
        UIButton * zhiFButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-50*W_UNIT, 14*H_UNIT, 22*W_UNIT,22*W_UNIT)];
        [zhiFButton addTarget:self action:@selector(zhiFuBaoPaySender:) forControlEvents:UIControlEventTouchUpInside];
        zhiFButton .tag = i;
        [zhiButtonArray addObject:zhiFButton];
        [bgZhiFULabel addSubview:zhiFButton];
        if (i==0) {
            UIImage * zhiFuBaoImage = [UIImage imageNamed:@"zhiFuBao"];
            UIImageView *  zhiFuBaoImgv=[[UIImageView alloc ]initWithFrame:CGRectMake(30*W_UNIT,10*H_UNIT, zhiFuBaoImage.size.width/2, zhiFuBaoImage.size.height/2)];
            [zhiFuBaoImgv setImage:zhiFuBaoImage];
            [bgZhiFULabel addSubview:zhiFuBaoImgv];
            
            UILabel * zhiFLabel = [[UILabel alloc]initWithFrame:CGRectMake(zhiFuBaoImgv.origin.x+zhiFuBaoImgv.size.width+20, 10*H_UNIT, 120, 30)];
            zhiFLabel.text = @"支付宝";
            [bgZhiFULabel addSubview:zhiFLabel];
            
            [zhiFButton setBackgroundImage:zhiButtonImage forState:UIControlStateNormal];
           
        }else{
            UIImage * weiXinImage = [UIImage imageNamed:@"weixin"];
            UIImageView *  weiXinImgv=[[UIImageView alloc]initWithFrame:CGRectMake(30*W_UNIT,10*H_UNIT, weiXinImage.size.width/2, weiXinImage.size.height/2)];
            [weiXinImgv setImage:weiXinImage];
            [bgZhiFULabel addSubview:weiXinImgv];
            
            UILabel * zhiFLabel = [[UILabel alloc]initWithFrame:CGRectMake(weiXinImgv.origin.x+weiXinImgv.size.width+20, 10*H_UNIT, 120, 30)];
            zhiFLabel.text = @"微信支付";
            [bgZhiFULabel addSubview:zhiFLabel];
            
           UIImage * zhiButtonImage = [UIImage imageNamed:@"toursuccess_hs"];
            [zhiFButton setBackgroundImage:zhiButtonImage forState:UIControlStateNormal];

        }
    }
    
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    if ([theTextField.text isEqualToString:@""] ||theTextField.text==nil ) {
        totalShowLabel.text= @"0.00元";
    }else{
      totalShowLabel.text= [NSString stringWithFormat:@"%@元",theTextField.text];
    }
    _price = theTextField.text;
    
    if (_price.floatValue !=0) {
        rightBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
        rightBtn.enabled = YES;
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        rightBtn.backgroundColor=[UIColor lightGrayColor];
        rightBtn.enabled = NO;
        [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
     NSCharacterSet *cs;

    NSUInteger nDotLoc = [textField.text rangeOfString:@"."].location;
    if (NSNotFound == nDotLoc && 0 != range.location) {
        cs = [[NSCharacterSet characterSetWithCharactersInString:myDotNumbers] invertedSet];
    }
    else {
        cs = [[NSCharacterSet characterSetWithCharactersInString:myNumbers] invertedSet];
    }
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    BOOL basicTest = [string isEqualToString:filtered];
    if (!basicTest) {
 
        return NO;
    }
    if (NSNotFound != nDotLoc && range.location > nDotLoc + 2) {
        return NO;
    }
    
    //只能输入13位，但第13、14位为小数点“.”的时候，还可以继续输入两位
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound)
        
    {
        if ((textField.text.length > 6) && string.length>0)
        {
            if ([string isEqualToString:@"."])
                return YES;
            else
                return NO;
        }
    }
    else{
        if([string isEqualToString:@"."])
            return NO;
        NSString *s1 = [textField.text substringFromIndex:[textField.text rangeOfString:@"."].location];
        
        NSString *s2 = [textField.text substringToIndex:[textField.text rangeOfString:@"."].location];
        
        if ((s1.length>2||s2.length>13) && string.length>0)
            
            return NO;
        
    }
    
//    return [self validateNumber:string];
    return YES;
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@".0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}
-(void)setUpBottomView{
    //
    UIView *btmBiew=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-74+25, SCREENSIZE.width, 74-25)];
    btmBiew.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btmBiew];
    
    //立即充值
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH, 74-25);
    rightBtn.backgroundColor=[UIColor lightGrayColor];
    rightBtn.enabled = NO;
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [rightBtn setTitle:@"立即充值" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(chongZhiClickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:rightBtn];
}

-(void)zhiFuBaoPaySender:(UIButton *)sender{
    for (UIButton * but in zhiButtonArray) {
        if (but.tag==sender.tag) {
              [but setBackgroundImage:[UIImage imageNamed:@"toursuccess_icon"] forState:UIControlStateNormal];
        }else{
            [but setBackgroundImage:[UIImage imageNamed:@"toursuccess_hs"] forState:UIControlStateNormal];
        }
    }
    if (sender.tag==0) {
        _type = @"3";
    }else{
        _type = @"8";
    }
}
-(void)chongZhiClickBuyBtn{
    NSLog(@"充值");
    [self getPayParamRequest];
    
}
-(void)getBalanceParamRequest{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"member_recharge" forKey:@"mod"];
    [dict setObject:@"check_balance" forKey:@"action"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *  is_agree = responseObject[@"retData"][@"balance"];
            usrMony = is_agree;
            NSMutableAttributedString *string;
            if ([usrMony isEqualToString:@"0"]||usrMony == nil ||[usrMony isEqual:nil]) {
                string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"当前余额为：0元"]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(6,usrMony.length+2)];
            }else{
                string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"当前余额为：%@元",usrMony]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(6,usrMony.length+1)];
            }
            yuELabel.attributedText = string;
             [AppUtils saveValue:is_agree forKey:User_Money];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
    
}
-(void)getPayParamRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"member_recharge" forKey:@"mod"];
    if ([_type isEqualToString:@"8"]) {
     [dict setObject:@"wx_recharge" forKey:@"action"];
    }else{
        [dict setObject:@"alipay_recharge" forKey:@"action"];
    }
    [dict setObject:_price forKey:@"recharge"];
    [dict setObject:_type forKey:@"type"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"微信 ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                //seleBtn.tag==0 微信   8== 支付宝
                if ([_type isEqualToString:@"8"]) {
                    id resObj=responseObject[@"retData"];
                    if ([resObj isKindOfClass:[NSDictionary class]]) {
                        FilmWeiXinPayModel *weixinPayModel=[FilmWeiXinPayModel new];
                        [weixinPayModel jsonDataForDictioanry:resObj];
                        [self weixinPayWithModel:weixinPayModel];
                    }
                }else{
                    id resObj=responseObject[@"retData"][@"sdkstr"];
                    if ([resObj isKindOfClass:[NSString class]]) {
                        [self alliPayWithId:resObj];
                    }
                }
                
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
#pragma mark - 调起微信支付
-(void)weixinPayWithModel:(FilmWeiXinPayModel *)pmodel{
    [WXApi registerApp:pmodel.appid];
    if([WXApi isWXAppInstalled]){
        PayReq* req    = [[PayReq alloc] init];
        req.openID     = pmodel.appid;
        req.partnerId  = pmodel.partnerid;           //商户号
        req.prepayId   = pmodel.prepayid;            //预支付交易会话ID
        req.package    = pmodel.package;             //扩展字段
        req.nonceStr   = pmodel.noncestr;            //随机字符串
        req.timeStamp  = pmodel.timestamp.intValue;  //时间戳（防止重发）
        req.sign       = pmodel.sign;                //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - 调起支付宝支付
-(void)alliPayWithId:(NSString *)strId{
    NSString *appScheme = @"youxiaalipay";
    //支付异步请求并回调
    [[AlipaySDK defaultService] payOrder:strId fromScheme:appScheme callback:^(NSDictionary *resultDic) {
//        [self payResultRequest];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
