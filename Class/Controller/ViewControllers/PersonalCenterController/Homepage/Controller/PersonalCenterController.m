//
//  PersonalCenterController.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "PersonalCenterController.h"
#import "UserInfoCell.h"
#import "PersonalCenterCell.h"
#import "ModifyPassWordController.h"
#import "CouponController.h"
#import "ApproveController.h"
#import "WelcomeController.h"
#import "PersonInfoController.h"
#import "AboutYouXiaController.h"
#import "OrderListController.h"
#import "FilmCouponController.h"
#import "FilmListController.h"
#import "YXCardApprove.h"
#import "SettingController.h"
#import "CheckADController.h"
#import "OverdueRecord.h"
#import "YTHListController.h"
#import "NewApproveInfo.h"
#import "PersonCenterOrderCell.h"
#import "HotelOrderList.h"
#import "RechargeViewController.h"
#import "AgreeWithPayViewController.h"
@interface PersonalCenterController ()<UITableViewDataSource,UITableViewDelegate,UserInfoCellDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,PersonCenterOrderDelegate>

@end

@implementation PersonalCenterController{
    
    NSArray *dataArr;
    UITableView *myTable;
    ModifyPassWordController *mdfPwdCtr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"个人中心页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    
    //如果没有登录返回到首页
    if (![AppUtils loginState]) {
        self.rdv_tabBarController.selectedIndex=0;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"个人中心页"];
    [self upDateUI];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //[self removeCache];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateUI) name:LoginSuccessNotifition object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateUI) name:@"Update_Is_NewPwd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout) name:@"Lgout_notification" object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpUI];
}

-(void)removeCache
{
    //===============清除缓存==============
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    DSLog(@"%f",[self folderSizeAtPath:cachePath]);
//    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachePath];
//    for (NSString *p in files)
//    {
//        NSError *error;
//        NSString *path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@",p]];
//        if([[NSFileManager defaultManager] fileExistsAtPath:path])
//        {
//            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
//        }
//    }
}

-(float)fileSizeAtPath:(NSString *)path{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:path]){
        long long size=[fileManager attributesOfItemAtPath:path error:nil].fileSize;
        return size/1024.0/1024.0;
    }
    return 0;
}
//计算缓存大小
-(float)folderSizeAtPath:(NSString *)path{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    float folderSize = 0.0;
    if ([fileManager fileExistsAtPath:path]) {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles) {
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            folderSize +=[self fileSizeAtPath:absolutePath];
        }
        //SDWebImage框架自身计算缓存的实现
//        folderSize+=[[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
        return folderSize;
    }
    return 0;
}

-(void)upDateUI{
    [myTable reloadData];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    //
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 20, SCREENSIZE.width-100, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"个人中心";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

#pragma mark - init UI

-(void)setUpUI{
    //
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"PersonalCenter" ofType:@"plist"];
    dataArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"UserInfoCell" bundle:nil] forCellReuseIdentifier:@"UserInfoCell"];
    [myTable registerNib:[UINib nibWithNibName:@"PersonalCenterCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (section==0||section==1) {
//        return 1;
//    }else{
//        return ((NSArray*)dataArr[section]).count;
//    }
    if (section==0||section==1||section==2) {
        return 1;
    }else{
        return ((NSArray*)dataArr[section]).count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 88;
    }else if (indexPath.section==1) {
        return SCREENSIZE.width/4+30;
    }else{
        return 46;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"-*-*-*-*-*-*%ld",indexPath.section);
    if (indexPath.section==0) {
        UserInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"UserInfoCell"];
        if (cell==nil) {
            cell=[[UserInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserInfoCell"];
        }
        NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
        
        if ([usrMony isEqualToString:@"0"]||usrMony == nil ||[usrMony isEqual:nil]) {
            cell.balanceLabel.text = @"0元";
        }else{
            cell.balanceLabel.text = [NSString stringWithFormat:@"%@元",usrMony];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        [cell.rechargeButton addTarget:self action:@selector(rechargeButtonSender) forControlEvents:UIControlEventTouchUpInside];

        return  cell;
    }else if (indexPath.section==1) {
        PersonCenterOrderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PersonCenterOrderCell"];
        if (cell==nil) {
            cell=[[PersonCenterOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonCenterOrderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        return  cell;
    }else{
        PersonalCenterCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (cell==nil) {
            cell=[[PersonalCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (indexPath.section==5&&indexPath.row==0) {
            cell.rightLabel.text=[self sizeWithImageCache:[[SDImageCache sharedImageCache] getSize]];
        }
        cell.titleLabel.text=dataArr[indexPath.section][indexPath.row][@"nameStr"];
        [cell.imgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"img"]]];
        return  cell;
    }
}
-(void)rechargeButtonSender{
    NSLog(@"充值");
  
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==dataArr.count-1){
        return 36;
    }
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *tagStr=dataArr[indexPath.section][indexPath.row][@"tag"];
    switch (tagStr.intValue) {
        case 0:
        {
            PersonInfoController *perInfo=[[PersonInfoController alloc]init];
            perInfo.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:perInfo animated:YES];
        }
            break;
        case 1000:
        {
            RechargeViewController * rechargeVC = [[RechargeViewController alloc]init];
            rechargeVC.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:rechargeVC animated:YES];

        }
            break;
        case 1:
        {
            [self.navigationController pushViewController:[PersonInfoController new] animated:YES];
        }
            break;
        case 2:
        {
            OrderListController *orderList=[[OrderListController alloc]init];
            orderList.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:orderList animated:YES];
        }
            break;
        case 3:
        {
//            ApproveController *appro=[[ApproveController alloc]init];
//            appro.hidesBottomBarWhenPushed=YES;
//            [self.navigationController pushViewController:appro animated:YES];
            
            
              [self upLoadPhotoHttpsForGet_is_agree];
        }
            break;
        case 4:
        {
            YXCardApprove *yxCart=[[YXCardApprove alloc]init];
            yxCart.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:yxCart animated:YES];
           
        }
            break;
        case 5:
        {
            CouponController *coupon=[[CouponController alloc]init];
            coupon.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:coupon animated:YES];
        }
            break;
        case 6:
        {
            AboutYouXiaController *aboutUs=[[AboutYouXiaController alloc]init];
            [self.navigationController pushViewController:aboutUs animated:YES];
        }
            
            break;
        case 7:
        {
            [self getADStatusRequest];
        }
            break;
        case 8:
            [self.navigationController pushViewController:[SettingController new] animated:YES];
            
//            [self.navigationController pushViewController:[ReactTest new] animated:YES];
            break;
        case 10:
        {
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否清除缓存！"preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                
            }];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                [self clearCaches];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case 11:
        {
            [self.navigationController pushViewController:[YTHListController new] animated:YES];
        }
            break;
        case 101:
        {
            [self.navigationController pushViewController:[FilmListController new] animated:YES];
        }
            break;
        case 102:
        {
            [self.navigationController pushViewController: [FilmCouponController new] animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark Request 获取是否同意协议
-(void)upLoadPhotoHttpsForGet_is_agree{
     [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"mod":@"me_t",
                         @"is_iso":@"1",
                         @"code":@"get_is_agree",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         };

    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
           NSString *  is_agree = responseObject[@"retData"][@"info"][@"is_agree"];
            NSString * rer = [NSString stringWithFormat:@"%@",is_agree];
            if ([rer isEqualToString:@"0"]  ) {
                AgreeWithPayViewController * AgreeWithPayVC = [[AgreeWithPayViewController alloc]init];
                AgreeWithPayVC.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:AgreeWithPayVC animated:YES];
            }else{
               [self.navigationController pushViewController:[NewApproveInfo new] animated:YES];
            }
            [AppUtils dismissHUDInView:self.view];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
    
   
}
#pragma mark - 点击我的订单
-(void)clickPersonCenterOrderBtn:(NSInteger)tag{
    switch (tag) {
        case 0:{
            OrderListController *orderList=[[OrderListController alloc]init];
            orderList.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:orderList animated:YES];
        }
            break;
        case 1:{
            [self.navigationController pushViewController:[HotelOrderList new] animated:YES];
        }
            break;
        case 2:{
            [self.navigationController pushViewController:[FilmListController new] animated:YES];
        }
            break;
        case 3:{
            
        }
            break;
        default:
            break;
    }
}

//计算缓存出大小
- (NSString *)sizeWithImageCache:(NSInteger)size{
    // 1k = 1024, 1m = 1024k
    if (size < 1024) {// 小于1k
        return [NSString stringWithFormat:@"%ldB",(long)size];
    }else if (size < 1024 * 1024){// 小于1m
        CGFloat aFloat = size/1024;
        return [NSString stringWithFormat:@"%.0fK",aFloat];
    }else if (size < 1024 * 1024 * 1024){// 小于1G
        CGFloat aFloat = size/(1024 * 1024);
        return [NSString stringWithFormat:@"%.1fM",aFloat];
    }else{
        CGFloat aFloat = size/(1024*1024*1024);
        return [NSString stringWithFormat:@"%.1fG",aFloat];
    }
}

#pragma mark - 点击退出按钮
-(void)clickCencelBtn{
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=23;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self logout];
                break;
            default:
                break;
        }
    };
    NSArray *items =
    @[MMItemMake(@"退出登录", MMItemTypeHighlight, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"退出后不会删除任何历史数据，下次登录依然可以使用本账号"
                                  items:items] showWithBlock:nil];
    
}

#pragma mark - 退出处理
//=================
//弹出登录、清空密码、
//发送通知以改变tabbar选中状态、
//并pop当前controller
//=================
-(void)logout{
    self.app.isLogin=NO;
    WelcomeController *login=[[WelcomeController alloc]init];
    UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
    [loginNav.navigationBar setShadowImage:[UIImage new]];
    [self presentViewController:loginNav animated:YES completion:^{
        [AppUtils saveValue:@"" forKey:User_Pass];
        [AppUtils saveValue:@"0" forKey:VIP_Flag];
        [AppUtils saveValue:@"" forKey:SSO_Login_UID];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - UserInfo delegate
-(void)clickUserInfoButtonWith:(UserInfoButtonType)type{
    //
    mdfPwdCtr=[[ModifyPassWordController alloc]init];
    mdfPwdCtr.hidesBottomBarWhenPushed=YES;
    switch (type) {
        case ModifyPhotoType:
//            [self showMdfView];
            break;
        case ModifyPasswordType:
            [self.navigationController pushViewController:mdfPwdCtr animated:YES];
            break;
            
        default:
            break;
    }
}

#pragma mark - 清除缓存
- (void)clearCaches{
    [AppUtils showSuccessMessage:@"正在清理缓存……" inView:self.view];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    
    [self performSelectorOnMainThread:@selector(cleanCacheSuccess) withObject:nil waitUntilDone:YES];
}

- (void)cleanCacheSuccess{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:4];
        [myTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        [AppUtils dismissHUDInView:self.view];
        return;
    });
}

#pragma mark - Request
-(void)getADStatusRequest{
    [AppUtils showProgressInView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_credit" forKey:@"mod"];
    [dict setObject:@"get_credit_status_new" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"uid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            NSString *statusStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"status"]];
            // statusStr=1 已获取征信信息   0 未获取征信信息  2 表示登录过，返回图片验证码和用户信息
            if ([statusStr isEqualToString:@"1"]) {
                OverdueRecord *vc=[OverdueRecord new];
                vc.loginName=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"login_name"]];
                vc.loginPass=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"passwd"]];
                [self.navigationController pushViewController:vc animated:YES];
            }else if([statusStr isEqualToString:@"0"]) {
                CheckADController *vc=[CheckADController new];
                vc.imgUrl=@"";
                vc.loginName=@"";
                vc.loginPass=@"";
                [self.navigationController pushViewController:vc animated:YES];
            }else if([statusStr isEqualToString:@"2"]){
                CheckADController *vc=[CheckADController new];
                vc.isHadLogin=YES;
                vc.imgUrl=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"data"][@"imgUrl"]];
                vc.loginName=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"login_name"]];
                vc.loginPass=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"passwd"]];
                [self.navigationController pushViewController:vc animated:YES];
            }
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
