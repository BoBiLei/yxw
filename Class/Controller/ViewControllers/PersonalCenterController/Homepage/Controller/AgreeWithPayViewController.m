//
//  AgreeWithPayViewController.m
//  youxia
//
//  Created by mac on 2017/6/5.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "AgreeWithPayViewController.h"
#import "XieYiViewController.h"
#import "NewApproveInfo.h"
@interface AgreeWithPayViewController (){
    UIButton *rightBtn;
    NSInteger  is_agree;
}

@end

@implementation AgreeWithPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    is_agree = 0;
    //
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 20, SCREENSIZE.width-100, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"获取个人信息服务协议";
    [self.view addSubview:titleLabel];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [cusBar addSubview:fanHuiButton];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
    
    [self GreactVC];
    [self setUpBottomView];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    
}
-(void)GreactVC{
    UIImageView * myImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 240*H_UNIT)];
    [myImageView setImage:[UIImage imageNamed:@"lvxingwebImage"]];
    [self.view addSubview:myImageView];
    
    for (int i=0; i<3; i++) {
        UILabel* bgZhiFULabel=[[UILabel alloc]initWithFrame:CGRectMake(20,myImageView.size.height+myImageView.origin.y+i*38*H_UNIT+30,SCREEN_WIDTH-40, 48*H_UNIT)];
        [bgZhiFULabel setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:bgZhiFULabel];
        
        if (i==0) {
       bgZhiFULabel.text = @"温馨提示：";
       bgZhiFULabel.font=kFontSize18;
        }else if(i==1) {
            bgZhiFULabel.text = @"1.申请主卡年龄要求18-60周岁，且具有稳定的职业和收入";
            bgZhiFULabel.font=kFontSize12;
        }else{
            bgZhiFULabel.text = @"2.目前只支持二代中国居民身份证进行网络申请";
            bgZhiFULabel.font=kFontSize12;
        }
    }

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(20,myImageView.size.height+myImageView.origin.y+100+38*2, 70, 30);
    button.backgroundColor = [UIColor whiteColor];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:@"同意" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"nuAgree"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(agreeWithButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(4,-20, 4, 0)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(5,0, 5, 50)];
    [self.view addSubview:button];
    
    UIButton *agreeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    agreeButton.frame = CGRectMake(20+60,myImageView.size.height+myImageView.origin.y+100+38*2, 240, 30);
    [agreeButton setTitleColor:[UIColor colorWithHexString:@"448aca"] forState:UIControlStateNormal];
    [agreeButton addTarget:self action:@selector(goOnPayButton) forControlEvents:UIControlEventTouchUpInside];
    [agreeButton setTitle:@"《用户身份认证服务协议》" forState:UIControlStateNormal];
    [self.view addSubview:agreeButton];
    
}
-(void)agreeWithButton:(UIButton *)sender{
    if (sender.selected==NO) {
        [sender setImage:[UIImage imageNamed:@"agreeGouXuan"] forState:UIControlStateNormal];
        rightBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        rightBtn.enabled = YES;
        is_agree = 1;
        sender.selected=YES;
    }else{
        [sender setImage:[UIImage imageNamed:@"nuAgree"] forState:UIControlStateNormal];
        rightBtn.backgroundColor=[UIColor lightGrayColor];
        [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        rightBtn.enabled = NO;
        is_agree = 0;
        sender.selected=NO;
    }
}
-(void)goOnPayButton{
    XieYiViewController * XieYiVC = [[XieYiViewController alloc]init];
    [self.navigationController pushViewController:XieYiVC animated:YES];
}
-(void)setUpBottomView{
    //
    UIView *btmBiew=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-74+25, SCREENSIZE.width, 74-25)];
    btmBiew.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btmBiew];
    
    //立即申请
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(0, 0, SCREEN_WIDTH, 74-25);
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    rightBtn.backgroundColor=[UIColor lightGrayColor];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn setTitle:@"立即申请" forState:UIControlStateNormal];
    rightBtn.enabled = NO;
    [rightBtn addTarget:self action:@selector(chongZhiClickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:rightBtn];
}
-(void)chongZhiClickBuyBtn{
    NSLog(@"申请");
    NSDictionary *dict=@{
                         @"mod":@"me_t",
                         @"is_iso":@"1",
                         @"code":@"save_is_agree",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"is_agree":@(is_agree),
                         };
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@----%@",responseObject,dict);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
//            NSString *  is_agree = responseObject[@"retData"][@"info"][@"is_agree"];
//            NSString * rer = [NSString stringWithFormat:@"%@",is_agree];
//            if ([rer isEqualToString:@"0"]  ) {
//                AgreeWithPayViewController * AgreeWithPayVC = [[AgreeWithPayViewController alloc]init];
//                AgreeWithPayVC.hidesBottomBarWhenPushed=YES;
//                [self.navigationController pushViewController:AgreeWithPayVC animated:YES];
//            }else{
//                [self.navigationController pushViewController:[NewApproveInfo new] animated:YES];
//            }
             [self.navigationController pushViewController:[NewApproveInfo new] animated:YES];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];

}

-(void)fanhui{
    [self .navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
