//
//  ChangePhoneController.m
//  youxia
//
//  Created by mac on 16/7/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ChangePhoneController.h"
#import "PersonInfoController.h"

#define RESETTIME 60
@interface ChangePhoneController ()

@end

@implementation ChangePhoneController{
    UITextField *phoneTf;
    UITextField *validTf;
    UIButton *getVerifyBtn;
    UIButton *bindingBtn;
    
    int timerCount;
    NSString *sendCode;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"更换手机绑定";
    [self.view addSubview:navBar];
    
    //
    [self setUpScrollerView];
}

-(void)setUpScrollerView{
    UIScrollView *scroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    [self.view addSubview:scroller];
    
    //
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(26,26, SCREENSIZE.width-52, 32)];
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTf.keyboardType=UIKeyboardTypePhonePad;
    [phoneTf addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
    phoneTf.placeholder=@"请输入要更换的手机号码";
    phoneTf.font=[UIFont systemFontOfSize:16];
    [phoneTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:phoneTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, phoneTf.origin.y+phoneTf.height+1)];
    
    CGFloat btnWidth=96;
    //
    validTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+22, phoneTf.width-btnWidth, phoneTf.height)];
    [validTf addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    validTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    validTf.keyboardType=UIKeyboardTypeNumberPad;
    validTf.placeholder=@"验证码";
    validTf.font=[UIFont systemFontOfSize:16];
    [validTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    validTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:validTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(validTf.origin.x, validTf.origin.y+validTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, validTf.origin.y+validTf.height+1)];
    
    //获取验证码按钮
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyBtn.frame=CGRectMake(phoneTf.origin.x+phoneTf.width-btnWidth, validTf.origin.y, btnWidth, validTf.height-3);
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    getVerifyBtn.layer.cornerRadius=getVerifyBtn.height/2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=[UIFont systemFontOfSize:14];
    [scroller addSubview:getVerifyBtn];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    //button
    bindingBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    bindingBtn.frame=CGRectMake(0, 0, SCREENSIZE.width-phoneTf.origin.x*2, 42);
    bindingBtn.center=CGPointMake(SCREENSIZE.width/2, validTf.center.y+70);
    bindingBtn.layer.cornerRadius=bindingBtn.height/2;
    bindingBtn.enabled=NO;
    [bindingBtn setTitle:@"确认更换" forState:UIControlStateNormal];
    [bindingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bindingBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [bindingBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:bindingBtn];
}

#pragma mark - text值改变时
-(void)phoneTextDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]) {
        getVerifyBtn.enabled=YES;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    }else{
        getVerifyBtn.enabled=NO;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    }
}

-(void)textDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]&&
        ![validTf.text isEqualToString:@""]
        ) {
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        bindingBtn.enabled=YES;
    }else{
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        bindingBtn.enabled=NO;
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    [self submitRequest];
}

#pragma mark - 点击获取验证码->查询是否注册过->发送验证码
-(void)clickVerifyBtn{
    [AppUtils closeKeyboard];
    [self isPhoneRegist:phoneTf.text];
}

#pragma mark - request查询手机是否已注册
//验证手机是否注册
-(void)isPhoneRegist:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"exists2_iso" forKey:@"code"];
    [dict setObject:@"field" forKey:@"field"];
    [dict setObject:phone forKey:@"value"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"account_t",
//                         @"code":@"exists2_iso",
//                         @"field":@"phone",
//                         @"value":phone
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //可以注册-->获取验证码
            [self getVerifyRequest];
        }else{
            //已经注册过
            [AppUtils showSuccessMessage:@"该号码已经被注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)setTimer:(NSTimer *)timer{
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        getVerifyBtn.enabled=YES;
        phoneTf.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

-(void)getVerifyRequest{
    
    //时间戳
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"user_send_smscode_ios" forKey:@"code"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
        }else{
            [AppUtils showSuccessMessage:@"已成功发送短信" inView:self.view];
            sendCode=responseObject[@"retData"][@"sendcode"];
            DSLog(@"%@",sendCode);
            timerCount=RESETTIME;
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//提交确认更换手机号
-(void)submitRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"set_user_phone_ios" forKey:@"code"];
    [dict setObject:sendCode forKey:@"reg_verify_code"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    //
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        DSLog(@"\n%@\n%@",[NewYXHost stringByAppendingString:@"mgo/index.php?"],dict);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"成功绑定新手机" inView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [AppUtils saveValue:phoneTf.text forKey:User_Phone];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BandPhoneSuccess" object:nil];
                //
                CGFloat vCtrs=self.navigationController.viewControllers.count;
                for (int i=0; i<vCtrs; i++) {
                    UIViewController *controller=self.navigationController.viewControllers[i];
                    if (i==1) {
                        if([controller isKindOfClass:[PersonInfoController class]]){
                            [self.navigationController popToViewController:controller animated:YES];
                        }
                    }
                }
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
