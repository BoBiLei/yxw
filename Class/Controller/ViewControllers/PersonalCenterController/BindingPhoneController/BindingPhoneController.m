//
//  BindingPhoneController.m
//  youxia
//
//  Created by mac on 16/7/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BindingPhoneController.h"
#import "BandingAccountController.h"
#import "ModifyPassWordController.h"

#define RESETTIME 60
@interface BindingPhoneController ()

@end

@implementation BindingPhoneController{
    UITextField *phoneTf;
    UITextField *validTf;
    UIButton *getVerifyBtn;
    UIButton *bindingBtn;
    
    int timerCount;
    NSString *sendCode;
    BOOL isClickYZ;
    BOOL isOK;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"绑定手机页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"绑定手机页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    //
    [self setUpTipView];
    
    //
    [self setUpScrollerView];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"绑定手机";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)clickReturnBtn{
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否取消手机号绑定"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction*okAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)setUpTipView{
    UIImageView *tipView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 30)];
    tipView.image=[UIImage imageNamed:@"tipview_img"];
    [self.view addSubview:tipView];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:tipView.bounds];
    tipLabel.font=kFontSize14;
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.text=@"为了你的账户安全，建议你绑定手机号！";
    tipLabel.textColor=[UIColor colorWithHexString:@"#8b8b8b"];
    [tipView addSubview:tipLabel];
}

-(void)setUpScrollerView{
    UIScrollView *scroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 94, SCREENSIZE.width, SCREENSIZE.height-94)];
    [self.view addSubview:scroller];
    
    UIImageView *phoneImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 74, 74)];
    phoneImgv.layer.cornerRadius=phoneImgv.height/2;
    phoneImgv.layer.masksToBounds=YES;
    phoneImgv.backgroundColor=[UIColor orangeColor];
    phoneImgv.center=CGPointMake(scroller.width/2, phoneImgv.height/2+18);
    [scroller addSubview:phoneImgv];
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
        phoneImgv.image=image;
    }else{
        [phoneImgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
    
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, phoneImgv.origin.y+phoneImgv.height+10, SCREENSIZE.width, 21)];
    nameLabel.font=kFontSize15;
    nameLabel.textAlignment=NSTextAlignmentCenter;
    NSString *nameStr;
    if (![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]) {
        nameStr=[AppUtils getValueWithKey:User_Name];
    }
    nameLabel.text=nameStr;
    nameLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    [scroller addSubview:nameLabel];
    
    //
    //
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(26, nameLabel.origin.y+nameLabel.height+20, SCREENSIZE.width-52, 32)];
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTf.keyboardType=UIKeyboardTypePhonePad;
    [phoneTf addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
    phoneTf.placeholder=@"请输入手机号码";
    phoneTf.font=kFontSize16;
    [phoneTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:phoneTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, phoneTf.origin.y+phoneTf.height+1)];
    
    CGFloat btnWidth=96;
    //
    validTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+22, phoneTf.width-btnWidth, phoneTf.height)];
    [validTf addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    validTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    validTf.keyboardType=UIKeyboardTypeNumberPad;
    validTf.placeholder=@"验证码";
    validTf.font=kFontSize16;
    [validTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    validTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:validTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(validTf.origin.x, validTf.origin.y+validTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, validTf.origin.y+validTf.height+1)];
    
    //获取验证码按钮
    timerCount = RESETTIME;
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyBtn.frame=CGRectMake(phoneTf.origin.x+phoneTf.width-btnWidth, validTf.origin.y, btnWidth, validTf.height-3);
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    getVerifyBtn.layer.cornerRadius=getVerifyBtn.height/2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=kFontSize14;
    [scroller addSubview:getVerifyBtn];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    //button
    bindingBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    bindingBtn.frame=CGRectMake(0, 0, SCREENSIZE.width-phoneTf.origin.x*2, 42);
    bindingBtn.center=CGPointMake(SCREENSIZE.width/2, validTf.center.y+70);
    bindingBtn.layer.cornerRadius=bindingBtn.height/2;
    bindingBtn.enabled=NO;
    [bindingBtn setTitle:@"绑定手机" forState:UIControlStateNormal];
    [bindingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bindingBtn.titleLabel.font=kFontSize16;
    [bindingBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:bindingBtn];
    
    [self setUpBottomView:scroller];
}

-(void)setUpBottomView:(UIView *)view{
    CGFloat btnHeight=52;
    CGFloat btnWidth=(view.width-1)/2;
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#0080c5"] height:0.8f firstPoint:CGPointMake(0, view.height-btnHeight-1) endPoint:CGPointMake(view.width, view.height-btnHeight-1)];
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"#0080c5"] height:0.8f firstPoint:CGPointMake(view.width/2, view.height-btnHeight-1) endPoint:CGPointMake(view.width/2, view.height)];
    for (int i=0; i<2; i++) {
        NSString *titStr;
        if (i==0) {
            titStr=@"绑定已注册账号";
        }else{
            titStr=@"跳过";
        }
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(btnWidth*i, view.height-btnHeight, btnWidth, btnHeight);
        btn.titleLabel.font=[UIFont  systemFontOfSize:17];
        [btn setTitleColor:[UIColor colorWithHexString:@"#0080c5"] forState:UIControlStateNormal];
        [btn setTitle:titStr forState:UIControlStateNormal];
        btn.tag=i;
        [btn addTarget:self action:@selector(clickBottomBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
    }
}

-(void)clickBottomBtn:(id)sender{
    UIButton *btn=sender;
    if (btn.tag==0) {
        [self.navigationController pushViewController:[BandingAccountController new] animated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - text值改变时
-(void)phoneTextDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]) {
        getVerifyBtn.enabled=YES;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    }else{
        getVerifyBtn.enabled=NO;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    }
}

-(void)textDidChange{
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]&&
        ![validTf.text isEqualToString:@""]
        ) {
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        bindingBtn.enabled=YES;
    }else{
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        bindingBtn.enabled=NO;
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    if (isClickYZ) {
        if (isOK) {
            if (![validTf.text isEqualToString:sendCode]) {
                [AppUtils showSuccessMessage:@"手机验证码错误" inView:self.view];
            }else{
                [self submitRequest];
            }
        }else{
            [AppUtils showSuccessMessage:@"该手机号码已被注册" inView:self.view];
        }
    }else{
        [AppUtils showSuccessMessage:@"请先获取手机验证码" inView:self.view];
    }
}

#pragma mark - 点击获取验证码->查询是否注册过->发送验证码
-(void)clickVerifyBtn{
    [AppUtils closeKeyboard];
    [self checkPhoneIsRegist:phoneTf.text];
}

#pragma mark - request查询手机是否已注册
-(void)checkPhoneIsRegist:(NSString *)phone{
    isClickYZ=YES;
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"exists_iso" forKey:@"code"];
    [dict setObject:@"phone" forKey:@"field"];
    [dict setObject:phone forKey:@"value"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"account_t",
//                         @"code":@"exists_iso",
//                         @"field":@"phone",
//                         @"value":phone
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //可以注册-->获取验证码
            isOK=YES;
            [self requestGetVerifyCodeForPhone:phone];
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        }else{
            //已经注册过
            isOK=NO;
            [AppUtils showSuccessMessage:@"该号码已经注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//获取手机验证码
-(void)requestGetVerifyCodeForPhone:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"getCode_bphone_iso" forKey:@"code"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"account_t",
//                         @"code":@"getCode_bphone_iso",
//                         @"phone":phone
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            DSLog(@"%@",responseObject[@"retData"][@"sendcode"]);
            sendCode=responseObject[@"retData"][@"sendcode"];
            [AppUtils showSuccessMessage:@"已发送短信验证码" inView:self.view];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"message"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//绑定手机号请求
-(void)submitRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:@"do_bind_phone_iso" forKey:@"code"];
    [dict setObject:sendCode forKey:@"reg_verify_code"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    //
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils saveValue:phoneTf.text forKey:User_Phone];
            
            //更新个人资料的手机号
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BandPhoneSuccess" object:nil];
            
            //跳转到设置密码
            [self.navigationController pushViewController:[ModifyPassWordController new] animated:YES];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


-(void)setTimer:(NSTimer *)timer{
    getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    getVerifyBtn.enabled=NO;
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        getVerifyBtn.enabled=YES;
        phoneTf.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

@end
