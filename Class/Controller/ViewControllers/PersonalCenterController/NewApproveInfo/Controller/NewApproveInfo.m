//
//  NewApproveInfo.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewApproveInfo.h"
#import "CreditDataCell.h"
#import "CreditDataUserInfoCell.h"
#import "CreditDataModel.h"
#import "IDCardApprove.h"
#import "BankCardValidate.h"
#import "NewPhoneValidate.h"
#import "NewContactValidate.h"
#import "NewAddressValidate.h"
#import "OverdueRecord.h"
#import "CheckADController.h"
#import "BankCardValModel.h"
#import "IDCardApproveModel.h"
#import "NewContactValModel.h"
#import "AddressValModel.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface NewApproveInfo ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *myTable;

@end

@implementation NewApproveInfo{
    CreditDataModel *dataModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor whiteColor];
        _myTable.showsVerticalScrollIndicator=NO;
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [_myTable registerNib:[UINib nibWithNibName:@"CreditDataCell" bundle:nil] forCellReuseIdentifier:@"CreditDataCell"];
        [self.view addSubview:_myTable];
    }
    return _myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refleshData) name:@"reflesh_approve_noti" object:nil];
    
    [self setNavBar];
    
    if ([self isUseContact]) {
        [self setUpUI];
        
        [self dataRequest];
        
        [self submitContact:[self test]];
    }else{
        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请在设置-隐私-通讯录中允许《游侠网》访问您的通讯录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(BOOL)isUseContact{
    ABAddressBookRef addressBook = NULL;
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - 创建联系人姓名、电话号码
-(NSMutableArray *)test{
    //这个变量用于记录授权是否成功，即用户是否允许我们访问通讯录
    int __block tip=0;
    //声明一个通讯簿的引用
    ABAddressBookRef addBook =nil;
    //创建通讯簿的引用
    addBook=ABAddressBookCreateWithOptions(NULL,NULL);
    
    dispatch_semaphore_t sema=dispatch_semaphore_create(0);
    
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool greanted,CFErrorRef error)        {
        
        if (!greanted) {
            tip=1;
        }
        
        dispatch_semaphore_signal(sema);
    });
    
    dispatch_semaphore_wait(sema,DISPATCH_TIME_FOREVER);
    
    NSMutableArray *contactArr=[NSMutableArray array];
    
    if (!tip) {
        //
        //获取所有联系人的数组
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addBook);
        
        //进行遍历
        for (NSInteger i=0; i<CFArrayGetCount(allPeople); i++) {
            
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //读取firstname
            NSString *firstNameStr=@"";
            NSString *firstName = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            if(firstName != nil){
                firstNameStr=firstName;
            }
            //读取lastname
            NSString *lastNameStr=@"";
            NSString *lastname = (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            if(lastname != nil){
                lastNameStr=lastname;
            }
            
            NSString *nameString;
            if (lastname==nil&&firstName == nil) {
                nameString=@"未填写";
            }else{
                nameString=[NSString stringWithFormat:@"%@%@",lastNameStr,firstNameStr];
            }
            
            NSString *phoneString=@"";
            
            //读取电话多值
            ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for (int k = 0; k<ABMultiValueGetCount(phone); k++){
                //获取該Label下的电话值
                NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phone, k);
                //去掉-
                NSMutableString *phoneStr=[NSMutableString stringWithString:personPhone];
                NSRange nameRang=[phoneStr rangeOfString:@"-"];
                while (nameRang.location!=NSNotFound) {
                    [phoneStr replaceCharactersInRange:nameRang withString:@""];
                    nameRang=[phoneStr rangeOfString:@"-"];
                }
                phoneString=[[NSString stringWithFormat:@"%@|",phoneString] stringByAppendingString:phoneStr];
            }
            [contactArr addObject:[NSString stringWithFormat:@"%@%@",nameString,phoneString]];
        }
    }
    return contactArr;
}

-(void)refleshData{
    [self dataRequest];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"实名验证获取额度";
    [self.view addSubview:navBar];
}

#pragma mark - init UI
-(void)setUpUI{
//    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
//    myTable.backgroundColor=[UIColor whiteColor];
//    myTable.showsVerticalScrollIndicator=NO;
//    myTable.dataSource=self;
//    myTable.delegate=self;
//    [myTable registerNib:[UINib nibWithNibName:@"CreditDataCell" bundle:nil] forCellReuseIdentifier:@"CreditDataCell"];
//    [self.view addSubview:myTable];
}

#pragma mark - table delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return ![dataModel.status isEqualToString:@"1"]?7:6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==6) {
        return 160;
    }else{
        return 48;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 14;
    }
    return 0.000000001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==5) {
        return 36;
    }
    return 14;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section!=6) {
        CreditDataCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CreditDataCell"];
        if (cell==nil) {
            cell=[[CreditDataCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditDataCell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataModel atIndexPath:indexPath];
        return  cell;
    }else{
        CreditDataUserInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CreditDataUserInfoCell"];
        if (cell==nil) {
            cell=[[CreditDataUserInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditDataUserInfoCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:dataModel];
        return  cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            [self idCardInfoRequest];
            break;
        case 1:
            [self bankInfoRequest];
            break;
        case 2:
            [self phoneInfoRequest];
            break;
        case 3:
            [self contactInfoRequest];
            break;
        case 4:
            [self msgAddressInfoRequest];
            break;
        case 5:
            [self getADStatusRequest];
            break;
        default:
            break;
    }
}

#pragma mark - Request 请求

-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    dataModel=[CreditDataModel new];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"vip_write_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        /*
         [person_status]=> 1 //'个人信息提交状况1：未提交',2已提交
         [bank_1_status]=> 1 //'银行卡和手机号码是否通过，默认为0,通过为1',
         [jj_status]=>1 //紧急联系人保存状态,1未提交，2提交
         [phone_status]=> 1 //0未验证 1已经验证
         [work_status]=>1 //'工作信息提交状况1：未提交', 2已经提交
         [credit_status]=> 1 //信用报告0未提交 1已验证
         [tx_status]=>0 //0未验证 1已经验证
         
         */
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            dataModel.person_name=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"person_name"]];
            dataModel.amount=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"amount"]];
            dataModel.phoneSt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"phone_status"]];
            dataModel.personSt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"person_status"]];
            dataModel.bankSt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"bank_1_status"]];
            dataModel.jj_status=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"jj_status"]];
            dataModel.creditSt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"credit_status"]];
            dataModel.fundSt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"fund_status"]];
            dataModel.securitySt=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"security_status"]];
            dataModel.work_status=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"work_status"]];
            dataModel.status=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"status"]];
            
            if (dataModel.status.integerValue==1) {
                UIView *fView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 56)];
                self.myTable.tableFooterView=fView;
                
                UIButton *subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                subBtn.frame=CGRectMake(0, 8, SCREENSIZE.width, 48);
                subBtn.titleLabel.font=kFontSize17;
                subBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
                [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:UIControlEventTouchUpInside];
                [fView addSubview:subBtn];
                
                if(dataModel.status.integerValue==1){
                    [subBtn setTitle:@"提交申请,获取额度" forState:UIControlStateNormal];
                    subBtn.userInteractionEnabled=YES;
                }else if (dataModel.status.integerValue==2){
                    [subBtn setTitle:@"审核中" forState:UIControlStateNormal];
                    subBtn.userInteractionEnabled=NO;
                }else if (dataModel.status.integerValue==4){
                    [subBtn setTitle:@"审核失败" forState:UIControlStateNormal];
                    subBtn.userInteractionEnabled=NO;
                }
            }else{
                self.myTable.tableFooterView=[UIView new];
            }
            
            [self.myTable reloadData];
        }else{
//            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@\n%@",error,error.localizedDescription);
        [AppUtils dismissHUDInView:self.view];
    }];
}

//身份证信息
-(void)idCardInfoRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"get_idcard_info" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"info"];
            IDCardApproveModel *bankModel=[IDCardApproveModel new];
            [bankModel jsonToModel:dic];
            [self.navigationController pushViewController:[IDCardApprove initWithModel:bankModel] animated:YES];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//银行卡信息
-(void)bankInfoRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"get_bank_info" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"info"];
            BankCardValModel *bankModel=[BankCardValModel new];
            [bankModel jsonToModel:dic];
            [self.navigationController pushViewController:[BankCardValidate initWithModel:bankModel] animated:YES];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//手机号验证信息
-(void)phoneInfoRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"get_phone_info" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"info"];
            NSString *phoneStr=dic[@"phone_num"];
            NSString *phoneStatus=dic[@"phone_status"];
            NSString *guishu=@"";
            NSString *dxPass=dic[@"phone_pwd"];
            
            NSMutableArray *phoneArr=[NSMutableArray arrayWithObjects:phoneStr,phoneStatus,@"",@"",guishu,dxPass, nil];
            
            NewPhoneValidate *vc=[NewPhoneValidate new];
            vc.phoneArr=phoneArr;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//紧急联系人信息
-(void)contactInfoRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"get_jj_person" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"info"];
            NewContactValModel *bankModel=[NewContactValModel new];
            [bankModel jsonToModel:dic];
            [self.navigationController pushViewController:[NewContactValidate initWithModel:bankModel] animated:YES];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//通讯地址信息
-(void)msgAddressInfoRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"get_word_home_info_new" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"][@"info"];
            AddressValModel *model=[AddressValModel new];
            [model jsonToModel:dic];
            [self.navigationController pushViewController:[NewAddressValidate initWithModel:model] animated:YES];
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)getADStatusRequest{
    [AppUtils showProgressInView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_credit" forKey:@"mod"];
    [dict setObject:@"get_credit_status_new" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"uid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            
            NSString *statusStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"status"]];
            // statusStr=1 已获取征信信息   0 未获取征信信息  2 表示登录过，返回图片验证码和用户信息
            if ([statusStr isEqualToString:@"1"]) {
                OverdueRecord *vc=[OverdueRecord new];
                vc.loginName=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"login_name"]];
                vc.loginPass=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"passwd"]];
                [self.navigationController pushViewController:vc animated:YES];
            }else if([statusStr isEqualToString:@"0"]) {
                CheckADController *vc=[CheckADController new];
                vc.imgUrl=@"";
                vc.loginName=@"";
                vc.loginPass=@"";
                [self.navigationController pushViewController:vc animated:YES];
            }else if([statusStr isEqualToString:@"2"]){
                CheckADController *vc=[CheckADController new];
                vc.isHadLogin=YES;
                vc.imgUrl=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"data"][@"imgUrl"]];
                vc.loginName=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"login_name"]];
                vc.loginPass=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"userinfo"][@"passwd"]];
                [self.navigationController pushViewController:vc animated:YES];
            }
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitAllStatusRequest{
    [AppUtils showProgressInView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_info_check" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestGet parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            [self dataRequest];
        };
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - 上传联系人信息请求

-(void)submitContact:(NSMutableArray *)arr{
    //is_nosign
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_user_phons_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [dict setObject:arr forKey:@"data"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        //DSLog(@"%@",dict[@"data"]);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 点击提交
-(void)clickSubBtn{
    if(dataModel.personSt.integerValue!=2){
        [AppUtils showSuccessMessage:@"请验证第一步,身份验验证" inView:self.view];
    }else if (dataModel.bankSt.integerValue!=1){
        [AppUtils showSuccessMessage:@"请验证第二步,银行卡验证" inView:self.view];
    }else if (dataModel.phoneSt.integerValue!=1){
        [AppUtils showSuccessMessage:@"请验证第三步,手机号码验证" inView:self.view];
    }else if (dataModel.jj_status.integerValue!=2){
        [AppUtils showSuccessMessage:@"请验证第四步,紧急联系人" inView:self.view];
    }else if (dataModel.work_status.integerValue!=2){
        [AppUtils showSuccessMessage:@"请验证第五步,通讯地址" inView:self.view];
    }
//    else if (dataModel.creditSt.integerValue!=1){
//        [AppUtils showSuccessMessage:@"请验证第六步,个人征信报告" inView:self.view];
//    }
    else{
        [self submitAllStatusRequest];
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
