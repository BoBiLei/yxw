//
//  CreditDataModel.h
//  YXJR
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠金融. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditDataModel : NSObject
/* phone_status=>1 //运营商认证状态 0通过 1已通过
 person_status=>1 //实名认证状态 0通过 1已通过
 bank_status=>1 //银行卡认证状态 0通过 1已通过
 credit_status=>1 //征信报告认证状态 0通过 1已通过
 fund_status=>1 //公积金认证状态 0通过 1已通过
 security_status=>1 //社保认证状态 0通过 1已通过
 jj_status=>1   //紧急联系人 0通过 2已通过
 tx_status=>1   //通讯录状态 0通过 1已通过
 status=>1      //总状态 0通过 1已通过
 amount=>1      //额度
 person_name=>1 //姓名
*/
@property (nonatomic, copy) NSString *phoneSt;
@property (nonatomic, copy) NSString *personSt;
@property (nonatomic, copy) NSString *bankSt;
@property (nonatomic, copy) NSString *creditSt;
@property (nonatomic, copy) NSString *fundSt;
@property (nonatomic, copy) NSString *securitySt;
@property (nonatomic, copy) NSString *jj_status;
@property (nonatomic, copy) NSString *tx_status;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *person_name;

@property (nonatomic, copy) NSString *work_status;

@end
