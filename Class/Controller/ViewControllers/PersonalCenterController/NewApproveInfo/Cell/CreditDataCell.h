//
//  CreditDataCell.h
//  YXJR
//
//  Created by mac on 2017/2/28.
//  Copyright © 2017年 游侠金融. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreditDataModel.h"

@interface CreditDataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

-(void)reflushDataForModel:(CreditDataModel *)model atIndexPath:(NSIndexPath *)indexPath;

@end
