//
//  CreditDataCell.m
//  YXJR
//
//  Created by mac on 2017/2/28.
//  Copyright © 2017年 游侠金融. All rights reserved.
//

#import "CreditDataCell.h"

@implementation CreditDataCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(CreditDataModel *)model atIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_idcard"] forState:UIControlStateNormal];
            self.centerLabel.text=@"身份证验证";
            if([model.personSt isEqualToString:@"2"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        case 1:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_bank"] forState:UIControlStateNormal];
            self.centerLabel.text=@"银行卡验证";
            if([model.bankSt isEqualToString:@"1"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        case 2:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_phone"] forState:UIControlStateNormal];
            self.centerLabel.text=@"手机号码验证";
            if([model.phoneSt isEqualToString:@"1"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        case 3:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_contact"] forState:UIControlStateNormal];
            self.centerLabel.text=@"紧急联系人";
            if([model.jj_status isEqualToString:@"2"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        case 4:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_address"] forState:UIControlStateNormal];
            self.centerLabel.text=@"通讯地址";
            if([model.work_status isEqualToString:@"2"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        case 5:
        {
            [self.leftBtn setImage:[UIImage imageNamed:@"approve_ad"] forState:UIControlStateNormal];
            self.centerLabel.text=@"个人征信报告";
            if([model.creditSt isEqualToString:@"1"]){
                self.rightLabel.text=@"已验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
            }else{
                self.rightLabel.text=@"未验证";
                self.rightLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            }
        }
            break;
        default:
            break;
    }
}

@end
