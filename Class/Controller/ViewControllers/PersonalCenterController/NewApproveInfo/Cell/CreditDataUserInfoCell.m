//
//  CreditDataUserInfoCell.m
//  youxia
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CreditDataUserInfoCell.h"

@implementation CreditDataUserInfoCell{
    TTTAttributedLabel *nameLabel;
    TTTAttributedLabel *edLabel;
    TTTAttributedLabel *statusLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *tiplabel=[[UILabel alloc] initWithFrame:CGRectMake(20, 12, SCREENSIZE.width-40, 21)];
        tiplabel.font=[UIFont systemFontOfSize:15];
        tiplabel.text=@"信用认证";
        [self addSubview:tiplabel];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, tiplabel.origin.y*2+tiplabel.height, SCREENSIZE.width, 0.5f)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [self addSubview:line];
        
        //
        CGFloat labWidth=SCREENSIZE.width/3;
        for (int i=0; i<3; i++) {
            TTTAttributedLabel *label=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(labWidth*i, tiplabel.origin.y*2+tiplabel.height+18, labWidth, 80)];
            label.font=[UIFont systemFontOfSize:16];
            label.textAlignment=NSTextAlignmentCenter;
            label.numberOfLines=0;
            [self addSubview:label];
            NSString *textStr=i==0?@"姓名\n李大龙":i==1?@"分期额度\n0元":@"状态\n审核中";
            [label setText:textStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                NSRange sumRange = [[mutableAttributedString string] rangeOfString:i==0?@"李大龙":i==1?@"0元":@"审核中" options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"ababab"] CGColor] range:sumRange];
                //调整行间距
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                [paragraphStyle setLineSpacing:8];
                [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [textStr length])];
                //字体大小
                UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
                CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
                if (font) {
                    [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                    CFRelease(font);
                }
                return mutableAttributedString;
            }];
            
            if (i==0) {
                nameLabel=label;
            }else if (i==1){
                edLabel=label;
            }else{
                statusLabel=label;
            }
            
            if (i!=2) {
                UIView *vv=[[UIView alloc] initWithFrame:CGRectMake(label.origin.x+label.width, label.origin.y+12, 0.5f, label.height-24)];
                vv.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
                [self addSubview:vv];
            }
        }
    }
    return self;
}

-(void)reflushData:(CreditDataModel *)model{
    if (model.person_name!=nil||model.person_name!=NULL) {
        NSString *str01=[NSString stringWithFormat:@"姓名\n%@",model.person_name];
        [nameLabel setText:str01 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.person_name options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"ababab"] CGColor] range:sumRange];
            //调整行间距
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:8];
            [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str01 length])];
            //字体大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
    if (model.amount!=nil||model.amount!=NULL) {
        NSString *str01=[NSString stringWithFormat:@"分期额度\n%@元",model.amount];
        [edLabel setText:str01 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",model.amount] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"ababab"] CGColor] range:sumRange];
            //调整行间距
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:8];
            [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str01 length])];
            //字体大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
    if (model.status!=nil||model.status!=NULL) {
        NSString *str;
        switch (model.status.integerValue) {
            case 1:
                str=@"未提交";
                break;
            case 2:
                str=@"审核中";
                break;
            case 3:
                str=@"已通过";
                break;
            default:
                str=@"审核失败";
                break;
        }
        NSString *str01=[NSString stringWithFormat:@"状态\n%@",str];
        [statusLabel setText:str01 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:str options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"ababab"] CGColor] range:sumRange];
            //调整行间距
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:8];
            [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str01 length])];
            //字体大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
}

@end
