//
//  CancelHotelOrder.m
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CancelHotelOrder.h"
#import "BRPlaceholderTextView.h"
#import "HotelOrderList.h"
#import "HotelHomeController.h"

@interface CancelHotelOrder ()

@property (nonatomic, copy) NSString *orderId;

@end

@implementation CancelHotelOrder{
    BRPlaceholderTextView *textView;
}

+(id)initWithOrderId:(NSString *)orderId{
    CancelHotelOrder *vc=[CancelHotelOrder new];
    vc.orderId=orderId;
    return vc;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setNavBar];
    
    [self setUpUI];
}

-(void)setNavBar{
    UIView *bar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    bar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    bar.userInteractionEnabled=YES;
    [self.view addSubview:bar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:back];
    
    UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    btn.backgroundColor=[UIColor clearColor];
    [btn setTitle:@"取消订单" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bar addSubview:btn];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init UI
-(void)setUpUI{
    UITableView *myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    [self.view addSubview:myTable];
    
    UIView *headView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 200)];
    headView.backgroundColor=myTable.backgroundColor;
    myTable.tableHeaderView=headView;
    
    UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, headView.width-20, 160)];
    imgv.image=[UIImage imageNamed:@"cencel_textview_bg"];
    imgv.userInteractionEnabled=YES;
    [headView addSubview:imgv];
    
    textView=[[BRPlaceholderTextView alloc] initWithFrame:CGRectMake(5, 1, imgv.width-5, imgv.height-2)];
    textView.backgroundColor=[UIColor clearColor];
    textView.layer.cornerRadius=2;
    textView.placeholder=@"请输入取消订单原因…";
    textView.font=[UIFont systemFontOfSize:15];
    [imgv addSubview:textView];
    [textView setPlaceholderFont:[UIFont systemFontOfSize:15]];
    [textView setPlaceholderColor:[UIColor colorWithHexString:@"a6a6a6"]];
    textView.tintColor=[UIColor colorWithHexString:@"55b2f0"];
    [textView addMaxTextLengthWithMaxLength:150 andEvent:^(BRPlaceholderTextView *text) {
        
        NSLog(@"----------");
    }];
    
    [textView addTextViewBeginEvent:^(BRPlaceholderTextView *text) {
        NSLog(@"begin");
    }];
    
    [textView addTextViewEndEvent:^(BRPlaceholderTextView *text) {
        NSLog(@"end");
    }];
    
#pragma mark FootView
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 200)];
    footView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.tableFooterView=footView;
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.cornerRadius=4;
    btn.frame=CGRectMake(12, 0, SCREENSIZE.width-24, 46);
    btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    btn.titleLabel.font=[UIFont systemFontOfSize:17];
    [btn setTitle:@"提 交" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickSubmit) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:btn];
}

-(void)clickSubmit{
    [AppUtils closeKeyboard];
    if ([textView.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请输入取消订单原因" inView:self.view];
    }else{
        [self dataRequest:textView.text];
    }
}

#pragma mark - request
-(void)dataRequest:(NSString *)reason{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"closeOrder",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId,
                         @"reason":reason
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reflush_hotellist_noti" object:nil];
                for (NSInteger i=self.navigationController.viewControllers.count-1; i>=0; i--) {
                    UIViewController *controller=self.navigationController.viewControllers[i];
                    
                    if ([controller isKindOfClass:[HotelOrderList class]]) {
                        [self.navigationController popToViewController:controller animated:YES];
                        break;
                    }
                    
                    if ([controller isKindOfClass:[HotelHomeController class]]) {
                        [self.navigationController popToViewController:controller animated:YES];
                        break;
                    }
                }
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
