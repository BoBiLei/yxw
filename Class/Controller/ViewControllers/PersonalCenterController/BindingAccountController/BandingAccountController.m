//
//  BandingAccountController.m
//  youxia
//
//  Created by mac on 16/7/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BandingAccountController.h"
#import "BindingPhoneController.h"
#import "PersonalCenterController.h"
#import "PersonInfoController.h"
#import "WelcomeController.h"

@interface BandingAccountController ()

@end

@implementation BandingAccountController{
    UITextField *phoneTf;
    UITextField *validTf;
    UIButton *bindingBtn;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"绑定已有账号";
    [self.view addSubview:navBar];
    
    //
    UIButton *rightBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-40, 25, 34, 34)];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitle:@"跳过" forState:UIControlStateNormal];
    rightBtn.titleLabel.font=kFontSize17;
    rightBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(1, -4, 0, 0)];
    [self.view addSubview:rightBtn];
    [rightBtn addTarget:self action:@selector(clickRightBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    [self setUpTipView];
    
    //
    [self setUpScrollerView];
}

-(void)clickRightBtn{
    CGFloat vCtrs=self.navigationController.viewControllers.count;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if (vCtrs==3) {
            if([controller isKindOfClass:[PersonalCenterController class]]){
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else if([controller isKindOfClass:[WelcomeController class]]){
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }else if (vCtrs==4){
            if ([controller isKindOfClass:[PersonInfoController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }
}

-(void)setUpTipView{
    UIImageView *tipView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 49)];
    tipView.image=[UIImage imageNamed:@"tipview_img"];
    [self.view addSubview:tipView];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 0, tipView.width-20, tipView.height)];
    tipLabel.font=kFontSize14;
    tipLabel.text=@"绑定已注册的账号，将把两个游侠账号的 ”个人中心“ 所有个人信息内容进行合并，请留意！";
    tipLabel.textColor=[UIColor colorWithHexString:@"#8b8b8b"];
    tipLabel.numberOfLines=0;
    [tipView addSubview:tipLabel];
}

-(void)setUpScrollerView{
    UIScrollView *scroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 113, SCREENSIZE.width, SCREENSIZE.height-113)];
    [self.view addSubview:scroller];
    
    UIImageView *phoneImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 74, 74)];
    phoneImgv.layer.cornerRadius=phoneImgv.height/2;
    phoneImgv.layer.masksToBounds=YES;
    phoneImgv.backgroundColor=[UIColor orangeColor];
    phoneImgv.center=CGPointMake(scroller.width/2, phoneImgv.height/2+18);
    [scroller addSubview:phoneImgv];
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
        phoneImgv.image=image;
    }else{
        [phoneImgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
    
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, phoneImgv.origin.y+phoneImgv.height+10, SCREENSIZE.width, 21)];
    nameLabel.font=kFontSize15;
    nameLabel.textAlignment=NSTextAlignmentCenter;
    NSString *nameStr;
    if (![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]) {
        nameStr=[AppUtils getValueWithKey:User_Name];
    }
    nameLabel.text=nameStr;
    nameLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    [scroller addSubview:nameLabel];
    
    //
    //
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(26, nameLabel.origin.y+nameLabel.height+20, SCREENSIZE.width-52, 32)];
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [phoneTf addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    phoneTf.placeholder=@"用户名/手机号";
    phoneTf.font=kFontSize16;
    [phoneTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:phoneTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, phoneTf.origin.y+phoneTf.height+1)];
    
    //
    validTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height+22, phoneTf.width, phoneTf.height)];
    [validTf addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    validTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    validTf.secureTextEntry=YES;
    validTf.placeholder=@"请输入密码";
    validTf.font=kFontSize16;
    [validTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    validTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:validTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(validTf.origin.x, validTf.origin.y+validTf.height+1) endPoint:CGPointMake(phoneTf.origin.x+phoneTf.width, validTf.origin.y+validTf.height+1)];
    
    
    //button
    bindingBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    bindingBtn.frame=CGRectMake(0, 0, SCREENSIZE.width-phoneTf.origin.x*2, 42);
    bindingBtn.center=CGPointMake(SCREENSIZE.width/2, validTf.center.y+70);
    bindingBtn.layer.cornerRadius=bindingBtn.height/2;
    bindingBtn.enabled=NO;
    [bindingBtn setTitle:@"确认绑定" forState:UIControlStateNormal];
    [bindingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bindingBtn.titleLabel.font=kFontSize16;
    [bindingBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:bindingBtn];
}

#pragma mark - text值改变时

-(void)textDidChange{
    if (![phoneTf.text isEqualToString:@""]&&
        ![validTf.text isEqualToString:@""]
        ) {
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        bindingBtn.enabled=YES;
    }else{
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        bindingBtn.enabled=NO;
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    [self submitRequest];
}

//绑定手机号请求
-(void)submitRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"do_bind_user_phone_ios" forKey:@"code"];
    [dict setObject:phoneTf.text forKey:@"username"];
    [dict setObject:validTf.text forKey:@"password"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    //
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            [AppUtils showSuccessMessage:@"成功绑定账号！" inView:self.navigationController.view];
            
            [AppUtils saveValue:phoneTf.text forKey:User_Phone];
            
            //更新个人资料的手机号
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BandPhoneSuccess" object:nil];
            
            /*******
             1、改变Is_NewPwd
             2、把修改密码覆盖到保存密码，以方便自动登录
             3、发送通知到个人中心，更改设置新密码为修改密码
             4、pop到上级Controller
             *******/
            //0--未修改过的  1--修改过的
            [AppUtils saveValue:@"1" forKey:Is_NewPwd];
            [AppUtils saveValue:validTf.text forKey:User_Pass];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_Is_NewPwd" object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:nil];
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[BindingPhoneController class]]) {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:LoginSuccessNotifition object:nil];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.navigationController.view];
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
