//
//  NewCouponCell.m
//  youxia
//
//  Created by mac on 16/5/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewCouponCell.h"

#define CPCell_Height 124
@implementation NewCouponCell{
    UILabel *mtLabel;
    CGFloat Btn_Width;
    UILabel *moneyLabel;
    UILabel *sLabel01;
    
    UIView *couponNumView;
    UILabel *couponNumLabel;
    
    UILabel *endTimeLabel;
    
    UIImageView *useImgv;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    Btn_Width=124*140/200;
    
    //
    UIImageView *juImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.height-25, self.height-25)];
    juImgv.image=[UIImage imageNamed:@"coupon_juan"];
    [self addSubview:juImgv];
    
    //
    mtLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 22, 22, 22)];
    mtLabel.textAlignment=NSTextAlignmentRight;
    mtLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?21:22];
    mtLabel.text=@"￥";
    mtLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
    [self addSubview:mtLabel];
    
    //moneyLabel
    moneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(mtLabel.origin.x+mtLabel.width, mtLabel.origin.y, 81, 55)];
    moneyLabel.textAlignment=NSTextAlignmentRight;
    moneyLabel.font=[UIFont systemFontOfSize:45];
    moneyLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
    [self addSubview:moneyLabel];
    
    //couponNumView
    couponNumView=[[UIView alloc] initWithFrame:CGRectMake(moneyLabel.origin.x+moneyLabel.width+2, moneyLabel.origin.y, SCREENSIZE.width-(moneyLabel.origin.x+moneyLabel.width+2)-(Btn_Width+2), moneyLabel.height)];
    [self addSubview:couponNumView];
    
    couponNumLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 6, couponNumView.width, couponNumView.height/3)];
    couponNumLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
    couponNumLabel.textColor=[UIColor colorWithHexString:@"#3f93cd"];
    couponNumLabel.adjustsFontSizeToFitWidth=YES;
    [couponNumView addSubview:couponNumLabel];
    
    sLabel01=[[UILabel alloc] initWithFrame:CGRectMake(0, couponNumLabel.height+couponNumLabel.origin.y-2, couponNumLabel.width, couponNumView.height-couponNumLabel.height-6)];
    sLabel01.font=[UIFont systemFontOfSize:IS_IPHONE5?18:19];
    sLabel01.text=@"元优惠券";
    sLabel01.textColor=[UIColor colorWithHexString:@"#0080c5"];
    [couponNumView addSubview:sLabel01];
    
    //有效期
    endTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(mtLabel.origin.x, moneyLabel.origin.y+moneyLabel.height+4, SCREENSIZE.width-(moneyLabel.origin.x)-(Btn_Width+8), 21)];
    endTimeLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
    endTimeLabel.textColor=[UIColor colorWithHexString:@"#979797"];
    [self addSubview:endTimeLabel];
    
    //
    CGRect mFrame=self.frame;
    mFrame.size.height=endTimeLabel.origin.y+endTimeLabel.height+moneyLabel.origin.y;
    self.frame=mFrame;
    
    //Btn_Width/self.height=157/200
    
    //使用按钮
    useImgv=[[UIImageView alloc] initWithFrame:CGRectMake(SCREENSIZE.width-Btn_Width, 0, Btn_Width, self.height)];
    useImgv.contentMode=UIViewContentModeScaleAspectFit;
    [useImgv setImage:[UIImage imageNamed:@"coupon_use"]];
    [self addSubview:useImgv];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickUseBtn)];
    [useImgv addGestureRecognizer:tap];
    
}

-(void)clickUseBtn{
    [self.delegate clickUserButton];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(CouponModel *)model{
    
    //money
    CGFloat moneyFloat=model.priceSum.floatValue;
    NSString *moneyStr=[NSString stringWithFormat:@"%.f",moneyFloat];
    CGRect mnlFrame=moneyLabel.frame;
    CGSize mnlSize=[AppUtils getStringSize:moneyStr withFont:45];
    mnlFrame.size.width=mnlSize.width;
    moneyLabel.frame=mnlFrame;
    moneyLabel.text=moneyStr;
    
    //优惠券号
    couponNumView.frame=CGRectMake(moneyLabel.origin.x+moneyLabel.width, moneyLabel.origin.y, SCREENSIZE.width-(moneyLabel.origin.x+moneyLabel.width)-(Btn_Width+8), moneyLabel.height);
    couponNumLabel.text=[NSString stringWithFormat:@"劵号：%@",model.couponNumber];
    
    //=1未使用
    
    //有效期
    NSString *valiStr;
    
    //当前时间戳
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    
    //时间戳转换
    NSTimeInterval time=[model.validity doubleValue];
    
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    [dateFormatter setTimeZone:timeZone];
    
    if ([model.status isEqualToString:@"1"]) {
        useImgv.userInteractionEnabled=YES;
        
        if ([model.validity isEqualToString:@"0"]) {
            valiStr=@"无限期";
        }else{
            
            //不是7天以内的优惠券  显示未使用
            if (model.validity.integerValue-time_stamp.integerValue>=604800) {
                mtLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
                moneyLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
                couponNumLabel.textColor=[UIColor colorWithHexString:@"#3f93cd"];
                sLabel01.textColor=[UIColor colorWithHexString:@"#0080c5"];
                [useImgv setImage:[UIImage imageNamed:@"coupon_use"]];
                valiStr = [dateFormatter stringFromDate: detaildate];
            }else{
                
                //说明是7天以内的优惠券  显示快过期
                if (model.validity.integerValue-time_stamp.integerValue>=0) {
                    mtLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
                    moneyLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
                    couponNumLabel.textColor=[UIColor colorWithHexString:@"#3f93cd"];
                    sLabel01.textColor=[UIColor colorWithHexString:@"#0080c5"];
                    [useImgv setImage:[UIImage imageNamed:@"coupon_ReadyTodate"]];
                    NSInteger days=(model.validity.integerValue-time_stamp.integerValue)/3600/24;
                    DSLog(@"%ld",(long)days);
                    valiStr = [NSString stringWithFormat:@"%@（%ld天后过期）",[dateFormatter stringFromDate: detaildate],(long)days];
                }
                
                //说明已过期
                else{
                    mtLabel.textColor=[UIColor lightGrayColor];
                    moneyLabel.textColor=[UIColor lightGrayColor];
                    couponNumLabel.textColor=[UIColor lightGrayColor];
                    sLabel01.textColor=[UIColor lightGrayColor];
                    [useImgv setImage:[UIImage imageNamed:@"coupon_overdue"]];
                    valiStr = [NSString stringWithFormat:@"%@（已过期）",[dateFormatter stringFromDate: detaildate]];
                }
            }
        }
    }
    //=0已使用
    else{
        useImgv.userInteractionEnabled=NO;
        mtLabel.textColor=[UIColor lightGrayColor];
        moneyLabel.textColor=[UIColor lightGrayColor];
        couponNumLabel.textColor=[UIColor lightGrayColor];
        sLabel01.textColor=[UIColor lightGrayColor];
        [useImgv setImage:[UIImage imageNamed:@"coupon_haduse"]];
        valiStr = [NSString stringWithFormat:@"%@（已使用）",[dateFormatter stringFromDate: detaildate]];
    }
    endTimeLabel.text=[NSString stringWithFormat:@"有效时间：%@",valiStr];
}

@end
