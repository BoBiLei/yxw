//
//  NewCouponCell.h
//  youxia
//
//  Created by mac on 16/5/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"

@protocol NewCouponCellDelegate <NSObject>

-(void)clickUserButton;

@end

@interface NewCouponCell : UITableViewCell

@property (nonatomic, retain) CouponModel *model;
@property (nonatomic, weak) id<NewCouponCellDelegate> delegate;

@end
