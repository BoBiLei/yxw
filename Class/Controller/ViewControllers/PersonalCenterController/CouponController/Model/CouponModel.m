//
//  CouponModel.m
//  youxia
//
//  Created by mac on 15/12/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "CouponModel.h"

@implementation CouponModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.couponId=dic[@"id"];
    self.couponNumber=dic[@"number"];
    self.priceSum=dic[@"price"];
    self.validity=dic[@"duetime"];
    self.status=dic[@"status"]; //=1,没使用 其他都是使用
//    NSMutableString *statusStr=[NSMutableString stringWithString:dic[@"handler"]];
//    NSRange range=[statusStr rangeOfString:@"<em>"];
//    if (range.location!=NSNotFound) {
//        [statusStr deleteCharactersInRange:range];
//        NSRange range1=[statusStr rangeOfString:@"</em>"];
//        if (range1.location!=NSNotFound) {
//            [statusStr deleteCharactersInRange:range1];
//        }
//    }
//    
//    DSLog(@"%@",statusStr);
//    self.status=statusStr;
}

@end
