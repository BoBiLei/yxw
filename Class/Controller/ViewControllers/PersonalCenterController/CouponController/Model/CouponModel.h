//
//  CouponModel.h
//  youxia
//
//  Created by mac on 15/12/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponModel : NSObject

@property (nonatomic,copy) NSString *couponId;
@property (nonatomic,copy) NSString *couponNumber;
@property (nonatomic,copy) NSString *priceSum;
@property (nonatomic,copy) NSString *validity;//有效期
@property (nonatomic,copy) NSString *status;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
