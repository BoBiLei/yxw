//
//  AddCouponController.m
//  youxia
//
//  Created by mac on 15/12/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "AddCouponController.h"

@interface AddCouponController ()<UITableViewDelegate>

@end

@implementation AddCouponController{
    UITableView *myTable;
    
    UITextField *cartText;
    UITextField *passText;
    UIButton *button;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"添加优惠券页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"添加优惠券页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"添加优惠券";
    [self.view addSubview:navBar];
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 64, SCREENSIZE.width-16, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.sectionHeaderHeight = 70;
    [self.view addSubview:myTable];
    myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self setUpHeaderView];
}

-(void)setUpHeaderView{
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 234)];
    headView.backgroundColor=[UIColor whiteColor];
    myTable.tableHeaderView=headView;
    myTable.delegate=self;
    
    //标题
    UIView *titView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, headView.frame.size.width, 58)];
    titView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    [headView addSubview:titView];
    
    //
    UIView *bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, titView.frame.size.width, 14)];
    bgView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [titView addSubview:bgView];
    
    //
    UILabel *titLabel=[UILabel new];
    titLabel.text=@"绑定新礼品卡";
    titLabel.font=kFontSize15;
    titLabel.textAlignment=NSTextAlignmentLeft;
    titLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [titView addSubview:titLabel];
    titLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* titLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[titLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titLabel,titLabel)];
    [NSLayoutConstraint activateConstraints:titLabel_h];
    
    NSArray* titLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[bgView][titLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bgView,titLabel)];
    [NSLayoutConstraint activateConstraints:titLabel_w];
    
    //top line
    UIView *line=[[UIView alloc]init];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [headView addSubview:line];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
    [NSLayoutConstraint activateConstraints:line_h];
    
    NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titLabel][line(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titLabel,line)];
    [NSLayoutConstraint activateConstraints:line_w];
    
    //卡号View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [headView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line]-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    UILabel *cartLabel=[[UILabel alloc]init];
    cartLabel.textAlignment=NSTextAlignmentCenter;
    cartLabel.text=@"卡  号：";
    cartLabel.font=kFontSize15;
    cartLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartLabel];
    cartLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartLabel(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_h];
    
    NSArray* cartLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_w];
    
    //
    cartText=[[UITextField alloc]init];
    cartText.clearButtonMode=UITextFieldViewModeWhileEditing;
    cartText.secureTextEntry = NO;
    [cartText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    cartText.placeholder=@"礼品卡号";
    cartText.font=kFontSize15;
    cartText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:cartText];
    cartText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cartLabel]-8-[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel,cartText)];
    [NSLayoutConstraint activateConstraints:cartText_h];
    
    NSArray* cartText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartText)];
    [NSLayoutConstraint activateConstraints:cartText_w];
    
    //密码View
    UIView *passView=[[UIView alloc]init];
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [headView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[passView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[[UILabel alloc]init];
    passLabel.textAlignment=NSTextAlignmentCenter;
    passLabel.text=@"密  码：";
    passLabel.font=kFontSize15;
    passLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLabel(cartLabel)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,cartLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    //
    passText=[[UITextField alloc]init];
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.secureTextEntry = YES;
    [passText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.placeholder=@"密码";
    passText.font=kFontSize15;
    passText.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-8-[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"绑定" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=kFontSize16;
    [headView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[passView]-12-[button(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView,button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - text值改变时
-(void)textDidChange{
    if (![cartText.text isEqualToString:@""]&&
        ![passText.text isEqualToString:@""]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - 点击绑定按钮
-(void)clickBtn{
    [AppUtils closeKeyboard];
    [self requestHttpsForCartNum:cartText.text pass:passText.text];
}

#pragma mark Request 请求
-(void)requestHttpsForCartNum:(NSString *)cartNum pass:(NSString *)pass{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"addcashcard_iso" forKey:@"code"];
    [dict setObject:cartText.text forKey:@"gifCard_id"];
    [dict setObject:passText.text forKey:@"gifCard_paw"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"======%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCartNum:cartText.text pass:passText.text];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpDateCoupon" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:@"您输入的卡号或密码有误！" inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
