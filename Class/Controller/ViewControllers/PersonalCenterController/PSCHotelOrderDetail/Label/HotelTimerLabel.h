//
//  HotelTimerLabel.h
//  youxia
//
//  Created by mac on 2016/12/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelTimerDelegate <NSObject>

-(void)isEndTime;

@end

@interface HotelTimerLabel : UILabel

@property (nonatomic,assign)NSInteger second;
@property (nonatomic,assign)NSInteger minute;
//@property (nonatomic,assign)NSInteger hour;

@property (weak,nonatomic) id<HotelTimerDelegate> delegate;

@end
