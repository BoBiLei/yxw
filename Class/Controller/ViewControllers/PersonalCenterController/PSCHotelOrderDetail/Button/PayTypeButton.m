//
//  PayTypeButton.m
//  YXJR
//
//  Created by mac on 2016/10/26.
//  Copyright © 2016年 游侠金融. All rights reserved.
//

#import "PayTypeButton.h"

@implementation PayTypeButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.leftImgv];
    [self addSubview:self.textLabel];
    [self addSubview:self.seleImgv];
}

-(UIImageView *)leftImgv{
    if (!_leftImgv) {
        _leftImgv=[[UIImageView alloc] init];
        _leftImgv.contentMode=UIViewContentModeScaleAspectFit;
    }
    return _leftImgv;
}

-(UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel=[[UILabel alloc] init];
    }
    return _textLabel;
}

-(UIImageView *)seleImgv{
    if (!_seleImgv) {
        _seleImgv=[[UIImageView alloc] init];
        _seleImgv.contentMode=UIViewContentModeScaleAspectFit;
    }
    return _seleImgv;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.leftImgv.frame = CGRectMake(12, 10, height-20, height-20);
    self.textLabel.frame = CGRectMake(self.leftImgv.origin.x+self.leftImgv.width+8, 0, 100, height);
    self.seleImgv.frame = CGRectMake(width-(height-11), 12, height-24, height-24);
}

@end
