//
//  PayTypeButton.h
//  YXJR
//
//  Created by mac on 2016/10/26.
//  Copyright © 2016年 游侠金融. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayTypeButton : UIControl

@property (nonatomic, strong) UIImageView *leftImgv;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *seleImgv;    

@end
