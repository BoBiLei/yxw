//
//  PSCHotelOrderDetailModel.m
//  youxia
//
//  Created by mac on 2017/5/3.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelOrderDetailModel.h"

@implementation PSCHotelOrderDetailModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"HotelAddress"] isKindOfClass:[NSNull class]]) {
        self.hotelAddress=@"";
    }else{
        self.hotelAddress=dic[@"HotelAddress"];
    }
    
    //
    if ([dic[@"HotelLogo"] isKindOfClass:[NSNull class]]) {
        self.hotelLogo=@"";
    }else{
        self.hotelLogo=dic[@"HotelLogo"];
    }
    
    //
    if ([dic[@"HotelName"] isKindOfClass:[NSNull class]]) {
        self.hotelName=@"";
    }else{
        self.hotelName=dic[@"HotelName"];
    }
    
    //
    if ([dic[@"RatePlanID"] isKindOfClass:[NSNull class]]) {
        self.ratePlanID=@"";
    }else{
        self.ratePlanID=dic[@"RatePlanID"];
    }
    
    //
    if ([dic[@"RatePlanName"] isKindOfClass:[NSNull class]]) {
        self.ratePlanName=@"";
    }else{
        self.ratePlanName=dic[@"RatePlanName"];
    }
    
    //
    if ([dic[@"adult"] isKindOfClass:[NSNull class]]) {
        self.adult=@"";
    }else{
        self.adult=dic[@"adult"];
    }
    
    //
    if ([dic[@"checkin"] isKindOfClass:[NSNull class]]) {
        self.checkin=@"";
    }else{
        self.checkin=dic[@"checkin"];
    }
    
    //
    if ([dic[@"checkout"] isKindOfClass:[NSNull class]]) {
        self.checkout=@"";
    }else{
        self.checkout=dic[@"checkout"];
    }
    
    //
    if ([dic[@"child"] isKindOfClass:[NSNull class]]) {
        self.child=@"";
    }else{
        self.child=dic[@"child"];
    }
    
    //
    if ([dic[@"contactEmail"] isKindOfClass:[NSNull class]]) {
        self.contactEmail=@"";
    }else{
        self.contactEmail=dic[@"contactEmail"];
    }
    
    //
    if ([dic[@"contactName"] isKindOfClass:[NSNull class]]) {
        self.contactName=@"";
    }else{
        self.contactName=dic[@"contactName"];
    }
    
    //
    if ([dic[@"contactPhone"] isKindOfClass:[NSNull class]]) {
        self.contactPhone=@"";
    }else{
        self.contactPhone=dic[@"contactPhone"];
    }
    
    //
    if ([dic[@"guestInfo"] isKindOfClass:[NSNull class]]) {
        self.guestInfo=@{};
    }else{
        self.guestInfo=dic[@"guestInfo"];
    }
    
    //
    if ([dic[@"invoice"] isKindOfClass:[NSNull class]]) {
        self.invoice=@"";
    }else{
        self.invoice=dic[@"invoice"];
    }
    
    //
    if ([dic[@"invoiceHeader"] isKindOfClass:[NSNull class]]) {
        self.invoiceHeader=@"";
    }else{
        self.invoiceHeader=dic[@"invoiceHeader"];
    }
    
    //
    if ([dic[@"iosGuestInfo"] isKindOfClass:[NSNull class]]) {
        self.iosGuestInfo=@[];
    }else{
        self.iosGuestInfo=dic[@"iosGuestInfo"];
    }
    
    //
    if ([dic[@"nights"] isKindOfClass:[NSNull class]]) {
        self.nights=@"";
    }else{
        self.nights=dic[@"nights"];
    }
    
    //
    if ([dic[@"orderid"] isKindOfClass:[NSNull class]]) {
        self.orderid=@"";
    }else{
        self.orderid=dic[@"orderid"];
    }
    
    //
    if ([dic[@"payMoney"] isKindOfClass:[NSNull class]]) {
        self.payMoney=@"0";
    }else{
        self.payMoney=dic[@"payMoney"];
    }
    
    //
    if ([dic[@"roomDetail"] isKindOfClass:[NSNull class]]) {
        self.roomDetail=@{};
    }else{
        self.roomDetail=dic[@"roomDetail"];
    }
    
    //
    if ([dic[@"rooms"] isKindOfClass:[NSNull class]]) {
        self.rooms=@"";
    }else{
        self.rooms=dic[@"rooms"];
    }
    
    //
    if ([dic[@"status"] isKindOfClass:[NSNull class]]) {
        self.status=@"";
    }else{
        self.status=dic[@"status"];
    }
    
    //
    if ([dic[@"minute"] isKindOfClass:[NSNull class]]) {
        self.minute=@"0";
    }else{
        self.minute=dic[@"minute"];
    }
    
    //
    if ([dic[@"second"] isKindOfClass:[NSNull class]]) {
        self.second=@"";
    }else{
        self.second=dic[@"second"];
    }
    
    //
    if ([dic[@"Tel"] isKindOfClass:[NSNull class]]) {
        self.tel=@"";
    }else{
        self.tel=dic[@"Tel"];
    }
    
    if ([dic[@"customerRequest"] isKindOfClass:[NSNull class]]) {
        self.customerRequest=@"";
    }else{
        self.customerRequest=dic[@"customerRequest"];
    }
    
}

@end
