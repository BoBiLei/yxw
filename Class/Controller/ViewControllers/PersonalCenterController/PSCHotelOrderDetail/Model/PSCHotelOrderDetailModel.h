//
//  PSCHotelOrderDetailModel.h
//  youxia
//
//  Created by mac on 2017/5/3.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSCHotelOrderDetailModel : NSObject

@property (nonatomic, copy) NSString *hotelAddress;
@property (nonatomic, copy) NSString *hotelLogo;
@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, copy) NSString *ratePlanID;
@property (nonatomic, copy) NSString *ratePlanName;
@property (nonatomic, copy) NSString *adult;
@property (nonatomic, copy) NSString *checkin;
@property (nonatomic, copy) NSString *checkout;
@property (nonatomic, copy) NSString *child;
@property (nonatomic, copy) NSString *contactEmail;
@property (nonatomic, copy) NSString *contactName;
@property (nonatomic, copy) NSString *contactPhone;
@property (nonatomic, copy) NSDictionary *guestInfo;
@property (nonatomic, copy) NSString *invoice;
@property (nonatomic, copy) NSString *invoiceHeader;
@property (nonatomic, copy) NSArray *iosGuestInfo;
@property (nonatomic, copy) NSString *nights;
@property (nonatomic, copy) NSString *orderid;
@property (nonatomic, copy) NSString *payMoney;
@property (nonatomic, copy) NSDictionary *roomDetail;
@property (nonatomic, copy) NSString *rooms;
@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *minute;
@property (nonatomic, copy) NSString *second;

@property (nonatomic, copy) NSString *tel;   //电话

@property (nonatomic, copy) NSString *customerRequest;   //特殊要求

-(void)jsonToModel:(NSDictionary *)dic;

@end
