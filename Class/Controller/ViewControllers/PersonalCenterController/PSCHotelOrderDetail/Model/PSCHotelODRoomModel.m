//
//  PSCHotelODRoomModel.m
//  youxia
//
//  Created by mac on 2017/5/3.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODRoomModel.h"

@implementation PSCHotelODRoomModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"adultNum"] isKindOfClass:[NSNull class]]) {
        self.adultNum=@"";
    }else{
        self.adultNum=dic[@"adultNum"];
    }
    
    //
    if ([dic[@"adultName"] isKindOfClass:[NSNull class]]) {
        self.adultName=@"";
    }else{
        self.adultName=dic[@"adultName"];
    }
    
    //
    if ([dic[@"childNum"] isKindOfClass:[NSNull class]]) {
        self.childNum=@"";
    }else{
        self.childNum=dic[@"childNum"];
    }
    
    //
    if ([dic[@"childName"] isKindOfClass:[NSNull class]]) {
        self.childName=@"";
    }else{
        self.childName=dic[@"childName"];
    }
}

@end
