//
//  PSCHotelODRoomModel.h
//  youxia
//
//  Created by mac on 2017/5/3.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSCHotelODRoomModel : NSObject

@property (nonatomic, copy) NSString *adultNum;
@property (nonatomic, copy) NSString *adultName;
@property (nonatomic, copy) NSString *childNum;
@property (nonatomic, copy) NSString *childName;

-(void)jsonToModel:(NSDictionary *)dic;

@end
