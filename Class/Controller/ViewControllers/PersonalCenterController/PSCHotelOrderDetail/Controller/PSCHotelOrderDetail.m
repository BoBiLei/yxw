//
//  PSCHotelOrderDetail.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelOrderDetail.h"
#import "PSCHotelODFirstCell.h"
#import "PSCHotelODSecondCell.h"
#import "PSCHotelODRoomCell.h"
#import "PSCHotelODContactCell.h"
#import "PSCHotelODShouJuCell.h"
#import "PSCHotelODIssueCell.h"
#import "PSCHotelOrderTrack.h"
#import "PSCHotelFeeDetail.h"
#import "CancelHotelOrder.h"
#import "CustomIOSAlertView.h"
#import "PayTypeButton.h"
#import "ApplyRefundController.h"
#import "PSCHotelOrderDetailModel.h"
#import "PSCHotelODRoomModel.h"
#import "HotelTimerLabel.h"
#import "HotelRefundDetail.h"
#import "FilmWeiXinPayModel.h"
#import "WXApi.h"
#import "HoteOreadPayController.h"
#import "HotelOrderList.h"
#import "HotelHomeController.h"

@interface PSCHotelOrderDetail ()<UITableViewDataSource,UITableViewDelegate,PSCHotelODSecondDelegate,HotelTimerDelegate>

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, strong) UITableView *myTable;

@property (nonatomic, strong) UIView *footView;

@property (nonatomic, retain) NSMutableArray *roomDataArr;

@end

@implementation PSCHotelOrderDetail{
    PSCHotelOrderDetailModel *model;

    CustomIOSAlertView *validaAlertView;
    PayTypeButton *seleBtn;
    UILabel *moneyLabel;
}

+(id)initWithOrderId:(NSString *)orderId{
    PSCHotelOrderDetail *vc=[PSCHotelOrderDetail new];
    vc.orderId=orderId;
    return vc;
}

-(void)viewDidAppear:(BOOL)animated{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *nav=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    nav.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:nav];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UIButton *titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.titleLabel.font=kFontSize16;
    [titleLabel setTitle:@"订单详情" forState:UIControlStateNormal];
    [titleLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:titleLabel];
    
    [self dataRequest];
}

-(void)turnBack{
    if ([model.status isEqualToString:@"0"]) {
        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"确定离开支付页面"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
            
        }];
        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
            for (NSInteger i=self.navigationController.viewControllers.count-1; i>=0; i--) {
                UIViewController *controller=self.navigationController.viewControllers[i];
                if ([controller isKindOfClass:[PSCHotelOrderDetail class]]) {
                    [self.navigationController popViewControllerAnimated:YES];
                    return;
                }
                
                if ([controller isKindOfClass:[HotelOrderList class]]) {
                    [self.navigationController popToViewController:controller animated:YES];
                    return;
                }
                
                if ([controller isKindOfClass:[HotelHomeController class]]) {
                    [self.navigationController popToViewController:controller animated:YES];
                    return;
                }
            }
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)payResult{
    [self payResultRequest];
}

-(NSMutableArray *)roomDataArr{
    if (!_roomDataArr) {
        _roomDataArr=[NSMutableArray array];
    }
    return _roomDataArr;
}

-(UITableView *)myTable{
    if (!_myTable) {
        CGFloat tbHeight=0;
        if([model.status isEqualToString:@"0"]){
            tbHeight=SCREENSIZE.height-146;
        }else if([model.status isEqualToString:@"1"]){
            tbHeight=SCREENSIZE.height-64;
        }else{
            tbHeight=SCREENSIZE.height-110;
        }
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, tbHeight) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODFirstCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODFirstCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODSecondCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODSecondCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODRoomCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODRoomCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODContactCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODContactCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODShouJuCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODShouJuCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelODIssueCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelODIssueCell"];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [self.view addSubview:_myTable];
    }
    return _myTable;
}

-(UIView *)footView{
    if (!_footView) {
        CGFloat tbHeight=0;
        if([model.status isEqualToString:@"0"]){
            tbHeight=82;
        }else if([model.status isEqualToString:@"1"]){
            tbHeight=0;
        }else{
            tbHeight=46;
        }
        _footView=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-tbHeight, SCREENSIZE.width, tbHeight)];
        /*
         0:订单创建成功(待付款), 1:订单支付成功(确认酒店信息中),
         2:酒店信息确认成功, 3:订单被取消,
         4:酒店信息确认失败, 5:客户成功入住,
         6:订单关闭(用户申请关闭或支付超时关闭), 7:用户申请退款,
         8:退款完成, 9:支付异常订单
         */
        switch (model.status.integerValue) {
            case 0:
            {
                HotelTimerLabel *timeLabel=[[HotelTimerLabel alloc] initWithFrame:CGRectMake(14, 0, SCREENSIZE.width-28, 32)];
                timeLabel.textColor=[UIColor colorWithHexString:@""];
                timeLabel.font=kFontSize16;
                timeLabel.textAlignment=NSTextAlignmentRight;
                timeLabel.textColor=[UIColor colorWithHexString:@"fa8d00"];
                timeLabel.minute=model.minute.integerValue;
                timeLabel.second=model.second.integerValue;
                timeLabel.delegate=self;
                [_footView addSubview:timeLabel];
                
                UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, timeLabel.origin.y+timeLabel.height, SCREENSIZE.width, 0.5f)];
                line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
                [_footView addSubview:line];
                
                //
                UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                rightBtn.frame=CGRectMake(SCREENSIZE.width-96, line.origin.y+8, 82, 32);
                rightBtn.layer.borderWidth=0.5f;
                rightBtn.layer.borderColor=[UIColor colorWithHexString:@"fa8d00"].CGColor;
                rightBtn.titleLabel.font=kFontSize16;
                [rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
                [rightBtn setTitleColor:[UIColor colorWithHexString:@"fa8d00"] forState:UIControlStateNormal];
                [rightBtn addTarget:self action:@selector(clickPayBtn) forControlEvents:UIControlEventTouchUpInside];
                [_footView addSubview:rightBtn];
                
                //
                UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                leftBtn.frame=CGRectMake(SCREENSIZE.width-186, rightBtn.origin.y, rightBtn.width, rightBtn.height);
                leftBtn.layer.borderWidth=0.5f;
                leftBtn.layer.borderColor=[UIColor colorWithHexString:@"ababab"].CGColor;
                leftBtn.titleLabel.font=kFontSize16;
                [leftBtn setTitle:@"关闭订单" forState:UIControlStateNormal];
                [leftBtn setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
                [leftBtn addTarget:self action:@selector(clickCloseOrder) forControlEvents:UIControlEventTouchUpInside];
                [_footView addSubview:leftBtn];
                
                //
                moneyLabel=[[UILabel alloc] initWithFrame:CGRectMake(14, leftBtn.origin.y, SCREENSIZE.width-leftBtn.origin.x-22, leftBtn.height)];
                moneyLabel.textColor=[UIColor colorWithHexString:@""];
                moneyLabel.font=kFontSize16;
                moneyLabel.text=[NSString stringWithFormat:@"合计：￥%@",model.payMoney];
                [_footView addSubview:moneyLabel];
            }
                break;
            case 1:
                
                break;
            case 2:
            {
                _footView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
                //
                UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame=CGRectMake(0, _footView.height-46, _footView.width, 46);
                btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
                btn.titleLabel.font=kFontSize16;
                [btn setTitle:@"申请退款" forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickRefundBtn) forControlEvents:UIControlEventTouchUpInside];
                [_footView addSubview:btn];
            }
                break;
            case 7: case 8:
            {
                _footView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
                //
                UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame=CGRectMake(0, _footView.height-46, _footView.width, 46);
                btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
                btn.titleLabel.font=kFontSize16;
                [btn setTitle:@"查看退款详情" forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickRefundDetail) forControlEvents:UIControlEventTouchUpInside];
                [_footView addSubview:btn];
            }
                break;
            default:
            {
                _footView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
                //
                UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame=CGRectMake(0, _footView.height-46, _footView.width, 46);
                btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
                btn.titleLabel.font=kFontSize16;
                [btn setTitle:@"再次预定" forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickReBuy) forControlEvents:UIControlEventTouchUpInside];
                [_footView addSubview:btn];
            }
                break;
        }
    }
    return _footView;
}

#pragma mark - 点击申请退款按钮
-(void)clickRefundBtn{
    NSLog(@"点击申请退款按钮");
    [self.navigationController pushViewController:[ApplyRefundController initWithOrderId:model.orderid] animated:YES];
}

#pragma mark - 点击查看退款详情按钮
-(void)clickRefundDetail{
    [self.navigationController pushViewController:[HotelRefundDetail initWithOrderId:model.orderid] animated:YES];
}

#pragma mark - 点击再次预定按钮
-(void)clickReBuy{
    NSLog(@"点击再次预定");
}

#pragma mark - 点击去付款
-(void)clickPayBtn{
    validaAlertView = [[CustomIOSAlertView alloc] init];
    validaAlertView.containerView.layer.cornerRadius=12;
    [validaAlertView setContainerView:[self createAlertView]];
    [validaAlertView setButtonTitles:nil];
    [validaAlertView setUseMotionEffects:true];
    [validaAlertView show];
}

#pragma mark - 创建弹出验证码提示框

-(UIView *)createAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-72, 300)];
    view.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    view.layer.cornerRadius=8;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 10, view.width-20, 24)];
    tipLabel.textAlignment=NSTextAlignmentLeft;
    tipLabel.font=[UIFont systemFontOfSize:17];
    tipLabel.textColor=[UIColor colorWithHexString:@"000000"];
    tipLabel.text=@"支付方式";
    [view addSubview:tipLabel];
    
    //x按钮
    UIButton *closeImg=[UIButton buttonWithType:UIButtonTypeCustom] ;
    closeImg.frame=CGRectMake(view.width-40, 4, 36, 36);
    [closeImg setImage:[UIImage imageNamed:@"getpass_delete"] forState:UIControlStateNormal];
    [closeImg addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeImg];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"eeeeee"] height:1.0f firstPoint:CGPointMake(0, tipLabel.origin.y*2+tipLabel.height) endPoint:CGPointMake(view.width, tipLabel.origin.y*2+tipLabel.height)];
    
    //支付费用
    UILabel *payTip=[[UILabel alloc] initWithFrame:CGRectMake(0, tipLabel.origin.y*2+tipLabel.height+26, view.width, 19)];
    payTip.font=[UIFont systemFontOfSize:15];
    payTip.textAlignment=NSTextAlignmentCenter;
    payTip.text=@"需要支付费用";
    payTip.textColor=[UIColor colorWithHexString:@"545454"];
    [view addSubview:payTip];
    
    UILabel *payPrice=[[UILabel alloc] initWithFrame:CGRectMake(0, payTip.origin.y+payTip.height+1, view.width, 24)];
    payPrice.font=[UIFont systemFontOfSize:20];
    payPrice.textAlignment=NSTextAlignmentCenter;
    payPrice.text=[NSString stringWithFormat:@"￥%@",model.payMoney];
    [view addSubview:payPrice];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"eeeeee"] height:1.0f firstPoint:CGPointMake(8, payPrice.origin.y+payPrice.height+23) endPoint:CGPointMake(view.width-8, payPrice.origin.y+payPrice.height+23)];
    
    CGFloat btnY=0;
    CGFloat btnHeight=48;
    CGFloat btnLast=0;
    for (int i=0; i<2; i++) {
        btnY=payPrice.origin.y+payPrice.height+23+btnHeight*i;
        PayTypeButton *btn=[[PayTypeButton alloc] initWithFrame:CGRectMake(0, btnY, view.width, btnHeight)];
        btn.tag=i;
        btn.leftImgv.image=[UIImage imageNamed:i==0?@"pay_wx":@"pay_zfb"];
        btn.textLabel.text=i==0?@"微信支付":@"支付宝支付";
        btn.seleImgv.image=[UIImage imageNamed:i==0?@"cell_sele":@"cell_nor"];
        [btn addTarget:self action:@selector(selectPayTypeBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        
        if (i==0) {
            btn.enabled=NO;
            seleBtn=btn;
        }
        
        if (i==1) {
            btnLast=btn.origin.y+btn.height;
        }
        
        [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"eeeeee"] height:1.0f firstPoint:CGPointMake(8, btn.origin.y+btn.height) endPoint:CGPointMake(view.width-8, btn.origin.y+btn.height)];
    }
    
    //
    UIButton *submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame=CGRectMake(10, btnLast+32, view.width-20, 48);
    submitBtn.layer.cornerRadius=4;
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:submitBtn];
    
    //
    CGFloat lastYH=submitBtn.origin.y+submitBtn.height+12;
    
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+22;
    view.frame=frame;
    
    return view;
}

#pragma mark - 点击提交付款
-(void)clickSubmitBtn{
    NSLog(@"%ld",seleBtn.tag);
    [validaAlertView close];
    [self getPayParamRequest];
}

-(void)selectPayTypeBtn:(id)sender{
    seleBtn.seleImgv.image=[UIImage imageNamed:@"cell_nor"];
    seleBtn.enabled=YES;
    
    PayTypeButton *btn=sender;
    btn.seleImgv.image=[UIImage imageNamed:@"cell_sele"];
    btn.enabled=YES;
    
    seleBtn=btn;
}

-(void)clickCloseBtn{
    [validaAlertView close];
}

#pragma mark - 点击关闭订单
-(void)clickCloseOrder{
    [self.navigationController pushViewController:[CancelHotelOrder initWithOrderId:model.orderid] animated:YES];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 8;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==2) {
        return self.roomDataArr.count;
    }else if (section==5){
        if ([model.invoice isEqualToString:@"0"]) {
            return 0;
        }else{
            return 1;
        }
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 80;
    }else if (indexPath.section==1){
        return 370;
    }else if (indexPath.section==2||indexPath.section==3||indexPath.section==7){
        return 90;
    }else if (indexPath.section==4||indexPath.section==6){
        return 0;
    }else if (indexPath.section==5){
        return 46;
    }
    return 197;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        PSCHotelODFirstCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODFirstCell"];
        if (!cell) {
            cell=[[PSCHotelODFirstCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODFirstCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else if (indexPath.section==1) {
        PSCHotelODSecondCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODSecondCell"];
        if (!cell) {
            cell=[[PSCHotelODSecondCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODSecondCell"];
        }
        [cell.zhiFButton addTarget:self action:@selector(yuEPaySender:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        cell.delegate=self;
        return  cell;
    }else if (indexPath.section==2) {
        PSCHotelODRoomCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODRoomCell"];
        if (!cell) {
            cell=[[PSCHotelODRoomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODRoomCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        PSCHotelODRoomModel *rmodel=self.roomDataArr[indexPath.row];
        [cell reflushData:rmodel];
        cell.roomNumLabel.text=[NSString stringWithFormat:@"房间 %ld",indexPath.row+1];
        return  cell;
    }else if (indexPath.section==3) {
        PSCHotelODContactCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODContactCell"];
        if (!cell) {
            cell=[[PSCHotelODContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODContactCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else if (indexPath.section==5) {
        PSCHotelODShouJuCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODShouJuCell"];
        if (!cell) {
            cell=[[PSCHotelODShouJuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODShouJuCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else if (indexPath.section==7) {
        PSCHotelODIssueCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelODIssueCell"];
        if (!cell) {
            cell=[[PSCHotelODIssueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelODIssueCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushData:model];
        return  cell;
    }else{
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
        if (!cell) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }
}
//点击使用余额
-(void)yuEPaySender:(UIButton *)sender{
    if (sender.selected==NO) {
        [sender setBackgroundImage:[UIImage imageNamed:@"toursuccess_icon"] forState:UIControlStateNormal];
        sender.selected=YES;
        NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
        //        NSMutableAttributedString *string;
                NSString * newTotaMony = [NSString stringWithFormat:@"%.2f",model.payMoney.floatValue - usrMony.floatValue];
        //        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%@元",newTotaMony]];
        //        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
        //        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
        //        totalLabel.attributedText = string;
        moneyLabel.text=[NSString stringWithFormat:@"合计：￥%@",newTotaMony];
    }else{
        [sender setBackgroundImage:[UIImage imageNamed:@"toursuccess_hs"] forState:UIControlStateNormal];
        sender.selected=NO;
        moneyLabel.text=[NSString stringWithFormat:@"合计：￥%@",model.payMoney];
        //        NSMutableAttributedString *string;
        //        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%.2f元",yuEMony.floatValue ]];
        //        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
        //        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
        //        totalLabel.attributedText = string;
    }
}
//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0||section==1?16:54;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 0.0000001f;
    }else if (section==7){
        return 32;
    }else{
        return 8;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0||section==1) {
        return nil;
    }else{
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 54)];
        view.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
        
        UIView *mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 10, SCREENSIZE.width, 44)];
        mainView.backgroundColor=[UIColor whiteColor];
        [view addSubview:mainView];
        
        UIView *lineView=[[UIView alloc] initWithFrame:CGRectMake(0, 12, 3, 20)];
        lineView.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        [mainView addSubview:lineView];
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(14, 0, mainView.width-28, mainView.height)];
        titleLabel.font=kFontSize16;
        titleLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
        NSString *titStr;
        switch (section) {
            case 2:
                titStr=[NSString stringWithFormat:@"预定详情：%@间 共%@晚",model.rooms,model.nights];
                break;
            case 3:
                titStr=[NSString stringWithFormat:@"联系人：%@",model.contactName];
                break;
            case 4:
                titStr=[model.customerRequest isEqualToString:@""]?@"特殊要求：无":[NSString stringWithFormat:@"特殊要求：%@",model.customerRequest];
                break;
            case 5:
                titStr=[model.invoice isEqualToString:@"0"]?@"收据：无":@"收据";
                break;
            case 6:
                titStr=@"取消政策：此订单不能取消获或变更";
                break;
            default:
                titStr=@"订单遇到问题？";
                break;
        }
        titleLabel.text=titStr;
        [mainView addSubview:titleLabel];
        
        return view;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        [self.navigationController pushViewController:[PSCHotelOrderTrack initWithOrderId:self.orderId] animated:YES];
    }
}

#pragma mark - 30分钟后自动取消订单
-(void)isEndTime{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflush_hotellist_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 点击费用明细按钮
-(void)clickFeeDetailBtn{
    [self.navigationController pushViewController:[PSCHotelFeeDetail initWithOrderId:self.orderId] animated:YES];
}

#pragma mark - request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"orderInfo",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                model=[PSCHotelOrderDetailModel new];
                [model jsonToModel:retObj];
                
                if ([model.status isEqualToString:@"0"]) {
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payResult) name:UIApplicationDidBecomeActiveNotification object:nil];
                }
                
                if (model.iosGuestInfo.count!=0) {
                    for (NSDictionary *dic in model.iosGuestInfo) {
                        PSCHotelODRoomModel *rModel=[PSCHotelODRoomModel new];
                        [rModel jsonToModel:dic];
                        [self.roomDataArr addObject:rModel];
                    }
                }
            }
            [self.myTable reloadData];
            [self.view addSubview:self.footView];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [self.myTable reloadData];
        DSLog(@"%@",error);
    }];
}

-(void)getPayParamRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"dida_hotel_pay" forKey:@"mod"];
    [dict setObject:@"ios_pay_hotel" forKey:@"action"];
    [dict setObject:model.orderid forKey:@"orderid"];
    [dict setObject:seleBtn.tag==0?@"8":@"3" forKey:@"type"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"微信 ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                //seleBtn.tag==0 微信   8== 支付宝
                if (seleBtn.tag==0) {
                    id resObj=responseObject[@"retData"];
                    if ([resObj isKindOfClass:[NSDictionary class]]) {
                        FilmWeiXinPayModel *weixinPayModel=[FilmWeiXinPayModel new];
                        [weixinPayModel jsonDataForDictioanry:resObj];
                        [self weixinPayWithModel:weixinPayModel];
                    }
                }else{
                    id resObj=responseObject[@"retData"][@"sdkstr"];
                    if ([resObj isKindOfClass:[NSString class]]) {
                        [self alliPayWithId:resObj];
                    }
                }
                
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//查询订单支付状态
-(void)payResultRequest{
    [AppUtils showProgressInView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"orderStatusCheck" forKey:@"code"];
    [dict setObject:model.orderid forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        //接口调用成功1, 失败0, 当retCode =3时表示订单还在确认中,需要继续等待.
        NSString *dataCode=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if (dataCode.integerValue==0) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else if(dataCode.integerValue==1){
            HoteOreadPayController *vc=[HoteOreadPayController new];
            vc.paymoney=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"paymoney"]];
            vc.payee=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"payee"]];
            vc.product=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"product"]];
            vc.payTime=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"payTime"]];
            vc.payType=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"payType"]];
            vc.orderid=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"orderid"]];
            vc.trade_no=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payInfo"][@"trade_no"]];
            [self.navigationController pushViewController:vc animated:YES];
        }else if(dataCode.integerValue==3){
//            HoteOreadPayController *vc=[HoteOreadPayController new];
//            vc.paymoney=model.payMoney;
//            vc.payee=model.payMoney;
//            vc.product=model.payMoney;
//            vc.payTime=model.payMoney;
//            vc.payType=model.payMoney;
//            vc.orderid=model.orderid;
//            vc.trade_no=model.payMoney;
//            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - 调起微信支付
-(void)weixinPayWithModel:(FilmWeiXinPayModel *)pmodel{
    [WXApi registerApp:pmodel.appid];
    if([WXApi isWXAppInstalled]){
        PayReq* req    = [[PayReq alloc] init];
        req.openID     = pmodel.appid;
        req.partnerId  = pmodel.partnerid;           //商户号
        req.prepayId   = pmodel.prepayid;            //预支付交易会话ID
        req.package    = pmodel.package;             //扩展字段
        req.nonceStr   = pmodel.noncestr;            //随机字符串
        req.timeStamp  = pmodel.timestamp.intValue;  //时间戳（防止重发）
        req.sign       = pmodel.sign;                //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - 调起支付宝支付
-(void)alliPayWithId:(NSString *)strId{
    NSString *appScheme = @"youxiaalipay";
    //支付异步请求并回调
    [[AlipaySDK defaultService] payOrder:strId fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        [self payResultRequest];
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self.view];
}

@end
