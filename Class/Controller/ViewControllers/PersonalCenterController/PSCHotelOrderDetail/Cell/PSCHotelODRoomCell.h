//
//  PSCHotelODRoomCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCHotelODRoomModel.h"

@interface PSCHotelODRoomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *roomNumLabel;

@property (weak, nonatomic) IBOutlet UILabel *adLabel;
@property (weak, nonatomic) IBOutlet UILabel *chLabel;


-(void)reflushData:(PSCHotelODRoomModel *)model;

@end
