//
//  PSCHotelODSecondCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODSecondCell.h"

@implementation PSCHotelODSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, _totalPriceLabel.origin.y+_totalPriceLabel.size.height+0.5, SCREEN_WIDTH, 0.5)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    [self addSubview:lineView];
    
    UILabel * yuELabel = [[UILabel alloc]initWithFrame:CGRectMake(15, _totalPriceLabel.origin.y+_totalPriceLabel.size.height+7, 90, 30)];
    yuELabel.text = @"我的余额：";
    [self addSubview:yuELabel];
    
    NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
    UILabel * tiShiYuELabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-200, _totalPriceLabel.origin.y+_totalPriceLabel.size.height+7, 155, 30)];
    if (usrMony.floatValue==0) {
        tiShiYuELabel.text = @"0元";
    }else{
        tiShiYuELabel.text = [NSString stringWithFormat:@"%.2f元",usrMony.floatValue];
    }
    
    tiShiYuELabel.textAlignment = 2;
    tiShiYuELabel.textColor = [UIColor colorWithHexString:@"ff5742"];
    [self addSubview:tiShiYuELabel];
    
    UIImage * zhiButtonImage = [UIImage imageNamed:@"toursuccess_hs"];
    _zhiFButton = [[UIButton alloc]initWithFrame:CGRectMake(tiShiYuELabel.origin.x+tiShiYuELabel.size.width+10, _totalPriceLabel.origin.y+_totalPriceLabel.size.height+10, 22*W_UNIT,22*W_UNIT)];
    
    [_zhiFButton setBackgroundImage:zhiButtonImage forState:UIControlStateNormal];
    [self addSubview:_zhiFButton];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickFeeDetailBtn:(id)sender {
    [self.delegate clickFeeDetailBtn];
}

-(void)reflushData:(PSCHotelOrderDetailModel *)model{
//    NSData *date=[NSData dataWithContentsOfURL:[NSURL URLWithString:model.hotelLogo]];
//    UIImage *img=[UIImage imageWithData:date];
//    self.hotelLogo.image=img;
    [self.hotelLogo sd_setImageWithURL:[NSURL URLWithString:model.hotelLogo] placeholderImage:[UIImage imageNamed:Image_Default]];
    self.hotelNameLabel.text=model.hotelName;
    self.hotelAddressLabel.text=model.hotelAddress;
    self.homeTypeLabel.text=model.ratePlanName;
    self.homeSumLabel.text=[NSString stringWithFormat:@"%@间 %@晚",model.rooms,model.nights];
    self.dataLabel.text=[NSString stringWithFormat:@"%@ 至 %@",model.checkin,model.checkout];
    if ([model.child isEqualToString:@"0"]) {
        self.ruzhuSumLabel.text=[NSString stringWithFormat:@"%@成人",model.adult];
    }else{
        self.ruzhuSumLabel.text=[NSString stringWithFormat:@"%@成人 %@儿童",model.adult,model.child];
    }
    self.totalPriceLabel.text=[NSString stringWithFormat:@"￥%@",model.payMoney];
}

@end
