//
//  PSCHotelODSecondCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCHotelOrderDetailModel.h"

@protocol PSCHotelODSecondDelegate <NSObject>

-(void)clickFeeDetailBtn;

@end

@interface PSCHotelODSecondCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *hotelLogo;
@property (weak, nonatomic) IBOutlet UILabel *hotelNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *hotelAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UILabel *ruzhuSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic,retain) UIButton * zhiFButton;

@property (weak, nonatomic) id<PSCHotelODSecondDelegate> delegate;

-(void)reflushData:(PSCHotelOrderDetailModel *)model;

@end
