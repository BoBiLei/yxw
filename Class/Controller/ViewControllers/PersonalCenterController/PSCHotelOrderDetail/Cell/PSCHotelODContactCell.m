//
//  PSCHotelODContactCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODContactCell.h"

@implementation PSCHotelODContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(PSCHotelOrderDetailModel *)model{
    self.phoneLabel.text=model.contactPhone;
    self.emailLabel.text=model.contactEmail;
}

@end
