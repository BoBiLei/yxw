//
//  PSCHotelODFirstCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODFirstCell.h"

@implementation PSCHotelODFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(PSCHotelOrderDetailModel *)model{
    /*
     0:订单创建成功,
     1:订单支付成功(确认酒店信息中),
     2:酒店信息确认成功,
     3:订单被取消,
     4:酒店信息确认失败,
     5:客户成功入住,
     6:订单关闭(用户申请关闭或支付超时关闭),
     7:用户申请退款,
     8:退款完成,
     9:支付异常订单
     */
    switch (model.status.integerValue) {
        case 0:
            self.topLabel.text=@"待支付";
            self.detailLabel.text=@"30分钟未支付，系统自动取消订单";
            break;
        case 1:
            self.topLabel.text=@"处理中";
            self.detailLabel.text=@"您的订单已经成功提交，我们将尽快处理";
            break;
        case 2:
            self.topLabel.text=@"预定成功";
            self.detailLabel.text=@"预定成功，您可安心入住";
            break;
        case 3:
            self.topLabel.text=@"订单被取消";
            self.detailLabel.text=@"订单被取消";
            break;
        case 4:
            self.topLabel.text=@"酒店信息确认失败";
            self.detailLabel.text=@"酒店信息确认失败";
            break;
        case 5:
            self.topLabel.text=@"已完成";
            self.detailLabel.text=@"感谢您的入住";
            break;
        case 6:
            self.topLabel.text=@"订单关闭";
            self.detailLabel.text=@"(用户申请关闭或支付超时关闭)";
            break;
        case 7:
            self.topLabel.text=@"用户申请退款";
            self.detailLabel.text=@"用户申请退款";
            break;
        case 8:
            self.topLabel.text=@"退款完成";
            self.detailLabel.text=@"退款完成";
            break;
        case 9:
            self.topLabel.text=@"支付异常订单";
            self.detailLabel.text=@"支付异常订单";
            break;
        default:
            break;
    }
}

@end
