//
//  PSCHotelODFirstCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCHotelOrderDetailModel.h"

@interface PSCHotelODFirstCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;


-(void)reflushData:(PSCHotelOrderDetailModel *)model;

@end
