//
//  PSCHotelODShouJuCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//  收据

#import <UIKit/UIKit.h>
#import "PSCHotelOrderDetailModel.h"

@interface PSCHotelODShouJuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

-(void)reflushData:(PSCHotelOrderDetailModel *)model;

@end
