//
//  PSCHotelODShouJuCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODShouJuCell.h"

@implementation PSCHotelODShouJuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(PSCHotelOrderDetailModel *)model{
    self.titleLabel.text=model.invoiceHeader;
}

@end
