//
//  PSCHotelODRoomCell.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelODRoomCell.h"

@implementation PSCHotelODRoomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(PSCHotelODRoomModel *)model{
    self.adLabel.text=[NSString stringWithFormat:@"%@成人    %@",model.adultNum,model.adultName];
    self.chLabel.text=[NSString stringWithFormat:@"%@儿童    %@",model.childNum,model.childName];
}

@end
