//
//  PSCHotelODIssueCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//  订单问题

#import <UIKit/UIKit.h>
#import "PSCHotelOrderDetailModel.h"

@interface PSCHotelODIssueCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;

-(void)reflushData:(PSCHotelOrderDetailModel *)model;

@end
