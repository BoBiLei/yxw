//
//  PSCHotelODContactCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//  联系人电话

#import <UIKit/UIKit.h>
#import "PSCHotelOrderDetailModel.h"

@interface PSCHotelODContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

-(void)reflushData:(PSCHotelOrderDetailModel *)model;

@end
