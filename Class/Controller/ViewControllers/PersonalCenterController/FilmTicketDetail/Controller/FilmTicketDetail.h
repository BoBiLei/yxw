//
//  FilmTicketDetail.h
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmTicketDetail : UIViewController

@property (nonatomic, copy) NSString *orderId;

@end
