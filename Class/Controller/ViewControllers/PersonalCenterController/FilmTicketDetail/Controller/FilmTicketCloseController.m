//
//  FilmTicketCloseController.m
//  youxia
//
//  Created by mac on 2016/12/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmTicketCloseController.h"
#import "FilmTicketCloseHeadCell.h"
#import "FilmTicketCloseBottomCell.h"
#import "FilmWaitTourModel.h"
#import "FilmListController.h"
#import "PickingSeatController.h"

@interface FilmTicketCloseController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation FilmTicketCloseController{
    FilmWaitTourModel *dataModel;
    UITableView *myTable;
}

-(void)viewDidAppear:(BOOL)animated{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavigateView];
    
    [self setUpTable];
    
    [self dataRequest];
}

-(void)addNavigateView{
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(16, 20, 64, 44);
    backBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [backBtn setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2, 44)];
    title.center=CGPointMake(SCREENSIZE.width/2, 42);
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.textAlignment=NSTextAlignmentCenter;
    title.text=@"电影票详情";
    [self.view addSubview:title];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"dcdcdc"] height:0.9f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

#pragma mark - Nav 点击返回按钮
-(void)turnBack{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[FilmListController class]]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else if ([controller isKindOfClass:[PickingSeatController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(void)setUpTable{
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.hidden=YES;
    [myTable registerNib:[UINib nibWithNibName:@"FilmTicketCloseBottomCell" bundle:nil] forCellReuseIdentifier:@"FilmTicketCloseBottomCell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.section==0?76:220;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000000001f;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        FilmTicketCloseHeadCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmTicketCloseHeadCell"];
        if (!cell) {
            cell=[[FilmTicketCloseHeadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmTicketCloseHeadCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }else{
        FilmTicketCloseBottomCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmTicketCloseBottomCell"];
        if (!cell) {
            cell=[[FilmTicketCloseBottomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmTicketCloseBottomCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataModel];
        return  cell;
    }
    
}
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
    
#pragma mark - request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"get_my_order_info",
                         @"orderid":_orderId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        //DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                dataModel=[FilmWaitTourModel new];
                [dataModel jsonDataForDictionary:retObj];
            }
            myTable.hidden=NO;
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
@end
