//
//  FilmTicketDetail.m
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmTicketDetail.h"
#import "FilmWaitTourModel.h"


@interface FilmTicketDetail ()

@end

@implementation FilmTicketDetail{
    FilmWaitTourModel *dataModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"电影票详情";
    [self.view addSubview:navBar];
    
    [self dataRequest];
}

-(void)setUI{
    UITableView *table=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    [self.view addSubview:table];
    
    table.tableHeaderView=[self addHeaderView];
}

-(UIView *)addHeaderView{
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    
#pragma mark 取票号
    CGFloat ticketNumViewHeight=0;
    NSString *tnNumStr=dataModel.codeDic;
    ticketNumViewHeight=150;
    
    
    UIView *ticketNumView=[[UIView alloc] initWithFrame:CGRectMake(12, 14, SCREENSIZE.width-24, ticketNumViewHeight)];
    ticketNumView.layer.cornerRadius=2;
    ticketNumView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [view addSubview:ticketNumView];
    
    //状态
    UIView *statusView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ticketNumView.width, 50)];
    
    [ticketNumView addSubview:statusView];
    
    UILabel *statusName=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, statusView.width, statusView.height)];
    
    statusName.font=[UIFont boldSystemFontOfSize:18];
    statusName.textAlignment=NSTextAlignmentCenter;
    NSString *statusStr;
    switch (dataModel.status.intValue) {
            //0未支付 1待出票 2待放映 3已放映
        case 0:
            statusStr=@"未支付";
            statusName.textColor=[UIColor colorWithHexString:@"ff9900"];
            statusView.backgroundColor=[UIColor colorWithHexString:@"fde384"];
            break;
        case 1:
            statusStr=@"待出票";
            statusName.textColor=[UIColor colorWithHexString:@"ff9900"];
            statusView.backgroundColor=[UIColor colorWithHexString:@"fde384"];
            break;
        case 2:
            statusStr=@"待放映";
            statusName.textColor=[UIColor colorWithHexString:@"ff9900"];
            statusView.backgroundColor=[UIColor colorWithHexString:@"fde384"];
            break;
        case 3:
            statusStr=@"已放映";
            statusName.textColor=[UIColor colorWithHexString:@"949494"];
            statusView.backgroundColor=[UIColor colorWithHexString:@"d7d7d7"];
            break;
        default:
            break;
    }
    statusName.text=statusStr;
    [statusView addSubview:statusName];

    //取票号
    UILabel *tnNum=[[UILabel alloc] initWithFrame:CGRectMake(12, statusView.height, ticketNumView.width-24, ticketNumView.height-statusView.height)];
    tnNum.textColor=[UIColor colorWithHexString:@"515151"];
    tnNum.font=[UIFont systemFontOfSize:14];
    NSString *tnNumTt=tnNumStr;
    NSMutableAttributedString *tnNumString = [[NSMutableAttributedString alloc] initWithString:tnNumTt];
    NSMutableParagraphStyle *tnNumStyle = [[NSMutableParagraphStyle alloc] init];
    
    [tnNumStyle setLineSpacing:6];//调整行间距
    
    [tnNumString addAttribute:NSParagraphStyleAttributeName value:tnNumStyle range:NSMakeRange(0, [tnNumTt length])];
    tnNum.attributedText = tnNumString;
    tnNum.textAlignment=NSTextAlignmentLeft;
    tnNum.numberOfLines=0;
    [ticketNumView addSubview:tnNum];
    
#pragma mark 电影名称
    UILabel *filmName=[[UILabel alloc] initWithFrame:CGRectMake(14, ticketNumView.origin.y+ticketNumView.height+14, SCREENSIZE.width-28, 26)];
    filmName.textColor=[UIColor colorWithHexString:@"000000"];
    filmName.font=[UIFont boldSystemFontOfSize:16];
    filmName.text=dataModel.movie_data;
    [view addSubview:filmName];
    
#pragma mark 电影院名称
    UILabel *filmAddress=[[UILabel alloc] initWithFrame:CGRectMake(14, filmName.origin.y+filmName.height+3, SCREENSIZE.width-28, 19)];
    filmAddress.textColor=[UIColor colorWithHexString:@"949494"];
    filmAddress.font=[UIFont systemFontOfSize:13];
    filmAddress.text=dataModel.cinemaName;
    [view addSubview:filmAddress];
    
    //
    UILabel *viewTime=[[UILabel alloc] initWithFrame:CGRectMake(14, filmAddress.origin.y+filmAddress.height+3, SCREENSIZE.width-28, 19)];
    viewTime.textColor=[UIColor colorWithHexString:@"949494"];
    viewTime.font=[UIFont systemFontOfSize:13];
    viewTime.text=dataModel.playtime;
    [view addSubview:viewTime];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, viewTime.origin.y+viewTime.height+10) endPoint:CGPointMake(SCREENSIZE.width-14, viewTime.origin.y+viewTime.height+10)];
    
#pragma mark 厅号
    UILabel *tingNum=[[UILabel alloc] initWithFrame:CGRectMake(14, viewTime.origin.y+viewTime.height+22, SCREENSIZE.width-28, 19)];
    tingNum.textColor=[UIColor colorWithHexString:@"949494"];
    tingNum.font=[UIFont systemFontOfSize:13];
    tingNum.text=dataModel.hallName;
    [view addSubview:tingNum];
    
    UILabel *paiNum=[[UILabel alloc] initWithFrame:CGRectMake(14, tingNum.origin.y+tingNum.height+4, SCREENSIZE.width-28, 19)];
    paiNum.textColor=[UIColor colorWithHexString:@"949494"];
    paiNum.font=[UIFont systemFontOfSize:13];
    NSString *seatStr=@"";
    for (NSString *str in dataModel.position) {
        NSString *itemStr=@"";
        NSArray *picarry=[str componentsSeparatedByString:@":"];
        for (int i=0; i<picarry.count; i++) {
            NSString *str=picarry[i];
            itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
        }
        seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
    }
    paiNum.text=seatStr;
    [view addSubview:paiNum];
    
#pragma mark 票价
    UILabel *leftPrice=[[UILabel alloc] initWithFrame:CGRectMake(14, paiNum.origin.y+paiNum.height+4, SCREENSIZE.width/2, 19)];
    leftPrice.textColor=[UIColor colorWithHexString:@"949494"];
    leftPrice.font=[UIFont systemFontOfSize:13];
    leftPrice.text=[NSString stringWithFormat:@"总价 ￥%@  优惠 -￥%@",dataModel.movie_amount,dataModel.credit_price];
    [view addSubview:leftPrice];
    
    UILabel *rightPrice=[[UILabel alloc] initWithFrame:CGRectMake(SCREENSIZE.width/2-14, leftPrice.origin.y, leftPrice.width, leftPrice.height)];
    rightPrice.textColor=[UIColor colorWithHexString:@"fb8023"];
    rightPrice.font=[UIFont systemFontOfSize:16];
    rightPrice.text=[NSString stringWithFormat:@"实付 ￥%@",dataModel.paymoney];
    rightPrice.textAlignment=NSTextAlignmentRight;
    [view addSubview:rightPrice];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, leftPrice.origin.y+leftPrice.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, leftPrice.origin.y+leftPrice.height+14)];
    
    UILabel *qpAdrTip=[[UILabel alloc] initWithFrame:CGRectMake(14, leftPrice.origin.y+leftPrice.height+25, SCREENSIZE.width-28, 19)];
    qpAdrTip.textColor=[UIColor colorWithHexString:@"949494"];
    qpAdrTip.font=[UIFont systemFontOfSize:13];
    qpAdrTip.text=@"取票地址 : 影院售票窗口兑换";
    [view addSubview:qpAdrTip];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, qpAdrTip.origin.y+qpAdrTip.height+12) endPoint:CGPointMake(SCREENSIZE.width-14, qpAdrTip.origin.y+qpAdrTip.height+12)];
    
#pragma mark 订单号
    UILabel *orderNum=[[UILabel alloc] initWithFrame:CGRectMake(14, qpAdrTip.origin.y+qpAdrTip.height+25, SCREENSIZE.width-28, 19)];
    orderNum.textColor=[UIColor colorWithHexString:@"949494"];
    orderNum.font=[UIFont boldSystemFontOfSize:13];
    orderNum.text=[NSString stringWithFormat:@"订单号 : %@",dataModel.orderid];
    [view addSubview:orderNum];
    
    UILabel *buyTime=[[UILabel alloc] initWithFrame:CGRectMake(14, orderNum.origin.y+orderNum.height+4, SCREENSIZE.width-28, 19)];
    buyTime.textColor=[UIColor colorWithHexString:@"949494"];
    buyTime.font=[UIFont systemFontOfSize:13];
    buyTime.text=[NSString stringWithFormat:@"购票时间 : %@",dataModel.buytime];
    [view addSubview:buyTime];
    
    UILabel *phone=[[UILabel alloc] initWithFrame:CGRectMake(14, buyTime.origin.y+buyTime.height+4, SCREENSIZE.width-28, 19)];
    phone.textColor=[UIColor colorWithHexString:@"949494"];
    phone.font=[UIFont systemFontOfSize:13];
    phone.text=[NSString stringWithFormat:@"手机号 : %@",dataModel.phone];
    [view addSubview:phone];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, phone.origin.y+phone.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, phone.origin.y+phone.height+14)];
    
    UILabel *yyName=[[UILabel alloc] initWithFrame:CGRectMake(14, phone.origin.y+phone.height+25, SCREENSIZE.width-86, 26)];
    yyName.textColor=[UIColor colorWithHexString:@"000000"];
    yyName.font=[UIFont systemFontOfSize:14];
    yyName.text=dataModel.cinemaName;
    [view addSubview:yyName];
    
#pragma mark 影院地址
    UILabel *yyAddress=[[UILabel alloc] init];
    yyAddress.textColor=[UIColor colorWithHexString:@"949494"];
    yyAddress.font=[UIFont systemFontOfSize:13];
    yyAddress.numberOfLines=0;
    CGSize yyadSize=[AppUtils getStringSize:dataModel.address withFont:13];
    if (yyadSize.width>SCREENSIZE.width-94) {
        yyAddress.frame=CGRectMake(14, yyName.origin.y+yyName.height+3, SCREENSIZE.width-94, 34);
    }else{
        yyAddress.frame=CGRectMake(14, yyName.origin.y+yyName.height+3, SCREENSIZE.width-94, 19);
    }
    NSString *labelTtt=dataModel.address;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:2];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    yyAddress.attributedText = attributedString;
    [view addSubview:yyAddress];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(SCREENSIZE.width-74, yyName.origin.y+4) endPoint:CGPointMake(SCREENSIZE.width-74, yyAddress.origin.y+yyAddress.height)];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, yyAddress.origin.y+yyAddress.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, yyAddress.origin.y+yyAddress.height+14)];
    
#pragma mark callBtn
    UIButton *callBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    callBtn.frame=CGRectMake(SCREENSIZE.width-88, phone.origin.y+phone.height+14, 74, 74);
    [callBtn setImage:[UIImage imageNamed:@"yydetail_phone"] forState:UIControlStateNormal];
    [callBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 21, 0, 0)];
    [view addSubview:callBtn];
    [callBtn addTarget:self action:@selector(clickCall) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *kefuTip=[[UILabel alloc] initWithFrame:CGRectMake(14, yyAddress.origin.y+yyAddress.height+38, SCREENSIZE.width-28, 19)];
    kefuTip.textColor=[UIColor colorWithHexString:@"949494"];
    kefuTip.font=[UIFont systemFontOfSize:13];
    kefuTip.text=@"订单问题请联系客服";
    kefuTip.textAlignment=NSTextAlignmentCenter;
    [view addSubview:kefuTip];
    
    UILabel *kefuPhone=[[UILabel alloc] initWithFrame:CGRectMake(14, kefuTip.origin.y+kefuTip.height, SCREENSIZE.width-28, 26)];
    kefuPhone.textColor=[UIColor colorWithHexString:@"000000"];
    kefuPhone.font=[UIFont systemFontOfSize:13];
//    kefuPhone.text=dataModel.tel;
    //SERVICEPHONE
    kefuPhone.text=@"400-1808-400";
    kefuPhone.textAlignment=NSTextAlignmentCenter;
    kefuPhone.userInteractionEnabled=YES;
    [view addSubview:kefuPhone];
    UITapGestureRecognizer *kefuTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapKefuCall)];
    [kefuPhone addGestureRecognizer:kefuTap];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, kefuPhone.origin.y+kefuPhone.height+20) endPoint:CGPointMake(SCREENSIZE.width, kefuPhone.origin.y+kefuPhone.height+20)];
    
    return view;
}

-(void)tapKefuCall{
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://400-1808-400"];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

-(void)clickCall{
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",dataModel.tel];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"get_my_order_info",
                         @"orderid":_orderId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                dataModel=[FilmWaitTourModel new];
                [dataModel jsonDataForDictionary:retObj];
                [self setUI];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
