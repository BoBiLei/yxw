//
//  FilmTicketCloseController.h
//  youxia
//
//  Created by mac on 2016/12/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmTicketCloseController : UIViewController

@property (nonatomic, copy) NSString *orderId;
    
@end
