//
//  FilmTicketCloseHeadCell.m
//  youxia
//
//  Created by mac on 2016/12/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmTicketCloseHeadCell.h"

@implementation FilmTicketCloseHeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *filmName=[[UILabel alloc] initWithFrame:CGRectMake(14, 14, SCREENSIZE.width-28, 26)];
        filmName.textColor=[UIColor colorWithHexString:@"000000"];
        filmName.font=[UIFont systemFontOfSize:16];
        filmName.text=@"订单关闭";
        [self addSubview:filmName];
        
        UILabel *filmAddress=[[UILabel alloc] initWithFrame:CGRectMake(14, filmName.origin.y+filmName.height+3, SCREENSIZE.width-28, 19)];
        filmAddress.textColor=[UIColor colorWithHexString:@"949494"];
        filmAddress.font=[UIFont systemFontOfSize:13];
        filmAddress.text=@"实付金额/优惠券会在1-2个工作日返回至您的账户";
        [self addSubview:filmAddress];
    }
    return self;
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
