//
//  FilmTicketCloseBottomCell.m
//  youxia
//
//  Created by mac on 2016/12/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmTicketCloseBottomCell.h"

@implementation FilmTicketCloseBottomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(FilmWaitTourModel *)model{
    self.MovieLabel.text=model.movie_data;
    self.cinemaLabel.text=model.cinemaName;
    self.yhPriceLabel.text=[NSString stringWithFormat:@"票价 ￥%@  优惠 -￥%@",model.movie_price,model.credit_price];
    self.payLabel.text=[NSString stringWithFormat:@"实付 ￥%@",model.paymoney];
    self.orderLabel.text=[NSString stringWithFormat:@"订单号 : %@",model.orderid];
    self.buyTimeLabel.text=[NSString stringWithFormat:@"购票时间 : %@",model.buytime];
    self.phoneLabel.text=[NSString stringWithFormat:@"手机号 : %@",model.phone];
}
    
@end
