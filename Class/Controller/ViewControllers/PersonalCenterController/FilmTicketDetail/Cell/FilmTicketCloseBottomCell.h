//
//  FilmTicketCloseBottomCell.h
//  youxia
//
//  Created by mac on 2016/12/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmWaitTourModel.h"

@interface FilmTicketCloseBottomCell : UITableViewCell

    @property (weak, nonatomic) IBOutlet UILabel *MovieLabel;
    @property (weak, nonatomic) IBOutlet UILabel *cinemaLabel;
    @property (weak, nonatomic) IBOutlet UILabel *yhPriceLabel;
    @property (weak, nonatomic) IBOutlet UILabel *payLabel;
    @property (weak, nonatomic) IBOutlet UILabel *orderLabel;
    @property (weak, nonatomic) IBOutlet UILabel *buyTimeLabel;
    @property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
    
    
-(void)reflushDataForModel:(FilmWaitTourModel *)model;
    
@end
