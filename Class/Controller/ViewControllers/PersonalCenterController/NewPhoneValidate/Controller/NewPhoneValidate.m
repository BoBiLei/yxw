//
//  NewPhoneValidate.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewPhoneValidate.h"
#import "SecondCell.h"

@interface NewPhoneValidate ()<UITableViewDataSource,UITableViewDelegate,ValidateXYKDelegate>

@end

@implementation NewPhoneValidate{
    NSString *dxTfStr;
    NSString *volatPhone;
    
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    NSString *code_type;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpUI];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"手机号码验证" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    dxTfStr=@"";
    //
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.scrollEnabled=NO;
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"SecondCell" bundle:nil] forCellReuseIdentifier:@"SecondCell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SecondCell *cell=[[SecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SecondCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (_phoneArr.count!=0) {
        [cell reflushDataForArray:self.phoneArr];
    }
    return cell.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SecondCell *cell=[[SecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SecondCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.delegate=self;
    if (_phoneArr.count!=0) {
        volatPhone=self.phoneArr[0];
        [cell reflushDataForArray:self.phoneArr];
        cell.SecondPictureChangeBlock=^(NSString *text){
            dxTfStr=text;
        };
    }
    return  cell;
}

#pragma mark - Cell Delegate
/**验证电话、服务密码*/
-(void)submitPhoneAndPass:(NSString *)phone password:(NSString *)passWord{
    [self requestPhoneAndPass:phone passWord:passWord];
}

/**验证电话、服务密码、图片验证码*/
-(void)submitPhoneAndPassAndPicture:(NSString *)phone password:(NSString *)passWord picture:(NSString *)picture{
    [self requestPhoneAndPassAndPicture:phone passWord:passWord picture:picture];
}

/**验证电话、短信*/
-(void)submitPhoneAndDuanxin:(NSString *)phone duanxin:(NSString *)duanxin needImage:(NSString *)needImg capcha:(NSString *)capcha name:(NSString *)name cartNo:(NSString *)cartNo{
    [self requestPhoneAndDuanXin:phone duanxin:duanxin needImage:(NSString *)needImg capcha:(NSString *)capcha name:(NSString *)name cartNo:(NSString *)cartNo];
}

//重新获取图片验证码
-(void)reGetImageValide:(NSString *)phone{
    [self reGetPictureCodeRequest:phone];
}

//重新获取短信验证码
-(void)reGetValideCode:(NSString *)phone{
    [self reGetValideCodeRequest:phone];
}

#pragma mark - 验证手机号、服务密码 Request

-(void)requestPhoneAndPass:(NSString *)phone passWord:(NSString *)passWord{
    [AppUtils showProgressMessage:@"正在验证中，请耐心等待！" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_sm" forKey:@"mod"];
    [dict setObject:@"save_phone_number_ios_new " forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:passWord forKey:@"phone_pdw"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndPass:phone passWord:passWord];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //error = 0 服务码验证成功;
                //error = 1 不成功/或进入判断is_dx=1
                NSString *errorCode=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"error"]];
                if ([errorCode isEqualToString:@"0"]) {
                    
                    //success = 1 全部验证成功;
                    //success = 0 需要下一步
                    NSString *succStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"success"]];
                    if ([succStr isEqualToString:@"0"]) {
                        NSString *codeType=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"code_type"]];
                        code_type=codeType;
                        /*
                         codeType = smsCode 需要短信验证
                         codeType = picCode 需要图片验证
                         */
                        
                        //100 需要图片验证码
                        //200 短信验证码
                        if ([codeType isEqualToString:@"smsCode"]) {
                            _phoneArr=@[phone,@"200",passWord,responseObject[@"retData"][@"img"],@"",@""];
                            [myTable reloadData];
                        }else if([codeType isEqualToString:@"picCode"]){
                            _phoneArr=@[phone,@"100",passWord,responseObject[@"retData"][@"img"],@"",@""];
                            [myTable reloadData];
                        }else{
                            /*
                             目前联通的都不需要图片
                             只有移动的才需要
                             */
                        }
                    }else{
                        [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                        _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                        [myTable reloadData];
                    }
                }else{
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark 验证手机号、服务密码 图片/短信验证码 Request

-(void)requestPhoneAndPassAndPicture:(NSString *)phone passWord:(NSString *)passWord picture:(NSString *)picture{
    
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_sm" forKey:@"mod"];
    [dict setObject:code_type forKey:@"code_type"];
    [dict setObject:@"save_phone_number_pwd_ios_new" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:passWord forKey:@"phone_pdw"];
    [dict setObject:picture forKey:@"phone_code"];
    [dict setObject:@"1" forKey:@"is_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        /*
         errCode = 0;
         retData =     {
         error = 0;
         imgUrl = "http://60.205.148.129/static/1489029090.27_yidong:15077683853.png";
         "is_code" = 0;
         msg = "\U8bf7\U6c42\U6210\U529f\Uff01ok";
         needImage = 1;
         success = 0;
         };
         retMsg = success;
         */
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndPassAndPicture:phone passWord:passWord picture:picture];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                //success = 1 全部验证成功;
                //success = 0 需要下一步短信验证
                NSString *succStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"success"]];
                if ([succStr isEqualToString:@"0"]) {
                    NSString *needImg=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"needImage"]];
                    //needImg = 1 需要图片验证码;
                    //needImg = 0 不需要图片验证码
                    
                    NSString *isDX=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"is_dx"]];
                    
                    if ([needImg isEqualToString:@"1"]) {
                        NSString *resImg=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"imgUrl"]];
                        _phoneArr=@[phone,@"999",@"1",resImg,[isDX isEqualToString:@"1"]?@"1":@"0",@"60"];
                    }else{
                        _phoneArr=@[phone,@"999",@"0",@"",[isDX isEqualToString:@"1"]?@"1":@"0",@"60"];
                    }
                    [myTable reloadData];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                    volatPhone=phone;
                    _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                    [myTable reloadData];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark 验证手机号、短信

-(void)requestPhoneAndDuanXin:(NSString *)phone duanxin:(NSString *)duanxin needImage:(NSString *)needImg capcha:(NSString *)capcha name:(NSString *)name cartNo:(NSString *)cartNo{
    //ttp://www.youxia.com/mgo/index.php?mod=me_t&code=save_phone_number_smscode_ios&is_iso=1&phone_number=13008828755&phone_sms_code=168200
    [AppUtils showProgressMessage:@"正在验证中，请耐心等待！" inView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_sm" forKey:@"mod"];
    [dict setObject:code_type forKey:@"code_type"];
    [dict setObject:@"save_phone_number_smscode_ios_new" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:duanxin forKey:@"phone_sms_code"];
    [dict setObject:needImg forKey:@"needImage"];
    [dict setObject:capcha forKey:@"capcha"];
    [dict setObject:name forKey:@"name"];
    [dict setObject:cartNo forKey:@"cert_no"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestPhoneAndDuanXin:phone duanxin:duanxin needImage:needImg capcha:capcha name:name cartNo:cartNo];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"error"]];
                if ([codeStr isEqualToString:@"0"]) {
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                    volatPhone=phone;
                    _phoneArr=@[phone,@"1",@"",@"",@"",@""];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAllStatusNotification" object:_phoneArr];
                    [myTable reloadData];
                }else{
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark 重新获取图片验证码

-(void)reGetPictureCodeRequest:(NSString *)phone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_sm" forKey:@"mod"];
    [dict setObject:@"get_imgUrl_capcan" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:@"1" forKey:@"is_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@=====---",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *resImg=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"imgUrl"]];
            _phoneArr=@[phone,@"999",@"1",resImg,dxTfStr,@"0"];
            [myTable reloadData];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

#pragma mark 重新获取短信验证码 Request

-(void)reGetValideCodeRequest:(NSString *)phone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_sm" forKey:@"mod"];
    [dict setObject:@"get_phone_number_smscode_ios_new" forKey:@"code"];
    [dict setObject:phone forKey:@"phone_number"];
    [dict setObject:@"1" forKey:@"is_code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@=====---",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

@end
