//
//  ModifyPassWordCell.m
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ModifyPassWordCell.h"

@implementation ModifyPassWordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_myTextField addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - textfield
- (void)phoneDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.TextFieldBlock) {
        self.TextFieldBlock(field.text);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
