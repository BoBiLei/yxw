//
//  ModifyPassWordCell.h
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModifyPassWordCell : UITableViewCell

@property (nonatomic, strong) void (^TextFieldBlock)(NSString * textFieldStr);

@property (weak, nonatomic) IBOutlet UITextField *myTextField;

@end
