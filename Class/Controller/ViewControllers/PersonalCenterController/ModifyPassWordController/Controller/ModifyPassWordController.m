//
//  ModifyPassWordController.m
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ModifyPassWordController.h"
#import "ModifyPassWordCell.h"
#import "BindingPhoneController.h"

#define LabelFont 15
@interface ModifyPassWordController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ModifyPassWordController{
    UIButton *submitBtn;
    
    UITableView *myTable;
    
    
    //
    NSString *orgPwdStr;
    NSString *newPwdStr;
    NSString *confirmPwdStr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
        [TalkingData trackPageBegin:@"设置密码页"];
    }else{
        [TalkingData trackPageBegin:@"修改密码页"];
    }
    
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
        [TalkingData trackPageBegin:@"设置密码页"];
    }else{
        [TalkingData trackPageBegin:@"修改密码页"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    orgPwdStr=@"";
    newPwdStr=@"";
    confirmPwdStr=@"";
    
    [self setUpUI];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    //Is_NewPwd=0 未修改过的  =1修改的
    if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
        titleLabel.text=@"设置密码密码";
    }else{
        titleLabel.text=@"修改密码";
    }
    
    [self.view addSubview:titleLabel];
    
    submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-78, 22, 64, 44)];
    submitBtn.titleLabel.font=kFontSize17;
    [submitBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];
    [submitBtn setTitle:@"保存" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitBtn.titleLabel.textAlignment=NSTextAlignmentRight;
    [submitBtn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - setup UI
-(void)setUpUI{

    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"ModifyPassWordCell" bundle:nil] forCellReuseIdentifier:@"ModifyPassWordCell"];
    [self.view addSubview:myTable];
    
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Login_Type=1--手机登陆  2--第三方登陆
    if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
        return 3;
    }else{
        //Is_NewPwd=0 未修改过的  =1修改的
        if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
            return 2;
        }else{
            return 3;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ModifyPassWordCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ModifyPassWordCell"];
    if (cell==nil) {
        cell=[[ModifyPassWordCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ModifyPassWordCell"];
    }
    
    //Login_Type=1--手机登陆  2--第三方登陆
    if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
        switch (indexPath.row) {
            case 0:
            {
                cell.myTextField.placeholder=@"原始密码";
                cell.TextFieldBlock = ^(NSString *textStr){
                    orgPwdStr=textStr;
                };
            }
                break;
            case 1:
            {
                cell.myTextField.placeholder=@"新密码";
                cell.TextFieldBlock = ^(NSString *textStr){
                    newPwdStr=textStr;
                };
            }
                break;
            case 2:{
                cell.myTextField.placeholder=@"再次输入新密码";
                cell.TextFieldBlock = ^(NSString *textStr){
                    confirmPwdStr=textStr;
                };
            }
                break;
                
            default:
                break;
        }
    }else{
        //Is_NewPwd=0 未修改过的  =1修改的
        if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
            //
            switch (indexPath.row) {
                case 0:
                {
                    cell.myTextField.placeholder=@"新密码";
                    cell.TextFieldBlock = ^(NSString *textStr){
                        newPwdStr=textStr;
                    };
                }
                    break;
                case 1:{
                    cell.myTextField.placeholder=@"再次输入新密码";
                    cell.TextFieldBlock = ^(NSString *textStr){
                        confirmPwdStr=textStr;
                    };
                }
                    break;
                    
                default:
                    break;
            }
        }else{
            switch (indexPath.row) {
                case 0:
                {
                    cell.myTextField.placeholder=@"原始密码";
                    cell.TextFieldBlock = ^(NSString *textStr){
                        orgPwdStr=textStr;
                    };
                }
                    break;
                case 1:
                {
                    cell.myTextField.placeholder=@"新密码";
                    cell.TextFieldBlock = ^(NSString *textStr){
                        newPwdStr=textStr;
                    };
                }
                    break;
                case 2:{
                    cell.myTextField.placeholder=@"再次输入新密码";
                    cell.TextFieldBlock = ^(NSString *textStr){
                        confirmPwdStr=textStr;
                    };
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击保存按钮
-(void)clickBtn{
    
    [AppUtils closeKeyboard];
    
    //Login_Type=1--手机登陆  2--第三方登陆
    if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
        if ([orgPwdStr isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入原始密码" inView:self.view];
        }else if ([newPwdStr isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入新密码" inView:self.view];
        }else if ([confirmPwdStr isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请再次输入新密码" inView:self.view];
        }else{
            if ([AppUtils checkPassworkLength:newPwdStr]) {
                if (![newPwdStr isEqualToString:confirmPwdStr]) {
                    [AppUtils showSuccessMessage:@"两次密码输入不一致" inView:self.view];
                }else{
                    NSString *str=[AppUtils judgePasswordStrength:newPwdStr];
                    if ([str isEqualToString:@"0"]) {
                        [AppUtils showSuccessMessage:@"您输入的密码太过简单" inView:self.view];
                    }else{
                        [self requestHttpsForOrgPwd:orgPwdStr newPwd:newPwdStr confirmPwd:confirmPwdStr];
                    }
                }
            }else{
                [AppUtils showSuccessMessage:@"至少输入6位密码" inView:self.view];
            }
        }
    }else{
        //Is_NewPwd=0 未修改过的  =1修改的
        if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"1"]) {
            if ([orgPwdStr isEqualToString:@""]) {
                [AppUtils showSuccessMessage:@"请输入原始密码" inView:self.view];
            }else if ([newPwdStr isEqualToString:@""]){
                [AppUtils showSuccessMessage:@"请输入新密码" inView:self.view];
            }else if ([confirmPwdStr isEqualToString:@""]){
                [AppUtils showSuccessMessage:@"请再次输入新密码" inView:self.view];
            }else{
                if ([AppUtils checkPassworkLength:newPwdStr]) {
                    if (![newPwdStr isEqualToString:confirmPwdStr]) {
                        [AppUtils showSuccessMessage:@"两次密码输入不一致" inView:self.view];
                    }else{
                        NSString *str=[AppUtils judgePasswordStrength:newPwdStr];
                        if ([str isEqualToString:@"0"]) {
                            [AppUtils showSuccessMessage:@"您输入的密码太过简单" inView:self.view];
                        }else{
                            [self requestHttpsForOrgPwd:orgPwdStr newPwd:newPwdStr confirmPwd:confirmPwdStr];
                        }
                    }
                }else{
                    [AppUtils showSuccessMessage:@"至少输入6位密码" inView:self.view];
                }
            }
        }else{
            if ([newPwdStr isEqualToString:@""]){
                [AppUtils showSuccessMessage:@"请输入新密码" inView:self.view];
            }else{
                if ([confirmPwdStr isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:@"请再次输入新密码" inView:self.view];
                }else{
                    if ([AppUtils checkPassworkLength:newPwdStr]) {
                        if (![newPwdStr isEqualToString:confirmPwdStr]) {
                            [AppUtils showSuccessMessage:@"两次密码输入不一致" inView:self.view];
                        }else{
                            NSString *str=[AppUtils judgePasswordStrength:newPwdStr];
                            if ([str isEqualToString:@"0"]) {
                                [AppUtils showSuccessMessage:@"您输入的密码太过简单" inView:self.view];
                            }else{
                                [self settingPassRequestForNewPwd:newPwdStr confirmPwd:confirmPwdStr];
                            }
                        }
                    }else{
                        [AppUtils showSuccessMessage:@"至少输入6位密码" inView:self.view];
                    }
                }
            }
        }
    }
}

#pragma mark - 修改密码请求
//修改密码
-(void)requestHttpsForOrgPwd:(NSString *)orgPwd newPwd:(NSString *)newPwd confirmPwd:(NSString *)confirmPwd{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:@"do_pw_iso" forKey:@"code"];
    [dict setObject:orgPwd forKey:@"oldpw"];
    [dict setObject:newPwd forKey:@"newpw"];
    [dict setObject:confirmPwd forKey:@"okpw"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
//        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
//            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
//        }
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrgPwd:orgPwd newPwd:newPwd confirmPwd:confirmPwd];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:@"修改密码成功" inView:self.navigationController.view];
                /*******
                 1、把修改密码覆盖到保存密码，以方便自动登录
                 2、pop到上级Controller
                 *******/
                [AppUtils saveValue:newPwd forKey:User_Pass];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


#pragma mark - 设置新密码请求
-(void)settingPassRequestForNewPwd:(NSString *)newPwd confirmPwd:(NSString *)confirmPwd{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:@"do_new_pw_iso" forKey:@"code"];
    [dict setObject:newPwd forKey:@"newpw"];
    [dict setObject:confirmPwd forKey:@"okpw"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self settingPassRequestForNewPwd:newPwd confirmPwd:confirmPwd];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:@"设置新密码成功" inView:self.navigationController.view];
                /*******
                 1、改变Is_NewPwd
                 2、把修改密码覆盖到保存密码，以方便自动登录
                 3、发送通知到个人中心，更改设置新密码为修改密码
                 4、pop到上级Controller
                 *******/
                //0--未修改过的  1--修改过的
                [AppUtils saveValue:@"1" forKey:Is_NewPwd];
                [AppUtils saveValue:newPwd forKey:User_Pass];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Update_Is_NewPwd" object:nil];
                
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[BindingPhoneController class]]) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }else{
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                }
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
