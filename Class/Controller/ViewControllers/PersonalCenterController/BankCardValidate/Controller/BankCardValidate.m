//
//  BankCardValidate.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "BankCardValidate.h"
#import <AVFoundation/AVFoundation.h>

@interface BankCardValidate ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic, strong) BankCardValModel *model;

@end

@implementation BankCardValidate{
    NSString *seleIdCartFlat;
    
    NSMutableDictionary *frontIdCartData;
    NSMutableDictionary *backIdCartData;
    
    BOOL isFrontImgHadata;
    BOOL isBackImgHadata;
    
    
    UITextField *nameTF;
    UITextField *idCardTF;
    UITextField *bankTF;
    UITextField *phoneTF;
    
    UIButton *idCardBtn01;
    UIButton *idCardBtn02;
    
    UIButton *subBtn;
}

+(id)initWithModel:(BankCardValModel *)model{
    BankCardValidate *vc=[BankCardValidate new];
    vc.model=model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpUI];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"银行卡验证" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    UITableView *table=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-108)];
    [self.view addSubview:table];
    
    UIView *footView=[[UIView alloc] initWithFrame:table.frame];
    table.tableFooterView=footView;
    
    UILabel *tt01=[[UILabel alloc] initWithFrame:CGRectMake(16, 8, SCREENSIZE.width-32, 44)];
    tt01.font=kFontSize16;
    tt01.numberOfLines=0;
    tt01.text=@"验证您最常用的银行储蓄卡,游侠分期/购物分期每月将自动还款。";
    [footView addSubview:tt01];
    
#pragma mark 姓名
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, tt01.origin.y+tt01.height+8, 88, 42)];
    nameLabel.font=kFontSize16;
    nameLabel.text=@"姓 名:";
    [footView addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, footView.width-(nameLabel.origin.x*2+nameLabel.width+8), nameLabel.height)];
    nameView.layer.cornerRadius=4;
    nameView.layer.borderWidth=0.5f;
    nameView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    nameView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:nameView];
    
    nameTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, nameView.width-10, nameView.height)];
    if ([_model.person_status isEqualToString:@"2"]) {
        nameTF.enabled=NO;
        nameTF.userInteractionEnabled = NO;
    }
    nameTF.font=kFontSize15;
    nameTF.placeholder=@"请输入姓名";
    nameTF.text=_model.name;
    if ([_model.status isEqualToString:@"1"]) {
        nameTF.enabled=NO;
    }else{
        nameTF.enabled=YES;
    }
    nameTF.textColor=[UIColor colorWithHexString:@"70635a"];
    nameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:nameTF];
    
#pragma mark 身份证号码
    UILabel *idCardLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, 88, nameLabel.height)];
    idCardLabel.font=kFontSize16;
    idCardLabel.text=@"身份证号码:";
    [footView addSubview:idCardLabel];
    
    UIView *idCardView=[[UIView alloc] initWithFrame:CGRectMake(idCardLabel.origin.x+idCardLabel.width+8, idCardLabel.origin.y, nameView.width, nameView.height)];
    idCardView.layer.cornerRadius=4;
    idCardView.layer.borderWidth=0.5f;
    idCardView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    idCardView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:idCardView];
    
    idCardTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, idCardView.width-10, idCardView.height)];
    idCardTF.font=kFontSize15;
    if ([_model.person_status isEqualToString:@"2"]) {
        idCardTF.enabled=NO;
        idCardTF.userInteractionEnabled = NO;
    }
    idCardTF.placeholder=@"请输入身份证号码";
    idCardTF.text=_model.idCard;
    if ([_model.status isEqualToString:@"1"]) {
        idCardTF.enabled=NO;
    }else{
        idCardTF.enabled=YES;
    }
    idCardTF.textColor=[UIColor colorWithHexString:@"70635a"];
    idCardTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [idCardView addSubview:idCardTF];
    
#pragma mark 银行卡卡号
    UILabel *bankLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, idCardLabel.origin.y+nameLabel.height+12, 88, nameLabel.height)];
    bankLabel.font=kFontSize16;
    bankLabel.text=@"银行卡卡号:";
    [footView addSubview:bankLabel];
    
    UIView *bankView=[[UIView alloc] initWithFrame:CGRectMake(bankLabel.origin.x+bankLabel.width+8, bankLabel.origin.y, nameView.width, nameView.height)];
    bankView.layer.cornerRadius=4;
    bankView.layer.borderWidth=0.5f;
    bankView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    bankView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:bankView];
    
    bankTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    bankTF.font=kFontSize15;
    bankTF.placeholder=@"请输入银行卡卡号";
    bankTF.text=_model.bankNum;
    if ([_model.status isEqualToString:@"1"]) {
        bankTF.enabled=NO;
    }else{
        bankTF.enabled=YES;
    }
    bankTF.textColor=[UIColor colorWithHexString:@"70635a"];
    bankTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [bankView addSubview:bankTF];
    
#pragma mark 预留手机号
    UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, bankLabel.origin.y+bankLabel.height+12, 88, nameLabel.height)];
    phoneLabel.font=kFontSize16;
    phoneLabel.text=@"预留手机号:";
    [footView addSubview:phoneLabel];
    
    UIView *phoneView=[[UIView alloc] initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, phoneLabel.origin.y, nameView.width, nameView.height)];
    phoneView.layer.cornerRadius=4;
    phoneView.layer.borderWidth=0.5f;
    phoneView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    phoneView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:phoneView];
    
    phoneTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    phoneTF.font=kFontSize15;
    phoneTF.placeholder=@"请输入预留手机号";
    phoneTF.text=_model.phone;
    if ([_model.status isEqualToString:@"1"]) {
        phoneTF.enabled=NO;
    }else{
        phoneTF.enabled=YES;
    }
    phoneTF.textColor=[UIColor colorWithHexString:@"70635a"];
    phoneTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [phoneView addSubview:phoneTF];
    
    /////////////////
    
    UILabel *tishitip01=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, phoneLabel.origin.y+phoneLabel.height+6, SCREENSIZE.width-2*nameLabel.origin.x, 60)];
    tishitip01.text=@"推荐使用通过率较高的工商银行，农业银行，中国银行，交通银行，中信银行，光大银行，华夏银行，民生银行，兴业银行，浦东发展银行，招商银行，广发银行，邮政银行，平安银行。";
    tishitip01.font=kFontSize11;
    tishitip01.textColor=[UIColor lightGrayColor];
    tishitip01.textAlignment=NSTextAlignmentLeft;
    tishitip01.numberOfLines=4;
    [footView addSubview:tishitip01];
    

    UILabel *tip01=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, tishitip01.origin.y+tishitip01.height, SCREENSIZE.width-2*nameLabel.origin.x, 32)];
    tip01.text=@"请上传以上银行储蓄卡正反面照片";
    tip01.font=kFontSize16;
    tip01.textColor=[UIColor colorWithHexString:@"#282828"];
    tip01.textAlignment=NSTextAlignmentLeft;
    tip01.numberOfLines=0;
    [footView addSubview:tip01];
    
    //
    UILabel *upLoadTip01=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, tip01.origin.y+tip01.height, SCREENSIZE.width-24, 26)];
    upLoadTip01.text=@"注：JPG、PNG格式图片，大小不超过2M";
    upLoadTip01.font=kFontSize14;
    upLoadTip01.textColor=[UIColor redColor];
    upLoadTip01.textAlignment=NSTextAlignmentLeft;
    upLoadTip01.numberOfLines=0;
    [footView addSubview:upLoadTip01];
    
#pragma mark 显示身份证
    //显示身份证的
    idCardBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    idCardBtn01.frame=CGRectMake(16, upLoadTip01.origin.y+upLoadTip01.height+10, (SCREENSIZE.width-48)/2, (SCREENSIZE.width-48)/2);
    idCardBtn01.layer.borderWidth=0.7f;
    idCardBtn01.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [idCardBtn01 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    idCardBtn01.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [idCardBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    idCardBtn01.titleLabel.font=kFontSize15;
    [idCardBtn01 addTarget:self action:@selector(clickBtn01:) forControlEvents:UIControlEventTouchUpInside];
    if (![_model.bandFrontImg isEqualToString:@""]||_model.bandFrontImg!=NULL) {
        [idCardBtn01 setImageWithURL:[NSURL URLWithString:_model.bandFrontImg] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"add_picture"]];
        isFrontImgHadata=YES;
    }else{
        [idCardBtn01 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    }
    
    [footView addSubview:idCardBtn01];
    
    
    
    idCardBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    idCardBtn02.frame=CGRectMake(idCardBtn01.origin.x+idCardBtn01.width+12, idCardBtn01.origin.y, idCardBtn01.width, idCardBtn01.height);
    idCardBtn02.layer.borderWidth=0.7f;
    idCardBtn02.imageView.contentMode=UIViewContentModeScaleAspectFit;
    idCardBtn02.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [idCardBtn02 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    [idCardBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    idCardBtn02.titleLabel.font=kFontSize15;
    [idCardBtn02 addTarget:self action:@selector(clickBtn02:) forControlEvents:UIControlEventTouchUpInside];
    if (![_model.bandBackImg isEqualToString:@""]||_model.bandBackImg!=NULL) {
        [idCardBtn02 setImageWithURL:[NSURL URLWithString:_model.bandBackImg] forState:UIControlStateNormal placeholder:[UIImage imageNamed:@"add_picture"]];
        isBackImgHadata=YES;
    }else{
        [idCardBtn02 setImage:[UIImage imageNamed:@"add_picture"] forState:UIControlStateNormal];
    }
    [footView addSubview:idCardBtn02];
    
    if ([_model.status isEqualToString:@"1"]) {
        [idCardBtn01 setUserInteractionEnabled:NO];
        [idCardBtn02 setUserInteractionEnabled:NO];
    }else{
        [idCardBtn01 setUserInteractionEnabled:YES];
        [idCardBtn02 setUserInteractionEnabled:YES];
    }
    
    //身份证照片左上角（正反面提示）
    UIImageView *zmImg=[UIImageView new];
    zmImg.image=[UIImage imageNamed:@"uploadimg_zm"];
    [idCardBtn01 addSubview:zmImg];
    zmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* zmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_h];
    NSArray* zmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_w];
    
    UIImageView *fmImg=[UIImageView new];
    fmImg.image=[UIImage imageNamed:@"uploadimg_fm"];
    [idCardBtn02 addSubview:fmImg];
    fmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* fmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_h];
    NSArray* fmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_w];
    
#pragma mark 提交按钮
    subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    subBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    subBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    subBtn.titleLabel.font=kFontSize17;
    [subBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:subBtn];
    if (![_model.status isEqualToString:@"1"]) {
        subBtn.hidden=NO;
    }else{
        subBtn.hidden=YES;
    }
}

-(void)clickBtn01:(id)sender{
    seleIdCartFlat=@"1";
    [self showAddPictureView];
}

-(void)clickBtn02:(id)sender{
    seleIdCartFlat=@"2";
    [self showAddPictureView];
}

#pragma mark - 点击提交按钮
-(void)clickSubBtn{
    if ([nameTF.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"姓名不能为空" inView:self.view];
    }else if ([idCardTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"身份证号码不能为空" inView:self.view];
    }else if ([bankTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"银行卡卡号不能为空" inView:self.view];
    }else if ([phoneTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"预留手机号不能为空" inView:self.view];
    }else if (!isFrontImgHadata){
        [AppUtils showSuccessMessage:@"请上传身份证正面图片" inView:self.view];
    }else if (!isBackImgHadata){
        [AppUtils showSuccessMessage:@"请上传身份证反面图片" inView:self.view];
    }else{
        [self requestCXKForName:nameTF.text personId:idCardTF.text cartNum:bankTF.text phone:phoneTF.text];
    }
}

#pragma mark - ShowAddPictureView
-(void)showAddPictureView{
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            default:
                [self pickImage];
                break;
        }
    };
    
    NSArray *items =
    @[MMItemMake(@"拍照", MMItemTypeNormal, block),
      MMItemMake(@"从手机相册选择", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:^(MMPopupView *popupView){
        
    }];
}

//拍照
- (void) snapImage{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"请在iPhone的“设置-隐私-相机”选项中，允许 游侠旅行 访问你的相机" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.videoQuality = UIImagePickerControllerQualityTypeHigh;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    ipc.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    ipc.navigationBar.barTintColor = [UIColor colorWithRed:20.f/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1];
    
    ipc.navigationBar.tintColor = [UIColor whiteColor];
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //取得原始图片
    UIImage *image= [info objectForKey:@"UIImagePickerControllerEditedImage"];
    NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
    UIImage *newImage=[UIImage imageWithData:imageData];
    
    //判断类型（拍照还是从相册选取）
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        
        UIImageWriteToSavedPhotosAlbum([info objectForKey:@"UIImagePickerControllerOriginalImage"], nil, nil, nil);
    }else{
        
    }
    
    UIImage *theImage = [self imageWithImageSimple:newImage scaledToSize:CGSizeMake(700, 700)];
    
    NSData* upImgData = UIImagePNGRepresentation(theImage);
    NSString *imgByte=[NSString stringWithFormat:@"%lu",(unsigned long)upImgData.length];
    
    if ([seleIdCartFlat isEqualToString:@"1"] ) {
        isFrontImgHadata=YES;
        
        frontIdCartData=[NSMutableDictionary dictionary];
        
        [frontIdCartData setObject:upImgData forKey:@"zfl"];
        [frontIdCartData setObject:@"png" forKey:@"img_type"];
        [frontIdCartData setObject:imgByte forKey:@"size"];
        
        backIdCartData=[NSMutableDictionary dictionary];
        
        [backIdCartData setObject:@"" forKey:@"zfl"];
        [backIdCartData setObject:@"" forKey:@"img_type"];
        [backIdCartData setObject:@"" forKey:@"size"];
        
        //上传
        [self UploadPictureWithFrontDatas:frontIdCartData backData:backIdCartData];
        
    }
    
    
    else{
        isBackImgHadata=YES;
        
        frontIdCartData=[NSMutableDictionary dictionary];
        [frontIdCartData setObject:@"" forKey:@"zfl"];
        [frontIdCartData setObject:@"" forKey:@"img_type"];
        [frontIdCartData setObject:@"" forKey:@"size"];
        
        backIdCartData=[NSMutableDictionary dictionary];
        [backIdCartData setObject:upImgData forKey:@"zfl"];
        [backIdCartData setObject:@"png" forKey:@"img_type"];
        [backIdCartData setObject:imgByte forKey:@"size"];
        
        //上传
        [self UploadPictureWithFrontDatas:frontIdCartData backData:backIdCartData];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationBar *navigationBar = navigationController.navigationBar;
    [navigationBar lt_setBackgroundColor:[UIColor colorWithHexString:@"0080c5"]];
    navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

#pragma mark - 上传银行卡图片 Request

-(void)UploadPictureWithFrontDatas:(NSDictionary *)frontDic backData:(NSDictionary *)backDic{
    [AppUtils showProgressMessage:@"正在上传图片…" inView:self.view];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"upload_imgs_bankcard_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:@"imgs_bankcard" forKey:@"type"];
    [dict setObject:frontDic forKey:@"data0"];
    [dict setObject:backDic forKey:@"data1"];
    [dict setObject:@"1" forKey:@"is_nosign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //正面
            if ([seleIdCartFlat isEqualToString:@"1"]) {
                //处理提示的
                if ([responseObject[@"retData"][@"front"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"front"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"front"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"front"][@"is_lock"] isEqualToString:@"1"]) {
                    NSString *imgStr=responseObject[@"retData"][@"front"][@"up_info"][@"url"];
                    [idCardBtn01 setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholder:nil];
                    [idCardBtn01 setUserInteractionEnabled:NO];
                }
                //正面不保存
                else{
                    
                }
            }
            
            //反面
            else{
                //处理提示的
                if ([responseObject[@"retData"][@"back"][@"yz_type"] isEqualToString:@"error"]) {
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"back"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }else{
                    [AppUtils showSuccessMessage:responseObject[@"retData"][@"back"][@"msg"] inView:self.view];
                }
                
                //处理按钮状态（显示或隐藏,=1保存，！=1不保存）
                if ([responseObject[@"retData"][@"back"][@"is_lock"] isEqualToString:@"1"]) {
                    NSString *imgStr=responseObject[@"retData"][@"back"][@"up_info"][@"url"];
                    [idCardBtn02 setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholder:nil];
                    [idCardBtn02 setUserInteractionEnabled:NO];
                }
                //反面不保存
                else{
                    
                }
            }
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark  储蓄卡验证 Request
//储蓄卡验证
-(void)requestCXKForName:(NSString *)name personId:(NSString *)personId cartNum:(NSString *)cartNum phone:(NSString *)phone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_bank1_iso" forKey:@"code"];
    [dict setObject:name forKey:@"person_name"];
    [dict setObject:personId forKey:@"person_idcard"];
    [dict setObject:cartNum forKey:@"bank_1_number"];
    [dict setObject:phone forKey:@"bank_1_mobile"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestCXKForName:name personId:personId cartNum:cartNum phone:phone];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
                nameTF.enabled=NO;
                idCardTF.enabled=NO;
                bankTF.enabled=NO;
                phoneTF.enabled=NO;
                idCardBtn01.userInteractionEnabled=NO;
                idCardBtn02.userInteractionEnabled=NO;
                subBtn.hidden=YES;
            }else{
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
