//
//  BankCardValidate.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardValModel.h"

@interface BankCardValidate : UIViewController

+(id)initWithModel:(BankCardValModel *)model;

@end
