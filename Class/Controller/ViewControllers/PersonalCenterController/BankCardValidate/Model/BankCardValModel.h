//
//  BankCardValModel.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankCardValModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *idCard;
@property (nonatomic, copy) NSString *bankNum;
@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *bandFrontImg;     //正面
@property (nonatomic, copy) NSString *bandFrontStatus;
@property (nonatomic, copy) NSString *bandBackImg;      //反面
@property (nonatomic, copy) NSString *bandBackStatus;
@property (nonatomic, copy) NSString *person_status;
@property (nonatomic, copy) NSString *status;           //总的status

-(void)jsonToModel:(NSDictionary *)dic;

@end
