//
//  BankCardValModel.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "BankCardValModel.h"

@implementation BankCardValModel

-(void)jsonToModel:(NSDictionary *)dic{
    if (dic[@"person_name"]&&![dic[@"person_name"] isKindOfClass:[NSNull class]]) {
        self.name=dic[@"person_name"];
    }else{
        self.name=@"";
    }
    
    if (dic[@"person_idcard"]&&![dic[@"person_idcard"] isKindOfClass:[NSNull class]]) {
        self.idCard=dic[@"person_idcard"];
    }else{
        self.idCard=@"";
    }
    
    if (dic[@"bank_1_number"]&&![dic[@"bank_1_number"] isKindOfClass:[NSNull class]]) {
        self.bankNum=dic[@"bank_1_number"];
    }else{
        self.bankNum=@"";
    }
    
    if (dic[@"person_status"]&&![dic[@"person_status"] isKindOfClass:[NSNull class]]) {
        self.person_status=dic[@"person_status"];
    }else{
        self.person_status=@"";
    }
    
    if (dic[@"bank_1_mobile"]&&![dic[@"bank_1_mobile"] isKindOfClass:[NSNull class]]) {
        self.phone=dic[@"bank_1_mobile"];
    }else{
        self.phone=@"";
    }
    
    if (dic[@"img_bankcard_front"]&&![dic[@"img_bankcard_front"] isKindOfClass:[NSNull class]]&&![dic[@"img_bankcard_front"] isEqualToString:@""]) {
        self.bandFrontImg=dic[@"img_bankcard_front"];
    }else{
        self.bandFrontImg=@"";
    }
    
    if (dic[@"idcard_front_status"]&&![dic[@"idcard_front_status"] isKindOfClass:[NSNull class]]) {
        self.bandFrontStatus=dic[@"idcard_front_status"];
    }else{
        self.bandFrontStatus=@"";
    }
    
    if (dic[@"img_bankcard_back"]&&![dic[@"img_bankcard_back"] isKindOfClass:[NSNull class]]&&![dic[@"img_bankcard_back"] isEqualToString:@""]) {
        self.bandBackImg=dic[@"img_bankcard_back"];
    }else{
        self.bandBackImg=@"";
    }
    
    if (dic[@"idcard_back_status"]&&![dic[@"idcard_back_status"] isKindOfClass:[NSNull class]]) {
        self.bandBackStatus=dic[@"idcard_back_status"];
    }else{
        self.bandBackStatus=@"";
    }
    
    if (dic[@"bank_1_status"]&&![dic[@"bank_1_status"] isKindOfClass:[NSNull class]]) {
        self.status=dic[@"bank_1_status"];
    }else{
        self.status=@"";
    }
}

@end
