//
//  SettingController.m
//  youxia
//
//  Created by mac on 2016/12/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SettingController.h"
#import "AboutYouXiaController.h"
#import "SettingCell.h"

@interface SettingController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SettingController{
    NSArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
    navBar.titleStr=@"设置";
    [self.view addSubview:navBar];
    
    [self setUpUI];
}

#pragma mark - init UI

-(void)setUpUI{
    //
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Setting" ofType:@"plist"];
    dataArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
    [self.view addSubview:myTable];
    
    //
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 100)];
    footView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.tableFooterView=footView;
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor=[UIColor redColor];
    btn.layer.cornerRadius=5;
    btn.frame=CGRectMake(12, 24, SCREENSIZE.width-24, 44);
    [btn setTitle:@"退出登录" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:16];
    [btn addTarget:self action:@selector(clickCancel) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:btn];
}

-(void)clickCancel{
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=23;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self logout];
                break;
            default:
                break;
        }
    };
    NSArray *items =
    @[MMItemMake(@"退出登录", MMItemTypeHighlight, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"退出后不会删除任何历史数据，下次登录依然可以使用本账号"
                                  items:items] showWithBlock:nil];
}

#pragma mark - 退出处理
//=================
//弹出登录、清空密码、
//发送通知以改变tabbar选中状态、
//并pop当前controller
//=================
-(void)logout{
    [AppUtils saveValue:@"" forKey:User_Pass];
    [AppUtils saveValue:@"" forKey:SSO_Login_UID];
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Lgout_notification" object:nil];
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)dataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
    if (cell==nil) {
        cell=[[SettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SettingCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"nameStr"];
    cell.leftLabel.font=[UIFont systemFontOfSize:16];
    cell.leftLabel.textColor=[UIColor colorWithHexString:@"515151"];
    if (indexPath.row==2) {
        cell.rightLabel.text=[self sizeWithImageCache:[[SDImageCache sharedImageCache] getSize]];
    }else{
        cell.rightLabel.text=nil;
    }
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==5){
        return 36;
    }
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *tagStr=dataArr[indexPath.section][indexPath.row][@"tag"];
    switch (tagStr.intValue) {
        case 1:
        {
            AboutYouXiaController *aboutUs=[[AboutYouXiaController alloc]init];
            [self.navigationController pushViewController:aboutUs animated:YES];
        }
            
            break;
        case 2:
        {
            //去评分
            NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",@"1076463089"];
            NSURL * url = [NSURL URLWithString:str];
            
            if ([[UIApplication sharedApplication] canOpenURL:url]){
                [[UIApplication sharedApplication] openURL:url];
            }else{
                DSLog(@"can not open");
            }
        }
            break;
        case 3:
        {
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否清除缓存！"preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                
            }];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                [self clearCaches];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
            break;
        default:
            break;
    }
}

//计算缓存出大小
- (NSString *)sizeWithImageCache:(NSInteger)size{
    // 1k = 1024, 1m = 1024k
    if (size < 1024) {// 小于1k
        return [NSString stringWithFormat:@"%ldB",(long)size];
    }else if (size < 1024 * 1024){// 小于1m
        CGFloat aFloat = size/1024;
        return [NSString stringWithFormat:@"%.0fK",aFloat];
    }else if (size < 1024 * 1024 * 1024){// 小于1G
        CGFloat aFloat = size/(1024 * 1024);
        return [NSString stringWithFormat:@"%.1fM",aFloat];
    }else{
        CGFloat aFloat = size/(1024*1024*1024);
        return [NSString stringWithFormat:@"%.1fG",aFloat];
    }
}

#pragma mark - 清除缓存
- (void)clearCaches{
    [AppUtils showSuccessMessage:@"正在清理缓存……" inView:self.view];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    
    [self performSelectorOnMainThread:@selector(cleanCacheSuccess) withObject:nil waitUntilDone:YES];
}

- (void)cleanCacheSuccess{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:2 inSection:0];
        [myTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        [AppUtils dismissHUDInView:self.view];
        return;
    });
}

@end
