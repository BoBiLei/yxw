//
//  PSCHotelOrderTrack.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelOrderTrack.h"
#import "PSCHotelOrderTrackCell.h"
//#import "HotelOrderTrackModel.h"

@interface PSCHotelOrderTrack ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, strong) UITableView *myTable;

@end

@implementation PSCHotelOrderTrack{
    NSMutableArray *dataArr;
}

+(id)initWithOrderId:(NSString *)orderId{
    PSCHotelOrderTrack *vc=[PSCHotelOrderTrack new];
    vc.orderId=orderId;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"订单轨迹";
    [self.view addSubview:navBar];
    
    [self dataRequest];
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
        _myTable.scrollEnabled=NO;
        _myTable.backgroundColor=[UIColor whiteColor];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelOrderTrackCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelOrderTrackCell"];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [self.view addSubview:_myTable];
    }
    return  _myTable;
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?0.00000001f:0.000001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?0.00000001f:0.000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PSCHotelOrderTrackCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelOrderTrackCell"];
    if (!cell) {
        cell=[[PSCHotelOrderTrackCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelOrderTrackCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.trackBtn setImage:[UIImage imageNamed:indexPath.section==0?@"track_sele":@"track_nor"] forState:UIControlStateNormal];
    HotelOrderTrackModel *model=dataArr[indexPath.section];
    [cell reflushData:model];
    if (indexPath.section==0) {
        cell.line01.hidden=YES;
        cell.line02.hidden=NO;
    }else{
        if (indexPath.section==dataArr.count-1) {
            cell.line01.hidden=NO;
            cell.line02.hidden=YES;
        }else{
            cell.line01.hidden=NO;
            cell.line02.hidden=NO;
        }
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - request
-(void)dataRequest{
    dataArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"orderTrack",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSArray class]]) {
                NSArray *arr=retObj;
                for (NSDictionary *dic in arr) {
                    HotelOrderTrackModel *model=[HotelOrderTrackModel new];
                    [model jsonToModel:dic];
                    [dataArr addObject:model];
                }
                [self.myTable reloadData];
            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
