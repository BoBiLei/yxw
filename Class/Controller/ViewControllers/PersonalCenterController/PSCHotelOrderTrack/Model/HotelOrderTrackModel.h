//
//  HotelOrderTrackModel.h
//  youxia
//
//  Created by mac on 2017/5/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelOrderTrackModel : NSObject

@property (nonatomic, copy) NSString *comment;
@property (nonatomic, copy) NSString *trackTime;

-(void)jsonToModel:(NSDictionary *)dic;

@end
