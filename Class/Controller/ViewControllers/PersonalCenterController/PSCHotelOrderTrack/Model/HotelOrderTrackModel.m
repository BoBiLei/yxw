//
//  HotelOrderTrackModel.m
//  youxia
//
//  Created by mac on 2017/5/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelOrderTrackModel.h"

@implementation HotelOrderTrackModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"comment"] isKindOfClass:[NSNull class]]) {
        self.comment=@"";
    }else{
        self.comment=dic[@"comment"];
    }
    
    if ([dic[@"trackTime"] isKindOfClass:[NSNull class]]) {
        self.trackTime=@"";
    }else{
        self.trackTime=dic[@"trackTime"];
    }
}

@end
