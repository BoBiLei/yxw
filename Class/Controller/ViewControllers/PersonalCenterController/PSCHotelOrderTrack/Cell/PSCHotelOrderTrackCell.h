//
//  PSCHotelOrderTrackCell.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelOrderTrackModel.h"

@interface PSCHotelOrderTrackCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *line01;
@property (weak, nonatomic) IBOutlet UIView *line02;

@property (weak, nonatomic) IBOutlet UIButton *trackBtn;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

-(void)reflushData:(HotelOrderTrackModel *)model;

@end
