//
//  ApproveController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ApproveController.h"
#import "SubmitCreditInfoController.h"
#import "PersonIdCartModel.h"
#import "ApproveCell.h"
#import "PersonInfoCell.h"
@interface ApproveController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ApproveController{
    
    UITableView *myTable;
    
    UILabel *label01;
    UILabel *label02;
    UILabel *label03;
    UILabel *label04;
    
    NSString *nameStr;
    NSString *fenqiStr;
    NSString *statusStr;
    
    //
    NSMutableArray *cxkArr;
    NSMutableArray *phoneArr;
    NSMutableArray *contactArr;
    NSMutableArray *txdzArr;
    PersonInfoCell *BankBtn;                //银行卡的
    PersonIdCartModel *frontIdCartModel;    //身份证正面
    PersonIdCartModel *backIdCartModel;     //身份证反面
    
    BankIdCartModel *frontIdCartModel2;   //银行卡正面
    BankIdCartModel *reversebankCardModel;  // 银行卡反面
    SubmitCreditInfoController *submitCic;
    
    //
    
    NSString *status_msg;   //审核是否失败   =0||=""失败   =有数据 失败
    
    NSString *cartStatus01;
    NSString *cartStatus02;
    
    NSString *carStatus03;
    NSString *carStatus04;
    
    NSString *subBtnStatus; //总的验证状态
    
    //
    BOOL isOk01;
    BOOL isOk02;
    BOOL isOk03;
    BOOL isOk04;
    BOOL isOk05;
    BOOL isOk06;
    BOOL isOk07;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"实名认证页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"实名认证页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCXKStatus:) name:UpdateXCKNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLXRStatus:) name:UpdateContactNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTXDZStatus:) name:UpdateTXDZNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subXYRZSuccess) name:SubmitXYRZSuccess object:nil];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"实名认证获取额度";
    [self.view addSubview:navBar];
    
    [self requestHttps];
    
}


#pragma mark - Notification Method
-(void)updateCXKStatus:(NSNotification *)noti{
    cxkArr=noti.object;
    [myTable reloadData];
}

-(void)updatePhoneStatus:(NSNotification *)noti{
    phoneArr=noti.object;
}

-(void)updateLXRStatus:(NSNotification *)noti{
    contactArr=noti.object;
}

-(void)updateTXDZStatus:(NSNotification *)noti{
    txdzArr=noti.object;
}

-(void)subXYRZSuccess{
    [self requestHttps];
}

#pragma mark - SetUpUI
-(void)createUI{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    [myTable registerNib:[UINib nibWithNibName:@"ApproveCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    UIView *headView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, myTable.height)];
//    myTable.tableHeaderView=headView;
    
    UIView *contentView=[[UIView alloc]initWithFrame:CGRectMake(8, 12, SCREENSIZE.width-16, 100)];
    contentView.layer.borderWidth=1.0f;
    contentView.layer.borderColor=[UIColor colorWithHexString:YxColor_Blue].CGColor;
    contentView.backgroundColor=[UIColor orangeColor];
    [headView addSubview:contentView];
    
    //
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, contentView.width, contentView.height)];
    topView.backgroundColor=[UIColor whiteColor];
    [contentView addSubview:topView];
    
    CGFloat labX=0.7;
    CGFloat labWidth=(topView.width-3.0)/4;
    NSString *titStr;
    
    NSString *btmTitStr;
    for (int i=0; i<4; i++) {
        
        UILabel *lab01=[[UILabel alloc]init];
        lab01.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        lab01.frame=CGRectMake(i*labX+i*labWidth, 0, labWidth, topView.height/2);
        lab01.font=kFontSize16;
        lab01.textColor=[UIColor whiteColor];
        
        lab01.textAlignment=NSTextAlignmentCenter;
        [topView addSubview:lab01];
        
        UILabel *lab02=[[UILabel alloc]init];
        lab02.frame=CGRectMake(i*labX+i*labWidth+i, topView.height/2, labWidth, topView.height/2);
        lab02.font=kFontSize16;
        lab02.textColor=[UIColor colorWithHexString:@"#282828"];
        lab02.textAlignment=NSTextAlignmentCenter;
        [topView addSubview:lab02];
        if (i==0) {
            titStr=@"姓名";
            if (![nameStr isEqualToString:@""]) {
                btmTitStr=nameStr;
            }else{
                btmTitStr=@"--";
            }
            label01=lab02;
        }else if (i==1){
            titStr=@"分期额度";
            if (![fenqiStr isEqualToString:@"0"]) {
                btmTitStr=fenqiStr;
            }else{
                btmTitStr=@"--";
            }
            label02=lab02;
        }else if (i==2){
            titStr=@"状态";
            if ([statusStr isEqualToString:@"1"]) {
                btmTitStr=@"--";
            }else if([statusStr isEqualToString:@"2"]){
                btmTitStr=@"审核中";
            }else{
                btmTitStr=@"已验证";
            }
            label03=lab02;
        }else{
            titStr=@"操作";
            if ([statusStr isEqualToString:@"1"]) {
                btmTitStr=@"完善信息";
                lab02.textColor=[UIColor blueColor];
                lab02.userInteractionEnabled=YES;
                UITapGestureRecognizer *tapg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goXyrzPage)];
                [lab02 addGestureRecognizer:tapg];
            }else if([statusStr isEqualToString:@"2"]){
                btmTitStr=@"审核中";
            }else{
                btmTitStr=@"查看";    //=3验证通过
                lab02.textColor=[UIColor blueColor];
                lab02.userInteractionEnabled=YES;
                UITapGestureRecognizer *tapg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goXyrzPage)];
                [lab02 addGestureRecognizer:tapg];
            }
            label04=lab02;
        }
        lab01.text=titStr;
        lab02.text=btmTitStr;
    }
}

-(void)goXyrzPage{
    [self.navigationController pushViewController:submitCic animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApproveCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[ApproveCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *leftStr;
    NSString *rightStr;
    switch (indexPath.row) {
        case 0:{
            leftStr=@"姓名";
            if (![nameStr isEqualToString:@""]) {
                rightStr=nameStr;
            }else{
                rightStr=@"--";
            }
        }
            break;
        case 1:{
            leftStr=@"分期额度";
            if (![fenqiStr isEqualToString:@"0"]) {
                if ([fenqiStr isEqualToString:@"50000"]) {
                    rightStr=@"分期额度2万+临时额度3万";
                }else{
                    rightStr=fenqiStr;
                }
            }else{
                rightStr=@"--";
            }
        }
            break;
        case 2:{
            leftStr=@"状态";
          
            if ([statusStr isEqualToString:@"1"]) {
                if ([status_msg isEqualToString:@"0"]||
                    [status_msg isEqualToString:@""]) {
                    rightStr=@"--";
                }else{
                    rightStr=@"审核失败";
                }
            }else if([statusStr isEqualToString:@"2"]){
                rightStr=@"审核中";
            }else if ([statusStr isEqualToString:@"4"])
            {
                rightStr =@"你的信息不符合开通分期";
            }
            else{
                rightStr=@"已验证";
            }
        }
            break;
        case 3:{
            leftStr=@"操作";
          
            if ([statusStr isEqualToString:@"1"]) {
                if ([status_msg isEqualToString:@"0"]||
                    [status_msg isEqualToString:@""]) {
                    rightStr=@"完善VIP信息";
                }else{
                    rightStr=@"重新提交审核";
                }
                
                cell.rightLabel.textColor=[UIColor blueColor];
                cell.rightLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer *tapg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goXyrzPage)];
                [cell.rightLabel addGestureRecognizer:tapg];
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;

            }else if([statusStr isEqualToString:@"2"]){
                rightStr=@"审核中";
                cell.rightLabel.textColor=[UIColor blueColor];
                cell.rightLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer *tapg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goXyrzPage)];
                [cell.rightLabel addGestureRecognizer:tapg];
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }else if ([statusStr isEqualToString:@"4"])
            {
                rightStr=@"审核失败";
                cell.rightLabel.textColor=[UIColor blueColor];
                cell.userInteractionEnabled = NO;
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
            else{
                rightStr=@"查看";    //=3验证通过
                cell.rightLabel.textColor=[UIColor blueColor];
                cell.rightLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer *tapg=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goXyrzPage)];
                [cell.rightLabel addGestureRecognizer:tapg];
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;

            }
        }
            break;
        default:
            break;
    }
    cell.leftLabel.text=leftStr;
    cell.rightLabel.text=rightStr;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark Request 请求
-(void)requestHttps{
    
    [AppUtils showProgressInView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"vip_write_iso" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttps];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                
                //
                nameStr=responseObject[@"retData"][@"info"][@"person_name"];
                fenqiStr=responseObject[@"retData"][@"info"][@"amount"];
                statusStr=responseObject[@"retData"][@"info"][@"status"];
                
                //储蓄卡
                NSString *personIdStr=responseObject[@"retData"][@"info"][@"person_idcard"];
                NSString *xckStr=responseObject[@"retData"][@"info"][@"bank_1_number"];
                NSString *xckPhone=responseObject[@"retData"][@"info"][@"bank_1_mobile"];
                NSString *xckStatus=responseObject[@"retData"][@"info"][@"bank_1_status"];
                if ([xckStatus isEqualToString:@"1"]) {
                    isOk01=YES;
                }
                cxkArr=[NSMutableArray arrayWithObjects:nameStr,personIdStr,xckStr,xckPhone,xckStatus, nil];
                
                
                
                //审核是否失败(有数据就是失败)
                status_msg=responseObject[@"retData"][@"info"][@"status_msg"];
                
                //
                NSString *ctName=responseObject[@"retData"][@"info"][@"person_jj_name"];
                NSString *ctPhone=responseObject[@"retData"][@"info"][@"person_jj_code"];
                NSString *ctStatus=responseObject[@"retData"][@"info"][@"jj_status"];
                if ([ctStatus isEqualToString:@"2"]) {
                    isOk03=YES;
                }
                contactArr=[NSMutableArray arrayWithObjects:ctName,ctPhone,ctStatus, nil];
                
                //
                NSString *cmpName=responseObject[@"retData"][@"info"][@"work_company"];
                NSString *cmpA01=responseObject[@"retData"][@"info"][@"SelectProvince3"];
                NSString *cmpA02=responseObject[@"retData"][@"info"][@"SelectCity3"];
                NSString *cmpA03=responseObject[@"retData"][@"info"][@"SelectDistrict3"];
                NSArray *cmpAddressArr=@[cmpA01,cmpA02,cmpA03];
                NSString *cmpDtAdr=responseObject[@"retData"][@"info"][@"work_address"];
                NSString *cmpRcv=responseObject[@"retData"][@"info"][@"work_sjname"];
                NSString *cmpPhone=responseObject[@"retData"][@"info"][@"work_phone"];
                
                NSString *homeA01=responseObject[@"retData"][@"info"][@"SelectProvince"];
                NSString *homeA02=responseObject[@"retData"][@"info"][@"SelectCity"];
                NSString *homeA03=responseObject[@"retData"][@"info"][@"SelectDistrict"];
                NSArray *homeAddressArr=@[homeA01,homeA02,homeA03];
                NSString *homeDetAdr=responseObject[@"retData"][@"info"][@"person_address_1"];
                NSString *homeRcv=responseObject[@"retData"][@"info"][@"home_sjname"];
                NSString *homePhone=responseObject[@"retData"][@"info"][@"home_phone"];
                NSString *txStatus=responseObject[@"retData"][@"info"][@"person_status"];
                if ([txStatus isEqualToString:@"2"]) {
                    isOk04=YES;
                }
                txdzArr=[NSMutableArray arrayWithObjects:cmpName,cmpAddressArr,cmpDtAdr,cmpRcv,cmpPhone,homeAddressArr,homeDetAdr,homeRcv,homePhone,txStatus,nil];
                
               
                
                
                //身份证照片
                NSDictionary *personDic=responseObject[@"retData"][@"info"];
                frontIdCartModel=[[PersonIdCartModel alloc]init];
                [frontIdCartModel jsonDataForFrontIdCart:personDic];
                backIdCartModel=[[PersonIdCartModel alloc]init];
                [backIdCartModel jsonDataForBackIdCart:personDic];
                
                //银行卡照片
                NSDictionary *BankCarDic=responseObject[@"retData"][@"info"];
                frontIdCartModel2=[[BankIdCartModel alloc]init];
                [frontIdCartModel2 jsonDataForBackIdCart:BankCarDic];
                reversebankCardModel=[[BankIdCartModel alloc]init];
                [reversebankCardModel jsonDataForBackIdCart:BankCarDic];
                
                
               //
//                carStatus03=responseObject[@"retData"][@"info"][@"bank_1_status"];
//                carStatus04=responseObject[@"reData"][@"info"][@"bank_2_status"];
                
                //
                cartStatus01=responseObject[@"retData"][@"info"][@"bank_1_status"];
                cartStatus02=responseObject[@"retData"][@"info"][@"bank_2_status"];
                
                //////////////////////////////
                //总的 status
                /*
                 *1/0--未审核
                 *2--审核中
                 *3--审核通过
                 *4--审核失败
                 */
                subBtnStatus=responseObject[@"retData"][@"info"][@"status"];
                
                
                               //验证手机
                //phone_status 0 ""未通过    1通过
                NSString *pHandStr=responseObject[@"retData"][@"info"][@"phone_num"];
                NSString *phoneStr;
                NSString *phoneStatus;
                if ([subBtnStatus isEqualToString:@"2"]||
                    [subBtnStatus isEqualToString:@"3"]) {
                    phoneStatus=@"1";
                    if ([pHandStr isEqualToString:@""]) {
                        phoneStr=xckPhone;
                    }else{
                        phoneStr=pHandStr;
                    }
                }else{
                    phoneStatus=responseObject[@"retData"][@"info"][@"phone_status"];
                    phoneStr=pHandStr;
                }
                //审核是否失败(有数据就是失败)
                NSString *guishu=responseObject[@"retData"][@"info"][@"phone_guishu"];
                NSString *dxPass=responseObject[@"retData"][@"info"][@"phone_pwd"];

                //4-归属 5-密码
                phoneArr=[NSMutableArray arrayWithObjects:phoneStr,phoneStatus,@"",@"",guishu,dxPass, nil];
                if ([phoneStatus isEqualToString:@"1"]||[guishu isEqualToString:@"电信"]) {
                    isOk02=YES;
                }
                
                //
                submitCic=[[SubmitCreditInfoController alloc]init];
                submitCic.cartStatus_01=cartStatus01;
                submitCic.cartStatus_01=cartStatus02;
                
                //银行卡
                submitCic.cartStatus_03=carStatus03;
                submitCic.cartStatus_03=carStatus04;
                
                submitCic.cxkArr=cxkArr;
                submitCic.phoneArr=phoneArr;
                submitCic.contaceArr=contactArr;
                submitCic.txdzArr=txdzArr;
                submitCic.frontIdCartModel=frontIdCartModel;
                submitCic.backIdCartModel=backIdCartModel;
                
                id fObj=responseObject[@"retData"][@"info"][@"img_bankcard_front"];
                if ([fObj isKindOfClass:[NSString class]]) {
                    submitCic.receFrontBankCartImg=fObj;             //银行卡正面
                }else{
                    submitCic.receFrontBankCartImg=@"";
                }
                
                id bObj=responseObject[@"retData"][@"info"][@"img_bankcard_back"];
                if ([bObj isKindOfClass:[NSString class]]) {
                    submitCic.receBackBankCartImg=bObj;            // 银行卡反面
                }else{
                    submitCic.receBackBankCartImg=@"";
                }
                
                
                submitCic.status_msg=status_msg;
                submitCic.submitBtnStatus=subBtnStatus;
//                submitCic.submitBtnStatus=@"2";
                
                submitCic.isOk01=isOk01;
                submitCic.isOk02=isOk02;
                submitCic.isOk03=isOk03;
                submitCic.isOk04=isOk04;
                submitCic.isOk05=isOk05;
                submitCic.isOk06=isOk06;
                [self createUI];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
