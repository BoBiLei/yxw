//
//  PersonIdCartModel.h
//  youxia
//
//  Created by mac on 16/1/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonIdCartModel : NSObject

@property (nonatomic, copy) NSString *idCartStatus;
@property (nonatomic, retain) NSArray *idCartArray;

/**正面*/
-(void)jsonDataForFrontIdCart:(NSDictionary *)dic;

/**反面*/
-(void)jsonDataForBackIdCart:(NSDictionary *)dic;

@end
