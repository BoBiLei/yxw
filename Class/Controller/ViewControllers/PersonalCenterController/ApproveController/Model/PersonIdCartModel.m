//
//  PersonIdCartModel.m
//  youxia
//
//  Created by mac on 16/1/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PersonIdCartModel.h"

@implementation PersonIdCartModel

/**正面*/
-(void)jsonDataForFrontIdCart:(NSDictionary *)dic{
    _idCartStatus=dic[@"idcard_front_status"];
    _idCartArray=dic[@"img_idcard_front"];
}

/**反面*/
-(void)jsonDataForBackIdCart:(NSDictionary *)dic{
    _idCartStatus=dic[@"idcard_back_status"];
    _idCartArray=dic[@"img_idcard_back"];
}

@end
