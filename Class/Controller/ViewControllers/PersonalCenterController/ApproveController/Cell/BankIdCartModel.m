//
//  PersonIdCartModel.m
//  youxia
//
//  Created by mac on 16/1/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BankIdCartModel.h"

@implementation BankIdCartModel

/**正面*/
-(void)jsonDataForFrontIdCart:(NSDictionary *)dic{
//    _idCartStatuss=dic[@"idcard_front_status"];
    _idCartArrays=dic[@"img_bankcard_front"];
}

/**反面*/
-(void)jsonDataForBackIdCart:(NSDictionary *)dic{
//    _idCartStatuss=dic[@"idcard_back_status"];
    _idCartArrays=dic[@"img_bankcard_back"];
}

@end
