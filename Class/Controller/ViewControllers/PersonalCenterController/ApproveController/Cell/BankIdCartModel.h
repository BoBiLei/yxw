//
//  PersonIdCartModel.h
//  youxia
//
//  Created by mac on 16/1/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BankIdCartModel : NSObject

@property (nonatomic, copy) NSString *idCartStatuss;
@property (nonatomic, retain) NSArray *idCartArrays;

/**银行卡正面*/
-(void)jsonDataForFrontIdCart:(NSDictionary *)dic;

/**银行卡反面*/
-(void)jsonDataForBackIdCart:(NSDictionary *)dic;

@end
