//
//  FilmListController.m
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmListController.h"
#import "FilmListCell.h"
#import "FilmListModel.h"
#import "FilmTicketDetail.h"
#import "WaitTourController.h"
#import "FilmConfirmOrder.h"
#import "FilmTicketCloseController.h"
#import <UIImage+GIF.h>
#import "TourSuccessController.h"

@interface FilmListController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation FilmListController{
    
    UIButton *seleFilterBtn;
    UIView *btLine;
    
    NSInteger pageInt;
    NSString *requestType;
    
    NSMutableArray *sourceArr;
    UITableView *myTable;
    MJRefreshNormalHeader *refleshHeader;
    MJRefreshAutoNormalFooter *refleshFooter;
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateRequest) name:FilmPay_Success_RefleshList_Noti object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateRequest) name:FilmOrder_CancelNotifacation object:nil];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"我的电影";
    [self.view addSubview:navBar];
    
    [self setUpTopBtnForArray:@[@"全部",@"待付款",@"待出票",@"待放映",@"已放映"]];
    
    [self setUpUI];
    
    /*100全部 0待付款 1待出票 2待放映 3已放映*/
    [AppUtils showProgressInView:self.view];
    [self dataRequestWithType:requestType];
}

-(void)upDateRequest{
    sourceArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    [self dataRequestWithType:requestType];
}

#pragma mark - setUp 筛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    pageInt=1;
    requestType=@"100";
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=64;
    for (int i=0; i<arr.count; i++) {
        
        NSString *name=arr[i];
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i==0?100:i==1?0:i==2?1:i==3?2:3;
        btn.frame=CGRectMake(i*btnWidth, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=[UIFont systemFontOfSize:16];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if (i==0) {
            lineX=btn.origin.x;
            seleFilterBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(lineX, btnY+btnHeihgt-3, btnWidth, 3.0f)];
    btLine.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [self.view addSubview:btLine];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 108) endPoint:CGPointMake(SCREENSIZE.width, 108)];
}

#pragma mark - 点击筛选按钮
-(void)clickFBtn:(id)sender{
    
    sourceArr=[NSMutableArray array];
    
    [seleFilterBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
    seleFilterBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    requestType=[NSString stringWithFormat:@"%ld",btn.tag];
    [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    //设置蓝色横条位置
    CGRect frame=btLine.frame;
    frame.origin.x=btn.origin.x;
    [UIView animateWithDuration:0.20 animations:^{
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        /*100全部 0待付款 1待出票 2待放映 3已放映*/
        [AppUtils showProgressInView:self.view];
        
        /*
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.mode=MBProgressHUDModeCustomView;
        [self.view addSubview:hud];
        UIImageView *imgv=[UIImageView new];
        imgv.image=[UIImage sd_animatedGIFNamed:@"loding"];
        hud.customView=imgv;
        hud.backgroundColor=[UIColor clearColor];
        [hud showAnimated:YES];*/
        
        //
        [self dataRequestWithType:requestType];
    }];
    
    //用一个button代替当前选择的
    seleFilterBtn=btn;
}

#pragma mark - init UI
-(void)setUpUI{
    sourceArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) style:UITableViewStyleGrouped];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor=[UIColor colorWithHexString:@"f0f0f0"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"FilmListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            sourceArr=[NSMutableArray array];
            [self dataRequestWithType:requestType];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    
    // 设置字体
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    myTable.mj_header = refleshHeader;
    
    ////////////////////
    //加载更多
    refleshFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self hadingFilmRefreshForPullUp];
    }];
    refleshFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    refleshFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [refleshFooter setTitle:@"" forState:MJRefreshStateIdle];
    [refleshFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [refleshFooter setTitle:@"没有更多" forState:MJRefreshStateNoMoreData];
    refleshFooter.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = refleshFooter;
}

#pragma mark - 加载更多
- (void)hadingFilmRefreshForPullUp{
    if (pageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            sourceArr=[NSMutableArray array];
            [self dataRequestWithType:requestType];
        });
    }
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sourceArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 125;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section==sourceArr.count-1) {
//        return 18;
//    }
    return 0.0000000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FilmListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[FilmListCell alloc]init];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row<sourceArr.count){
        FilmListModel *model=sourceArr[indexPath.section];
        [cell refluhsDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*100全部 0待付款 1待出票 2待放映 3已放映*/
    FilmListModel *model=sourceArr[indexPath.section];
//    TourSuccessController *ctr=[TourSuccessController new];
//    ctr.orderId=model.orderId;
//    [self.navigationController pushViewController:ctr animated:YES];
    
    
    switch (model.status.intValue) {
        case 0:
        {
            FilmConfirmOrder *ctr=[FilmConfirmOrder new];
            ctr.orderId=model.orderId;
            [self.navigationController pushViewController:ctr animated:YES];
        }
            break;
        case 1:
        {
            WaitTourController *ctr=[WaitTourController new];
            ctr.orderId=model.orderId;
            [self.navigationController pushViewController:ctr animated:YES];
        }
            break;
        case 6:
        {
            FilmTicketCloseController *ctr=[FilmTicketCloseController new];
            ctr.orderId=model.orderId;
            [self.navigationController pushViewController:ctr animated:YES];
        }
            break;
        default:
        {
            FilmTicketDetail *ctr=[FilmTicketDetail new];
            ctr.orderId=model.orderId;
            [self.navigationController pushViewController:ctr animated:YES];
        }
            break;
    }
}

#pragma mark - request
-(void)dataRequestWithType:(NSString *)type{
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"get_my_movie_order",
                         @"type":type,
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"page":[NSString stringWithFormat:@"%ld",pageInt]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@ -- type = %@",responseObject,type);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"][@"list"];
            if ([retObj isKindOfClass:[NSArray class]]) {
                NSArray *arr=retObj;
                for (NSDictionary *dic in arr) {
                    FilmListModel *model=[FilmListModel new];
                    [model jsonDataForDictionary:dic];
                    [sourceArr addObject:model];
                }
                
                NSString *totalPage=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"totalPage"]];
                if (pageInt<totalPage.integerValue) {
                    pageInt++;
                    [refleshFooter endRefreshing];
                }else{
                    [refleshFooter endRefreshingWithNoMoreData];
                }
                [myTable.mj_header endRefreshing];
                [myTable reloadData];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [myTable.mj_header endRefreshing];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
