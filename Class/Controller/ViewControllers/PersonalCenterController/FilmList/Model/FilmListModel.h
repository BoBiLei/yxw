//
//  FilmListModel.h
//  youxia
//
//  Created by mac on 2016/12/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmListModel : NSObject

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *movie_data;
@property (nonatomic, copy) NSString *movie_amount;
@property (nonatomic, copy) NSString *hallName;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, retain) NSArray *position;
@property (nonatomic, copy) NSString *playtime;
@property (nonatomic, copy) NSString *cinemaName;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
