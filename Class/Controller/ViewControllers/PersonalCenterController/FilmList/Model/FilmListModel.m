//
//  FilmListModel.m
//  youxia
//
//  Created by mac on 2016/12/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmListModel.h"

@implementation FilmListModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.orderId=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.movie_data=[NSString stringWithFormat:@"%@",dic[@"movie_data"]];
    self.movie_amount=[NSString stringWithFormat:@"%@",dic[@"movie_amount"]];
    self.hallName=[NSString stringWithFormat:@"%@",dic[@"hallName"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.position=dic[@"position"];
    self.playtime=[NSString stringWithFormat:@"%@",dic[@"playtime"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
}

@end
