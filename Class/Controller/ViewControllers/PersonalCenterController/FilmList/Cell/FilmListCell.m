//
//  FilmListCell.m
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmListCell.h"

@implementation FilmListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refluhsDataForModel:(FilmListModel *)model{
    self.titleLabel.text=model.movie_data;
    self.cinemaLabel.text=model.cinemaName;
    self.timeLabel.text=[NSString stringWithFormat:@"%@",model.playtime];
    switch (model.status.integerValue) {
        case 0:
            self.statusImgv.image=[UIImage imageNamed:@"filmlist_waitpay"];
            break;
        case 1:
            self.statusImgv.image=[UIImage imageNamed:@"filmlist_wait_chupiao"];
            break;
        case 2:
            self.statusImgv.image=[UIImage imageNamed:@"filmlist_wait_play"];
            break;
        case 3:
            self.statusImgv.image=[UIImage imageNamed:@"filmlist_han_play"];
            break;
        case 6:
            self.statusImgv.image=[UIImage imageNamed:@"filmlist_had_close"];
            break;
        default:
            break;
    }
    
    NSString *seatStr=@"";
    for (NSString *str in model.position) {
        NSString *itemStr=@"";
        NSArray *picarry=[str componentsSeparatedByString:@":"];
        for (int i=0; i<picarry.count; i++) {
            NSString *str=picarry[i];
            itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
        }
        seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
    }
    self.seatLbel.text=[NSString stringWithFormat:@"%@ %@",model.hallName,seatStr];
    
    self.priceLabel.text=[NSString stringWithFormat:@"总价 : %@元",model.movie_amount];
}

@end
