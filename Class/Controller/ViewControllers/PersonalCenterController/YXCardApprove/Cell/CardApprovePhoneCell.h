//
//  CardApprovePhoneCell.h
//  youxia
//
//  Created by mac on 2016/12/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardApproveModel.h"

@interface CardApprovePhoneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *rightLabel;
@property (weak, nonatomic) IBOutlet UIButton *validaBtn;

@property (nonatomic, copy) void (^CardApprovePhoneBlock)(NSString *text);

-(void)reflushDataForModel:(CardApproveModel *)model;

@end
