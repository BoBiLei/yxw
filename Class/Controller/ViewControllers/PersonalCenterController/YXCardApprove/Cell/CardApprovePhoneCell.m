//
//  CardApprovePhoneCell.m
//  youxia
//
//  Created by mac on 2016/12/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CardApprovePhoneCell.h"


@implementation CardApprovePhoneCell{
    NSInteger second;
    NSTimer *timer;
    
    CardApproveModel *receiveModel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_rightLabel addTarget:self action:@selector(goodsNameChange:) forControlEvents:UIControlEventEditingChanged];
    self.validaBtn.layer.cornerRadius=3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - textField
- (void)goodsNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.CardApprovePhoneBlock) {
        self.CardApprovePhoneBlock(field.text);
    }
}

-(void)reflushDataForModel:(CardApproveModel *)model{
    receiveModel=model;
    self.rightLabel.text=model.phone_validate;
}
- (IBAction)clickValidateBtn:(id)sender {
    [self getValidateRequest];
    second=60;
}

- (void)timeHeadle{
    second--;
    [_validaBtn setTitle:[NSString stringWithFormat:@"%ld秒重发",second] forState:UIControlStateNormal];
    if (second==0) {
        _validaBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        [_validaBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _validaBtn.enabled=YES;
        [timer invalidate];
        timer = nil;
    }
}

#pragma mark - Request

-(void)getValidateRequest{
    [AppUtils showActivityIndicatorView:_validaBtn];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"check_user_ios",
                         @"phone":receiveModel.mobile
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissActivityIndicatorView:_validaBtn];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            UIWindow *window = [UIApplication sharedApplication].keyWindow;
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:window];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _validaBtn.backgroundColor=[UIColor lightGrayColor];
                _validaBtn.enabled=NO;
                timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissActivityIndicatorView:_validaBtn];
        DSLog(@"%@",error);
    }];
}

@end
