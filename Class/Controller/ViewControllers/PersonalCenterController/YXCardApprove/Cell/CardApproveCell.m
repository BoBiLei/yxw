//
//  CardApproveCell.m
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CardApproveCell.h"

@implementation CardApproveCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect frame=self.frame;
        frame.size.width=SCREENSIZE.width;
        frame.size.height=48;
        self.frame=frame;
        
        _leftLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 9, 82, 30)];
        _leftLabel.font=[UIFont systemFontOfSize:16];
        [self addSubview:_leftLabel];
        
        _rightTextField=[[UITextField alloc] initWithFrame:CGRectMake(_leftLabel.origin.x+_leftLabel.width+8, _leftLabel.origin.y, self.width-(_leftLabel.origin.x*2+_leftLabel.width+8), _leftLabel.height)];
        _rightTextField.textColor=[UIColor colorWithHexString:@"949494"];
        _rightTextField.font=[UIFont systemFontOfSize:16];
        _rightTextField.placeholder=@"请输入中文姓名";
        _rightTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
        _rightTextField.tintColor=[UIColor colorWithHexString:@"55b2f0"];
        [_rightTextField addTarget:self action:@selector(goodsNameChange:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:_rightTextField];
    }
    return self;
}

-(void)reflushDataForDictionary:(CardApproveModel *)model indexPath:(NSIndexPath *)indexPath{
    NSString *leftStr;
    NSString *rightStr;
    switch (indexPath.row) {
        case 0:
            leftStr=@"卡类型";
            rightStr=model.cardtype;
            _rightTextField.keyboardType=UIKeyboardTypeDefault;
            [_rightTextField setEnabled:NO];
            break;
        case 1:
            leftStr=@"持卡人";
            rightStr=model.name;
            _rightTextField.keyboardType=UIKeyboardTypeDefault;
            [_rightTextField setEnabled:YES];
            break;
        case 2:
            leftStr=@"身份证";
            rightStr=model.id_entity;
            _rightTextField.keyboardType=UIKeyboardTypeDefault;
            [_rightTextField setEnabled:YES];
            break;
        case 3:
            leftStr=@"卡号";
            rightStr=model.code;
            _rightTextField.keyboardType=UIKeyboardTypeNumberPad;
            [_rightTextField setEnabled:YES];
            break;
        case 4:
            leftStr=@"预留手机号";
            rightStr=model.mobile;
            _rightTextField.keyboardType=UIKeyboardTypeNumberPad;
            [_rightTextField setEnabled:YES];
            break;
        default:
            break;
    }
    
    if (indexPath.row!=0||indexPath.row!=4) {
        if ([model.status isEqualToString:@"0"]||
            [model.status isEqualToString:@""]||
            [model.status isEqualToString:@"2"]){
            [_rightTextField setEnabled:YES];
        }else{
            if ([model.phone_status isEqualToString:@"1"]) {
                [_rightTextField setEnabled:NO];
            }else{
                [_rightTextField setEnabled:YES];
            }
        }
    }
    self.leftLabel.text=leftStr;
    self.rightTextField.placeholder=[NSString stringWithFormat:@"请输入%@",leftStr];
    self.rightTextField.text=rightStr;
}

#pragma mark - textField
- (void)goodsNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.CardApproveTextBlock) {
        self.CardApproveTextBlock(field.text);
    }
}

@end
