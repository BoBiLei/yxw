//
//  CardApproveCell.h
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardApproveModel.h"

@interface CardApproveCell : UITableViewCell

@property (strong, nonatomic) UILabel *leftLabel;
@property (strong, nonatomic) UITextField *rightTextField;

@property (nonatomic, copy) void (^CardApproveTextBlock)(NSString *text);

-(void)reflushDataForDictionary:(CardApproveModel *)model indexPath:(NSIndexPath *)indexPath;

@end
