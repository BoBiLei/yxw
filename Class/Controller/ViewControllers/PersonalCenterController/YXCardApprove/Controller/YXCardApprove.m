//
//  YXCardApprove.m
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YXCardApprove.h"
#import "CardApproveCell.h"
#import "CardApproveModel.h"
#import "CardApprovePhoneCell.h"

@interface YXCardApprove ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation YXCardApprove{
    CardApproveModel *sourceModel;
    UITableView *myTable;
    NSInteger rowNum;
    
    NSInteger subButtonType;   //0---绑定游侠信用卡  1---验证手机验证码
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"绑定游侠信用卡";
    [self.view addSubview:navBar];
    
    [self xykInfoRequest];
}

#pragma mark - init UI
-(void)setUpUI{
    
    sourceModel=[CardApproveModel new];
    sourceModel.cardtype=@"中国邮政储蓄银行 游侠旅行信用卡";
    sourceModel.name=@"";
    sourceModel.id_entity=@"";
    sourceModel.code=@"";
    sourceModel.mobile=@"";
    sourceModel.phone_validate=@"";
    
    rowNum=5;
    subButtonType=0;
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"CardApprovePhoneCell" bundle:nil] forCellReuseIdentifier:@"CardApprovePhoneCell"];
    [self.view addSubview:myTable];
    
    //
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 100)];
    myTable.tableFooterView=footView;
    
    UIButton *submitBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.layer.cornerRadius=3;
    submitBtn.frame=CGRectMake(10, 40, SCREENSIZE.width-20, 46);
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"提 交" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [footView addSubview:submitBtn];
    [submitBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - 点击提交
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    if (rowNum==5) {
        if ([sourceModel.name isEqualToString:@""]||sourceModel.name==nil) {
            [AppUtils showSuccessMessage:@"请输入持卡人姓名" inView:self.view];
        }else if ([sourceModel.id_entity isEqualToString:@""]||sourceModel.id_entity==nil){
            [AppUtils showSuccessMessage:@"请输入身份证号" inView:self.view];
        }else if ([sourceModel.code isEqualToString:@""]||sourceModel.code==nil){
            [AppUtils showSuccessMessage:@"请输入信用卡号" inView:self.view];
        }else if ([sourceModel.mobile isEqualToString:@""]||sourceModel.mobile==nil){
            [AppUtils showSuccessMessage:@"请输入预留手机号" inView:self.view];
        }else{
            if (![AppUtils checkPhoneNumber:sourceModel.mobile]) {
                [AppUtils showSuccessMessage:@"请输入正确手机号" inView:self.view];
            }else{
                if (subButtonType==0) {
                    [self submitFirstRequest];
                }else{
                    //验证
                }
            }
        }
    }else{
        if ([sourceModel.name isEqualToString:@""]||sourceModel.name==nil) {
            [AppUtils showSuccessMessage:@"请输入持卡人姓名" inView:self.view];
        }else if ([sourceModel.id_entity isEqualToString:@""]||sourceModel.id_entity==nil){
            [AppUtils showSuccessMessage:@"请输入身份证号" inView:self.view];
        }else if ([sourceModel.code isEqualToString:@""]||sourceModel.code==nil){
            [AppUtils showSuccessMessage:@"请输入信用卡号" inView:self.view];
        }else if ([sourceModel.mobile isEqualToString:@""]||sourceModel.mobile==nil){
            [AppUtils showSuccessMessage:@"请输入预留手机号" inView:self.view];
        }else if ([sourceModel.phone_validate isEqualToString:@""]||sourceModel.phone_validate==nil){
            [AppUtils showSuccessMessage:@"请输入手机验证码" inView:self.view];
        }else{
            if (![AppUtils checkPhoneNumber:sourceModel.mobile]) {
                [AppUtils showSuccessMessage:@"请输入正确手机号" inView:self.view];
            }else{
                if (subButtonType==0) {
                    //[self submitFirstRequest];
                }else{
                    //验证
                    [self submitPhoneValidateRequest];
                }
            }
        }
    }
}

#pragma mark - table delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return rowNum;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0000000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==5) {
        CardApprovePhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CardApprovePhoneCell"];
        if (cell==nil) {
            cell=[[CardApprovePhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardApprovePhoneCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.separatorInset=UIEdgeInsetsMake(0, 100, 0, 0);
        [cell reflushDataForModel:sourceModel];
        cell.CardApprovePhoneBlock=^(NSString *text){
            sourceModel.phone_validate=text;
        };
        return  cell;
    }else{
        CardApproveCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CardApproveCell"];
        if (cell==nil) {
            cell=[[CardApproveCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardApproveCell"];
        }
        cell.rightTextField.enabled=YES;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.separatorInset=UIEdgeInsetsMake(0, 100, 0, 0);
        [cell reflushDataForDictionary:sourceModel indexPath:indexPath];
        cell.CardApproveTextBlock=^(NSString *text){
            switch (indexPath.row) {
                case 0:
                    sourceModel.cardtype=text;
                    break;
                case 1:
                    sourceModel.name=text;
                    break;
                case 2:
                    sourceModel.id_entity=text;
                    break;
                case 3:
                    sourceModel.code=text;
                    break;
                case 4:
                    sourceModel.mobile=text;
                    break;
                default:
                    break;
            }
        };
        return  cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Request

-(void)xykInfoRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"get_bank_info",
                         @"userid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            [self setUpUI];
            
            id resObj=responseObject[@"retData"];
            if ([resObj isKindOfClass:[NSDictionary class]]) {
                [sourceModel jsonDataForDictioanry:resObj];
                if (([sourceModel.name isEqualToString:@""]&&
                     [sourceModel.id_entity isEqualToString:@""]&&
                     [sourceModel.code isEqualToString:@""]&&
                     [sourceModel.mobile isEqualToString:@""])||
                    (sourceModel.name==nil&&
                     sourceModel.id_entity==nil&&
                     sourceModel.code==nil&&
                     sourceModel.mobile==nil)) {
                        rowNum=5;
                        subButtonType=0;
                    }else if ([sourceModel.status isEqualToString:@"0"]||
                              [sourceModel.status isEqualToString:@""]||
                              [sourceModel.status isEqualToString:@"2"]){
                        rowNum=5;
                        subButtonType=0;
                    }else{
                        if ([sourceModel.phone_status isEqualToString:@"1"]) {
                            rowNum=5;
                            subButtonType=0;
                            myTable.tableFooterView=[UIView new];
                        }else{
                            rowNum=6;
                            subButtonType=1;
                        }
                    }
            }
            [myTable reloadData];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitFirstRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"do_yx_bank",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"yx_bank":sourceModel.code,
                         @"name":sourceModel.name,
                         @"idcard":sourceModel.id_entity,
                         @"phone":sourceModel.mobile
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            rowNum=6;
            subButtonType=1;
            [myTable reloadData];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)submitPhoneValidateRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"check_sms_code",
                         @"phone":sourceModel.mobile,
                         @"smscode":sourceModel.phone_validate,
                         @"userid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self xykInfoRequest];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
