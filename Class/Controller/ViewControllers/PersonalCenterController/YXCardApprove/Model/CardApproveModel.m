//
//  CardApproveModel.m
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CardApproveModel.h"

@implementation CardApproveModel

-(void)jsonDataForDictioanry:(NSDictionary *)dic{
    self.after_code=[NSString stringWithFormat:@"%@",dic[@"after_code"]];
    self.areainfo=[NSString stringWithFormat:@"%@",dic[@"areainfo"]];
    self.bank_id=[NSString stringWithFormat:@"%@",dic[@"bank_id"]];
    self.bankname=[NSString stringWithFormat:@"%@",dic[@"bankname"]];
    self.cardname=[NSString stringWithFormat:@"%@",dic[@"cardname"]];
    self.cardprefixnum=[NSString stringWithFormat:@"%@",dic[@"cardprefixnum"]];
//    self.cardtype=[NSString stringWithFormat:@"%@",dic[@"cardtype"]];
    self.cardtype=@"中国邮政储蓄银行 游侠旅行信用卡";
    self.code=[NSString stringWithFormat:@"%@",dic[@"code"]];
    self.cid=[NSString stringWithFormat:@"%@",dic[@"cid"]];
    self.id_entity=[NSString stringWithFormat:@"%@",dic[@"id_entity"]];
    self.mobile=[NSString stringWithFormat:@"%@",dic[@"mobile"]];
    self.name=[NSString stringWithFormat:@"%@",dic[@"name"]];
    self.phone_status=[NSString stringWithFormat:@"%@",dic[@"phone_status"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.submit_count=[NSString stringWithFormat:@"%@",dic[@"submit_count"]];
    self.time_stop=[NSString stringWithFormat:@"%@",dic[@"time_stop"]];
    self.uid=[NSString stringWithFormat:@"%@",dic[@"uid"]];
}

@end
