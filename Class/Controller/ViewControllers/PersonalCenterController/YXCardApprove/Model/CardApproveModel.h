//
//  CardApproveModel.h
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardApproveModel : NSObject

@property (nonatomic, copy) NSString *after_code;
@property (nonatomic, copy) NSString *areainfo;
@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *bankname;
@property (nonatomic, copy) NSString *cardname;
@property (nonatomic, copy) NSString *cardprefixnum;
@property (nonatomic, copy) NSString *cardtype;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *id_entity;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone_status;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *submit_count;
@property (nonatomic, copy) NSString *time_stop;
@property (nonatomic, copy) NSString *uid;

@property (nonatomic, copy) NSString *phone_validate;

@property (nonatomic, assign) BOOL isAgree;

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

@end
