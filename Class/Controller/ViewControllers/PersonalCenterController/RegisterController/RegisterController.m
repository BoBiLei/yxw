//
//  RegisterController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "RegisterController.h"
#import "PrivacyAgreementController.h"


#define RESETTIME 60

@interface RegisterController ()

@end

@implementation RegisterController{
    int timerCount;
    UITextField *phoneTf;
    UIButton *verifyBtn;
    NSString *sendCode;
    UITextField *verifyTf;
    UITextField *passTf;
    UITextField *cfPassTf;
    UITextField *tjcTf;         //推荐码
    UIButton *regiBtn;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"注册";
    [self.view addSubview:navBar];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    UIScrollView *mainScroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    mainScroll.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    mainScroll.userInteractionEnabled=YES;
    [self.view addSubview:mainScroll];
    
    //
    //手机号
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(30, 40, SCREENSIZE.width-60, 32)];
    phoneTf.placeholder=@"请输入手机号";
    phoneTf.font=kFontSize16;
    phoneTf.textColor=[UIColor whiteColor];
    [phoneTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTf.keyboardType=UIKeyboardTypeNumberPad;
    phoneTf.tintColor=[UIColor whiteColor];
    [phoneTf addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainScroll addSubview:phoneTf];
    
    UIView *line01=[[UIView alloc] initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height, phoneTf.width, 0.6f)];
    line01.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    [mainScroll addSubview:line01];
    
    
    //短信验证码
    
    CGFloat btnWidth=96;
    
    verifyTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, line01.origin.y+line01.height+22, phoneTf.width-btnWidth, phoneTf.height)];
    verifyTf.textColor=[UIColor whiteColor];
    [verifyTf addTarget:self action:@selector(verifyDidChange:) forControlEvents:UIControlEventEditingChanged];
    verifyTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    verifyTf.keyboardType=UIKeyboardTypeNumberPad;
    verifyTf.placeholder=@"短信验证码";
    verifyTf.font=kFontSize16;
    [verifyTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    verifyTf.tintColor=[UIColor whiteColor];
    [mainScroll addSubview:verifyTf];
    
    UIView *line02=[[UIView alloc] initWithFrame:CGRectMake(line01.origin.x, 127, line01.width, line01.height)];
    line02.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    [mainScroll addSubview:line02];
    
    
    //获取验证码按钮
    timerCount = RESETTIME;
    verifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    verifyBtn.frame=CGRectMake(line02.origin.x+line02.width-btnWidth, verifyTf.origin.y, btnWidth, verifyTf.height-3);
    verifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    verifyBtn.enabled=NO;
    verifyBtn.layer.cornerRadius=verifyBtn.height/2;
    [verifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verifyBtn setTitleColor:[UIColor colorWithHexString:@"#0080c5"] forState:UIControlStateNormal];
    verifyBtn.titleLabel.font=kFontSize14;
    [mainScroll addSubview:verifyBtn];
    [verifyBtn addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    
    
    //passVie
    //
    passTf=[[UITextField alloc]initWithFrame:CGRectMake(30, line02.origin.y+line02.height+22, SCREENSIZE.width-60, 32)];
    passTf.placeholder=@"请输入密码，至少6位";
    passTf.font=kFontSize16;
    [passTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    passTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    passTf.tintColor=[UIColor whiteColor];
    passTf.textColor=[UIColor whiteColor];
    passTf.secureTextEntry = YES;
    [passTf addTarget:self action:@selector(verifyDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainScroll addSubview:passTf];
    
    UIView *line03=[[UIView alloc] initWithFrame:CGRectMake(phoneTf.origin.x, passTf.origin.y+passTf.height, phoneTf.width, 0.6f)];
    line03.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    [mainScroll addSubview:line03];
    
    
    
    //confirmPassView
    cfPassTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, line03.origin.y+line03.height+24, phoneTf.width, phoneTf.height)];
    cfPassTf.placeholder=@"确认密码";
    cfPassTf.font=kFontSize16;
    cfPassTf.textColor=[UIColor whiteColor];
    [cfPassTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    cfPassTf.tag=1;
    cfPassTf.secureTextEntry=YES;
    cfPassTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    cfPassTf.tintColor=[UIColor whiteColor];
    [cfPassTf addTarget:self action:@selector(verifyDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainScroll addSubview:cfPassTf];
    
    UIView *line04=[[UIView alloc] initWithFrame:CGRectMake(passTf.origin.x, cfPassTf.origin.y+cfPassTf.height, cfPassTf.width, 0.5f)];
    line04.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    [mainScroll addSubview:line04];
    
    //tjView
    tjcTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, line04.origin.y+line04.height+24, phoneTf.width, phoneTf.height)];
    tjcTf.placeholder=@"推荐码（选填）";
    tjcTf.font=kFontSize16;
    tjcTf.textColor=[UIColor whiteColor];
    [tjcTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    tjcTf.tag=1;
    tjcTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    tjcTf.tintColor=[UIColor whiteColor];
    [mainScroll addSubview:tjcTf];
    
    UIView *line05=[[UIView alloc] initWithFrame:CGRectMake(tjcTf.origin.x, tjcTf.origin.y+tjcTf.height, tjcTf.width, 0.5f)];
    line05.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    [mainScroll addSubview:line05];
    
    
    
    //button
    regiBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    regiBtn.frame=CGRectMake(0, 0, SCREENSIZE.width/2+32, 44);
    regiBtn.center=CGPointMake(line05.center.x, line05.center.y+72);
    regiBtn.layer.borderWidth=0.7f;
    regiBtn.layer.borderColor=[UIColor colorWithHexString:@"#adcae2"].CGColor;
    regiBtn.layer.cornerRadius=regiBtn.height/2-2;
    regiBtn.enabled=NO;
    [regiBtn setTitle:@"同意协议并注册" forState:UIControlStateNormal];
    [regiBtn setTitleColor:[UIColor colorWithHexString:@"#adcae2"] forState:UIControlStateNormal];
    [regiBtn addTarget:self action:@selector(submitRegist) forControlEvents:UIControlEventTouchUpInside];
    [mainScroll addSubview:regiBtn];
    
    //同意协议
    NSString *sizStr=@"游侠旅行用户隐私协议";
    CGFloat fontSize=16;
    CGSize size=[AppUtils getStringSize:sizStr withFont:fontSize];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, regiBtn.origin.y+regiBtn.height+12, size.width, size.height)];
    label.center=CGPointMake(mainScroll.width/2, label.center.y);
    label.textColor = [UIColor colorWithHexString:@"#adcae2"];
    label.textAlignment=NSTextAlignmentCenter;
    label.font=[UIFont systemFontOfSize:fontSize];
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc]initWithString:sizStr];
    NSRange contentRange = {0,[content length]};
    [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    label.attributedText = content;
    label.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetail)];
    [label addGestureRecognizer:tap];
    [mainScroll addSubview:label];
}

#pragma mark - 点击游侠协议
-(void)tapDetail{
    PrivacyAgreementController *prAgreement=[[PrivacyAgreementController alloc] init];
    [self.navigationController pushViewController:prAgreement animated:YES];
}

#pragma mark - button event
-(void)getVerifyCode{
    [AppUtils closeKeyboard];
    [self isPhoneRegist:phoneTf.text];
}

-(void)submitRegist{
    [AppUtils closeKeyboard];
    if (![AppUtils checkPhoneNumber:phoneTf.text]) {
        [AppUtils showSuccessMessage:@"请输入正确的手机号" inView:self.view];
    }else{
        if ([AppUtils checkPassworkLength:passTf.text]) {
            if (![passTf.text isEqualToString:cfPassTf.text]) {
                [AppUtils showSuccessMessage:@"输入确认密码不一致" inView:self.view];
            }else{
                NSString *str=[AppUtils judgePasswordStrength:passTf.text];
                if ([str isEqualToString:@"0"]) {
                    [AppUtils showSuccessMessage:@"您输入的密码太过简单" inView:self.view];
                }else{
                    //提交注册
                    [self submitRegistRequest];
                }
            }
        }else{
            [AppUtils showSuccessMessage:@"至少输入6位密码" inView:self.view];
        }
    }
}

#pragma mark - Request 请求
//验证手机是否注册
-(void)isPhoneRegist:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"exists_iso" forKey:@"code"];
    [dict setObject:@"phone" forKey:@"field"];
    [dict setObject:phone forKey:@"value"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //可以注册-->获取验证码
            [self requestGetVerifyCodeForPhone:phone];
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        }else{
            //已经注册过
            [AppUtils showSuccessMessage:@"该号码已经注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//获取手机验证码
-(void)requestGetVerifyCodeForPhone:(NSString *)phone{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"getrandcode_iso" forKey:@"code"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        [AppUtils showSuccessMessage:@"已成功发送短信到你的手机" inView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            DSLog(@"%@",responseObject[@"retData"][@"sendcode"]);
            sendCode=responseObject[@"retData"][@"sendcode"];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"message"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//提交注册
-(void)submitRegistRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"register_iso" forKey:@"code"];
    [dict setObject:@"done" forKey:@"op"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:verifyTf.text forKey:@"reg_verify_code"];
    [dict setObject:passTf.text forKey:@"pwd"];
    [dict setObject:cfPassTf.text forKey:@"ckpwd"];
    [dict setObject:tjcTf.text forKey:@"ttcode"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            [TalkingData trackEvent:@"用户注册"];
            
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.navigationController.view];
            //注册成功保存用户名
            [AppUtils saveValue:phoneTf.text forKey:User_Account];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:RegistSuccessNotification object:nil];
            
            if (![tjcTf.text isEqualToString:@""]) {
                [self submitTTcodeRequest];
            }
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


-(void)submitTTcodeRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    //
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"tuijian_ios" forKey:@"code"];
    [dict setObject:phoneTf.text forKey:@"phone"];
    [dict setObject:tjcTf.text forKey:@"ttcode"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)setTimer:(NSTimer *)timer{
    if (timerCount<0){
        phoneTf.enabled=YES;
        [timer invalidate];
        [verifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        verifyBtn.backgroundColor=[UIColor whiteColor];
        verifyBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }else{
        phoneTf.enabled=NO;
        verifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        verifyBtn.enabled=NO;
        [verifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    }
    timerCount--;
}

#pragma mark - textfield
- (void)phoneDidChange:(id) sender {   
    if ([AppUtils checkPhoneNumber:phoneTf.text]&&
        ![phoneTf.text isEqualToString:@""]) {
        verifyBtn.backgroundColor=[UIColor whiteColor];
        verifyBtn.enabled=YES;
    }else{
        verifyBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
        verifyBtn.enabled=NO;
    }
}

- (void)verifyDidChange:(id) sender {
    
    if ((![verifyTf.text isEqualToString:@""])&&(![passTf.text isEqualToString:@""])&&(![cfPassTf.text isEqualToString:@""])) {
        regiBtn.layer.borderColor=[UIColor whiteColor].CGColor;
        [regiBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        regiBtn.enabled=YES;
    }else{
        regiBtn.layer.borderColor=[UIColor colorWithHexString:@"#adcae2"].CGColor;
        [regiBtn setTitleColor:[UIColor colorWithHexString:@"#adcae2"] forState:UIControlStateNormal];
        regiBtn.enabled=NO;
    }
}

@end
