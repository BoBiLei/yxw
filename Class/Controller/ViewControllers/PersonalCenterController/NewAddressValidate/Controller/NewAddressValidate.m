//
//  NewAddressValidate.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewAddressValidate.h"
#import "SelectCityView.h"

@interface NewAddressValidate ()

@property (nonatomic, strong) AddressValModel *model;

@end

@implementation NewAddressValidate{
    UITextField *cpnNameTF;
    UIView *cpnAddressView;
    UITextField *cpnAddressTF;
    UITextField *cpnDetailAdsTF;
    UITextField *cpnPhoneTF;
    
    UIView *homeAddressView;
    UITextField *homeAddressTF;
    UITextField *homeDetailAdsTF;
    UITextField *homePhoneTF;
    
    UIButton *subBtn;
    
    SelectCityView *companyCityPick;
    SelectCityView *homeCityPick;
    NSArray *cpnAdrArr;
    NSArray *homeAdrArr;
}

+(id)initWithModel:(AddressValModel *)model{
    NewAddressValidate *vc=[NewAddressValidate new];
    vc.model=model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpUI];
    
    cpnAdrArr=@[_model.cpnProvince,_model.cpnCity,_model.cpnArea];
    homeAdrArr=@[_model.homeProvince,_model.homeCity,_model.homeArea];
    
    companyCityPick=[[SelectCityView alloc]initPickerViewWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    homeCityPick=[[SelectCityView alloc]initPickerViewWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"通讯地址" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    UITableView *table=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-112)];
    [self.view addSubview:table];
    
    UIView *footView=[[UIView alloc] initWithFrame:table.frame];
    table.tableFooterView=footView;
    
    UILabel *tt01=[[UILabel alloc] initWithFrame:CGRectMake(16, 8, SCREENSIZE.width-32, 44)];
    tt01.font=kFontSize16;
    tt01.numberOfLines=0;
    tt01.text=@"您的通讯地址,请准确填写,我们将邮寄分期合同及发票给您。";
    [footView addSubview:tt01];
    
#pragma mark 单位名称
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, tt01.origin.y+tt01.height+8, 88, 42)];
    nameLabel.font=kFontSize16;
    nameLabel.text=@"单位名称:";
    [footView addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, footView.width-(nameLabel.origin.x*2+nameLabel.width+8), nameLabel.height)];
    nameView.layer.cornerRadius=4;
    nameView.layer.borderWidth=0.5f;
    nameView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    nameView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:nameView];
    
    cpnNameTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, nameView.width-10, nameView.height)];
    cpnNameTF.font=kFontSize15;
    cpnNameTF.placeholder=@"请填写单位名称";
    cpnNameTF.textColor=[UIColor colorWithHexString:@"70635a"];
    cpnNameTF.text=_model.cpnName;
    if ([_model.status isEqualToString:@"1"]) {
        cpnNameTF.enabled=NO;
    }else{
        cpnNameTF.enabled=YES;
    }
    cpnNameTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:cpnNameTF];
    
#pragma mark 单位地址
    UILabel *idCardLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, nameLabel.width, nameLabel.height)];
    idCardLabel.font=kFontSize16;
    idCardLabel.text=@"单位地址:";
    [footView addSubview:idCardLabel];
    
    cpnAddressView=[[UIView alloc] initWithFrame:CGRectMake(idCardLabel.origin.x+idCardLabel.width+8, idCardLabel.origin.y, nameView.width, nameView.height)];
    cpnAddressView.layer.cornerRadius=4;
    cpnAddressView.layer.borderWidth=0.5f;
    cpnAddressView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    cpnAddressView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:cpnAddressView];
    
    cpnAddressTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, cpnAddressView.width-10, cpnAddressView.height)];
    cpnAddressTF.font=kFontSize15;
    cpnAddressTF.placeholder=@"请填写单位地址";
    cpnAddressTF.textColor=[UIColor colorWithHexString:@"70635a"];
    NSString *cpnadrStr=[NSString stringWithFormat:@"%@%@%@",_model.cpnProvince,_model.cpnCity,_model.cpnArea];
    cpnAddressTF.text=cpnadrStr;
    cpnAddressTF.enabled=NO;
    cpnAddressTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [cpnAddressView addSubview:cpnAddressTF];
    
    UITapGestureRecognizer *tap01=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectCompanyAddress)];
    [cpnAddressView addGestureRecognizer:tap01];
    
#pragma mark 单位详细地址
    UIView *cpnAdsView=[[UIView alloc] initWithFrame:CGRectMake(idCardLabel.origin.x+idCardLabel.width+8, cpnAddressView.origin.y+cpnAddressView.height+12, nameView.width, nameView.height)];
    cpnAdsView.layer.cornerRadius=4;
    cpnAdsView.layer.borderWidth=0.5f;
    cpnAdsView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    cpnAdsView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:cpnAdsView];
    
    cpnDetailAdsTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, cpnAdsView.width-10, cpnAdsView.height)];
    cpnDetailAdsTF.font=kFontSize15;
    cpnDetailAdsTF.placeholder=@"请填写单位详细地址";
    cpnDetailAdsTF.textColor=[UIColor colorWithHexString:@"70635a"];
    cpnDetailAdsTF.text=_model.cpnAddress;
    if ([_model.status isEqualToString:@"1"]) {
        cpnDetailAdsTF.enabled=NO;
    }else{
        cpnDetailAdsTF.enabled=YES;
    }
    cpnDetailAdsTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [cpnAdsView addSubview:cpnDetailAdsTF];
    
#pragma mark 单位电话
    UILabel *bankLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, cpnAdsView.origin.y+cpnAdsView.height+12, nameLabel.width, nameLabel.height)];
    bankLabel.font=kFontSize16;
    bankLabel.text=@"单位电话:";
    [footView addSubview:bankLabel];
    
    UIView *bankView=[[UIView alloc] initWithFrame:CGRectMake(bankLabel.origin.x+bankLabel.width+8, bankLabel.origin.y, nameView.width, nameView.height)];
    bankView.layer.cornerRadius=4;
    bankView.layer.borderWidth=0.5f;
    bankView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    bankView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:bankView];
    
    cpnPhoneTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    cpnPhoneTF.font=kFontSize15;
    cpnPhoneTF.placeholder=@"请填写单位电话";
    cpnPhoneTF.textColor=[UIColor colorWithHexString:@"70635a"];
    cpnPhoneTF.text=_model.cpnPhone;
    if ([_model.status isEqualToString:@"1"]) {
        cpnPhoneTF.enabled=NO;
    }else{
        cpnPhoneTF.enabled=YES;
    }
    cpnPhoneTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [bankView addSubview:cpnPhoneTF];
    
#pragma mark 家庭地址
    UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, bankLabel.origin.y+bankLabel.height+28, nameLabel.width, nameLabel.height)];
    phoneLabel.font=kFontSize16;
    phoneLabel.text=@"家庭地址:";
    [footView addSubview:phoneLabel];
    
    homeAddressView=[[UIView alloc] initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, phoneLabel.origin.y, nameView.width, nameView.height)];
    homeAddressView.userInteractionEnabled=YES;
    homeAddressView.layer.cornerRadius=4;
    homeAddressView.layer.borderWidth=0.5f;
    homeAddressView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    homeAddressView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:homeAddressView];
    
    homeAddressTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    homeAddressTF.font=kFontSize15;
    homeAddressTF.placeholder=@"请填写家庭地址";
    homeAddressTF.textColor=[UIColor colorWithHexString:@"70635a"];
    NSString *homeadrStr=[NSString stringWithFormat:@"%@%@%@",_model.homeProvince,_model.homeCity,_model.homeArea];
    homeAddressTF.text=homeadrStr;
    homeAddressTF.enabled=NO;
    homeAddressTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [homeAddressView addSubview:homeAddressTF];
    
    UITapGestureRecognizer *tap02=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectHomeAddress)];
    [homeAddressView addGestureRecognizer:tap02];
    
#pragma mark 家庭详细地址
    UIView *comeAdsView=[[UIView alloc] initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, homeAddressView.origin.y+homeAddressView.height+12, nameView.width, nameView.height)];
    comeAdsView.layer.cornerRadius=4;
    comeAdsView.layer.borderWidth=0.5f;
    comeAdsView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    comeAdsView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:comeAdsView];
    
    homeDetailAdsTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    homeDetailAdsTF.font=kFontSize15;
    homeDetailAdsTF.placeholder=@"请填写家庭详细地址";
    homeDetailAdsTF.textColor=[UIColor colorWithHexString:@"70635a"];
    homeDetailAdsTF.text=_model.homeAddress;
    if ([_model.status isEqualToString:@"1"]) {
        homeDetailAdsTF.enabled=NO;
    }else{
        homeDetailAdsTF.enabled=YES;
    }
    homeDetailAdsTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [comeAdsView addSubview:homeDetailAdsTF];
    
#pragma mark 家庭电话
    UILabel *homePhoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, comeAdsView.origin.y+comeAdsView.height+12, nameLabel.width, nameLabel.height)];
    homePhoneLabel.font=kFontSize16;
    homePhoneLabel.text=@"家庭电话:";
    [footView addSubview:homePhoneLabel];
    
    UIView *homePhoneView=[[UIView alloc] initWithFrame:CGRectMake(homePhoneLabel.origin.x+homePhoneLabel.width+8, homePhoneLabel.origin.y, nameView.width, nameView.height)];
    homePhoneView.layer.cornerRadius=4;
    homePhoneView.layer.borderWidth=0.5f;
    homePhoneView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    homePhoneView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:homePhoneView];
    
    homePhoneTF=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    homePhoneTF.font=kFontSize15;
    homePhoneTF.placeholder=@"请填写家庭电话";
    homePhoneTF.textColor=[UIColor colorWithHexString:@"70635a"];
    homePhoneTF.text=_model.homePhone;
    if ([_model.status isEqualToString:@"1"]) {
        homePhoneTF.enabled=NO;
    }else{
        homePhoneTF.enabled=YES;
    }
    homePhoneTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [homePhoneView addSubview:homePhoneTF];
    
#pragma mark 提交按钮
    subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    subBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    subBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    subBtn.titleLabel.font=kFontSize17;
    [subBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:subBtn];
    
    if ([_model.status isEqualToString:@"1"]) {
        subBtn.hidden=YES;
        cpnAddressView.userInteractionEnabled=NO;
        homeAddressView.userInteractionEnabled=NO;
    }else{
        subBtn.hidden=NO;
        cpnAddressView.userInteractionEnabled=YES;
        homeAddressView.userInteractionEnabled=YES;
    }
}

-(void)selectCompanyAddress{
    [AppUtils closeKeyboard];
    [self.view addSubview:companyCityPick];
    [companyCityPick showPickerViewCompletion:^(id selectedObject) {
        cpnAdrArr=selectedObject;
        NSString *textStr=@"";
        for (NSString *str in cpnAdrArr) {
            textStr=[textStr stringByAppendingString:str];
        }
        cpnAddressTF.text=textStr;
    }];
}

-(void)selectHomeAddress{
    [AppUtils closeKeyboard];
    [self.view addSubview:homeCityPick];
    [homeCityPick showPickerViewCompletion:^(id selectedObject) {
        homeAdrArr=selectedObject;
        NSString *textStr=@"";
        for (NSString *str in homeAdrArr) {
            textStr=[textStr stringByAppendingString:str];
        }
        homeAddressTF.text=textStr;
    }];
}

-(void)clickSubBtn{
    if ([cpnNameTF.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"单位名称不能为空" inView:self.view];
    }else if ([cpnAddressTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"单位地址不能为空" inView:self.view];
    }else if ([cpnDetailAdsTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"单位详细地址不能为空" inView:self.view];
    }else if ([cpnPhoneTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"单位电话不能为空" inView:self.view];
    }else if ([homeAddressTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"家庭地址不能为空" inView:self.view];
    }else if ([homeDetailAdsTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"家庭详细地址不能为空" inView:self.view];
    }else if ([homePhoneTF.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"家庭电话不能为空" inView:self.view];
    }else{
        [self requestAddressForCompanyName:cpnNameTF.text cpnProvince:cpnAdrArr[0] cpnCity:cpnAdrArr[1] cpnArea:cpnAdrArr[2] cpnAddress:cpnDetailAdsTF.text cpnPhone:cpnPhoneTF.text homeProvince:homeAdrArr[0] homeCity:homeAdrArr[1] homeArea:homeAdrArr[2] homeAddress:homeDetailAdsTF.text homePhone:homePhoneTF.text];
    }
}

#pragma mark  提交 Request

-(void)requestAddressForCompanyName:(NSString *)companyName cpnProvince:(NSString *)cpnProvince cpnCity:(NSString *)cpnCity cpnArea:(NSString *)cpnArea cpnAddress:(NSString *)cpnAddress cpnPhone:(NSString *)cpnPhone homeProvince:(NSString *)homeProvince homeCity:(NSString *)homeCity homeArea:(NSString *)homeArea homeAddress:(NSString *)homeAddress homePhone:(NSString *)homePhone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_word_home_info_new" forKey:@"code"];
    [dict setObject:companyName forKey:@"work_company"];
    [dict setObject:cpnProvince forKey:@"SelectProvince3"];
    [dict setObject:cpnCity forKey:@"SelectCity3"];
    [dict setObject:cpnArea forKey:@"SelectDistrict3"];
    [dict setObject:cpnAddress forKey:@"work_address"];
    [dict setObject:cpnPhone forKey:@"work_phone"];
    
    [dict setObject:homeProvince forKey:@"SelectProvince"];
    [dict setObject:homeCity forKey:@"SelectCity"];
    [dict setObject:homeArea forKey:@"SelectDistrict"];
    [dict setObject:homeAddress forKey:@"person_address_1"];
    [dict setObject:homePhone forKey:@"home_phone"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            cpnNameTF.enabled=NO;
            cpnAddressView.userInteractionEnabled=NO;
            cpnDetailAdsTF.enabled=NO;
            cpnPhoneTF.enabled=NO;
            
            homeAddressView.userInteractionEnabled=NO;
            homeDetailAdsTF.enabled=NO;
            homePhoneTF.enabled=NO;
            
            subBtn.hidden=YES;
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
