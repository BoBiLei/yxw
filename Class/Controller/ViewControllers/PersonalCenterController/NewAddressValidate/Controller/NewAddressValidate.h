//
//  NewAddressValidate.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressValModel.h"

@interface NewAddressValidate : UIViewController

+(id)initWithModel:(AddressValModel *)model;

@end
