//
//  AddressValModel.m
//  youxia
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "AddressValModel.h"

@implementation AddressValModel

-(void)jsonToModel:(NSDictionary *)dic{
    if (dic[@"work_company"]&&![dic[@"work_company"] isKindOfClass:[NSNull class]]) {
        self.cpnName=dic[@"work_company"];
    }else{
        self.cpnName=@"";
    }
    
    if (dic[@"SelectProvince3"]&&![dic[@"SelectProvince3"] isKindOfClass:[NSNull class]]) {
        self.cpnProvince=dic[@"SelectProvince3"];
    }else{
        self.cpnProvince=@"";
    }
    
    if (dic[@"SelectCity3"]&&![dic[@"SelectCity3"] isKindOfClass:[NSNull class]]) {
        self.cpnCity=dic[@"SelectCity3"];
    }else{
        self.cpnCity=@"";
    }
    
    if (dic[@"SelectDistrict3"]&&![dic[@"SelectDistrict3"] isKindOfClass:[NSNull class]]) {
        self.cpnArea=dic[@"SelectDistrict3"];
    }else{
        self.cpnArea=@"";
    }
    
    if (dic[@"work_address"]&&![dic[@"work_address"] isKindOfClass:[NSNull class]]) {
        self.cpnAddress=dic[@"work_address"];
    }else{
        self.cpnAddress=@"";
    }
    
    if (dic[@"work_phone"]&&![dic[@"work_phone"] isKindOfClass:[NSNull class]]) {
        self.cpnPhone=dic[@"work_phone"];
    }else{
        self.cpnPhone=@"";
    }
    
    //家庭省
    if (dic[@"SelectProvince"]&&![dic[@"SelectProvince"] isKindOfClass:[NSNull class]]) {
        self.homeProvince=dic[@"SelectProvince"];
    }else{
        self.homeProvince=@"";
    }
    
    if (dic[@"SelectCity"]&&![dic[@"SelectCity"] isKindOfClass:[NSNull class]]) {
        self.homeCity=dic[@"SelectCity"];
    }else{
        self.homeCity=@"";
    }
    
    if (dic[@"SelectDistrict"]&&![dic[@"SelectDistrict"] isKindOfClass:[NSNull class]]) {
        self.homeArea=dic[@"SelectDistrict"];
    }else{
        self.homeArea=@"";
    }
    
    if (dic[@"person_address_1"]&&![dic[@"person_address_1"] isKindOfClass:[NSNull class]]) {
        self.homeAddress=dic[@"person_address_1"];
    }else{
        self.homeAddress=@"";
    }
    
    if (dic[@"home_phone"]&&![dic[@"home_phone"] isKindOfClass:[NSNull class]]) {
        self.homePhone=dic[@"home_phone"];
    }else{
        self.homePhone=@"";
    }
    
    
    if (dic[@"tx_status"]&&![dic[@"tx_status"] isKindOfClass:[NSNull class]]) {
        self.status=dic[@"tx_status"];
    }else{
        self.status=@"";
    }
}

@end
