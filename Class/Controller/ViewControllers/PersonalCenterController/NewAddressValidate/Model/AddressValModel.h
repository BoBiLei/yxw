//
//  AddressValModel.h
//  youxia
//
//  Created by mac on 2017/4/18.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressValModel : NSObject

/*SelectCity = "\U5317\U4eac\U8f96\U533a";
 SelectCity3 = "\U5317\U4eac\U8f96\U533a";
 SelectDistrict = "\U4e1c\U57ce\U533a";
 SelectDistrict3 = "\U4e1c\U57ce\U533a";
 SelectProvince = "\U5317\U4eac\U5e02";
 SelectProvince3 = "\U5317\U4eac\U5e02";
 "home_phone" = 15678065811;
 "home_sjname" = "";
 "person_address_1" = www;
 "tx_status" = 0;
 "work_address" = "\U6211";
 "work_company" = "\U5728\U5bb6";
 "work_phone" = "";
 "work_sjname" = "\U65e0";*/
@property (nonatomic, copy) NSString *cpnName;      //单位名称
@property (nonatomic, copy) NSString *cpnProvince;
@property (nonatomic, copy) NSString *cpnCity;
@property (nonatomic, copy) NSString *cpnArea;
@property (nonatomic, copy) NSString *cpnAddress;   //详细地址
@property (nonatomic, copy) NSString *cpnPhone;     //单位电话

@property (nonatomic, copy) NSString *homeProvince;
@property (nonatomic, copy) NSString *homeCity;
@property (nonatomic, copy) NSString *homeArea;
@property (nonatomic, copy) NSString *homeAddress;   //详细地址
@property (nonatomic, copy) NSString *homePhone;    //家庭电话

@property (nonatomic, copy) NSString *status;           //总的status

-(void)jsonToModel:(NSDictionary *)dic;

@end
