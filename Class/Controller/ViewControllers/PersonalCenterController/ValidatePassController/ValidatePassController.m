//
//  ValidatePassController.m
//  youxia
//
//  Created by mac on 16/7/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ValidatePassController.h"
#import "ValidatePhoneController.h"

@interface ValidatePassController ()

@end

@implementation ValidatePassController{
    UITextField *passTf;
    UIButton *bindingBtn;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"验证账号密码";
    [self.view addSubview:navBar];
    
    //
    [self setUpTipView];
    
    //
    [self setUpScrollerView];
}

-(void)setUpTipView{
    UIImageView *tipView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 64)];
    tipView.image=[UIImage imageNamed:@"tipview_img"];
    [self.view addSubview:tipView];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(40, 0, tipView.width-80, tipView.height)];
    tipLabel.font=[UIFont systemFontOfSize:14];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.text=[NSString stringWithFormat:@"更换手机后，下次登录可使用新手机号登录。\n当前手机号：\n%@",[AppUtils getValueWithKey:User_Phone]];
    tipLabel.textColor=[UIColor colorWithHexString:@"#8b8b8b"];
    tipLabel.numberOfLines=0;
    [tipView addSubview:tipLabel];
}

-(void)setUpScrollerView{
    UIScrollView *scroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 128, SCREENSIZE.width, SCREENSIZE.height-128)];
    [self.view addSubview:scroller];
    
    //
    passTf=[[UITextField alloc]initWithFrame:CGRectMake(26,22, SCREENSIZE.width-52, 32)];
    passTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [passTf addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
    passTf.placeholder=@"请输入当前账号密码";
    passTf.secureTextEntry=YES;
    passTf.font=[UIFont systemFontOfSize:16];
    [passTf setValue:[UIColor colorWithHexString:@"#757575"] forKeyPath:@"_placeholderLabel.textColor"];
    passTf.tintColor=[UIColor colorWithHexString:@"#0080c5"];
    [scroller addSubview:passTf];
    
    [AppUtils drawZhiXian:scroller withColor:[UIColor colorWithHexString:@"#d9d9d9"] height:0.8f firstPoint:CGPointMake(passTf.origin.x, passTf.origin.y+passTf.height+1) endPoint:CGPointMake(passTf.origin.x+passTf.width, passTf.origin.y+passTf.height+1)];
    
    //button
    bindingBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    bindingBtn.frame=CGRectMake(passTf.origin.x, passTf.origin.y+passTf.height+24, SCREENSIZE.width-passTf.origin.x*2, 42);
    bindingBtn.layer.cornerRadius=bindingBtn.height/2;
    bindingBtn.enabled=NO;
    [bindingBtn setTitle:@"验证密码" forState:UIControlStateNormal];
    [bindingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bindingBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [bindingBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:bindingBtn];
}

#pragma mark - text值改变时
-(void)phoneTextDidChange{
    if (![passTf.text isEqualToString:@""]) {
        bindingBtn.enabled=YES;
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    }else{
        bindingBtn.enabled=NO;
        bindingBtn.backgroundColor=[UIColor colorWithHexString:@"#adcae2"];
    }
}

#pragma mark - 点击提交按钮
-(void)clickSubmitBtn{
    [AppUtils closeKeyboard];
    [self submitRequest];
}

#pragma mark - Request验证当前账号密码

-(void)submitRequest{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"check_user_password_ios" forKey:@"code"];
    [dict setObject:passTf.text forKey:@"password"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [self.navigationController pushViewController:[ValidatePhoneController new] animated:YES];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
