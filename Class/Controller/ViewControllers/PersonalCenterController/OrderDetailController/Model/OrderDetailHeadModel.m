//
//  OrderDetailHeadModel.m
//  youxia
//
//  Created by mac on 15/12/28.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderDetailHeadModel.h"

@implementation OrderDetailHeadModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.orderId=dic[@"order"][@"orderid"];
    
    //
    self.orderName=dic[@"order"][@"product"][@"name"];
    NSMutableString *nameStr=[NSMutableString stringWithString:_orderName];
    NSRange nameRang=[nameStr rangeOfString:@"&amp;"];
    while (nameRang.location!=NSNotFound) {
        [nameStr replaceCharactersInRange:nameRang withString:@" "];
        nameRang=[nameStr rangeOfString:@"&amp;"];
    }
    self.orderName=nameStr;
    
    //
    self.orderRoom=dic[@"order_option"][@"rooms"];
    NSMutableString *roomStr=[NSMutableString stringWithString:_orderRoom];
    NSRange roomRang=[roomStr rangeOfString:@"&nbsp;"];
    while (roomRang.location!=NSNotFound) {
        [roomStr replaceCharactersInRange:roomRang withString:@" "];
        roomRang=[roomStr rangeOfString:@"&nbsp;"];
    }
    self.orderRoom=roomStr;
    
    //
    self.orderFlight=dic[@"order_option"][@"flight"];
    self.goTime=dic[@"start_date"];
    self.goPlace=dic[@"start_city"];
    self.orderTime=dic[@"order"][@"buytime"];
}

@end
