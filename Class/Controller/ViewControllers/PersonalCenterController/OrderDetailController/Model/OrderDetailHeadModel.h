//
//  OrderDetailHeadModel.h
//  youxia
//
//  Created by mac on 15/12/28.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailHeadModel : NSObject

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *orderName;
@property (nonatomic, copy) NSString *orderRoom;
@property (nonatomic, copy) NSString *orderFlight;
@property (nonatomic, copy) NSString *goTime;
@property (nonatomic, copy) NSString *goPlace;
@property (nonatomic, copy) NSString *orderTime;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
