//
//  OrderDetailFootModel.m
//  youxia
//
//  Created by mac on 15/12/28.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderDetailFootModel.h"

@implementation OrderDetailFootModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.stageNum=dic[@"qishu"];
    self.time=dic[@"time"];
    self.price=dic[@"jine"];
    self.status=dic[@"status"];
}

@end
