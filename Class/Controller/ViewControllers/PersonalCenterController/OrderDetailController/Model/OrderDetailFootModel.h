//
//  OrderDetailFootModel.h
//  youxia
//
//  Created by mac on 15/12/28.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailFootModel : NSObject

@property (nonatomic, copy) NSString *stageNum;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *status;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
