//
//  OrderDetailController.m
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//
/*
    1机票
    2酒店
    3分期合同
    3协同合同
 */

#import "OrderDetailController.h"
#import "OrderDetailHeaderCell.h"
#import "OrderDetailFooterCell.h"
#import "YHTTokenManager.h"
#import "YHTContractContentViewController.h"

NSString *headerIdentifier=@"headerIdentifier";
NSString *footerIdentifier=@"footerIdentifier";

@interface OrderDetailController ()<UITableViewDataSource,UITableViewDelegate,OrderDetailQianYueDelegate>

@end

@implementation OrderDetailController{
    
    BOOL isHanHT;       //是否有合同
    
    NSString *htToken;  //token
    
    //合同ID
    NSNumber *jphtId;
    NSNumber *jdhtId;
    NSNumber *fqhtId;
    NSNumber *xthtId;       //协同合同
    
    //合同状态
    NSString *jpStatus;
    NSString *jdStatus;
    NSString *fqStatus;
    NSString *xtStatus;     //协同状态
    
    NSMutableArray *headArr;
    NSMutableArray *footArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"订单详情页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"订单详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateJPStatus) name:@"JP_QianShu_Success" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateJDStatus) name:@"JD_QianShu_Success" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFQStatus) name:@"FQ_QianShu_Success" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateXTStatus) name:@"XT_QianShu_Success" object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"订单详情";
    [self.view addSubview:navBar];
    
    [self setUpUI];
    
    [self requestHttpsForOrderDetail:_orderId];
    
    [self htRequest:_orderId];
}

#pragma mark - Notification

-(void)updateJPStatus{
    jpStatus=@"1";
    [myTable reloadData];
}

-(void)updateJDStatus{
    jdStatus=@"1";
    [myTable reloadData];
}

-(void)updateFQStatus{
    fqStatus=@"1";
    [myTable reloadData];
}

-(void)updateXTStatus{
    xtStatus=@"1";
    [myTable reloadData];
}

#pragma mark - init view
-(void)setUpUI{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.hidden=YES;
    myTable.sectionHeaderHeight = 50;
    [myTable registerNib:[UINib nibWithNibName:@"OrderDetailHeaderCell" bundle:nil] forCellReuseIdentifier:headerIdentifier];
    [myTable registerNib:[UINib nibWithNibName:@"OrderDetailFooterCell" bundle:nil] forCellReuseIdentifier:footerIdentifier];
    [self.view addSubview:myTable];
    myTable.backgroundColor=[UIColor groupTableViewBackgroundColor];
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return headArr.count;
    }else{
        return footArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderDetailHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:headerIdentifier];
        if (!cell) {
            cell=[[OrderDetailHeaderCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderDetailHeadModel *model=headArr[indexPath.row];
        [cell reflushDataForModel:model type:_islandType isHadHt:isHanHT jpId:jphtId dId:jdhtId fqId:fqhtId jpStatus:jpStatus jdStatus:jdStatus fqStatus:fqStatus xtId:xthtId xtStatus:xtStatus];
        return cell.frame.size.height;
    }else{
        return 36;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderDetailHeaderCell *cell=(OrderDetailHeaderCell *)[tableView dequeueReusableCellWithIdentifier:headerIdentifier forIndexPath:indexPath];
        if (!cell) {
            cell=[[OrderDetailHeaderCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        while ([cell.subviews lastObject] != nil) {
            [(UIView*)[cell.subviews lastObject] removeFromSuperview];
        }
        cell.delegate=self;
        OrderDetailHeadModel *model=headArr[indexPath.row];
        [cell reflushDataForModel:model type:_islandType isHadHt:isHanHT jpId:jphtId dId:jdhtId fqId:fqhtId jpStatus:jpStatus jdStatus:jdStatus fqStatus:fqStatus xtId:xthtId xtStatus:xtStatus];
        return  cell;
    }else{
        OrderDetailFooterCell *cell=[tableView dequeueReusableCellWithIdentifier:footerIdentifier];
        if (!cell) {
            cell=[[OrderDetailFooterCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderDetailFootModel *model=footArr[indexPath.row];
        [cell reflushDataForModel:model];
        return  cell;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1) {
        return 79;
    }else{
        return 40;
    }
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    CGFloat labelWidth=(SCREENSIZE.width-2)/3;
    
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 48)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    CGRect frame=headerSectionView.frame;
    frame.size.height=79;
    
    //订单编号
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width/2+20, headerSectionView.frame.size.height-8)];
    titleLabel.font=kFontSize15;
    [headerSectionView addSubview:titleLabel];
    
    if (section==0){
        titleLabel.text=@"订单信息";
    }
    else{
        titleLabel.text=@"扣款详情";
        headerSectionView.frame=frame;
        //top line
        UIView *line=[[UIView alloc]init];
        line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [headerSectionView addSubview:line];
        line.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)];
        [NSLayoutConstraint activateConstraints:line_h];
        
        NSArray* line_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel][line(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLabel,line)];
        [NSLayoutConstraint activateConstraints:line_w];
        
        //白色view
        UIView *mView=[[UIView alloc]init];
        mView.backgroundColor=[UIColor whiteColor];
        [headerSectionView addSubview:mView];
        mView.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* mView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(mView)];
        [NSLayoutConstraint activateConstraints:mView_h];
        
        NSArray* mView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[line][mView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line,mView)];
        [NSLayoutConstraint activateConstraints:mView_w];
        
        //期数
        UILabel *stageLabel=[[UILabel alloc]init];
        stageLabel.text=@"期 数";
        stageLabel.font=kFontSize14;
        stageLabel.textAlignment=NSTextAlignmentCenter;
        stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:stageLabel];
        stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSString *str01=[NSString stringWithFormat:@"H:|[stageLabel(%.f)]",labelWidth];
        NSArray* stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:str01 options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
        [NSLayoutConstraint activateConstraints:stageLabel_h];
        
        NSArray* stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stageLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel)];
        [NSLayoutConstraint activateConstraints:stageLabel_w];
        
        //line01
        UIView *line01=[[UIView alloc]init];
        line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [mView addSubview:line01];
        line01.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[stageLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(stageLabel,line01)];
        [NSLayoutConstraint activateConstraints:line01_h];
        
        NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
        [NSLayoutConstraint activateConstraints:line01_w];
        
        //扣款日期
        UILabel *cutPayTimeLabel=[[UILabel alloc]init];
        cutPayTimeLabel.text=@"扣款日期";
        cutPayTimeLabel.font=kFontSize14;
        cutPayTimeLabel.textAlignment=NSTextAlignmentCenter;
        cutPayTimeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:cutPayTimeLabel];
        cutPayTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSString *str02=[NSString stringWithFormat:@"H:[line01][cutPayTimeLabel(%.f)]",labelWidth];
        NSArray* cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:str02 options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,cutPayTimeLabel)];
        [NSLayoutConstraint activateConstraints:cutPayTimeLabel_h];
        
        NSArray* cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cutPayTimeLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel)];
        [NSLayoutConstraint activateConstraints:cutPayTimeLabel_w];
        
        //line02
        UIView *line02=[[UIView alloc]init];
        line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
        [mView addSubview:line02];
        line02.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cutPayTimeLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cutPayTimeLabel,line02)];
        [NSLayoutConstraint activateConstraints:line02_h];
        
        NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
        [NSLayoutConstraint activateConstraints:line02_w];
        
        //金额
        UILabel *priceLabel=[[UILabel alloc]init];
        priceLabel.text=@"金 额";
        priceLabel.font=kFontSize14;
        priceLabel.textAlignment=NSTextAlignmentCenter;
        priceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        [mView addSubview:priceLabel];
        priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray* priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line02][priceLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,priceLabel)];
        [NSLayoutConstraint activateConstraints:priceLabel_h];
        
        NSArray* priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[priceLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(priceLabel)];
        [NSLayoutConstraint activateConstraints:priceLabel_w];
    }
    return headerSectionView;
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 12;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - 签约 Delegate
//机票
-(void)jphtQianyue{
    [self requestNewToken:_orderId contactType:@"1"];
}
//酒店
-(void)jdhtQianyue{
    [self requestNewToken:_orderId contactType:@"2"];
}
//分期
-(void)fqhtQianyue{
    [self requestNewToken:_orderId contactType:@"3"];
}
//协同
-(void)xtQianyue{
    [self requestNewToken:_orderId contactType:@"4"];
}

#pragma mark - Request 请求
-(void)requestHttpsForOrderDetail:(NSString *)orderId{
    
    [AppUtils showProgressInView:self.view];
    
    headArr=[NSMutableArray array];
    footArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
//    [dict setObject:@"getOrderDetail_iso" forKey:@"code"];
    [dict setObject:_isXieTong?@"GetOrderDetail_xie_iso":@"getOrderDetail_iso" forKey:@"code"];
    [dict setObject:orderId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderDetail:self.orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                myTable.hidden=NO;
                //订单详情
                OrderDetailHeadModel *model=[[OrderDetailHeadModel alloc]init];
                [model jsonDataForDictionary:responseObject[@"retData"]];
                [headArr addObject:model];
                
                //扣款详情
                NSArray *stArr=responseObject[@"retData"][@"olist"];
                for (NSDictionary *stageDic in stArr) {
                    OrderDetailFootModel *model=[[OrderDetailFootModel alloc]init];
                    [model jsonDataForDictionary:stageDic];
                    [footArr addObject:model];
                }
            }else{
                NSLog(@"%@",responseObject[@""]);
            }
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)htRequest:(NSString *)orderId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"hetong" forKey:@"mod"];
//    [dict setObject:@"get_ht_list" forKey:@"code"];
    [dict setObject:_isXieTong?@"get_ht_list_xie":@"get_ht_list" forKey:@"code"];
    [dict setObject:orderId forKey:@"yxid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@--",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self htRequest:self.orderId];
            });
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"error"]];
            if ([codeStr isEqualToString:@"0"]) {
                isHanHT=YES;
                htToken=[NSString stringWithFormat:@"%@",responseObject[@"token"]];
                [[YHTTokenManager sharedManager] setTokenWithString:htToken];
                
                NSString *jpStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_flight"]];
                NSString *jdStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_hotel"]];
                NSString *fqStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_fq"]];
                NSString *xtStr=[NSString stringWithFormat:@"%@",responseObject[@"buzhong_ht"]];
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                jphtId=[numberFormatter numberFromString:jpStr];
                jdhtId=[numberFormatter numberFromString:jdStr];
                fqhtId=[numberFormatter numberFromString:fqStr];
                xthtId=[numberFormatter numberFromString:xtStr];
                
                NSString *jpStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"flight_ht_status"]];
                NSString *jdStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"hotel_ht_status"]];
                NSString *fqStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"yx_ht_status"]];
                NSString *xtStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"buzhong_ht_status"]];
                jpStatus=jpStatusStr;
                jdStatus=jdStatusStr;
                fqStatus=fqStatusStr;
                xtStatus=xtStatusStr;
            }else{
                isHanHT=NO;
            }
            [myTable reloadData];
        }
        
        DSLog(@"\n合同token=%@",htToken);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)requestNewToken:(NSString *)orderId contactType:(NSString *)type{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"hetong" forKey:@"mod"];
//    [dict setObject:@"get_ht_list" forKey:@"code"];
    [dict setObject:_isXieTong?@"get_ht_list_xie":@"get_ht_list" forKey:@"code"];
    [dict setObject:orderId forKey:@"yxid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@--",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self htRequest:self.orderId];
            });
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"error"]];
            if ([codeStr isEqualToString:@"0"]) {
                isHanHT=YES;
                htToken=[NSString stringWithFormat:@"%@",responseObject[@"token"]];
                [[YHTTokenManager sharedManager] setTokenWithString:htToken];
                
                NSString *jpStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_flight"]];
                NSString *jdStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_hotel"]];
                NSString *fqStr=[NSString stringWithFormat:@"%@",responseObject[@"ht_fq"]];
                NSString *xtStr=[NSString stringWithFormat:@"%@",responseObject[@"buzhong_ht"]];
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                jphtId=[numberFormatter numberFromString:jpStr];
                jdhtId=[numberFormatter numberFromString:jdStr];
                fqhtId=[numberFormatter numberFromString:fqStr];
                xthtId=[numberFormatter numberFromString:xtStr];
                
                NSString *jpStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"flight_ht_status"]];
                NSString *jdStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"hotel_ht_status"]];
                NSString *fqStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"yx_ht_status"]];
                NSString *xtStatusStr=[NSString stringWithFormat:@"%@",responseObject[@"buzhong_ht_status"]];
                jpStatus=jpStatusStr;
                jdStatus=jdStatusStr;
                fqStatus=fqStatusStr;
                xtStatus=xtStatusStr;
            }else{
                isHanHT=NO;
            }
            [myTable reloadData];
            if([type isEqualToString:@"1"]){
                [self.navigationController pushViewController:[YHTContractContentViewController instanceWithContractID:jphtId contractType:type orderId:self.orderId isXieTong:_isXieTong] animated:YES];
            }else if ([type isEqualToString:@"2"]){
                [self.navigationController pushViewController:[YHTContractContentViewController instanceWithContractID:jdhtId contractType:type orderId:self.orderId isXieTong:_isXieTong] animated:YES];
            }else if ([type isEqualToString:@"3"]){
                [self.navigationController pushViewController:[YHTContractContentViewController instanceWithContractID:fqhtId contractType:type orderId:self.orderId isXieTong:_isXieTong] animated:YES];
            }else if ([type isEqualToString:@"4"]){
                [self.navigationController pushViewController:[YHTContractContentViewController instanceWithContractID:xthtId contractType:type orderId:self.orderId isXieTong:_isXieTong] animated:YES];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
