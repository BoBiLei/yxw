//
//  OrderDetailController.h
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

@interface OrderDetailController : UIViewController

@property (nonatomic) IslandType islandType;
@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, assign) BOOL isXieTong;

@end
