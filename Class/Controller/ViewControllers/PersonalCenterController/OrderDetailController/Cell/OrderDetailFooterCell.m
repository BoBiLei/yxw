//
//  OrderDetailFooterCell.m
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderDetailFooterCell.h"

#define TextFont 14
@implementation OrderDetailFooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat labelWidth=(SCREENSIZE.width-2)/3;
    
    //期数
    _stageLabel=[[UILabel alloc]init];
    _stageLabel.font=[UIFont systemFontOfSize:TextFont];
    _stageLabel.textAlignment=NSTextAlignmentCenter;
    _stageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_stageLabel];
    _stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *str01=[NSString stringWithFormat:@"H:|[_stageLabel(%.f)]",labelWidth];
    NSArray* _stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:str01 options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_h];
    
    NSArray* _stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_stageLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_w];
    
    //line01
    UIView *line01=[[UIView alloc]init];
    line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line01];
    line01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line01_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_stageLabel][line01(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel,line01)];
    [NSLayoutConstraint activateConstraints:line01_h];
    
    NSArray* line01_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01)];
    [NSLayoutConstraint activateConstraints:line01_w];
    
    //扣款日期
    _cutPayTimeLabel=[[UILabel alloc]init];
    _cutPayTimeLabel.font=[UIFont systemFontOfSize:TextFont];
    _cutPayTimeLabel.textAlignment=NSTextAlignmentCenter;
    _cutPayTimeLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_cutPayTimeLabel];
    _cutPayTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *str02=[NSString stringWithFormat:@"H:[line01][_cutPayTimeLabel(%.f)]",labelWidth];
    NSArray* _cutPayTimeLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:str02 options:0 metrics:nil views:NSDictionaryOfVariableBindings(line01,_cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_h];
    
    NSArray* _cutPayTimeLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_cutPayTimeLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_cutPayTimeLabel)];
    [NSLayoutConstraint activateConstraints:_cutPayTimeLabel_w];
    
    //line02
    UIView *line02=[[UIView alloc]init];
    line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line02];
    line02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* line02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_cutPayTimeLabel][line02(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_cutPayTimeLabel,line02)];
    [NSLayoutConstraint activateConstraints:line02_h];
    
    NSArray* line02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02)];
    [NSLayoutConstraint activateConstraints:line02_w];
    
    //金额
    _priceLabel=[[UILabel alloc]init];
    _priceLabel.font=[UIFont systemFontOfSize:TextFont];
    _priceLabel.textAlignment=NSTextAlignmentCenter;
    _priceLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    [self addSubview:_priceLabel];
    _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* _priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[line02][_priceLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line02,_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_h];
    
    NSArray* _priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_priceLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_w];
}


-(void)reflushDataForModel:(OrderDetailFootModel *)model{
    self.stageLabel.text=[NSString stringWithFormat:@"%@期",model.stageNum];
    
    NSString *priceStr=[NSString stringWithFormat:@"%@",model.price];
    CGFloat priceFloat=priceStr.floatValue;
    _priceLabel.text=[NSString stringWithFormat:@"%.f元",priceFloat];
    NSString *statuStr=[NSString stringWithFormat:@"%@",model.status];
    if ([statuStr isEqualToString:@"1"]) {
        self.cutPayTimeLabel.text=model.time;
    }else{
        self.cutPayTimeLabel.text=@"---";
    }
}

@end
