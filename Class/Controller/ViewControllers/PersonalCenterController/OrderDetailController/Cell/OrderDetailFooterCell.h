//
//  OrderDetailFooterCell.h
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailFootModel.h"

@interface OrderDetailFooterCell : UITableViewCell

@property (nonatomic ,strong) UILabel *stageLabel;
@property (nonatomic ,strong) UILabel *cutPayTimeLabel;//扣款如期
@property (nonatomic ,strong) UILabel *priceLabel;//金额
@property (nonatomic ,strong) UILabel *statusLabel;//状态

-(void)reflushDataForModel:(OrderDetailFootModel *)model;

@end
