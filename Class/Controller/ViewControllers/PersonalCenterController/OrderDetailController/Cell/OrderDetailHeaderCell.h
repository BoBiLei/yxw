//
//  OrderDetailHeaderCell.h
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailHeadModel.h"

@protocol OrderDetailQianYueDelegate <NSObject>

/*
 *机票合同签约
 */
-(void)jphtQianyue;

/*
 *酒店合同签约
 */
-(void)jdhtQianyue;

/*
 *分期合同签约
 */
-(void)fqhtQianyue;

/*
 *协同合同签约
 */
-(void)xtQianyue;

@end

@interface OrderDetailHeaderCell : UITableViewCell

@property (nonatomic,weak) id<OrderDetailQianYueDelegate> delegate;

-(void)reflushDataForModel:(OrderDetailHeadModel *)model type:(IslandType)type isHadHt:(BOOL)isHadHt jpId:(NSNumber *)jpId dId:(NSNumber *)jdId fqId:(NSNumber *)fqId jpStatus:(NSString *)jpStatus jdStatus:(NSString *)jdStatus fqStatus:(NSString *)fqStatus xtId:(NSNumber *)xietId xtStatus:(NSString *)xtStatus;

@end
