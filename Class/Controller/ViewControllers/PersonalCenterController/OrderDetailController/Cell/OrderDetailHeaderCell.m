//
//  OrderDetailHeaderCell.m
//  youxia
//
//  Created by mac on 15/12/26.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "OrderDetailHeaderCell.h"

#define TextFont IS_IPHONE6?16:15
#define leftLabel_Width 70
#define label_margin 10
@implementation OrderDetailHeaderCell{
    CGRect flFrameL;
    CGRect flFrameR;
    
    CGRect jdFrameL;
    CGRect jdFrameR;
    
    CGRect fqFrameL;
    CGRect fqFrameR;
    
    CGRect xtFrameL;
    CGRect xtFrameR;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)reflushDataForModel:(OrderDetailHeadModel *)model type:(IslandType)type isHadHt:(BOOL)isHadHt jpId:(NSNumber *)jpId dId:(NSNumber *)jdId fqId:(NSNumber *)fqId jpStatus:(NSString *)jpStatus jdStatus:(NSString *)jdStatus fqStatus:(NSString *)fqStatus xtId:(NSNumber *)xietId xtStatus:(NSString *)xtStatus{
    //
    UILabel *label01=[[UILabel alloc]initWithFrame:CGRectMake(8, 12, leftLabel_Width, 21)];
    label01.font=[UIFont systemFontOfSize:TextFont];
    label01.text=@"产品名称:";
    [self addSubview:label01];
    CGSize size=[AppUtils getStringSize:model.orderName withFont:TextFont];
    CGFloat pnWidth=SCREENSIZE.width-(label01.origin.x+label01.width+16);
    CGFloat pnHeight=0;
    if (size.width>pnWidth) {
        pnHeight=40;
    }else{
        pnHeight=21;
    }
    
    UILabel *productName=[[UILabel alloc]initWithFrame:CGRectMake(label01.origin.x+label01.width+8, label01.origin.y, pnWidth, pnHeight)];
    productName.font=[UIFont systemFontOfSize:TextFont];
    productName.text=model.orderName;
    productName.numberOfLines=0;
    [self addSubview:productName];
    
    //
    UILabel *label02=[[UILabel alloc]initWithFrame:CGRectMake(8, productName.origin.y+productName.height+label_margin, leftLabel_Width, 21)];
    label02.font=[UIFont systemFontOfSize:TextFont];
    label02.text=@"订单包含:";
    [self addSubview:label02];
    CGSize roomSize=[AppUtils getStringSize:[NSString stringWithFormat:@"1、%@",model.orderRoom] withFont:TextFont];
    CGFloat roomHeight=0;
    if (roomSize.width<pnWidth) {
        roomHeight=21;
    }else{
        roomHeight=43;
    }
    UILabel *orderInclude=[[UILabel alloc]initWithFrame:CGRectMake(label02.origin.x+label02.width+8, label02.origin.y, pnWidth, roomHeight+21)];
    orderInclude.font=[UIFont systemFontOfSize:TextFont];
    
    NSString *labelTtt;
    labelTtt=[NSString stringWithFormat:@"1、%@\n2、%@",model.orderRoom,model.orderFlight];
    orderInclude.numberOfLines=0;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    orderInclude.attributedText = attributedString;
    [self addSubview:orderInclude];
    
    ///////////////
    //出发地点
    UILabel *label03=[[UILabel alloc]initWithFrame:CGRectMake(8, orderInclude.origin.y+orderInclude.height+label_margin, leftLabel_Width, 21)];
    label03.font=[UIFont systemFontOfSize:TextFont];
    label03.text=@"出发地点:";
    [self addSubview:label03];
    
    UILabel *goPlace=[[UILabel alloc]initWithFrame:CGRectMake(label03.origin.x+label03.width+8, label03.origin.y, pnWidth, 21)];
    goPlace.font=[UIFont systemFontOfSize:TextFont];
    goPlace.text=model.goPlace;
    [self addSubview:goPlace];
    
    //
    UILabel *label04=[UILabel new];
    label04.font=[UIFont systemFontOfSize:TextFont];
    label04.text=@"出发时间:";
    [self addSubview:label04];
    
    CGFloat label04Y=0.0f;
    label04Y=goPlace.origin.y+goPlace.height+label_margin;
    label04.frame=CGRectMake(8, label04Y, leftLabel_Width, 21);
    
    UILabel *goTime=[[UILabel alloc]initWithFrame:CGRectMake(label04.origin.x+label04.width+8, label04.origin.y, pnWidth, 21)];
    goTime.font=[UIFont systemFontOfSize:TextFont];
    goTime.text=model.goTime;
    [self addSubview:goTime];
    
    //下单时间:
    UILabel *orderTimeLeft=[[UILabel alloc]initWithFrame:CGRectMake(8, goTime.origin.y+goTime.height+label_margin, leftLabel_Width, 21)];
    orderTimeLeft.font=[UIFont systemFontOfSize:TextFont];
    orderTimeLeft.text=@"下单时间:";
    [self addSubview:orderTimeLeft];
    
    //时间戳转换
    NSTimeInterval time=[model.orderTime doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *valiStr = [dateFormatter stringFromDate: detaildate];
    //
    UILabel *orderTimeRight=[[UILabel alloc]initWithFrame:CGRectMake(orderTimeLeft.origin.x+orderTimeLeft.width+8, orderTimeLeft.origin.y, pnWidth, 21)];
    orderTimeRight.font=[UIFont systemFontOfSize:TextFont];
    orderTimeRight.text=valiStr;
    [self addSubview:orderTimeRight];
    
    CGFloat endYW=0;
    
#pragma mark 有合同
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    NSString* str01 = [numberFormatter stringFromNumber:jpId];
    NSString* str02 = [numberFormatter stringFromNumber:jdId];
    if (isHadHt) {
        if([str01 isEqualToString:str02]){
            flFrameL=CGRectMake(8, orderTimeLeft.origin.y+orderTimeLeft.height+label_margin, leftLabel_Width+72, 21);
            UILabel *flightLeft=[[UILabel alloc]initWithFrame:flFrameL];
            flightLeft.font=[UIFont systemFontOfSize:TextFont];
            flightLeft.text=@"代订机票酒店合同:";
            [self addSubview:flightLeft];
            
            flFrameR=CGRectMake(flightLeft.origin.x+flightLeft.width, flightLeft.origin.y, leftLabel_Width, flightLeft.height);
            UILabel *flightRight=[[UILabel alloc]initWithFrame:flFrameR];
            flightRight.font=[UIFont systemFontOfSize:TextFont];
            flightRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *jpLStr=[jpStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *htContent = [[NSMutableAttributedString alloc]initWithString:jpLStr];
            NSRange mvRange = {0,[htContent length]};
            [htContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:mvRange];
            flightRight.attributedText = htContent;
            flightRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *htTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickJPHTLabel)];
            [flightRight addGestureRecognizer:htTap];
            [self addSubview:flightRight];
            
            //旅游安全提醒
            xtFrameL=CGRectMake(flightLeft.origin.x, flightLeft.origin.y+flightLeft.height+label_margin, flightLeft.width, flightLeft.height);
            UILabel *xietongLeft=[[UILabel alloc]initWithFrame:xtFrameL];
            xietongLeft.font=[UIFont systemFontOfSize:TextFont];
            xietongLeft.text=@"旅游安全提醒:";
            [self addSubview:xietongLeft];
            
            xtFrameR=CGRectMake(xietongLeft.origin.x+xietongLeft.width, xietongLeft.origin.y, leftLabel_Width, xietongLeft.height);
            UILabel *xietongRight=[[UILabel alloc]initWithFrame:xtFrameR];
            xietongRight.font=[UIFont systemFontOfSize:TextFont];
            xietongRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *xtStr=[xtStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *xtContent = [[NSMutableAttributedString alloc]initWithString:xtStr];
            NSRange xtRange = {0,[xtContent length]};
            [xtContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:xtRange];
            xietongRight.attributedText = xtContent;
            xietongRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *xtTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickXTHTLabel)];
            [xietongRight addGestureRecognizer:xtTap];
            [self addSubview:xietongRight];
            
            endYW=xietongLeft.origin.y+xietongLeft.height+16;
        }else{
            //代订机票合同:
            flFrameL=CGRectMake(8, orderTimeLeft.origin.y+orderTimeLeft.height+label_margin, leftLabel_Width+40, 21);
            UILabel *flightLeft=[[UILabel alloc]initWithFrame:flFrameL];
            flightLeft.font=[UIFont systemFontOfSize:TextFont];
            flightLeft.text=@"代订机票合同:";
            [self addSubview:flightLeft];
            
            flFrameR=CGRectMake(flightLeft.origin.x+flightLeft.width, flightLeft.origin.y, leftLabel_Width, flightLeft.height);
            UILabel *flightRight=[[UILabel alloc]initWithFrame:flFrameR];
            flightRight.font=[UIFont systemFontOfSize:TextFont];
            flightRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *jpLStr=[jpStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *htContent = [[NSMutableAttributedString alloc]initWithString:jpLStr];
            NSRange mvRange = {0,[htContent length]};
            [htContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:mvRange];
            flightRight.attributedText = htContent;
            flightRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *htTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickJPHTLabel)];
            [flightRight addGestureRecognizer:htTap];
            [self addSubview:flightRight];
            
            //
            //代订酒店合同:
            jdFrameL=CGRectMake(flightLeft.origin.x, flightLeft.origin.y+flightLeft.height+label_margin, flightLeft.width, flightLeft.height);
            UILabel *hotelLeft=[[UILabel alloc]initWithFrame:jdFrameL];
            hotelLeft.font=[UIFont systemFontOfSize:TextFont];
            hotelLeft.text=@"代订酒店合同:";
            [self addSubview:hotelLeft];
            
            jdFrameR=CGRectMake(hotelLeft.origin.x+hotelLeft.width, hotelLeft.origin.y, leftLabel_Width, hotelLeft.height);
            UILabel *hotelRight=[[UILabel alloc]initWithFrame:jdFrameR];
            hotelRight.font=[UIFont systemFontOfSize:TextFont];
            hotelRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *jdLStr=[jdStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *jdhtContent = [[NSMutableAttributedString alloc]initWithString:jdLStr];
            NSRange jdRange = {0,[jdhtContent length]};
            [jdhtContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:jdRange];
            hotelRight.attributedText = jdhtContent;
            hotelRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *jdTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickJDHTLabel)];
            [hotelRight addGestureRecognizer:jdTap];
            [self addSubview:hotelRight];
            
            //分期协议合同
            fqFrameL=CGRectMake(hotelLeft.origin.x, hotelLeft.origin.y+hotelLeft.height+label_margin, hotelLeft.width, hotelLeft.height);
            UILabel *stageLeft=[[UILabel alloc]initWithFrame:fqFrameL];
            stageLeft.font=[UIFont systemFontOfSize:TextFont];
            stageLeft.text=@"分期协议合同:";
            [self addSubview:stageLeft];
            
            fqFrameR=CGRectMake(stageLeft.origin.x+stageLeft.width, stageLeft.origin.y, leftLabel_Width, stageLeft.height);
            UILabel *stageRight=[[UILabel alloc]initWithFrame:fqFrameR];
            stageRight.font=[UIFont systemFontOfSize:TextFont];
            stageRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *fqLStr=[fqStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *fqhtContent = [[NSMutableAttributedString alloc]initWithString:fqLStr];
            NSRange fqRange = {0,[fqhtContent length]};
            [fqhtContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:fqRange];
            stageRight.attributedText = fqhtContent;
            stageRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *fqTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickFQHTLabel)];
            [stageRight addGestureRecognizer:fqTap];
            [self addSubview:stageRight];
            
            //安全提醒
            xtFrameL=CGRectMake(stageLeft.origin.x, stageLeft.origin.y+stageLeft.height+label_margin, stageLeft.width, stageLeft.height);
            UILabel *xietongLeft=[[UILabel alloc]initWithFrame:xtFrameL];
            xietongLeft.font=[UIFont systemFontOfSize:TextFont];
            xietongLeft.text=@"安全提醒:";
            [self addSubview:xietongLeft];
            
            xtFrameR=CGRectMake(xietongLeft.origin.x+xietongLeft.width, xietongLeft.origin.y, leftLabel_Width, xietongLeft.height);
            UILabel *xietongRight=[[UILabel alloc]initWithFrame:xtFrameR];
            xietongRight.font=[UIFont systemFontOfSize:TextFont];
            xietongRight.textColor=[UIColor colorWithHexString:@"#f06500"];
            NSString *xtStr=[xtStatus isEqualToString:@"0"]?@"签约合同":@"查看合同";
            NSMutableAttributedString *xtContent = [[NSMutableAttributedString alloc]initWithString:xtStr];
            NSRange xtRange = {0,[xtContent length]};
            [xtContent addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:xtRange];
            xietongRight.attributedText = xtContent;
            xietongRight.userInteractionEnabled=YES;
            UITapGestureRecognizer *xtTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickXTHTLabel)];
            [xietongRight addGestureRecognizer:xtTap];
            [self addSubview:xietongRight];
            
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            NSString *jpstr = [numberFormatter stringFromNumber:jpId];
            NSString *jdstr = [numberFormatter stringFromNumber:jdId];
            NSString *fqstr = [numberFormatter stringFromNumber:fqId];
            NSString *xtstr = [numberFormatter stringFromNumber:xietId];
            
            //机票为空
            if ([jpstr isEqualToString:@"0"] || [jpstr isEqualToString:@""]) {
                flightLeft.hidden=YES;
                flightRight.hidden=YES;
                endYW=hotelLeft.origin.y+hotelLeft.height+16;
                //机票为空，酒店为空
                if ([jdstr isEqualToString:@"0"] || [jdstr isEqualToString:@""]) {
                    hotelLeft.hidden=YES;
                    hotelRight.hidden=YES;
                    endYW=flightLeft.origin.y+flightLeft.height+16;
                    //机票为空，酒店为空，分期为空
                    if ([fqstr isEqualToString:@"0"] || [fqstr isEqualToString:@""]) {
                        stageLeft.hidden=YES;
                        stageRight.hidden=YES;
                        endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        //机票为空，酒店为空，分期为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=orderTimeLeft.origin.y+orderTimeLeft.height+16;
                        }
                        //机票为空，酒店为空，分期为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=flFrameL;
                            xietongRight.frame=flFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                    //机票为空，酒店为空，分期不为空
                    else{
                        stageLeft.hidden=NO;
                        stageRight.hidden=NO;
                        stageLeft.frame=flFrameL;
                        stageRight.frame=flFrameR;
                        endYW=stageLeft.origin.y+stageLeft.height+16;
                        //机票为空，酒店为空，分期不为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=flFrameL.origin.y+flFrameL.size.height+16;
                        }
                        //机票为空，酒店为空，分期不为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=jdFrameL;
                            xietongRight.frame=jdFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                }
                //机票为空，酒店不为空
                else{
                    hotelLeft.hidden=NO;
                    hotelRight.hidden=NO;
                    hotelLeft.frame=flFrameL;
                    hotelRight.frame=flFrameR;
                    endYW=hotelLeft.origin.y+hotelLeft.height+16;
                    //机票为空，酒店不为空，分期为空
                    if ([fqstr isEqualToString:@"0"] || [fqstr isEqualToString:@""]) {
                        stageLeft.hidden=YES;
                        stageRight.hidden=YES;
                        endYW=hotelLeft.origin.y+hotelLeft.height+16;
                        //机票为空，酒店不为空，分期为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=hotelLeft.origin.y+hotelLeft.height+16;
                        }
                        //机票为空，酒店不为空，分期为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=jdFrameL;
                            xietongRight.frame=jdFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                    //机票为空，酒店不为空，分期不为空
                    else{
                        stageLeft.hidden=NO;
                        stageRight.hidden=NO;
                        stageLeft.frame=jdFrameL;
                        stageRight.frame=jdFrameR;
                        endYW=stageLeft.origin.y+stageLeft.height+16;
                        //机票为空，酒店不为空，分期不为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=stageLeft.origin.y+stageLeft.height+16;
                        }
                        //机票为空，酒店不为空，分期不为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=fqFrameL;
                            xietongRight.frame=fqFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                }
            }
            //机票不为空
            else{
                flightLeft.hidden=NO;
                flightRight.hidden=NO;
                endYW=flightLeft.origin.y+flightLeft.height+16;
                //机票不为空，酒店为空
                if ([jdstr isEqualToString:@"0"] || [jdstr isEqualToString:@""]) {
                    hotelLeft.hidden=YES;
                    hotelRight.hidden=YES;
                    //机票不为空，酒店为空，分期为空
                    if ([fqstr isEqualToString:@"0"] || [fqstr isEqualToString:@""]) {
                        stageLeft.hidden=YES;
                        stageRight.hidden=YES;
                        //机票不为空，酒店为空，分期为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=flightLeft.origin.y+flightLeft.height+16;
                        }
                        //机票不为空，酒店为空，分期为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=jdFrameL;
                            xietongRight.frame=jdFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                    //机票不为空，酒店为空，分期不为空
                    else{
                        stageLeft.hidden=NO;
                        stageRight.hidden=NO;
                        stageLeft.frame=jdFrameL;
                        stageRight.frame=jdFrameR;
                        endYW=stageLeft.origin.y+stageLeft.height+16;
                        //机票不为空，酒店为空，分期不为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=stageLeft.origin.y+stageLeft.height+16;
                        }
                        //机票不为空，酒店为空，分期不为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=fqFrameL;
                            xietongRight.frame=fqFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                }
                //机票不为空，酒店不为空
                else{
                    hotelLeft.hidden=NO;
                    hotelRight.hidden=NO;
                    hotelLeft.frame=jdFrameL;
                    hotelRight.frame=jdFrameR;
                    endYW=hotelLeft.origin.y+hotelLeft.height+16;
                    //机票不为空，酒店不为空，分期为空
                    if ([fqstr isEqualToString:@"0"] || [fqstr isEqualToString:@""]) {
                        stageLeft.hidden=YES;
                        stageRight.hidden=YES;
                        endYW=hotelLeft.origin.y+hotelLeft.height+16;
                        //机票不为空，酒店不为空，分期为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=hotelLeft.origin.y+hotelLeft.height+16;
                        }
                        //机票不为空，酒店不为空，分期为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=fqFrameL;
                            xietongRight.frame=fqFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                    //机票不为空，酒店不为空，分期不为空
                    else{
                        stageLeft.hidden=NO;
                        stageRight.hidden=NO;
                        stageLeft.frame=fqFrameL;
                        stageRight.frame=fqFrameR;
                        endYW=stageLeft.origin.y+stageLeft.height+16;
                        //机票不为空，酒店不为空，分期不为空，协同为空
                        if ([xtstr isEqualToString:@"0"] || [xtstr isEqualToString:@""]) {
                            xietongLeft.hidden=YES;
                            xietongRight.hidden=YES;
                            endYW=stageLeft.origin.y+stageLeft.height+16;
                        }
                        //机票不为空，酒店不为空，分期不为空，协同不为空
                        else{
                            xietongLeft.hidden=NO;
                            xietongRight.hidden=NO;
                            xietongLeft.frame=xtFrameL;
                            xietongRight.frame=xtFrameR;
                            endYW=xietongLeft.origin.y+xietongLeft.height+16;
                        }
                    }
                }
            }
        }
    }
#pragma mark 没有合同
    else{
        endYW=orderTimeLeft.origin.y+orderTimeLeft.height+16;
    }
    
    //
    CGRect frame=self.frame;
    frame.size.height=endYW;
    self.frame=frame;
}

#pragma mark - 点击合同签约
-(void)clickJPHTLabel{
    [self.delegate jphtQianyue];
}

-(void)clickJDHTLabel{
    [self.delegate jdhtQianyue];
}

-(void)clickFQHTLabel{
    [self.delegate fqhtQianyue];
}

-(void)clickXTHTLabel{
    [self.delegate xtQianyue];
}

@end
