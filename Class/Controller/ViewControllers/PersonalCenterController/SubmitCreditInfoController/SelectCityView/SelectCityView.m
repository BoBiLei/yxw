//
//  SelectCityView.m
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "SelectCityView.h"
#import "FmdbCityModel.h"
#import "CityFmdb.h"

@interface SelectCityView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIPickerView *customPickerView;
@property (copy) void (^onDismissCompletion)(NSString *);
@property (copy) NSString *(^objectToStringConverter)(id object);

@end

@implementation SelectCityView{
    NSMutableArray *provinceArray;
    NSMutableArray *cityArr;
    NSMutableArray *districtArr;
    
    NSString *provinceId;
    NSString *cityId;
    NSString *districtId;
    
    NSArray *selectedArray;
}

#pragma mark - get Addressdata
- (void)getPickerData {
    provinceArray=[NSMutableArray array];
    cityArr=[NSMutableArray array];
    districtArr=[NSMutableArray array];
    
    [XMLDictionaryParser sharedInstance].alwaysUseArrays=YES;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"xml"];
    
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLFile:filePath];
    
    //默认
    NSArray *pArr=xmlDoc[@"province"];
    for (int i=0; i<pArr.count; i++) {
        NSDictionary *pDic=pArr[i];
        [provinceArray addObject:pDic];
        if (i==0) {
            NSArray *cArr=pArr[i][@"city"];
            for (NSDictionary *cDic in cArr) {
                [cityArr addObject:cDic];
                NSArray *ctArr=cDic[@"country"];
                for (NSDictionary *ctDic in ctArr) {
                    [districtArr addObject:ctDic];
                }
            }
        }
    }
}

#pragma mark - Init PickerView
-(id)initPickerViewWithFrame:(CGRect)frame{
    self = [super init];
    if (self) {
        
        [self getPickerData];
        
        [self setFrame:frame];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //半透明黑色背景
        _backgroundView = [[UIView alloc] initWithFrame:self.frame];
        [_backgroundView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
        [_backgroundView setAlpha:0.0];
        [_backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
        [self addSubview:_backgroundView];
        [self bringSubviewToFront:_backgroundView];
        
        //PickerView Container with top bar
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _backgroundView.height - 236.0, self.width, 236.0)];
        _containerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_containerView];
        [self bringSubviewToFront:_containerView];
        
        //ToolbarBackgroundColor - Black
        UIColor *toolbarBackgroundColor = [[UIColor alloc] initWithCGColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8].CGColor];
        
        //Top bar view
        UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _containerView.width, 44.0)];
        [_containerView addSubview:topBarView];
        [topBarView setBackgroundColor:[UIColor whiteColor]];
        
        
        UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:topBarView.frame];
        [_containerView addSubview:pickerToolBar];
        
        CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        if (iOSVersion < 7.0) {
            pickerToolBar.tintColor = toolbarBackgroundColor;
        }else{
            [pickerToolBar setBackgroundColor:toolbarBackgroundColor];
            
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
            pickerToolBar.barTintColor = toolbarBackgroundColor;
#endif
        }
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.width-50, 4, 40, 40)];
        [closeBtn setTitle:@"完成" forState:UIControlStateNormal];
        [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, -6)];
        [closeBtn setTitleColor:[UIColor colorWithHexString:YxColor_Blue] forState:UIControlStateNormal];
        closeBtn.titleLabel.font=[UIFont boldSystemFontOfSize:16];
        [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
        pickerToolBar.items = @[flexibleSpace, barButtonItem];
        
        //Add pickerView
        _customPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, topBarView.height, self.width, _containerView.height-topBarView.height)];
        [_customPickerView setDelegate:self];
        [_customPickerView setDataSource:self];
        [_containerView addSubview:_customPickerView];
        
        [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
    }
    return self;
}

#pragma mark - Show Methods
-(void)showPickerViewCompletion:(void (^)(id))completion{
    [self setPickerHidden:NO callBack:nil];
    self.onDismissCompletion = completion;
}

#pragma mark - Dismiss Methods
-(void)dismissWithCompletion:(void (^)(NSString *))completion{
    [self setPickerHidden:YES callBack:completion];
}

-(void)dismiss{
    [self dismissWithCompletion:self.onDismissCompletion];
}

-(void)removePickerView{
    [self removeFromSuperview];
}

#pragma mark - Show/hide PickerView methods
-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(id))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_backgroundView setAlpha:0.0];
                             [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
                         } else {
                             [_backgroundView setAlpha:1.0];
                             [_containerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [self removePickerView];
                             callBack([self selectedObject]);
                         }
                     }];
}

#pragma mark - 点击透明背景hidden
- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_backgroundView setAlpha:1.0];
                         [_containerView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component {
    if (component == 0) {
        return provinceArray.count;
    }else if (component == 1){
        return cityArr.count;
    }else{
        return districtArr.count;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component __TVOS_PROHIBITED{
    return 38;
}

- (NSString *)pickerView: (UIPickerView *)pickerView
             titleForRow: (NSInteger)row
            forComponent: (NSInteger)component {
    NSDictionary *str;
    if (component == 0) {
        str=provinceArray[row];
    }
    else if(component==1){
        str=cityArr[row];
    }else{
        str=districtArr[row];
    }
    return str[@"_name"];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        pickerLabel.textAlignment=NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:kFontSize18];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (id)selectedObject {
    NSArray *arr;
    NSString *proStr=[provinceArray objectAtIndex:[_customPickerView selectedRowInComponent:0]][@"_name"];
    NSString *cityStr=[cityArr objectAtIndex:[_customPickerView selectedRowInComponent:1]][@"_name"];
    NSString *distStr=[districtArr objectAtIndex:[_customPickerView selectedRowInComponent:2]][@"_name"];
    arr=@[proStr,cityStr==NULL?@"":cityStr,distStr==NULL?@"":distStr];
    return arr;
}

#pragma mark - UIPickerViewDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    if (component == 0) {
        selectedArray =provinceArray;
        cityArr=selectedArray[row][@"city"];
        districtArr=cityArr[0][@"country"];
        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectedRowInComponent:2];
    }else if (component == 1){
        if (selectedArray.count > 0 && cityArr.count > 0) {
            districtArr=cityArr[row][@"country"];
        }else{
            districtArr = nil;
        }
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    [pickerView reloadComponent:2];
}
@end
