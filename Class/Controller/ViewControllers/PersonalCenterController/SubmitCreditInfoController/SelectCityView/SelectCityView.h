//
//  SelectCityView.h
//  MMG
//
//  Created by mac on 15/10/20.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCityView : UIView

-(id)initPickerViewWithFrame:(CGRect)frame;

-(void)showPickerViewCompletion:(void (^)(id))completion;
@end
