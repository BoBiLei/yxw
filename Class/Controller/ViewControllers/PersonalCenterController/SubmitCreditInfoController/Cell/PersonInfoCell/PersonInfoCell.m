//
//  PersonInfoCell.m
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "PersonInfoCell.h"
#import  <UIButton+WebCache.h>
#define Add_picture2 @"add_picture.jpg"
#define textBGColor @"#fafafa"
@implementation PersonInfoCell{
    UITextField *nameTf;    //姓名
    UITextField *eduTf;     //身份证
    UITextField *jjkTf;     //借记卡
    UITextField *jjkPhoneTf;//借记卡预留手机号
    
    
    
    CGFloat anliHeight;
    CGRect anl01Frame;  //案例1的frame
    CGRect anl02Frame;  //案例2的frame
    BOOL isSeleTwo;
    UIImageView *anl01;     //银行卡正面
    UIImageView *anl02;     //银行卡反面
    UIView *personIdLabelView;
    
    
    
 
    UIButton *saveBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNoti:) name:UpdateXCKNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiImgCount) name:UpdateUploadImgCount object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadButtonIsNot) name:UploadButtonIsNot object:nil];
    
  
    
    //nameLabel
    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 12, 88, 34)];;
    nameLabel.text=@"姓名:";
    nameLabel.font=kFontSize16;
    nameLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    nameLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc]initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+32), nameLabel.height)];
    nameView.layer.borderWidth=0.7f;
    nameView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    nameView.layer.cornerRadius=3;
    nameView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    [self addSubview:nameView];
    
    nameTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, nameView.width-10, nameView.height)];
    nameTf.placeholder=@"请输入姓名";
    nameTf.font=kFontSize15;
    nameTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:nameTf];
    
    //educationLabel
    UILabel *educationLabel=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, nameLabel.width, nameLabel.height)];
    educationLabel.text=@"身份证号码:";
    educationLabel.font=kFontSize16;
    educationLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    educationLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:educationLabel];
    
    UIView *educationLabelView=[[UIView alloc]initWithFrame:CGRectMake(educationLabel.origin.x+educationLabel.width+8, nameView.origin.y+nameView.height+12, SCREENSIZE.width-(educationLabel.origin.x+educationLabel.width+32), educationLabel.height)];
    educationLabelView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    educationLabelView.layer.borderWidth=0.7f;
    educationLabelView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    educationLabelView.layer.cornerRadius=3;
    [self addSubview:educationLabelView];
    
    eduTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, educationLabelView.width-10, educationLabelView.height)];
    eduTf.placeholder=@"请输入身份证号码";
    eduTf.font=kFontSize15;
    eduTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [educationLabelView addSubview:eduTf];
    
    //marriageLabel
    UILabel *marriageLabel=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, educationLabel.origin.y+educationLabel.height+12, educationLabel.width, educationLabel.height)];
    marriageLabel.text=@"储蓄卡卡号:";
    marriageLabel.font=kFontSize16;
    marriageLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    marriageLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:marriageLabel];
    
    UIView *marriageLabelView=[[UIView alloc]initWithFrame:CGRectMake(marriageLabel.origin.x+marriageLabel.width+8, marriageLabel.origin.y, SCREENSIZE.width-(marriageLabel.origin.x+marriageLabel.width+32), marriageLabel.height)];
    marriageLabelView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    marriageLabelView.layer.borderWidth=0.7f;
    marriageLabelView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    marriageLabelView.layer.cornerRadius=3;
    [self addSubview:marriageLabelView];
    
    jjkTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, marriageLabelView.width-10, marriageLabelView.height)];
    jjkTf.placeholder=@"请输入储蓄卡号";
    jjkTf.font=kFontSize15;
    jjkTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [marriageLabelView addSubview:jjkTf];
    
    //personIdLabel
    UILabel *personIdLabel=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, marriageLabel.origin.y+marriageLabel.height+12, marriageLabel.width, marriageLabel.height)];
    personIdLabel.text=@"预留手机号:";
    personIdLabel.font=kFontSize16;
    personIdLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    personIdLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:personIdLabel];
    
    personIdLabelView=[[UIView alloc]initWithFrame:CGRectMake(personIdLabel.origin.x+personIdLabel.width+8, personIdLabel.origin.y, SCREENSIZE.width-(personIdLabel.origin.x+personIdLabel.width+32), personIdLabel.height)];
    personIdLabelView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    personIdLabelView.layer.borderWidth=0.7f;
    personIdLabelView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    personIdLabelView.layer.cornerRadius=3;
    [self addSubview:personIdLabelView];
    
    jjkPhoneTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, personIdLabelView.width-10, personIdLabelView.height)];
    jjkPhoneTf.placeholder=@"输入储蓄卡银行预留手机号";
    jjkPhoneTf.font=kFontSize15;
    jjkPhoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    jjkPhoneTf.keyboardType=UIKeyboardTypeNumberPad;
    [personIdLabelView addSubview:jjkPhoneTf];
    

    
#pragma mark 验证按钮
    saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame=CGRectMake(SCREENSIZE.width-144, personIdLabelView.origin.y+personIdLabelView.height+12, 120, personIdLabelView.height);

    saveBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [saveBtn setTitle:@"储蓄卡验证" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveBtn.titleLabel.font=kFontSize15;
    saveBtn.layer.cornerRadius=3;
    [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveBtn];
    
    
    //1
    UILabel *tip01=[[UILabel alloc]initWithFrame:CGRectMake(personIdLabel.origin.x, saveBtn.origin.y+saveBtn.height+5, SCREENSIZE.width-24, 40)];
    tip01.text=@"请上传以上银行储蓄卡正反面图片";
    tip01.font=kFontSize16;
    tip01.textColor=[UIColor colorWithHexString:@"#282828"];
    tip01.textAlignment=NSTextAlignmentLeft;
    tip01.numberOfLines=0;
    [self addSubview:tip01];
    
    
    UILabel *upLoadTip01=[[UILabel alloc]initWithFrame:CGRectMake(12, tip01.origin.y+tip01.height+8, SCREENSIZE.width-24, 26)];
    upLoadTip01.text=@"注：JPG、PNG格式图片，大小不超过2M";
    upLoadTip01.font=kFontSize14;
    upLoadTip01.textColor=[UIColor redColor];
    upLoadTip01.textAlignment=NSTextAlignmentLeft;
    upLoadTip01.numberOfLines=0;
    [self addSubview:upLoadTip01];
    
    //显示银行卡的
    _selectedBtn03=[IdCartButton buttonWithType:UIButtonTypeCustom];
    _selectedBtn03.frame=CGRectMake(12, upLoadTip01.origin.y+upLoadTip01.height+10, (SCREENSIZE.width-36)/2, (SCREENSIZE.width-36)/2);
    _selectedBtn03.layer.borderWidth=0.7f;
    _selectedBtn03.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [_selectedBtn03 setImage:[UIImage imageNamed:Add_picture2] forState:UIControlStateNormal];
    _selectedBtn03.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [_selectedBtn03 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectedBtn03.titleLabel.font=kFontSize15;
    [_selectedBtn03 addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_selectedBtn03];
    _selectedBtn04=[IdCartButton buttonWithType:UIButtonTypeCustom];
    _selectedBtn04.frame=CGRectMake(_selectedBtn03.origin.x+_selectedBtn03.width+12, _selectedBtn03.origin.y, _selectedBtn03.width, _selectedBtn03.height);
    _selectedBtn04.layer.borderWidth=0.7f;
    _selectedBtn04.imageView.contentMode=UIViewContentModeScaleAspectFit;
    _selectedBtn04.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [_selectedBtn04 setImage:[UIImage imageNamed:Add_picture2] forState:UIControlStateNormal];
    [_selectedBtn04 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectedBtn04.titleLabel.font=kFontSize15;
    [_selectedBtn04 addTarget:self action:@selector(clickSBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_selectedBtn04];
    
    
    UIImageView *zmImg=[UIImageView new];
    zmImg.image=[UIImage imageNamed:@"uploadimg_zm"];
    [_selectedBtn03 addSubview:zmImg];
    zmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* zmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_h];
    NSArray* zmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_w];
    
    UIImageView *fmImg=[UIImageView new];
    fmImg.image=[UIImage imageNamed:@"uploadimg_fm"];
    [_selectedBtn04 addSubview:fmImg];
    fmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* fmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_h];
    NSArray* fmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    
    [NSLayoutConstraint activateConstraints:fmImg_w];
    
    //银行卡下面删除按钮  改成审核状态
    _deleteBtn011=[UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn011.frame=CGRectMake(12, _selectedBtn03.origin.y+_selectedBtn03.height+2, _selectedBtn03.width, 36);
    _deleteBtn011.hidden=YES;
    _deleteBtn011.backgroundColor=[UIColor colorWithHexString:YxColor_MBlue];
    [_deleteBtn011 setTitle:@"待审核" forState:UIControlStateNormal];
    _deleteBtn011.titleLabel.font=kFontSize15;
    [_deleteBtn011 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[_deleteBtn011 addTarget:self action:@selector(clickDeleteBtn011:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteBtn011];
    
    _deleteBtn022=[UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn022.frame=CGRectMake(_deleteBtn011.origin.x+_deleteBtn011.width+12, _deleteBtn011.origin.y, _deleteBtn011.width, _deleteBtn011.height);
    _deleteBtn022.hidden=YES;
    _deleteBtn022.backgroundColor=[UIColor colorWithHexString:YxColor_MBlue];
    [_deleteBtn022 setTitle:@"待审核" forState:UIControlStateNormal];
    _deleteBtn022.titleLabel.font=kFontSize15;
    [_deleteBtn022 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[_deleteBtn022 addTarget:self action:@selector(clickDeleteBtn022:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteBtn022];
    
    CGRect frame=self.frame;
    frame.size.height=_deleteBtn011.origin.y+_deleteBtn011.height+12;
    self.frame=frame;
    
    
}

#pragma mark - 点击保存按钮
-(void)clickSaveBtn{
    [AppUtils closeKeyboard];
    if ([nameTf.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请填写姓名" inView:self.window];
    }else if ([eduTf.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"身份证号不能为空" inView:self.window];
    }else if (![AppUtils validateIdentityCard:eduTf.text]){
        [AppUtils showSuccessMessage:@"请填写正确的身份证号" inView:self.window];
    }else if ([jjkTf.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"储蓄卡卡号不能为空" inView:self.window];
    }else if (![AppUtils isCardNumber:jjkTf.text]){
        [AppUtils showSuccessMessage:@"储蓄卡卡号有误" inView:self.window];
    }else if ([jjkPhoneTf.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"储蓄卡预留手机号不能为空" inView:self.window];
    }else if (![AppUtils checkPhoneNumber:jjkPhoneTf.text]){
        [AppUtils showSuccessMessage:@"填写的手机号有误" inView:self.window];
    }else{
        [self.delegate clickValidateCXKWithName:nameTf.text personId:eduTf.text cxkNumber:jjkTf.text phone:jjkPhoneTf.text];
    }
}


-(void)notiImgCount{
    isSeleTwo=YES;
}

-(void)uploadButtonIsNot{
    isSeleTwo=NO;
}



#pragma mark - 点击正面删除按钮
-(void)clickDeleteBtn011:(id)sender{
    [self.deleteDelegate deleteImageWithButton:0];
}

#pragma mark - 点击反面删除按钮
-(void)clickDeleteBtn022:(id)sender{
    [self.deleteDelegate deleteImageWithButton:1];
}

#pragma mark - 点击正面选择图片按钮
-(void)clickFBtn:(id)sender{
    UIButton *btn=sender;
    [self.delegate01 showSelectedcard01WithButton:btn];
}

#pragma mark - 点击反面选择图片按钮
-(void)clickSBtn:(id)sender{
    UIButton *btn=sender;
    [self.delegate02 showSelectedcard02WithButton:btn];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(void)updateNoti:(NSNotification *)noti{
    NSArray *arr=noti.object;
    NSDictionary *zm=arr[5];
    NSDictionary *fm=arr[6];
    [self reflushDataForArray:arr withFontImg:zm[@"zfl"] backImg:fm[@"zfl"] totalStatus:_tStatus];
}

-(void)reflushDataForArray:(NSArray *)array withFontImg:(NSString *)fontStr backImg:(NSString *)backStr totalStatus:(NSString *)totalStatus{
    
    _tStatus=totalStatus;
    
    NSString *status=array[4];
    for (int i=0; i<array.count; i++) {
        NSString *str=array[i];
        if (i==0) {
            nameTf.text=str;
        }else if (i==1){
            NSString *textStr;
            if ([status isEqualToString:@"1"]) {
                textStr=[self replaceMyString:str range:NSMakeRange(4, str.length-8)];
                eduTf.text=textStr;
            }else{
                eduTf.text=str;
            }
            
        }else if (i==2){
            NSString *cck;
            if ([status isEqualToString:@"1"]) {
                cck=[self replaceMyString:str range:NSMakeRange(4, str.length-8)];
                jjkTf.text=cck;
            }else{
                jjkTf.text=str;
            }
        }else if (i==3){
            jjkPhoneTf.text=str;
        }else if (i==4){
            if ([str isEqualToString:@"0"]) {
                saveBtn.hidden=NO;
            }else{
                nameTf.enabled=NO;
                nameTf.textColor=[UIColor grayColor];
                nameTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
                eduTf.enabled=NO;
                eduTf.textColor=[UIColor grayColor];
                eduTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
                jjkTf.enabled=NO;
                jjkTf.textColor=[UIColor grayColor];
                jjkTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
                jjkPhoneTf.enabled=NO;
                jjkPhoneTf.textColor=[UIColor grayColor];
                jjkPhoneTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
                
                CGRect frame=self.frame;
                frame.size.height=_deleteBtn022.origin.y+_deleteBtn022.height+12;
                self.frame=frame;
                saveBtn.hidden=YES;
            }
        }
    }
    
    NSString *fData=fontStr;
    
    if(![fData isEqualToString:@""])
    {
        //  显示银行卡删除按钮_deleteBtn011.hidden=NO;
        _deleteBtn011.hidden=NO;
        [_selectedBtn03 setImageWithURL:[NSURL URLWithString:fData] forState:UIControlStateNormal placeholder:nil];
//        [anl01 setImageURL:[NSURL URLWithString:fData]];
        CGRect frame=self.frame;
        frame.size.height=_deleteBtn011.origin.y+_deleteBtn011.height+12;
        self.frame=frame;
    }
  
    NSString *bData=backStr;
    
    

    
    if (![bData isEqualToString:@""]) {
        
        //  显示银行卡删除按钮_deleteBtn022.hidden=NO;
        
        _deleteBtn022.hidden=NO;

         [_selectedBtn04 setImageWithURL:[NSURL URLWithString:bData] forState:UIControlStateNormal placeholder:nil];
//        [anl02 setImageURL:[NSURL URLWithString:bData]];
        CGRect frame=self.frame;
        frame.size.height=_deleteBtn011.origin.y+_deleteBtn011.height+12;
        self.frame=frame;
         }
    
    
    /*
     *1--未审核
     *2--审核中
     *3--审核通过
     */
    if ([totalStatus isEqualToString:@"3"]) {
        [_deleteBtn011 setTitle:@"审核通过" forState:UIControlStateNormal];
         [_deleteBtn022 setTitle:@"审核通过" forState:UIControlStateNormal];
        _selectedBtn03.enabled=NO;
        _selectedBtn04.enabled=NO;
    }else if ([totalStatus isEqualToString:@"2"]) {
        [_deleteBtn011 setTitle:@"审核中" forState:UIControlStateNormal];
        [_deleteBtn022 setTitle:@"审核中" forState:UIControlStateNormal];
        _selectedBtn03.enabled=NO;
        _selectedBtn04.enabled=NO;
    }else{
        [_deleteBtn011 setTitle:@"未审核" forState:UIControlStateNormal];
        [_deleteBtn022 setTitle:@"未审核" forState:UIControlStateNormal];
        _selectedBtn03.enabled=YES;
        _selectedBtn04.enabled=YES;
    }
    
}


-(NSString *)replaceMyString:(NSString *)handleStr range:(NSRange)rang{
    
    NSString *star=@"";
    for (int i=0; i<handleStr.length-rang.location*2; i++) {
        star=[star stringByAppendingString:@"*"];
    }
    
    NSMutableString * str = [[NSMutableString alloc]initWithString:handleStr];
    [str replaceCharactersInRange:rang withString:star];
    return [NSString stringWithString:str];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
