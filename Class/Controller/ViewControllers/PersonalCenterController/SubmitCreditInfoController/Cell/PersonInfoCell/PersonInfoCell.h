//
//  PersonInfoCell.h
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonIdCartModel.h"
#import "IdCartButton.h"
@protocol ValidateInfoDelegate <NSObject>
-(void)clickValidateCXKWithName:(NSString *)name personId:(NSString *)personId cxkNumber:(NSString *)cxkNum phone:(NSString *)phone;
@end

@protocol SelectedPhoto011Delegate <NSObject>
-(void)showSelectedcard01WithButton:(UIButton *)button;
@end


@protocol SelectedPhoto022Delegate <NSObject>
-(void)showSelectedcard02WithButton:(UIButton *)button;
@end

@protocol DeleteImageDelegate2 <NSObject>
-(void)deleteImageWithButton:(NSInteger)tag;
@end

@interface PersonInfoCell : UITableViewCell

@property (weak, nonatomic) id<ValidateInfoDelegate> delegate;

@property (nonatomic, strong) IdCartButton *selectedBtn03;
@property (nonatomic, strong) IdCartButton *selectedBtn04;

@property (nonatomic, strong) UIButton *deleteBtn011;
@property (nonatomic, strong) UIButton *deleteBtn022;


@property (weak, nonatomic) id<SelectedPhoto011Delegate> delegate01;
@property (weak, nonatomic) id<SelectedPhoto022Delegate> delegate02;

@property (weak, nonatomic) id<DeleteImageDelegate2> deleteDelegate;

@property (nonatomic ,copy) NSString *tStatus;


-(void)reflushDataForArray:(NSArray *)array withFontImg:(NSString *)fontStr backImg:(NSString *)backStr totalStatus:(NSString *)totalStatus;

@end
