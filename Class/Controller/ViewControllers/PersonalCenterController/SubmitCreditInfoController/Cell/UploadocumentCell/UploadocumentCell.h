//
//  UploadocumentCell.h
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonIdCartModel.h"
#import "IdCartButton.h"

@protocol SelectedPhoto01Delegate <NSObject>
-(void)showSelectedPhoto01WithButton:(UIButton *)button;
@end

@protocol SelectedPhoto02Delegate <NSObject>
-(void)showSelectedPhoto02WithButton:(UIButton *)button;
@end

@protocol DeleteImageDelegate <NSObject>
-(void)deleteImageWithButton:(NSInteger)tag;
@end

@interface UploadocumentCell : UITableViewCell

@property (nonatomic, strong) IdCartButton *selectedBtn01;
@property (nonatomic, strong) IdCartButton *selectedBtn02;
@property (nonatomic, strong) IdCartButton *selectedBtn03;
@property (nonatomic, strong) IdCartButton *selectedBtn04;
@property (nonatomic, strong) UIButton *deleteBtn01;
@property (nonatomic, strong) UIButton *deleteBtn02;

@property (weak, nonatomic) id<SelectedPhoto01Delegate> delegate01;
@property (weak, nonatomic) id<SelectedPhoto02Delegate> delegate02;

@property (weak, nonatomic) id<DeleteImageDelegate> deleteDelegate;

@end
