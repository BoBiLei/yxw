//
//  UploadocumentCell.m
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "UploadocumentCell.h"
#import <UIButton+WebCache.h>
#define Add_picture @"add_picture.jpg"
#define textBGColor @"#fafafa"
@implementation UploadocumentCell{
    CGFloat anliHeight;
    CGRect anl01Frame;  //案例1的frame
    CGRect anl02Frame;  //案例2的frame
    UIImageView *anl01;
    UIImageView *anl02;
    BOOL isSeleTwo;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notiImgCount) name:UpdateUploadImgCount object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadButtonIsNot) name:UploadButtonIsNot object:nil];
    UILabel *tip01=[[UILabel alloc]initWithFrame:CGRectMake(12, 12, SCREENSIZE.width-24, 40)];
    tip01.text=@"身份证 照片/复印件 正反面（仅用于机票及酒店预订）";
    tip01.font=kFontSize16;
    tip01.textColor=[UIColor colorWithHexString:@"#282828"];
    tip01.textAlignment=NSTextAlignmentLeft;
    tip01.numberOfLines=0;
    [self addSubview:tip01];
    
    //
    UILabel *upLoadTip01=[[UILabel alloc]initWithFrame:CGRectMake(12, tip01.origin.y+tip01.height+8, SCREENSIZE.width-24, 26)];
    upLoadTip01.text=@"注：JPG、PNG格式图片，大小不超过2M";
    upLoadTip01.font=kFontSize14;
    upLoadTip01.textColor=[UIColor redColor];
    upLoadTip01.textAlignment=NSTextAlignmentLeft;
    upLoadTip01.numberOfLines=0;
    [self addSubview:upLoadTip01];
    
    
    //显示身份证的
    _selectedBtn01=[IdCartButton buttonWithType:UIButtonTypeCustom];
    _selectedBtn01.frame=CGRectMake(12, upLoadTip01.origin.y+upLoadTip01.height+10, (SCREENSIZE.width-36)/2, (SCREENSIZE.width-36)/2);
    _selectedBtn01.layer.borderWidth=0.7f;
    _selectedBtn01.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [_selectedBtn01 setImage:[UIImage imageNamed:Add_picture] forState:UIControlStateNormal];
    _selectedBtn01.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [_selectedBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectedBtn01.titleLabel.font=kFontSize15;
    [_selectedBtn01 addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_selectedBtn01];
    _selectedBtn02=[IdCartButton buttonWithType:UIButtonTypeCustom];
    _selectedBtn02.frame=CGRectMake(_selectedBtn01.origin.x+_selectedBtn01.width+12, _selectedBtn01.origin.y, _selectedBtn01.width, _selectedBtn01.height);
    _selectedBtn02.layer.borderWidth=0.7f;
    _selectedBtn02.imageView.contentMode=UIViewContentModeScaleAspectFit;
    _selectedBtn02.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [_selectedBtn02 setImage:[UIImage imageNamed:Add_picture] forState:UIControlStateNormal];
    [_selectedBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectedBtn02.titleLabel.font=kFontSize15;
    [_selectedBtn02 addTarget:self action:@selector(clickSBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_selectedBtn02];
    
    UILabel *tipLabel01=[UILabel new];
    tipLabel01.hidden=YES;
    tipLabel01.font=kFontSize14;
    tipLabel01.backgroundColor=[UIColor colorWithHexString:YxColor_MBlue];
    tipLabel01.textAlignment=NSTextAlignmentCenter;
    tipLabel01.text=@"验证成功";
    tipLabel01.textColor=[UIColor whiteColor];
    tipLabel01.alpha=0.75f;
    [_selectedBtn01 addSubview:tipLabel01];
    tipLabel01.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tipLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tipLabel01]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipLabel01)];
    [NSLayoutConstraint activateConstraints:tipLabel_h];
    NSArray* tipLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tipLabel01(34)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipLabel01)];
    [NSLayoutConstraint activateConstraints:tipLabel_w];
    _selectedBtn01.tipLabel=tipLabel01;
    
    UILabel *tipLabel02=[UILabel new];
    tipLabel02.hidden=YES;
    tipLabel02.font=kFontSize14;
    tipLabel02.backgroundColor=[UIColor colorWithHexString:YxColor_MBlue];
    tipLabel02.textAlignment=NSTextAlignmentCenter;
    tipLabel02.text=@"验证失败";
    tipLabel02.textColor=[UIColor whiteColor];
    tipLabel02.alpha=0.75f;
    [_selectedBtn02 addSubview:tipLabel02];
    tipLabel02.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* tipLabel02_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tipLabel02]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipLabel02)];
    [NSLayoutConstraint activateConstraints:tipLabel02_h];
    NSArray* tipLabel02_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tipLabel02(34)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipLabel02)];
    [NSLayoutConstraint activateConstraints:tipLabel02_w];
    _selectedBtn02.tipLabel=tipLabel02;
    
    //身份证照片左上角（正反面提示）
    UIImageView *zmImg=[UIImageView new];
    zmImg.image=[UIImage imageNamed:@"uploadimg_zm"];
    [_selectedBtn01 addSubview:zmImg];
    zmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* zmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_h];
    NSArray* zmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[zmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(zmImg)];
    [NSLayoutConstraint activateConstraints:zmImg_w];
    
    UIImageView *fmImg=[UIImageView new];
    fmImg.image=[UIImage imageNamed:@"uploadimg_fm"];
    [_selectedBtn02 addSubview:fmImg];
    fmImg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* fmImg_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_h];
    NSArray* fmImg_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fmImg(56)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(fmImg)];
    [NSLayoutConstraint activateConstraints:fmImg_w];
    
    //身份证下面删除按钮
    _deleteBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn01.frame=CGRectMake(12, _selectedBtn01.origin.y+_selectedBtn01.height+2, _selectedBtn01.width, 36);
    _deleteBtn01.hidden=YES;
    _deleteBtn01.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [_deleteBtn01 setTitle:@"[删除]" forState:UIControlStateNormal];
    _deleteBtn01.titleLabel.font=kFontSize15;
    [_deleteBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_deleteBtn01 addTarget:self action:@selector(clickDeleteBtn01:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteBtn01];
    
    _deleteBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn02.frame=CGRectMake(_deleteBtn01.origin.x+_deleteBtn01.width+12, _deleteBtn01.origin.y, _deleteBtn01.width, _deleteBtn01.height);
    _deleteBtn02.hidden=YES;
    _deleteBtn02.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [_deleteBtn02 setTitle:@"[删除]" forState:UIControlStateNormal];
    _deleteBtn02.titleLabel.font=kFontSize15;
    [_deleteBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_deleteBtn02 addTarget:self action:@selector(clickDeleteBtn02:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteBtn02];
    
    //上传说明
    UILabel *stamentLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, _deleteBtn01.origin.y+_deleteBtn01.height+10, SCREENSIZE.width-24, 36)];
    stamentLabel.font=kFontSize14;
    stamentLabel.numberOfLines=0;
    NSString *labelTtt=@"请把身份证摆正平放在白纸上拍照（确保手机竖着拍摄），不要反光，例如：";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    stamentLabel.attributedText = attributedString;
    [self addSubview:stamentLabel];
    
    //身份证案例（正方面）
    
    CGFloat al1X=SCREENSIZE.width/4;
    anliHeight=(SCREENSIZE.width-al1X*2)*1022/500;
    anl01Frame=CGRectMake(al1X, stamentLabel.origin.y+stamentLabel.height+12, SCREENSIZE.width-al1X*2, anliHeight);
    anl01=[[UIImageView alloc]initWithFrame:anl01Frame];
    anl01.contentMode=UIViewContentModeScaleAspectFit;
    anl01.image=[UIImage imageNamed:@"sfz01"];
    [self addSubview:anl01];
    
//    CGFloat anliHeight02=(SCREENSIZE.width-12)*245/500;
//    anl02Frame=CGRectMake(6, anl01.origin.y+anl01.height+12, SCREENSIZE.width-12, anliHeight02);
//    anl02=[[UIImageView alloc]initWithFrame:anl02Frame];
//    anl02.contentMode=UIViewContentModeScaleAspectFit;
//    anl02.image=[UIImage imageNamed:@"sfz02"];
//    [self addSubview:anl02];
    
    //DSLog(@"%f",anl02.origin.y+anl02.height+12);
    
    //
    CGRect frame=self.frame;
    frame.size.height=anl02.origin.y+anl02.height+12;
}

-(void)notiImgCount{
    isSeleTwo=YES;
}

-(void)uploadButtonIsNot{
    isSeleTwo=NO;
}

#pragma mark - 点击正面删除按钮
-(void)clickDeleteBtn01:(id)sender{
    [self.deleteDelegate deleteImageWithButton:0];
}

#pragma mark - 点击反面删除按钮
-(void)clickDeleteBtn02:(id)sender{
    [self.deleteDelegate deleteImageWithButton:1];
}

#pragma mark - 点击正面选择图片按钮
-(void)clickFBtn:(id)sender{
    UIButton *btn=sender;
    [self.delegate01 showSelectedPhoto01WithButton:btn];
}

#pragma mark - 点击反面选择图片按钮
-(void)clickSBtn:(id)sender{
    UIButton *btn=sender;
    [self.delegate02 showSelectedPhoto02WithButton:btn];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
