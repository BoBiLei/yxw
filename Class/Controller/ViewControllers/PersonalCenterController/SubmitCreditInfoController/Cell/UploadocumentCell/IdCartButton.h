//
//  IdCartButton.h
//  youxia
//
//  Created by mac on 16/1/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IdCartButton : UIButton

@property (nonatomic, strong) UILabel *tipLabel;

@end
