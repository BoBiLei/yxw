//
//  SubPhoneCell.h
//  youxia
//
//  Created by mac on 16/1/4.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShowSelectContactDelegate <NSObject>

-(void)clickShowSelectView;

@end


@protocol SaveContactDelegate <NSObject>

-(void)saveContactInfoWithName:(NSString *)name phone:(NSString *)phone;

@end

@interface SubPhoneCell : UITableViewCell

@property (weak, nonatomic) id<SaveContactDelegate> delegate;

@property (weak, nonatomic) id<ShowSelectContactDelegate> selectContactDelegate;

-(void)reflushDataForArray:(NSArray *)array validateStatus:(NSString *)status;

@end
