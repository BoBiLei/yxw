//
//  SubPhoneCell.m
//  youxia
//
//  Created by mac on 16/1/4.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SubPhoneCell.h"

#define textBGColor @"#fafafa"
@implementation SubPhoneCell{
    UITextField *nameTf;    //姓名
    UITextField *eduTf;     //身份证
    UIButton *saveBtn;
    UIView *educationLabelView;
    UIButton *showCtBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNoti:) name:UpdateContactNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLXR:) name:SelectedContactNotification object:nil];
    //nameLabel
    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 12, 88, 34)];;
    nameLabel.text=@"紧急联系人:";
    nameLabel.font=kFontSize16;
    nameLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    nameLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc]initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, SCREENSIZE.width-(nameLabel.origin.x+nameLabel.width+32), nameLabel.height)];
    nameView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    nameView.layer.borderWidth=0.7f;
    nameView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    nameView.layer.cornerRadius=3;
    [self addSubview:nameView];
    
    nameTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, nameView.width-10, nameView.height)];
    nameTf.placeholder=@"填写紧急联系人姓名";
    nameTf.font=kFontSize15;
    nameTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:nameTf];
    
    //educationLabel
    UILabel *educationLabel=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, nameLabel.width, nameLabel.height)];
    educationLabel.text=@"联系人电话:";
    educationLabel.font=kFontSize16;
    educationLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    educationLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:educationLabel];
    
    educationLabelView=[[UIView alloc]initWithFrame:CGRectMake(educationLabel.origin.x+educationLabel.width+8, nameView.origin.y+nameView.height+12, SCREENSIZE.width-(educationLabel.origin.x+educationLabel.width+32), educationLabel.height)];
    educationLabelView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    educationLabelView.layer.borderWidth=0.7f;
    educationLabelView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    educationLabelView.layer.cornerRadius=3;
    [self addSubview:educationLabelView];
    
    eduTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, educationLabelView.width-10, educationLabelView.height)];
    eduTf.placeholder=@"填写紧急联系人电话";
    eduTf.enabled=NO;
    eduTf.font=kFontSize15;
    eduTf.keyboardType=UIKeyboardTypePhonePad;
    eduTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [educationLabelView addSubview:eduTf];
    
    showCtBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    showCtBtn.layer.cornerRadius=2;
    showCtBtn.frame=CGRectMake(educationLabelView.width-60, 2, 58, educationLabelView.height-4);
    showCtBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [showCtBtn setTitle:@"选择" forState:UIControlStateNormal];
    [showCtBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    showCtBtn.titleLabel.font=kFontSize14;
    [showCtBtn addTarget:self action:@selector(clickSeleContactBtn) forControlEvents:UIControlEventTouchUpInside];
    [educationLabelView addSubview:showCtBtn];
    
    //saveBtn
    saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame=CGRectMake(SCREENSIZE.width-144, educationLabelView.origin.y+educationLabelView.height+12, 120, educationLabelView.height);
    saveBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [saveBtn setTitle:@"保存信息" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveBtn.titleLabel.font=kFontSize15;
    saveBtn.layer.cornerRadius=3;
    [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveBtn];
    
    CGRect frame=self.frame;
    frame.size.height=saveBtn.origin.y+saveBtn.height+12;
    self.frame=frame;
}

-(void)updateLXR:(NSNotification *)noti{
    NSArray *arr=noti.object;
    NSString *name=arr[0];
    if (![name isEqualToString:@""]) {
        nameTf.text=name;
    }
    eduTf.text=arr[1];
}

-(void)clickSeleContactBtn{
    [self.selectContactDelegate clickShowSelectView];
}

#pragma mark - 点击保存按钮
-(void)clickSaveBtn{
    [AppUtils closeKeyboard];
//    if ([nameTf.text isEqualToString:@""]) {
//        [AppUtils showSuccessMessage:@"紧急联系人姓名不能为空" inView:self.window];
//    }else if ([eduTf.text isEqualToString:@""]){
//        [AppUtils showSuccessMessage:@"联系人电话不能为空" inView:self.window];
//    }else if (![AppUtils checkPhoneNumber:eduTf.text]){
//        [AppUtils showSuccessMessage:@"电话号码有误" inView:self.window];
//    }else{
//        [self.delegate saveContactInfoWithName:nameTf.text phone:eduTf.text];
//    }
    if ([nameTf.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"紧急联系人姓名不能为空" inView:self.window];
    }else if ([eduTf.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"联系人电话不能为空" inView:self.window];
    }else{
        [self.delegate saveContactInfoWithName:nameTf.text phone:eduTf.text];
    }
}

-(void)updateNoti:(NSNotification *)noti{
    NSArray *arr=noti.object;
    [self reflushDataForArray:arr validateStatus:@""];
}

-(void)reflushDataForArray:(NSArray *)array validateStatus:(NSString *)status{
    for (int i=0; i<array.count; i++) {
        NSString *str=array[i];
        if (i==0) {
            nameTf.text=str;
        }else if (i==1){
            eduTf.text=str;
        }
    }
    
    //=3说明验证通过，（隐藏保存信息按钮）
    if ([status isEqualToString:@"2"]||
        [status isEqualToString:@"3"]) {
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y;
        self.frame=frame;
        saveBtn.hidden=YES;
        
        nameTf.enabled=NO;
        nameTf.textColor=[UIColor grayColor];
        nameTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        
        eduTf.enabled=NO;
        eduTf.textColor=[UIColor grayColor];
        eduTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        
        showCtBtn.hidden=YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
