//
//  SecondCell.m
//  youxia
//
//  Created by mac on 16/1/4.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SecondCell.h"
#import <UIButton+WebCache.h>

#define textBGColor @"#fafafa"
#define RESETTIME 60
@implementation SecondCell{
    
    int ReSetTime;
    int timerCount;
    
    UIView *tipView;        //提示View
    
    UILabel *phoneLabel;
    UIView *phoneView;
    UITextField *phoneTf;
    
    UIView *passView;
    UILabel *passLabel;
    UITextField *passTf;
    
    UILabel *validLeftLabel;
    UIView *validView;      //图片验证码
    UITextField *validTf;
    
    UIButton *reGetValidBtn;    //重新获取验证码按钮
    
    NSString *nedImg;
    NSString *name;
    NSString *certNo;
    
    UIButton *saveBtn;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAllStatusNoti:) name:@"UpdateAllStatusNotification" object:nil];
        
        //TipLabel
        tipView=[[UIView alloc] initWithFrame:CGRectMake(12, 12, SCREENSIZE.width-24, 48)];
        tipView.layer.borderWidth=0.5f;
        tipView.layer.borderColor=[UIColor colorWithHexString:@"ff9900"].CGColor;
        tipView.backgroundColor=[UIColor colorWithHexString:@"ffffcc"];
        [self addSubview:tipView];
        
        UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, tipView.width-16, tipView.height)];
        NSString *labelTtt=@"如忘记服务密码，请拨打手机运营商电话，或登录手机运营商网站查询！";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:2];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        tipLabel.attributedText = attributedString;
        tipLabel.font=kFontSize14;
        tipLabel.textColor=[UIColor colorWithHexString:@"ec6b00"];
        tipLabel.numberOfLines=0;
        [tipView addSubview:tipLabel];
        
        //手机号码
        phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, tipView.origin.y+tipView.height+12, 88, 38)];
        phoneLabel.text=@"手机号码:";
        phoneLabel.font=kFontSize16;
        phoneLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        phoneLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:phoneLabel];
        
        phoneView=[[UIView alloc]initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, phoneLabel.origin.y, SCREENSIZE.width-(phoneLabel.origin.x+phoneLabel.width+32), phoneLabel.height)];
        phoneView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        phoneView.layer.borderWidth=0.7f;
        phoneView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        phoneView.layer.cornerRadius=3;
        [self addSubview:phoneView];
        
        phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, phoneView.width-10, phoneView.height)];
        phoneTf.placeholder=@"请输入手机号码";
        phoneTf.font=kFontSize15;
        phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
        phoneTf.keyboardType=UIKeyboardTypeNumberPad;
        [phoneView addSubview:phoneTf];
        
        //查询密码
        passLabel=[[UILabel alloc]initWithFrame:CGRectMake(phoneLabel.origin.x, phoneLabel.origin.y+phoneLabel.height+12, phoneLabel.width, phoneLabel.height)];
        passLabel.text=@"服务密码:";
        passLabel.font=kFontSize16;
        passLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        passLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:passLabel];
        
        passView=[[UIView alloc]initWithFrame:CGRectMake(passLabel.origin.x+passLabel.width+8, phoneView.origin.y+phoneView.height+12, SCREENSIZE.width-(passLabel.origin.x+passLabel.width+32), passLabel.height)];
        passView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        passView.layer.borderWidth=0.7f;
        passView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        passView.layer.cornerRadius=3;
        [self addSubview:passView];
        
        passTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, passView.width-10, passView.height)];
        passTf.placeholder=@"请输入服务密码";
        passTf.font=kFontSize15;
        passTf.secureTextEntry=YES;
        passTf.clearButtonMode=UITextFieldViewModeWhileEditing;
        [passView addSubview:passTf];
        
        //图片验证码
        validLeftLabel=[[UILabel alloc]initWithFrame:CGRectMake(phoneLabel.origin.x, passLabel.origin.y+passLabel.height+12, passLabel.width, passLabel.height)];
        validLeftLabel.text=@"登录验证码:";
        validLeftLabel.font=kFontSize16;
        validLeftLabel.textColor=[UIColor colorWithHexString:@"#282828"];
        validLeftLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:validLeftLabel];
        
        validView=[[UIView alloc]initWithFrame:CGRectMake(validLeftLabel.origin.x+validLeftLabel.width+8, validLeftLabel.origin.y, passView.width, validLeftLabel.height)];
        validView.backgroundColor=[UIColor colorWithHexString:textBGColor];
        validView.layer.borderWidth=0.7f;
        validView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        validView.layer.cornerRadius=3;
        [self addSubview:validView];
        
        validTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, validView.width-12, validView.height)];
        validTf.placeholder=@"请输入登录验证码";
        validTf.font=kFontSize15;
        validTf.clearButtonMode=UITextFieldViewModeWhileEditing;
        [validView addSubview:validTf];
        
        ReSetTime=60;
        timerCount = RESETTIME;
        reGetValidBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        reGetValidBtn.frame=CGRectMake(validView.width-82,2, 80, validView.height-4);
        reGetValidBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        [reGetValidBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [reGetValidBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        reGetValidBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        reGetValidBtn.layer.cornerRadius=3;
        [reGetValidBtn addTarget:self action:@selector(clickReGetValidBtn) forControlEvents:UIControlEventTouchUpInside];
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        [validView addSubview:reGetValidBtn];
        
        //saveBtn
        saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        saveBtn.tag=99;
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        saveBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        saveBtn.titleLabel.font=kFontSize15;
        saveBtn.layer.cornerRadius=3;
        [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:saveBtn];
        
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }
    return self;
}

#pragma mark - 点击重新获取按钮
-(void)clickReGetValidBtn{
    [AppUtils closeKeyboard];
    ReSetTime=60;
    timerCount = ReSetTime;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
    [self.delegate reGetValideCode:phoneTf.text];
}

-(void)setTimer:(NSTimer *)timer{
    if (timerCount<0){
        [timer invalidate];
        [reGetValidBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        reGetValidBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
        reGetValidBtn.enabled=YES;
        timerCount = ReSetTime;
        return;
    }else{
        reGetValidBtn.backgroundColor=[UIColor lightGrayColor];
        reGetValidBtn.enabled=NO;
        [reGetValidBtn setTitle:[NSString stringWithFormat:@"%d秒后获取",timerCount] forState:UIControlStateNormal];
    }
    timerCount--;
}

#pragma mark - 点击保存按钮
-(void)clickSaveBtn{
    //tag=100 手机号、查询密码
    //tag=200 手机号、查询密码、图形码
    [AppUtils closeKeyboard];
    if (saveBtn.tag==99) {
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPass:phoneTf.text password:passTf.text];
        }
    }else if (saveBtn.tag==100){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入图片验证码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPassAndPicture:phoneTf.text password:passTf.text picture:validTf.text];
        }
    }else if (saveBtn.tag==200){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([passTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入服务密码" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入短信验证码" inView:self.window];
        }else{
            [self.delegate submitPhoneAndPassAndPicture:phoneTf.text password:passTf.text picture:validTf.text];
        }
    }else if (saveBtn.tag==999){
        if ([phoneTf.text isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请输入手机号" inView:self.window];
        }else if (![AppUtils checkPhoneNumber:phoneTf.text]){
            [AppUtils showSuccessMessage:@"手机号码有误" inView:self.window];
        }else if ([validTf.text isEqualToString:@""]){
            [AppUtils showSuccessMessage:@"请输入短信验证码" inView:self.window];
        }else{
            //验证短信
            [self.delegate submitPhoneAndDuanxin:phoneTf.text duanxin:validTf.text needImage:nedImg capcha:passTf.text name:name cartNo:certNo];
        }
    }
}

#pragma mark - NSNotification
-(void)updateAllStatusNoti:(NSNotification *)noti{
    passTf.frame=CGRectMake(6, 0, passView.width-10, passView.height);
    NSArray *arr=noti.object;
    [self reflushDataForArray:arr];
}

#pragma mark - 生成/刷新UI或数据
-(void)reflushDataForArray:(NSArray *)array{
    
    nedImg=@"0";
    name=@"";
    certNo=@"";
    
    /*phone_status
     0   未通过
     1   通过
     100 需要图片验证码
     200 短信验证码
     999 第二个短信验证码
     */
    NSString *rcphoneStr=array[0];
    NSString *rcStatusStr=array[1];
    NSString *imgStr=array[3];
    NSString *validTime=array[5];
    ReSetTime=validTime.intValue;
    
    phoneTf.text=rcphoneStr;
    if ([rcStatusStr isEqualToString:@""]
        ||[rcStatusStr isEqualToString:@"0"]) {
        if ([array[4] isEqualToString:@"电信"]) {
            passTf.text=array[5];
        }
        
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        
        //
        validLeftLabel.hidden=YES;
        validView.hidden=YES;
        validView.hidden=YES;
        
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
        
    }else if([rcStatusStr isEqualToString:@"1"]){
        if ([array[4] isEqualToString:@"电信"]) {
            passTf.text=array[5];
        }
        
        reGetValidBtn.hidden=YES;
        reGetValidBtn.enabled=NO;
        
        //
        phoneTf.enabled=NO;
        phoneTf.textColor=[UIColor grayColor];
        phoneTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
        
        passLabel.hidden=YES;
        passView.hidden=YES;
        passTf.secureTextEntry=YES;
        
        validLeftLabel.text=@"";
        validLeftLabel.hidden=YES;
        validView.hidden=YES;
        
        saveBtn.hidden=YES;
        
        tipView.hidden=YES;
        
        CGRect phFrame=phoneLabel.frame;
        phFrame.origin.y=tipView.origin.y;
        phoneLabel.frame=phFrame;
        
        CGRect pvFrame=phoneView.frame;
        pvFrame.origin.y=tipView.origin.y;
        phoneView.frame=pvFrame;
        
        CGRect frame=self.frame;
        frame.size.height=phoneView.origin.y+phoneView.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"100"]){
        
        saveBtn.tag=100;
        
        NSString *recPass=array[2];
        passTf.text=recPass;
        passTf.secureTextEntry=YES;
        passLabel.hidden=NO;
        passView.hidden=NO;
        
        reGetValidBtn.hidden=NO;
        reGetValidBtn.enabled=YES;
        [reGetValidBtn sd_setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal];
        
        //
        validLeftLabel.text=@"图片验证码";
        validLeftLabel.hidden=NO;
        validView.hidden=NO;
        validTf.placeholder=@"请输入图片验证码";
        
        //
        saveBtn.hidden=NO;
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"200"]){
        
        saveBtn.tag=200;
        
        //
        NSString *recPass=array[2];
        passTf.text=recPass;
        passTf.secureTextEntry=YES;
        reGetValidBtn.enabled=NO;
        passLabel.hidden=NO;
        passView.hidden=NO;
        
        reGetValidBtn.hidden=YES;
        
        //
        validLeftLabel.text=@"短信验证码";
        validLeftLabel.hidden=NO;
        validView.hidden=NO;
        validTf.placeholder=@"请输入短信验证码";
        
        saveBtn.hidden=NO;
        [saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
    }else if([rcStatusStr isEqualToString:@"999"]){
        if (ReSetTime!=0) {
            timerCount = ReSetTime;
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
        }else{
            validTf.text=array[4];
        }
        
        passTf.secureTextEntry=NO;
        
        saveBtn.tag=999;
        
        //needImg = 1 需要图片验证码
        //needImg = 0 不需要图片验证码
        NSString *needImg=array[2];
        if([needImg isEqualToString:@"1"]){
            
            nedImg=@"1";
            
            passLabel.hidden=NO;
            passView.hidden=NO;
            reGetValidBtn.hidden=NO;
            passLabel.text=@"图片验证码";
            passTf.placeholder=@"请输入图片验证码";
            
            UIButton *picBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            picBtn.frame=CGRectMake(passView.width-82,2, 80, passView.height-4);
            picBtn.backgroundColor=[UIColor colorWithHexString:textBGColor];
            [picBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal];
            picBtn.layer.cornerRadius=3;
            [picBtn addTarget:self action:@selector(clickImageValidBtn) forControlEvents:UIControlEventTouchUpInside];
            [passView addSubview:picBtn];
            
            validLeftLabel.text=@"短信验证码";
            validTf.placeholder=@"再次输入短信验证码";
            [validTf addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
            
            //isDX = 1 电信
            //isDX = 0 非电信
            NSString *isDX=array[4];
            if([isDX isEqualToString:@"1"]){
#pragma mark 真实姓名
                UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(phoneLabel.origin.x, validLeftLabel.origin.y+validLeftLabel.height+12, validLeftLabel.width, validLeftLabel.height)];
                nameLabel.text=@"真实姓名:";
                nameLabel.font=kFontSize16;
                nameLabel.textColor=[UIColor colorWithHexString:@"#282828"];
                nameLabel.textAlignment=NSTextAlignmentRight;
                [self addSubview:nameLabel];
                
                UIView *nameView=[[UIView alloc]initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, passView.width, nameLabel.height)];
                nameView.backgroundColor=[UIColor colorWithHexString:textBGColor];
                nameView.layer.borderWidth=0.7f;
                nameView.layer.borderColor=[UIColor lightGrayColor].CGColor;
                nameView.layer.cornerRadius=3;
                [self addSubview:nameView];
                
                UITextField *nameTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, nameView.width-12, nameView.height)];
                nameTf.placeholder=@"请输入真实姓名";
                nameTf.font=kFontSize15;
                nameTf.clearButtonMode=UITextFieldViewModeWhileEditing;
                [nameTf addTarget:self action:@selector(nameTfChange:) forControlEvents:UIControlEventEditingChanged];
                [nameView addSubview:nameTf];
                
#pragma mark 身份证
                UILabel *idcardLabel=[[UILabel alloc]initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, nameLabel.width, nameLabel.height)];
                idcardLabel.text=@"身份证号码:";
                idcardLabel.font=kFontSize16;
                idcardLabel.textColor=[UIColor colorWithHexString:@"#282828"];
                idcardLabel.textAlignment=NSTextAlignmentRight;
                [self addSubview:idcardLabel];
                
                UIView *idcardView=[[UIView alloc]initWithFrame:CGRectMake(idcardLabel.origin.x+idcardLabel.width+8, idcardLabel.origin.y, nameView.width, idcardLabel.height)];
                idcardView.backgroundColor=[UIColor colorWithHexString:textBGColor];
                idcardView.layer.borderWidth=0.7f;
                idcardView.layer.borderColor=[UIColor lightGrayColor].CGColor;
                idcardView.layer.cornerRadius=3;
                [self addSubview:idcardView];
                
                UITextField *idcardTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, nameView.width-12, nameView.height)];
                idcardTf.placeholder=@"请输入身份证号码";
                idcardTf.font=kFontSize15;
                idcardTf.clearButtonMode=UITextFieldViewModeWhileEditing;
                [idcardTf addTarget:self action:@selector(idcardTfChange:) forControlEvents:UIControlEventEditingChanged];
                [idcardView addSubview:idcardTf];
                
                //
                saveBtn.hidden=NO;
                [saveBtn setTitle:@"提交验证" forState:UIControlStateNormal];
                saveBtn.frame=CGRectMake(SCREENSIZE.width-144, idcardView.origin.y+idcardView.height+12, 120, idcardView.height);
                CGRect frame=self.frame;
                frame.size.height=saveBtn.origin.y+saveBtn.height+12;
                self.frame=frame;
            }else if([isDX isEqualToString:@"0"]){
                //
                saveBtn.hidden=NO;
                [saveBtn setTitle:@"提交验证" forState:UIControlStateNormal];
                saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
                CGRect frame=self.frame;
                frame.size.height=saveBtn.origin.y+saveBtn.height+12;
                self.frame=frame;
            }
        }else{
            
            nedImg=@"0";
            
            passLabel.hidden=YES;
            passView.hidden=YES;
            
            reGetValidBtn.hidden=NO;
            
            //
            validLeftLabel.frame=passLabel.frame;
            validView.frame=passView.frame;
            validLeftLabel.text=@"短信验证码";
            validTf.placeholder=@"再次输入短信验证码";
            
            //
            saveBtn.hidden=NO;
            [saveBtn setTitle:@"提交验证" forState:UIControlStateNormal];
            saveBtn.frame=CGRectMake(SCREENSIZE.width-144, validView.origin.y+validView.height+12, 120, validView.height);
            CGRect frame=self.frame;
            frame.size.height=saveBtn.origin.y+saveBtn.height+12;
            self.frame=frame;
        }
    }
}

#pragma mark - textField
- (void)textChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.SecondPictureChangeBlock) {
        self.SecondPictureChangeBlock(field.text);
    }
}

-(void)clickImageValidBtn{
    [self.delegate reGetImageValide:phoneTf.text];
}

-(void)nameTfChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    name=field.text;
}

-(void)idcardTfChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    certNo=field.text;
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
