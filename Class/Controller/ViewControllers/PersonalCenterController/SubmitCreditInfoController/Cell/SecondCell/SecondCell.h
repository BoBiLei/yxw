//
//  SecondCell.h
//  youxia
//
//  Created by mac on 16/1/4.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ValidateXYKDelegate <NSObject>

/**验证电话、服务密码*/
-(void)submitPhoneAndPass:(NSString *)phone password:(NSString *)passWord;

/**验证电话、服务密码、图片验证码*/
-(void)submitPhoneAndPassAndPicture:(NSString *)phone password:(NSString *)passWord picture:(NSString *)picture;

/**验证电话、短信*/
-(void)submitPhoneAndDuanxin:(NSString *)phone duanxin:(NSString *)duanxin needImage:(NSString *)needImg capcha:(NSString *)capcha name:(NSString *)name cartNo:(NSString *)cartNo;

//重新获取图片验证码
-(void)reGetImageValide:(NSString *)phone;

//重新获取短信验证码
-(void)reGetValideCode:(NSString *)phone;

@end

@interface SecondCell : UITableViewCell

@property (nonatomic, copy) void (^SecondPictureChangeBlock)(NSString *text);

@property (weak, nonatomic) id<ValidateXYKDelegate> delegate;

-(void)reflushDataForArray:(NSArray *)array;

@end
