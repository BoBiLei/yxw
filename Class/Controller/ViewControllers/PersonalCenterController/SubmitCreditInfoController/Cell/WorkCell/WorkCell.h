//
//  WorkCell.h
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SaveContactInfoDelegate <NSObject>

-(void)saveAddressWithCompany:(NSString *)company companyAddress:(NSString *)companyAddress companyDetailAdr:(NSString *)companyDetailAdr companyReceiver:(NSString *)companyReceiver companyPhone:(NSString *)companyPhone homeAddress:(NSString *)homeAddress homeDetailAdr:(NSString *)homeDetailAdr homeReceiver:(NSString *)homeReceiver homePhone:(NSString *)homePhone;

@end

@protocol SelectedCompanyAddressDelegate <NSObject>
-(void)showSelectedCompanyAddressSheet;
@end

@protocol SelectedHomeAddressDelegate <NSObject>
-(void)showSelectedHomeAddressSheet;
@end

@interface WorkCell : UITableViewCell

@property (nonatomic, strong) void (^CompanyAddressCallBack)(NSString * address);
@property (weak, nonatomic) id<SelectedCompanyAddressDelegate> delegate;
@property (weak, nonatomic) id<SelectedHomeAddressDelegate> HomeDelegate;
@property (weak, nonatomic) id<SaveContactInfoDelegate> saveDelegate;

-(void)reflushDataForArray:(NSArray *)array validateStatus:(NSString *)status;

@end
