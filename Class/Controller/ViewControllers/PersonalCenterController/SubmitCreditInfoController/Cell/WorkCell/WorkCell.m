//
//  WorkCell.m
//  youxia
//
//  Created by mac on 15/12/7.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "WorkCell.h"
#import "FmdbCityModel.h"

#define textBGColor @"#fafafa"
@implementation WorkCell{
    UITextField *companyTf;        //单位名称
    UITextField *companyAdr;    //单位地址
    UITextField *cmpAdrDetail;  //单位详细地址
//    UITextField *cmpReceiver;   //单位收件人
//    UITextField *cmpPhone;      //办公电话
    UITextField *homeAdr;
    UITextField *homeDetailAdr;
    
//    UIView *phoneView;
    UIView *nsrView;
    UIButton *saveBtn;
    
    UIView *companyLabelView;
    UIView *sbView;
    
    //
    NSString *cpnProvince;
    NSString *cpnCity;
    NSString *cpnDistrict;
    
    //
    NSString *homeProvince;
    NSString *homeCity;
    NSString *homeDistrict;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCompanyAddress:) name:SelectedCompanyAddressNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateHomeAddress:) name:SelectedHomeAddressNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTXDZ:) name:UpdateTXDZNotification object:nil];
    //
    UILabel *companyLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 12, 88, 34)];
    companyLabel.text=@"单位名称:";
    companyLabel.font=kFontSize16;
    companyLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    companyLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:companyLabel];
    
    UIView *companyView=[[UIView alloc]initWithFrame:CGRectMake(companyLabel.origin.x+companyLabel.width+8, companyLabel.origin.y, SCREENSIZE.width-(companyLabel.origin.x+companyLabel.width+32), companyLabel.height)];
    companyView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    companyView.layer.borderWidth=0.7f;
    companyView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    companyView.layer.cornerRadius=3;
    [self addSubview:companyView];
    
    companyTf=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, companyView.width-10, companyView.height)];
    companyTf.placeholder=@"请填写单位名称";
    companyTf.font=kFontSize15;
    companyTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    [companyView addSubview:companyTf];
    
    //公司地址
    UILabel *companyAddLabel=[[UILabel alloc]initWithFrame:CGRectMake(companyLabel.origin.x, companyLabel.origin.y+companyLabel.height+12, companyLabel.width, companyLabel.height)];
    companyAddLabel.text=@"单位地址:";
    companyAddLabel.font=kFontSize16;
    companyAddLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    companyAddLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:companyAddLabel];
    
    companyLabelView=[[UIView alloc]initWithFrame:CGRectMake(companyAddLabel.origin.x+companyAddLabel.width+8, companyAddLabel.origin.y, SCREENSIZE.width-(companyAddLabel.origin.x+companyAddLabel.width+32), companyAddLabel.height)];
    companyLabelView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    companyLabelView.layer.borderWidth=0.7f;
    companyLabelView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    companyLabelView.layer.cornerRadius=3;
    [self addSubview:companyLabelView];
    companyLabelView.userInteractionEnabled=YES;
    UITapGestureRecognizer *cmptap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectCompanyAddress)];
    [companyLabelView addGestureRecognizer:cmptap];
    
    companyAdr=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, companyLabelView.width-10, companyLabelView.height)];
    companyAdr.placeholder=@"请选择单位地址";
    companyAdr.font=kFontSize15;
    companyAdr.enabled=NO;
    [companyLabelView addSubview:companyAdr];
    
    //详细地址
    UIView *addDetailView=[[UIView alloc]initWithFrame:CGRectMake(companyLabelView.origin.x, companyLabelView.origin.y+companyLabelView.height+12, companyLabelView.width, companyLabelView.height)];
    addDetailView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    addDetailView.layer.borderWidth=0.7f;
    addDetailView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    addDetailView.layer.cornerRadius=3;
    [self addSubview:addDetailView];
    
    cmpAdrDetail=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, addDetailView.width-10, addDetailView.height)];
    cmpAdrDetail.placeholder=@"请填写公司详细地址";
    cmpAdrDetail.font=kFontSize15;
    cmpAdrDetail.clearButtonMode=UITextFieldViewModeWhileEditing;
    [addDetailView addSubview:cmpAdrDetail];
    
    //家庭地址
    UILabel *sbLabel=[[UILabel alloc]initWithFrame:CGRectMake(companyAddLabel.origin.x, addDetailView.origin.y+addDetailView.height+12, companyAddLabel.width, addDetailView.height)];
    sbLabel.text=@"家庭地址:";
    sbLabel.font=kFontSize16;
    sbLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    sbLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:sbLabel];
    
    sbView=[[UIView alloc]initWithFrame:CGRectMake(sbLabel.origin.x+sbLabel.width+8, sbLabel.origin.y, SCREENSIZE.width-(sbLabel.origin.x+sbLabel.width+32), sbLabel.height)];
    sbView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    sbView.layer.borderWidth=0.7f;
    sbView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    sbView.layer.cornerRadius=3;
    sbView.userInteractionEnabled=YES;
    [self addSubview:sbView];
    
    homeAdr=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, sbView.width-10, sbView.height)];
    homeAdr.font=kFontSize15;
    homeAdr.placeholder=@"请选择家庭地址";
    homeAdr.enabled=NO;
    homeAdr.clearButtonMode=UITextFieldViewModeWhileEditing;
    [sbView addSubview:homeAdr];
    
    UITapGestureRecognizer *tap01=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectHomeAddress)];
    [sbView addGestureRecognizer:tap01];
    
    //家庭详细地址
    nsrView=[[UIView alloc]initWithFrame:CGRectMake(sbView.origin.x, sbView.origin.y+sbView.height+12, sbView.width, sbView.height)];
    nsrView.backgroundColor=[UIColor colorWithHexString:textBGColor];
    nsrView.layer.borderWidth=0.7f;
    nsrView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    nsrView.layer.cornerRadius=3;
    [self addSubview:nsrView];
    
    homeDetailAdr=[[UITextField alloc]initWithFrame:CGRectMake(6, 0, nsrView.width-10, nsrView.height)];
    homeDetailAdr.font=kFontSize15;
    homeDetailAdr.placeholder=@"请填写家庭详细地址";
    homeDetailAdr.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nsrView addSubview:homeDetailAdr];
    
    //saveBtn
    saveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame=CGRectMake(SCREENSIZE.width-144, nsrView.origin.y+nsrView.height+12, 120, nsrView.height);
    saveBtn.backgroundColor=[UIColor colorWithHexString:YxColor_Blue];
    [saveBtn setTitle:@"保存信息" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveBtn.titleLabel.font=kFontSize15;
    saveBtn.layer.cornerRadius=3;
    [saveBtn addTarget:self action:@selector(clickSaveBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveBtn];
    
    //
    CGRect frame=self.frame;
    frame.size.height=saveBtn.origin.y+saveBtn.height+12;
    self.frame=frame;
}

#pragma mark - 点击保存按钮
-(void)clickSaveBtn{
    [AppUtils closeKeyboard];
    if ([companyTf.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"单位名称不能为空" inView:self.window];
    }else if ([companyAdr.text isEqualToString:@""]
              ||[companyAdr.text isEqualToString:@"选择省选择市选择区县"]){
        [AppUtils showSuccessMessage:@"请选择单位地址" inView:self.window];
    }else if ([cmpAdrDetail.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"单位详细地址不能为空" inView:self.window];
    }
//    else if ([cmpReceiver.text isEqualToString:@""]){
//        [AppUtils showSuccessMessage:@"单位收件人不能为空" inView:self.window];
//    }else if ([cmpPhone.text isEqualToString:@""]){
//        [AppUtils showSuccessMessage:@"联系电话不能为空" inView:self.window];
//    }
    else if ([homeAdr.text isEqualToString:@""]
              ||[homeAdr.text isEqualToString:@"选择省选择市选择区县"]){
        [AppUtils showSuccessMessage:@"请选择家庭地址" inView:self.window];
    }else if ([homeDetailAdr.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"家庭详细地址不能为空" inView:self.window];
    }else{
        [self.saveDelegate saveAddressWithCompany:companyTf.text companyAddress:companyAdr.text companyDetailAdr:cmpAdrDetail.text companyReceiver:@"无" companyPhone:@"15678065811" homeAddress:homeAdr.text homeDetailAdr:homeDetailAdr.text homeReceiver:@"" homePhone:@"15678065711"];
    }
}

-(void)selectCompanyAddress{
    [self.delegate showSelectedCompanyAddressSheet];
}

-(void)selectHomeAddress{
    [self.HomeDelegate showSelectedHomeAddressSheet];
}

#pragma mark - Notification Method
-(void)updateCompanyAddress:(NSNotification *)noti{
    NSArray *areaArr=noti.object;
    cpnProvince=areaArr[0];
    cpnCity=areaArr[1];
    cpnDistrict=areaArr[2];
    NSString *areaStr=[NSString stringWithFormat:@"%@%@%@",cpnProvince,cpnCity,cpnDistrict];
    companyAdr.text=areaStr;
}

-(void)updateHomeAddress:(NSNotification *)noti{
    NSArray *areaArr=noti.object;
    homeProvince=areaArr[0];
    homeCity=areaArr[1];
    homeDistrict=areaArr[2];
    NSString *areaStr=[NSString stringWithFormat:@"%@%@%@",homeProvince,homeCity,homeDistrict];
    homeAdr.text=areaStr;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateTXDZ:(NSNotification *)noti{
    NSArray *arr=noti.object;
    [self reflushDataForArray:arr validateStatus:@""];
}

-(void)reflushDataForArray:(NSArray *)array validateStatus:(NSString *)status{
    /*
     UITextField *nameTf;        //单位名称
     UITextField *companyAdr;    //单位地址
     UITextField *cmpAdrDetail;  //单位详细地址
     UITextField *cmpReceiver;   //单位收件人
     UITextField *cmpPhone;      //办公电话
     UITextField *homeAdr;
     UITextField *homeDetailAdr;
     */
    for (int i=0; i<array.count; i++) {
        NSString *str=array[i];
        if (i==0) {
            companyTf.text=str;
        }else if (i==1){
            NSArray *addArr=array[1];
            NSString *adtStr=@"";
            for (NSString *str in addArr) {
                adtStr=[adtStr stringByAppendingString:str];
            }
            companyAdr.text=adtStr;
        }else if (i==2){
            cmpAdrDetail.text=str;
        }else if (i==3){
            //cmpReceiver.text=str;
        }else if (i==8){
            //cmpPhone.text=str;
        }else if (i==5){
            NSArray *addArr=array[5];
            NSString *adtStr=@"";
            for (NSString *str in addArr) {
                adtStr=[adtStr stringByAppendingString:str];
            }
            homeAdr.text=adtStr;
        }else if (i==6){
            homeDetailAdr.text=str;
        }
    }
    
    if ([status isEqualToString:@"2"]||
        [status isEqualToString:@"3"]) {
         companyTf.enabled=NO;
         companyTf.textColor=[UIColor grayColor];
         companyTf.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
         companyAdr.enabled=NO;
         companyAdr.textColor=[UIColor grayColor];
         companyAdr.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
         cmpAdrDetail.enabled=NO;
         cmpAdrDetail.textColor=[UIColor grayColor];
         cmpAdrDetail.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
//         cmpReceiver.enabled=NO;
//         cmpReceiver.textColor=[UIColor grayColor];
//         cmpReceiver.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
//         cmpPhone.enabled=NO;
//         cmpPhone.textColor=[UIColor grayColor];
//         cmpPhone.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
         homeAdr.enabled=NO;
         homeAdr.textColor=[UIColor grayColor];
         homeAdr.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
         homeDetailAdr.enabled=NO;
         homeDetailAdr.textColor=[UIColor grayColor];
         homeDetailAdr.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
         
         companyLabelView.userInteractionEnabled=NO;
         sbView.userInteractionEnabled=NO;
         
         
         CGRect frame=self.frame;
         frame.size.height=nsrView.origin.y+nsrView.height+12;
         self.frame=frame;
         saveBtn.hidden=YES;
    }else{
        CGRect frame=self.frame;
        frame.size.height=saveBtn.origin.y+saveBtn.height+12;
        self.frame=frame;
        saveBtn.hidden=NO;
    }
    //DSLog(@"%f",self.frame.size.height);
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
