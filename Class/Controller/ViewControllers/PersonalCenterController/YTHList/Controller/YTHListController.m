//
//  YTHListController.m
//  youxia
//
//  Created by mac on 2017/4/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "YTHListController.h"
#import "YTHListModel.h"
#import "YTHListHeaderCell.h"

@interface YTHListController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation YTHListController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"合同列表";
    [self.view addSubview:navBar];
    
    [self setUpTableView];
    
    [self yhtRequest];
}

#pragma mark - setup TableView
-(void)setUpTableView{
    self.automaticallyAdjustsScrollViewInsets = NO; //解决表无故偏移
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"YTHListHeaderCell" bundle:nil] forCellReuseIdentifier:@"YTHListHeaderCell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return dataArr.count;
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 76;
    }else{
        return 48;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YTHListHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"YTHListHeaderCell"];
        if (!cell) {
            cell=[[YTHListHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"YTHListHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        if (!cell) {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Request
-(void)yhtRequest{
    
    dataArr=[NSMutableArray array];
    
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me" forKey:@"mod"];
    [dict setObject:@"get_ht_list" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@--",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self yhtRequest];
            });
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
            if ([codeStr isEqualToString:@"0"]) {
                id resObj=responseObject[@"retData"][@"result"];
                if ([resObj isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *dic in resObj) {
                        YTHListModel *model=[YTHListModel new];
                        [model jsonDataForDictionary:dic];
                        [dataArr addObject:model];
                    }
                    NSLog(@"%@",dataArr);
                }
            }else{
                NSLog(@"%@",responseObject[@"retData"][@"msg"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
