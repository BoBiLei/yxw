//
//  YTHListModel.h
//  youxia
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YTHListModel : NSObject

@property (nonatomic, copy) NSString *buzhong_ht;
@property (nonatomic, copy) NSString *buzhong_ht_status;
@property (nonatomic, copy) NSString *error;
@property (nonatomic, copy) NSString *flight_ht_status;
@property (nonatomic, copy) NSString *hotel_ht_status;
@property (nonatomic, copy) NSString *ht_flight;
@property (nonatomic, copy) NSString *ht_fq;
@property (nonatomic, copy) NSString *ht_hotel;
@property (nonatomic, copy) NSString *msg;
@property (nonatomic, copy) NSString *orderid;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *yx_ht_status;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
