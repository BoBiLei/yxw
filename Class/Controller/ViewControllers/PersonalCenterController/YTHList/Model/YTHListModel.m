//
//  YTHListModel.m
//  youxia
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "YTHListModel.h"

@implementation YTHListModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    //
    if (![dic[@"buzhong_ht"] isKindOfClass:[NSNull class]]) {
        self.buzhong_ht=[NSString stringWithFormat:@"%@",dic[@"buzhong_ht"]];
    }else{
        self.buzhong_ht=@"";
    }
    //
    if (![dic[@"buzhong_ht_status"] isKindOfClass:[NSNull class]]) {
        self.buzhong_ht_status=[NSString stringWithFormat:@"%@",dic[@"buzhong_ht_status"]];
    }else{
        self.buzhong_ht_status=@"";
    }
    //
    if (![dic[@"error"] isKindOfClass:[NSNull class]]) {
        self.error=[NSString stringWithFormat:@"%@",dic[@"error"]];
    }else{
        self.error=@"";
    }
    //
    if (![dic[@"flight_ht_status"] isKindOfClass:[NSNull class]]) {
        self.flight_ht_status=[NSString stringWithFormat:@"%@",dic[@"flight_ht_status"]];
    }else{
        self.flight_ht_status=@"";
    }
    //
    if (![dic[@"hotel_ht_status"] isKindOfClass:[NSNull class]]) {
        self.hotel_ht_status=[NSString stringWithFormat:@"%@",dic[@"hotel_ht_status"]];
    }else{
        self.hotel_ht_status=@"";
    }
    //
    if (![dic[@"ht_flight"] isKindOfClass:[NSNull class]]) {
        self.ht_flight=[NSString stringWithFormat:@"%@",dic[@"ht_flight"]];
    }else{
        self.ht_flight=@"";
    }
    //
    if (![dic[@"ht_fq"] isKindOfClass:[NSNull class]]) {
        self.ht_fq=[NSString stringWithFormat:@"%@",dic[@"ht_fq"]];
    }else{
        self.ht_fq=@"";
    }
    //
    if (![dic[@"ht_hotel"] isKindOfClass:[NSNull class]]) {
        self.ht_hotel=[NSString stringWithFormat:@"%@",dic[@"ht_hotel"]];
    }else{
        self.ht_hotel=@"";
    }
    //
    if (![dic[@"msg"] isKindOfClass:[NSNull class]]) {
        self.msg=[NSString stringWithFormat:@"%@",dic[@"msg"]];
    }else{
        self.msg=@"";
    }
    //
    if (![dic[@"orderid"] isKindOfClass:[NSNull class]]) {
        self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    }else{
        self.orderid=@"";
    }
    //
    if (![dic[@"title"] isKindOfClass:[NSNull class]]) {
        self.title=[NSString stringWithFormat:@"%@",dic[@"title"]];
    }else{
        self.title=@"";
    }
    //
    if (![dic[@"token"] isKindOfClass:[NSNull class]]) {
        self.token=[NSString stringWithFormat:@"%@",dic[@"token"]];
    }else{
        self.token=@"";
    }
    //
    if (![dic[@"yx_ht_status"] isKindOfClass:[NSNull class]]) {
        self.yx_ht_status=[NSString stringWithFormat:@"%@",dic[@"yx_ht_status"]];
    }else{
        self.yx_ht_status=@"";
    }
}

@end
