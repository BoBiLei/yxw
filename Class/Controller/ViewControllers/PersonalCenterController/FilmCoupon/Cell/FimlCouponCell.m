//
//  FimlCouponCell.m
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FimlCouponCell.h"

@implementation FimlCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.mainView.layer.cornerRadius=6;
    self.mainView.layer.borderWidth=0.9f;
    self.mainView.layer.borderColor=[UIColor colorWithHexString:@"c7c7c7"].CGColor;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(FilmCouponModel *)model{
    switch (model.used_status.intValue) {
        case 0:
            self.statusImgv.image=[UIImage imageNamed:@""];
            break;
        case 1:
            self.statusImgv.image=[UIImage imageNamed:@""];
            break;
        case 2:
            self.statusImgv.image=[UIImage imageNamed:@"filmcoupon_hanuse"];
            break;
        default:
            self.statusImgv.image=[UIImage imageNamed:@""];
            break;
    }
    self.timeLabel.text=[NSString stringWithFormat:@"使用期限 %@",model.duetime];
}

@end
