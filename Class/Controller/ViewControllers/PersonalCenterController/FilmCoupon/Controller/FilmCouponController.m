//
//  FilmCouponController.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmCouponController.h"
#import "FimlCouponCell.h"
#import "FilmCouponModel.h"
#import "CustomIOSAlertView.h"

@interface FilmCouponController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation FilmCouponController{
    NSMutableArray *sourceArr;
    UITableView *myTable;
    MJRefreshNormalHeader *refleshHeader;
    
    //
    UITextField *couponNumTF;
    CustomIOSAlertView *validaAlertView;
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavBar];
    
    [self setUI];
    
    [self dataRequest];
}

-(void)addNavBar{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [self.view addSubview:view];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:back];
    
    UIButton *titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    titleLabel.titleLabel.font=[UIFont systemFontOfSize:17];
    [titleLabel setTitle:@"电影票优惠券" forState:UIControlStateNormal];
    [view addSubview:titleLabel];
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.frame=CGRectMake(SCREENSIZE.width-56, 20, 44.f, 44.f);
    [addBtn setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [addBtn setTitle:@"添加" forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(clickAddBtn) forControlEvents:UIControlEventTouchUpInside];
    addBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [view addSubview:addBtn];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [myTable registerNib:[UINib nibWithNibName:@"FimlCouponCell" bundle:nil] forCellReuseIdentifier:@"FimlCouponCell"];
    [self.view addSubview:myTable];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dataRequest];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    
    // 设置字体
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    myTable.mj_header = refleshHeader;
}

#pragma mark - 点击添加
-(void)clickAddBtn{
    validaAlertView = [[CustomIOSAlertView alloc] init];
    validaAlertView.containerView.layer.cornerRadius=12;
    [validaAlertView setContainerView:[self createAlertView]];
    [validaAlertView setButtonTitles:nil];
    [validaAlertView setUseMotionEffects:true];
    [validaAlertView show];
}

#pragma mark - 创建弹出验证码提示框

-(UIView *)createAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    view.layer.cornerRadius=8;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 10, view.width-20, 24)];
    tipLabel.textAlignment=NSTextAlignmentLeft;
    tipLabel.font=[UIFont systemFontOfSize:17];
    tipLabel.textColor=[UIColor colorWithHexString:@"000000"];
    tipLabel.text=@"添加优惠券";
    [view addSubview:tipLabel];
    
#pragma mark x按钮
    UIButton *closeImg=[UIButton buttonWithType:UIButtonTypeCustom] ;
    closeImg.frame=CGRectMake(view.width-40, 4, 36, 36);
    [closeImg setImage:[UIImage imageNamed:@"add_coupon"] forState:UIControlStateNormal];
    [closeImg addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeImg];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"eeeeee"] height:1.0f firstPoint:CGPointMake(0, tipLabel.origin.y*2+tipLabel.height) endPoint:CGPointMake(view.width, tipLabel.origin.y*2+tipLabel.height)];
    
#pragma mark 验证码View
    UIView *volatileView=[[UIView alloc] initWithFrame:CGRectMake(24, tipLabel.origin.y*2+tipLabel.height+22, view.width-48, 42)];
    volatileView.layer.cornerRadius=1;
    volatileView.backgroundColor=[UIColor colorWithHexString:@"#ffffff"];
    volatileView.layer.borderWidth=0.5f;
    volatileView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [view addSubview:volatileView];
    
    couponNumTF=[[UITextField alloc] initWithFrame:CGRectMake(8, 0, volatileView.width-14, volatileView.height)];
    couponNumTF.tintColor=[UIColor colorWithHexString:@"#55b2f0"];
    couponNumTF.placeholder=@"请输入优惠券码";
    couponNumTF.font=[UIFont systemFontOfSize:15];
    couponNumTF.textColor=[UIColor colorWithHexString:@"#363636"];
    couponNumTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [volatileView addSubview:couponNumTF];
    
    UIButton *reflBtn=[UIButton buttonWithType:UIButtonTypeCustom] ;
    reflBtn.layer.cornerRadius=3;
    reflBtn.frame=CGRectMake(volatileView.origin.x, volatileView.origin.y+volatileView.height+30, volatileView.width, volatileView.height);
    reflBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [reflBtn setTitle:@"提 交" forState:UIControlStateNormal];
    reflBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [reflBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [reflBtn addTarget:self action:@selector(clickRefleBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:reflBtn];
    
    //
    CGFloat lastYH=reflBtn.origin.y+reflBtn.height+20;
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+16;
    view.frame=frame;
    
    return view;
}

-(void)clickCloseBtn{
    [AppUtils closeKeyboard];
    [validaAlertView close];
}

//点击提交
-(void)clickRefleBtn{
    NSLog(@"点击提交");
    if ([couponNumTF.text isEqualToString:@""]||couponNumTF.text==nil) {
        [AppUtils showSuccessMessage:@"请输入优惠券码" inView:validaAlertView.dialogView];
    }else{
        [AppUtils closeKeyboard];
        [validaAlertView close];
        [self addCouponRequest];
    }
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sourceArr.count;
//    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 14;
    }else{
        return 0.000001f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 128;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FimlCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FimlCouponCell"];
    if (!cell) {
        cell=[[FimlCouponCell alloc]init];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.section<sourceArr.count){
        FilmCouponModel *model=sourceArr[indexPath.section];
        [cell reflushDataForModel:model];
    }
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - request

-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    sourceArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"coupon_list" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"coupon_list",
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSArray class]]) {
                NSArray *arr=responseObject[@"retData"];
                for (NSDictionary *dic in arr) {
                    FilmCouponModel *model=[FilmCouponModel new];
                    [model jsonDataForDictionary:dic];
                    [sourceArr addObject:model];
                }
                [myTable.mj_header endRefreshing];
                [myTable reloadData];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [myTable.mj_header endRefreshing];
        DSLog(@"%@",error);
    }];
}

-(void)addCouponRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_order",
                         @"code":@"add_coupon",
                         @"number":couponNumTF.text,
                         @"userid":[AppUtils getValueWithKey:User_ID]
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:@"成功添加一张优惠券" inView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dataRequest];
            });
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
