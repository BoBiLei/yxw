//
//  FilmCouponModel.h
//  youxia
//
//  Created by mac on 2016/12/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmCouponModel : NSObject

@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *used_price;
@property (nonatomic, copy) NSString *part_used;
@property (nonatomic, copy) NSString *usetime;
@property (nonatomic, copy) NSString *duetime;
@property (nonatomic, copy) NSString *orderid;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *addtime;
@property (nonatomic, copy) NSString *used_rule;
@property (nonatomic, copy) NSString *used_status;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
