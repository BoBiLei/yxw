//
//  FilmCouponModel.m
//  youxia
//
//  Created by mac on 2016/12/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmCouponModel.h"

@implementation FilmCouponModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.cId=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.number=[NSString stringWithFormat:@"%@",dic[@"number"]];
    self.password=[NSString stringWithFormat:@"%@",dic[@"password"]];
    self.price=[NSString stringWithFormat:@"%@",dic[@"price"]];
    self.used_price=[NSString stringWithFormat:@"%@",dic[@"used_price"]];
    self.part_used=[NSString stringWithFormat:@"%@",dic[@"part_used"]];
    self.usetime=[NSString stringWithFormat:@"%@",dic[@"usetime"]];
    self.duetime=[NSString stringWithFormat:@"%@",dic[@"duetime"]];
    self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.uid=[NSString stringWithFormat:@"%@",dic[@"uid"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.addtime=[NSString stringWithFormat:@"%@",dic[@"addtime"]];
    self.used_rule=[NSString stringWithFormat:@"%@",dic[@"used_rule"]];
    self.used_status=[NSString stringWithFormat:@"%@",dic[@"used_status"]];
}

@end
