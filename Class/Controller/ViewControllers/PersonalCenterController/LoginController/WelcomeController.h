//
//  LoginController.h
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

@interface WelcomeController : UIViewController

@property (nonatomic, assign) BOOL isGoIslandDetail;

@property (nonatomic, assign) BOOL isGoBuyMovie;

@property (nonatomic, assign) BOOL isShop;

@end
