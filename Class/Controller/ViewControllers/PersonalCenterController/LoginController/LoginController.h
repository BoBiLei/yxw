//
//  LoginController.h
//  youxia
//
//  Created by mac on 16/4/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

typedef void(^LoginSuccess)(BOOL isLoginSuccess);

@interface LoginController : UIViewController

@property (nonatomic, assign) BOOL isGoIslandDetail;

@property (nonatomic, assign) BOOL isGoBuyMovie;

- (void)loginResult:(LoginSuccess)isLoginSuccess;

@end
