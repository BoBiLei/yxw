//
//  LoginController.m
//  youxia
//
//  Created by mac on 16/4/27.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "LoginController.h"

@interface LoginController ()

@end

@implementation LoginController{
    UIView *mainView;
    UITextField *phoneTf;
    UITextField *passTf;
    UIButton *loginBtn;
    
    LoginSuccess _isLoginSuccess;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"abcd"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    NSLog(@"%@",dict);
    //
    //NavigationBar
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"登陆";
    [self.view addSubview:navBar];
    
    //MainView
    mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    mainView.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [self.view addSubview:mainView];
    
    //
    phoneTf=[[UITextField alloc]initWithFrame:CGRectMake(30, 40, SCREENSIZE.width-60, 32)];
    phoneTf.placeholder=@"请输入手机号";
    
    //Login_Type=1--手机登陆  2--第三方登陆
    if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
        //
        if(![[AppUtils getValueWithKey:User_Account] isEqualToString:@""]){
            phoneTf.text=[AppUtils getValueWithKey:User_Account];
        }
    }else{
        //
        if(![[AppUtils getValueWithKey:User_Account] isEqualToString:@""]){
            phoneTf.text=@"";
        }
    }
    
    phoneTf.font=kFontSize16;
    phoneTf.textColor=[UIColor whiteColor];
    [phoneTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    phoneTf.tag=0;
    phoneTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTf.keyboardType=UIKeyboardTypeNumberPad;
    phoneTf.tintColor=[UIColor whiteColor];
    [phoneTf addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainView addSubview:phoneTf];
    
    UIView *line01=[[UIView alloc] initWithFrame:CGRectMake(phoneTf.origin.x, phoneTf.origin.y+phoneTf.height, phoneTf.width, 0.6f)];
    line01.backgroundColor=[UIColor whiteColor];
    [mainView addSubview:line01];
    
    //
    passTf=[[UITextField alloc]initWithFrame:CGRectMake(phoneTf.origin.x, line01.origin.y+line01.height+24, phoneTf.width, phoneTf.height)];
    passTf.placeholder=@"输入登陆密码,至少6位";
    passTf.font=kFontSize16;
    passTf.textColor=[UIColor whiteColor];
    [passTf setValue:[UIColor colorWithHexString:@"#adcae2"] forKeyPath:@"_placeholderLabel.textColor"];
    passTf.tag=1;
    passTf.secureTextEntry=YES;
    passTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    passTf.tintColor=[UIColor whiteColor];
    [passTf addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
    [mainView addSubview:passTf];
    
    UIView *line02=[[UIView alloc] initWithFrame:CGRectMake(passTf.origin.x, passTf.origin.y+passTf.height, passTf.width, 0.6f)];
    line02.backgroundColor=[UIColor whiteColor];
    [mainView addSubview:line02];
    
    
    //button
    loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame=CGRectMake(0, 0, SCREENSIZE.width/3+20, 42);
    loginBtn.center=CGPointMake(SCREENSIZE.width/2, line02.center.y+70);
    loginBtn.layer.cornerRadius=loginBtn.height/2;
    loginBtn.layer.borderWidth=0.6f;
    loginBtn.layer.borderColor=[UIColor colorWithHexString:@"#adcae2"].CGColor;
    loginBtn.enabled=NO;
    [loginBtn setTitle:@"立即登陆" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor colorWithHexString:@"#adcae2"] forState:UIControlStateNormal];
    loginBtn.titleLabel.font=kFontSize16;
    [loginBtn addTarget:self action:@selector(clickLogin) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:loginBtn];
}

#pragma mark - textfield
- (void)phoneDidChange:(id) sender {
    
    if ([phoneTf.text isEqualToString:@""]||
        [passTf.text isEqualToString:@""]) {
        loginBtn.layer.borderColor=[UIColor colorWithHexString:@"#adcae2"].CGColor;
        [loginBtn setTitleColor:[UIColor colorWithHexString:@"#adcae2"] forState:UIControlStateNormal];
        loginBtn.enabled=NO;
    }else{
        loginBtn.layer.borderColor=[UIColor whiteColor].CGColor;
        [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        loginBtn.enabled=YES;
    }
}

- (void)loginResult:(LoginSuccess)isLoginSuccess{
    _isLoginSuccess=isLoginSuccess;
}

-(void)clickLogin{
    [AppUtils closeKeyboard];
//    if ([AppUtils checkPassworkLength:passTf.text]) {
//        [self requestForLogin];
//    }else{
//        [AppUtils showSuccessMessage:@"至少输入6位密码" inView:mainView];
//    }
    [self requestForLogin];
}

#pragma mark - Request 请求
-(void)requestForLogin{
    [AppUtils showProgressMessage:@"正在登录…" inView:mainView];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"account_t" forKey:@"mod"];
    [dict setObject:@"Login" forKey:@"code"];
    [dict setObject:@"done" forKey:@"op"];
    [dict setObject:phoneTf.text forKey:@"username"];
    [dict setObject:passTf.text forKey:@"password"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:mainView];
        DSLog(@"%@--",responseObject);
        
        BOOL isOk;
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            isOk=YES;
            [TalkingData trackEvent:@"手机号码登录"];
            
            //手机登录类型--1
            [AppUtils saveValue:@"1" forKey:Login_Type];
            [self updateUIAndLoginStatusWithDic:responseObject[@"retData"]];
        }else{
            isOk=NO;
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"result"] inView:mainView];
        }
        if (_isLoginSuccess) {
            _isLoginSuccess(isOk);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 保存账号、保存密码。标识程序登录状态
//=====================================
//登录成功保存账号、保存密码。标识程序登录状态。
//保存用户的信息：id、头像、用户名、可用余额
//发送通知改变tabbar index
//=====================================
-(void)updateUIAndLoginStatusWithDic:(NSDictionary *)dic{
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.isLogin=YES;
    [AppUtils saveValue:dic[@"user_id"] forKey:User_ID];
    [AppUtils saveValue:phoneTf.text forKey:User_Account];
    [AppUtils saveValue:phoneTf.text forKey:User_Name];
    [AppUtils saveValue:passTf.text forKey:User_Pass];
    [AppUtils saveValue:dic[@"user_list"][@"face"] forKey:User_Photo];
    
    [AppUtils saveValue:dic[@"user_list"][@"phone"] forKey:User_Phone];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Name object:nil];
    
    NSString *roleStr=dic[@"user_list"][@"role_id"];
    if ([roleStr isEqualToString:@"11"]) {
        [AppUtils saveValue:@"1" forKey:VIP_Flag];
    }else{
        [AppUtils saveValue:@"0" forKey:VIP_Flag];
    }
    
    if (_isGoIslandDetail) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoIslandDetail" object:nil];
    }else if(_isGoBuyMovie){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBuyMovie_Notification" object:nil];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:LoginSuccessNotifition object:nil];
    }
}

@end
