//
//  LoginController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "WelcomeController.h"
#import "RegisterController.h"
#import "ForgetPassController.h"
#import "LoginController.h"
#import "BindingPhoneController.h"
#import "WXApiObject.h"
#import "WXApi.h"

@interface WelcomeController ()

@end

@implementation WelcomeController{
    UIButton *loginBtn;
    UIView *mainView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [AppUtils closeKeyboard];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpCustomSearBar];
    
    mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    mainView.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [self.view addSubview:mainView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleForRegistSuccess) name:RegistSuccessNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissViewController) name:LoginSuccessNotifition object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetPwdSuccess) name:@"ResetPassWordSuccess" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxLoginSuccess:) name:@"WXAuth_SUCCESS" object:nil];
    
    
    
    [self setUpUI];
}

-(void)wxLoginSuccess:(NSNotification *)noti{
    SendAuthResp *obj=noti.object;
    [self wxLoginRequest:obj.code];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}


-(void)clickReturnBtn{
    [self dismissViewController];
}

-(void)handleForRegistSuccess{
    LoginController *loginCtr=[[LoginController alloc]init];
    loginCtr.isGoIslandDetail=_isGoIslandDetail;
    loginCtr.isGoBuyMovie=_isGoBuyMovie;
    [self.navigationController pushViewController:loginCtr animated:YES];
}

-(void)dismissViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)resetPwdSuccess{
    LoginController *loginCtr=[[LoginController alloc]init];
    loginCtr.isGoIslandDetail=_isGoIslandDetail;
    loginCtr.isGoBuyMovie=_isGoBuyMovie;
    [self.navigationController pushViewController:loginCtr animated:YES];
}

-(void)turnBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - setup UI
-(void)setUpUI{
    
    //WelcomeLabel
    UILabel *welcomeLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 30, SCREENSIZE.width, 23)];
    welcomeLabel.textColor=[UIColor whiteColor];
    welcomeLabel.textAlignment=NSTextAlignmentCenter;
    welcomeLabel.text=@"欢迎来到游侠！";
    welcomeLabel.font=kFontSize19;
    [mainView addSubview:welcomeLabel];
    
    
    //第三方登录按钮
    CGFloat btnX=12;
    CGFloat btnY=welcomeLabel.origin.y+welcomeLabel.height+24;
    UIView *line;
    if ([WXApi isWXAppInstalled]) {
        CGFloat btnW=(SCREENSIZE.width-2*btnX)/3;
        CGFloat btnH=btnW;
        for (int i=0; i<3; i++) {
            CGFloat aaX=btnX+i*btnW;
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(aaX, btnY, btnW, btnH);
            NSString *imgStr;
            if (i==0) {
                imgStr=@"login_wechat";
            }else if (i==1){
                imgStr=@"login_sina";
            }else{
                imgStr=@"login_qq";
            }
            [btn setImage:[UIImage imageNamed:imgStr] forState:UIControlStateNormal];
            [mainView addSubview:btn];
            
            btn.tag=i;
            [btn addTarget:self action:@selector(clickAutoBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //
            if (i!=2) {
                UIView *line=[[UIView alloc] initWithFrame:CGRectMake(btn.origin.x+btn.width, btn.origin.y+30, 0.6f, btn.height-60)];
                line.backgroundColor=[UIColor whiteColor];
                [mainView addSubview:line];
            }
            
            //
            line=[[UIView alloc] initWithFrame:CGRectMake(44, btnY+btnH+26, SCREENSIZE.width-88, 0.7f)];
            line.backgroundColor=[UIColor whiteColor];
            [mainView addSubview:line];
        }
    }else{
        CGFloat btnW=(SCREENSIZE.width-2*btnX)/2;
        CGFloat btnH=(SCREENSIZE.width-2*btnX)/3;
        for (int i=0; i<2; i++) {
            CGFloat aaX=btnX+i*btnW;
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(aaX, btnY, btnW, btnH);
            NSString *imgStr;
            if (i==0){
                imgStr=@"login_sina";
            }else{
                imgStr=@"login_qq";
            }
            [btn setImage:[UIImage imageNamed:imgStr] forState:UIControlStateNormal];
            [mainView addSubview:btn];
            
            btn.tag=i+1;
            [btn addTarget:self action:@selector(clickAutoBtn:) forControlEvents:UIControlEventTouchUpInside];
            //
            if (i!=1) {
                UIView *line=[[UIView alloc] initWithFrame:CGRectMake(btn.origin.x+btn.width, btn.origin.y+30, 0.6f, btn.height-60)];
                line.backgroundColor=[UIColor whiteColor];
                [mainView addSubview:line];
            }
            
            //
            line=[[UIView alloc] initWithFrame:CGRectMake(44, btnY+btnH+26, SCREENSIZE.width-88, 0.7f)];
            line.backgroundColor=[UIColor whiteColor];
            [mainView addSubview:line];
        }
    }
    
    NSString *loginTipStr=@"使用游侠账号登陆";
    CGFloat tipFont=16;
    CGSize tipSize=[AppUtils getStringSize:loginTipStr withFont:tipFont];
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, tipSize.width+24, 21)];
    tipLabel.backgroundColor=mainView.backgroundColor;
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.textColor=[UIColor whiteColor];
    tipLabel.text=loginTipStr;
    tipLabel.font=[UIFont systemFontOfSize:tipFont];
    tipLabel.center=CGPointMake(line.center.x, line.center.y);
    [mainView addSubview:tipLabel];
    
    
    //button
    loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame=CGRectMake(line.origin.x, tipLabel.frame.origin.y+tipLabel.frame.size.height+24, SCREENSIZE.width-line.origin.x*2, 42);
    loginBtn.layer.cornerRadius=loginBtn.height/2;
    loginBtn.backgroundColor=[UIColor whiteColor];
    [loginBtn setTitle:@"通过手机号码登陆" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor colorWithHexString:@"#0080c5"] forState:UIControlStateNormal];
    loginBtn.titleLabel.font=[UIFont systemFontOfSize:tipFont];
    [loginBtn addTarget:self action:@selector(clickLogin) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:loginBtn];
    
    
    //
    CGFloat btmBtnH=52;
    
    UIView *btmLine=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-(btmBtnH+64), SCREENSIZE.width, 0.7f)];
    btmLine.backgroundColor=[UIColor whiteColor];
    [mainView addSubview:btmLine];
    
    //底部两个按钮
    CGFloat btmBtnX=0;
    CGFloat btmBtnY=btmLine.origin.y+btmLine.height;
    CGFloat btmBtnW=SCREENSIZE.width/2;
    
    for (int i=0; i<2; i++) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i;
        btn.frame=CGRectMake(btmBtnX*i+i*btmBtnW, btmBtnY, btmBtnW, btmBtnH);
        [mainView addSubview:btn];
        
        NSString *titStr;
        if (i==0) {
            titStr=@"忘记密码";
        }else{
            titStr=@"注册账号";
        }
        btn.titleLabel.font=[UIFont systemFontOfSize:tipFont];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:titStr forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickBtmBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //
        if (i!=1) {
            UIView *line=[[UIView alloc] initWithFrame:CGRectMake(btn.origin.x+btn.width, btn.origin.y, 0.6f, btn.height-8)];
            line.backgroundColor=[UIColor whiteColor];
            [mainView addSubview:line];
        }
    }
}

#pragma mark - button event

//微信、qq、新浪登录
-(void)clickAutoBtn:(id)sender{
    UIButton *btn=sender;
    switch (btn.tag) {
        case 0:
        {
            [self wxLogin];
            //            [ShareSDK getUserInfo:SSDKPlatformTypeWechat
            //                   onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error)
            //             {
            //                 DSLog(@"%@",user.uid);
            //                 if (state == SSDKResponseStateSuccess)
            //                 {
            //                     [self requestLoginWithType:@"wxphone" openId:user.uid faceImg:user.icon nickName:user.nickname token:user.credential.token];
            //                 }else
            //                 {
            //                     DSLog(@"%@",error);
            //                 }
            //             }];
        }
            break;
        case 1:
        {
            [ShareSDK getUserInfo:SSDKPlatformTypeSinaWeibo
                   onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error)
             {
                 if (state == SSDKResponseStateSuccess)
                 {
                     [self requestLoginWithType:@"xlweb" openId:user.uid faceImg:user.icon nickName:user.nickname token:user.credential.token];
                 }else
                 {
                     DSLog(@"%@",error);
                 }
             }];
        }
            break;
        case 2:
        {
            [ShareSDK getUserInfo:SSDKPlatformTypeQQ
                   onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error)
             {
                 if (state == SSDKResponseStateSuccess)
                 {
                     [self requestLoginWithType:@"qqweb" openId:user.uid faceImg:user.icon nickName:user.nickname token:user.credential.token];
                 }else
                 {
                     DSLog(@"%@",error);
                 }
             }];
        }
            break;
        default:
            break;
    }
}

-(void)wxLogin{
    [WXApi registerApp:@"wxc60eab93d2dba545"];
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo"; // @"post_timeline,sns"
    [WXApi sendReq:req];
}

-(void)clickBtmBtn:(id)sender{
    UIButton *btn=sender;
    if (btn.tag==0) {
        ForgetPassController *forgetPwd=[[ForgetPassController alloc]init];
        [self.navigationController pushViewController:forgetPwd animated:YES];
    }else{
        RegisterController *registCtr=[[RegisterController alloc]init];
        [self.navigationController pushViewController:registCtr animated:YES];
    }
}

-(void)clickLogin{
    LoginController *loginCtr=[[LoginController alloc]init];
    loginCtr.isGoIslandDetail=_isGoIslandDetail;
    loginCtr.isGoBuyMovie=_isGoBuyMovie;
    [self.navigationController pushViewController:loginCtr animated:YES];
}

#pragma mark -  第三方登陆 Request 请求
-(void)requestLoginWithType:(NSString *)loginType openId:(NSString *)openId faceImg:(NSString *)faceImg nickName:(NSString *)nickName token:(NSString *)token{
    
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    [AppUtils showProgressMessage:@"正在登录…" inView:app.window];
    
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"is_nosign":@"1",
                         @"mod":@"account_t",
                         @"code":@"otherconn_login_ios",
                         @"type":loginType,
                         @"openid":openId,
                         @"face":faceImg,
                         @"nickname":nickName,
                         @"token":token
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        DSLog(@"%@--",dict);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:app.window];
        DSLog(@"*-*-*-%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            if ([loginType isEqualToString:@"wxphone"]) {
                [TalkingData trackEvent:@"微信登录"];
            }else if ([loginType isEqualToString:@"xlweb"]){
                [TalkingData trackEvent:@"新浪登录"];
            }else{
                [TalkingData trackEvent:@"QQ登录"];
            }
            
            [AppUtils saveValue:openId forKey:SSO_Login_UID];
            [AppUtils saveValue:loginType forKey:SSO_Login_Type];
            ////登录类型  1：手机号登录   2：第三方登录
            [AppUtils saveValue:@"2" forKey:Login_Type];
            [self updateUIAndLoginStatusWithDic:responseObject[@"retData"]];

        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"result"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:app.window];
        DSLog(@"%@",error);
    }];
}

-(void)wxLoginRequest:(NSString *)code{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"is_nosign":@"1",
                         @"mod":@"account_t",
                         @"code":@"thirdconn_login_iso",
                         @"wx_code":code,
                         @"type":@"wxphone"
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        //0516cFxR1uJZs71IF3xR1vIkxR16cFxO
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@--",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            [AppUtils saveValue:[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"unionid"]] forKey:SSO_Login_UID];
            [AppUtils saveValue:code forKey:yx_wx_Code];
            [AppUtils saveValue:@"wxphone" forKey:SSO_Login_Type];
            [AppUtils saveValue:@"2" forKey:Login_Type];
            [self updateUIAndLoginStatusWithDic:responseObject[@"retData"]];
            
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"result"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - 保存账号、保存密码。标识程序登录状态
//=====================================
//登录成功保存账号、保存密码。标识程序登录状态。
//保存用户的信息：id、头像、用户名
//发送通知改变tabbar index
//=====================================
-(void)updateUIAndLoginStatusWithDic:(NSDictionary *)dic{
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.isLogin=YES;
    [AppUtils saveValue:dic[@"uid"] forKey:User_ID];
    [AppUtils saveValue:dic[@"truename"] forKey:User_Name];
    [AppUtils saveValue:dic[@"face"] forKey:User_Photo];
    
    [AppUtils saveValue:dic[@"phone"] forKey:User_Phone];
    [AppUtils saveValue:dic[@"bind_user_id"] forKey:Bind_User_Id];
    
    //Is_NewPwd=0 未修改过的  =1修改的
    [AppUtils saveValue:dic[@"is_new_password"] forKey:Is_NewPwd];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Name object:nil];
    
    NSString *roleStr=dic[@"user_list"][@"role_id"];
    if ([roleStr isEqualToString:@"11"]) {
        [AppUtils saveValue:@"1" forKey:VIP_Flag];
    }else{
        [AppUtils saveValue:@"0" forKey:VIP_Flag];
    }
    
    if (([dic[@"phone"] isEqualToString:@""]&&
         [dic[@"bind_user_id"] isEqualToString:@""])||
        ([dic[@"phone"] isEqualToString:@""]&&
         [dic[@"bind_user_id"] isEqualToString:@"0"])) {
            [self.navigationController pushViewController:[BindingPhoneController new] animated:YES];
        }else{
            if (_isShop) {
                [self dismissViewControllerAnimated:YES completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ShopLoginSuccess" object:nil];
            }else{
                if (_isGoIslandDetail) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoIslandDetail" object:nil];
                }else if(_isGoBuyMovie){
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBuyMovie_Notification" object:nil];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:LoginSuccessNotifition object:nil];
                }
            }
        }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
