//
//  PSCHotelFeeDetailCell.m
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelFeeDetailCell.h"

@implementation PSCHotelFeeDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(HotelFeeDetailPriceListModel *)model{
    self.leftLabel.text=model.stayDate;
    self.rightLabel.text=[NSString stringWithFormat:@"￥%@",model.price];
}

@end
