//
//  PSCHotelFeeDetailCell.h
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelFeeDetailPriceListModel.h"

@interface PSCHotelFeeDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

-(void)reflushData:(HotelFeeDetailPriceListModel *)model;

@end
