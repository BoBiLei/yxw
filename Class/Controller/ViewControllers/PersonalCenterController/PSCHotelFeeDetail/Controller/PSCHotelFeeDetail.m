//
//  PSCHotelFeeDetail.m
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelFeeDetail.h"
#import "PSCHotelFeeDetailCell.h"
#import "PSCHotelFeeDetailModel.h"
#import "HotelFeeDetailPriceListModel.h"

@interface PSCHotelFeeDetail ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, strong) UITableView *myTable;

@end

@implementation PSCHotelFeeDetail{
    PSCHotelFeeDetailModel *model;
    NSMutableArray *dataArr;
}

+(id)initWithOrderId:(NSString *)orderId{
    PSCHotelFeeDetail *vc=[PSCHotelFeeDetail new];
    vc.orderId=orderId;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"费用明细";
    [self.view addSubview:navBar];
    
    [self dataRequest];
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
        _myTable.scrollEnabled=NO;
        _myTable.backgroundColor=[UIColor whiteColor];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"PSCHotelFeeDetailCell" bundle:nil] forCellReuseIdentifier:@"PSCHotelFeeDetailCell"];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [self.view addSubview:_myTable];
        
        //headView
        UIView *headView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 48)];
        _myTable.tableHeaderView=headView;
        
        UILabel *leftL=[[UILabel alloc] initWithFrame:CGRectMake(14, 0, 110, headView.height)];
        leftL.font=kFontSize16;
        leftL.textColor=[UIColor colorWithHexString:@"55b2f0"];
        leftL.text=@"房费";
        [headView addSubview:leftL];
        
        UILabel *rightL=[[UILabel alloc] initWithFrame:CGRectMake(leftL.origin.x+leftL.width+8, 0, SCREENSIZE.width-(leftL.origin.x*2+leftL.width+8), headView.height)];
        rightL.textAlignment=NSTextAlignmentRight;
        rightL.font=kFontSize16;
        rightL.textColor=[UIColor colorWithHexString:@"ffa800"];
        rightL.text=[NSString stringWithFormat:@"￥%@",model.payMoney];;
        [headView addSubview:rightL];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(14, headView.height-0.5, headView.width-28, 0.5)];
        line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [headView addSubview:line];
        
        //footerView
        UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 48)];
        _myTable.tableFooterView=footView;
        
        UIView *line02=[[UIView alloc] initWithFrame:CGRectMake(14, 0, SCREENSIZE.width-28, 0.5f)];
        line02.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
        [footView addSubview:line02];
        
        TTTAttributedLabel *ttPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(14, 0, SCREENSIZE.width-28, headView.height)];
        ttPriceLabel.font=kFontSize16;
        ttPriceLabel.textAlignment=NSTextAlignmentRight;
        [footView addSubview:ttPriceLabel];
        NSString *stageStr=[NSString stringWithFormat:@"总额：￥%@",model.payMoney];
        [ttPriceLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@",model.payMoney] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ffa800"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
    }
    return _myTable;
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?0.00000001f:0.000001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?0.00000001f:0.000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PSCHotelFeeDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PSCHotelFeeDetailCell"];
    if (!cell) {
        cell=[[PSCHotelFeeDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PSCHotelFeeDetailCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    HotelFeeDetailPriceListModel *pmodel=dataArr[indexPath.row];
    [cell reflushData:pmodel];
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - request
-(void)dataRequest{
    dataArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"hotel",
                         @"code":@"orderPriceDetail",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"orderid":_orderId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            model=[PSCHotelFeeDetailModel new];
            [model jsonToModel:responseObject[@"retDate"]];
            if (model.priceList.count!=0) {
                for (NSDictionary *dic in model.priceList) {
                    HotelFeeDetailPriceListModel *pModel=[HotelFeeDetailPriceListModel new];
                    [pModel jsonToModel:dic];
                    [dataArr addObject:pModel];
                }
                [self.myTable reloadData];
            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
