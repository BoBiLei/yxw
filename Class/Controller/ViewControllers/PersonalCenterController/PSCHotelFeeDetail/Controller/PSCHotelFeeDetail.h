//
//  PSCHotelFeeDetail.h
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//  费用明细

#import <UIKit/UIKit.h>

@interface PSCHotelFeeDetail : UIViewController

+(id)initWithOrderId:(NSString *)orderId;

@end
