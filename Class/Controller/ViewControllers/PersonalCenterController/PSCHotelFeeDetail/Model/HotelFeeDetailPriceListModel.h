//
//  HotelFeeDetailPriceListModel.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelFeeDetailPriceListModel : NSObject

@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *stayDate;

-(void)jsonToModel:(NSDictionary *)dic;

@end
