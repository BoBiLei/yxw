//
//  HotelFeeDetailPriceListModel.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelFeeDetailPriceListModel.h"

@implementation HotelFeeDetailPriceListModel


-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"Price"] isKindOfClass:[NSNull class]]) {
        self.price=@"";
    }else{
        self.price=dic[@"Price"];
    }
    
    if ([dic[@"StayDate"] isKindOfClass:[NSNull class]]) {
        self.stayDate=@"";
    }else{
        self.stayDate=dic[@"StayDate"];
    }
}

@end
