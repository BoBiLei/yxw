//
//  PSCHotelFeeDetailModel.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSCHotelFeeDetailModel : NSObject

@property (nonatomic, copy) NSString *payMoney;
@property (nonatomic, copy) NSArray *priceList;

-(void)jsonToModel:(NSDictionary *)dic;

@end
