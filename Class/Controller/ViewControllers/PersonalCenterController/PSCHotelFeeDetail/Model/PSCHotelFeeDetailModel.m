//
//  PSCHotelFeeDetailModel.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "PSCHotelFeeDetailModel.h"

@implementation PSCHotelFeeDetailModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"payMoney"] isKindOfClass:[NSNull class]]) {
        self.payMoney=@"0";
    }else{
        self.payMoney=dic[@"payMoney"];
    }
    
    if ([dic[@"priceList"] isKindOfClass:[NSNull class]]) {
        self.priceList=@[];
    }else{
        self.priceList=dic[@"priceList"];
    }
}

@end
