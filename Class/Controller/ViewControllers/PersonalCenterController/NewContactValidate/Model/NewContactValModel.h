//
//  NewContactValModel.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewContactValModel : NSObject

@property (nonatomic, copy) NSString *contact01;
@property (nonatomic, copy) NSString *phone01;
@property (nonatomic, copy) NSString *contact02;
@property (nonatomic, copy) NSString *phone02;

@property (nonatomic, copy) NSString *status;           //总的status

-(void)jsonToModel:(NSDictionary *)dic;

@end
