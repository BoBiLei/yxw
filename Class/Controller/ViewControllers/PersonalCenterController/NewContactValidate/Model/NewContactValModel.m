//
//  NewContactValModel.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewContactValModel.h"

@implementation NewContactValModel

-(void)jsonToModel:(NSDictionary *)dic{
    if (dic[@"person_jj_name"]&&![dic[@"person_jj_name"] isKindOfClass:[NSNull class]]) {
        self.contact01=dic[@"person_jj_name"];
    }else{
        self.contact01=@"";
    }
    
    if (dic[@"person_jj_code"]&&![dic[@"person_jj_code"] isKindOfClass:[NSNull class]]) {
        self.phone01=dic[@"person_jj_code"];
    }else{
        self.phone01=@"";
    }
    
    if (dic[@"person_jj_name_2"]&&![dic[@"person_jj_name_2"] isKindOfClass:[NSNull class]]) {
        self.contact02=dic[@"person_jj_name_2"];
    }else{
        self.contact02=@"";
    }
    
    if (dic[@"person_jj_code_2"]&&![dic[@"person_jj_code_2"] isKindOfClass:[NSNull class]]) {
        self.phone02=dic[@"person_jj_code_2"];
    }else{
        self.phone02=@"";
    }
    
    
    if (dic[@"jj_status"]&&![dic[@"jj_status"] isKindOfClass:[NSNull class]]) {
        self.status=dic[@"jj_status"];
    }else{
        self.status=@"";
    }
}

@end
