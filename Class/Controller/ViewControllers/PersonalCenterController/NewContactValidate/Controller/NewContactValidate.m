//
//  NewContactValidate.m
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewContactValidate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface NewContactValidate ()<ABPeoplePickerNavigationControllerDelegate>

@property (nonatomic, strong) NewContactValModel *model;

@end

@implementation NewContactValidate{
    UITextField *contactTF01;
    UITextField *phoneTF01;
    UITextField *contactTF02;
    UITextField *phoneTF02;
    
    UIButton *seleBtn01;
    UIButton *seleBtn02;
    
    UIButton *subBtn;
    
    NSInteger clickTag;
}

+(id)initWithModel:(NewContactValModel *)model{
    NewContactValidate *vc=[NewContactValidate new];
    vc.model=model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBar];
    
    [self setUpUI];
}

-(void)setNavBar{
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *navBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.backgroundColor=[UIColor colorWithHexString:@"0080c5"];
    [self.view addSubview:navBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 56.f, 44.f);
    [back setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *myTitleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    myTitleLabel.backgroundColor=[UIColor clearColor];
    [myTitleLabel setTitle:@"紧急联系人" forState:UIControlStateNormal];
    [navBar addSubview:myTitleLabel];
}

-(void)turnBack{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reflesh_approve_noti" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    UITableView *table=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-112)];
    [self.view addSubview:table];
    
    UIView *footView=[[UIView alloc] initWithFrame:table.frame];
    table.tableFooterView=footView;
    
    UILabel *tt01=[[UILabel alloc] initWithFrame:CGRectMake(16, 8, SCREENSIZE.width-32, 44)];
    tt01.font=kFontSize16;
    tt01.numberOfLines=0;
    tt01.text=@"请填写您的紧急联系人信息,请选择亲属或好友。";
    [footView addSubview:tt01];
    
#pragma mark 紧急联系人1
    UILabel *nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, tt01.origin.y+tt01.height+8, 98, 42)];
    nameLabel.font=kFontSize16;
    nameLabel.text=@"紧急联系人1:";
    [footView addSubview:nameLabel];
    
    UIView *nameView=[[UIView alloc] initWithFrame:CGRectMake(nameLabel.origin.x+nameLabel.width+8, nameLabel.origin.y, footView.width-(nameLabel.origin.x*2+nameLabel.width+8), nameLabel.height)];
    nameView.layer.cornerRadius=4;
    nameView.layer.borderWidth=0.5f;
    nameView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    nameView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:nameView];
    
    contactTF01=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, nameView.width-10, nameView.height)];
    contactTF01.font=kFontSize15;
    contactTF01.placeholder=@"请输入紧急联系人姓名";
    contactTF01.text=_model.contact01;
    if ([_model.status isEqualToString:@"2"]) {
        contactTF01.enabled=NO;
    }else{
        contactTF01.enabled=YES;
    }
    contactTF01.textColor=[UIColor colorWithHexString:@"70635a"];
    contactTF01.clearButtonMode=UITextFieldViewModeWhileEditing;
    [nameView addSubview:contactTF01];
    
#pragma mark 联系人电话1
    UILabel *idCardLabel=[[UILabel alloc] initWithFrame:CGRectMake(nameLabel.origin.x, nameLabel.origin.y+nameLabel.height+12, nameLabel.width, nameLabel.height)];
    idCardLabel.font=kFontSize16;
    idCardLabel.text=@"联系人电话:";
    [footView addSubview:idCardLabel];
    
    UIView *idCardView=[[UIView alloc] initWithFrame:CGRectMake(idCardLabel.origin.x+idCardLabel.width+8, idCardLabel.origin.y, nameView.width, nameView.height)];
    idCardView.layer.cornerRadius=4;
    idCardView.layer.borderWidth=0.5f;
    idCardView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    idCardView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:idCardView];
    
    phoneTF01=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, idCardView.width-82, idCardView.height)];
    phoneTF01.font=kFontSize15;
    phoneTF01.placeholder=@"联系人电话";
    phoneTF01.text=_model.phone01;
    if ([_model.status isEqualToString:@"2"]) {
        phoneTF01.enabled=NO;
    }else{
        phoneTF01.enabled=YES;
    }
    phoneTF01.textColor=[UIColor colorWithHexString:@"70635a"];
    phoneTF01.clearButtonMode=UITextFieldViewModeWhileEditing;
    [idCardView addSubview:phoneTF01];
    
    seleBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    seleBtn01.frame=CGRectMake(idCardView.width-68, 4, 64, idCardView.height-8);
    seleBtn01.layer.cornerRadius=3;
    seleBtn01.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    seleBtn01.titleLabel.font=kFontSize16;
    [seleBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [seleBtn01 setTitle:@"选择" forState:UIControlStateNormal];
    [seleBtn01 addTarget:self action:@selector(clickSele01) forControlEvents:UIControlEventTouchUpInside];
    [idCardView addSubview:seleBtn01];
    
    if ([_model.status isEqualToString:@"2"]) {
        seleBtn01.hidden=YES;
    }else{
        seleBtn01.hidden=NO;
    }
    
#pragma mark 紧急联系人2
    UILabel *bankLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, idCardLabel.origin.y+nameLabel.height+28, nameLabel.width, nameLabel.height)];
    bankLabel.font=kFontSize16;
    bankLabel.text=@"紧急联系人2:";
    [footView addSubview:bankLabel];
    
    UIView *bankView=[[UIView alloc] initWithFrame:CGRectMake(bankLabel.origin.x+bankLabel.width+8, bankLabel.origin.y, nameView.width, nameView.height)];
    bankView.layer.cornerRadius=4;
    bankView.layer.borderWidth=0.5f;
    bankView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    bankView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:bankView];
    
    contactTF02=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, bankView.width-10, bankView.height)];
    contactTF02.font=kFontSize15;
    contactTF02.placeholder=@"请输入紧急联系人姓名";
    contactTF02.text=_model.contact02;
    if ([_model.status isEqualToString:@"2"]) {
        contactTF02.enabled=NO;
    }else{
        contactTF02.enabled=YES;
    }
    contactTF02.textColor=[UIColor colorWithHexString:@"70635a"];
    contactTF02.clearButtonMode=UITextFieldViewModeWhileEditing;
    [bankView addSubview:contactTF02];
    
#pragma mark 联系人电话2
    UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(idCardLabel.origin.x, bankLabel.origin.y+bankLabel.height+12, nameLabel.width, nameLabel.height)];
    phoneLabel.font=kFontSize16;
    phoneLabel.text=@"联系人电话:";
    [footView addSubview:phoneLabel];
    
    UIView *phoneView=[[UIView alloc] initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width+8, phoneLabel.origin.y, nameView.width, nameView.height)];
    phoneView.layer.cornerRadius=4;
    phoneView.layer.borderWidth=0.5f;
    phoneView.layer.borderColor=[UIColor colorWithHexString:@"d7d7d7"].CGColor;
    phoneView.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    [footView addSubview:phoneView];
    
    phoneTF02=[[UITextField alloc] initWithFrame:CGRectMake(5, 0, phoneTF01.width, bankView.height)];
    phoneTF02.font=kFontSize15;
    phoneTF02.placeholder=@"请输入联系人电话";
    phoneTF02.text=_model.phone02;
    if ([_model.status isEqualToString:@"2"]) {
        phoneTF02.enabled=NO;
    }else{
        phoneTF02.enabled=YES;
    }
    phoneTF02.textColor=[UIColor colorWithHexString:@"70635a"];
    phoneTF02.clearButtonMode=UITextFieldViewModeWhileEditing;
    [phoneView addSubview:phoneTF02];
    
    seleBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    seleBtn02.frame=CGRectMake(idCardView.width-68, 4, 64, idCardView.height-8);
    seleBtn02.layer.cornerRadius=3;
    seleBtn02.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    seleBtn02.titleLabel.font=kFontSize16;
    [seleBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [seleBtn02 setTitle:@"选择" forState:UIControlStateNormal];
    [seleBtn02 addTarget:self action:@selector(clickSele02) forControlEvents:UIControlEventTouchUpInside];
    [phoneView addSubview:seleBtn02];
    
    if ([_model.status isEqualToString:@"2"]) {
        seleBtn02.hidden=YES;
    }else{
        seleBtn02.hidden=NO;
    }
    
#pragma mark 提交按钮
    subBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    subBtn.frame=CGRectMake(0, SCREENSIZE.height-48, SCREENSIZE.width, 48);
    subBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    subBtn.titleLabel.font=kFontSize17;
    [subBtn setTitle:@"提 交" forState:UIControlStateNormal];
    [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:subBtn];
    
    if (![_model.status isEqualToString:@"2"]) {
        subBtn.hidden=NO;
    }else{
        subBtn.hidden=YES;
    }
}

//选择联系人01
-(void)clickSele01{
    clickTag=0;
    [self selectPeople];
}

-(void)clickSele02{
    clickTag=1;
    [self selectPeople];
}

#pragma mark - 选择联系人
-(void)selectPeople{
    
    int __block tip=0;
    
    ABAddressBookRef addBook =nil;
    
    addBook=ABAddressBookCreateWithOptions(NULL,NULL);
    
    dispatch_semaphore_t sema=dispatch_semaphore_create(0);
    
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool greanted,CFErrorRef error)        {
        
        if (!greanted) {
            tip=1;
        }
        
        dispatch_semaphore_signal(sema);
    });
    
    dispatch_semaphore_wait(sema,DISPATCH_TIME_FOREVER);
    if (tip) {
        
        NSString *tipStr=[NSString stringWithFormat:@"您没有访问联系人权限，您可以在\n设置-隐私-通讯录-游侠网\n中启用访问权限"];
        UIAlertView * alart = [[UIAlertView alloc]initWithTitle:@"提示"message:tipStr delegate:self cancelButtonTitle:@"知道了"otherButtonTitles:nil,nil];
        [alart show];
    }else{
        ABPeoplePickerNavigationController *peoplePicker= [[ABPeoplePickerNavigationController alloc] init];
        
        [peoplePicker.navigationBar setBarStyle:UIBarStyleDefault];
        
        peoplePicker.peoplePickerDelegate =self;
        
        NSArray *displayItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty],nil];
        
        peoplePicker.displayedProperties=displayItems;
        
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            peoplePicker.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
        }
        [self presentViewController:peoplePicker animated:YES completion:nil];
    }
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    
    //获取联系人电话
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSRange prang=[phoneNO rangeOfString:@"+86 "];
    if (prang.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang.location+prang.length];
    }
    
    //获取联系人姓名
    NSString *ssstr = (__bridge NSString*)ABRecordCopyCompositeName(person);
    
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    
    if (clickTag==0) {
        contactTF01.text=ssstr==nil||[ssstr isEqualToString:@""]?@"没填写联系人":ssstr;
        phoneTF01.text=phoneNO;
    }else{
        contactTF02.text=ssstr==nil||[ssstr isEqualToString:@""]?@"没填写联系人":ssstr;
        phoneTF02.text=phoneNO;
    }
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0){
    ABPersonViewController *personViewController = [[ABPersonViewController alloc] init];
    personViewController.displayedPerson = person;
    [peoplePicker pushViewController:personViewController animated:YES];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person NS_DEPRECATED_IOS(2_0, 8_0){
    return YES;
}
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_DEPRECATED_IOS(2_0, 8_0){
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if (phone && phoneNO.length == 11) {
        [peoplePicker dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"错误提示" message:@"请选择正确手机号" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
    return YES;
}

-(void)clickSubBtn{
    if ([contactTF01.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"紧急联系人1不能为空" inView:self.view];
    }else if ([phoneTF01.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"紧急联系人电话1不能为空" inView:self.view];
    }else if ([contactTF02.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"紧急联系人2不能为空" inView:self.view];
    }else if ([phoneTF02.text isEqualToString:@""]){
        [AppUtils showSuccessMessage:@"紧急联系人电话2不能为空" inView:self.view];
    }else{
        [self requestContactForFirstName:contactTF01.text firstPhone:phoneTF01.text secondName:contactTF02.text secondPhone:phoneTF02.text];
    }
}

#pragma mark  提交紧急联系人 Request

-(void)requestContactForFirstName:(NSString *)name firstPhone:(NSString *)firstPhone secondName:(NSString *)secondName secondPhone:(NSString *)secondPhone{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"me_t" forKey:@"mod"];
    [dict setObject:@"save_vip_jj_iso_new" forKey:@"code"];
    [dict setObject:name forKey:@"person_jj_name"];
    [dict setObject:firstPhone forKey:@"person_jj_code"];
    [dict setObject:secondName forKey:@"person_jj_name_2"];
    [dict setObject:secondPhone forKey:@"person_jj_code_2"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            contactTF01.enabled=NO;
            phoneTF01.enabled=NO;
            contactTF02.enabled=NO;
            phoneTF02.enabled=NO;
            seleBtn01.hidden=YES;
            seleBtn02.hidden=YES;
            subBtn.hidden=YES;
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
