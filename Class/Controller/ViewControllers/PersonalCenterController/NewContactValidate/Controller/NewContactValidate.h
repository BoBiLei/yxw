//
//  NewContactValidate.h
//  youxia
//
//  Created by mac on 2017/4/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewContactValModel.h"

@interface NewContactValidate : UIViewController

+(id)initWithModel:(NewContactValModel *)model;

@end
