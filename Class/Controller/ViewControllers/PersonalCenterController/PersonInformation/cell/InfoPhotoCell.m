//
//  InfoPhotoCell.m
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "InfoPhotoCell.h"

@implementation InfoPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDatePhoto:) name:UpDateUser_Photo object:nil];
    self.leftLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    _imgv.layer.masksToBounds=YES;
    _imgv.layer.cornerRadius=self.imgv.height/2;
    _imgv.layer.borderWidth=0.7f;
    _imgv.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
}

-(void)upDatePhoto:(NSNotification *)noti{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
        _imgv.image=image;
    }else{
        [_imgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
