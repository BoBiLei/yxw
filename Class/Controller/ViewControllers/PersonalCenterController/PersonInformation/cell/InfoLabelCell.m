//
//  InfoLabelCell.m
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "InfoLabelCell.h"

@implementation InfoLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftLabel.textColor=[UIColor colorWithHexString:@"#5d5d5d"];
    self.rightLabel.textColor=[UIColor colorWithHexString:@"#c7c7c7"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
