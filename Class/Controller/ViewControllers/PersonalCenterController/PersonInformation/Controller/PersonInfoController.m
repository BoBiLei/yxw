//
//  PersonInfoController.m
//  youxia
//
//  Created by mac on 16/4/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PersonInfoController.h"
#import "InfoPhotoCell.h"
#import "InfoLabelCell.h"
#import "BindingPhoneController.h"
#import "ChangePhoneController.h"
#import "ModifyPassWordController.h"
#import "ValidatePassController.h"

@interface PersonInfoController ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@end

@implementation PersonInfoController{
    NSArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"个人资料页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"个人资料页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBandPhone:) name:@"BandPhoneSuccess" object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"个人资料";
    [self.view addSubview:navBar];
    
    [self setUpUI];
}

-(void)updateBandPhone:(NSNotification *)noti{
    [myTable reloadData];
}

#pragma mark - init UI

-(void)setUpUI{
    
    //
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"PersonInfoPlist" ofType:@"plist"];
    dataArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"InfoPhotoCell" bundle:nil] forCellReuseIdentifier:@"InfoPhotoCell"];
    [myTable registerNib:[UINib nibWithNibName:@"InfoLabelCell" bundle:nil] forCellReuseIdentifier:@"InfoLabelCell"];
    [self.view addSubview:myTable];
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)dataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 78;
        }else{
            return 44;
        }
    }else{
        return 44;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            InfoPhotoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoPhotoCell"];
            if (cell==nil) {
                cell=[[InfoPhotoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoPhotoCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            SDImageCache *imgCache=[SDImageCache sharedImageCache];
            if ([imgCache diskImageExistsWithKey:[AppUtils getValueWithKey:User_Photo]]) {
                UIImage *image = [imgCache imageFromDiskCacheForKey:[AppUtils getValueWithKey:User_Photo]];
                cell.imgv.image=image;
            }else{
                [cell.imgv sd_setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]] placeholderImage:[UIImage imageNamed:Image_Default]];
            }
            return  cell;
        }else{
            InfoLabelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoLabelCell"];
            if (cell==nil) {
                cell=[[InfoLabelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoLabelCell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=dataArr[indexPath.section][indexPath.row][@"leftlabel"];
            cell.rightLabel.text=[AppUtils getValueWithKey:User_Name];
            return  cell;
        }
        
    }else{
        InfoLabelCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InfoLabelCell"];
        if (cell==nil) {
            cell=[[InfoLabelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoLabelCell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSString *rightStr;
        NSString *leftStr=dataArr[indexPath.section][indexPath.row][@"leftlabel"];;
        if (indexPath.section==1&&indexPath.row==1) {
            
            //Login_Type=1--手机登陆  2--第三方登陆
            if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
                DSLog(@"11");
                if(![[AppUtils getValueWithKey:User_Name] isEqualToString:@""]){
                    rightStr=[AppUtils getValueWithKey:User_Name];
                }else{
                    rightStr=@"未设置";
                }
            }else{
                DSLog(@"22%@",[AppUtils getValueWithKey:User_Phone]);
                if([[AppUtils getValueWithKey:User_Phone] isEqualToString:@""]){
                    rightStr=@"未绑定手机号";
                }else{
                    rightStr=[AppUtils getValueWithKey:User_Phone];
                }
            }
            
        }else{
            rightStr=@"";
            //Login_Type=1--手机登陆  2--第三方登陆
            if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
                leftStr=@"修改密码";
            }else{
                //Is_NewPwd=0 设置新密码  =1修改的
                if ([[AppUtils getValueWithKey:Is_NewPwd] isEqualToString:@"0"]) {
                    leftStr=@"设置新密码";
                }else{
                    leftStr=@"修改密码";
                }
            }
        }
        cell.leftLabel.text=leftStr;
        cell.rightLabel.text=rightStr;
        return  cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *tagStr=dataArr[indexPath.section][indexPath.row][@"tag"];
    switch (tagStr.intValue) {
        case 0:
            [self showMdfView];
            break;
        case 1:
            
            break;
        case 2:
        {
            if (![[AppUtils getValueWithKey:User_Phone] isEqualToString:@""]) {
                [self.navigationController pushViewController:[ModifyPassWordController new] animated:YES];
            }else{
                //Login_Type=1--手机登陆  2--第三方登陆
                if ([[AppUtils getValueWithKey:Login_Type] isEqualToString:@"1"]) {
                    [self.navigationController pushViewController:[ModifyPassWordController new] animated:YES];
                }else{
                    [AppUtils showSuccessMessage:@"请先绑定手机号" inView:self.view];
                }
            }
        }
            break;
        case 3:
        {
            if([[AppUtils getValueWithKey:User_Phone] isEqualToString:@""]){
                [self.navigationController pushViewController:[BindingPhoneController new] animated:YES];
            }else{
                [self.navigationController pushViewController:[ValidatePassController new] animated:YES];
            }
        }
            break;
        case 4:
            
            break;
        case 5:
            
            break;
        case 6:
            
            break;
        default:
            break;
    }
}


#pragma mark - show ModifyPhoto View
-(void)showMdfView{
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self snapImage];
                break;
            case 1:
                [self pickImage];
                break;
                
            default:
                break;
        }
    };
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.titleFontSize=14;
    sheetConfig.buttonHeight=49;
    sheetConfig.buttonFontSize=17;
    sheetConfig.innerMargin=12;
    sheetConfig.titleColor=[UIColor lightGrayColor];
    sheetConfig.itemNormalColor=[UIColor colorWithHexString:@"#0080c5"];
    
    NSArray *items =
    @[MMItemMake(@"拍 照", MMItemTypeNormal, block),
      MMItemMake(@"从相册上传", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:@"更新头像"
                                  items:items] showWithBlock:^(MMPopupView *popupView){
        
    }];
}

//拍照
- (void) snapImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}
//从相册里找
- (void) pickImage{
    UIImagePickerController *ipc=[[UIImagePickerController alloc] init];
    ipc.navigationController.delegate=self;
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController delegate--
//选择好照片后回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    DSLog(@"%@",info);
    UIImage *image= [info objectForKey:@"UIImagePickerControllerEditedImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(200.0, 200.0)];
    
    NSData* imageData = UIImagePNGRepresentation(theImage);
    
    [self upLoadPhotoHttpsForImg:imageData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

//保存图片到document
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imageData writeToFile:fullPathToFile atomically:NO];
}

#pragma mark - navigation delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    UIColor * color = [UIColor whiteColor];
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    navigationController.navigationBar.titleTextAttributes=dict;
    navigationController.navigationBar.tintColor=[UIColor whiteColor];
}


#pragma mark Request 上传头像请求
-(void)upLoadPhotoHttpsForImg:(NSData *)imgDate{
    NSDictionary *dict=@{
                         @"mod":@"me_t",
                         @"is_iso":@"1",
                         @"code":@"upload_iso",
                         @"user_id":[AppUtils getValueWithKey:User_ID],
                         @"zfl":imgDate,
                         @"type":@"face",
                         @"img_type":@"png",
                         @"size":@"0",
                         @"is_nosign":@"1"
                         };
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        NSLog(@"%@",imgDate);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *newPhtotStr=responseObject[@"retData"][@"data"][@"url"];
            [AppUtils saveValue:newPhtotStr forKey:User_Photo];
            [[NSNotificationCenter defaultCenter] postNotificationName:UpDateUser_Photo object:newPhtotStr];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
