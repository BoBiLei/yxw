//
//  IslandController.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "IslandController.h"
#import "SearchViewController.h"
#import "IslandCell.h"
#import "DistingceLeftCell.h"
#import "IslandOnlineController.h"
#import "NewMaldiThematiController.h"
#import "MaldDistinceListController.h"
#import "DistingRightModel.h"
#import "DistingcOrtherListController.h"

@interface IslandController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UITableView *myTable;
@property(nonatomic,strong)UICollectionView *collectView;

@end

@implementation IslandController{
    NSMutableArray *leftArr;
    NSMutableArray *rightArr;
    UINavigationBar *cusBar;
    UIImageView *searchView;
    
    //记录左右点击的index
    NSInteger leftTag;
    NSInteger rightTag;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"游侠目的地页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"游侠目的地页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpCustomSearBar];
    
    [self setUpUI];
    
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestDistingceData];
        }
    };
}

-(void)setUpCustomSearBar{
    //
    //（首先把原来的bar去掉）必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    searchView=[[UIImageView alloc] initWithFrame:CGRectMake(12, 26, SCREENSIZE.width-24, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"请输入目的地";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
}

#pragma mark - 搜索栏点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

#pragma mark - 懒加载

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width/4, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor whiteColor];
        _myTable.dataSource=self;
        _myTable.delegate=self;
        _myTable.bounces=NO;
        _myTable.showsVerticalScrollIndicator=NO;
        _myTable.separatorStyle = UITableViewCellSelectionStyleNone;
        [_myTable registerNib:[UINib nibWithNibName:@"DistingceLeftCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _myTable;
}

-(UICollectionView *)collectView{
    if (!_collectView) {
        UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
        _collectView=[[UICollectionView alloc]initWithFrame:CGRectMake(SCREENSIZE.width/4, 64, SCREENSIZE.width-SCREENSIZE.width/4, SCREENSIZE.height-113) collectionViewLayout:myLayout];
        [_collectView registerNib:[UINib nibWithNibName:@"IslandCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
        _collectView.backgroundColor=[UIColor clearColor];
        _collectView.dataSource=self;
        _collectView.delegate=self;
        _collectView.bounces=YES;
    }
    return _collectView;
}

#pragma mark - init UI
-(void)setUpUI{
    
    [self.view addSubview:self.myTable];
    
    [self.view addSubview:self.collectView];
}


#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return leftArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 49;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DistingceLeftCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell=[[DistingceLeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.myTitle.text=leftArr[indexPath.row][@"title"];
    cell.myTitle.highlightedTextColor = [UIColor whiteColor];
    cell.selectedBackgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"disting_seleItem"]];
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    leftTag=indexPath.row;
    
    [rightArr removeAllObjects];    //每次要remove掉数据
    NSArray *rigArr=leftArr[indexPath.row][@"data"];
    for (NSDictionary *dic in rigArr) {
        DistingRightModel *model=[DistingRightModel new];
        [model jsonDataForDictionary:dic];
        [rightArr addObject:model];
    }
    [_collectView reloadData];
}


#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return rightArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    IslandCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[IslandCell alloc]init];
    }
    DistingRightModel *model=rightArr[indexPath.row];
    [cell setPropetyWithIndexPath:indexPath atModel:model];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth=((SCREENSIZE.width-SCREENSIZE.width/4)-36)/2;
    
    return CGSizeMake(cellWidth, cellWidth-30);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 12;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(16, 12, 16, 12);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    rightTag=indexPath.row;
    
    DistingRightModel *model=rightArr[indexPath.row];
    
    if ([model.name isEqualToString:@"马尔代夫"]) {
        MaldDistinceListController *distShow=[[MaldDistinceListController alloc]init];
        distShow.urlStr=model.url;
        [self.navigationController pushViewController:distShow animated:YES];
    }else{
        DistingcOrtherListController *distShow=[[DistingcOrtherListController alloc]init];
        distShow.reveivleftTag=leftTag;
        distShow.reveivRightTag=rightTag;
        distShow.urlStr=model.url;
        DSLog(@"%ld--%ld--%@",(long)leftTag,(long)rightTag,model.url);
        [self.navigationController pushViewController:distShow animated:YES];
    }
    
}

#pragma mark - Request 请求
-(void)requestDistingceData{
    leftArr=[NSMutableArray array];
    rightArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&a=mudi_index_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            NSArray *itemArr=responseObject[@"retData"];
            
            //左边
            for (NSDictionary *dic in itemArr) {
                [leftArr addObject:dic];
            }
            
            //默认右边（赛选的第一个）
            NSArray *rDicArr=leftArr[0][@"data"];
            for (NSDictionary *dic in rDicArr) {
                DistingRightModel *model=[DistingRightModel new];
                [model jsonDataForDictionary:dic];
                [rightArr addObject:model];
            }
            
        }else{
            DSLog(@"请求数据异常");
        }
        
        //（刷新左边表格）并设置默认选中第一行
        [self.myTable reloadData];
        NSIndexPath *defaultSele=[NSIndexPath indexPathForRow:0 inSection:0];
        [self.myTable selectRowAtIndexPath:defaultSele animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        //（刷新右边表格）
        [self.collectView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
