//
//  DistingceLeftCell.m
//  youxia
//
//  Created by mac on 16/3/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "DistingceLeftCell.h"

@implementation DistingceLeftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor colorWithHexString:@"#f8f8f8"];
    self.myTitle.textColor=[UIColor colorWithHexString:@"#929292"];
    self.myTitle.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 48, self.width, 1)];
    line.backgroundColor=[UIColor whiteColor];
    [self addSubview:line];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
