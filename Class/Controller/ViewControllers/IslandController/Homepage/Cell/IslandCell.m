//
//  IslandCell.m
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "IslandCell.h"
#import <TTTAttributedLabel.h>

@implementation IslandCell{
    TTTAttributedLabel *titleLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor colorWithHexString:YxColor_LightBlue];
    CGFloat cellWidth=((SCREENSIZE.width-SCREENSIZE.width/4)-36)/2;
    _imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellWidth-30)];
    _imgv.image=[UIImage imageNamed:@"island_test"];
    [self addSubview:_imgv];
    
    UIView *bgView=[[UIView alloc] initWithFrame:_imgv.frame];
    bgView.backgroundColor=[UIColor colorWithHexString:@"#000000"];
    bgView.alpha=0.2f;
    [self addSubview:bgView];
    
    //
    titleLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, 0, _imgv.width, _imgv.height)];
    titleLabel.center=_imgv.center;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:16];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.numberOfLines=0;
    [self addSubview:titleLabel];
}

-(void)setPropetyWithIndexPath:(NSIndexPath *)indexPath atModel:(DistingRightModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.img]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.img];
        _imgv.image=image;
    }else{
        [_imgv sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x193"]];
    }
    
    
    NSString *stageStr=[NSString stringWithFormat:@"%@\n%@",model.name,model.ywName];
    [titleLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.ywName options:NSCaseInsensitiveSearch];
        
        //设置选择文本的大小
        UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:13];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
}

@end
