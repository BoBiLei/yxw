//
//  IslandCell.h
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DistingRightModel.h"

@interface IslandCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imgv;

-(void)setPropetyWithIndexPath:(NSIndexPath *)indexPath atModel:(DistingRightModel *)model;

@end
