//
//  DistingRightModel.h
//  youxia
//
//  Created by mac on 16/5/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DistingRightModel : NSObject

@property(nonatomic,copy) NSString *img;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *url;
@property(nonatomic,copy) NSString *ywName;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
