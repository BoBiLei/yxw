//
//  DistingRightModel.m
//  youxia
//
//  Created by mac on 16/5/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "DistingRightModel.h"

@implementation DistingRightModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.img=dic[@"img"];
    self.name=dic[@"name"];
    self.url=dic[@"url"];
    self.ywName=dic[@"yw_name"];
}

@end
