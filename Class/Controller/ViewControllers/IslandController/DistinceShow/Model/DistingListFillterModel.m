//
//  DistingListFillterModel.m
//  youxia
//
//  Created by mac on 16/5/20.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "DistingListFillterModel.h"

@implementation DistingListFillterModel

-(void)setModel:(NSDictionary *)model{
    self.title=model[@"title"];
    self.dataArr=model[@"data"];
}

@end
