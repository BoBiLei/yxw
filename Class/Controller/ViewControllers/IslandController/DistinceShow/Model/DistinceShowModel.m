//
//  DistinceShowModel.m
//  youxia
//
//  Created by mac on 16/5/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "DistinceShowModel.h"

@implementation DistinceShowModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.islandId=dic[@"id"];
    self.pId=dic[@"pid"];
    self.mdId=dic[@"mdid"];
    self.islandImg=dic[@"pic_position"];
    self.islandType=dic[@"lvtype"];
    self.islandTitle=dic[@"title"];
    self.islandPrice=dic[@"p_price"];
    self.stagePrice=dic[@"p_f_price"];
    self.islandYwName=dic[@"mudi_name"];
    self.starLevel=dic[@"xingji"];
    self.jiaotong=dic[@"jiaotong"];
}

@end
