//
//  DistinceShowModel.h
//  youxia
//
//  Created by mac on 16/5/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DistinceShowModel : NSObject

@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,copy) NSString *pId;
@property (nonatomic,copy) NSString *mdId;
@property (nonatomic,copy) NSString *islandImg;
@property (nonatomic,copy) NSString *islandType;
@property (nonatomic,copy) NSString *islandYwName;
@property (nonatomic,copy) NSString *islandTitle;
@property (nonatomic,copy) NSString *islandPrice;
@property (nonatomic,copy) NSString *stagePrice;
@property (nonatomic,copy) NSString *starLevel;     //星级
@property (nonatomic,copy) NSString *jiaotong;      //交通

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
