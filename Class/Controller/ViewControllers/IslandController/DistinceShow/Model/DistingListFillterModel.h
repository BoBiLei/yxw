//
//  DistingListFillterModel.h
//  youxia
//
//  Created by mac on 16/5/20.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DistingListFillterModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, retain) NSArray *dataArr;

@property (nonatomic, retain) NSDictionary *model;

@end
