//
//  DistingcOrtherListController.m
//  youxia
//
//  Created by mac on 16/5/20.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "DistingcOrtherListController.h"
#import "SearchViewController.h"
#import "NewIslandOnlineCell.h"
#import "DistingDropDownMenu.h"
#import "DistingListFillterModel.h"
#import "DistingRightFillterModel.h"
#import "MaldDistinceListController.h"
#import "MdIslandOnlineCell.h"

#define  NiocellHeight SCREENSIZE.width/3-12
@interface DistingcOrtherListController ()<DistingDropDownMenuDataSource,DistingDropDownMenuDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation DistingcOrtherListController{
    
    NSString *topTag;       //选择的滚动到顶部标识
    NSString *seleTopTag;       //选择的滚动到顶部标识
    
    NSInteger page;
    MJRefreshAutoNormalFooter *footer;
    
    //在线岛屿筛选id
    NSString *firstId;
    NSString *secondId;
    NSString *threeId;
    
    NSMutableArray *dyFillArr;
    NSArray *menuArr01;
    NSArray *menuArr02;
    NSArray *menuArr03;
    DistingDropDownMenu *mdMenu;
    DistingDropDownMenu *plMenu;
    
    //
    NSMutableArray *plDataArr;
    UITableView *plTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"目的地列表页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"目的地列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateReturn) name:@"Md_Return" object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    topTag=@"";
    
    [self setUpCustomSearBar];
    
    [self requestDistingceData];
}

-(void)updateReturn{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"请输入目的地";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

#pragma mark - setUpTableView
-(void)setUpTableView{
    plTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-157) style:UITableViewStyleGrouped];
    plTable.dataSource=self;
    plTable.delegate=self;
    [self.view addSubview:plTable];
    
    //
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = kFontSize14;
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    plTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (page!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self fillterRequestWithTypeId:firstId moreFilterId:threeId andPage:page];
        });
    }
}

#pragma mark - SetUp DropMenu
-(void)setUpDopMenu{
    
    if (_isMadil) {
        firstId=@"0";
        secondId=@"0";
        threeId=@"0";
        
        // 数据
        NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"StarLevel" ofType:@"plist"];
        menuArr01 = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
        
        NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"travelPlan" ofType:@"plist"];
        menuArr02 = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
        
        NSString *goIslandTypePath = [[NSBundle mainBundle] pathForResource:@"goIslandType" ofType:@"plist"];
        menuArr03 = [[NSArray alloc] initWithContentsOfFile:goIslandTypePath];
    }else{
        firstId=@"all";
        secondId=self.urlStr;
        threeId=@"zx";
        
        // 数据
        menuArr01 = @[@{@"id":@"all",@"name":@"全部"},
                      @{@"id":@"zyx",@"name":@"自由行"},
                      @{@"id":@"ddy",@"name":@"当地游"}];
        
        menuArr03 = @[@{@"id":@"zx",@"name":@"最新"},
                      @{@"id":@"rm",@"name":@"热门"}];
    }
    
    // 添加下拉菜单
    
    mdMenu = [[DistingDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    mdMenu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    mdMenu.textColor=[UIColor colorWithHexString:@"#686868"];
    mdMenu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    mdMenu.delegate = self;
    mdMenu.dataSource = self;
    [mdMenu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:mdMenu];
    
    
    //
    plMenu = [[DistingDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    plMenu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    plMenu.textColor=[UIColor colorWithHexString:@"#686868"];
    plMenu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    plMenu.delegate = self;
    plMenu.dataSource = self;
    [plMenu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:plMenu];
    
    if (_isMadil) {
        // 创建menu 第一次显示 不会调用点击代理，可以用这个手动调用
        [plMenu selectDefalutIndexPath];
    }else{
        if (self.reveivRightTag!=100) {
            [plMenu selectIndexPath:[DistingIndexPath indexPathWithCol:1 row:self.reveivleftTag item:self.reveivRightTag]];
        }else{
            [plMenu selectIndexPath:[DistingIndexPath indexPathWithCol:1 row:self.reveivleftTag]];
        }
    }
}

#pragma mark -DOPMenu Delegate
- (NSInteger)numberOfColumnsInMenu:(DistingDropDownMenu *)menu{
    return 3;
}

- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column{
    if (column == 0) {
        return menuArr01.count;
    }else if (column == 1){
        return _isMadil?menuArr02.count:dyFillArr.count;
    }else {
        return menuArr03.count;
    }
}

- (NSString *)menu:(DistingDropDownMenu *)menu titleForRowAtIndexPath:(DistingIndexPath *)indexPath{
    
    //
    if (indexPath.column == 0) {
        return menuArr01[indexPath.row][@"name"];
    } else if (indexPath.column == 1){
        if (_isMadil) {
            return menuArr02[indexPath.row][@"name"];
        }else{
            DistingListFillterModel *model=dyFillArr[indexPath.row];
            return model.title;
        }
    } else {
        return menuArr03[indexPath.row][@"name"];
    }
}

- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column{
    
    if (_isMadil) {
        if (column == 1) {
            if (row == 0) {
                return 0;
            } else if (row == 2){
                return 0;
            } else if (row == 3){
                return 0;
            }
        }
        return 0;
    }else{
        if (column == 1) {
            DistingListFillterModel *model=dyFillArr[row];
            return model.dataArr.count;
        }
        return 0;
    }
}

- (NSString *)menu:(DistingDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DistingIndexPath *)indexPath{
    
    if (_isMadil) {
        if (indexPath.column == 0) {
            if (indexPath.row == 0) {
                return 0;
            } else if (indexPath.row == 2){
                return 0;
            } else if (indexPath.row == 3){
                return 0;
            }
        }
        return nil;
    }else{
        if (indexPath.column == 1) {
            DistingListFillterModel *model=dyFillArr[indexPath.row];
            return model.dataArr[indexPath.item][@"name"];
        }
        
        return nil;
    }
}

- (void)menu:(DistingDropDownMenu *)menu didSelectRowAtIndexPath:(DistingIndexPath *)indexPath{
    
    if(_isMadil){
        if (indexPath.column == 0) {
            firstId=menuArr01[indexPath.row][@"id"];
        }else if (indexPath.column == 1){
            secondId=menuArr02[indexPath.row][@"id"];
        }else{
            threeId=menuArr03[indexPath.row][@"id"];
        }
        [self fillterIslandRequestStarId:firstId gplaceId:secondId gIslandId:threeId];
    }else{
        
        //
        plDataArr=[NSMutableArray array];
        
        page=1;  //默认第一页（每次点击都把page设置为第一页）
        
        if (indexPath.column == 0) {
            firstId=menuArr01[indexPath.row][@"id"];
        }else if (indexPath.column == 1) {
            DistingListFillterModel *model=dyFillArr[indexPath.row];
            NSString *str=model.dataArr[indexPath.item][@"name"];
            DSLog(@"%@====",str);
            if ([str isEqualToString:@"马尔代夫"]) {
                _isMadil=YES;
                plMenu.hidden=YES;
                // 数据
                NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"StarLevel" ofType:@"plist"];
                menuArr01 = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
                
                NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"travelPlan" ofType:@"plist"];
                menuArr02 = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
                
                NSString *goIslandTypePath = [[NSBundle mainBundle] pathForResource:@"goIslandType" ofType:@"plist"];
                menuArr03 = [[NSArray alloc] initWithContentsOfFile:goIslandTypePath];
                [mdMenu reloadData];
                [self fillterIslandRequestStarId:firstId gplaceId:secondId gIslandId:threeId];
            }else{
                _isMadil=NO;
                plMenu.hidden=NO;
                secondId=model.dataArr[indexPath.item][@"url"];
            }
        }else if (indexPath.column == 2) {
            threeId=menuArr03[indexPath.row][@"id"];
        }
        
        if (!_isMadil) {
            seleTopTag=[NSString stringWithFormat:@"%@%@%@",firstId,secondId,threeId];
            [self fillterRequestWithTypeId:firstId moreFilterId:threeId andPage:page];
        }
        
        //
        //
//        DistingListFillterModel *model=dyFillArr[indexPath.row];
//        DSLog(@"%@-%ld-%@",plDataArr,page,model.dataArr);
//        NSString *str=model.dataArr[indexPath.item][@"name"];
//        if ([str isEqualToString:@"马尔代夫"]) {
//            DSLog(@"跳到马尔代夫");
//            _isMadil=YES;
//            plMenu.hidden=YES;
//            // 数据
//            NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"StarLevel" ofType:@"plist"];
//            menuArr01 = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
//            
//            NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"travelPlan" ofType:@"plist"];
//            menuArr02 = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
//            
//            NSString *goIslandTypePath = [[NSBundle mainBundle] pathForResource:@"goIslandType" ofType:@"plist"];
//            menuArr03 = [[NSArray alloc] initWithContentsOfFile:goIslandTypePath];
//            [self fillterIslandRequestStarId:firstId gplaceId:secondId gIslandId:threeId];
//            [mdMenu reloadData];
//        }else{
//            DSLog(@"NONONO");
//            _isMadil=NO;
//            plMenu.hidden=NO;
//            if (indexPath.column == 0) {
//                firstId=menuArr01[indexPath.row][@"id"];
//                DSLog(@"%@-",firstId);
//            }else if (indexPath.column == 1) {
//                if (indexPath.item>=0) {
//                    secondId=model.dataArr[indexPath.item][@"url"];
//                }
//            }else{
//                threeId=menuArr03[indexPath.row][@"id"];
//            }
//            seleTopTag=[NSString stringWithFormat:@"%@%@%@",firstId,secondId,threeId];
//            
//            [self fillterRequestWithTypeId:firstId moreFilterId:threeId andPage:page];
//        }
    }
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return plDataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return NiocellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isMadil) {
        MdIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
        if (!cell) {
            cell=[[MdIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell1"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        DistinceShowModel *model=plDataArr[indexPath.row];
        cell.model=model;
        return  cell;
    }else{
        NewIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[NewIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        DistinceShowModel *model=plDataArr[indexPath.row];
        cell.model=model;
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DistinceShowModel *model=plDataArr[indexPath.row];
    
    IslandDetailController *islDetail=[[IslandDetailController alloc] init];
    if ([model.islandYwName isEqualToString:@"帕劳"]) {
        islDetail.detail_landType=PalauIslandType;
    }else{
        islDetail.detail_landType=MaldivesIslandType;
    }
    islDetail.islandName=model.islandTitle;
    islDetail.islandId=model.islandId;
    islDetail.islandImg=model.islandImg;
    islDetail.islandPId=model.pId;
    islDetail.mdId=model.mdId;
    islDetail.islandDescript=model.islandTitle;
    [self.navigationController pushViewController:islDetail animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([plTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [plTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([plTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [plTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestDistingceData{
    plDataArr=[NSMutableArray array];
    dyFillArr=[NSMutableArray array];
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:self.urlStr] method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        
        page=2;
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            NSArray *fArr=responseObject[@"retData"][@"mudi"];
            for (NSDictionary *dic in fArr) {
                DistingListFillterModel *model=[DistingListFillterModel new];
                model.model=dic;
                [dyFillArr addObject:model];
            }
            
            
            //
            NSArray *arr=responseObject[@"retData"][@"data"];
            //DSLog(@"%@",responseObject[@"retData"]);
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                [plDataArr addObject:model];
            }
            
            [self setUpDopMenu];
            
            [self setUpTableView];
            
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)fillterRequestWithTypeId:(NSString *)typeId moreFilterId:(NSString *)fillterId andPage:(NSInteger)cpage{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    NSDictionary *dic=@{@"lvtype":typeId,
                        @"order":fillterId,
                        @"page":[NSString stringWithFormat:@"%ld",(long)cpage]};
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:secondId] method:HttpRequestPost parameters:dic prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //
            NSArray *arr=responseObject[@"retData"][@"data"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                [plDataArr addObject:model];
            }
            
            [plTable reloadData];
            
            //
//            if (![seleTopTag isEqualToString:topTag]) {
//                [plTable setContentOffset:CGPointMake(0,0) animated:YES];
//            }
//            topTag=seleTopTag;
            
            //
            if (arr.count!=0) {
                page++;
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//
-(void)fillterIslandRequestStarId:(NSString *)sId gplaceId:(NSString *)gplId gIslandId:(NSString *)gIsId{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    plDataArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"typeid"];
    [dict setObject:sId forKey:@"dy"];
    [dict setObject:gplId forKey:@"jg"];
    [dict setObject:gIsId forKey:@"jt"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{@"dy":sId,
//                         @"jg":gplId,
//                         @"jt":gIsId,
//                         @"is_iso":@"1",
//                         @"typeid":@"1"
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:[@"http://m.youxia.com/" stringByAppendingString:@"maldives/search.html?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        //DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"sealist"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                [plDataArr addObject:model];
            }
        }
        [plTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
