//
//  DistingDropDownMenu.h
//  DistingDropDownMenuDemo
//
//  Created by weizhou on 9/26/14.
//  Modify by tanyang on 20/3/15.
//  Copyright (c) 2014 fengweizhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistingIndexPath : NSObject

@property (nonatomic, assign) NSInteger column;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, assign) NSInteger item;
- (instancetype)initWithColumn:(NSInteger)column row:(NSInteger)row;
// default item = -1
+ (instancetype)indexPathWithCol:(NSInteger)col row:(NSInteger)row;
+ (instancetype)indexPathWithCol:(NSInteger)col row:(NSInteger)row item:(NSInteger)item;
@end

@interface DistingBackgroundCellView : UIView

@end

#pragma mark - data source protocol
@class DistingDropDownMenu;

@protocol DistingDropDownMenuDataSource <NSObject>

@required

/**
 *  返回 menu 第column列有多少行
 */
- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column;

/**
 *  返回 menu 第column列 每行title
 */
- (NSString *)menu:(DistingDropDownMenu *)menu titleForRowAtIndexPath:(DistingIndexPath *)indexPath;

@optional

/**
 *  addHotel
 */
- (UIView *)menu:(DistingDropDownMenu *)menu addHotelAtIndexPath:(DistingIndexPath *)indexPath;

/**
 *  返回 menu 有多少列 ，默认1列
 */
- (NSInteger)numberOfColumnsInMenu:(DistingDropDownMenu *)menu;

// 新增 返回 menu 第column列 每行image
- (NSString *)menu:(DistingDropDownMenu *)menu imageNameForRowAtIndexPath:(DistingIndexPath *)indexPath;


/** 新增
 *  当有column列 row 行 返回有多少个item ，如果>0，说明有二级列表 ，=0 没有二级列表
 *  如果都没有可以不实现该协议
 */
- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column;

/** 新增
 *  当有column列 row 行 item项 title
 *  如果都没有可以不实现该协议
 */
- (NSString *)menu:(DistingDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DistingIndexPath *)indexPath;

// 新增 当有column列 row 行 item项 image
- (NSString *)menu:(DistingDropDownMenu *)menu imageNameForItemsInRowAtIndexPath:(DistingIndexPath *)indexPath;


@end

#pragma mark - delegate
@protocol DistingDropDownMenuDelegate <NSObject>
@optional
/**
 *  点击代理，点击了第column 第row 或者item项，如果 item >=0
 */
- (void)menu:(DistingDropDownMenu *)menu didSelectRowAtIndexPath:(DistingIndexPath *)indexPath;

/** 新增
 *  return nil if you don't want to user select specified indexpath
 *  optional
 */
- (NSIndexPath *)menu:(DistingDropDownMenu *)menu willSelectRowAtIndexPath:(DistingIndexPath *)indexPath;

@end

#pragma mark - interface
@interface DistingDropDownMenu : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) BOOL isHotelMenu;

@property (nonatomic, weak) id <DistingDropDownMenuDataSource> dataSource;
@property (nonatomic, weak) id <DistingDropDownMenuDelegate> delegate;

@property (nonatomic, assign) UITableViewCellStyle cellStyle; // default value1
@property (nonatomic, strong) UIColor *indicatorColor;      // 三角指示器颜色
@property (nonatomic, strong) UIColor *textColor;           // 文字title颜色
@property (nonatomic, strong) UIColor *textSelectedColor;   // 文字title选中颜色
@property (nonatomic, strong) UIColor *detailTextColor;     // detailText文字颜色
@property (nonatomic, strong) UIFont *detailTextFont;       // font
@property (nonatomic, strong) UIColor *separatorColor;      // 分割线颜色
@property (nonatomic, assign) NSInteger fontSize;           // 字体大小
// 当有二级列表item时，点击row 是否调用点击代理方法
@property (nonatomic, assign) BOOL isClickHaveItemValid;

@property (nonatomic, getter=isRemainMenuTitle) BOOL remainMenuTitle; // 切换条件时是否更改menu title
@property (nonatomic, strong) NSMutableArray  *currentSelectRowArray; // 恢复默认选项用
/**
 *  the width of menu will be set to screen width defaultly
 *
 *  @param origin the origin of this view's frame
 *  @param height menu's height
 *
 *  @return menu
 */
- (void)backgroundTappedClear;

- (instancetype)initWithOrigin:(CGPoint)origin andHeight:(CGFloat)height;

// 获取title
- (NSString *)titleForRowAtIndexPath:(DistingIndexPath *)indexPath;

// 重新加载数据
- (void)reloadData;

// 创建menu 第一次显示 不会调用点击代理，这个手动调用
- (void)selectDefalutIndexPath;

- (void)selectIndexPath:(DistingIndexPath *)indexPath; // 默认trigger delegate

- (void)selectIndexPath:(DistingIndexPath *)indexPath triggerDelegate:(BOOL)trigger; // 调用代理
@end

