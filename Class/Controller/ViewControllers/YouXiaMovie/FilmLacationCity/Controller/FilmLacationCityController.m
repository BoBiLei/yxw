//
//  FilmLacationCityController.m
//  youxia
//
//  Created by mac on 2016/12/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmLacationCityController.h"
#import "CinemaCityDB.h"
#import "CinemaCityModel.h"
#import <CoreLocation/CoreLocation.h>

@interface FilmLacationCityController ()<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager* locationManager;

@end

@implementation FilmLacationCityController{
    
    CinemaCityDB *db;
    
    UITableView *myTable;
    NSMutableDictionary *dataDic;
    //首字母
    NSMutableArray *indexArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavBar];
    
    [self setUpTable];
    
    [self startLocation];
}

#pragma mark - AddNavBar
-(void)addNavBar{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:back];
    
    UIButton *titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    titleLabel.titleLabel.font=[UIFont systemFontOfSize:16];
    [titleLabel setTitle:@"城市列表" forState:UIControlStateNormal];
    [view addSubview:titleLabel];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)turnBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 开始定位
-(void)startLocation{
    if ([CLLocationManager locationServicesEnabled]) {//判断定位操作是否被允许
        
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;//遵循代理
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        self.locationManager.distanceFilter = 10.0f;
        
        [_locationManager requestWhenInUseAuthorization];//使用程序其间允许访问位置数据（iOS8以上版本定位需要）
        
        [self.locationManager startUpdatingLocation];//开始定位
        
    }else{//不能定位用户的位置的情况再次进行判断，并给与用户提示
        
        //1.提醒用户检查当前的网络状况
        
        //2.提醒用户打开定位开关
        NSLog(@"提醒用户打开定位开关");
        
    }
    
}

#pragma mark - CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    //当前所在城市的坐标值
    CLLocation *currLocation = [locations lastObject];
    
    //根据经纬度反向地理编译出地址信息
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:currLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark * placemark in placemarks) {
            
            NSDictionary *address = [placemark addressDictionary];
            
//            NSLog(@"%@--%@", [address objectForKey:@"Country"],error);
//            
//            NSLog(@"%@--%@", [address objectForKey:@"State"],error);
//            
//            NSLog(@"%@--%ld", [address objectForKey:@"City"],(long)error.code);
            
            if (indexArray.count!=0) {
                NSString *gpsKey=@"GPS";
                [indexArray removeFirstObject];
                [dataDic removeObjectForKey:gpsKey];
                
                CinemaCityModel *model=[CinemaCityModel new];
                NSString *cityStr=[NSString stringWithFormat:@"%@",[address objectForKey:@"City"]];
                NSRange rang=[cityStr rangeOfString:@"市"];
                if (rang.location!=NSNotFound) {
                    cityStr=[cityStr substringToIndex:rang.location];
                }
                model.cityName=cityStr;
                model.cityId=[db getCityIdWithName:model.cityName];
                [dataDic setObject:@[model] forKey:gpsKey];
                [indexArray insertObject:gpsKey atIndex:0];
                
                [UIView animateWithDuration:0.001 animations:^{
                    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                    [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                }];
            }
        }
    }];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if ([error code] == kCLErrorDenied){
        NSLog(@"访问被拒绝");
        //添加GPS
        CinemaCityModel *model=[CinemaCityModel new];
        model.cityName=@"无法获取定位信息";
        model.cityId=@"ErrorDenied";
        NSString *gpsKey=@"GPS";
        [dataDic removeObjectForKey:gpsKey];
        [indexArray removeFirstObject];
        [dataDic setObject:@[model] forKey:gpsKey];
        [indexArray insertObject:gpsKey atIndex:0];
        
        //一个section刷新
        NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
        [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    if ([error code] == kCLErrorLocationUnknown) {
        NSLog(@"无法获取位置信息");
    }
}

#pragma mark - setUp Tableview
-(void)setUpTable{
    
    db=[CinemaCityDB shareDB];
    [db openDBForPath:@"cinemacity"];
    
    NSDictionary *cityDic=[db getCinemaCity];
    
    NSArray *orgIndexArr = [cityDic allKeys];
    orgIndexArr = [orgIndexArr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    
    indexArray=[NSMutableArray arrayWithArray:orgIndexArr];
    
    dataDic=[NSMutableDictionary dictionaryWithDictionary:cityDic];
    
    NSArray *hotCityArr=[db getHotCinemaCity];
    NSString *hotKey=@"热门";
    [dataDic setObject:hotCityArr forKey:hotKey];
    [indexArray insertObject:hotKey atIndex:0];
    
    //添加GPS
    CinemaCityModel *model=[CinemaCityModel new];
    model.cityName=@"正在定位…";
    NSString *gpsKey=@"GPS";
    [dataDic setObject:@[model] forKey:gpsKey];
    [indexArray insertObject:gpsKey atIndex:0];
    
    
    //UITableView
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    [myTable setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSectionIndexColor:[UIColor colorWithHexString:@"777777"]];    //字母颜色
    [myTable setSectionIndexBackgroundColor:[UIColor clearColor]];//清空section颜色
    [self.view addSubview:myTable];
    
    myTable.tableFooterView=[UIView new];
}

#pragma mark - UITableView
//section
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [dataDic allKeys].count;
}
//row
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = [dataDic objectForKey:indexArray[section]];
    return array.count;
}
//height
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}
//初始化cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *ID1 = @"cellIdentifier1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID1];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID1];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.adjustsFontSizeToFitWidth=YES;
    cell.textLabel.textColor=[UIColor colorWithHexString:indexPath.section==0?@"FB8023":@"515151"];
    cell.textLabel.font=kFontSize16;
    if (indexPath.row<[dataDic allKeys].count) {
        CinemaCityModel *model = [[dataDic objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
        [cell.textLabel setText:model.cityName];
    }
    return cell;
}

//返回的字母
-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return indexArray;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 24)];
    headerView.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, SCREENSIZE.width-24, headerView.height)];
    label.textColor = [UIColor colorWithHexString:@"1a1a1a"];
    label.font=[UIFont systemFontOfSize:15];
    label.text=indexArray[section];
    [headerView addSubview:label];
    
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CinemaCityModel *model = [[dataDic objectForKey:indexArray[indexPath.section]] objectAtIndex:indexPath.row];
    if ([model.cityId isEqualToString:@"ErrorDenied"]) {
        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"定位服务未开启"message:@"请在系统设置中开启定位服务"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"暂不"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        }];
        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"去设置"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
            //定位服务设置界面
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [AppUtils saveValue:model.cityId forKey:Cinema_City_Id];
        [AppUtils saveValue:model.cityName forKey:Cinema_City_Name];
        [[NSNotificationCenter defaultCenter] postNotificationName:SelectCinema_City_Notification object:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
