//
//  YingYuanDetailHeaderCell.h
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel.h>
#import "CinemaInfoModel.h"

@protocol YingYuanDetailHeaderDelegate <NSObject>

-(void)tapHeaderNameView;

@end

@interface YingYuanDetailHeaderCell : UITableViewCell<iCarouselDataSource, iCarouselDelegate>


@property (weak, nonatomic) IBOutlet UILabel *cinemaName;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic, strong) iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UIView *yyNameHeaderView;

@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentView;

@property (weak, nonatomic) id<YingYuanDetailHeaderDelegate> delegate;

-(void)reflushDataForModel:(CinemaInfoModel *)model withDataArray:(NSArray *)array;

@end
