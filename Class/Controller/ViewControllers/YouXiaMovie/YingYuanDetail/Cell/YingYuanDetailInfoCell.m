//
//  YingYuanDetailInfoCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YingYuanDetailInfoCell.h"

@implementation YingYuanDetailInfoCell{
    GouPiaoInfoModel *receiveModel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.starTimeLabel.adjustsFontSizeToFitWidth=YES;
    self.endTimeLabel.adjustsFontSizeToFitWidth=YES;
    self.langageLabel.adjustsFontSizeToFitWidth=YES;
    self.tingNum.adjustsFontSizeToFitWidth=YES;
    self.nowPrice.adjustsFontSizeToFitWidth=YES;
    
    //
    self.buyBtn.layer.cornerRadius=5;
    self.buyBtn.layer.borderWidth=1;
    self.buyBtn.layer.borderColor=[UIColor colorWithHexString:@"55b2f0"].CGColor;
    [self.buyBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickBtn:(id)sender {
    [self.delegate clickBuy:receiveModel];
}

-(void)reflushDataForModel:(GouPiaoInfoModel *)model{
    receiveModel=model;
    self.starTimeLabel.text=model.play_time;
    self.endTimeLabel.text=[NSString stringWithFormat:@"%@散场",model.end_time];
    self.langageLabel.text=[NSString stringWithFormat:@"%@%@",model.language,model.dimensional];
    self.tingNum.text=[NSString stringWithFormat:@"%@",model.hallName];
    self.nowPrice.text=[NSString stringWithFormat:@"%@元",model.price];
    NSString *str = [NSString stringWithFormat:@"%@元",model.marketPrice];
    NSDictionary *dict = @{
                           NSStrikethroughStyleAttributeName:[NSNumber           numberWithInteger:NSUnderlineStyleThick],
                           NSStrikethroughColorAttributeName:[UIColor colorWithHexString:@"787878"]
                           };
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:str];
    [attribtStr addAttributes:dict range:NSMakeRange(0, str.length)];
    _origPrice.attributedText = attribtStr;
}

@end
