//
//  YingYuanDetailHeaderCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YingYuanDetailHeaderCell.h"
#import "YYDetailMovieListModel.h"

@implementation YingYuanDetailHeaderCell{
    NSString *phoneStr;
    
    NSArray *dataArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tipLabel.layer.cornerRadius=2;
    self.tipLabel.layer.borderWidth=0.5f;
    self.tipLabel.layer.borderColor=[UIColor colorWithHexString:@"198ccb"].CGColor;
    
    _yyNameHeaderView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapName=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeaderName)];
    [_yyNameHeaderView addGestureRecognizer:tapName];
    _commentView.userInteractionEnabled=YES;
    
    self.carousel=[[iCarousel alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 148)];
    _carousel.dataSource=self;
    _carousel.delegate=self;
    _carousel.type = iCarouselTypeCoverFlow2;
    _carousel.decelerationRate = 1.2;
    _carousel.scrollSpeed=1.5;
    _carousel.bounceDistance=2;
    [_commentView addSubview:_carousel];
}

-(void)tapHeaderName{
    [self.delegate tapHeaderNameView];
}

-(void)reflushDataForModel:(CinemaInfoModel *)model withDataArray:(NSArray *)array{
    phoneStr=model.tel;
    self.cinemaName.text=model.cinemaName;
    self.addressLabel.text=model.address;
    dataArr=array;
    [self.carousel reloadData];
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return dataArr.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    UILabel *label = nil;
    
    if (view == nil){
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 88.0f, 130.0f)];
        view.contentMode = UIViewContentModeScaleToFill;
    }else{
        label = (UILabel *)[view viewWithTag:1];
    }
    YYDetailMovieListModel *model=dataArr[index];
    [((UIImageView *)view) sd_setImageWithURL:[NSURL URLWithString:model.picAddr] placeholderImage:[UIImage imageNamed:Image_Default]];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option){
        case iCarouselOptionWrap:
        {
            return 0;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionTilt:
        {
            return 0;
        }
        case iCarouselOptionSpacing:
        {
            return 1.15;
        }
        default:
        {
            return value;
        }
    }
    
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    YYDetailMovieListModel *model=dataArr[carousel.currentItemIndex];
    NSLog(@"%ld=====",carousel.currentItemIndex);
}

- (IBAction)clickCall:(id)sender {
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",phoneStr];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

@end
