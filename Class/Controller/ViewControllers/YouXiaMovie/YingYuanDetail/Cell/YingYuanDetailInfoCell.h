//
//  YingYuanDetailInfoCell.h
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GouPiaoInfoModel.h"

@protocol YingYuanDetailInfoDelegate <NSObject>

-(void)clickBuy:(GouPiaoInfoModel *)model;

@end

@interface YingYuanDetailInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *starTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *langageLabel;
@property (weak, nonatomic) IBOutlet UILabel *tingNum;
@property (weak, nonatomic) IBOutlet UILabel *nowPrice;


@property (weak, nonatomic) IBOutlet UILabel *origPrice;  //原价
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;

@property (nonatomic, weak) id<YingYuanDetailInfoDelegate> delegate;

-(void)reflushDataForModel:(GouPiaoInfoModel *)model;

@end
