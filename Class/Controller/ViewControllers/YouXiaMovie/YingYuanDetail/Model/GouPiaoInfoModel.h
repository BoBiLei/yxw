//
//  GouPiaoInfoModel.h
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GouPiaoInfoModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *cinemaId;
@property (nonatomic, copy) NSString *cinemaName;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *dimensional;
@property (nonatomic, copy) NSString *end_time;
@property (nonatomic, copy) NSString *marketPrice;
@property (nonatomic, copy) NSString *play_time;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, copy) NSString *showDate;
@property (nonatomic, copy) NSString *showTime;
@property (nonatomic, copy) NSString *timeout;

@property (nonatomic, copy) NSString *hallName;         //几号厅
@property (nonatomic, copy) NSString *language;         //语言
@property (nonatomic, copy) NSString *foretellId;       //场次ID

-(void)jsonDataForDictioary:(NSDictionary *)dic;

@end
