//
//  YYDetailDateModel.m
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YYDetailDateModel.h"

@implementation YYDetailDateModel

-(void)jsonDataForDictioary:(NSDictionary *)dic{
    self.name=[NSString stringWithFormat:@"%@",dic[@"name"]];
    self.infoArr=dic[@"info"];
}

@end
