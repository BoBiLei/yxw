//
//  GouPiaoInfoModel.m
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "GouPiaoInfoModel.h"

@implementation GouPiaoInfoModel

-(void)jsonDataForDictioary:(NSDictionary *)dic{
    self.address=[NSString stringWithFormat:@"%@",dic[@"address"]];
    self.cinemaId=[NSString stringWithFormat:@"%@",dic[@"cinemaId"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.cityName=[NSString stringWithFormat:@"%@",dic[@"cityName"]];
    self.dimensional=[NSString stringWithFormat:@"%@",dic[@"dimensional"]];
    self.end_time=[NSString stringWithFormat:@"%@",dic[@"end_time"]];
    self.marketPrice=[NSString stringWithFormat:@"%@",dic[@"marketPrice"]];
    self.play_time=[NSString stringWithFormat:@"%@",dic[@"play_time"]];
    self.price=[NSString stringWithFormat:@"%@",dic[@"price"]];
    self.region=[NSString stringWithFormat:@"%@",dic[@"region"]];
    self.showDate=[NSString stringWithFormat:@"%@",dic[@"showDate"]];
    self.showTime=[NSString stringWithFormat:@"%@",dic[@"showTime"]];
    self.timeout=[NSString stringWithFormat:@"%@",dic[@"timeout"]];
    
    self.hallName=[NSString stringWithFormat:@"%@",dic[@"hallName"]];
    self.language=[NSString stringWithFormat:@"%@",dic[@"language"]];
    self.foretellId=[NSString stringWithFormat:@"%@",dic[@"foretellId"]];
}

@end
