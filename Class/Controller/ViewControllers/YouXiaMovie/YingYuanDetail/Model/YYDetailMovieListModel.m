//
//  YYDetailMovieListModel.m
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YYDetailMovieListModel.h"

@implementation YYDetailMovieListModel

-(void)jsonDataForDictioary:(NSDictionary *)dic{
    self.grade=[NSString stringWithFormat:@"%@",dic[@"grade"]];
    self.movie_id=[NSString stringWithFormat:@"%@",dic[@"movie_id"]];
    self.movie_name=[NSString stringWithFormat:@"%@",dic[@"movie_name"]];
    self.picAddr=[NSString stringWithFormat:@"%@",dic[@"picAddr"]];
    self.director=[NSString stringWithFormat:@"%@",dic[@"director"]];
    self.language=[NSString stringWithFormat:@"%@",dic[@"language"]];
    self.length=[NSString stringWithFormat:@"%@",dic[@"length"]];
    self.type=[NSString stringWithFormat:@"%@",dic[@"type"]];
}

@end
