//
//  CinemaInfoModel.m
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CinemaInfoModel.h"

@implementation CinemaInfoModel

-(void)jsonDataForDictioary:(NSDictionary *)dic{
    self.address=[NSString stringWithFormat:@"%@",dic[@"address"]];
    self.busPath=[NSString stringWithFormat:@"%@",dic[@"busPath"]];
    self.businessCircle=[NSString stringWithFormat:@"%@",dic[@"businessCircle"]];
    self.cinemaId=[NSString stringWithFormat:@"%@",dic[@"cinemaId"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.cityId=[NSString stringWithFormat:@"%@",dic[@"cityId"]];
    self.cityName=[NSString stringWithFormat:@"%@",dic[@"cityName"]];
    self.hallIds=[NSString stringWithFormat:@"%@",dic[@"hallIds"]];
    self.hallNames=[NSString stringWithFormat:@"%@",dic[@"hallNames"]];
    self.iid=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.latitude=[NSString stringWithFormat:@"%@",dic[@"latitude"]];
    self.logo=[NSString stringWithFormat:@"%@",dic[@"logo"]];
    self.longitude=[NSString stringWithFormat:@"%@",dic[@"longitude"]];
    self.region=[NSString stringWithFormat:@"%@",dic[@"region"]];
    self.seatCounts=[NSString stringWithFormat:@"%@",dic[@"seatCounts"]];
    self.seatFlag=[NSString stringWithFormat:@"%@",dic[@"seatFlag"]];
    self.state=[NSString stringWithFormat:@"%@",dic[@"state"]];
    self.subway=[NSString stringWithFormat:@"%@",dic[@"subway"]];
    self.tel=[NSString stringWithFormat:@"%@",dic[@"tel"]];
    self.ticketFlag=[NSString stringWithFormat:@"%@",dic[@"ticketFlag"]];
    self.vipFlags=[NSString stringWithFormat:@"%@",dic[@"vipFlags"]];
}

@end
