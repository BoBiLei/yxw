//
//  YYDetailMovieListModel.h
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYDetailMovieListModel : NSObject

@property (nonatomic, copy) NSString *grade;
@property (nonatomic, copy) NSString *movie_id;
@property (nonatomic, copy) NSString *movie_name;
@property (nonatomic, copy) NSString *picAddr;
@property (nonatomic, copy) NSString *director;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *length;
@property (nonatomic, copy) NSString *type;

-(void)jsonDataForDictioary:(NSDictionary *)dic;

@end
