//
//  YYDetailDateModel.h
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  日期

#import <Foundation/Foundation.h>

@interface YYDetailDateModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) NSArray *infoArr;

-(void)jsonDataForDictioary:(NSDictionary *)dic;

@end
