//
//  CinemaInfoModel.h
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CinemaInfoModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *busPath;
@property (nonatomic, copy) NSString *businessCircle;
@property (nonatomic, copy) NSString *cinemaId;
@property (nonatomic, copy) NSString *cinemaName;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *hallIds;
@property (nonatomic, copy) NSString *hallNames;
@property (nonatomic, copy) NSString *iid;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, copy) NSString *seatCounts;
@property (nonatomic, copy) NSString *seatFlag;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *subway;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *ticketFlag;
@property (nonatomic, copy) NSString *vipFlags;

-(void)jsonDataForDictioary:(NSDictionary *)dic;

@end
