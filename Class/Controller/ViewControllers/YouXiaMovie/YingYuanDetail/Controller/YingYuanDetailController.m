//
//  YingYuanDetailController.m
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YingYuanDetailController.h"
#import "YingYuanDetailInfoCell.h"
#import "HMSegmentedControl.h"
#import "PickingSeatController.h"
#import "YingYuanIntro.h"
#import "CinemaInfoModel.h"
#import "YYDetailMovieListModel.h"
#import "YYDetailDateModel.h"
#import "GouPiaoInfoModel.h"
#import <iCarousel.h>

@interface YingYuanDetailController ()<UITableViewDataSource,UITableViewDelegate,YingYuanDetailInfoDelegate,iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) iCarousel *carousel;

@end

@implementation YingYuanDetailController{
    
    UIImageView *cellListHeaderView;
    
    TTTAttributedLabel *movieNameLabel;
    UILabel *movieTypeLabel;
    
    //
    UITableView *myTable;
    CinemaInfoModel *cinemaInfoModel;
    NSMutableArray *movieListArr;
    
    NSMutableArray *dateArr;            //日期
    NSMutableArray *dateTitleArr;
    NSMutableArray *sourceArr;
    
    
    NSInteger seleIndex;
    
    HMSegmentedControl *mySegment;
    
    BOOL canScroRequest;
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"影院详情";
    [self.view addSubview:navBar];
    
    [self setUpTableView];
    
    if (_type==0) {
        [self dyyDataRequest];
    }else{
        [self dataRequest];
    }
}

-(void)setUpTableView{
    
    seleIndex=0;
    
    movieListArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.backgroundColor=[UIColor colorWithHexString:@"eeeeee"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.hidden=YES;
    [myTable registerNib:[UINib nibWithNibName:@"YingYuanDetailInfoCell" bundle:nil] forCellReuseIdentifier:@"YingYuanDetailInfoCell"];
    [self.view addSubview:myTable];
    
    UIView *footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 16)];
    footView.backgroundColor=myTable.backgroundColor;
    myTable.tableFooterView=footView;
}

-(UIView *)addHeaderView{
    UIView *headView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 318)];
    headView.backgroundColor=[UIColor whiteColor];
    
    //callBtn
    UIButton *callBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    callBtn.frame=CGRectMake(SCREENSIZE.width-56, 23, 40, 40);
    [callBtn setImage:[UIImage imageNamed:@"yydetail_phone"] forState:UIControlStateNormal];
    [callBtn addTarget:self action:@selector(clickCallBtn) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:callBtn];
    
    //tapView
    UIView *tapView=[[UIView alloc] initWithFrame:CGRectMake(2, 8, headView.width-54, 70)];
    [headView addSubview:tapView];
    tapView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapName=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeaderNameView)];
    [tapView addGestureRecognizer:tapName];
    
    //电影院名称
    UILabel *cinemaLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 10, tapView.width-16, 21)];
    cinemaLabel.textColor=[UIColor colorWithHexString:@"000000"];
    cinemaLabel.font=[UIFont boldSystemFontOfSize:16];
    cinemaLabel.text=cinemaInfoModel.cinemaName;
    [tapView addSubview:cinemaLabel];
    
    [AppUtils drawZhiXian:tapView withColor:[UIColor colorWithHexString:@"e4e4e4"] height:1.0 firstPoint:CGPointMake(tapView.width-22, 4) endPoint:CGPointMake(tapView.width-22, tapView.height-4)];
    
    //电影院地址
    UILabel *cinemaAddress=[[UILabel alloc] initWithFrame:CGRectMake(8, cinemaLabel.origin.y+cinemaLabel.height+8, tapView.width-41, 21)];
    cinemaAddress.textColor=[UIColor colorWithHexString:@"949494"];
    cinemaAddress.font=[UIFont systemFontOfSize:12];
    cinemaAddress.text=cinemaInfoModel.address;
    [tapView addSubview:cinemaAddress];
    
    //cellListView
    cellListHeaderView=[[UIImageView alloc] initWithFrame:CGRectMake(0, tapView.origin.y+tapView.height+8, SCREENSIZE.width, 148)];
    cellListHeaderView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yydetail_headerbg"]];
    cellListHeaderView.userInteractionEnabled=YES;
    [headView addSubview:cellListHeaderView];
    
    FXBlurView *blueView=[[FXBlurView alloc] initWithFrame:cellListHeaderView.frame];
    [blueView setDynamic:YES];
    blueView.iterations=4;
    [headView addSubview:blueView];
    
    UIView *blurbgView=[[UIView alloc] initWithFrame:cellListHeaderView.frame];
    blurbgView.backgroundColor=[UIColor blackColor];
    blurbgView.alpha=0.4;
    [headView addSubview:blurbgView];
    
    self.carousel=[[iCarousel alloc] initWithFrame:CGRectMake(0, cellListHeaderView.origin.y, SCREENSIZE.width, 148)];
    _carousel.dataSource=self;
    _carousel.delegate=self;
    _carousel.type = iCarouselTypeCoverFlow2;
    _carousel.decelerationRate = 0.65;
    _carousel.scrollSpeed=0.65;
    _carousel.bounceDistance=1;
    _carousel.pagingEnabled=NO;
    _carousel.stopAtItemBoundary=NO;
    _carousel.currentItemIndex=seleIndex;
    [headView addSubview:_carousel];
    
    UIImageView *arr_btmImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 16, 6)];
    arr_btmImgv.center=CGPointMake(SCREENSIZE.width/2, _carousel.origin.y+_carousel.height-3);
    arr_btmImgv.contentMode=UIViewContentModeScaleAspectFit;
    arr_btmImgv.image=[UIImage imageNamed:@"cinima_detail_arrow"];
    [headView addSubview:arr_btmImgv];
    
    //电影名称
    CGFloat lastMagi=headView.height-(cellListHeaderView.origin.y+cellListHeaderView.height);
    movieNameLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, cellListHeaderView.origin.y+cellListHeaderView.height+12, headView.width, (lastMagi-24)/2)];
    movieNameLabel.textColor=[UIColor colorWithHexString:@"000000"];
    movieNameLabel.font=[UIFont systemFontOfSize:16];
    movieNameLabel.textAlignment=NSTextAlignmentCenter;
    [headView addSubview:movieNameLabel];
    
    //电影类型
    movieTypeLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, movieNameLabel.origin.y+movieNameLabel.height, headView.width, movieNameLabel.height)];
    movieTypeLabel.textColor=[UIColor colorWithHexString:@"949494"];
    movieTypeLabel.font=[UIFont systemFontOfSize:12];
    movieTypeLabel.textAlignment=NSTextAlignmentCenter;
    [headView addSubview:movieTypeLabel];
    
    [AppUtils drawZhiXian:headView withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, 318) endPoint:CGPointMake(SCREENSIZE.width, 318)];
    
    return headView;
}

#pragma mark - 点击拨打电话
-(void)clickCallBtn{
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",cinemaInfoModel.tel];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return sourceArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 46;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return mySegment;
}

#pragma mark - filterview
-(UIView *)filterview{
    for (UIView *controller in self.view.subviews) {
        if ([controller isKindOfClass:[HMSegmentedControl class]]) {
            [mySegment removeFromSuperview];
        }
    }
    mySegment = [[HMSegmentedControl alloc] initWithSectionTitles:dateTitleArr];
    mySegment.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    mySegment.frame = CGRectMake(0, 0, SCREENSIZE.width, 46);
    //mySegment.borderType=HMSegmentedControlBorderTypeBottom;
    mySegment.borderColor=[UIColor colorWithHexString:@"dcdcdc"];
    mySegment.selectionIndicatorColor=[UIColor colorWithHexString:@"55b2f0"];
    mySegment.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    mySegment.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    mySegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    mySegment.segmentWidthStyle=HMSegmentedControlSegmentWidthStyleDynamic;
    [mySegment setTitleFormatter:^NSAttributedString *(HMSegmentedControl *segmentedControl, NSString *title, NSUInteger index, BOOL selected) {
        NSDictionary *dic;
        if (selected) {
            dic=@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"55b2f0"],NSFontAttributeName:[UIFont systemFontOfSize:15]};
        }else{
            dic=@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"787878"],NSFontAttributeName:[UIFont systemFontOfSize:15]};
        }
        NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:dic];
        return attString;
    }];
    [mySegment addTarget:self action:@selector(segmentedChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [AppUtils drawZhiXian:mySegment withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, 0) endPoint:CGPointMake(SCREENSIZE.width, 0)];
    [AppUtils drawZhiXian:mySegment withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, 45.5f) endPoint:CGPointMake(SCREENSIZE.width, 45.5f)];
    return mySegment;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YingYuanDetailInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"YingYuanDetailInfoCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row<sourceArr.count){
        GouPiaoInfoModel *model=sourceArr[indexPath.row];
        [cell reflushDataForModel:model];
        cell.delegate=self;
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return movieListArr.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    UILabel *label = nil;
    
    if (view == nil){
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 88.0f, 130.0f)];
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds=YES;
    }else{
        label = (UILabel *)[view viewWithTag:1];
    }
    YYDetailMovieListModel *model=movieListArr[index];
    [((UIImageView *)view) sd_setImageWithURL:[NSURL URLWithString:model.picAddr] placeholderImage:[UIImage imageNamed:Image_Default]];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option){
        case iCarouselOptionWrap:
        {
            return 0;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionTilt:
        {
            return 0;
        }
        case iCarouselOptionSpacing:
        {
            return 1.15;
        }
        default:
        {
            return value;
        }
    }
    
}

- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel{
    if (canScroRequest) {
        dateArr=[NSMutableArray array];
        
        dateTitleArr=[NSMutableArray array];
        
        sourceArr=[NSMutableArray array];
        
        [myTable reloadData];
        
        //
        MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:self.view];
        CGFloat yf=IS_IPHONE6P?(SCREENSIZE.height-382)/2+40:(SCREENSIZE.height-382)/2+64;
        [hud setYOffset:yf];
        [hud show:YES];
//        [hud setOffset:CGPointMake(0, IS_IPHONE6P?(SCREENSIZE.height-382)/2+40:(SCREENSIZE.height-382)/2+64)];
//        [hud showAnimated:YES];
        [self.view addSubview:hud];
        
    }
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    YYDetailMovieListModel *model=movieListArr[carousel.currentItemIndex];
    NSData *imgData=[NSData dataWithContentsOfURL:[NSURL URLWithString:model.picAddr]];
    UIImage *headImg=[UIImage imageWithData:imgData];
    cellListHeaderView.backgroundColor=[UIColor colorWithPatternImage:headImg];
    
    if (canScroRequest) {
        
        YYDetailMovieListModel *model=movieListArr[carousel.currentItemIndex];
        NSString *str=[NSString stringWithFormat:@"%@ %@分",model.movie_name,model.grade];
        [movieNameLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@分",model.grade] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
        movieTypeLabel.text=[NSString stringWithFormat:@"%@分钟 | %@ | %@",model.length,model.type,model.director];
        
        //
        [self scrollerListRequest:model];
    }
    canScroRequest=YES;
}

#pragma mark - 添加 SelectDateSegment

-(void)segmentedChangedValue:(HMSegmentedControl *)segmentedControl{
    sourceArr=[NSMutableArray array];
    YYDetailDateModel *model=dateArr[segmentedControl.selectedSegmentIndex];
    NSArray *arr=model.infoArr;
    for (NSDictionary *dic in arr) {
        GouPiaoInfoModel *model=[GouPiaoInfoModel new];
        [model jsonDataForDictioary:dic];
        [sourceArr addObject:model];
    }
    [self refleshHeaderSection];
}

//一个section刷新
-(void)refleshHeaderSection{
    [UIView animateWithDuration:0.01 animations:^{
        NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
        [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

#pragma mark - 点击购买
-(void)clickBuy:(GouPiaoInfoModel *)model{
    PickingSeatController *ctr=[PickingSeatController new];
    ctr.gpModel=model;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - 滚动电影 Delegate
-(void)movieListDidScroller:(YYDetailMovieListModel *)model{
    [self scrollerListRequest:model];
}

#pragma mark - 查看影院介绍
-(void)tapHeaderNameView{
    YingYuanIntro *ctr=[YingYuanIntro new];
    ctr.model=cinemaInfoModel;
    [self presentViewController:ctr animated:YES completion:nil];
}

#pragma mark - request
//从电影详情过来的
-(void)dataRequest{
    
    [AppUtils showProgressInView:self.view];
    
    dateArr=[NSMutableArray array];
    
    dateTitleArr=[NSMutableArray array];
    
    sourceArr=[NSMutableArray array];
    
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"get_movie_for_cinema",
                         @"movie_id":_filmModel.fId,
                         @"cinemaId":_cinemaId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *cinemaDic=responseObject[@"retData"][@"cinema_info"];
            cinemaInfoModel=[CinemaInfoModel new];
            [cinemaInfoModel jsonDataForDictioary:cinemaDic];
            
            //电影列表
            NSArray *mvListObj=responseObject[@"retData"][@"movie_list"];
            if ([mvListObj isKindOfClass:[NSArray class]]) {
                for (int i=0; i<mvListObj.count; i++) {
                    NSDictionary *dic=mvListObj[i];
                    YYDetailMovieListModel *model=[YYDetailMovieListModel new];
                    [model jsonDataForDictioary:dic];
                    if ([_filmModel.fId isEqualToString:model.movie_id]) {
                        seleIndex=i;
                    }
                    [movieListArr addObject:model];
                }
            }
            
            //排期
            id paiqiObj=responseObject[@"retData"][@"movie_paiqi"];
            if ([paiqiObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in paiqiObj) {
                    YYDetailDateModel *model=[YYDetailDateModel new];
                    [model jsonDataForDictioary:dic];
                    [dateTitleArr addObject:model.name];
                    [dateArr addObject:model];
                }
            }
            
            //
            if (dateArr.count!=0) {
                YYDetailDateModel *model=dateArr[0];
                NSArray *arr=model.infoArr;
                for (NSDictionary *dic in arr) {
                    GouPiaoInfoModel *model=[GouPiaoInfoModel new];
                    [model jsonDataForDictioary:dic];
                    [sourceArr addObject:model];
                }
                
                myTable.hidden=NO;
                
                [self filterview];
                
                myTable.tableHeaderView=[self addHeaderView];
                
                if (movieListArr.count!=0) {
                    YYDetailMovieListModel *model=movieListArr[seleIndex];
                    NSString *str=[NSString stringWithFormat:@"%@ %@分",model.movie_name,model.grade];
                    [movieNameLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@分",model.grade] options:NSCaseInsensitiveSearch];
                        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
                        return mutableAttributedString;
                    }];
                    movieTypeLabel.text=[NSString stringWithFormat:@"%@分钟 | %@ | %@",model.length,model.type,model.director];
                }
                
                [self refleshHeaderSection];
            }else{
                [self showAlertView];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//从电影院/搜索电影院过来的
-(void)dyyDataRequest{
    
    [AppUtils showProgressInView:self.view];
    
    dateArr=[NSMutableArray array];
    
    dateTitleArr=[NSMutableArray array];
    
    sourceArr=[NSMutableArray array];
    
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"get_cinema_movie",
                         @"cinemaId":_cinemaId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *cinemaDic=responseObject[@"retData"][@"cinema_info"];
            cinemaInfoModel=[CinemaInfoModel new];
            [cinemaInfoModel jsonDataForDictioary:cinemaDic];
            
            //电影列表
            NSArray *mvListObj=responseObject[@"retData"][@"movie_list"];
            if ([mvListObj isKindOfClass:[NSArray class]]) {
                for (int i=0; i<mvListObj.count; i++) {
                    NSDictionary *dic=mvListObj[i];
                    YYDetailMovieListModel *model=[YYDetailMovieListModel new];
                    [model jsonDataForDictioary:dic];
                    if ([_filmModel.fId isEqualToString:model.movie_id]) {
                        seleIndex=i;
                    }
                    [movieListArr addObject:model];
                }
            }
            
            //排期
            id paiqiObj=responseObject[@"retData"][@"movie_paiqi"];
            if ([paiqiObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in paiqiObj) {
                    YYDetailDateModel *model=[YYDetailDateModel new];
                    [model jsonDataForDictioary:dic];
                    [dateTitleArr addObject:model.name];
                    [dateArr addObject:model];
                }
            }
            
            //
            if (dateArr.count!=0) {
                YYDetailDateModel *model=dateArr[0];
                NSArray *arr=model.infoArr;
                for (NSDictionary *dic in arr) {
                    GouPiaoInfoModel *model=[GouPiaoInfoModel new];
                    [model jsonDataForDictioary:dic];
                    [sourceArr addObject:model];
                }
                myTable.hidden=NO;
                
                [self filterview];
                
                myTable.tableHeaderView=[self addHeaderView];
                
                if (movieListArr.count!=0) {
                    YYDetailMovieListModel *model=movieListArr[0];
                    NSString *str=[NSString stringWithFormat:@"%@ %@分",model.movie_name,model.grade];
                    [movieNameLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@分",model.grade] options:NSCaseInsensitiveSearch];
                        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
                        return mutableAttributedString;
                    }];
                    movieTypeLabel.text=[NSString stringWithFormat:@"%@分钟 | %@ | %@",model.length,model.type,model.director];
                }
                
                [self refleshHeaderSection];
            }else{
                [self showAlertView];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)scrollerListRequest:(YYDetailMovieListModel *)model{
    dateArr=[NSMutableArray array];
    
    dateTitleArr=[NSMutableArray array];
    
    sourceArr=[NSMutableArray array];
    
    NSDictionary *dict=@{
                         @"is_iso":@"1", 
                         @"mod":@"m_movie",
                         @"code":@"get_movie_for_cinema",
                         @"movie_id":model.movie_id,
                         @"cinemaId":_cinemaId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //排期
            id paiqiObj=responseObject[@"retData"][@"movie_paiqi"];
            if ([paiqiObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in paiqiObj) {
                    YYDetailDateModel *model=[YYDetailDateModel new];
                    [model jsonDataForDictioary:dic];
                    [dateTitleArr addObject:model.name];
                    [dateArr addObject:model];
                }
            }
            
            //
            if (dateArr.count!=0) {
                YYDetailDateModel *model=dateArr[0];
                NSArray *arr=model.infoArr;
                for (NSDictionary *dic in arr) {
                    GouPiaoInfoModel *model=[GouPiaoInfoModel new];
                    [model jsonDataForDictioary:dic];
                    [sourceArr addObject:model];
                }
            }
            
            [self filterview];
            
            [self refleshHeaderSection];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - show alert
-(void)showAlertView{
    
    [[NetWorkRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"https://www.youxia.com/mgo/index.php?mod=mz_data&code=do_paiqi_one&cinemaid=%@",_cinemaId] method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
    
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"该影院暂无排期" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:yesAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
