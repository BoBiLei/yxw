//
//  YingYuanDetailController.h
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModel.h"
#import "CinemaModel.h"

@interface YingYuanDetailController : UIViewController

@property (nonatomic, assign) NSInteger type;//（0电影院  1电影）

@property (nonatomic, strong) FilmModel *filmModel;
@property (nonatomic, strong) NSString *cinemaId;

@end
