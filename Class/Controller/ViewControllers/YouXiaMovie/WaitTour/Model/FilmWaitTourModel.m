//
//  FilmWaitTourModel.m
//  youxia
//
//  Created by mac on 2016/12/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmWaitTourModel.h"

@implementation FilmWaitTourModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.address=[NSString stringWithFormat:@"%@",dic[@"address"]];
    self.atm=[NSString stringWithFormat:@"%@",dic[@"atm"]];
    self.buytime=[NSString stringWithFormat:@"%@",dic[@"buytime"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.coupon_id=[NSString stringWithFormat:@"%@",dic[@"coupon_id"]];
    self.credit_price=[NSString stringWithFormat:@"%@",dic[@"credit_price"]];
    self.hallName=[NSString stringWithFormat:@"%@",dic[@"hallName"]];
    self.movie_amount=[NSString stringWithFormat:@"%@",dic[@"movie_amount"]];
    self.movie_code=[NSString stringWithFormat:@"%@",dic[@"movie_code"]];
    self.movie_data=[NSString stringWithFormat:@"%@",dic[@"movie_data"]];
    self.movie_num=[NSString stringWithFormat:@"%@",dic[@"movie_num"]];
    self.movie_price=[NSString stringWithFormat:@"%@",dic[@"movie_price"]];
    self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.pay=[NSString stringWithFormat:@"%@",dic[@"pay"]];
    self.paymoney=[NSString stringWithFormat:@"%@",dic[@"paymoney"]];
    self.phone=[NSString stringWithFormat:@"%@",dic[@"phone"]];
    self.playtime=[NSString stringWithFormat:@"%@",dic[@"playtime"]];
    self.position=dic[@"position"];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.tel=[NSString stringWithFormat:@"%@",dic[@"tel"]];
    self.seatType=[NSString stringWithFormat:@"%@",dic[@"seatType"]];
    self.codeDic=[NSString stringWithFormat:@"%@",dic[@"code"]];
}

@end
