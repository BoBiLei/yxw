//
//  FilmWaitTourModel.h
//  youxia
//
//  Created by mac on 2016/12/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmWaitTourModel : NSObject

@property (nonatomic ,copy) NSString *address;
@property (nonatomic ,copy) NSString *atm;
@property (nonatomic ,copy) NSString *buytime;
@property (nonatomic ,copy) NSString *cinemaName;
@property (nonatomic ,copy) NSString *coupon_id;
@property (nonatomic ,copy) NSString *credit_price;
@property (nonatomic ,copy) NSString *hallName;
@property (nonatomic ,copy) NSString *movie_amount;
@property (nonatomic ,copy) NSString *movie_code;
@property (nonatomic ,copy) NSString *movie_data;
@property (nonatomic ,copy) NSString *movie_num;
@property (nonatomic ,copy) NSString *movie_price;
@property (nonatomic ,copy) NSString *orderid;
@property (nonatomic ,copy) NSString *pay;
@property (nonatomic ,copy) NSString *paymoney;
@property (nonatomic ,copy) NSString *phone;
@property (nonatomic ,copy) NSString *playtime;
@property (nonatomic ,retain) NSArray *position;
@property (nonatomic ,copy) NSString *status;
@property (nonatomic ,copy) NSString *tel;
@property (nonatomic ,copy) NSString *seatType;
@property (nonatomic ,copy) NSString *codeDic;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
