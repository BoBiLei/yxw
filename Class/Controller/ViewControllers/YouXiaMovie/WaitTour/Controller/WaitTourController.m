//
//  WaitTourController.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "WaitTourController.h"
#import "WaitTourHeaderCell.h"
#import "WaitTourBottomCell.h"
#import "TourSuccessModel.h"
#import "YingYuanDetailController.h"
#import "FilmListController.h"
#import "TourSuccessController.h"
#import "MovieActivityController.h"

@interface WaitTourController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation WaitTourController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewDidAppear:(BOOL)animated{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavBar];
    
    [self setUI];
    
    [self dataRequest];
}

-(void)addNavBar{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [self.view addSubview:view];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:back];
    
    UIButton *titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    titleLabel.titleLabel.font=[UIFont systemFontOfSize:17];
    [titleLabel setTitle:@"等待出票" forState:UIControlStateNormal];
    [view addSubview:titleLabel];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)turnBack{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[YingYuanDetailController class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FilmOrder_CancelNotifacation object:nil];
            [self.navigationController popToViewController:controller animated:YES];
        }else if ([controller isKindOfClass:[FilmListController class]]){
            [self.navigationController popToViewController:controller animated:YES];
        }else if ([controller isKindOfClass:[MovieActivityController class]]){
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - init table
-(void)setUI{
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    [myTable registerNib:[UINib nibWithNibName:@"WaitTourHeaderCell" bundle:nil] forCellReuseIdentifier:@"WaitTourHeaderCell"];
    [myTable registerNib:[UINib nibWithNibName:@"WaitTourBottomCell" bundle:nil] forCellReuseIdentifier:@"WaitTourBottomCell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 100;
    }
    return 244;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        WaitTourHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"WaitTourHeaderCell"];
        if (!cell) {
            cell=[[WaitTourHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WaitTourHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }else{
        WaitTourBottomCell *cell=[tableView dequeueReusableCellWithIdentifier:@"WaitTourBottomCell"];
        if (!cell) {
            cell=[[WaitTourBottomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WaitTourBottomCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataArr];
        return  cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - request
//等待出票
-(void)dataRequest{
    //[AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"queryOrder" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"queryOrder",
//                         @"orderid":_orderId,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"等待出票=%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                TourSuccessModel *model=[TourSuccessModel new];
                [model jsonDataForDictionary:retObj];
                [dataArr addObject:model];
                if ([model.status isEqualToString:@"1"] || [model.status isEqualToString:@"4"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self dataRequest];
                    });
                }else{
                    TourSuccessController *ctr=[TourSuccessController new];
                    ctr.orderId=_orderId;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
            }
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
