//
//  WaitTourBottomCell.h
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourSuccessModel.h"

@interface WaitTourBottomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView01;
@property (weak, nonatomic) IBOutlet UIView *mainView02;
@property (weak, nonatomic) IBOutlet UIView *mainView03;
@property (weak, nonatomic) IBOutlet UIView *mainView04;

@property (weak, nonatomic) IBOutlet UILabel *movieNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cinemaLabel;
@property (weak, nonatomic) IBOutlet UILabel *tingNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *seatLabel;


-(void)reflushDataForModel:(NSArray *)dataArr;

@end
