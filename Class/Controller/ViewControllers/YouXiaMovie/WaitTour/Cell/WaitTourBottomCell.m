//
//  WaitTourBottomCell.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "WaitTourBottomCell.h"

@implementation WaitTourBottomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.mainView01.layer.cornerRadius=3;
    self.mainView02.layer.cornerRadius=3;
    self.mainView03.layer.cornerRadius=3;
    self.mainView04.layer.cornerRadius=3;
    self.tingNumLabel.adjustsFontSizeToFitWidth=YES;
    self.timeLabel.adjustsFontSizeToFitWidth=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(NSArray *)dataArr{
    if (dataArr.count!=0) {
        
        TourSuccessModel *model=dataArr[0];
        
        self.movieNameLabel.text=model.movie_data;
        self.cinemaLabel.text=model.cinemaName;
        self.tingNumLabel.text=model.hallName;
        self.timeLabel.text=model.playtime;
        
        NSString *seatStr=@"";
        for (NSString *str in model.position) {
            NSString *itemStr=@"";
            NSArray *picarry=[str componentsSeparatedByString:@":"];
            for (int i=0; i<picarry.count; i++) {
                NSString *str=picarry[i];
                itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
            }
            seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
        }
        self.seatLabel.text=seatStr;
    }
}

@end
