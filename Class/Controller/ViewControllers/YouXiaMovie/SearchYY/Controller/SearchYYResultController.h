//
//  SearchYYResultController.h
//  youxia
//
//  Created by mac on 2016/11/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchYYResultController : UIViewController

@property (nonatomic, copy) NSString *keyWord;

@end
