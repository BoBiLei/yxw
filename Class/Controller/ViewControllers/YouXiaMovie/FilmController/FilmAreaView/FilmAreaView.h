//
//  FilmAreaView.h
//  youxia
//
//  Created by mac on 2016/11/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  电影院地区显示

#import <UIKit/UIKit.h>

@interface FilmAreaView : UIView

@property (nonatomic, retain) NSArray *dataArr;

@property (nonatomic, strong) UICollectionView *collectionView;

/**
 *type=0----scrollerview
 *type=1----uiview
 */
-(id)initInView:(UIView *)view andViewType:(NSInteger)type;

-(void)showFilmAreaViewCompletion:(void (^)(id))completion;

-(void)hiddenFilmAreaView;

@end
