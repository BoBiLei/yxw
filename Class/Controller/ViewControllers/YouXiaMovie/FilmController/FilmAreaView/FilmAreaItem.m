//
//  FilmAreaItem.m
//  youxia
//
//  Created by mac on 2016/11/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmAreaItem.h"

@implementation FilmAreaItem

- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleBtn.layer.cornerRadius=3;
    self.titleBtn.layer.borderWidth=0.7f;
    self.titleBtn.layer.borderColor=[UIColor colorWithHexString:@"c9c9c9"].CGColor;
    [self.titleBtn setTitleColor:[UIColor colorWithHexString:@"c9c9c9"] forState:UIControlStateNormal];
    self.titleBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    self.titleBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
}

-(void)setSelected:(BOOL)selected{
    if (selected) {
        self.titleBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        self.titleBtn.layer.borderColor=[UIColor colorWithHexString:@"55b2f0"].CGColor;
        [self.titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        self.titleBtn.backgroundColor=[UIColor whiteColor];
        self.titleBtn.layer.borderColor=[UIColor colorWithHexString:@"c9c9c9"].CGColor;
        [self.titleBtn setTitleColor:[UIColor colorWithHexString:@"c9c9c9"] forState:UIControlStateNormal];
    }
}

@end
