//
//  FilmAreaView.m
//  youxia
//
//  Created by mac on 2016/11/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmAreaView.h"
#import "FilmAreaItem.h"

@interface FilmAreaView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIView *bgView;
@property (copy) void (^onDismissCompletion)(id);

@end

@implementation FilmAreaView{
    
    UIView *supView;
    
    NSArray *sourceArr;
    
    id selectObject;
}


#pragma mark - Init
-(id)initInView:(UIView *)view andViewType:(NSInteger)type{
    self = [super init];
    if (self) {
        
        supView=view;
        
        [self setFrame:CGRectMake(type==0?SCREENSIZE.width:0, 0, SCREENSIZE.width, view.height)];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //bgView
        _bgView = [[UIView alloc] initWithFrame:CGRectZero];
        [_bgView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
        [_bgView setAlpha:0.0];
        [_bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
        [self addSubview:_bgView];
        
        //
        UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:myLayout];
        [_collectionView registerNib:[UINib nibWithNibName:@"FilmAreaItem" bundle:nil] forCellWithReuseIdentifier:@"cell"];
        _collectionView.backgroundColor=[UIColor whiteColor];
        _collectionView.dataSource=self;
        _collectionView.delegate=self;
        _collectionView.bounces=YES;
        [self addSubview:_collectionView];
    }
    return self;
}

-(void)setDataArr:(NSArray *)dataArr{
    selectObject=dataArr.firstObject;
    sourceArr=dataArr;
    NSInteger rowCount=0;
    if (dataArr.count%4==0) {
        rowCount=dataArr.count/4;
    }else{
        CGFloat rowFloat=dataArr.count/4;
        NSString *rowStr=[NSString stringWithFormat:@"%.2f",rowFloat];
        NSRange rang=[rowStr rangeOfString:@"."];
        if (rang.location!=NSNotFound) {
            rowStr=[rowStr substringToIndex:rang.location];
            rowCount=rowStr.integerValue+1;
        }
    }
    
    _bgView.frame=CGRectMake(0, 0, self.width, SCREENSIZE.height-49);
    
    _collectionView.frame=CGRectMake(0, 0, self.width, 32+12*(rowCount-1)+rowCount*36+1);
    [_collectionView setTransform:CGAffineTransformMakeTranslation(0, -CGRectGetHeight(_collectionView.frame))];
    [_collectionView reloadData];
    
    //刚开始默认选择第一个
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_collectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
}

#pragma mark - Show Methods
-(void)showFilmAreaViewCompletion:(void (^)(id))completion{
    [self setPickerHidden:NO callBack:nil];
    self.onDismissCompletion = completion;
}

-(void)hiddenFilmAreaView{
    selectObject=nil;
    [self setPickerHidden:YES callBack:self.onDismissCompletion];
}

#pragma mark - Dismiss Methods

-(void)dismiss{
    [self setPickerHidden:YES callBack:self.onDismissCompletion];
}

#pragma mark - Show/hide PickerView methods
-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(id))callBack{
    [supView addSubview:self];
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_bgView setAlpha:0.0];
                             [_collectionView setTransform:CGAffineTransformMakeTranslation(0.0, -CGRectGetHeight(_collectionView.frame))];
                         } else {
                             [_bgView setAlpha:1.0];
                             [_collectionView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [self removeFromSuperview];
                             if (selectObject!=nil) {
                                 callBack([self selectedObject]);
                             }
                         }
                     }];
}

#pragma mark - 点击透明背景hidden
- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_bgView setAlpha:1.0];
                         [_collectionView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

- (id)selectedObject {
    return selectObject;
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return sourceArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FilmAreaItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSString *str=sourceArr[indexPath.row];
    [cell.titleBtn setTitle:str forState:UIControlStateNormal];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth=(SCREENSIZE.width-84)/4;
    
    return CGSizeMake(cellWidth, 36);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 12;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(16, 12, 16, 12);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectObject=sourceArr[indexPath.row];
    [self dismiss];
}

@end
