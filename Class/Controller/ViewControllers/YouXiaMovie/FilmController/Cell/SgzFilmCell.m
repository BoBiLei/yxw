//
//  SgzFilmCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SgzFilmCell.h"
#import "IntroActorParam.h"

@implementation SgzFilmCell{
    UIImageView *imgv;
    UILabel *titleLabel;
    UILabel *detailLabel;
    UILabel *dateLabel;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat imgHeight=actor_imageWidth+26;
        
#pragma mark 图片
        
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(actor_marginY, 0, actor_imageWidth, imgHeight)];
        imgv.center=CGPointMake(actor_marginY+imgHeight/2, actor_imageWidth/2);
        imgv.contentMode=UIViewContentModeScaleToFill;
        //逆时针90°
        [imgv setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        imgv.image=[UIImage imageNamed:@"sgz_img"];
        [self addSubview:imgv];
        
#pragma mark 标题
        titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.height, actor_labelHeight)];
        titleLabel.center=CGPointMake(imgHeight+actor_marginY+actor_labelHeight/2+actor_titlemaginY, actor_imageWidth/2);
        titleLabel.font=[UIFont systemFontOfSize:14];
        titleLabel.textAlignment=NSTextAlignmentCenter;
        //逆时针90°
        [titleLabel setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        titleLabel.text=@"大闹天竺";
        titleLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
        [self addSubview:titleLabel];
        
#pragma mark 标题
        detailLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.height, actor_labelHeight)];
        detailLabel.center=CGPointMake(imgHeight+actor_marginY+actor_labelHeight/2+actor_labelHeight-2, actor_imageWidth/2);
        detailLabel.textColor=[UIColor colorWithHexString:@"#787878"];
        detailLabel.font=[UIFont systemFontOfSize:12];
        detailLabel.textAlignment=NSTextAlignmentCenter;
        [detailLabel setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        detailLabel.text=@"9560想看";
        [self addSubview:detailLabel];
        
#pragma mark 时间
        detailLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.height, actor_labelHeight)];
        detailLabel.center=CGPointMake(imgHeight+actor_marginY+actor_labelHeight/2+actor_labelHeight+16, actor_imageWidth/2);
        detailLabel.textColor=[UIColor colorWithHexString:@"#787878"];
        detailLabel.font=[UIFont systemFontOfSize:12];
        detailLabel.textAlignment=NSTextAlignmentCenter;
        [detailLabel setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        detailLabel.text=@"12-23";
        [self addSubview:detailLabel];
    }
    return self;
}

@end
