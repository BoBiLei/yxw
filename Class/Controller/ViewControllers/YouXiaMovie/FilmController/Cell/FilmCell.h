//
//  FilmCell.h
//  FilmCell
//
//  Created by tianzhuo on 5/27/16.
//  Copyright © 2016 tianzhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModel.h"

@protocol FilmCellDelegate <NSObject>

-(void)clickGoPickSeat:(FilmModel *)model;

@end

@interface FilmCell : UITableViewCell

@property (weak, nonatomic) id<FilmCellDelegate> delegate;

@property (nonatomic, strong) FilmModel *model;

-(void)reflushDataForModel:(FilmModel *)model isSelectWillPlay:(BOOL)isSelectWillPlay;

@end
