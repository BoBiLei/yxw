//
//  FilmCell.m
//  FilmCell
//
//  Created by tianzhuo on 5/27/16.
//  Copyright © 2016 tianzhuo. All rights reserved.
//

#import "FilmCell.h"

@interface FilmCell()


@end

@implementation FilmCell{
    UIImageView *imgv;
    UILabel *gradeLabel;
    TTTAttributedLabel *titleLabel;
    UILabel *detailLabel;
    UIButton *sellBtn;
    UILabel *actorLabel;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGRect frame=self.frame;
        frame.size.height=107;
        self.frame=frame;
        
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(6, 7, 70, self.height-14)];
        [self addSubview:imgv];
        
        gradeLabel=[[UILabel alloc] initWithFrame:CGRectMake(SCREENSIZE.width-64, 14, 54, 21)];
        gradeLabel.textAlignment=NSTextAlignmentRight;
        gradeLabel.font=[UIFont systemFontOfSize:15];
        gradeLabel.textColor=[UIColor colorWithHexString:@"FB8023"];
        [self addSubview:gradeLabel];
        
        titleLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(imgv.origin.x+imgv.width+10, 12, SCREENSIZE.width-(imgv.origin.x+imgv.width+30+gradeLabel.width), 18)];
        titleLabel.font=[UIFont systemFontOfSize:16];
        titleLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
        titleLabel.numberOfLines=0;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        [self addSubview:titleLabel];
        
        detailLabel=[[UILabel alloc] initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height+10, titleLabel.width, 26)];
        detailLabel.font=[UIFont systemFontOfSize:13];
        detailLabel.textColor=[UIColor colorWithHexString:@"6b6b6b"];
        [self addSubview:detailLabel];
        
        //
        sellBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        sellBtn.frame=CGRectMake(SCREENSIZE.width-72, self.height-39, 62, 28);
        sellBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        sellBtn.layer.cornerRadius=5;
        sellBtn.layer.borderWidth=1;
        sellBtn.layer.borderColor=[UIColor colorWithHexString:@"55b2f0"].CGColor;
        [sellBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        [sellBtn addTarget:self action:@selector(clickPiakBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:sellBtn];
        
        actorLabel=[[UILabel alloc] initWithFrame:CGRectMake(titleLabel.origin.x, self.height-39, detailLabel.width, 30)];
        actorLabel.font=[UIFont systemFontOfSize:12];
        actorLabel.textColor=[UIColor colorWithHexString:@"6b6b6b"];
        [self addSubview:actorLabel];
    }
    return self;
}

- (void)clickPiakBtn{
    [self.delegate clickGoPickSeat:self.model];
}

-(void)reflushDataForModel:(FilmModel *)model isSelectWillPlay:(BOOL)isSelectWillPlay{
    NSString *titStr;
    if ([model.dimensional isEqualToString:@""]) {
        titStr=[NSString stringWithFormat:@"%@%@",model.title,model.dimensional];
        [titleLabel setText:titStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.dimensional] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
            //背景色
            [mutableAttributedString addAttribute:kTTTBackgroundFillColorAttributeName
                                            value:[UIColor colorWithHexString:@"#55b2f0"]
                                            range:sumRange];
            [mutableAttributedString addAttribute:kTTTBackgroundStrokeColorAttributeName
                                            value:[UIColor colorWithHexString:@"#55b2f0"]
                                            range:sumRange];
            //
            [mutableAttributedString addAttribute:kTTTBackgroundCornerRadiusAttributeName
                                            value:@(2)
                                            range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:13];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else if([model.dimensional isEqualToString:@"2D"]){
        titStr=[NSString stringWithFormat:@"%@   %@ ",model.title,model.dimensional];
        [titleLabel setText:titStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@" %@ ",model.dimensional] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
            //背景色
            [mutableAttributedString addAttribute:kTTTBackgroundFillColorAttributeName
                                            value:[UIColor colorWithHexString:@"#55b2f0"]
                                            range:sumRange];
            [mutableAttributedString addAttribute:kTTTBackgroundStrokeColorAttributeName
                                            value:[UIColor colorWithHexString:@"#55b2f0"]
                                            range:sumRange];
            //
            [mutableAttributedString addAttribute:kTTTBackgroundCornerRadiusAttributeName
                                            value:@(2)
                                            range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:13];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        titStr=[NSString stringWithFormat:@"%@   %@ ",model.title,model.dimensional];
        [titleLabel setText:titStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@" %@ ",model.dimensional] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#55b2f0"] CGColor] range:sumRange];
            [mutableAttributedString addAttribute:kTTTBackgroundStrokeColorAttributeName
                                            value:[UIColor colorWithHexString:@"#55b2f0"]
                                            range:sumRange];
            //
            [mutableAttributedString addAttribute:kTTTBackgroundCornerRadiusAttributeName
                                            value:@(2)
                                            range:sumRange];
            UIFont *boldSystemFont = [UIFont systemFontOfSize:13];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
    self.model=model;
    [imgv sd_setImageWithURL:[NSURL URLWithString:model.imgv] placeholderImage:[UIImage imageNamed:Image_Default]];
    
    
    if ([model.isShow isEqualToString:@"1"]) {
        [sellBtn setTitle:@"购票" forState:UIControlStateNormal];
        sellBtn.hidden=NO;
    }else{
        [sellBtn setTitle:@"预售" forState:UIControlStateNormal];
        sellBtn.hidden=YES;
    }
    
    detailLabel.text=model.detail;
    
    gradeLabel.text=[NSString stringWithFormat:@"%@分",model.grade];
    
    if (isSelectWillPlay) {
        actorLabel.text=[NSString stringWithFormat:@"上映时间  %@",model.willPlayTime];
        actorLabel.font=[UIFont systemFontOfSize:14];
        actorLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
    }else{
        actorLabel.text=model.actor;
        actorLabel.font=[UIFont systemFontOfSize:12];
        actorLabel.textColor=[UIColor colorWithHexString:@"6b6b6b"];
    }
}

@end
