//
//  YingYuanCell.m
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YingYuanCell.h"

@implementation YingYuanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(YingYuanModel *)model{
    self.titleLabel.text=model.yyName;
    self.addressLabel.text=model.yyAddress;
    self.sumLabel.text=[NSString stringWithFormat:@"共有%@部电影正在上映",model.fangyingSum];
}

@end
