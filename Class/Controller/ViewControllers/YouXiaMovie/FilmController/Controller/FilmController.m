//
//  FilmController.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmController.h"
#import "NewFilmIntroController.h"
#import "FilmForYYListController.h"
#import "YingYuanCell.h"
#import "YingYuanDetailController.h"
#import "FilmCell.h"
#import "IntroActorParam.h"
#import "SgzFilmCell.h"
#import "SearchFilmNameController.h"
#import "FilmAreaView.h"
#import "FilmModel.h"
#import "YingYuanModel.h"
#import "SearchYYController.h"
#import "SDCycleScrollView.h"
#import "FilmLacationCityController.h"
#import "ImageBtn.h"
#import "MovieActivityController.h"

@interface FilmController ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,FilmCellDelegate,SDCycleScrollViewDelegate>


@end

@implementation FilmController{
    
    NSArray *advImageArr;
    
    UIButton *adrBtn;                       //地区
    
    //0--电影   1--影院
    NSInteger switchTag;
    
    //1--正在热映   2--即将上映
    NSInteger filmTableIndex;
    
    NSString *regionStr;                    //选择的区域
    NSString *currentRegion;                //当前区域
    
    
    
    
    NSInteger hadingFilmPageInt;            //正在热映当前页数
    MJRefreshAutoNormalFooter *hadingFilmFooter;
    NSMutableArray *hadingFilmArr;          //正在热映Arr
    
    BOOL willFilmFirst;
    BOOL hadingFilmFirst;
    NSInteger willFilmPageInt;              //即将上映当前页数
    MJRefreshAutoNormalFooter *willFilmFooter;
    NSMutableArray *willFilmArr;            //即将上映Arr
    
    BOOL yyFirst;
    BOOL areaFirst;
    NSInteger yyPageInt;                    //影院当前页数
    MJRefreshAutoNormalFooter *yyFooter;
    NSMutableArray *yyArr;                  //影院Arr
    
    UIButton *hadFilmBtn;       //正在热映按钮
    UIButton *willFilmBtn;      //即将上映
    
    UIScrollView *myScrolview;
    UIScrollView *filmScroller;
    UIButton *fillterBtn;
    NSArray *dataArr;
    UIView *btLine;
    UITableView *yyTable;
    UITableView *filmTable01;   //正在热映
    UITableView *filmTable02;   //即将上映
    UITableView *sgzFilmTabel;  //最受关注的影片
    
    FilmAreaView *filmAreaView;
    
    BOOL isSelectWillPlay;
    
    //Reflesh header
    MJRefreshNormalHeader *filmTableRefleshHeader01;
    MJRefreshNormalHeader *filmTableRefleshHeader02;
    MJRefreshNormalHeader *cinemaRefleshHeader;
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //去定位
    NSString *locStr=[AppUtils getValueWithKey:Cinema_City_Name];
    if ([locStr isEqualToString:@""] || locStr == nil) {
        [self.navigationController presentViewController:[FilmLacationCityController new] animated:YES completion:nil];
    }else{
        NSString *ccName=[AppUtils getValueWithKey:Cinema_City_Name];
        [adrBtn setTitle:ccName forState:UIControlStateNormal];
        [adrBtn setImage:[UIImage imageNamed:@"cinema_city_arrow"] forState:UIControlStateNormal];
        
        CGFloat labelWidth=[AppUtils getStringSize:ccName withFont:16].width;
        CGFloat imageWith=13;
        adrBtn.imageEdgeInsets = UIEdgeInsetsMake(1, labelWidth, 0, -labelWidth);
        adrBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, 0, imageWith);
    }
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateCurrentCity) name:SelectCinema_City_Notification object:nil];
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavigateView];
    
    [self setUpMainScrollview];
    
    [self setUpTopBtnForArray:@[@"正在热映",@"即将上映"]];
    
    [self setUpFilmScrollerView];
    
    [self setUPYingYuan];
    
    //正在上映1 即将上映2
    [self hadingFilmRequest];
}

#pragma mark - 更新当前城市 （选择城市后）
-(void)upDateCurrentCity{
    
    NSString *ccName=[AppUtils getValueWithKey:Cinema_City_Name];
    [adrBtn setTitle:ccName forState:UIControlStateNormal];
    [adrBtn setImage:[UIImage imageNamed:@"cinema_city_arrow"] forState:UIControlStateNormal];
    
    CGFloat labelWidth=[AppUtils getStringSize:ccName withFont:16].width;
    CGFloat imageWith=13;
    adrBtn.imageEdgeInsets = UIEdgeInsetsMake(1, labelWidth, 0, -labelWidth);
    adrBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, 0, imageWith);
    
    //页数重置为第一页
    hadingFilmPageInt=1;
    willFilmPageInt=1;
    yyPageInt=1;
    
    //所有元数据清空
    hadingFilmArr=[NSMutableArray array];
    willFilmArr=[NSMutableArray array];
    yyArr=[NSMutableArray array];
    
    //所有请求都设置为YES
    hadingFilmFirst=YES;
    willFilmFirst=YES;
    yyFirst=YES;
    areaFirst=YES;
    
    regionStr=@"";
    
    currentRegion=@"全部";
    
    if (switchTag==0) {
        if (filmTableIndex==1) {
            [self hadingFilmRequest];
        }else{
            [self willFilmRequest];
        }
    }else{
        [self yyFirstRequest];
        [self areaRequest];
    }
}

#pragma mark - 添加 NavigationBar
-(void)addNavigateView{
    
    switchTag=0;
    
    filmTableIndex=1;
    
    regionStr=@"";
    
    currentRegion=@"全部";
    
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    [self.view addSubview:view];
    
    adrBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    adrBtn.frame=CGRectMake(12, 20, 88, 44);
    adrBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    
    NSString *ccName=[AppUtils getValueWithKey:Cinema_City_Name];
    
    if ([ccName isEqualToString:@""]||ccName==nil||[ccName isKindOfClass:[NSNull class]]) {
        [adrBtn setTitle:@"获取定位" forState:UIControlStateNormal];
    }else{
        UIImage *btnImg=[UIImage imageNamed:@"cinema_city_arrow"];
        [adrBtn setTitle:ccName forState:UIControlStateNormal];
        [adrBtn setImage:btnImg forState:UIControlStateNormal];
        
        CGFloat labelWidth=[AppUtils getStringSize:ccName withFont:16].width;
        CGFloat imageWith=13;
        adrBtn.imageEdgeInsets = UIEdgeInsetsMake(1, labelWidth, 0, -labelWidth);
        adrBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, 0, imageWith);
    }
    
    [adrBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    adrBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [adrBtn addTarget:self action:@selector(clickCity) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:adrBtn];
    
#pragma mark UISegmentedControl
    UISegmentedControl *mySegment=[[UISegmentedControl alloc] initWithItems:@[@"电影",@"影院"]];
    mySegment.layer.cornerRadius = 4;
    mySegment.layer.masksToBounds = YES;
    //修改字体的默认颜色与选中颜色
    [mySegment setTitleTextAttributes:@{
                                      NSForegroundColorAttributeName:
                                          [UIColor whiteColor],
                                      NSFontAttributeName:[UIFont systemFontOfSize:15]}
                           forState:UIControlStateSelected];
    [mySegment setTitleTextAttributes:@{NSForegroundColorAttributeName:
                                          [UIColor colorWithHexString:@"55b2f0"],
                                      NSFontAttributeName:[UIFont systemFontOfSize:15]}
                           forState:UIControlStateNormal];
    mySegment.frame=CGRectMake(0, 0, 148, 30);
    mySegment.center=CGPointMake(SCREENSIZE.width/2, 42);
    mySegment.selectedSegmentIndex=0;
    mySegment.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    mySegment.tintColor=[UIColor colorWithHexString:@"55b2f0"];
    [mySegment addTarget:self action:@selector(switchingView:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySegment];
    
#pragma mark Search Btn
    UIButton *searchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    searchBtn.backgroundColor=[UIColor orangeColor];
    searchBtn.frame=CGRectMake(SCREENSIZE.width-40, 20, 36, 44);
    [searchBtn setImage:[UIImage imageNamed:@"film_searimg"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(clickNavSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:searchBtn];
    
#pragma mark Fillter Btn
    fillterBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    fillterBtn.frame=CGRectMake(SCREENSIZE.width-76, 20, 36, 44);
    [fillterBtn setImage:[UIImage imageNamed:@"film_filterimg"] forState:UIControlStateNormal];
    [fillterBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    fillterBtn.hidden=YES;
    [fillterBtn addTarget:self action:@selector(clickSelectAreaBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fillterBtn];
    
//    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

#pragma mark - 点击城市
-(void)clickCity{
    [filmAreaView hiddenFilmAreaView];
    [self.navigationController presentViewController:[FilmLacationCityController new] animated:YES completion:nil];
}

#pragma mark - SelectedSegment
-(void)switchingView:(id)sender{
    UISegmentedControl *segment=sender;
    switch (segment.selectedSegmentIndex) {
        case 0:
            [filmAreaView hiddenFilmAreaView];
            switchTag=0;
            fillterBtn.hidden=YES;
            [myScrolview setContentOffset:CGPointMake(0*SCREENSIZE.width, 0) animated:NO];
            if (filmTableIndex==1) {
                [self hadingFilmRequest];
            }else{
                [self willFilmRequest];
            }
            break;
        default:
            switchTag=1;
            fillterBtn.hidden=NO;
            [myScrolview setContentOffset:CGPointMake(1*SCREENSIZE.width, 0) animated:NO];
            [self yyFirstRequest];
            [self areaFirstRequest];
            break;
    }
}

#pragma mark - 点击搜索
-(void)clickNavSearch{
    if(switchTag==0){
        SearchFilmNameController *searCtr=[[SearchFilmNameController alloc]init];
        searCtr.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:searCtr animated:YES];
    }else{
        SearchYYController *searCtr=[[SearchYYController alloc]init];
        searCtr.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:searCtr animated:YES];
    }
    
}

#pragma mark - 点击选择区域
-(void)clickSelectAreaBtn{
    [filmAreaView showFilmAreaViewCompletion:^(id object) {
        NSString *backStr=(NSString *)object;
        if(![currentRegion isEqualToString:backStr]){
            [AppUtils showProgressInView:self.view];
            yyPageInt=1;
            yyArr=[NSMutableArray array];
            regionStr=backStr;
            [self yingyuanDataRequest];
        }
        currentRegion=backStr;
    }];
}

#pragma mark - SetUpScrollview
-(void)setUpMainScrollview{
    yyFirst=YES;
    areaFirst=YES;
    yyPageInt=1;
    yyArr=[NSMutableArray array];
    
    myScrolview=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113)];
    myScrolview.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    myScrolview.bounces=NO;
    myScrolview.showsHorizontalScrollIndicator=NO;
    myScrolview.showsVerticalScrollIndicator=NO;
    myScrolview.scrollEnabled=NO;
    myScrolview.pagingEnabled=NO;
    myScrolview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:myScrolview];
}

#pragma mark - adv delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSString *)index andAdvModel:(ImageModel *)model{
    if (![model.imgId isEqualToString:@""]) {
        MovieActivityController *ctr=[MovieActivityController new];
        ctr.model=model;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - setUp 正在热映筛选按钮
-(void)setUpTopBtnForArray:(NSArray *)arr{
    CGFloat btnWidth=SCREENSIZE.width/4;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    CGFloat btnY=0;
    for (int i=0; i<arr.count; i++) {
        NSString *name=arr[i];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i+1;
        btn.frame=CGRectMake((SCREENSIZE.width-2*btnWidth-16)/2+btnWidth*i+i*16, btnY, btnWidth, btnHeihgt);
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [myScrolview addSubview:btn];
        
        if (i==0) {
            lineX=btn.origin.x;
            hadFilmBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        }else{
            willFilmBtn=btn;
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake((SCREENSIZE.width-2*btnWidth-16)/2, btnY+btnHeihgt-2.5f, btnWidth, 3.0f)];
    btLine.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [myScrolview addSubview:btLine];
    
    [AppUtils drawZhiXian:myScrolview withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 44) endPoint:CGPointMake(SCREENSIZE.width, 44)];
}

#pragma mark - 点击正在热映/即将上映
-(void)clickFBtn:(id)sender{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==1) {
        
        filmTableIndex=1;
        
        isSelectWillPlay=NO;
        
        [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        hadFilmBtn.enabled=NO;
        
        [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        willFilmBtn.enabled=YES;
        
        [self hadingFilmRequest];     //正在上映
    }else{
        
        filmTableIndex=2;
        
        isSelectWillPlay=YES;
        
        [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
        hadFilmBtn.enabled=YES;
        
        [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
        willFilmBtn.enabled=NO;
        
        [self willFilmRequest];     //即将上映
    }
    
    //设置蓝色横条位置
    [UIView animateWithDuration:0.20 animations:^{
        CGFloat btnWidth=SCREENSIZE.width/4;
        CGRect frame=btLine.frame;
        if (btn.tag==1) {
            frame.origin.x=(SCREENSIZE.width-2*btnWidth-16)/2;
            [filmScroller setContentOffset:CGPointMake(0*SCREENSIZE.width, 0) animated:NO];
        }else{
            frame.origin.x=(SCREENSIZE.width-2*btnWidth-16)/2+btnWidth+16;
            [filmScroller setContentOffset:CGPointMake(1*SCREENSIZE.width, 0) animated:NO];
        }
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 电影的 ScrollView
//电影的scroll
-(void)setUpFilmScrollerView{
    
    willFilmFirst = YES;
    hadingFilmFirst = YES;
    hadingFilmArr=[NSMutableArray array];
    willFilmArr=[NSMutableArray array];
    
    filmScroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, SCREENSIZE.width, myScrolview.height-44)];
    filmScroller.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    filmScroller.bounces=NO;
    filmScroller.showsHorizontalScrollIndicator=NO;
    filmScroller.showsVerticalScrollIndicator=NO;
    filmScroller.scrollEnabled=YES;
    filmScroller.pagingEnabled=YES;
    filmScroller.delegate=self;
    [myScrolview addSubview:filmScroller];
    
    hadingFilmPageInt=1;
#pragma mark 正在热映 TableView
    filmTable01=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, filmScroller.width, filmScroller.height) style:UITableViewStyleGrouped];
    filmTable01.backgroundColor=[UIColor whiteColor];
    filmTable01.dataSource=self;
    filmTable01.delegate=self;
    filmTable01.separatorInset=UIEdgeInsetsMake(0, 82, 0, 0);
    [filmScroller addSubview:filmTable01];
    
    //下拉刷新
    filmTableRefleshHeader01 = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            hadingFilmPageInt=1;
            hadingFilmArr=[NSMutableArray array];
            [self filmDataRequestWithType:@"1"];
        });
    }];
    
    [filmTableRefleshHeader01 setMj_h:64];//设置高度
    [filmTableRefleshHeader01 setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [filmTableRefleshHeader01.stateLabel setMj_y:13];
    filmTableRefleshHeader01.stateLabel.font = kFontSize13;
    filmTableRefleshHeader01.lastUpdatedTimeLabel.font = kFontSize13;
    filmTable01.mj_header = filmTableRefleshHeader01;
    
    //加载更多
    hadingFilmFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self hadingFilmRefreshForPullUp];
    }];
    hadingFilmFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    hadingFilmFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [hadingFilmFooter setTitle:@"" forState:MJRefreshStateIdle];
    [hadingFilmFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [hadingFilmFooter setTitle:@"" forState:MJRefreshStateNoMoreData];
    hadingFilmFooter.triggerAutomaticallyRefreshPercent=0.5;
    filmTable01.mj_footer = hadingFilmFooter;
    
    
#pragma mark 即将上映 TableView
    willFilmPageInt=1;
    filmTable02=[[UITableView alloc]initWithFrame:CGRectMake(SCREENSIZE.width, 0, filmScroller.width, filmScroller.height) style:UITableViewStyleGrouped];
    filmTable02.backgroundColor=[UIColor whiteColor];
    filmTable02.dataSource=self;
    filmTable02.delegate=self;
    filmTable02.separatorInset=UIEdgeInsetsMake(0, 82, 0, 0);
    [filmScroller addSubview:filmTable02];
    
    //下拉刷新
    filmTableRefleshHeader02 = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            willFilmPageInt=1;
            willFilmArr=[NSMutableArray array];
            [self filmDataRequestWithType:@"2"];
        });
    }];
    
    [filmTableRefleshHeader02 setMj_h:64];//设置高度
    [filmTableRefleshHeader02 setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [filmTableRefleshHeader02.stateLabel setMj_y:13];
    filmTableRefleshHeader02.stateLabel.font = kFontSize13;
    filmTableRefleshHeader02.lastUpdatedTimeLabel.font = kFontSize13;
    filmTable02.mj_header = filmTableRefleshHeader02;
    
    //加载更多
    willFilmFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self willFilmRefreshForPullUp];
    }];
    willFilmFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    willFilmFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [willFilmFooter setTitle:@"" forState:MJRefreshStateIdle];
    [willFilmFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [willFilmFooter setTitle:@"" forState:MJRefreshStateNoMoreData];
    willFilmFooter.triggerAutomaticallyRefreshPercent=0.5;
    filmTable02.mj_footer = willFilmFooter;
    
    //
    
    
    UIView *head02=[[UIView alloc] initWithFrame:CGRectMake(0, 0, filmTable01.width, 38+actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2+24)];
    head02.backgroundColor=[UIColor whiteColor];
    //最受关注影片
    //filmTable02.tableHeaderView=head02;
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 200, 38)];
    headLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    headLabel.text=@"最受关注的影片";
    headLabel.font=[UIFont systemFontOfSize:16];
    [head02 addSubview:headLabel];
    
    [AppUtils drawZhiXian:head02 withColor:[UIColor colorWithHexString:@"949494"] height:0.2f firstPoint:CGPointMake(8, 37) endPoint:CGPointMake(SCREENSIZE.width-8, 37)];
    
    //最受关注电影
    sgzFilmTabel=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 38, actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2+24, SCREENSIZE.width) style:UITableViewStyleGrouped];
    sgzFilmTabel.separatorStyle=UITableViewCellSeparatorStyleNone;
    sgzFilmTabel.backgroundColor=[UIColor whiteColor];
    sgzFilmTabel.center=CGPointMake(SCREENSIZE.width/2, (actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2+24)/2+38);
    [sgzFilmTabel setContentOffset:CGPointMake(0,SCREENSIZE.height+164) animated:NO];
    sgzFilmTabel.showsVerticalScrollIndicator=YES;
    sgzFilmTabel.dataSource=self;
    sgzFilmTabel.delegate=self;
    [head02 addSubview:sgzFilmTabel];
    CGAffineTransform transform = CGAffineTransformMakeRotation(90 * M_PI_4);
    [sgzFilmTabel setTransform:transform];
}

#pragma mark - 加载更多
- (void)hadingFilmRefreshForPullUp{
    if (hadingFilmPageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self filmDataRequestWithType:@"1"];
        });
    }
}

- (void)willFilmRefreshForPullUp{
    if (willFilmPageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self filmDataRequestWithType:@"2"];
        });
    }
}

- (void)yyRefreshForPullUp{
    if (yyPageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self yingyuanDataRequest];
        });
    }
}

-(void)setUPYingYuan{
    yyTable=[[UITableView alloc]initWithFrame:CGRectMake(SCREENSIZE.width, 0, SCREENSIZE.width, myScrolview.height) style:UITableViewStylePlain];
    yyTable.dataSource=self;
    yyTable.delegate=self;
    [yyTable registerNib:[UINib nibWithNibName:@"YingYuanCell" bundle:nil] forCellReuseIdentifier:@"YingYuanCell"];
    yyTable.separatorInset=UIEdgeInsetsMake(0, 10, 0, 0);
    [myScrolview addSubview:yyTable];
    
    //下拉刷新
    cinemaRefleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            yyPageInt=1;
            yyArr=[NSMutableArray array];
            [self yingyuanDataRequest];
        });
    }];
    
    [cinemaRefleshHeader setMj_h:64];//设置高度
    [cinemaRefleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [cinemaRefleshHeader.stateLabel setMj_y:13];
    cinemaRefleshHeader.stateLabel.font = kFontSize13;
    cinemaRefleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    yyTable.mj_header = cinemaRefleshHeader;
    
    //加载更多
    yyFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self yyRefreshForPullUp];
    }];
    yyFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    yyFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [yyFooter setTitle:@"" forState:MJRefreshStateIdle];
    [yyFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [yyFooter setTitle:@"" forState:MJRefreshStateNoMoreData];
    yyFooter.triggerAutomaticallyRefreshPercent=0.5;
    yyTable.mj_footer = yyFooter;
}

#pragma mark - 设置滚动图片view (SDCycleScrollView)

-(SDCycleScrollView *)setUpImageViewDisPlayView{
    SDCycleScrollView *cycleScroll = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width*187/500)];
    cycleScroll.autoScrollTimeInterval=6;//设置滚动间隔
    cycleScroll.infiniteLoop = YES;
//    cycleScroll.isLocalImage=YES;
    cycleScroll.delegate=self;
    cycleScroll.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    [cycleScroll setImageURLStringsGroup:advImageArr];
    cycleScroll.backgroundColor=[UIColor lightGrayColor];
    //-----------------
    return cycleScroll;
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView==sgzFilmTabel){
        return 8;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView==filmTable01) {
        return hadingFilmArr.count;
    }else if (tableView==filmTable02){
        return willFilmArr.count;
    }else if (tableView==sgzFilmTabel){
        return 1;
    }
    return yyArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==filmTable01||tableView==filmTable02) {
        return 107;
    }else if (tableView==sgzFilmTabel){
        return actor_imageWidth;
    }
    return 96;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView==sgzFilmTabel){
        return 8;
    }
    return 0.0000001f;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if(tableView==filmTable02){
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 20)];
//        headerView.backgroundColor=[UIColor colorWithHexString:@"eeeeee"];
//        return headerView;
//    }else{
//        return nil;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (tableView==sgzFilmTabel) {
        if (section==7) {
            return 8;
        }else{
            return 0.0000001f;
        }
    }
    return 0.0000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==filmTable01) {
        FilmCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmCell"];
        if (!cell) {
            cell=[[FilmCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if(indexPath.row<hadingFilmArr.count){
            FilmModel *model=hadingFilmArr[indexPath.row];
            [cell reflushDataForModel:model isSelectWillPlay:isSelectWillPlay];
            cell.delegate=self;
        }
        return  cell;
    }else if(tableView==filmTable02){
        FilmCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmCell"];
        if (!cell) {
            cell=[[FilmCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        if(indexPath.row<willFilmArr.count){
            FilmModel *model=willFilmArr[indexPath.row];
            [cell reflushDataForModel:model isSelectWillPlay:isSelectWillPlay];
            cell.delegate=self;
        }
        
        return  cell;
    }else if(tableView==sgzFilmTabel){
        SgzFilmCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SgzFilmCell"];
        if (!cell) {
            cell=[[SgzFilmCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SgzFilmCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return  cell;
    }else{
        YingYuanCell *cell=[tableView dequeueReusableCellWithIdentifier:@"YingYuanCell"];
        if (!cell) {
            cell=[[YingYuanCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"YingYuanCell"];
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if(indexPath.row<yyArr.count){
            YingYuanModel *model=yyArr[indexPath.row];
            [cell reflushDataForModel:model];
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==yyTable) {
        YingYuanModel *model=yyArr[indexPath.row];
        YingYuanDetailController *ctr=[YingYuanDetailController new];
        ctr.type=0;
        ctr.cinemaId=model.yyId;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if(tableView==filmTable01){
        FilmModel *model=hadingFilmArr[indexPath.row];
        NewFilmIntroController *ctr=[NewFilmIntroController new];
        ctr.model=model;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if(tableView==filmTable02){
        FilmModel *model=willFilmArr[indexPath.row];
        NewFilmIntroController *ctr=[NewFilmIntroController new];
        ctr.model=model;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (tableView==sgzFilmTabel){
        [self.navigationController pushViewController:[NewFilmIntroController new] animated:YES];
    }
}

#pragma mark -------scrollview delegate-------

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView==filmScroller) {
        CGPoint p=scrollView.contentOffset;
        int page=p.x/SCREENSIZE.width;
        if (page==0) {
            
            filmTableIndex=1;
            
            [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
            hadFilmBtn.enabled=NO;
            
            [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
            willFilmBtn.enabled=YES;
        }else{
            
            filmTableIndex=2;
            
            [hadFilmBtn setTitleColor:[UIColor colorWithHexString:@"787878"] forState:UIControlStateNormal];
            hadFilmBtn.enabled=YES;
            
            [willFilmBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
            willFilmBtn.enabled=NO;
            
            [self willFilmRequest];     //即将上映
        }
        //设置蓝色横条位置
        [UIView animateWithDuration:0.15 animations:^{
            CGFloat btnWidth=SCREENSIZE.width/4;
            CGRect frame=btLine.frame;
            if (page==0) {
                frame.origin.x=(SCREENSIZE.width-2*btnWidth-16)/2;
                [filmScroller setContentOffset:CGPointMake(0*SCREENSIZE.width, 0) animated:NO];
            }else{
                frame.origin.x=(SCREENSIZE.width-2*btnWidth-16)/2+btnWidth+16;
                [filmScroller setContentOffset:CGPointMake(1*SCREENSIZE.width, 0) animated:NO];
            }
            btLine.frame=frame;
        } completion:^(BOOL finished) {
            
        }];
        [filmScroller setContentOffset:CGPointMake(page*SCREENSIZE.width, 0) animated:YES];
    }
}

#pragma mark - FilmCell Delegate
-(void)clickGoPickSeat:(FilmModel *)model{
    FilmForYYListController *ctr=[FilmForYYListController new];
    ctr.model=model;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - request
//电影
-(void)filmDataRequestWithType:(NSString *)type{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        if ([type isEqualToString:@"1"]) {
            if (hadingFilmFirst) {
                [AppUtils showProgressInView:self.view];
            }
        }else if([type isEqualToString:@"2"]){
            if (willFilmFirst) {
                [AppUtils showProgressInView:self.view];
            }
        }
        //
        time_t now;
        time(&now);
        NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
        NSString *nonce_str	= [YXWSign randomNumber];
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        [dict setObject:@"1" forKey:@"is_iso"];
        [dict setObject:@"m_movie" forKey:@"mod"];
        [dict setObject:@"movie_list" forKey:@"code"];
        [dict setObject:ccId forKey:@"city_id"];
        [dict setObject:type forKey:@"isShow"];
        [dict setObject:[type isEqualToString:@"1"]?[NSString stringWithFormat:@"%ld",(long)hadingFilmPageInt]:[NSString stringWithFormat:@"%ld",willFilmPageInt] forKey:@"page"];
        [dict setObject:time_stamp forKey:@"stamp"];
        [dict setObject:nonce_str forKey:@"noncestr"];
        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
        NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
        [dict setObject:sign forKey:@"sign"];
        //
//        NSDictionary *dict=@{
//                             @"is_iso":@"1",
//                             @"mod":@"m_movie",
//                             @"code":@"movie_list",
//                             @"city_id":ccId,
//                             @"isShow":type,
//                             @"page":[type isEqualToString:@"1"]?[NSString stringWithFormat:@"%ld",(long)hadingFilmPageInt]:[NSString stringWithFormat:@"%ld",willFilmPageInt]
//                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [AppUtils dismissHUDInView:self.view];
            [filmTableRefleshHeader01 endRefreshing];
            [filmTableRefleshHeader02 endRefreshing];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                if ([type isEqualToString:@"1"]) {
                    id arrObj=responseObject[@"retData"][@"movie_list"];
                    if (arrObj && [arrObj isKindOfClass:[NSArray class]]) {
                        
                        advImageArr=responseObject[@"retData"][@"adv_list"];
                        
                        filmTable01.tableHeaderView=[self setUpImageViewDisPlayView];
                        NSArray *movieArr=responseObject[@"retData"][@"movie_list"];
                        for (NSDictionary *dic in movieArr) {
                            FilmModel *model=[FilmModel new];
                            [model jsonDataForDictionary:dic];
                            [hadingFilmArr addObject:model];
                        }
                    }
                    
                    //总页数
                    NSString *totalPage=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"totalPage"]];
                    if (hadingFilmPageInt<totalPage.integerValue) {
                        hadingFilmPageInt++;
                        [hadingFilmFooter endRefreshing];
                    }else{
                        [hadingFilmFooter endRefreshingWithNoMoreData];
                    }
                    
                    [filmTable01 reloadData];
                }else{
                    id arrObj=responseObject[@"retData"][@"movie_list"];
                    if (arrObj && [arrObj isKindOfClass:[NSArray class]]) {
                        NSArray *movieArr=responseObject[@"retData"][@"movie_list"];
                        for (NSDictionary *dic in movieArr) {
                            FilmModel *model=[FilmModel new];
                            [model jsonDataForDictionary:dic];
                            [willFilmArr addObject:model];
                        }
                    }
                    
                    NSString *totalPage=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"totalPage"]];
                    if (willFilmPageInt<totalPage.integerValue) {
                        willFilmPageInt++;
                        [willFilmFooter endRefreshing];
                    }else{
                        [willFilmFooter endRefreshingWithNoMoreData];
                    }
                    
                    [filmTable02 reloadData];
                }
            }
            DSLog(@"%@",responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [AppUtils dismissHUDInView:self.view];
            [filmTableRefleshHeader01 endRefreshing];
            [filmTableRefleshHeader02 endRefreshing];
            [hadingFilmFooter endRefreshing];
            [willFilmFooter endRefreshing];
            DSLog(@"%@",error);
        }];
    }
}

-(void)yingyuanDataRequest{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        if (yyFirst) {
            [AppUtils showProgressInView:self.view];
        }
        //
        time_t now;
        time(&now);
        NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
        NSString *nonce_str	= [YXWSign randomNumber];
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        [dict setObject:@"1" forKey:@"is_iso"];
        [dict setObject:@"m_movie" forKey:@"mod"];
        [dict setObject:@"cinema_list" forKey:@"code"];
        [dict setObject:ccId forKey:@"city_id"];
        [dict setObject:@"" forKey:@"keyword"];
        [dict setObject:[regionStr isEqualToString:@"全部"]?@"":regionStr forKey:@"region"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)yyPageInt] forKey:@"page"];
        [dict setObject:time_stamp forKey:@"stamp"];
        [dict setObject:nonce_str forKey:@"noncestr"];
        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
        NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
        [dict setObject:sign forKey:@"sign"];
        //
//        NSDictionary *dict=@{
//                             @"is_iso":@"1",
//                             @"mod":@"m_movie",
//                             @"code":@"cinema_list",
//                             @"city_id":ccId,
//                             @"keyword":@"",
//                             @"region":[regionStr isEqualToString:@"全部"]?@"":regionStr,
//                             @"page":[NSString stringWithFormat:@"%ld",(long)yyPageInt]
//                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [AppUtils dismissHUDInView:self.view];
            [cinemaRefleshHeader endRefreshing];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSArray *arr=responseObject[@"retData"][@"cinema_list"];
                for (NSDictionary *dic in arr) {
                    YingYuanModel *model=[YingYuanModel new];
                    [model jsonDataForDictionary:dic];
                    
                    if ([model.fangyingSum isEqualToString:@"0"]) {
                        NSLog(@"----------");
                    }else{
                        [yyArr addObject:model];
                    }
                }
                
                NSString *totalPage=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"totalPage"]];
                if (yyPageInt<totalPage.integerValue) {
                    yyPageInt++;
                    [yyFooter endRefreshing];
                }else{
                    [yyFooter endRefreshingWithNoMoreData];
                }
                
                [yyTable reloadData];
            }
            DSLog(@"%@",responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [AppUtils dismissHUDInView:self.view];
            [cinemaRefleshHeader endRefreshing];
            [yyFooter endRefreshing];
            DSLog(@"%@",error);
        }];
    }
}

-(void)areaRequest{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        //
        time_t now;
        time(&now);
        NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
        NSString *nonce_str	= [YXWSign randomNumber];
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        [dict setObject:@"1" forKey:@"is_iso"];
        [dict setObject:@"m_movie" forKey:@"mod"];
        [dict setObject:@"get_area_list" forKey:@"code"];
        [dict setObject:ccId forKey:@"city_id"];
        [dict setObject:time_stamp forKey:@"stamp"];
        [dict setObject:nonce_str forKey:@"noncestr"];
        [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
        NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
        [dict setObject:sign forKey:@"sign"];
        //
//        NSDictionary *dict=@{
//                             @"is_iso":@"1",
//                             @"mod":@"m_movie",
//                             @"code":@"get_area_list",
//                             @"city_id":ccId
//                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [AppUtils dismissHUDInView:self.view];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                filmAreaView=[[FilmAreaView alloc] initInView:myScrolview andViewType:0];
                id arrObj=responseObject[@"retData"];
                NSMutableArray *daArr=[NSMutableArray array];
                if ([arrObj isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *dic in arrObj) {
                        NSString *str=dic[@"region"];
                        [daArr addObject:str];
                    }
                    [daArr insertObject:@"全部" atIndex:0];
                }
                filmAreaView.dataArr=daArr;
            }
            DSLog(@"areaRequest=\n%@",responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [AppUtils dismissHUDInView:self.view];
            DSLog(@"%@",error);
        }];
    }
}

//只会执行一次

-(void)hadingFilmRequest{
    if (hadingFilmFirst) {
        [self filmDataRequestWithType:@"1"];
        hadingFilmFirst = NO;
    }
}

-(void)willFilmRequest{
    if (willFilmFirst) {
        [self filmDataRequestWithType:@"2"];
        willFilmFirst = NO;
    }
}

-(void)yyFirstRequest{
    if (yyFirst) {
        [self yingyuanDataRequest];
        yyFirst = NO;
    }
}

-(void)areaFirstRequest{
    if (areaFirst) {
        [self areaRequest];
        areaFirst = NO;
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
