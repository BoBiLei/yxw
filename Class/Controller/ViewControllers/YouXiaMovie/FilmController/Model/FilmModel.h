//
//  FilmModel.h
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmModel : NSObject

@property (nonatomic, copy) NSString *fId;
@property (nonatomic, copy) NSString *imgv;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *dimensional;  //电影维度
@property (nonatomic, copy) NSString *grade;        //评分
@property (nonatomic, copy) NSString *detail;       //简短说明
@property (nonatomic, copy) NSString *actor;        //演员
@property (nonatomic, copy) NSString *area;         //地区
@property (nonatomic, copy) NSString *type;         //类型
@property (nonatomic, copy) NSString *releaseDate;  //首映时间
@property (nonatomic, copy) NSString *willPlayTime; //即将上映时间
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *isShow;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
