//
//  YingYuanModel.h
//  youxia
//
//  Created by mac on 2016/11/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YingYuanModel : NSObject

@property (nonatomic, copy) NSString *yyId;         //电影院ID
@property (nonatomic, copy) NSString *yyName;       //电影院名称
@property (nonatomic, copy) NSString *yyAddress;    //电影院地址
@property (nonatomic, copy) NSString *fangyingSum;  //放映多少部
@property (nonatomic, copy) NSString *changshu;     //场数

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
