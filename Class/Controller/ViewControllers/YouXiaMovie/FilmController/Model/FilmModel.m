//
//  FilmModel.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmModel.h"

@implementation FilmModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.fId=[NSString stringWithFormat:@"%@",dic[@"movie_id"]];
    self.imgv=[NSString stringWithFormat:@"%@",dic[@"picAddr"]];
    self.title=[NSString stringWithFormat:@"%@",dic[@"movie_name"]];
    self.dimensional=[NSString stringWithFormat:@"%@",dic[@"dimensional"]];
    self.grade=[NSString stringWithFormat:@"%@",dic[@"grade"]];
    self.detail=[NSString stringWithFormat:@"%@",dic[@"shortInfo"]];
    self.actor=[NSString stringWithFormat:@"%@",dic[@"actors"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.isShow=[NSString stringWithFormat:@"%@",dic[@"isShow"]];
    self.area=[NSString stringWithFormat:@"%@",dic[@"area"]];
    self.willPlayTime=[NSString stringWithFormat:@"%@",dic[@"releaseDate"]];
    self.type=[NSString stringWithFormat:@"%@",dic[@"type"]];
    self.releaseDate=[NSString stringWithFormat:@"%@",dic[@"releaseDate"]];
    /*@property (nonatomic, copy) NSString *area;         //地区
     @property (nonatomic, copy) NSString *type;         //类型
     @property (nonatomic, copy) NSString *releaseDate;  //首映时间*/
}

@end
