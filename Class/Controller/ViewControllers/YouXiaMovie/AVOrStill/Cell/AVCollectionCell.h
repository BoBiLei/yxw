//
//  AVCollectionCell.h
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@end
