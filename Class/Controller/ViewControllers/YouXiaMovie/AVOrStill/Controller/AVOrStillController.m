//
//  AVOrStillController.m
//  youxia
//
//  Created by mac on 2016/11/24.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "AVOrStillController.h"
#import "AVCollectionCell.h"

@interface AVOrStillController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation AVOrStillController{
    
    NSMutableArray *photoArr;
    UICollectionView *photoCollect;
    
    UIScrollView *myScrolview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavigateView];
    
    [self setUpMainScrollview];
    
    [self photoDataRequest];
}

-(void)addNavigateView{
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    [self.view addSubview:view];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(16, 20, 64, 44);
    [backBtn setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(clickBackBtn) forControlEvents:UIControlEventTouchUpInside];
    
#pragma mark UISegmentedControl
    UISegmentedControl *mySegment=[[UISegmentedControl alloc] initWithItems:@[@"剧照",@"预告片"]];
    mySegment.layer.cornerRadius = 4;
    mySegment.layer.masksToBounds = YES;
    //修改字体的默认颜色与选中颜色
    [mySegment setTitleTextAttributes:@{
                                        NSForegroundColorAttributeName:
                                            [UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:15]}
                             forState:UIControlStateSelected];
    [mySegment setTitleTextAttributes:@{NSForegroundColorAttributeName:
                                            [UIColor colorWithHexString:@"55b2f0"],
                                        NSFontAttributeName:[UIFont systemFontOfSize:15]}
                             forState:UIControlStateNormal];
    mySegment.frame=CGRectMake(0, 0, 148, 30);
    mySegment.center=CGPointMake(SCREENSIZE.width/2, 42);
    mySegment.selectedSegmentIndex=0;
    mySegment.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    mySegment.tintColor=[UIColor colorWithHexString:@"55b2f0"];
    [mySegment addTarget:self action:@selector(switchingView:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySegment];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"dcdcdc"] height:0.9f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)clickBackBtn{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)switchingView:(id)sender{
    UISegmentedControl *segment=sender;
    switch (segment.selectedSegmentIndex) {
        case 0:
            [myScrolview setContentOffset:CGPointMake(0*SCREENSIZE.width, 0) animated:NO];
            break;
        default:
            [myScrolview setContentOffset:CGPointMake(1*SCREENSIZE.width, 0) animated:NO];
            break;
    }
}

#pragma mark - SetUpScrollview
-(void)setUpMainScrollview{
    myScrolview=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    myScrolview.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    myScrolview.bounces=NO;
    myScrolview.showsHorizontalScrollIndicator=NO;
    myScrolview.showsVerticalScrollIndicator=NO;
    myScrolview.scrollEnabled=NO;
    myScrolview.pagingEnabled=NO;
    myScrolview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:myScrolview];
    
    //
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    photoCollect=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, myScrolview.height) collectionViewLayout:myLayout];
    [photoCollect registerNib:[UINib nibWithNibName:@"AVCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"AVCollectionCell"];
    photoCollect.backgroundColor=[UIColor clearColor];
    photoCollect.dataSource=self;
    photoCollect.delegate=self;
    photoCollect.bounces=YES;
    [myScrolview addSubview:photoCollect];
    
    [self setUpWebView];
}

-(void)setUpWebView{
    UIWebView *_webView = [[UIWebView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 0, myScrolview.width, myScrolview.height)];
    _webView.backgroundColor=[UIColor whiteColor];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    [myScrolview addSubview:_webView];
    NSString *urlStr=[NSString stringWithFormat:@"http://www.youxia.com/mgo/index.php?mod=movie_phone&code=movie_vedio_app&movie_id=%@",_model.fId];
    NSURL *url = [NSURL URLWithString:urlStr];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return photoArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AVCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"AVCollectionCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[AVCollectionCell alloc]init];
    }
    NSString *str=photoArr[indexPath.row];
    cell.imgv.clipsToBounds  = YES;
    cell.imgv.contentMode=UIViewContentModeScaleAspectFill;
    [cell.imgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:Image_Default]];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth=(SCREENSIZE.width-40)/3;
    
    return CGSizeMake(cellWidth, cellWidth-30);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
//    return 12;
    return 10;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 8, 10, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *imgArr=[NSMutableArray array];
    for (NSString *img in photoArr) {
        NSURL *url=[NSURL URLWithString:img];
        [imgArr addObject:url];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:imgArr];
    browser.displayToolbar=YES;
    browser.displayCounterLabel=YES;
    browser.displayActionButton = NO;
    [browser setInitialPageIndex:indexPath.row];
    [self presentViewController:browser animated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - request

-(void)photoDataRequest{
    [AppUtils showProgressInView:self.view];
    photoArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"movie_photos",
                         @"movie_id":_model.fId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id photoObj=responseObject[@"retData"];
            if ([photoObj isKindOfClass:[NSArray class]]) {
                for (NSString *str in photoObj) {
                    if (![str isEqualToString:@""]||str!=nil) {
                        [photoArr addObject:str];
                    }
                }
            }
            [photoCollect reloadData];
        }
        DSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)avDataRequest{
    photoArr=[NSMutableArray array];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"movie_photos",
                         @"movie_id":_model.fId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id photoObj=responseObject[@"retData"];
            if ([photoObj isKindOfClass:[NSArray class]]) {
                for (NSString *str in photoObj) {
                    if (![str isEqualToString:@""]||str!=nil) {
                        [photoArr addObject:str];
                    }
                }
            }
            [photoCollect reloadData];
        }
        DSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
