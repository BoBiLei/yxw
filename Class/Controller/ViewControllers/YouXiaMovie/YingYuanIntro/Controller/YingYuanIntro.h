//
//  YingYuanIntro.h
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CinemaInfoModel.h"

@interface YingYuanIntro : UIViewController

@property (nonatomic, strong) CinemaInfoModel *model;

@end
