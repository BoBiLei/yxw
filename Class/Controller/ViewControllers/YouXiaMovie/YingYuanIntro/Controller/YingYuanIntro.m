//
//  YingYuanIntro.m
//  youxia
//
//  Created by mac on 2016/11/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YingYuanIntro.h"

@interface YingYuanIntro ()

@end

@implementation YingYuanIntro

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *bgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    bgView.userInteractionEnabled=YES;
    bgView.image=[UIImage imageNamed:@"yy_intro_bg"];
    [self.view addSubview:bgView];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(0, 20, 44, 44);
    [backBtn setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    [bgView addSubview:backBtn];
    [backBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    
#pragma mark titleLabel
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(backBtn.origin.x+backBtn.width+8, 20, SCREENSIZE.width-(backBtn.origin.x*2+backBtn.width*2+8), 44)];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=_model.cinemaName;
    [bgView addSubview:titleLabel];
    
    [AppUtils drawZhiXian:bgView withColor:[UIColor whiteColor] height:0.5f firstPoint:CGPointMake(8, 66) endPoint:CGPointMake(SCREENSIZE.width-8, 66)];
    
#pragma mark 地址
    UIButton *dwBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    dwBtn.frame=CGRectMake(8, 80, 18, 21);
    [dwBtn setImage:[UIImage imageNamed:@"yy_intro_dingwei"] forState:UIControlStateNormal];
    [bgView addSubview:dwBtn];
    
    NSString *adrStr=_model.address;
    CGSize size=[AppUtils getStringSize:adrStr withFont:16];
    CGFloat ardLabelHeight=0;
    if (size.width>SCREENSIZE.width-(dwBtn.origin.x*2+dwBtn.width*2)) {
        ardLabelHeight=41;
    }else{
        ardLabelHeight=21;
    }
    
    UILabel *address=[[UILabel alloc] initWithFrame:CGRectMake(dwBtn.origin.x+dwBtn.width+4, dwBtn.origin.y, SCREENSIZE.width-(dwBtn.origin.x*2+dwBtn.width*2), ardLabelHeight)];
    address.textColor=[UIColor whiteColor];
    address.text=adrStr;
    address.font=[UIFont systemFontOfSize:16];
    address.numberOfLines=0;
    [bgView addSubview:address];
    
    [AppUtils drawZhiXian:bgView withColor:[UIColor whiteColor] height:0.5f firstPoint:CGPointMake(8, address.origin.y+address.height+12) endPoint:CGPointMake(SCREENSIZE.width-8, address.origin.y+address.height+12)];
    
#pragma mark 电话
    UIButton *phoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    phoneBtn.frame=CGRectMake(8, address.origin.y+address.height+24, 18, 21);
    [phoneBtn setImage:[UIImage imageNamed:@"yy_intro_phone"] forState:UIControlStateNormal];
    [bgView addSubview:phoneBtn];
    
    UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(phoneBtn.origin.x+phoneBtn.width+4, phoneBtn.origin.y, SCREENSIZE.width-(phoneBtn.origin.x*4+phoneBtn.width*2), 21)];
    phoneLabel.textColor=[UIColor whiteColor];
    phoneLabel.text=_model.tel;
    phoneLabel.font=[UIFont systemFontOfSize:16];
    [bgView addSubview:phoneLabel];
    
    [AppUtils drawZhiXian:bgView withColor:[UIColor whiteColor] height:0.5f firstPoint:CGPointMake(8, phoneLabel.origin.y+phoneLabel.height+12) endPoint:CGPointMake(SCREENSIZE.width-8, phoneLabel.origin.y+phoneLabel.height+12)];
    
 #pragma mark 说明
    UILabel *explant=[[UILabel alloc] initWithFrame:CGRectMake(8, phoneLabel.origin.y+phoneLabel.height+28, SCREENSIZE.width-16, 120)];
    explant.textColor=[UIColor whiteColor];
    explant.font=[UIFont systemFontOfSize:15];
    explant.numberOfLines=0;
    [bgView addSubview:explant];
    NSString *labelTtt=@"影院从7月1日起，不再提供免费的3D眼镜。您可以自备3D眼镜或者到影院现场售票处自行购买。（IMAX厅除外）\n1米3以下儿童免费观影，需大人抱在手中，一个大人仅限抱一个儿童，观看3D电影需自备3D眼镜。";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    explant.attributedText = attributedString;
    
    UIButton *closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame=CGRectMake(0, 0, 54, 54);
    closeBtn.center=CGPointMake(SCREENSIZE.width/2, explant.origin.y+explant.height+64);
    [closeBtn setImage:[UIImage imageNamed:@"yy_intro_close"] forState:UIControlStateNormal];
    [bgView addSubview:closeBtn];
    [closeBtn addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
