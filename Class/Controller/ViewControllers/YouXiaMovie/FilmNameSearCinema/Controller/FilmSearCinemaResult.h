//
//  FilmSearCinemaResult.h
//  youxia
//
//  Created by mac on 2016/12/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModel.h"

@interface FilmSearCinemaResult : UIViewController

@property (nonatomic, copy) NSString *regionStr;  //区域

@property (nonatomic, strong) FilmModel *model;

@property (nonatomic, copy) NSString *cinemaKeyWord;

@end
