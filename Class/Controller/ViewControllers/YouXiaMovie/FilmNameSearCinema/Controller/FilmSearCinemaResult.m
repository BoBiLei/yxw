//
//  FilmSearCinemaResult.m
//  youxia
//
//  Created by mac on 2016/12/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmSearCinemaResult.h"
#import "FilmForYYListCell.h"
#import "CinemaModel.h"
#import "YingYuanDetailController.h"

@interface FilmSearCinemaResult ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation FilmSearCinemaResult{
    NSInteger page;
    MJRefreshAutoNormalFooter *footer;
    
    UITableView *myTable;
    NSMutableArray *sourceArr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpTableView];
    
    [self dataRequest];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    UIColor * color = [[UIColor colorWithHexString:@"#ffffff"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width;
    TTTAttributedLabel *title=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(sViewX, 20, SCREENSIZE.width-2*sViewX, 44)];
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.font=kFontSize16;
    title.numberOfLines=0;
    [self.view addSubview:title];
    NSString *stageStr=[NSString stringWithFormat:@"%@\n(%@ 影院)搜索结果",_model.title,self.cinemaKeyWord];
    [title setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"(%@ 影院)搜索结果",self.cinemaKeyWord] options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize11;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#ffffff"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init TableView
-(void)setUpTableView{
    
    page=1;  //默认第一页
    
    sourceArr=[NSMutableArray array];
    
    //
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"FilmForYYListCell" bundle:nil] forCellReuseIdentifier:@"FilmForYYListCell"];
    [self.view addSubview:myTable];
}

- (void)refreshForPullUp{
    if (page!=1) {
        [self dataRequest];
    }
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return sourceArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 96;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FilmForYYListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmForYYListCell"];
    if (!cell) {
        cell=[[FilmForYYListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmForYYListCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    if(indexPath.row<sourceArr.count){
        CinemaModel *model=sourceArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CinemaModel *cmodel=sourceArr[indexPath.row];
    YingYuanDetailController *ctr=[YingYuanDetailController new];
    ctr.filmModel=_model;
    ctr.cinemaId=cmodel.cinemaId;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 搜索请求
-(void)dataRequest{
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"cinema_foretells_list",
                         @"movie_id":_model.fId,
                         @"cinema_name":_cinemaKeyWord,
                         @"region":[_regionStr isEqualToString:@"全部"]?@"":_regionStr
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id paiqiObj=responseObject[@"retData"][@"movie_paiqi"];
            if ([paiqiObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in paiqiObj) {
                    CinemaModel *model=[CinemaModel new];
                    [model jsonDataForDictioanry:dic];
                    [sourceArr addObject:model];
                }
                if (sourceArr.count!=0) {
                    [myTable reloadData];
                }else{
                    
                    //
                    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"获取影院信息失败！"preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alertController addAction:yesAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
