//
//  SearchFilmNameResultCell.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchFilmNameResultCell.h"

@implementation SearchFilmNameResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(FilmModel *)model{
    [self.imgv sd_setImageWithURL:[NSURL URLWithString:model.imgv] placeholderImage:[UIImage imageNamed:Image_Default]];
    self.nameLabel.text=model.title;
    self.gradeLabel.text=[NSString stringWithFormat:@"%@分",model.grade];
    self.typeLabel.text=model.type;
    self.areaLabel.text=model.area;
    self.timeLabel.text=[NSString stringWithFormat:@"上映时间: %@",model.releaseDate];
}

@end
