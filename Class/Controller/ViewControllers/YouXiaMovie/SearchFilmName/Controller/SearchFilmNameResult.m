//
//  SearchFilmNameResult.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchFilmNameResult.h"
#import "SearchFilmNameResultCell.h"
#import "FilmModel.h"
#import "NewFilmIntroController.h"

@interface SearchFilmNameResult ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SearchFilmNameResult{
    NSInteger page;
    MJRefreshAutoNormalFooter *footer;
    
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self setUpTableView];
    
    [self dataRequest];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    UIColor * color = [[UIColor colorWithHexString:@"#ffffff"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width;
    TTTAttributedLabel *title=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(sViewX, 20, SCREENSIZE.width-2*sViewX, 44)];
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.font=kFontSize16;
    title.numberOfLines=0;
    [self.view addSubview:title];
    NSString *stageStr=[NSString stringWithFormat:@"%@\n搜索结果",self.keyWord];
    [title setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"搜索结果" options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize11;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    //
    UIColor * ccolor = [[UIColor colorWithHexString:@"#ffffff"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init TableView
-(void)setUpTableView{
    
    page=1;  //默认第一页
    
    dataArr=[NSMutableArray array];
    
    //
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"SearchFilmNameResultCell" bundle:nil] forCellReuseIdentifier:@"SearchFilmNameResultCell"];
    [self.view addSubview:myTable];
}

- (void)refreshForPullUp{
    if (page!=1) {
        [self dataRequest];
    }
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 107;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchFilmNameResultCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SearchFilmNameResultCell"];
    if (!cell) {
        cell=[[SearchFilmNameResultCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchFilmNameResultCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row<dataArr.count){
        FilmModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    FilmModel *tabModel=dataArr[indexPath.row];
    NewFilmIntroController *ctr=[NewFilmIntroController new];
    ctr.model=tabModel;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 搜索请求
-(void)dataRequest{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        NSDictionary *dict=@{
                             @"is_iso":@"1",
                             @"mod":@"m_movie",
                             @"code":@"search_movie",
                             @"city_id":ccId,
                             @"keyword":_keyWord,
                             @"page":@"1"
                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            DSLog(@"%@",responseObject);
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id arr=responseObject[@"retData"];
                if ([arr isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *dic in arr) {
                        FilmModel *model=[FilmModel new];
                        [model jsonDataForDictionary:dic];
                        [dataArr addObject:model];
                    }
                }
                
                [myTable reloadData];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [AppUtils dismissHUDInView:self.view];
            DSLog(@"%@",error);
        }];
    }
}

@end
