//
//  SearchFilmNameResult.h
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchFilmNameResult : UIViewController

@property (nonatomic, copy) NSString *keyWord;

@end
