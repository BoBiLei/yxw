//
//  SearchFilmNameController.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchFilmNameController.h"
#import "SearchFilmNameResult.h"

@interface SearchFilmNameController ()<UITextFieldDelegate>

@end

@implementation SearchFilmNameController{
    UITextField *searchTf;
}

-(void)viewWillAppear:(BOOL)animated{
    [IQKeyboardManager sharedManager].enableAutoToolbar=NO;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(instancetype)init{
    self=[super init];
    if (self) {
        [self setUpCustomSearBar];      //自定义NavigationBar
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    [self.view addSubview:view];
}

#pragma mark - setUpCustomSearBar
-(void)setUpCustomSearBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    CGFloat phoneWidth=44;
    
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar lt_setBackgroundColor:[UIColor colorWithHexString:@"#ffffff"]];
    [self.view addSubview:cusBar];
    
    //
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(12, 26, SCREENSIZE.width-24-phoneWidth, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=5;
    searchView.layer.borderWidth=0.6;
    searchView.layer.borderColor=[UIColor colorWithHexString:@"#a0a0a0"].CGColor;
    [self.view addSubview:searchView];
    
    //放大镜图片
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    //textField
    searchTf=[[UITextField alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    searchTf.font=kFontSize14;
    searchTf.placeholder=@"输入电影名称";
    [searchTf setValue:[UIColor colorWithHexString:@"#c3c3c3"] forKeyPath:@"_placeholderLabel.textColor"];
    searchTf.delegate=self;
    searchTf.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTf.textColor=[UIColor blackColor];
    searchTf.tintColor=[UIColor colorWithHexString:YxColor_Blue];
    searchTf.returnKeyType=UIReturnKeySearch;
    [searchTf becomeFirstResponder];
    [searchView addSubview:searchTf];
    
    //取消按钮
    UIButton *cencleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cencleBtn.frame=CGRectMake(cusBar.width-searchView.height-12, searchView.origin.y, searchView.height, searchView.height);
    cencleBtn.titleLabel.font=kFontSize15;
    [cencleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cencleBtn setTitleColor:[UIColor colorWithHexString:@"#a0a0a0"] forState:UIControlStateNormal];
    [cencleBtn addTarget:self action:@selector(cancelNav) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cencleBtn];
}

-(void)cancelNav{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length>0) {
        [AppUtils closeKeyboard];
        SearchFilmNameResult *resultCtr=[SearchFilmNameResult new];
        resultCtr.keyWord=textField.text;
        [self.navigationController pushViewController:resultCtr animated:YES];
    }else{
        [AppUtils showSuccessMessage:@"请输入电影名称" inView:self.view];
    }
    return YES;
}

@end
