//
//  TourSuccessBottomCell.h
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourSuccessModel.h"

@interface TourSuccessBottomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *movieNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cinemaLabel;
@property (weak, nonatomic) IBOutlet UILabel *playTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *qupiaoNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *tingNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *seatLabel;
@property (weak, nonatomic) IBOutlet UILabel *seatPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *payPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *btmCinemaLabel;
@property (weak, nonatomic) IBOutlet UILabel *cinemaAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *yxServicePhone;

@property (weak, nonatomic) IBOutlet UIView *piaoNum;

-(void)reflushDataForModel:(NSArray *)dataArr;

@end
