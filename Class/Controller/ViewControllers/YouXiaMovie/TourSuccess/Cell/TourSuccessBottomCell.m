//
//  TourSuccessBottomCell.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "TourSuccessBottomCell.h"

@implementation TourSuccessBottomCell{
    TourSuccessModel *recModel;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
#pragma mark 电影名称
        UILabel *filmName=[[UILabel alloc] initWithFrame:CGRectMake(14, 14, SCREENSIZE.width-28, 26)];
        filmName.textColor=[UIColor colorWithHexString:@"000000"];
        filmName.font=[UIFont boldSystemFontOfSize:16];
        [self addSubview:filmName];
        
#pragma mark 电影院名称
        UILabel *filmAddress=[[UILabel alloc] initWithFrame:CGRectMake(14, filmName.origin.y+filmName.height+3, SCREENSIZE.width-28, 19)];
        filmAddress.textColor=[UIColor colorWithHexString:@"949494"];
        filmAddress.font=[UIFont systemFontOfSize:13];
        [self addSubview:filmAddress];
        
        //播放时间
        UILabel *viewTime=[[UILabel alloc] initWithFrame:CGRectMake(14, filmAddress.origin.y+filmAddress.height+3, SCREENSIZE.width-28, 19)];
        viewTime.textColor=[UIColor colorWithHexString:@"949494"];
        viewTime.font=[UIFont systemFontOfSize:13];
        [self addSubview:viewTime];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, viewTime.origin.y+viewTime.height+10) endPoint:CGPointMake(SCREENSIZE.width-14, viewTime.origin.y+viewTime.height+10)];
        
#pragma mark 厅号
        UILabel *tingNum=[[UILabel alloc] initWithFrame:CGRectMake(14, viewTime.origin.y+viewTime.height+22, SCREENSIZE.width-28, 19)];
        tingNum.textColor=[UIColor colorWithHexString:@"949494"];
        tingNum.font=[UIFont systemFontOfSize:13];
        [self addSubview:tingNum];
        
        UILabel *paiNum=[[UILabel alloc] initWithFrame:CGRectMake(14, tingNum.origin.y+tingNum.height+4, SCREENSIZE.width-28, 19)];
        paiNum.textColor=[UIColor colorWithHexString:@"949494"];
        paiNum.font=[UIFont systemFontOfSize:13];
        NSString *seatStr=@"";
//        for (NSString *str in dataModel.position) {
//            NSString *itemStr=@"";
//            NSArray *picarry=[str componentsSeparatedByString:@":"];
//            for (int i=0; i<picarry.count; i++) {
//                NSString *str=picarry[i];
//                itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
//            }
//            seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
//        }
        paiNum.text=seatStr;
        [self addSubview:paiNum];
        
#pragma mark 票价
        UILabel *leftPrice=[[UILabel alloc] initWithFrame:CGRectMake(14, paiNum.origin.y+paiNum.height+4, SCREENSIZE.width/2, 19)];
        leftPrice.textColor=[UIColor colorWithHexString:@"949494"];
        leftPrice.font=[UIFont systemFontOfSize:13];
//        leftPrice.text=[NSString stringWithFormat:@"总价 ￥%@  优惠 -￥%@",dataModel.movie_amount,dataModel.credit_price];
        [self addSubview:leftPrice];
        
        UILabel *rightPrice=[[UILabel alloc] initWithFrame:CGRectMake(SCREENSIZE.width/2-14, leftPrice.origin.y, leftPrice.width, leftPrice.height)];
        rightPrice.textColor=[UIColor colorWithHexString:@"fb8023"];
        rightPrice.font=[UIFont systemFontOfSize:16];
//        rightPrice.text=[NSString stringWithFormat:@"实付 ￥%@",dataModel.paymoney];
        rightPrice.textAlignment=NSTextAlignmentRight;
        [self addSubview:rightPrice];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, leftPrice.origin.y+leftPrice.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, leftPrice.origin.y+leftPrice.height+14)];
        
        UILabel *qpAdrTip=[[UILabel alloc] initWithFrame:CGRectMake(14, leftPrice.origin.y+leftPrice.height+25, SCREENSIZE.width-28, 19)];
        qpAdrTip.textColor=[UIColor colorWithHexString:@"949494"];
        qpAdrTip.font=[UIFont systemFontOfSize:13];
        qpAdrTip.text=@"取票地址 : 影院售票窗口兑换";
        [self addSubview:qpAdrTip];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, qpAdrTip.origin.y+qpAdrTip.height+12) endPoint:CGPointMake(SCREENSIZE.width-14, qpAdrTip.origin.y+qpAdrTip.height+12)];
        
#pragma mark 订单号
        UILabel *orderNum=[[UILabel alloc] initWithFrame:CGRectMake(14, qpAdrTip.origin.y+qpAdrTip.height+25, SCREENSIZE.width-28, 19)];
        orderNum.textColor=[UIColor colorWithHexString:@"949494"];
        orderNum.font=[UIFont boldSystemFontOfSize:13];
//        orderNum.text=[NSString stringWithFormat:@"订单号 : %@",dataModel.orderid];
        [self addSubview:orderNum];
        
        UILabel *buyTime=[[UILabel alloc] initWithFrame:CGRectMake(14, orderNum.origin.y+orderNum.height+4, SCREENSIZE.width-28, 19)];
        buyTime.textColor=[UIColor colorWithHexString:@"949494"];
        buyTime.font=[UIFont systemFontOfSize:13];
//        buyTime.text=[NSString stringWithFormat:@"购票时间 : %@",dataModel.buytime];
        [self addSubview:buyTime];
        
        UILabel *phone=[[UILabel alloc] initWithFrame:CGRectMake(14, buyTime.origin.y+buyTime.height+4, SCREENSIZE.width-28, 19)];
        phone.textColor=[UIColor colorWithHexString:@"949494"];
        phone.font=[UIFont systemFontOfSize:13];
//        phone.text=[NSString stringWithFormat:@"手机号 : %@",dataModel.phone];
        [self addSubview:phone];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, phone.origin.y+phone.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, phone.origin.y+phone.height+14)];
        
        UILabel *yyName=[[UILabel alloc] initWithFrame:CGRectMake(14, phone.origin.y+phone.height+25, SCREENSIZE.width-86, 26)];
        yyName.textColor=[UIColor colorWithHexString:@"000000"];
        yyName.font=[UIFont systemFontOfSize:14];
//        yyName.text=dataModel.cinemaName;
        [self addSubview:yyName];
        
#pragma mark 影院地址
        UILabel *yyAddress=[[UILabel alloc] init];
        yyAddress.textColor=[UIColor colorWithHexString:@"949494"];
        yyAddress.font=[UIFont systemFontOfSize:13];
        yyAddress.numberOfLines=0;
//        CGSize yyadSize=[AppUtils getStringSize:dataModel.address withFont:13];
//        if (yyadSize.width>SCREENSIZE.width-94) {
//            yyAddress.frame=CGRectMake(14, yyName.origin.y+yyName.height+3, SCREENSIZE.width-94, 34);
//        }else{
//            yyAddress.frame=CGRectMake(14, yyName.origin.y+yyName.height+3, SCREENSIZE.width-94, 19);
//        }
//        NSString *labelTtt=dataModel.address;
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
//        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        
//        [paragraphStyle setLineSpacing:2];//调整行间距
//        
//        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
//        yyAddress.attributedText = attributedString;
        [self addSubview:yyAddress];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(SCREENSIZE.width-74, yyName.origin.y+4) endPoint:CGPointMake(SCREENSIZE.width-74, yyAddress.origin.y+yyAddress.height)];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(14, yyAddress.origin.y+yyAddress.height+14) endPoint:CGPointMake(SCREENSIZE.width-14, yyAddress.origin.y+yyAddress.height+14)];
        
#pragma mark callBtn
        UIButton *callBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        callBtn.frame=CGRectMake(SCREENSIZE.width-88, phone.origin.y+phone.height+14, 74, 74);
        [callBtn setImage:[UIImage imageNamed:@"yydetail_phone"] forState:UIControlStateNormal];
        [callBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 21, 0, 0)];
        [self addSubview:callBtn];
        [callBtn addTarget:self action:@selector(clickCall) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *kefuTip=[[UILabel alloc] initWithFrame:CGRectMake(14, yyAddress.origin.y+yyAddress.height+38, SCREENSIZE.width-28, 19)];
        kefuTip.textColor=[UIColor colorWithHexString:@"949494"];
        kefuTip.font=[UIFont systemFontOfSize:13];
        kefuTip.text=@"订单问题请联系客服";
        kefuTip.textAlignment=NSTextAlignmentCenter;
        [self addSubview:kefuTip];
        
        UILabel *kefuPhone=[[UILabel alloc] initWithFrame:CGRectMake(14, kefuTip.origin.y+kefuTip.height, SCREENSIZE.width-28, 26)];
        kefuPhone.textColor=[UIColor colorWithHexString:@"000000"];
        kefuPhone.font=[UIFont systemFontOfSize:13];
        //    kefuPhone.text=dataModel.tel;
        //SERVICEPHONE
        kefuPhone.text=@"400-1808-400";
        kefuPhone.textAlignment=NSTextAlignmentCenter;
        kefuPhone.userInteractionEnabled=YES;
        [self addSubview:kefuPhone];
        UITapGestureRecognizer *kefuTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPhone)];
        [kefuPhone addGestureRecognizer:kefuTap];
        
        [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, kefuPhone.origin.y+kefuPhone.height+20) endPoint:CGPointMake(SCREENSIZE.width, kefuPhone.origin.y+kefuPhone.height+20)];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.piaoNum.layer.cornerRadius=3;
    self.cinemaAddressLabel.adjustsFontSizeToFitWidth=YES;
    
    self.playTimeLabel.numberOfLines=0;
    
    self.yxServicePhone.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPhone)];
    [self.yxServicePhone addGestureRecognizer:tap];
}

-(void)tapPhone{
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://400-1808-400"];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

-(void)clickCall{
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",recModel.tel];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

-(void)reflushDataForModel:(NSArray *)dataArr{
    if (dataArr.count!=0) {
        TourSuccessModel *model=dataArr[0];
        recModel=model;
        
        self.movieNameLabel.text=model.movie_data;
        self.cinemaLabel.text=model.cinemaName;
        self.playTimeLabel.text=model.playtime;
        NSString *labelTtt=model.code;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:6];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        self.qupiaoNumLabel.attributedText = attributedString;
        
        self.tingNumLabel.text=model.hallName;
        
        NSString *seatStr=@"";
        for (NSString *str in model.position) {
            NSString *itemStr=@"";
            NSArray *picarry=[str componentsSeparatedByString:@":"];
            for (int i=0; i<picarry.count; i++) {
                NSString *str=picarry[i];
                itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
            }
            seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
        }
        self.seatLabel.text=seatStr;
        
        self.seatPriceLabel.text=[NSString stringWithFormat:@"票价 ￥%@ 优惠 -￥%@",model.movie_price,model.credit_price];
        
        self.payPriceLabel.text=[NSString stringWithFormat:@"实付 ￥%@",model.paymoney];
        
        self.orderIdLabel.text=[NSString stringWithFormat:@"订单号 %@",model.orderid];
        self.buyTimeLabel.text=[NSString stringWithFormat:@"购票时间 %@",model.paytime];
        self.phoneLabel.text=[NSString stringWithFormat:@"手机号 %@",model.phone];
        self.btmCinemaLabel.text=model.cinemaName;
        self.cinemaAddressLabel.text=model.address;
        
        if ([model.status isEqualToString:@"1"]) {
            self.piaoNum.translatesAutoresizingMaskIntoConstraints = NO;
            self.piaoNum.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
            NSArray* imgBig_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_piaoNum]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_piaoNum)];
            [NSLayoutConstraint activateConstraints:imgBig_h];

            NSArray* imgBig_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_playTimeLabel]-8-[_piaoNum(0.5)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_playTimeLabel,_piaoNum)];
            [NSLayoutConstraint activateConstraints:imgBig_w];
        }
    }
}



- (IBAction)clickCallBtn:(id)sender {
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",recModel.tel];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *phoneRequest=[NSURLRequest requestWithURL:url];
    [self addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

@end
