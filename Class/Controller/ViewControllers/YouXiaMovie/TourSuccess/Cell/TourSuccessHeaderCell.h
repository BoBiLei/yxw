//
//  TourSuccessHeaderCell.h
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourSuccessModel.h"

@interface TourSuccessHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *succBtn;

-(void)reflushDataForModel:(NSArray *)dataArr;

@end
