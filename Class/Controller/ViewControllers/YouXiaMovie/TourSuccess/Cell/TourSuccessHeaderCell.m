//
//  TourSuccessHeaderCell.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "TourSuccessHeaderCell.h"

@implementation TourSuccessHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

-(void)reflushDataForModel:(NSArray *)dataArr{
    if (dataArr.count!=0) {
        TourSuccessModel *model=dataArr[0];
        if ([model.status isEqualToString:@"1"]) {
            [self.succBtn setImage:[UIImage imageNamed:@"toursuccess_icon"] forState:UIControlStateNormal];
            [self.succBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
            [self.succBtn setTitle:@"等待出票" forState:UIControlStateNormal];
            [self.succBtn setTitleColor:[UIColor colorWithHexString:@"fb8023"] forState:UIControlStateNormal];
        }else{
            [self.succBtn setImage:[UIImage imageNamed:@"toursuccess_icon"] forState:UIControlStateNormal];
            [self.succBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
            [self.succBtn setTitle:@"购票成功" forState:UIControlStateNormal];
            [self.succBtn setTitleColor:[UIColor colorWithHexString:@"fb8023"] forState:UIControlStateNormal];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
