//
//  TourSuccessController.h
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourSuccessController : UIViewController

@property (nonatomic, copy) NSString *orderId;

@end
