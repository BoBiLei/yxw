//
//  TourSuccessController.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "TourSuccessController.h"
#import "TourSuccessHeaderCell.h"
#import "TourSuccessBottomCell.h"
#import "TourSuccessModel.h"
#import "YingYuanDetailController.h"
#import "FilmListController.h"
#import "MovieActivityController.h"

@interface TourSuccessController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation TourSuccessController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewDidAppear:(BOOL)animated{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavigateView];
    
    [self setUI];
    
    [self dataRequest];
}

-(void)addNavigateView{
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(16, 20, 64, 44);
    backBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [backBtn setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2, 44)];
    title.center=CGPointMake(SCREENSIZE.width/2, 42);
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.textAlignment=NSTextAlignmentCenter;
    title.text=@"购票成功";
    [self.view addSubview:title];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"dcdcdc"] height:0.9f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

#pragma mark - Nav 点击返回按钮
-(void)turnBack{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[YingYuanDetailController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }else if([controller isKindOfClass:[FilmListController class]]){
            [self.navigationController popToViewController:controller animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:FilmPay_Success_RefleshList_Noti object:nil];
        }else if([controller isKindOfClass:[MovieActivityController class]]){
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - init table
-(void)setUI{
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    [myTable registerNib:[UINib nibWithNibName:@"TourSuccessHeaderCell" bundle:nil] forCellReuseIdentifier:@"TourSuccessHeaderCell"];
    [myTable registerNib:[UINib nibWithNibName:@"TourSuccessBottomCell" bundle:nil] forCellReuseIdentifier:@"TourSuccessBottomCell"];
    myTable.hidden=YES;
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 93;
    }else{
        if (dataArr.count!=0) {
            TourSuccessModel *model=dataArr[0];
            if ([model.status isEqualToString:@"1"]) {
                return 450;
            }else{
                return 550;
            }
        }else{
            return 550;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==1) {
        return 32;
    }
    return 0.000000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        TourSuccessHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"TourSuccessHeaderCell"];
        if (!cell) {
            cell=[[TourSuccessHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TourSuccessHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (dataArr.count!=0) {
            [cell reflushDataForModel:dataArr];
        }
        return  cell;
    }else{
        TourSuccessBottomCell *cell=[tableView dequeueReusableCellWithIdentifier:@"TourSuccessBottomCell"];
        if (!cell) {
            cell=[[TourSuccessBottomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TourSuccessBottomCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (dataArr.count!=0) {
            [cell reflushDataForModel:dataArr];
        }
        return  cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Request
-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"queryOrder" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"queryOrder",
//                         @"orderid":_orderId,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id resObj=responseObject[@"retData"];
            if ([resObj isKindOfClass:[NSDictionary class]]) {
                myTable.hidden=NO;
                TourSuccessModel *dataModel=[TourSuccessModel new];
                [dataModel jsonDataForDictionary:resObj];
                if ([dataModel.status isEqualToString:@"1"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self dataRequest];
                    });
                }else{
                    [AppUtils dismissHUDInView:self.view];
                }
                [dataArr addObject:dataModel];
            }
            [myTable reloadData];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
