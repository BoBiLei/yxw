//
//  TourSuccessModel.m
//  youxia
//
//  Created by mac on 2016/12/10.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "TourSuccessModel.h"

@implementation TourSuccessModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.address=[NSString stringWithFormat:@"%@",dic[@"address"]];
    self.atm=[NSString stringWithFormat:@"%@",dic[@"atm"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.confirmId=[NSString stringWithFormat:@"%@",dic[@"confirmId"]];
    self.credit_price=[NSString stringWithFormat:@"%@",dic[@"credit_price"]];
    self.hallName=[NSString stringWithFormat:@"%@",dic[@"hallName"]];
    self.movie_amount=[NSString stringWithFormat:@"%@",dic[@"movie_amount"]];
    self.movie_data=[NSString stringWithFormat:@"%@",dic[@"movie_data"]];
    self.movie_num=[NSString stringWithFormat:@"%@",dic[@"movie_num"]];
    self.movie_price=[NSString stringWithFormat:@"%@",dic[@"movie_price"]];
    self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.paymoney=[NSString stringWithFormat:@"%@",dic[@"paymoney"]];
    self.paytime=[NSString stringWithFormat:@"%@",dic[@"paytime"]];
    self.phone=[NSString stringWithFormat:@"%@",dic[@"phone"]];
    self.playtime=[NSString stringWithFormat:@"%@",dic[@"playtime"]];
    self.position=dic[@"position"];
    self.region=[NSString stringWithFormat:@"%@",dic[@"region"]];
    self.tel=[NSString stringWithFormat:@"%@",dic[@"tel"]];
    
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    
    self.code=[NSString stringWithFormat:@"%@",dic[@"code_data"]];
}

@end
