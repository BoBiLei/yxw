//
//  FilmIntroModel.h
//  youxia
//
//  Created by mac on 2016/11/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmIntroModel : NSObject

@property (nonatomic, retain) NSArray *actors;          //演员
@property (nonatomic, copy) NSString *area;             //地区
@property (nonatomic, copy) NSString *company;
@property (nonatomic, copy) NSString *dimensional;      //电影维度
@property (nonatomic, retain) NSArray *director;        //导演
@property (nonatomic, copy) NSString *grade;            //评分
@property (nonatomic, copy) NSString *fid;              //ID
@property (nonatomic, copy) NSString *info;             //信息
@property (nonatomic, copy) NSString *isShow;           //
@property (nonatomic, copy) NSString *language;         //语言
@property (nonatomic, copy) NSString *length;
@property (nonatomic, copy) NSString *movie_id;
@property (nonatomic, copy) NSString *movie_name;       //电影名称
@property (nonatomic) id photos;                        //照片
@property (nonatomic, copy) NSString *picAddr;          //
@property (nonatomic, copy) NSString *releaseDate;      //上映时间
@property (nonatomic, copy) NSString *shortInfo;        //简短介绍
@property (nonatomic, copy) NSString *status;           //状态
@property (nonatomic, copy) NSString *type;             //类型
@property (nonatomic, copy) NSString *videoUrl;         //

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
