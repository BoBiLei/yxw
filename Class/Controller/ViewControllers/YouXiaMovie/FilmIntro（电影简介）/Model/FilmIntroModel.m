//
//  FilmIntroModel.m
//  youxia
//
//  Created by mac on 2016/11/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmIntroModel.h"

@implementation FilmIntroModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    id actObj=dic[@"actors"];
    if ([actObj isKindOfClass:[NSArray class]]) {
        self.actors=actObj;
    }else{
        self.actors=@[];
    }
    self.area=[NSString stringWithFormat:@"%@",dic[@"area"]];
    self.company=[NSString stringWithFormat:@"%@",dic[@"company"]];
    self.dimensional=[NSString stringWithFormat:@"%@",dic[@"dimensional"]];
    self.director=dic[@"director"];
    self.grade=[NSString stringWithFormat:@"%@",dic[@"grade"]];
    self.fid=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.info=[NSString stringWithFormat:@"%@",dic[@"info"]];
    self.isShow=[NSString stringWithFormat:@"%@",dic[@"isShow"]];
    self.language=[NSString stringWithFormat:@"%@",dic[@"language"]];
    self.length=[NSString stringWithFormat:@"%@",dic[@"length"]];
    self.movie_id=[NSString stringWithFormat:@"%@",dic[@"movie_id"]];
    self.movie_name=[NSString stringWithFormat:@"%@",dic[@"movie_name"]];
    self.photos=dic[@"photos"];
    self.picAddr=[NSString stringWithFormat:@"%@",dic[@"picAddr"]];
    self.releaseDate=[NSString stringWithFormat:@"%@",dic[@"releaseDate"]];
    self.shortInfo=[NSString stringWithFormat:@"%@",dic[@"shortInfo"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.type=[NSString stringWithFormat:@"%@",dic[@"type"]];
    self.videoUrl=[NSString stringWithFormat:@"%@",dic[@"videoUrl"]];
}

@end
