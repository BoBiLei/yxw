//
//  NewFilmIntroController.m
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewFilmIntroController.h"
#import "FilmIntroModel.h"
#import "IntroActorParam.h"
#import "NewFilmIntroHeaderCell.h"
#import "NewFilmIntroCommentCell.h"
#import "TopLeftLabel.h"
#import "NewFilmIntroActorCell.h"
#import "NewAVAndStillCell.h"
#import "AVOrStillController.h"
#import "FilmForYYListController.h"
#import "FilmIntroHeadReusView.h"
#import <UIImage+GIF.h>

@interface NewFilmIntroController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NewAVAndStillDelegate>

@end

@implementation NewFilmIntroController{
    NSMutableArray *actorArr;
    FilmIntroModel *dataModel;
    BOOL isShowMore;
    UICollectionView *myCollection;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"电影简介";
    [self.view addSubview:navBar];
    
    [self initCollectionView];
    
    [self dataRequest];
}

#pragma mark - Init CollectionView
-(void)initCollectionView{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    myCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-([_model.isShow isEqualToString:@"1"]?113:64)) collectionViewLayout:myLayout];
    [myCollection registerNib:[UINib nibWithNibName:@"NewFilmIntroHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NewFilmIntroHeaderCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"NewFilmIntroCommentCell" bundle:nil] forCellWithReuseIdentifier:@"NewFilmIntroCommentCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"NewFilmIntroActorCell" bundle:nil] forCellWithReuseIdentifier:@"NewFilmIntroActorCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"NewAVAndStillCell" bundle:nil] forCellWithReuseIdentifier:@"NewAVAndStillCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"FilmIntroHeadReusView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"fillterheader"];
    myCollection.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    myCollection.dataSource=self;
    myCollection.delegate=self;
    myCollection.hidden=YES;
    [self.view addSubview:myCollection];
    
    //buy btn
    if ([_model.isShow isEqualToString:@"1"]) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 49);
        btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        btn.titleLabel.font=[UIFont systemFontOfSize:16];
        [btn setTitle:@"立即购票" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
}

#pragma mark - 点击购买按钮
-(void)clickBuyBtn{
    FilmForYYListController *ctr=[FilmForYYListController new];
    ctr.model=_model;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return section==2?actorArr.count:1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        NewFilmIntroHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewFilmIntroHeaderCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewFilmIntroHeaderCell alloc]init];
        }
        [cell reflushDataForModel:dataModel];
        return cell;
    }else if(indexPath.section==1){
        NewFilmIntroCommentCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewFilmIntroCommentCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewFilmIntroCommentCell alloc]init];
        }
        
        CGSize size=[AppUtils getStringSize:dataModel.info withFont:15];
        CGFloat rowFloat=size.width/(SCREENSIZE.width-16);
        NSString *rowStr=[NSString stringWithFormat:@"%.2f",rowFloat];
        NSRange rang=[rowStr rangeOfString:@"."];
        NSInteger rowCount=0;   //行数
        if (rang.location!=NSNotFound) {
            rowStr=[rowStr substringToIndex:rang.location];
            rowCount=rowStr.integerValue+1;
        }
        
        CGFloat height=0;
        if (isShowMore) {
            if (rowCount<=4) {
                height=130;
            }else{
                height=rowCount*22.5+43+rowCount*1.0-14;
            }
        }else{
            height=130;
        }
        
        for (UIView *view in cell.subviews) {
            if ([view isKindOfClass:[TopLeftLabel class]]) {
                [view removeFromSuperview];
            }
        }
        
        TopLeftLabel *_myLabel=[[TopLeftLabel alloc] initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, height-43)];
        _myLabel.textColor=[UIColor colorWithHexString:@"949494"];
        _myLabel.font=[UIFont systemFontOfSize:15];
        _myLabel.numberOfLines=0;
        [cell addSubview:_myLabel];
        [cell sendSubviewToBack:_myLabel];
        
        if (dataModel.info==nil) {
            
        }else{
            NSString *labelTtt=dataModel.info;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:6];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
            if (attributedString!=nil||[attributedString isEqual:@""]||attributedString!=NULL) {
                _myLabel.attributedText = attributedString;
            }
            [_myLabel sizeToFit];
            if (isShowMore) {
                cell.imgv.image=[UIImage imageNamed:@"film_showmore_up"];
            }else{
                cell.imgv.image=[UIImage imageNamed:@"film_showmore"];
            }
        }
        return cell;
    }else if(indexPath.section==2){
        NewFilmIntroActorCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewFilmIntroActorCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewFilmIntroActorCell alloc]init];
        }
        NSString *actorStr=actorArr[indexPath.row];
        cell.titleLabel.text=actorStr;
        cell.nameLabel.text=![actorStr isEqualToString:@""]?indexPath.row==0?@"导演":@"演员":@"";
        return cell;
    }else{
        
        NewAVAndStillCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewAVAndStillCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewAVAndStillCell alloc]init];
        }
        [cell reflushDataForModel:dataModel];
        cell.delegate=self;
        return cell;
    }
}

//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, 164);
    }else if(indexPath.section==1){
        CGSize size=[AppUtils getStringSize:dataModel.info withFont:15];
        CGFloat rowFloat=size.width/(SCREENSIZE.width-16);
        NSString *rowStr=[NSString stringWithFormat:@"%.2f",rowFloat];
        NSRange rang=[rowStr rangeOfString:@"."];
        NSInteger rowCount=0;   //行数
        if (rang.location!=NSNotFound) {
            rowStr=[rowStr substringToIndex:rang.location];
            rowCount=rowStr.integerValue+1;
        }
        if (isShowMore) {
            if (rowCount<=4) {
                return CGSizeMake(SCREENSIZE.width, 130);
            }else{
                return CGSizeMake(SCREENSIZE.width, rowCount*22.5+43+rowCount*1.0-5);
            }
        }else{
            return CGSizeMake(SCREENSIZE.width, 130);
        }
    }else if(indexPath.section==2){
        return CGSizeMake(SCREENSIZE.width/3, 58);
    }else{
        return CGSizeMake(SCREENSIZE.width, 198);
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==2) {
        return UIEdgeInsetsMake(0, 0, 14, 0);
    }else if (section==3){
        return UIEdgeInsetsMake(0, 0, 40, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShowMore=!isShowMore;
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
            [UIView animateWithDuration:0.00001 animations:^{
                [myCollection reloadSections:indexSet];
            }];
        });
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if(indexPath.section==2){
            FilmIntroHeadReusView *fillterView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"fillterheader" forIndexPath:indexPath];
//            fillterView.delegate=self;
//            fillterView.fillterArr=fillterBtnArr;
            return fillterView;
        }
    }
    return nil;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section==2) {
        return CGSizeMake(SCREENSIZE.width, 64);
    }else{
        return CGSizeMake(0, 0);
    }
}

#pragma mark - 点击更多视频或剧照
-(void)clickMore{
    AVOrStillController *ctr=[AVOrStillController new];
    ctr.model=_model;
    [self.navigationController pushViewController:ctr animated:YES];
}

-(void)didAVAndStillSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *imgArr=[NSMutableArray array];
    for (NSString *img in dataModel.photos) {
        NSURL *url=[NSURL URLWithString:img];
        [imgArr addObject:url];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:imgArr];
    browser.displayToolbar=YES;
    browser.displayCounterLabel=YES;
    browser.displayActionButton = NO;
    [browser setInitialPageIndex:imgArr.count-indexPath.section-1];
    [self presentViewController:browser animated:YES completion:nil];
}

#pragma mark - Request

-(void)dataRequest{
    //
    [AppUtils showProgressInView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_movie" forKey:@"mod"];
    [dict setObject:@"movie" forKey:@"code"];
    [dict setObject:_model.fId forKey:@"movie_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_movie",
//                         @"code":@"movie",
//                         @"movie_id":_model.fId
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            myCollection.hidden=NO;
            NSDictionary *dic=responseObject[@"retData"];
            dataModel=[FilmIntroModel new];
            [dataModel jsonDataForDictionary:dic];
            
            actorArr=[NSMutableArray array];
            
            for (NSString *str in dataModel.actors) {
                if (![str isEqualToString:@""]||str!=nil) {
                    [actorArr addObject:str];
                }
            }
            for (NSString *str in dataModel.director) {
                if (![str isEqualToString:@""]||str!=nil) {
                    [actorArr insertObject:str atIndex:0];
                }
            }
            
            if (actorArr.count%3==1) {
                for (int i=0; i<2; i++) {
                    [actorArr addObject:@""];
                }
            }
            if (actorArr.count%3==2) {
                for (int i=0; i<1; i++) {
                    [actorArr addObject:@""];
                }
            }
            
            NSLog(@"%@---",actorArr);
        }
        [myCollection reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
