//
//  FilmIntroController.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmIntroController.h"
#import "FilmIntroHeaderCell.h"
#import "FilmIntroCommentCell.h"
#import "IntroActorsCell.h"
#import "IntroActorParam.h"
#import "FilmForYYListController.h"
#import "AVAndStillCell.h"
#import "AVOrStillController.h"
#import "FilmIntroModel.h"

@interface FilmIntroController ()<UITableViewDataSource,UITableViewDelegate,AVAndStillDelegate>

@end

@implementation FilmIntroController{
    UITableView *myTable;
    BOOL isShowMore;
    
    FilmIntroModel *dataModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
//    navBar.titleStr=@"电影简介";
    [self.view addSubview:navBar];
    
    [self setUpUI];
    
    [self dataRequest];
}

-(void)setUpUI{
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-([_model.isShow isEqualToString:@"1"]?113:64)) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"FilmIntroHeaderCell" bundle:nil] forCellReuseIdentifier:@"FilmIntroHeaderCell"];
    [myTable registerNib:[UINib nibWithNibName:@"FilmIntroCommentCell" bundle:nil] forCellReuseIdentifier:@"FilmIntroCommentCell"];
    [self.view addSubview:myTable];
    
    //buy btn
    if ([_model.isShow isEqualToString:@"1"]) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 49);
        btn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
        btn.titleLabel.font=[UIFont systemFontOfSize:16];
        [btn setTitle:@"立即购票" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
}


#pragma mark - 点击购买按钮
-(void)clickBuyBtn{
    FilmForYYListController *ctr=[FilmForYYListController new];
    ctr.model=_model;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    id photoObj=dataModel.photos;
    if ([photoObj isKindOfClass:[NSArray class]]) {
        return 4;
    }else{
        return 3;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 164;
    }else if(indexPath.section==1){
        CGSize size=[AppUtils getStringSize:dataModel.info withFont:16];
        CGFloat rowFloat=size.width/(SCREENSIZE.width-16);
        NSString *rowStr=[NSString stringWithFormat:@"%.2f",rowFloat];
        NSRange rang=[rowStr rangeOfString:@"."];
        NSInteger rowCount=0;
        if (rang.location!=NSNotFound) {
            rowStr=[rowStr substringToIndex:rang.location];
            rowCount=rowStr.integerValue+1;
        }
        if (isShowMore) {
            if (rowCount<=3) {
                return 102;
            }else{
                return rowCount*19+45;
            }
        }else{
            return 102;
        }
    }else if(indexPath.section==2){
        return 38+actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2-6;
    }else{
        return 180;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 0.00000001f;
    }else if (section==3){
        return 48;
    }else {
        return 16;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        FilmIntroHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmIntroHeaderCell"];
        if (!cell) {
            cell=[[FilmIntroHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmIntroHeaderCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataModel];
        return  cell;
    }else if(indexPath.section==1){
        FilmIntroCommentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmIntroCommentCell"];
        if (!cell) {
            cell=[[FilmIntroCommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmIntroCommentCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.myLabel.text=dataModel.info;
        if (isShowMore) {
            cell.imgv.image=[UIImage imageNamed:@"film_showmore_up"];
        }else{
            cell.imgv.image=[UIImage imageNamed:@"film_showmore"];
        }
        return  cell;
    }else if(indexPath.section==2){
        IntroActorsCell *cell=[[IntroActorsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IntroActorsCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataModel];
        return  cell;
    }else{
        AVAndStillCell *cell=[[AVAndStillCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AVAndStillCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reflushDataForModel:dataModel];
        cell.delegate=self;
        return  cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 38)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 150, 38)];
    titleLabel.font=[UIFont systemFontOfSize:15];
    titleLabel.textColor=[UIColor colorWithHexString:@"949494"];
    if (section == 2){
        titleLabel.text = @"全体主演人员";
        [AppUtils drawZhiXian:myView withColor:[UIColor colorWithHexString:@"949494"] height:0.4f firstPoint:CGPointMake(0, 37) endPoint:CGPointMake(SCREENSIZE.width, 37)];
    }
    else if (section == 3){
        titleLabel.text = @"视频和剧照";
        [AppUtils drawZhiXian:myView withColor:[UIColor colorWithHexString:@"949494"] height:0.4f firstPoint:CGPointMake(0, 37) endPoint:CGPointMake(SCREENSIZE.width, 37)];
    }else{
        titleLabel.text = @"";
    }
    [myView addSubview:titleLabel];
    
    return myView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        isShowMore=!isShowMore;
        NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
        [UIView animateWithDuration:0.00001 animations:^{
            [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
        }];
    }
}


#pragma mark - 点击更多视频或剧照
-(void)clickMore{
    AVOrStillController *ctr=[AVOrStillController new];
    ctr.model=_model;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - Request

-(void)dataRequest{
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"movie",
                         @"movie_id":_model.fId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSDictionary *dic=responseObject[@"retData"];
            dataModel=[FilmIntroModel new];
            [dataModel jsonDataForDictionary:dic];
        }
        [myTable reloadData];
        DSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
