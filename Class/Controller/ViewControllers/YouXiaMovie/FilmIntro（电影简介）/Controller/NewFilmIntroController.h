//
//  NewFilmIntroController.h
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModel.h"

@interface NewFilmIntroController : UIViewController

@property (nonatomic, strong) FilmModel *model;

@end
