//
//  NewFilmIntroHeaderCell.m
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewFilmIntroHeaderCell.h"

@implementation NewFilmIntroHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)reflushDataForModel:(FilmIntroModel *)model{
    
    //根据图片设置背景色
    NSData *imgData=[NSData dataWithContentsOfURL:[NSURL URLWithString:model.picAddr]];
    UIImage *headImg=[UIImage imageWithData:imgData];
    _bgImgv.backgroundColor=[UIColor colorWithPatternImage:headImg];
    
    [_blurView setDynamic:YES];
    _blurView.iterations=20;
    _blurView.tintColor=[UIColor blackColor];
    [_blurView clearImage];
    
    [self.movieImgv sd_setImageWithURL:[NSURL URLWithString:model.picAddr] placeholderImage:[UIImage imageNamed:Image_Default]];
    
    //
    self.movieNameLabel.text=model.movie_name;
    self.gradeLabel.text=@"用户评分";
    self.gradeSumLabel.text=[NSString stringWithFormat:@"%@分",model.grade];
    self.typelabel.text=model.type;
    self.yuyanLabel.text=[NSString stringWithFormat:@"%@/%@分钟",model.area,model.length];
    self.timeLabel.text=[NSString stringWithFormat:@"上映时间: %@",model.releaseDate];
}

@end
