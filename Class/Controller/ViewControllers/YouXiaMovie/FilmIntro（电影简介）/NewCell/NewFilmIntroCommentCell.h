//
//  NewFilmIntroCommentCell.h
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopLeftLabel.h"

@interface NewFilmIntroCommentCell : UICollectionViewCell

@property (assign, nonatomic) BOOL isShow;

@property (weak, nonatomic) IBOutlet UIImageView *imgv;


@end
