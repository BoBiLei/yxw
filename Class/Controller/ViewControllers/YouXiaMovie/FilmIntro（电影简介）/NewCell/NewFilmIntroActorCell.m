//
//  NewFilmIntroActorCell.m
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewFilmIntroActorCell.h"

@implementation NewFilmIntroActorCell{
    NSMutableArray *dataArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"e4e4e4"] height:1.0 firstPoint:CGPointMake(0, 0) endPoint:CGPointMake(SCREENSIZE.width/3, 0)];
    [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"e4e4e4"] height:1.0 firstPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, SCREENSIZE.width/3)];
}

//[super drawRect:rect] 得到一条线条
- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}

@end
