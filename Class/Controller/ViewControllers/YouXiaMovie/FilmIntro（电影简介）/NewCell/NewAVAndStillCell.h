//
//  NewAVAndStillCell.h
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmIntroModel.h"

@protocol NewAVAndStillDelegate <NSObject>

-(void)clickMore;

-(void)didAVAndStillSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface NewAVAndStillCell : UICollectionViewCell<UITableViewDelegate,UITableViewDataSource>

-(void)reflushDataForModel:(FilmIntroModel *)model;

@property (weak, nonatomic) id<NewAVAndStillDelegate> delegate;

@end
