//
//  NewAVAndStillCell.m
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewAVAndStillCell.h"
#import "IntroPhoneItemCell.h"

@implementation NewAVAndStillCell{
    UITableView *myTable;
    NSMutableArray *dataArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UILabel *topLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, 50)];
    topLabel.text=@"视频和剧照";
    topLabel.font=[UIFont systemFontOfSize:15];
    topLabel.textColor=[UIColor colorWithHexString:@"000000"];
    [self addSubview:topLabel];
    
    [AppUtils drawZhiXian:self withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 50) endPoint:CGPointMake(SCREENSIZE.width, 50)];
    
    //table 高=100
    CGFloat tHeight=110;
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 50, tHeight, SCREENSIZE.width) style:UITableViewStyleGrouped];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.center=CGPointMake(SCREENSIZE.width/2, tHeight/2+50);
    [myTable setContentOffset:CGPointMake(0,SCREENSIZE.height+164) animated:NO];
    myTable.showsVerticalScrollIndicator=YES;
    myTable.dataSource=self;
    myTable.delegate=self;
    [self addSubview:myTable];
    //顺时针90°
    CGAffineTransform transform = CGAffineTransformMakeRotation(90 * M_PI_4);
    [myTable setTransform:transform];
    
#pragma mark avView
    UIView *avView=[[UIView alloc] initWithFrame:CGRectMake(0, 160, SCREENSIZE.width, 32)];
    [self addSubview:avView];
    avView.userInteractionEnabled=YES;
    
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, avView.width, avView.height)];
    label.text=@"查看更多";
    label.font=[UIFont systemFontOfSize:15];
    label.textAlignment=NSTextAlignmentCenter;
    label.textColor=[UIColor colorWithHexString:@"55b2f0"];
    [avView addSubview:label];
    
    label.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAVBtn)];
    [label addGestureRecognizer:tap];
}

-(void)reflushDataForModel:(FilmIntroModel *)model{
    dataArr=[NSMutableArray array];
    id photoObj=model.photos;
    if ([photoObj isKindOfClass:[NSArray class]]) {
        for (NSString *str in photoObj) {
            if (str!=nil||![str isEqualToString:@""]||![str isKindOfClass:[NSString class]]) {
                [dataArr insertObject:str atIndex:0];
            }
        }
        [myTable reloadData];
        [myTable scrollToBottomAnimated:NO];
    }
}

#pragma mark - 点击更多视频/剧照
-(void)clickAVBtn{
    [self.delegate clickMore];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREENSIZE.width/3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 8;
    }
    return 0.0000000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IntroPhoneItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"IntroPhoneItemCell"];
    if (!cell) {
        cell=[[IntroPhoneItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IntroPhoneItemCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *str=dataArr[indexPath.section];
    [cell.imgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:Image_Default]];
    cell.imgv.clipsToBounds  = YES;
    cell.imgv.contentMode=UIViewContentModeScaleAspectFill;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate didAVAndStillSelectItemAtIndexPath:indexPath];
}

@end
