//
//  NewFilmIntroHeaderCell.h
//  youxia
//
//  Created by mac on 2016/12/14.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmIntroModel.h"

@interface NewFilmIntroHeaderCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bgImgv;
@property (weak, nonatomic) IBOutlet FXBlurView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *movieImgv;
@property (weak, nonatomic) IBOutlet UILabel *movieNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *typelabel;
@property (weak, nonatomic) IBOutlet UILabel *yuyanLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

-(void)reflushDataForModel:(FilmIntroModel *)model;

@end
