//
//  AVAndStillCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "AVAndStillCell.h"
#import "IntroPhoneItemCell.h"

@implementation AVAndStillCell{
    UITableView *myTable;
    NSMutableArray *dataArr;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //table 高=100
        CGFloat tHeight=110;
        myTable=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 38, tHeight, SCREENSIZE.width) style:UITableViewStyleGrouped];
        myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        myTable.backgroundColor=[UIColor whiteColor];
        myTable.center=CGPointMake(SCREENSIZE.width/2, tHeight/2+38);
        [myTable setContentOffset:CGPointMake(0,SCREENSIZE.height+164) animated:NO];
        myTable.showsVerticalScrollIndicator=YES;
        myTable.dataSource=self;
        myTable.delegate=self;
        [self addSubview:myTable];
        //顺时针90°
        CGAffineTransform transform = CGAffineTransformMakeRotation(90 * M_PI_4);
        [myTable setTransform:transform];
        
#pragma mark avView
        UIView *avView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, SCREENSIZE.width)];
        avView.center=CGPointMake((8+SCREENSIZE.width/2), 162);
        CGAffineTransform avViewTransform = CGAffineTransformMakeRotation(90 * M_PI_4);
        [avView setTransform:avViewTransform];
        [self addSubview:avView];
        avView.userInteractionEnabled=YES;
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, avView.width, avView.height)];
        label.text=@"查看更多";
        label.font=[UIFont systemFontOfSize:15];
        label.textAlignment=NSTextAlignmentCenter;
        label.textColor=[UIColor colorWithHexString:@"55b2f0"];
        label.center=CGPointMake(avView.height/2-2, avView.width/2+8);
        CGAffineTransform labTransform = CGAffineTransformMakeRotation(-90 * M_PI_4);
        [label setTransform:labTransform];
        [avView addSubview:label];
        
        label.userInteractionEnabled=YES;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAVBtn)];
        [label addGestureRecognizer:tap];
    }
    return self;
}


-(void)reflushDataForModel:(FilmIntroModel *)model{
    dataArr=[NSMutableArray array];
    id photoObj=model.photos;
    if ([photoObj isKindOfClass:[NSArray class]]) {
        for (NSString *str in photoObj) {
            if (str!=nil||![str isEqualToString:@""]||![str isKindOfClass:[NSString class]]) {
                [dataArr insertObject:str atIndex:0];
            }
        }
        [myTable reloadData];
        [myTable scrollToBottomAnimated:NO];
    }
}

#pragma mark - 点击更多视频/剧照
-(void)clickAVBtn{
    [self.delegate clickMore];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREENSIZE.width/3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 8;
    }
    return 0.0000000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IntroPhoneItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"IntroPhoneItemCell"];
    if (!cell) {
        cell=[[IntroPhoneItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IntroPhoneItemCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *str=dataArr[indexPath.section];
    [cell.imgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:Image_Default]];
    cell.imgv.clipsToBounds  = YES;
    cell.imgv.contentMode=UIViewContentModeScaleAspectFill;
    return  cell;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    HomePageCagegoryModel *model=dataArr[indexPath.section];
//    NSLog(@"%@----------",model.img);
//    [self.businessDelegate clickBusinessWithModel:model];
//}

@end
