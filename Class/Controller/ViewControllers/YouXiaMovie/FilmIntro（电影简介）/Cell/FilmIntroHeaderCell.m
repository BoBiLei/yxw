//
//  FilmIntroHeaderCell.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmIntroHeaderCell.h"

@implementation FilmIntroHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 @property (weak, nonatomic) IBOutlet UILabel *titleLabel;
 @property (weak, nonatomic) IBOutlet UILabel *titleEnLabel;
 @property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
 @property (weak, nonatomic) IBOutlet UILabel *commentSum;
 @property (weak, nonatomic) IBOutlet UILabel *typeLabel;
 @property (weak, nonatomic) IBOutlet UILabel *areaLabel;
 @property (weak, nonatomic) IBOutlet UILabel *syTimeLabel;
 
 
 -(void)reflushDataForModel:(FilmIntroModel *)model;*/

-(void)reflushDataForModel:(FilmIntroModel *)model{
    [self.filmImgv sd_setImageWithURL:[NSURL URLWithString:model.picAddr] placeholderImage:[UIImage imageNamed:Image_Default]];
    self.titleLabel.text=model.movie_name;
    self.gradeLabel.text=[NSString stringWithFormat:@"%@分",model.grade];
    self.typeLabel.text=model.type;
    self.areaLabel.text=[NSString stringWithFormat:@"%@/%@分钟",model.area,model.length];
    self.syTimeLabel.text=[NSString stringWithFormat:@"上映时间: %@",model.releaseDate];
}

@end
