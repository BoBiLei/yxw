//
//  IntroPhoneItemCell.h
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  剧照

#import <UIKit/UIKit.h>

@interface IntroPhoneItemCell : UITableViewCell

@property (nonatomic, strong) UIImageView *imgv;

@end
