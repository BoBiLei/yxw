//
//  FilmIntroCommentCell.h
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmIntroCommentCell : UITableViewCell

@property (assign, nonatomic) BOOL isShow;

@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@end
