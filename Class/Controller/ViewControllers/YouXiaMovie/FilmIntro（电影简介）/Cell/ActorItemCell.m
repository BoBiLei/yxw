//
//  ActorItemCell.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ActorItemCell.h"
#import "IntroActorParam.h"

@implementation ActorItemCell{
    UIImageView *imgv;
    
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat imgHeight=actor_imageWidth+26;
        
#pragma mark 图片
        
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(actor_marginY, 0, actor_imageWidth, imgHeight)];
        imgv.center=CGPointMake(actor_marginY+imgHeight/2, actor_imageWidth/2);
        imgv.contentMode=UIViewContentModeScaleToFill;
        //逆时针90°
        [imgv setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        imgv.image=[UIImage imageNamed:@"intro_actor"];
        [self addSubview:imgv];
        
#pragma mark 标题
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.height, actor_labelHeight)];
        _titleLabel.center=CGPointMake(imgHeight+actor_marginY+actor_labelHeight/2+actor_titlemaginY-4, actor_imageWidth/2);
        _titleLabel.font=[UIFont systemFontOfSize:11];
        _titleLabel.textAlignment=NSTextAlignmentCenter;
        //逆时针90°
        [_titleLabel setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        _titleLabel.text=@"官元宏彰";
        [self addSubview:_titleLabel];
        
#pragma mark 标题
        _detailLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.height, actor_labelHeight)];
        _detailLabel.center=CGPointMake(imgHeight+actor_marginY+actor_labelHeight/2+actor_labelHeight-8, actor_imageWidth/2);
        _detailLabel.textColor=[UIColor colorWithHexString:@"#898989"];
        _detailLabel.font=[UIFont systemFontOfSize:11];
        _detailLabel.textAlignment=NSTextAlignmentCenter;
        [_detailLabel setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        _detailLabel.text=@"导演";
        [self addSubview:_detailLabel];
    }
    return self;
}

@end
