//
//  FilmIntroHeaderCell.h
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  头部

#import <UIKit/UIKit.h>
#import "FilmIntroModel.h"

@interface FilmIntroHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bgImgv;
@property (weak, nonatomic) IBOutlet UIImageView *filmImgv;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *syTimeLabel;


-(void)reflushDataForModel:(FilmIntroModel *)model;

@end
