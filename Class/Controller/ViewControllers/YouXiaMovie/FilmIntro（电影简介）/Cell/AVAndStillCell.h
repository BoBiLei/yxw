//
//  AVAndStillCell.h
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  //视频和剧照

#import <UIKit/UIKit.h>
#import "FilmIntroModel.h"

@protocol AVAndStillDelegate <NSObject>

-(void)clickMore;

@end

@interface AVAndStillCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

-(void)reflushDataForModel:(FilmIntroModel *)model;

@property (weak, nonatomic) id<AVAndStillDelegate> delegate;

@end
