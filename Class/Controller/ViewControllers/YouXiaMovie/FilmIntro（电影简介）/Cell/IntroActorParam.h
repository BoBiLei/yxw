//
//  IntroActorParam.h
//  youxia
//
//  Created by mac on 2016/11/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#ifndef IntroActorParam_h
#define IntroActorParam_h

#define actor_marginY 8          //Y轴间距
#define actor_labelHeight 24     //label高度
#define actor_imageWidth (SCREENSIZE.width-48)/5        //image宽度
#define actor_imageHeight ((SCREENSIZE.width-48)/5)+26    //image高度
#define actor_titlemaginY 4      //label离image的间距

#endif /* IntroActorParam_h */
