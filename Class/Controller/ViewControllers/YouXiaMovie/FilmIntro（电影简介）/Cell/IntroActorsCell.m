//
//  IntroActorsCell.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "IntroActorsCell.h"
#import "ActorItemCell.h"
#import "IntroActorParam.h"
#import "IntroActorParam.h"

@implementation IntroActorsCell{
    UITableView *myTable;
    NSMutableArray *dataArr;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        myTable=[[UITableView alloc] initWithFrame:CGRectMake(SCREENSIZE.width, 38, actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2-6, SCREENSIZE.width) style:UITableViewStyleGrouped];
        myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        myTable.backgroundColor=[UIColor whiteColor];
        myTable.center=CGPointMake(SCREENSIZE.width/2, (actor_marginY+actor_imageHeight+actor_titlemaginY+actor_labelHeight*2-6)/2+38);
        [myTable setContentOffset:CGPointMake(0,SCREENSIZE.height+164) animated:NO];
        myTable.showsVerticalScrollIndicator=YES;
        myTable.dataSource=self;
        myTable.delegate=self;
        [self addSubview:myTable];
        
        //顺时针90°
        CGAffineTransform transform = CGAffineTransformMakeRotation(90 * M_PI_4);
        [myTable setTransform:transform];
    }
    return self;
}

-(void)reflushDataForModel:(FilmIntroModel *)model{
    dataArr=[NSMutableArray array];
    id actorObj=model.actors;
    if ([actorObj isKindOfClass:[NSArray class]]) {
        for (NSString *str in actorObj) {
            if (![str isEqualToString:@""]||str!=nil) {
                [dataArr insertObject:str atIndex:0];
            }
        }
        
        id dyObj=model.director;
        if ([dyObj isKindOfClass:[NSArray class]]) {
            for (NSString *str in dyObj) {
                if (![str isEqualToString:@""]||str!=nil) {
                    [dataArr addObject:str];
                }
            }
        }
        [myTable reloadData];
        [myTable scrollToBottomAnimated:NO];
    }
}


#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return actor_imageWidth;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 8;
    }
    return 0.0000000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ActorItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ActorItemCell"];
    if (!cell) {
        cell=[[ActorItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ActorItemCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *str=dataArr[indexPath.section];
    cell.titleLabel.text=str;
    if (indexPath.section==dataArr.count-1) {
        cell.detailLabel.text=@"导演";
    }else{
        cell.detailLabel.text=@"演员";
    }
    return  cell;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    HomePageCagegoryModel *model=dataArr[indexPath.section];
//    NSLog(@"%@----------",model.img);
//    [self.businessDelegate clickBusinessWithModel:model];
//}

@end
