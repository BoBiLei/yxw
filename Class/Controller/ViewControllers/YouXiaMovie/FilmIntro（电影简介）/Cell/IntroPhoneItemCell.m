//
//  AVItemCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "IntroPhoneItemCell.h"

@implementation IntroPhoneItemCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/3, 94)];
        _imgv.backgroundColor=[UIColor whiteColor];
        _imgv.center=CGPointMake(54, (SCREENSIZE.width/3)/2);
        _imgv.contentMode=UIViewContentModeScaleAspectFit;
        //逆时针90°
        [_imgv setTransform:CGAffineTransformMakeRotation(-90 * M_PI_4)];
        [self addSubview:_imgv];
    }
    return self;
}

@end
