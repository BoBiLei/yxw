//
//  IntroActorsCell.h
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmIntroModel.h"

@interface IntroActorsCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

-(void)reflushDataForModel:(FilmIntroModel *)model;

@end
