//
//  SelectFilmCouponController.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SelectFilmCouponController.h"
#import "SelectFilmCouponCell.h"
#import "CustomIOSAlertView.h"
#import "SelectFilmCouponModel.h"

@interface SelectFilmCouponController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SelectFilmCouponController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    //
    UITextField *couponNumTF;
    CustomIOSAlertView *validaAlertView;
    
    //
    SelectFilmCouponModel *selectCouponModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavBar];
    
    [self setUI];
    
    [self dataRequest];
}

-(void)addNavBar{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [self.view addSubview:view];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:back];
    
    UIButton *titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    titleLabel.titleLabel.font=[UIFont systemFontOfSize:17];
    [titleLabel setTitle:@"选择优惠券" forState:UIControlStateNormal];
    [view addSubview:titleLabel];
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.frame=CGRectMake(SCREENSIZE.width-56, 20, 44.f, 44.f);
    [addBtn setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    addBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [addBtn setTitle:@"添加" forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(clickAddBtn) forControlEvents:UIControlEventTouchUpInside];
    addBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [view addSubview:addBtn];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickAddBtn{
    validaAlertView = [[CustomIOSAlertView alloc] init];
    validaAlertView.containerView.layer.cornerRadius=12;
    [validaAlertView setContainerView:[self createAlertView]];
    [validaAlertView setButtonTitles:nil];
    [validaAlertView setUseMotionEffects:true];
    [validaAlertView show];
}

#pragma mark - 创建弹出验证码提示框

-(UIView *)createAlertView{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width-48, 300)];
    view.backgroundColor=[UIColor colorWithHexString:@"ffffff"];
    view.layer.cornerRadius=8;
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 10, view.width-20, 24)];
    tipLabel.textAlignment=NSTextAlignmentLeft;
    tipLabel.font=[UIFont systemFontOfSize:17];
    tipLabel.textColor=[UIColor colorWithHexString:@"000000"];
    tipLabel.text=@"添加优惠券";
    [view addSubview:tipLabel];
    
#pragma mark x按钮
    UIButton *closeImg=[UIButton buttonWithType:UIButtonTypeCustom] ;
    closeImg.frame=CGRectMake(view.width-40, 4, 36, 36);
    [closeImg setImage:[UIImage imageNamed:@"add_coupon"] forState:UIControlStateNormal];
    [closeImg addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeImg];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"eeeeee"] height:1.0f firstPoint:CGPointMake(0, tipLabel.origin.y*2+tipLabel.height) endPoint:CGPointMake(view.width, tipLabel.origin.y*2+tipLabel.height)];
    
#pragma mark 验证码View
    UIView *volatileView=[[UIView alloc] initWithFrame:CGRectMake(24, tipLabel.origin.y*2+tipLabel.height+22, view.width-48, 42)];
    volatileView.layer.cornerRadius=1;
    volatileView.backgroundColor=[UIColor colorWithHexString:@"#ffffff"];
    volatileView.layer.borderWidth=0.5f;
    volatileView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [view addSubview:volatileView];
    
    couponNumTF=[[UITextField alloc] initWithFrame:CGRectMake(8, 0, volatileView.width-14, volatileView.height)];
    couponNumTF.tintColor=[UIColor colorWithHexString:@"#55b2f0"];
    couponNumTF.placeholder=@"请输入优惠券码";
    couponNumTF.font=[UIFont systemFontOfSize:15];
    couponNumTF.textColor=[UIColor colorWithHexString:@"#363636"];
    couponNumTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [volatileView addSubview:couponNumTF];
    
    UIButton *reflBtn=[UIButton buttonWithType:UIButtonTypeCustom] ;
    reflBtn.layer.cornerRadius=3;
    reflBtn.frame=CGRectMake(volatileView.origin.x, volatileView.origin.y+volatileView.height+30, volatileView.width, volatileView.height);
    reflBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [reflBtn setTitle:@"提 交" forState:UIControlStateNormal];
    reflBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [reflBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [reflBtn addTarget:self action:@selector(clickRefleBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:reflBtn];
    
    //
    CGFloat lastYH=reflBtn.origin.y+reflBtn.height+20;
    
    CGRect frame=view.frame;
    frame.size.height=lastYH+16;
    view.frame=frame;
    
    return view;
}

-(void)clickCloseBtn{
    [AppUtils closeKeyboard];
    [validaAlertView close];
}

//点击提交
-(void)clickRefleBtn{
    NSLog(@"点击提交");
    if ([couponNumTF.text isEqualToString:@""]||couponNumTF.text==nil) {
        [AppUtils showSuccessMessage:@"请输入优惠券码" inView:validaAlertView.dialogView];
    }else{
        [AppUtils closeKeyboard];
        [validaAlertView close];
        [self addCouponRequest];
    }
}

#pragma mark - init table
-(void)setUI{
    
    selectCouponModel=[SelectFilmCouponModel new];
    selectCouponModel.cid=_couponId;
    
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"SelectFilmCouponCell" bundle:nil] forCellReuseIdentifier:@"SelectFilmCouponCell"];
    [self.view addSubview:myTable];
    
    UIButton *confirmBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame=CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 49);
    confirmBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    [confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirmBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [confirmBtn setTitle:@"确 定" forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(clickConfirmBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmBtn];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 46;
    }
    return 134;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==dataArr.count-1) {
        return 16;
    }
    return 0.000000001f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectFilmCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectFilmCouponCell"];
    if (!cell) {
        cell=[[SelectFilmCouponCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectFilmCouponCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    SelectFilmCouponModel *model=dataArr[indexPath.section];
    [cell reflushDataForModel:model atIndexPath:indexPath];
    if ([model.cid isEqualToString:_couponId]) {
        [myTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectFilmCouponModel *model=dataArr[indexPath.section];
    selectCouponModel=model;
    _couponId=model.cid;
}

#pragma mark - 点击确定
-(void)clickConfirmBtn{
//    self.couponId=selectCouponModel.cid;
//    if (self.filmCouponBlock) {
//        self.filmCouponBlock(selectCouponModel);
//    }
//    if (![selectCouponModel.cid isEqualToString:@"0"]) {
//        [self selectCouponRequest];
//    }else{
//        [[NSNotificationCenter defaultCenter] postNotificationName:FilmOrder_SelectCoupon_Noti object:selectCouponModel];
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    
    [self selectCouponRequest];
}

#pragma mark - Request

-(void)dataRequest{
    dataArr=[NSMutableArray array];
    selectCouponModel.cid=_couponId;
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"coupon_list" forKey:@"code"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"coupon_list",
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id obj=responseObject[@"retData"];
            if ([obj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in obj) {
                    SelectFilmCouponModel *model=[SelectFilmCouponModel new];
                    [model jsonDataForDictionary:dic];
                    [dataArr addObject:model];
                }
                
                SelectFilmCouponModel *model=[SelectFilmCouponModel new];
                model.cid=@"0";
                model.price=@"0";
                [dataArr insertObject:model atIndex:0];
                
                [myTable reloadData];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)selectCouponRequest{
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"use_coupon" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:selectCouponModel.cid forKey:@"coupon_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"use_coupon",
//                         @"orderid":_orderId,
//                         @"coupon_id":selectCouponModel.cid,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FilmOrder_SelectCoupon_Noti object:selectCouponModel];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

-(void)addCouponRequest{
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"add_coupon" forKey:@"code"];
    [dict setObject:couponNumTF.text forKey:@"number"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"add_coupon",
//                         @"number":couponNumTF.text,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [self dataRequest];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
