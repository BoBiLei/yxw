//
//  SelectFilmCouponModel.m
//  youxia
//
//  Created by mac on 2016/12/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SelectFilmCouponModel.h"

@implementation SelectFilmCouponModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.addtime=[NSString stringWithFormat:@"%@",dic[@"addtime"]];
    self.duetime=[NSString stringWithFormat:@"%@",dic[@"duetime"]];
    self.cid=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.number=[NSString stringWithFormat:@"%@",dic[@"number"]];
    self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.part_used=[NSString stringWithFormat:@"%@",dic[@"part_used"]];
    self.password=[NSString stringWithFormat:@"%@",dic[@"password"]];
    self.price=[NSString stringWithFormat:@"%@",dic[@"price"]];
    self.status=[NSString stringWithFormat:@"%@",dic[@"status"]];
    self.uid=[NSString stringWithFormat:@"%@",dic[@"uid"]];
    self.used_price=[NSString stringWithFormat:@"%@",dic[@"used_price"]];
    self.used_rule=[NSString stringWithFormat:@"%@",dic[@"used_rule"]];
    self.used_status=[NSString stringWithFormat:@"%@",dic[@"used_status"]];
    self.usetime=[NSString stringWithFormat:@"%@",dic[@"usetime"]];
}

@end
