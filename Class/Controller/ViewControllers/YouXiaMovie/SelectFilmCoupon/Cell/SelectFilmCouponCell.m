//
//  SelectFilmCouponCell.m
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SelectFilmCouponCell.h"

@implementation SelectFilmCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    self.mainView.layer.cornerRadius=4;
    self.mainView.layer.borderWidth=0.5f;
    self.mainView.layer.borderColor=[UIColor colorWithHexString:@"c7c7c7"].CGColor;
    NSString *labelTtt=@"恭喜您获得一张卢米埃影城优惠券，不限场次/影片每个账号每个月限用一张。";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:4];
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    _commentLabel.attributedText = attributedString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _selectedBtn.image=[UIImage imageNamed:@"film_pay_sele"];
    }else{
        _selectedBtn.image=[UIImage imageNamed:@"film_pay_nor"];
    }
}

-(void)reflushDataForModel:(SelectFilmCouponModel *)model atIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        self.titleLabel.text=@"不使用优惠券";
        self.titleLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
        
        self.commentLabel.hidden=YES;
        self.dateLabel.hidden=YES;
        self.line.hidden=YES;
    }else{
        self.titleLabel.text=@"当月免费一张优惠券";
        self.titleLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
        
        self.commentLabel.hidden=NO;
        self.dateLabel.hidden=NO;
        self.line.hidden=NO;
    }
    
    self.dateLabel.text=[NSString stringWithFormat:@"使用期限: %@",model.duetime];
}

@end
