//
//  SelectFilmCouponCell.h
//  youxia
//
//  Created by mac on 2016/11/28.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectFilmCouponModel.h"

@interface SelectFilmCouponCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *selectedBtn;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *line;



-(void)reflushDataForModel:(SelectFilmCouponModel *)model atIndexPath:(NSIndexPath *)indexPath;

@end
