//
//  SeatBtn.m
//  wangle
//
//  Created by zm on 15/6/11.
//
//

#import "SeatBtn.h"

@implementation SeatBtn

//根据状态选择座位图片
-(void)setSeatStatus:(SeatStatus)seatStatus{
    _seatStatus = seatStatus;
    
    switch (_seatStatus) {
        case 0:
            [self setBackgroundImage:[UIImage imageNamed:@"sear_hadsell"] forState:UIControlStateNormal];
            self.userInteractionEnabled = NO;
            break;
        case 1:
            [self setBackgroundImage:[UIImage imageNamed:@"choosable"] forState:UIControlStateNormal];
            break;
        case 2:
            [self setBackgroundImage:[UIImage imageNamed:@"seat_sele"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

//给座位分配座位号
-(void)setColNum:(NSString *)colNum{
    _colNum = colNum;
}

-(void)setRowNum:(NSString *)rowNum{
    _rowNum = rowNum;
}

@end
