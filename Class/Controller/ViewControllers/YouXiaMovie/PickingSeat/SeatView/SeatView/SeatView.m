//
//  SeatView.m
//  wangle
//
//  Created by zm on 15/6/11.
//
//

#import "SeatView.h"
#import "Utils.h"

#define SeatScrollTAG 1000
#define IndexScrollTAG 2000

#define Max_SeleSeat 4          //最多选择座位数

@implementation SeatView{
    NSMutableArray *rowLabelArr;

    UIImageView *screen;
    UIView *_bgView;
}

-(id)initWithFrame:(CGRect)frame rowMaxNum:(NSInteger)rowMaxNum columnMaxNum:(NSInteger)columnMaxNum noSeatArray:(NSArray *)noSeatArr{
    self = [super initWithFrame:frame];
    if (self) {
        rowLabelArr = [NSMutableArray array];
        _seatBtnArr = [NSMutableArray array];
        _selectedBtnArr = [NSMutableArray array];
        
        _rowMaxNum = rowMaxNum;
        _columnMaxNum = columnMaxNum;
        
        [self setBackgroundColor:HexRGB(0xf5f5f5)];
        
        [self setInstructionsUI];//顶部 厅号和座位总数
        [self initSeatsWithRow:_rowMaxNum column:_columnMaxNum noSeatArr:noSeatArr];//座位布局
    }
    return self;
}

//页面初始化 
-(void)setInstructionsUI{
    //厅号和座位总数
    screen = [[UIImageView alloc] initWithFrame:CGRectMake((kScreen.size.width-110)/2, 0, 110, 24)];
    [screen setImage:[UIImage imageNamed:@"screenBg"]];
    [self addSubview:screen];
    
    _seatInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake( screen.frame.origin.x+10, screen.frame.origin.y, screen.frame.size.width - 20, screen.frame.size.height)];
    _seatInfoLabel.font = [UIFont systemFontOfSize:13];
    _seatInfoLabel.textColor = [UIColor colorWithHexString:@"1a1a1a"];
    _seatInfoLabel.textAlignment = NSTextAlignmentCenter;
    _seatInfoLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:_seatInfoLabel];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(screen.frame), kScreen.size.width, self.frame.size.height  - CGRectGetMaxY(screen.frame))];
    _scrollView.backgroundColor = [UIColor clearColor];
    [_scrollView setTag:SeatScrollTAG];
    _scrollView.delegate = self;
//    _scrollView.bounces = NO;
    _scrollView.maximumZoomScale = 2.0;
    _scrollView.minimumZoomScale = 1.0;
    [self addSubview:_scrollView];
    
    _bgView = [UIView new];
    _bgView.frame = CGRectMake(0, 0, kScreen.size.width, self.frame.size.height  - CGRectGetMaxY(screen.frame));
    [_scrollView addSubview:_bgView];
}

//显示厅号
-(void)setRoomName:(NSString *)roomName seatNum:(int)seatNum{
    [_seatInfoLabel setText:[NSString stringWithFormat:@"%@",roomName]];
}

#pragma mark - 初始化座位，最大列，最大行
-(void)initSeatsWithRow:(NSInteger)rowNum column:(NSInteger)columnNum noSeatArr:(NSArray *)noSeatArr{
    _maxColNum = columnNum;
    _maxRowNum = rowNum;
    
    CGFloat btnWidth = Item_Width;
    _bgView.frame = CGRectMake(0, 0, columnNum*btnWidth + (columnNum + 1)*Item_Gap + btnWidth + 50,  rowNum*btnWidth + (rowNum + 1)*Item_Gap + 40);
    
    for (int i = 0; i < rowNum * columnNum; i ++) {
        CGFloat column = i % columnNum;//第几列
        CGFloat row = i / columnNum;//第几行
        
        SeatBtn *seatBtn = [SeatBtn buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(column * btnWidth + (column + 1) * Item_Gap + btnWidth + 25 ,row * btnWidth + (row + 1) * Item_Gap + 20, btnWidth, btnWidth);
        seatBtn.frame = frame;
        [seatBtn setSeatStatus:CHOOSABLE];
        
        seatBtn.seat_X = row + 1;
        seatBtn.seat_Y = column + 1;
        seatBtn.hidden = YES;
        [seatBtn addTarget:self action:@selector(seatBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [_seatBtnArr addObject:seatBtn];
        [_bgView addSubview:seatBtn];
    }
    _scrollView.contentSize = CGSizeMake(columnNum*btnWidth + (columnNum + 1)*Item_Gap + btnWidth + 50, rowNum*btnWidth + (rowNum + 1)*Item_Gap + 40);
    
    _rowScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(5,_scrollView.frame.origin.y, 15, _scrollView.frame.size.height)];
    _rowScrollView.backgroundColor = [UIColor colorWithHexString:@"a5a5a5"];
    _rowScrollView.alpha=0.8;
    _rowScrollView.layer.cornerRadius = 7.5;
    _rowScrollView.layer.masksToBounds = YES;
    _rowScrollView.delegate = self;
    _rowScrollView.tag = IndexScrollTAG;
    [self addSubview:_rowScrollView];
    
    NSMutableArray *tagArr=[NSMutableArray array];
    for (int j = 0; j < rowNum; j ++) {
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(0, j * btnWidth + (j + 1) * Item_Gap + 20, 15, btnWidth);
        if (noSeatArr.count!=0) {
            [noSeatArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString *str=obj;
                if ([[NSString stringWithFormat:@"%d",j+1] isEqualToString:str]) {
                    [tagArr addObject:obj];
                    label.hidden=YES;
                }
                label.text = [NSString stringWithFormat:@"%ld",j-tagArr.count+1];
            }];
        }else{
            label.text = [NSString stringWithFormat:@"%d",j+1];
        }
        
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:11];
        label.textColor = [UIColor whiteColor];
        [rowLabelArr addObject:label];
        [_rowScrollView addSubview:label];
    }
    _rowScrollView.contentSize = CGSizeMake(15, _scrollView.contentSize.height);
}

#pragma mark  - 设置座位的状态，是否显示

-(void)setSeatStatus:(NSArray *)arr{
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *seatInfoDic = [arr objectAtIndex:idx];
        
        NSString *colNumber = [NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"columnId"]];
        NSString *rowNumber = [NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"rowId"]];
        
        NSInteger _x = [[seatInfoDic objectForKey:@"rowNum"] integerValue];
        NSInteger _y = [[seatInfoDic objectForKey:@"columnNum"] integerValue];
        
        //座位是否卖出（已经被锁定）
        NSString *isLock=[NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"isLock"]];
        
        //座位号（用户删除）
        NSString *seatCode = [NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"seatId"]];
        
        //情侣座
        NSString *loveIndex = [NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"loveIndex"]];
        
        [_seatBtnArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SeatBtn *seatBtn = (SeatBtn *)[_seatBtnArr objectAtIndex:idx];
            if (seatBtn.seat_X == _x && seatBtn.seat_Y == _y) {
                seatBtn.rowNum = rowNumber;
                seatBtn.colNum = colNumber;
                seatBtn.seatCode=seatCode;
                if ((![rowNumber isEqualToString:@""] && ![colNumber isEqualToString:@""])||
                    (rowNumber == nil && colNumber == nil)) {
                    seatBtn.hidden = NO;
                }
                if ([isLock isEqualToString:@"1"]) {
                    [seatBtn setSeatStatus:SOLD];
                }
                [seatBtn setIsLove:[loveIndex isEqualToString:@"0"]?NO:YES];
            }
            
        }];
    }];
}

//将已经售出或预定的座位改为红色并且不可选
-(void)setSoldSeatsStatus:(NSArray *)arr{
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *seatInfoDic = [arr objectAtIndex:idx];
        NSString *seatCode = [NSString stringWithFormat:@"%@",[seatInfoDic objectForKey:@"seatCode"]];
        [_seatBtnArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SeatBtn *seatBtn = (SeatBtn *)[_seatBtnArr objectAtIndex:idx];
            if ([seatBtn.seatCode isEqualToString:seatCode]) {
                 [seatBtn setSeatStatus:SOLD];
            }
        }];
    }];
}

//设置（未选中）
-(void)setNormalSeatsStatus:(id)seatBtn{
    SeatBtn *cancelBtn=(SeatBtn *)seatBtn;
    [_seatBtnArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        SeatBtn *seatBtn = (SeatBtn *)[_seatBtnArr objectAtIndex:idx];
        if ([seatBtn.seatCode isEqualToString:cancelBtn.seatCode]) {
            [seatBtn setSeatStatus:CHOOSABLE];
            //必须从_selectedBtnArr删除所选
            [_selectedBtnArr removeObject:seatBtn];
        }
    }];
}

#pragma mark  - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    if (scrollView.tag == SeatScrollTAG){
        [_rowScrollView setContentOffset:CGPointMake(0, y)];
    }else if (scrollView.tag == IndexScrollTAG){
        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y)];
    }
}

#pragma mark
-(void)seatBtnClick:(id)sender{
    SeatBtn *seatBtn = (SeatBtn *)sender;
    if (seatBtn.seatStatus == SELECTED){
        [seatBtn setSeatStatus:CHOOSABLE];
        [_selectedBtnArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SeatBtn *btn = (SeatBtn *)[_selectedBtnArr objectAtIndex:idx];
            if (btn.seatStatus == CHOOSABLE) {
                *stop = YES;
                if (*stop) {
                    [_selectedBtnArr removeObject:btn];
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"seat_Y" ascending:YES];
                    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
                    [_selectedBtnArr sortUsingDescriptors:sortDescriptors];
                }
            }
        }];
    }else if (seatBtn.seatStatus == CHOOSABLE){
        AppDelegate *appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
        if (_selectedBtnArr.count >= Max_SeleSeat) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"最多可选%d个座位",Max_SeleSeat] inView:appdel.window];
            return;
        }
        [seatBtn setSeatStatus:SELECTED];
        [_selectedBtnArr addObject:seatBtn];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"seat_Y" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
        [_selectedBtnArr sortUsingDescriptors:sortDescriptors];
    }
    [self.delegate hadSelectSeat:_selectedBtnArr];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    if (scrollView.tag == SeatScrollTAG) {
        return _bgView;
    }else{
        return nil;
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    if (scrollView.tag == SeatScrollTAG) {
        CGFloat newHeight = Item_Width * scrollView.zoomScale;
        CGFloat newGap = Item_Gap * scrollView.zoomScale;
        
        _scrollView.contentSize = CGSizeMake(_columnMaxNum*newHeight + (_columnMaxNum + 1)*newGap + newHeight + 50, _rowMaxNum*newHeight + (_rowMaxNum + 1)*newGap + 40);
        
        [rowLabelArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UILabel *rowLabel = (UILabel *)rowLabelArr[idx];
            [rowLabel setFrame:CGRectMake(0, idx * newHeight + (idx + 1) * newGap + 20*scrollView.zoomScale, 15, newHeight)];
        }];
    }
}

@end
