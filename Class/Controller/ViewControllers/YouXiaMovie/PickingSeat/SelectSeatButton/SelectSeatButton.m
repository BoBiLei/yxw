//
//  SelectSeatButton.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SelectSeatButton.h"

@implementation SelectSeatButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.titleLabel];
    [self addSubview:self.imgv];
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.font=[UIFont systemFontOfSize:13];
        _titleLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
        _titleLabel.adjustsFontSizeToFitWidth=YES;
        _titleLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(UIButton *)imgv{
    if (!_imgv) {
        _imgv=[UIButton buttonWithType:UIButtonTypeCustom];
        _imgv.userInteractionEnabled=NO;
    }
    return _imgv;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.titleLabel.frame = CGRectMake(2, 0, width-height+8, height);
    self.imgv.frame = CGRectMake(width-height+2, 0, height-8, height);
}

@end
