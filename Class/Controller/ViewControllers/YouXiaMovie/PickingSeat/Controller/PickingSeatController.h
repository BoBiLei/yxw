//
//  PickingSeatController.h
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GouPiaoInfoModel.h"

@interface PickingSeatController : UIViewController

@property (nonatomic, strong) GouPiaoInfoModel *gpModel;

@property (nonatomic, assign) BOOL isActive;

@end
