//
//  PickingSeatController.m
//  youxia
//
//  Created by mac on 2016/11/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PickingSeatController.h"
#import "SelectSeatButton.h"
#import "FilmConfirmOrder.h"
#import "SeatView.h"
#import "SeatBtn.h"
#import "Utils.h"
#import "PickSeatBottomView.h"
#import "WelcomeController.h"

@interface PickingSeatController ()<SeatViewDelegate>

@end

@implementation PickingSeatController{
    NSString *navTitleStr;
    UIButton *titleLabel;
    UILabel *cinemaLabel;
    UILabel *playTimeLabel;
    
    UIView *headerView;
    SeatView *_seatView;
    NSMutableArray *hadSelectSeatArr;
    
    UILabel *tipLabel;
    
    NSString *ccPrice;                  //场次价格
    
    PickSeatBottomView *seleSeatView;
    UILabel *totaoPirceLabel;
    UILabel *buySumLabel;
    
    NSString *param_seatIds;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:HexRGB(0xf5f5f5)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBuyMovieNofi) name:@"GoBuyMovie_Notification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upNotifacation) name:FilmOrder_CancelNotifacation object:nil];
    
    [self addNavBar];
    
    if (_isActive) {
        [self activeDataRequest];
    }else{
        [self dataRequest];
    }
}

-(void)goBuyMovieNofi{
    [self confirmRequest];
}

-(void)upNotifacation{
    if (_isActive) {
        [self activeDataRequest];
    }else{
        [self dataRequest];
    }
}

#pragma mark - AddNavBar
-(void)addNavBar{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(16, 20, 52.f, 44.f);
    [back setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:back];
    
    titleLabel=[[UIButton alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
    titleLabel.titleLabel.font=[UIFont systemFontOfSize:17];
    [view addSubview:titleLabel];
    
    [AppUtils drawZhiXian:view withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - addHeaderView
-(void)addHeaderView{
    headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 124)];
    [self.view addSubview:headerView];
    
    cinemaLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 16, SCREENSIZE.width-16, 21)];
    cinemaLabel.textColor=[UIColor colorWithHexString:@"000000"];
    cinemaLabel.font=[UIFont systemFontOfSize:16];
    [headerView addSubview:cinemaLabel];
    
    playTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, cinemaLabel.origin.y+cinemaLabel.height+4, cinemaLabel.width, 19)];
    playTimeLabel.textColor=[UIColor colorWithHexString:@"949494"];
    playTimeLabel.font=[UIFont systemFontOfSize:14];
    [headerView addSubview:playTimeLabel];
    
    [AppUtils drawZhiXian:headerView withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(8, playTimeLabel.origin.y+playTimeLabel.height+15) endPoint:CGPointMake(SCREENSIZE.width-8, playTimeLabel.origin.y+playTimeLabel.height+15)];
    
    CGFloat btnWidth=SCREENSIZE.width/5;
    CGFloat btnY=playTimeLabel.origin.y+playTimeLabel.height+17;
    CGFloat btnHeight=42;
    CGFloat lastY=0;
    for (int i=0; i<3; i++) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(i*btnWidth+8, btnY, btnWidth, btnHeight);
        btn.titleLabel.font=[UIFont systemFontOfSize:13];
        btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [btn setTitle:i==0?@"可选":i==1?@"已售":@"已选" forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:i==0?@"seat_nor":i==1?@"sear_hadsell":@"seat_sele"] forState:UIControlStateNormal];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [btn setTitleColor:[UIColor colorWithHexString:@"878787"] forState:UIControlStateNormal];
        [headerView addSubview:btn];
        
        lastY=btn.origin.y+btn.height;
    }
    CGRect frame=headerView.frame;
    frame.size.height=lastY;
    headerView.frame=frame;
}

#pragma mark - AddSeatView
//添加最大数量座位视图
-(void)addSeatViewByMaxX:(NSInteger)maxX MaxY:(NSInteger)maxY withObject:(id)object roomName:(NSString *)roomName noSeatArr:(NSArray *)noSeatArr{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[SeatView class]]) {
            [view removeFromSuperview];
        }
    }
    //根据最大行和最大列进行座位矩阵布局，包括现实存在的座位和不存在的座位（初始状态为隐藏）
    _seatView = [[SeatView alloc] initWithFrame:CGRectMake(0, 64+headerView.frame.size.height, SCREENSIZE.width, SCREENSIZE.height-(64+headerView.frame.size.height+100)) rowMaxNum:maxX columnMaxNum:maxY noSeatArray:noSeatArr];
    [_seatView setRoomName:roomName seatNum:108];
    _seatView.delegate=self;
    [self.view addSubview:_seatView];
    
    [self getSeatList:object];
}

//所有现实存在的座位（将现实存在的座位显示出来）
-(void)getSeatList:(id)object{
    if (object && [object isKindOfClass:[NSArray class]]) {
        [_seatView setSeatStatus:object];
    }
}

#pragma mark - 选择座位 Delegate
-(void)hadSelectSeat:(NSArray *)selectSeatArr{
    hadSelectSeatArr=[NSMutableArray arrayWithArray:selectSeatArr];
    [self createSelectSeatButtonWithArray:hadSelectSeatArr];
}

-(void)createSelectSeatButtonWithArray:(NSArray *)arr{
    if (arr.count!=0) {
        seleSeatView.hidden=NO;
        tipLabel.hidden=YES;
        totaoPirceLabel.hidden=NO;
        buySumLabel.hidden=NO;
    }else{
        seleSeatView.hidden=YES;
        tipLabel.hidden=NO;
        totaoPirceLabel.hidden=YES;
        buySumLabel.hidden=YES;
    }
    
    for (UIView *view in seleSeatView.subviews) {
        if (
            [view isKindOfClass:[SelectSeatButton class]]) {
            [view removeFromSuperview];
        }
    }
    CGFloat btnX=8;
    CGFloat btnY=9;
    CGFloat btnWidth=(SCREENSIZE.width-40)/4;
    CGFloat btnHeight=32;
    param_seatIds=@"";
    for (int i=0; i<arr.count; i++) {
        SeatBtn *seatBtn=arr[i];
        SelectSeatButton *btn=[[SelectSeatButton alloc] initWithFrame:CGRectMake(btnX+(btnWidth+btnX)*i, btnY, btnWidth, btnHeight)];
        btn.backgroundColor=[UIColor whiteColor];
        btn.tag=i;
        btn.layer.cornerRadius=2;
        btn.layer.borderWidth=0.6f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"949494"].CGColor;
        btn.titleLabel.text=[NSString stringWithFormat:@"%@排%@座",seatBtn.rowNum,seatBtn.colNum];
        param_seatIds=[param_seatIds stringByAppendingString:i==arr.count-1?[NSString stringWithFormat:@"%@:%@",seatBtn.rowNum,seatBtn.colNum]:[NSString stringWithFormat:@"%@:%@|",seatBtn.rowNum,seatBtn.colNum]];
        [btn.imgv setImage:[UIImage imageNamed:@"select_seat_close"] forState:UIControlStateNormal];
        btn.seatCode=seatBtn.seatCode;
        btn.imgv.imageEdgeInsets=UIEdgeInsetsMake(0, 12, 0, 0);
        [btn addTarget:self action:@selector(cancleHadSelectSeat:) forControlEvents:UIControlEventTouchUpInside];
        [seleSeatView addSubview:btn];
    }
    //
    CGFloat priFloat=ccPrice.floatValue*arr.count;
    buySumLabel.text=[NSString stringWithFormat:@"￥%@ x %lu张",ccPrice,(unsigned long)arr.count];
    totaoPirceLabel.text=[NSString stringWithFormat:@"￥%.2f",priFloat];
}

#pragma mark - 点击已选座位（取消选择的座位）
-(void)cancleHadSelectSeat:(id)sender{
    SelectSeatButton *btn=sender;
    [hadSelectSeatArr removeObjectAtIndex:btn.tag];
    [self createSelectSeatButtonWithArray:hadSelectSeatArr];
    [_seatView setNormalSeatsStatus:btn];
    
    seleSeatView.hidden=hadSelectSeatArr.count!=0?NO:YES;
    tipLabel.hidden=hadSelectSeatArr.count!=0?YES:NO;
    totaoPirceLabel.hidden=hadSelectSeatArr.count!=0?NO:YES;
    buySumLabel.hidden=hadSelectSeatArr.count!=0?NO:YES;
}

#pragma mark - addBottomView
-(void)addBottomView{
    for (UIView *view in self.view.subviews) {
        if (
            [view isKindOfClass:[PickSeatBottomView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [hadSelectSeatArr removeAllObjects];
    
#pragma mark 购买 View
    
    UIView *buyView=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-50, SCREENSIZE.width, 50)];
    buyView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:buyView];
    
    UIButton *confirmBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame=CGRectMake(SCREENSIZE.width-96, 0, 96, 50);
    confirmBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    confirmBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [confirmBtn setTitle:@"确定选座" forState:UIControlStateNormal];
    [confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(clickConfirmSelect) forControlEvents:UIControlEventTouchUpInside];
    [buyView addSubview:confirmBtn];
    
    tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 0, 150, confirmBtn.height)];
    tipLabel.textColor=[UIColor colorWithHexString:@"949494"];
    tipLabel.text=@"一次最多选4个座位";
    tipLabel.font=[UIFont systemFontOfSize:12];
    [buyView addSubview:tipLabel];
    
    totaoPirceLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 4, SCREENSIZE.width-110, 21)];
    totaoPirceLabel.textColor=[UIColor colorWithHexString:@"fb8023"];
    totaoPirceLabel.text=@"￥0.0";
    totaoPirceLabel.font=[UIFont systemFontOfSize:18];
    totaoPirceLabel.hidden=YES;
    [buyView addSubview:totaoPirceLabel];
    
    buySumLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, 25, 110, 21)];
    buySumLabel.textColor=[UIColor colorWithHexString:@"787878"];
    buySumLabel.text=[NSString stringWithFormat:@"￥%@ x 0张",ccPrice];
    buySumLabel.font=[UIFont systemFontOfSize:14];
    buySumLabel.hidden=YES;
    [buyView addSubview:buySumLabel];
    
    [AppUtils drawZhiXian:buyView withColor:[UIColor colorWithHexString:@"c7c7c7"] height:1.0f firstPoint:CGPointMake(0, SCREENSIZE.height-50) endPoint:CGPointMake(SCREENSIZE.width, SCREENSIZE.height-50)];
    
#pragma mark 选座 View
    seleSeatView=[[PickSeatBottomView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-100, SCREENSIZE.width, 50)];
    seleSeatView.backgroundColor=[UIColor colorWithHexString:@"e4e4e4"];
    seleSeatView.hidden=YES;
    [self.view addSubview:seleSeatView];
}

#pragma mark - 点击确定选座

-(void)clickConfirmSelect{
    if (_seatView.selectedBtnArr && _seatView.selectedBtnArr.count > 0) {
        
        BOOL canBuyTicket = YES;
        
        //购票逻辑（重要：键值形式----某一行选择的座位）
        NSMutableDictionary *seleSeatArrDic = [NSMutableDictionary dictionary];
        
        //选中的排号数组 （选中的排号）
        NSMutableArray *rowX_arr = [[NSMutableArray alloc] init];
        //seatView.selectedBtnArr 已经选择的座位Array
        for (NSInteger i = 0; i < _seatView.selectedBtnArr.count; i++) {
            SeatBtn *seat = _seatView.selectedBtnArr[i];
            if (![rowX_arr containsObject:[NSString stringWithFormat:@"%ld",seat.seat_X]]) {
                [rowX_arr addObject:[NSString stringWithFormat:@"%ld",seat.seat_X]];
            }
        }
        
        for (NSString *tempX in rowX_arr) {
            NSMutableArray *multXArra = [[NSMutableArray alloc]init];
            for (NSInteger i = 0; i < _seatView.selectedBtnArr.count; i++) {
                SeatBtn *seat = _seatView.selectedBtnArr[i];
                if ([tempX integerValue] == seat.seat_X) {
                    [multXArra addObject:seat.colNum];
                }
            }
            [seleSeatArrDic setObject:multXArra forKey:tempX];
            //NSLog(@"seleSeatArrDic=%@",seleSeatArrDic);
        }
        
        
#pragma mark 第 1 步：取出每一排的数据比较
        for (NSString *rowKey in seleSeatArrDic.allKeys) {
            
            //当前选择排的全部座位（不包括隐藏的）
            NSMutableArray *allShowColArr = [[NSMutableArray alloc] init];
            for (SeatBtn *btn in _seatView.seatBtnArr) {
                if (btn.seat_X == rowKey.integerValue) {
                    if (btn.rowNum == nil || btn.colNum == nil) {
                        
                    }else{
                        [allShowColArr addObject:btn];
                    }
                }
            }
            
            //当前选择排的全部座位（包括隐藏的）
            NSMutableArray *allSeatRowArr = [NSMutableArray array];
            for (SeatBtn *btn in _seatView.seatBtnArr) {
                if (btn.seat_X == rowKey.integerValue) {
                    [allSeatRowArr addObject:btn];
                }
            }
            
            
            //得到当前排选择的所有座位号 并升序
            //然后 取出（当前排）选择的排座位的最小号和最大号  （如果只选一个，大小号都一样）
            NSArray *tempArr = [seleSeatArrDic objectForKey:rowKey];
            NSArray *seleSeatNumArr = [tempArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                NSNumber *num1 = [NSNumber numberWithInteger:[obj1 integerValue]];
                NSNumber *num2 = [NSNumber numberWithInteger:[obj2 integerValue]];
                NSComparisonResult result = [num1 compare:num2];
                return result == NSOrderedDescending;
            }];
            NSInteger min = [seleSeatNumArr[0] integerValue];
            NSInteger max = [seleSeatNumArr[seleSeatNumArr.count - 1] integerValue];
            NSLog(@"最小号=%ld---最大号=%ld",min,max);
            
            
#pragma mark 第 2 步：判断当前排座位是否有隐藏的
            NSMutableArray *edgeArr = [NSMutableArray arrayWithArray:seleSeatNumArr];//边界数组
            
#pragma mark  没有没有没有没有没有没有 隐藏的排
            if (allShowColArr.count == allSeatRowArr.count) {
                //找选择点后的最小点
                for (NSInteger i = min - 1; i >=0; i--) {
                    if (i != 0) {
                        __block SeatBtn *seatBtnLeft;
                        [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            SeatBtn *btn = allSeatRowArr[idx];
                            if (btn.colNum.integerValue == i) {
                                seatBtnLeft = btn;
                                *stop = YES;
                            }
                        }];
                        if (seatBtnLeft != nil) {
                            if ([seatBtnLeft.rowNum isEqualToString:@""] || [seatBtnLeft.colNum isEqualToString:@""] || seatBtnLeft.seatStatus == SOLD || seatBtnLeft.rowNum == nil || seatBtnLeft.colNum == nil || seatBtnLeft.isHidden) {
                                [edgeArr addObject:[NSString stringWithFormat:@"%ld",i]];
                                break;
                            }
                        }
                    }else{
                        __block NSInteger edCount=0;
                        [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            SeatBtn *btn = allSeatRowArr[idx];
                            if (btn.hidden) {
                                edCount=btn.seat_Y;
                                *stop = YES;
                            }
                        }];
                        [edgeArr addObject:@"0"];
                        break;
                    }
                }
                NSLog(@"没有隐藏的排%@",edgeArr);
                //找选择点后的最大点
                for (NSInteger i = max + 1; i <= allShowColArr.count + 1; i++) {
                    if (i <= allShowColArr.count) {
                        __block SeatBtn *seatBtnRight;
                        [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            SeatBtn *btn = (SeatBtn *)allSeatRowArr[idx];
                            if (btn.colNum.integerValue == i) {
                                seatBtnRight = btn;
                                *stop = YES;
                            }
                        }];
                        if (seatBtnRight != nil) {
                            if ([seatBtnRight.rowNum isEqualToString:@""] || [seatBtnRight.colNum isEqualToString:@""] || seatBtnRight.rowNum == nil || seatBtnRight.colNum == nil || seatBtnRight.seatStatus == SOLD || seatBtnRight.isHidden) {
                                [edgeArr addObject:[NSString stringWithFormat:@"%ld",i]];
                                break;
                            }
                        }
                    }else{
                        __block NSInteger edCount=0;
                        [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            SeatBtn *btn = allSeatRowArr[idx];
                            if (btn.isHidden) {
                                edCount=btn.seat_Y;
                                *stop = YES;
                            }
                        }];
                        [edgeArr addObject:[NSString stringWithFormat:@"%ld",(long)allShowColArr.count + 1]];
                        break;
                    }
                }
                NSLog(@"没有隐藏的排%@",edgeArr);
            }
            
#pragma mark 有有有有有有有有有有有 隐藏的排
            else{
                BOOL isNiXu = false;        //是否逆序
                for (int i=0; i<allShowColArr.count; i++) {
                    if (i+1<allShowColArr.count) {
                        SeatBtn *btn1=allShowColArr[i];
                        SeatBtn *btn2=allShowColArr[i+1];
                        if (btn1.colNum.integerValue<btn2.colNum.integerValue) {
                            isNiXu=NO;
                            break;
                        }else{
                            isNiXu=YES;
                            break;
                        }
                    }
                }
                NSLog(@"没有隐藏的排");
                //
#pragma mark 找选择点后的最左边 min
                
                __block  NSInteger minParamY=0;
                [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    SeatBtn *btn = allSeatRowArr[idx];
                    NSInteger intCol=btn.colNum.intValue;
                    NSString *newStr=[NSString stringWithFormat:@"%ld",intCol];
                    if ([newStr isEqualToString:isNiXu?[NSString stringWithFormat:@"%ld",max]:[NSString stringWithFormat:@"%ld",min] ]) {
                        minParamY=btn.seat_Y;
                        *stop = YES;
                    }
                }];
                
                for (NSInteger ii = minParamY - 1; ii >=0; ii--) {
                    __block SeatBtn *seatBtn_Left;
                    [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        SeatBtn *btn = allSeatRowArr[idx];
                        if (btn.seat_Y == ii) {
                            seatBtn_Left = btn;
                            *stop = YES;
                        }
                    }];
                    if (ii != 0) {
                        //判断小于当前座位的号是否有隐藏或已经选座的
                        if (seatBtn_Left != nil) {
                            if ([seatBtn_Left.rowNum isEqualToString:@""] || [seatBtn_Left.colNum isEqualToString:@""] || seatBtn_Left.seatStatus == SOLD || seatBtn_Left.rowNum == nil || seatBtn_Left.colNum == nil || seatBtn_Left.isHidden) {
                                [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    SeatBtn *btn111 = allSeatRowArr[idx];
                                    if (isNiXu) {
                                        if (btn111.seat_Y == seatBtn_Left.seat_Y+1) {
                                            NSInteger btnInt=btn111.colNum.integerValue+1;
                                            [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                            *stop = YES;
                                        }
                                    }else{
                                        if (btn111.seat_Y == seatBtn_Left.seat_Y+1) {
                                            NSInteger btnInt=btn111.colNum.integerValue-1;
                                            [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                            *stop = YES;
                                        }
                                    }
                                }];
                                break;
                            }
                        }
                    }else{
                        if (isNiXu) {
                            [edgeArr addObject:[NSString stringWithFormat:@"%ld",(long)allShowColArr.count + 1]];
                        }else{
                            [edgeArr addObject:[NSString stringWithFormat:@"0"]];
                        }
                        break;
                    }
                }
                NSLog(@"\n有隐藏找左边界edgeArr=%@  minParamY=%ld",edgeArr,minParamY);
                
                //
#pragma mark 找选择点后的最右边 max
                
                __block  NSInteger maxParamY=0;
                [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    SeatBtn *btn = allSeatRowArr[idx];
                    NSInteger intCol=btn.colNum.intValue;
                    NSString *newStr=[NSString stringWithFormat:@"%ld",intCol];
                    if ([newStr isEqualToString:isNiXu?[NSString stringWithFormat:@"%ld",min]:[NSString stringWithFormat:@"%ld",max] ]) {
                        maxParamY=btn.seat_Y;
                        *stop = YES;
                    }
                    /*
                    if ([btn.colNum isEqualToString:isNiXu?[NSString stringWithFormat:@"%ld",min]:[NSString stringWithFormat:@"%ld",max]]) {
                        maxParamY=btn.seat_Y;
                        *stop = YES;
                    }*/
                     
                     
                }];//选择最大点对应的座位 Y
                NSLog(@"maxParamY=%ld",maxParamY);
                
                for (NSInteger jjj = maxParamY + 1; jjj <= allSeatRowArr.count + 1; jjj++) {
                    __block SeatBtn *seatBtn_Right;
                    [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        SeatBtn *btn = (SeatBtn *)allSeatRowArr[idx];
                        if (btn.seat_Y == jjj) {
                            seatBtn_Right = btn;
                            *stop = YES;
                        }
                    }];
                    
                    if (jjj==allSeatRowArr.count+1) {
                        //NSLog(@"逆序逆序逆序逆序逆序逆序逆序逆序逆序逆序");
                        [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            SeatBtn *btn111 = allSeatRowArr[idx];
                            if (isNiXu) {
                                if (btn111.seat_Y == allSeatRowArr.count) {
                                    NSInteger btnInt=btn111.colNum.integerValue-1;
                                    [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                    *stop = YES;
                                }
                            }else{
                                if (btn111.seat_Y == allSeatRowArr.count) {
                                    NSInteger btnInt=btn111.colNum.integerValue+1;
                                    [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                    *stop = YES;
                                }
                            }
                        }];
                        break;
                    }else{
                        if (seatBtn_Right != nil) {
                            if ([seatBtn_Right.rowNum isEqualToString:@""] || [seatBtn_Right.colNum isEqualToString:@""] || seatBtn_Right.rowNum == nil || seatBtn_Right.colNum == nil || seatBtn_Right.seatStatus == SOLD || seatBtn_Right.isHidden) {
                                [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    SeatBtn *btn111 = allSeatRowArr[idx];
                                    if (isNiXu) {
                                        if (btn111.seat_Y == seatBtn_Right.seat_Y-1) {
                                            NSInteger btnInt=btn111.colNum.integerValue-1;
                                            [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                            *stop = YES;
                                        }
                                    }else{
                                        if (btn111.seat_Y == seatBtn_Right.seat_Y-1) {
                                            NSInteger btnInt=btn111.colNum.integerValue+1;
                                            [edgeArr addObject:[NSString stringWithFormat:@"%ld",btnInt]];
                                            *stop = YES;
                                        }
                                    }
                                }];
                                break;
                            }
                        }
                    }
                }
                NSLog(@"\n有 隐藏找右边界edgeArr=%@",edgeArr);
            }
            
            //找中间边界
            for (int j = 0; j < seleSeatNumArr.count - 1; j ++) {
                NSInteger cur = [seleSeatNumArr[j] integerValue];
                NSInteger next = [seleSeatNumArr[j + 1] integerValue];
                
                NSInteger left = -1;
                NSInteger right = -1;
                //中间 的  左边界
                for (NSInteger k = cur + 1; k < next; k ++) {
                    
                    __block SeatBtn *seatBtnMiddle_left;
                    [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        SeatBtn *btn = (SeatBtn *)allSeatRowArr[idx];
                        if ([btn.colNum integerValue] == k) {
                            seatBtnMiddle_left = btn;
                            *stop = YES;
                        }
                    }];
                    if (seatBtnMiddle_left != nil) {
                        if ([seatBtnMiddle_left.rowNum isEqualToString:@""] || [seatBtnMiddle_left.colNum isEqualToString:@""] || seatBtnMiddle_left.seatStatus == SOLD || seatBtnMiddle_left.rowNum == nil || seatBtnMiddle_left.colNum == nil || seatBtnMiddle_left.isHidden) {
                            left = k;
                            break;
                        }
                    }
                }
                
                //中间 的  右边界
                for (NSInteger l = next - 1; l > cur; l --) {
                    __block SeatBtn *seatBtnMiddle_right;
                    [allSeatRowArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        SeatBtn *btn = (SeatBtn *)allSeatRowArr[idx];
                        if ([btn.colNum integerValue] == l) {
                            seatBtnMiddle_right = btn;
                            *stop = YES;
                        }
                    }];
                    if (seatBtnMiddle_right != nil) {
                        if ([seatBtnMiddle_right.rowNum isEqualToString:@""] || [seatBtnMiddle_right.colNum isEqualToString:@""] || seatBtnMiddle_right.seatStatus == SOLD || seatBtnMiddle_right.rowNum == nil || seatBtnMiddle_right.colNum == nil || seatBtnMiddle_right.isHidden) {
                            right = l;
                            break;
                        }
                    }
                }
                
                if (left != -1 ) {
                    [edgeArr addObject:[NSString stringWithFormat:@"%ld",left]];
                }
                
                if (right != -1 && left != right) {
                    [edgeArr addObject:[NSString stringWithFormat:@"%ld",right]];
                }
            }
            //NSLog(@"\n中间边界=%@",edgeArr);
            
            //排序
            NSArray *newEdgeArr = [edgeArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                
                NSNumber *number1 = [NSNumber numberWithInteger:[obj1 integerValue]];
                NSNumber *number2 = [NSNumber numberWithInteger:[obj2 integerValue]];
                
                NSComparisonResult result = [number1 compare:number2];
                
                return result == NSOrderedDescending; // 升序
            }];
            //NSLog(@"\n中间边界排序=%@",newEdgeArr);
            
            BOOL isConnect = NO;
            BOOL isAlone = NO;
            BOOL isPoint = NO;
            
            NSInteger left_first = [newEdgeArr[0] integerValue];//第一个节点，只记录，不清算
            NSInteger right_first = [newEdgeArr[1] integerValue];
            
            if(right_first - left_first == 2){ //如果相隔一个座位
                isAlone = YES;
            }
            
            if(right_first - left_first == 1 && [seleSeatNumArr containsObject:[NSString stringWithFormat:@"%ld",(long)left_first]] != [seleSeatNumArr containsObject:[NSString stringWithFormat:@"%ld",(long)right_first]]){
                isConnect = YES;
            }
            
            for (NSInteger i = 1; i < newEdgeArr.count - 1; i ++) {
                
                NSInteger left = [newEdgeArr[i] integerValue];
                NSInteger right = [newEdgeArr[i + 1] integerValue];
                
                BOOL isLeftSelf = [seleSeatNumArr containsObject:[NSString stringWithFormat:@"%ld",(long)left]];//左边是否是自己选择的座位
                BOOL isRightSelf = [seleSeatNumArr containsObject:[NSString stringWithFormat:@"%ld",(long)right]];//右边是否是自己选择的座位
                
                if (isLeftSelf != isRightSelf || right - left > 1) {
                    isPoint = YES;
                }else{
                    isPoint = NO;
                }
                
                if(isPoint){//从第二个节点开始
                    if(right - left == 2){
                        isAlone |= YES;
                    }
                    
                    if(right - left == 1 && isLeftSelf != isRightSelf){
                        isConnect |= YES;
                    }
                    
                    if(!isConnect && isAlone){//清算前两个节点
                        canBuyTicket = NO;
                    }
                    
                    isConnect = NO;//重置为初始值
                    isAlone = NO;
                    
                    if(!canBuyTicket) break;
                    
                    //重新记录，作为下一次清算前的记录 (下次循环用到)
                    if(right - left == 2){
                        isAlone = YES;
                    }
                    if(right - left == 1 && isLeftSelf != isRightSelf){
                        isConnect = YES;
                    }
                }
            }
        }
        
        NSLog(@"canBuyTicket=%d",canBuyTicket);
        
        if(!canBuyTicket) {
            [AppUtils showSuccessMessage:@"请不要留下单个座位" inView:self.view];
            return;
        }else{
            if (![AppUtils loginState]) {
                //跳到登录页面
                WelcomeController *login=[[WelcomeController alloc]init];
                login.isGoBuyMovie=YES;
                UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
                [loginNav.navigationBar setShadowImage:[UIImage new]];
                //隐藏tabbar
                login.hidesBottomBarWhenPushed = YES;
                [self presentViewController:loginNav animated:YES completion:nil];
            }else{
#pragma mark 可以提交
                [self confirmRequest];
            }
        }
    }else{
        [AppUtils showSuccessMessage:@"请选择座位" inView:self.view];
    }
}


#pragma mark - request

-(void)activeDataRequest{
    [AppUtils showProgressMessage:@"正在获取座位信息…" inView:self.view];
    //
//    time_t now;
//    time(&now);
//    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
//    NSString *nonce_str	= [YXWSign randomNumber];
//    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//    [dict setObject:@"1" forKey:@"is_iso"];
//    [dict setObject:@"m_movie" forKey:@"mod"];
//    [dict setObject:@"activity" forKey:@"code"];
//    [dict setObject:_gpModel.foretellId forKey:@"foretellId"];
//    [dict setObject:time_stamp forKey:@"stamp"];
//    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    //
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"activity",
                         @"foretellId":_gpModel.foretellId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                id noSeatObj=responseObject[@"retData"][@"seat_list"];
                if ([noSeatObj isKindOfClass:[NSArray class]]) {
                    NSArray *arr=noSeatObj;
                    if (arr.count!=0) {
                        
                        [self addHeaderView];
                        
                        navTitleStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"movie_name"]];
                        [titleLabel setTitle:navTitleStr forState:UIControlStateNormal];
                        cinemaLabel.text=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"cinemaName"]];
                        playTimeLabel.text=[NSString stringWithFormat:@"%@（%@）",responseObject[@"retData"][@"info"][@"play_time"],responseObject[@"retData"][@"info"][@"dimensional"]];
                        
                        //电影单价
                        ccPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"price"]];
                        
                        //
                        NSInteger maxRow=[responseObject[@"retData"][@"info"][@"rowNum"] integerValue];
                        NSInteger maxColumn=[responseObject[@"retData"][@"info"][@"columnNum"] integerValue];
                        NSString *room=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"hallName"]];
                        [self addSeatViewByMaxX:maxRow MaxY:maxColumn withObject:responseObject[@"retData"][@"seat_list"] roomName:room noSeatArr:responseObject[@"retData"][@"new_seat_tmp"]];
                        [self addBottomView];
                    }else{
                        
                        [[NetWorkRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"http://www.youxia.com/mgo/index.php?mod=mz_data&code=do_paiqi_one&cinemaid=%@",_gpModel.cinemaId] method:HttpRequestPost parameters:nil prepareExecute:^{
                            
                        } success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                            
                        }];
                        
                        //
                        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"获取座位信息失败！"preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        [alertController addAction:yesAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

-(void)dataRequest{
    [AppUtils showProgressMessage:@"正在获取座位信息…" inView:self.view];
    //
//    time_t now;
//    time(&now);
//    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
//    NSString *nonce_str	= [YXWSign randomNumber];
//    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//    [dict setObject:@"1" forKey:@"is_iso"];
//    [dict setObject:@"m_movie" forKey:@"mod"];
//    [dict setObject:@"movie_seat_api" forKey:@"code"];
//    [dict setObject:_gpModel.foretellId forKey:@"foretellId"];
//    [dict setObject:time_stamp forKey:@"stamp"];
//    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
    //
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"movie_seat_api",
                         @"foretellId":_gpModel.foretellId
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            id retObj=responseObject[@"retData"];
            if ([retObj isKindOfClass:[NSDictionary class]]) {
                id noSeatObj=responseObject[@"retData"][@"seat_list"];
                if ([noSeatObj isKindOfClass:[NSArray class]]) {
                    NSArray *arr=noSeatObj;
                    if (arr.count!=0) {
                        
                        [self addHeaderView];
                        
                        navTitleStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"movie_name"]];
                        [titleLabel setTitle:navTitleStr forState:UIControlStateNormal];
                        cinemaLabel.text=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"cinemaName"]];
                        playTimeLabel.text=[NSString stringWithFormat:@"%@（%@）",responseObject[@"retData"][@"info"][@"play_time"],responseObject[@"retData"][@"info"][@"dimensional"]];
                        
                        //电影单价
                        ccPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"price"]];
                        
                        //
                        NSInteger maxRow=[responseObject[@"retData"][@"info"][@"rowNum"] integerValue];
                        NSInteger maxColumn=[responseObject[@"retData"][@"info"][@"columnNum"] integerValue];
                        NSString *room=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"info"][@"hallName"]];
                        [self addSeatViewByMaxX:maxRow MaxY:maxColumn withObject:responseObject[@"retData"][@"seat_list"] roomName:room noSeatArr:responseObject[@"retData"][@"new_seat_tmp"]];
                        [self addBottomView];
                    }else{
                        
                        [[NetWorkRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"http://www.youxia.com/mgo/index.php?mod=mz_data&code=do_paiqi_one&cinemaid=%@",_gpModel.cinemaId] method:HttpRequestPost parameters:nil prepareExecute:^{
                            
                        } success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                            
                        }];
                        
                        //
                        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"获取座位信息失败！"preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        [alertController addAction:yesAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//确认选座
-(void)confirmRequest{
    [AppUtils showProgressMessage:@"正在为你订票……" inView:self.view];
    NSDictionary *dict=@{
                         @"is_iso":@"1",
                         @"mod":@"m_movie",
                         @"code":@"get_mz_seat_api",
                         @"userid":[AppUtils getValueWithKey:User_ID],
                         @"foretellId":_gpModel.foretellId,
                         @"seatIds":param_seatIds,
                         @"count":[NSString stringWithFormat:@"%ld",hadSelectSeatArr.count],
                         @"price":ccPrice,
                         @"movie_name":navTitleStr,
                         @"is_active":_isActive?@"1":@"0"
                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *oId=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"orderid"]];
            FilmConfirmOrder *ctr=[FilmConfirmOrder new];
            ctr.orderId=oId;
            ctr.isActive=_isActive;
            [self.navigationController pushViewController:ctr animated:YES];
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
