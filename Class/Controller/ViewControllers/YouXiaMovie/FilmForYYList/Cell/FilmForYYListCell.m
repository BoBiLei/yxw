//
//  FilmForYYListCell.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmForYYListCell.h"

@implementation FilmForYYListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tipLabel.layer.cornerRadius=3;
    self.tipLabel.layer.masksToBounds=YES;
    self.tipLabel.layer.borderWidth=0.8f;
    self.tipLabel.layer.borderColor=[UIColor colorWithHexString:@"55b2f0"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(CinemaModel *)model{
    self.titleLabel.text=model.cinemaName;
    self.addressLabel.text=model.address;
    self.tipLabel.text=model.dimensional[0];
    self.priceLabel.text=[NSString stringWithFormat:@"￥%@起",model.min_price];
    
    NSArray *timeArr=model.showTime;
    NSString *timeStr=@"";
    for (int i=0; i<timeArr.count; i++) {
        if (i<3) {
            timeStr=[timeStr stringByAppendingString:i==2?[NSString stringWithFormat:@"%@",timeArr[i]]:[NSString stringWithFormat:@"%@ | ",timeArr[i]]];
        }
        
        if (timeArr.count==1) {
            timeStr=[NSString stringWithFormat:@"%@",timeArr[i]];
        }
    }
    self.timeLabel.text=[NSString stringWithFormat:@"近期场次 %@",timeStr];
}

@end
