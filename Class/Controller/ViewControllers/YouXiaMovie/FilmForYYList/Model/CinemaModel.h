//
//  CinemaModel.h
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CinemaModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *cinemaId;
@property (nonatomic, copy) NSString *cinemaName;
@property (nonatomic, retain) NSArray *dimensional;
@property (nonatomic, copy) NSString *min_price;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, retain) NSArray *showTime;

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

@end
