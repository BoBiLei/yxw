//
//  CinemaModel.m
//  youxia
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CinemaModel.h"

@implementation CinemaModel

-(void)jsonDataForDictioanry:(NSDictionary *)dic{
    self.address=[NSString stringWithFormat:@"%@",dic[@"address"]];
    self.cinemaId=[NSString stringWithFormat:@"%@",dic[@"cinemaId"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.dimensional=dic[@"dimensional"];
    self.min_price=[NSString stringWithFormat:@"%@",dic[@"min_price"]];
    self.region=[NSString stringWithFormat:@"%@",dic[@"region"]];
    self.showTime=dic[@"showTime"];
}

@end
