//
//  FilmForYYListController.h
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModel.h"

@interface FilmForYYListController : UIViewController

@property (nonatomic, strong) FilmModel *model;

@end
