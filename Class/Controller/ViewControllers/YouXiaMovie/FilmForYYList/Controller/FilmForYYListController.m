//
//  FilmForYYListController.m
//  youxia
//
//  Created by mac on 2016/11/23.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmForYYListController.h"
#import "HMSegmentedControl.h"
#import "FilmForYYListCell.h"
#import "YingYuanDetailController.h"
#import "SearchFilmNameController.h"
#import "FilmAreaView.h"
#import "CinemaModel.h"
#import "FilmNameSearCinemaController.h"

@interface FilmForYYListController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation FilmForYYListController{
    BOOL isFirstRequest;
    UIView *mainView;
    NSMutableArray *titleArr;               //日期
    NSMutableArray *dateArr;                //日期
    NSMutableArray *sourceArr;
    MJRefreshNormalHeader *refleshHeader;
    UITableView *myTable;
    FilmAreaView *filmAreaView;
    
    NSString *regionStr;                    //选择的区域
    NSString *currentRegion;                //当前地区
    
    BOOL areaFirst;
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    mainView=[[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    [self.view addSubview:mainView];
    
    [self addNavigateView];
    
    [self dataRequest];
    
    [self setUpTableView];
    
    [self areaFirstRequest];
}

-(void)addNavigateView{
    
    areaFirst=YES;
    
    titleArr=[NSMutableArray array];
    
    dateArr=[NSMutableArray array];
    
    sourceArr=[NSMutableArray array];
    
    regionStr=@"";
    
    currentRegion=@"全部";
    
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
#pragma mark UISegmentedControl
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(16, 20, 64, 44);
    backBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [backBtn setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn addTarget:self action:@selector(clickBackBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
#pragma mark Title
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2, 44)];
    title.center=CGPointMake(SCREENSIZE.width/2, 42);
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.textAlignment=NSTextAlignmentCenter;
    title.text=_model.title;
    [self.view addSubview:title];
    
    
#pragma mark Search Btn
    UIButton *searchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame=CGRectMake(SCREENSIZE.width-48, 20, 44, 44);
    [searchBtn setImage:[UIImage imageNamed:@"film_searimg"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(clickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:searchBtn];
    
#pragma mark Fillter Btn
    UIButton *fillterBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    fillterBtn.frame=CGRectMake(SCREENSIZE.width-92, 20, 44, 44);
    [fillterBtn setImage:[UIImage imageNamed:@"film_filterimg"] forState:UIControlStateNormal];
    [fillterBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [fillterBtn addTarget:self action:@selector(clickFilterBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fillterBtn];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"dcdcdc"] height:0.9f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}

#pragma mark - 导航栏按钮点击事件
-(void)clickBackBtn{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickFilterBtn{
    [filmAreaView showFilmAreaViewCompletion:^(id object) {
        NSString *backStr=(NSString *)object;
        if(![currentRegion isEqualToString:backStr]){
            sourceArr=[NSMutableArray array];
            regionStr=backStr;
            [self dataRequest];
        }
        currentRegion=backStr;
    }];
}

-(void)clickSearchBtn{
    FilmNameSearCinemaController *searCtr=[[FilmNameSearCinemaController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    searCtr.regionStr=regionStr;
    searCtr.model=_model;
    [self.navigationController pushViewController:searCtr animated:YES];
}

#pragma mark - setup TableView
-(void)setUpTableView{
    self.automaticallyAdjustsScrollViewInsets = NO; //解决表无故偏移
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, mainView.width, mainView.height) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"FilmForYYListCell" bundle:nil] forCellReuseIdentifier:@"FilmForYYListCell"];
    [mainView addSubview:myTable];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            sourceArr=[NSMutableArray array];
            [self dataRequest];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    myTable.mj_header = refleshHeader;
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return sourceArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 96;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 24;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FilmForYYListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmForYYListCell"];
    if (!cell) {
        cell=[[FilmForYYListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmForYYListCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    CinemaModel *model=sourceArr[indexPath.row];
    [cell reflushDataForModel:model];
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CinemaModel *cmodel=sourceArr[indexPath.row];
    YingYuanDetailController *ctr=[YingYuanDetailController new];
    ctr.filmModel=_model;
    ctr.cinemaId=cmodel.cinemaId;
    ctr.type=1;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - request

-(void)dataRequest{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        if (!isFirstRequest) {
            [AppUtils showProgressInView:self.view];
        }
        NSDictionary *dict=@{
                             @"is_iso":@"1",
                             @"mod":@"m_movie",
                             @"code":@"cinema_foretells_list",
                             @"movie_id":_model.fId,
                             @"cinema_name":@"",
                             @"city_id":ccId,
                             @"region":[regionStr isEqualToString:@"全部"]?@"":regionStr
                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [AppUtils dismissHUDInView:self.view];
            [refleshHeader endRefreshing];
            DSLog(@"%@",responseObject);
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id paiqiObj=responseObject[@"retData"][@"movie_paiqi"];
                if ([paiqiObj isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *dic in paiqiObj) {
                        CinemaModel *model=[CinemaModel new];
                        [model jsonDataForDictioanry:dic];
                        [sourceArr addObject:model];
                    }
                    if (sourceArr.count!=0) {
                        [myTable reloadData];
                    }else{
                        
                        //
                        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"获取影院信息失败！"preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        [alertController addAction:yesAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [AppUtils dismissHUDInView:self.view];
            [refleshHeader endRefreshing];
            DSLog(@"%@",error);
        }];
        isFirstRequest=YES;
    }
}

-(void)areaRequest{
    NSString *ccId=[AppUtils getValueWithKey:Cinema_City_Id];
    if (ccId!=nil) {
        NSDictionary *dict=@{
                             @"is_iso":@"1",
                             @"mod":@"m_movie",
                             @"code":@"get_area_list",
                             @"city_id":ccId
                             };
        [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                filmAreaView=[[FilmAreaView alloc] initInView:mainView andViewType:1];
                id arrObj=responseObject[@"retData"];
                NSMutableArray *daArr=[NSMutableArray array];
                if ([arrObj isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *dic in arrObj) {
                        NSString *str=dic[@"region"];
                        [daArr addObject:str];
                    }
                    [daArr insertObject:@"全部" atIndex:0];
                }
                filmAreaView.dataArr=daArr;
            }
            DSLog(@"areaRequest=\n%@",responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

-(void)areaFirstRequest{
    if (areaFirst) {
        [self areaRequest];
        areaFirst = NO;
    }
}

@end
