//
//  FilmTimeLabel.m
//  youxia
//
//  Created by mac on 2016/12/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmTimeLabel.h"

@interface FilmTimeLabel ()

@property (nonatomic, strong)NSTimer *timer;

@end

@implementation FilmTimeLabel

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)timeHeadle{
    self.second--;
    if (self.second==-1) {
        self.second=59;
        self.minute--;
        if (self.minute==-1) {
            self.minute=59;
            //self.hour--;
        }
    }
//    self.text = [NSString stringWithFormat:@"请在%ld时%ld分%ld秒内完成付款，晚了座位就没了哦",(long)self.hour,(long)self.minute,(long)self.second];
//    if (self.second==0 && self.minute==0 && self.hour==0) {
//        [self.timer invalidate];
//        self.timer = nil;
//    }
    self.text = [NSString stringWithFormat:@"请在%ld分%ld秒内完成付款，晚了座位就没了哦",(long)self.minute,(long)self.second];
    if (self.second==0 && self.minute==0) {
        [self.delegate isEndTime];
        [self.timer invalidate];
        self.timer = nil;
    }
    
}

@end
