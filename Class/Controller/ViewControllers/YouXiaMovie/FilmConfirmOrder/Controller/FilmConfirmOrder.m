//
//  FilmConfirmOrder.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmOrder.h"
#import "FilmConfirmHeaderCell.h"
#import "FilmConfirmCouponCell.h"
#import "FilmConfirmPhoneCell.h"
#import "FilmConfirmPayTypeCell.h"
#import "FilmConfirmGpxzCell.h"
#import "SelectFilmCouponController.h"
#import "WaitTourController.h"
#import "TourSuccessController.h"
#import "FilmTimeLabel.h"
#import "FilmConfirmModel.h"
#import "SelectFilmCouponModel.h"
#import "WXApi.h"
#import "FilmWeiXinPayModel.h"
#import "FilmConfirmTipiew.h"
#import <AlipaySDK/AlipaySDK.h>
#import "DataSigner.h"
#import "FilmListController.h"
#import "PickingSeatController.h"
#import "FilmTicketDetail.h"
#import "FilmTicketCloseController.h"

@interface FilmConfirmOrder ()<UITableViewDataSource,UITableViewDelegate,FilmTimeDelegate>
    
    @end

@implementation FilmConfirmOrder{
    
    FilmConfirmModel *sourceModel;
    
//    SelectFilmCouponController *filmCouponCtr;
    
    SelectFilmCouponModel *seleCouponModel;
    
    UITableView *myTable;
    
    NSString *timeMinute;
    NSString *timeSecond;
    
    UIButton *payPriceBtn;
    
    NSString *subPayType;   //(8-微信支付 3-支付宝 12-优惠券)
    
    NSIndexPath *selectedIndexPath;
    
    CGFloat totalPrice;
    CGFloat payPrice;
    BOOL   is_YuE;
    NSString * totalPayPrice;
}
    
-(void)viewDidAppear:(BOOL)animated{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    is_YuE = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //支付监听从后台进入前台的通知（重要哦）
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxSender:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotifacation:) name:FilmOrder_SelectCoupon_Noti object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addNavigateView];
    
    [self setUI];
    
    [self setUpBottomView];
    
    [self firstDataRequest];
}
    
#pragma mark - NSNotification Method
    
-(void)updateNotifacation:(NSNotification *)noti{
    seleCouponModel=noti.object;
    NSLog(@"%@]]]",seleCouponModel.cid);
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
    [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    
    if ([seleCouponModel.cid isEqualToString:@"0"]) {
        payPrice=sourceModel.movie_price.floatValue*sourceModel.position.count;
        [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
    }else{
        if (sourceModel.position.count>1) {
            payPrice=sourceModel.movie_price.floatValue*(sourceModel.position.count-1);
            [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
        }else{
            //优惠券支付
            subPayType=@"12";
            payPrice=0.0;
            [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
        }
    }
}
    
-(void)addNavigateView{
    UIImageView *view=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.userInteractionEnabled=YES;
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame=CGRectMake(16, 20, 64, 44);
    backBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [backBtn setImage:[UIImage imageNamed:@"film_navback"] forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2, 44)];
    title.center=CGPointMake(SCREENSIZE.width/2, 42);
    title.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    title.textAlignment=NSTextAlignmentCenter;
    title.text=@"确认订单";
    [self.view addSubview:title];
    
    [AppUtils drawZhiXian:self.view withColor:[UIColor colorWithHexString:@"dcdcdc"] height:0.9f firstPoint:CGPointMake(0, 64) endPoint:CGPointMake(SCREENSIZE.width, 64)];
}
    
#pragma mark - Nav 点击返回按钮
-(void)turnBack{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[FilmListController class]]) {
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:FilmOrder_CancelNotifacation object:nil];
        }else if ([controller isKindOfClass:[PickingSeatController class]]) {
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"确定不要刚才选择的座位了吗？"preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                
            }];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                [self cancelOrderRequest];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}
    
-(void)initTipView{
    //（进入后台还能执行 定时器的方法）
    UIApplication*   app = [UIApplication sharedApplication];
    __block    UIBackgroundTaskIdentifier bgTask;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid){
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid){
                bgTask = UIBackgroundTaskInvalid;
                
                for (UIView *view in self.view.subviews) {
                    if ([view isKindOfClass:[FilmConfirmTipiew class]]) {
                        [view removeFromSuperview];
                    }
                }
                
                FilmConfirmTipiew *tipView=[[FilmConfirmTipiew alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 26)];
                tipView.backgroundColor=[UIColor colorWithHexString:@"ffffcc"];
                [self.view addSubview:tipView];
                
                FilmTimeLabel *label=[[FilmTimeLabel alloc] initWithFrame:CGRectMake(10, 0, tipView.width-20, tipView.height)];
                label.font=[UIFont systemFontOfSize:13];
                label.textColor=[UIColor colorWithHexString:@"fb8023"];
                label.minute=timeMinute.integerValue;
                label.second=timeSecond.integerValue;
                
                label.delegate=self;
                [tipView addSubview:label];
                
                //[AppUtils drawZhiXian:tipView withColor:[UIColor colorWithHexString:@"c7c7c7"] height:0.5f firstPoint:CGPointMake(0, tipView.height+0.5f) endPoint:CGPointMake(tipView.width, tipView.height+0.5f)];
            }
        });
    });
}
    
-(void)setUI{
    
    
    
    seleCouponModel=[SelectFilmCouponModel new];
    seleCouponModel.cid=@"0";
    seleCouponModel.price=@"0";
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 90, SCREENSIZE.width, SCREENSIZE.height-169) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f6f6f6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"FilmConfirmHeaderCell" bundle:nil] forCellReuseIdentifier:@"FilmConfirmHeaderCell"];
    [myTable registerNib:[UINib nibWithNibName:@"FilmConfirmCouponCell" bundle:nil] forCellReuseIdentifier:@"FilmConfirmCouponCell"];
    [myTable registerNib:[UINib nibWithNibName:@"FilmConfirmPhoneCell" bundle:nil] forCellReuseIdentifier:@"FilmConfirmPhoneCell"];
    [myTable registerNib:[UINib nibWithNibName:@"FilmConfirmGpxzCell" bundle:nil] forCellReuseIdentifier:@"FilmConfirmGpxzCell"];
    myTable.hidden=YES;
    [self.view addSubview:myTable];
    
    
}
    
-(void)setUpBottomView{
    UIView *btmView=[[UIView alloc] initWithFrame:CGRectMake(0, SCREENSIZE.height-79, SCREENSIZE.width, 77)];
    [self.view addSubview:btmView];
    
    UILabel *tipLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, btmView.width, 30)];
    tipLabel.backgroundColor=[UIColor colorWithHexString:@"e4e4e4"];
    [btmView addSubview:tipLabel];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.textColor=[UIColor colorWithHexString:@"1a1a1a"];
    tipLabel.text=@"本影院不可退票";
    tipLabel.font=[UIFont systemFontOfSize:13];
    
    payPriceBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    payPriceBtn.frame=CGRectMake(0, 30, SCREENSIZE.width, 49);
    payPriceBtn.backgroundColor=[UIColor colorWithHexString:@"55b2f0"];
    payPriceBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [payPriceBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [payPriceBtn addTarget:self action:@selector(clickConfirmPay) forControlEvents:UIControlEventTouchUpInside];
    [btmView addSubview:payPriceBtn];
}
    
#pragma mark - 点击确认支付
-(void)clickConfirmPay{
    if ([sourceModel.user_phone isEqualToString:@""]||sourceModel.user_phone==nil) {
        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:nil message:@"手机号码不能为空"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
            
        }];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else if (![AppUtils checkPhoneNumber:sourceModel.user_phone]){
        UIAlertController*alertController = [UIAlertController alertControllerWithTitle:nil message:@"您请输入的手机号码有误"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
            
        }];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        switch (subPayType.intValue) {
            case 8:
            [self weixinPayRequest];
            break;
            case 3:
            [self aliyPayRequest];
            break;
            case 12:
            [self couponPayRequest];
            break;
            default:
            break;
        }
    }
}
    
#pragma mark - Table Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _isActive?4:5;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_isActive) {
        if (section==2) {
            return 2;
        }
        return 1;
    }else if(section==1){
        return 3;
    }
    
    else{
        if (section==3) {
            return 2;
        }
        return 1;
    }
    
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isActive) {
        if(indexPath.section==0){
            return 156;
        }else if (indexPath.section==3){
            return 144;
        }
        return 48;
    }else{
        if(indexPath.section==0){
            return 156;
        }else if (indexPath.section==4){
            return 144;
        }
        return 48;
    }
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_isActive) {
        if (section==0) {
            return 0.000000001f;
        }else if (section==1){
            return 14;
        }
        return 30;
    }else{
        if (section==0) {
            return 0.000000001f;
        }else if (section==1||section==2){
            return 14;
        }
        return 30;
    }
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_isActive) {
        if(section==3){
            return 30;
        }
        return 0.0000000001f;
    }else{
        if(section==4){
            return 30;
        }
        return 0.0000000001f;
    }
}
    
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (_isActive) {
        if (section==0||section==1) {
            return nil;
        }else{
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 30)];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, headerView.height)];
            label.textColor = [UIColor colorWithHexString:@"#1a1a1a"];
            label.font=[UIFont systemFontOfSize:14];
            label.text=section==3?@"选座支付方式":@"购票须知";
            [headerView addSubview:label];
            return headerView;
        }
    }else{
        if (section==0||section==1||section==2) {
            return nil;
        }else{
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 30)];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, headerView.height)];
            label.textColor = [UIColor colorWithHexString:@"#1a1a1a"];
            label.font=[UIFont systemFontOfSize:14];
            label.text=section==3?@"选座支付方式":@"购票须知";
            [headerView addSubview:label];
            return headerView;
        }
    }
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isActive) {
        if (indexPath.section==0) {
            FilmConfirmHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmHeaderCell"];
            if (cell==nil) {
                cell=[[FilmConfirmHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmHeaderCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDataForModel:sourceModel];
            return cell;
        }else if(indexPath.section==1){
            FilmConfirmPhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmPhoneCell"];
            if (cell==nil) {
                cell=[[FilmConfirmPhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmPhoneCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDataForModel:sourceModel];
            cell.PhoneInputBlock=^(NSString *text){
                sourceModel.user_phone=text;
            };
            return cell;
        }else if(indexPath.section==2){
            FilmConfirmPayTypeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmPayTypeCell"];
            if (cell==nil) {
                cell=[[FilmConfirmPayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmPayTypeCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (indexPath.row==0) {
                [cell.leftBtn setTitle:@"微信支付" forState:UIControlStateNormal];
                [cell.leftBtn setImage:[UIImage imageNamed:@"film_pay_wx"] forState:UIControlStateNormal];
            }else{
                [cell.leftBtn setTitle:@"支付宝" forState:UIControlStateNormal];
                [cell.leftBtn setImage:[UIImage imageNamed:@"film_pay_ali"] forState:UIControlStateNormal];
            }
            return cell;
        }else{
            FilmConfirmGpxzCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmGpxzCell"];
            if (cell==nil) {
                cell=[[FilmConfirmGpxzCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmGpxzCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return cell;
        }
    }else{
        if (indexPath.section==0) {
            FilmConfirmHeaderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmHeaderCell"];
            if (cell==nil) {
                cell=[[FilmConfirmHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmHeaderCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDataForModel:sourceModel];
            return cell;
        }else if(indexPath.section==1){
            FilmConfirmCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmCouponCell"];
            if (cell==nil) {
                cell=[[FilmConfirmCouponCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmCouponCell"];
            }
                if (indexPath.row==1) {
                    cell.yuELabel.hidden = NO;
                    cell.tiShiYuELabel.hidden = NO;
                    cell.zhiFButton.hidden = NO;
                    cell.priceLabel.hidden = YES;
                    cell.youhuilabel.hidden = YES;
                    cell.totalLabel.hidden = YES;
                    NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
                    if (usrMony.floatValue !=0) {
                        cell.tiShiYuELabel.text = [NSString stringWithFormat:@"%@元",usrMony];
                    }else{
                        cell.tiShiYuELabel.text = @"0元";
                    }
                    [cell.zhiFButton addTarget:self action:@selector(yuEPaySender: event:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(indexPath.row==2){
                    cell.priceLabel.hidden = YES;
                    cell.youhuilabel.hidden = YES;
                    cell.totalLabel.hidden = NO;
                    NSMutableAttributedString *string;
                    if (is_YuE ==NO) {
                        NSString * mony = [NSString stringWithFormat:@"%.2f",sourceModel.movie_num.floatValue * sourceModel.movie_price.floatValue];
                        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%@元",mony]];
                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
                        cell.totalLabel.attributedText = string;
                    }else{
                        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%@元",totalPayPrice]];
                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
                        cell.totalLabel.attributedText = string;
                    }
                    
                    
                }else{
                    cell.priceLabel.hidden = NO;
                    cell.youhuilabel.hidden = NO;
                    cell.totalLabel.hidden = YES;
                    cell.yuELabel.hidden = YES;
                    cell.tiShiYuELabel.hidden = YES;
                    cell.zhiFButton.hidden = YES;
                    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                    [cell reflushDataForModel:seleCouponModel];
                }
             cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return cell;
        }else if(indexPath.section==2){
            FilmConfirmPhoneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmPhoneCell"];
            if (cell==nil) {
                cell=[[FilmConfirmPhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmPhoneCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell reflushDataForModel:sourceModel];
            cell.PhoneInputBlock=^(NSString *text){
                sourceModel.user_phone=text;
            };
            return cell;
        }else if(indexPath.section==3){
            FilmConfirmPayTypeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmPayTypeCell"];
            if (cell==nil) {
                cell=[[FilmConfirmPayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmPayTypeCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (indexPath.row==0) {
                [cell.leftBtn setTitle:@"微信支付" forState:UIControlStateNormal];
                [cell.leftBtn setImage:[UIImage imageNamed:@"film_pay_wx"] forState:UIControlStateNormal];
            }else{
                [cell.leftBtn setTitle:@"支付宝" forState:UIControlStateNormal];
                [cell.leftBtn setImage:[UIImage imageNamed:@"film_pay_ali"] forState:UIControlStateNormal];
            }
            return cell;
        }else{
            FilmConfirmGpxzCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FilmConfirmGpxzCell"];
            if (cell==nil) {
                cell=[[FilmConfirmGpxzCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilmConfirmGpxzCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
}
//使用余额
-(void)yuEPaySender:(UIButton *)sender event:(id)event{
    is_YuE = YES;
//    NSSet *touches =[event allTouches];
//    UITouch *touch =[touches anyObject];
//    CGPoint currentTouchPosition = [touch locationInView:myTable];
//    NSIndexPath *indexPath= [myTable indexPathForRowAtPoint:currentTouchPosition];
//    FilmConfirmCouponCell  *cell = [myTable cellForRowAtIndexPath:indexPath];
    if (sender.selected==NO) {
        [sender setBackgroundImage:[UIImage imageNamed:@"film_pay_sele"] forState:UIControlStateNormal];
        sender.selected=YES;
        NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
        NSString * mony = [NSString stringWithFormat:@"%d",sourceModel.movie_num.intValue * sourceModel.movie_price.intValue];
        NSString * strPrice = [NSString stringWithFormat:@"%.2f",mony.floatValue-usrMony.floatValue];
        totalPayPrice = strPrice;
    }else{
        [sender setBackgroundImage:[UIImage imageNamed:@"film_pay_nor"] forState:UIControlStateNormal];
        sender.selected=NO;
        NSString * mony = [NSString stringWithFormat:@"%.2f",sourceModel.movie_num.floatValue * sourceModel.movie_price.floatValue];
        totalPayPrice = mony;
    }
    [myTable reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isActive) {
        if (indexPath.section==1 && indexPath.row==0) {
            SelectFilmCouponController *filmCouponCtr=[SelectFilmCouponController new];
            filmCouponCtr.orderId=_orderId;
            filmCouponCtr.couponId=seleCouponModel.cid;
            [self.navigationController pushViewController:filmCouponCtr animated:YES];
        }else if (indexPath.section==2){
            subPayType=indexPath.row==0?@"8":@"3";
            if (selectedIndexPath) {
                FilmConfirmPayTypeCell *cell=(FilmConfirmPayTypeCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
                [cell.rightBtn setImage:[UIImage imageNamed:@"film_pay_nor"] forState:UIControlStateNormal];
            }
            FilmConfirmPayTypeCell *cell=(FilmConfirmPayTypeCell *)[tableView cellForRowAtIndexPath:indexPath];
            [cell.rightBtn setImage:[UIImage imageNamed:@"film_pay_sele"] forState:UIControlStateNormal];
            selectedIndexPath = indexPath;
        }
    }else{
        if (indexPath.section==1&& indexPath.row==0) {

            SelectFilmCouponController *filmCouponCtr=[SelectFilmCouponController new];
            filmCouponCtr.orderId=_orderId;
            filmCouponCtr.couponId=seleCouponModel.cid;
            [self.navigationController pushViewController:filmCouponCtr animated:YES];
        }else if (indexPath.section==3){
            subPayType=indexPath.row==0?@"8":@"3";
            if (selectedIndexPath) {
                FilmConfirmPayTypeCell *cell=(FilmConfirmPayTypeCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
                [cell.rightBtn setImage:[UIImage imageNamed:@"film_pay_nor"] forState:UIControlStateNormal];
            }
            FilmConfirmPayTypeCell *cell=(FilmConfirmPayTypeCell *)[tableView cellForRowAtIndexPath:indexPath];
            [cell.rightBtn setImage:[UIImage imageNamed:@"film_pay_sele"] forState:UIControlStateNormal];
            selectedIndexPath = indexPath;
        }
    }
}

#pragma mark - 15分钟后自动返回
-(void)isEndTime{
    [self cancelOrderRequest];
}

#pragma mark - Request
    
-(void)firstDataRequest{
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"order_info" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"order_info",
//                         @"orderid":_orderId
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            myTable.hidden=NO;
            
            NSString *isAct=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"isActive"]];
            if ([isAct isEqualToString:@"1"]) {
                _isActive=YES;
            }else{
                _isActive=NO;
            }
            
            sourceModel=[FilmConfirmModel new];
            id obj=responseObject[@"retData"];
            if ([obj isKindOfClass:[NSDictionary class]]) {
                [sourceModel jsonDataForDictionary:obj];
            }
            
            totalPrice=sourceModel.movie_amount.floatValue;
            
            seleCouponModel.cid=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"coupon_id"]];;
            
            timeMinute=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"minute"]];
            timeSecond=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"second"]];
            [self initTipView];
            
            if ([seleCouponModel.cid isEqualToString:@"0"]) {
                payPrice=sourceModel.movie_price.floatValue*sourceModel.position.count;
                [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
            }else{
                if (sourceModel.position.count>1) {
                    payPrice=sourceModel.movie_price.floatValue*(sourceModel.position.count-1);
                    [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
                }else{
                    //优惠券支付
                    subPayType=@"12";
                    payPrice=0.0;
                    [payPriceBtn setTitle:[NSString stringWithFormat:@"确认支付￥%.2f",payPrice] forState:UIControlStateNormal];
                }
            }
            
            [myTable reloadData];
            //默认选中
            subPayType=@"8";
            NSIndexPath * selIndex = [NSIndexPath indexPathForRow:0 inSection:_isActive?2:3];
            [myTable selectRowAtIndexPath:selIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
            NSIndexPath * path = [NSIndexPath indexPathForItem:0 inSection:_isActive?2:3];
            [self tableView:myTable didSelectRowAtIndexPath:path];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
-(void)cancelOrderRequest{
    [AppUtils showProgressMessage:@"正在取消订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_movie" forKey:@"mod"];
    [dict setObject:@"cancel_seat_api" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderId"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_movie",
//                         @"code":@"cancel_seat_api",
//                         @"orderId":_orderId,
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            // 延迟加载
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:FilmOrder_CancelNotifacation object:nil];
            });
        }else{
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示" message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                
            }];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
-(void)couponPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"order_pay" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:seleCouponModel.cid forKey:@"coupon_id"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",payPrice] forKey:@"movie_amount"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",totalPrice-payPrice] forKey:@"credit_price"];
    [dict setObject:sourceModel.user_phone forKey:@"phone"];
    [dict setObject:subPayType forKey:@"pay_type"];
    
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"order_pay",
//                         @"orderid":_orderId,
//                         @"coupon_id":seleCouponModel.cid,
//                         @"movie_amount":[NSString stringWithFormat:@"%.2f",payPrice],
//                         @"credit_price":[NSString stringWithFormat:@"%.2f",totalPrice-payPrice],
//                         @"phone":sourceModel.user_phone,
//                         @"pay_type":subPayType,
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"优惠券支付 %@",responseObject);
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            if ([subPayType isEqualToString:@"12"]) {
                WaitTourController *ctr=[WaitTourController new];
                ctr.orderId=_orderId;
                ctr.isCouponPay=YES;
                [self.navigationController pushViewController:ctr animated:YES];
            }else{
                TourSuccessController *ctr=[TourSuccessController new];
                ctr.orderId=_orderId;
                [self.navigationController pushViewController:ctr animated:YES];
            }
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
-(void)weixinPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"movie_order" forKey:@"mod"];
    [dict setObject:@"get_weixin_pre_payid" forKey:@"action"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:seleCouponModel.cid forKey:@"coupon_id"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",payPrice] forKey:@"movie_amount"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",totalPrice-payPrice] forKey:@"credit_price"];
    [dict setObject:sourceModel.user_phone forKey:@"phone"];
    [dict setObject:subPayType forKey:@"pay_type"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"movie_order",
//                         @"action":@"get_weixin_pre_payid",
//                         @"orderid":_orderId,
//                         @"coupon_id":seleCouponModel.cid,
//                         @"movie_amount":[NSString stringWithFormat:@"%.2f",payPrice],
//                         @"credit_price":[NSString stringWithFormat:@"%.2f",totalPrice-payPrice],
//                         @"phone":sourceModel.user_phone,
//                         @"pay_type":subPayType,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"微信 ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"][@"post_data"];
                if ([resObj isKindOfClass:[NSDictionary class]]) {
                    FilmWeiXinPayModel *weixinPayModel=[FilmWeiXinPayModel new];
                    [weixinPayModel jsonDataForDictioanry:resObj];
                    [self weixinPayWithModel:weixinPayModel];
                }
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
-(void)aliyPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"movie_order" forKey:@"mod"];
    [dict setObject:@"get_alipay_payid" forKey:@"action"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:seleCouponModel.cid forKey:@"coupon_id"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",payPrice] forKey:@"movie_amount"];
    [dict setObject:[NSString stringWithFormat:@"%.2f",totalPrice-payPrice] forKey:@"credit_price"];
    [dict setObject:sourceModel.user_phone forKey:@"phone"];
    [dict setObject:subPayType forKey:@"pay_type"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"movie_order",
//                         @"action":@"get_alipay_payid",
//                         @"orderid":_orderId,
//                         @"coupon_id":seleCouponModel.cid,
//                         @"movie_amount":[NSString stringWithFormat:@"%.2f",payPrice],
//                         @"credit_price":[NSString stringWithFormat:@"%.2f",totalPrice-payPrice],
//                         @"phone":sourceModel.user_phone,
//                         @"pay_type":subPayType,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"支付宝 ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"][@"result"];
                if ([resObj isKindOfClass:[NSString class]]) {
                    [self alliPayWithId:resObj];
                }
            }else{
                NSString *str=responseObject[@"retData"][@"msg"];
                if ([str isEqualToString:@"订单已支付!"]||[str isEqualToString:@"订单已支付！"]) {
                    TourSuccessController *ctr=[TourSuccessController new];
                    ctr.orderId=_orderId;
                    [self.navigationController pushViewController:ctr animated:YES];
                }else{
                    [AppUtils showSuccessMessage:str inView:self.view];
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//查询订单支付状态
//0待付款 1待出票 2待放映 3已放映
-(void)wxPayResultRequest{
    [AppUtils showProgressInView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"m_order" forKey:@"mod"];
    [dict setObject:@"queryOrder" forKey:@"code"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //
//    NSDictionary *dict=@{
//                         @"is_iso":@"1",
//                         @"mod":@"m_order",
//                         @"code":@"queryOrder",
//                         @"orderid":_orderId,
//                         @"userid":[AppUtils getValueWithKey:User_ID]
//                         };
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",responseObject);
        NSLog(@"查询订单支付状态=%@",responseObject);
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"status"]];
            if ([str isEqualToString:@"2"]) {
                TourSuccessController *ctr=[TourSuccessController new];
                ctr.orderId=_orderId;
                [self.navigationController pushViewController:ctr animated:YES];
            }else if([str isEqualToString:@"1"]){
                WaitTourController *ctr=[WaitTourController new];
                ctr.orderId=_orderId;
                [self.navigationController pushViewController:ctr animated:YES];
            }else if([str isEqualToString:@"5"]){
                FilmTicketCloseController *ctr=[FilmTicketCloseController new];
                ctr.orderId=_orderId;
                [self.navigationController pushViewController:ctr animated:YES];
            }
        }else{
            [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
    
#pragma mark - 微信支付
-(void)weixinPayWithModel:(FilmWeiXinPayModel *)model{
    [WXApi registerApp:model.appid];
    if([WXApi isWXAppInstalled]){
        PayReq* req    = [[PayReq alloc] init];
        req.openID     = model.appid;
        req.partnerId  = model.partnerid;           //商户号
        req.prepayId   = model.prepayid;            //预支付交易会话ID
        req.package    = model.package;             //扩展字段
        req.nonceStr   = model.noncestr;            //随机字符串
        req.timeStamp  = model.timestamp.intValue;  //时间戳（防止重发）
        req.sign       = model.sign;                //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}
    
#pragma mark - 微信支付回调
-(void)wxSender:(id)sender{
    [self wxPayResultRequest];
}
    
-(void)alliPayWithId:(NSString *)strId{
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"youxiaalipay";
    //支付异步请求并回调
    [[AlipaySDK defaultService] payOrder:strId fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
        [self wxPayResultRequest];
    }];
}
    
- (NSString*)orderItemWithKey:(NSString*)key andValue:(NSString*)value encoded:(BOOL)bEncoded{
    if (key.length > 0 && value.length > 0) {
        if (bEncoded) {
            value = [self encodeValue:value];
        }
        return [NSString stringWithFormat:@"%@=%@", key, value];
    }
    return nil;
}
    
- (NSString*)encodeValue:(NSString*)value{
    NSString* encodedValue = value;
    if (value.length > 0) {
        encodedValue = (__bridge_transfer  NSString*)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)value, NULL, (__bridge CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 );
    }
    return encodedValue;
}
    
#pragma mark - 支付宝支付回调
-(void)alipaySender:(id)sender{
    [self wxPayResultRequest];
}
    
#pragma mark - dealloc
    
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
    
    @end
