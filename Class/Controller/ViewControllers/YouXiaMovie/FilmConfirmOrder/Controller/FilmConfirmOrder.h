//
//  FilmConfirmOrder.h
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmConfirmOrder : UIViewController

@property (nonatomic ,copy) NSString *orderId;

@property (nonatomic, assign) BOOL isActive;

@end
