//
//  FilmWeiXinPayModel.h
//  youxia
//
//  Created by mac on 2016/12/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmWeiXinPayModel : NSObject

@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *partnerid;
@property (nonatomic, copy) NSString *prepayid;
@property (nonatomic, copy) NSString *package;
@property (nonatomic, copy) NSString *noncestr;
@property (nonatomic, copy) NSString *timestamp;
@property (nonatomic, copy) NSString *sign;

-(void)jsonDataForDictioanry:(NSDictionary *)dic;

@end
