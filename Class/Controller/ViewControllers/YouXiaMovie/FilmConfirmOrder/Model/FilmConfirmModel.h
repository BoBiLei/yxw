//
//  FilmConfirmModel.h
//  youxia
//
//  Created by mac on 2016/12/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmConfirmModel : NSObject

@property (nonatomic, copy) NSString *cinemaId;
@property (nonatomic, copy) NSString *cinemaName;
@property (nonatomic, copy) NSString *movie_amount;
@property (nonatomic, copy) NSString *movie_data;
@property (nonatomic, copy) NSString *movie_id;
@property (nonatomic, copy) NSString *movie_num;
@property (nonatomic, copy) NSString *movie_price;
@property (nonatomic, copy) NSString *orderid;
@property (nonatomic, copy) NSString *playtime;
@property (nonatomic, retain) NSArray *position;
@property (nonatomic, copy) NSString *user_phone;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
