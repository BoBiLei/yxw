//
//  FilmConfirmModel.m
//  youxia
//
//  Created by mac on 2016/12/6.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmModel.h"

@implementation FilmConfirmModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.cinemaId=[NSString stringWithFormat:@"%@",dic[@"cinemaId"]];
    self.cinemaName=[NSString stringWithFormat:@"%@",dic[@"cinemaName"]];
    self.movie_amount=[NSString stringWithFormat:@"%@",dic[@"movie_amount"]];
    self.movie_data=[NSString stringWithFormat:@"%@",dic[@"movie_data"]];
    self.movie_id=[NSString stringWithFormat:@"%@",dic[@"movie_id"]];
    self.movie_num=[NSString stringWithFormat:@"%@",dic[@"movie_num"]];
    self.movie_price=[NSString stringWithFormat:@"%@",dic[@"movie_price"]];
    self.orderid=[NSString stringWithFormat:@"%@",dic[@"orderid"]];
    self.playtime=[NSString stringWithFormat:@"%@",dic[@"playtime"]];
    self.position=dic[@"position"];
    self.user_phone=[NSString stringWithFormat:@"%@",dic[@"user_phone"]];
}

@end
