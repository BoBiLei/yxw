//
//  FilmWeiXinPayModel.m
//  youxia
//
//  Created by mac on 2016/12/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmWeiXinPayModel.h"

@implementation FilmWeiXinPayModel


-(void)jsonDataForDictioanry:(NSDictionary *)dic{
    self.appid=[NSString stringWithFormat:@"%@",dic[@"appid"]];
    self.partnerid=[NSString stringWithFormat:@"%@",dic[@"partnerid"]];
    self.prepayid=[NSString stringWithFormat:@"%@",dic[@"prepayid"]];
    self.package=[NSString stringWithFormat:@"%@",dic[@"package"]];
    self.noncestr=[NSString stringWithFormat:@"%@",dic[@"noncestr"]];
    self.timestamp=[NSString stringWithFormat:@"%@",dic[@"timestamp"]];
    self.sign=[NSString stringWithFormat:@"%@",dic[@"sign"]];
}

@end
