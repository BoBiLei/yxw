//
//  FilmConfirmCouponCell.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmCouponCell.h"

@implementation FilmConfirmCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _yuELabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 7, 90, 30)];
    _yuELabel.text = @"我的余额：";
    _yuELabel.hidden = YES;
    [self addSubview:_yuELabel];
    
    _tiShiYuELabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-200, 7, 155, 30)];
    _tiShiYuELabel.textAlignment = 2;
    _tiShiYuELabel.hidden = YES;
    _tiShiYuELabel.textColor = [UIColor colorWithHexString:@"ff5742"];
    [self addSubview:_tiShiYuELabel];
    
    UIImage * zhiButtonImage = [UIImage imageNamed:@"film_pay_nor"];
    _zhiFButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-35*W_UNIT, 10*H_UNIT, 22*W_UNIT,22*W_UNIT)];
    _zhiFButton.hidden = YES;
    //        [_zhiFButton addTarget:self action:@selector(yuEPaySender:) forControlEvents:UIControlEventTouchUpInside];
    [_zhiFButton setBackgroundImage:zhiButtonImage forState:UIControlStateNormal];
    [self  addSubview:_zhiFButton];
    
    
    _totalLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,7, SCREENSIZE.width-45, 30)];
 
    _totalLabel.textAlignment = 2;
    [self addSubview:_totalLabel];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(SelectFilmCouponModel *)model{
    self.priceLabel.text=[model.cid isEqualToString:@"0"]?@"不使用":[NSString stringWithFormat:@"当月免费一张电影票"];
}

@end
