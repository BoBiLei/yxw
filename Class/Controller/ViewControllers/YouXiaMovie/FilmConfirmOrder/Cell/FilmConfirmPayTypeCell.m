//
//  FilmConfirmPayTypeCell.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmPayTypeCell.h"

@implementation FilmConfirmPayTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.frame=CGRectMake(10, 0, 100, 44);
        [_leftBtn setTitleColor:[UIColor colorWithHexString:@"1a1a1a"] forState:UIControlStateNormal];
        _leftBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [_leftBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        _leftBtn.enabled=YES;
        _leftBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [self addSubview:_leftBtn];
        [self sendSubviewToBack:_leftBtn];
        
        _rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.frame=CGRectMake(SCREENSIZE.width-34, 11, 22, 22);
        [_rightBtn setImage:[UIImage imageNamed:@"film_pay_nor"] forState:UIControlStateNormal];
        [self addSubview:_rightBtn];
        [self sendSubviewToBack:_rightBtn];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

//    if (selected) {
//        [_rightBtn setImage:[UIImage imageNamed:@"payType_sele"] forState:UIControlStateNormal];
//    }else{
//        [_rightBtn setImage:[UIImage imageNamed:@"payType_nor"] forState:UIControlStateNormal];
//    }
}

@end
