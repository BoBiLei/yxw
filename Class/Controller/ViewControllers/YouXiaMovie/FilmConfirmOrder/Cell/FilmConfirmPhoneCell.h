//
//  FilmConfirmPhoneCell.h
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmConfirmModel.h"

@interface FilmConfirmPhoneCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (nonatomic, copy) void (^PhoneInputBlock)(NSString *text);

-(void)reflushDataForModel:(FilmConfirmModel *)model;
    
@end
