//
//  FilmConfirmHeaderCell.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmHeaderCell.h"

@implementation FilmConfirmHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(FilmConfirmModel *)model{
    self.movieNameLabel.text=model.movie_data;
    self.dateLabel.text=model.playtime;
    self.cinemaLabel.text=model.cinemaName;
    
    NSString *seatStr=@"";
    for (NSString *str in model.position) {
        NSString *itemStr=@"";
        NSArray *picarry=[str componentsSeparatedByString:@":"];
        for (int i=0; i<picarry.count; i++) {
            NSString *str=picarry[i];
            itemStr=[itemStr stringByAppendingString:i==0?[NSString stringWithFormat:@"%@排",str]:[NSString stringWithFormat:@"%@座",str]];
        }
        seatStr=[seatStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",itemStr]];
    }
    self.seatLabel.text=seatStr;
    self.priceLabel.text=[NSString stringWithFormat:@"票价: ￥%@",model.movie_amount];
}

@end
