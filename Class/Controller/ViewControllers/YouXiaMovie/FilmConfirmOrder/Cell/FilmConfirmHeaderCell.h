//
//  FilmConfirmHeaderCell.h
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmConfirmModel.h"

@interface FilmConfirmHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *movieNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *cinemaLabel;
@property (weak, nonatomic) IBOutlet UILabel *seatLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

-(void)reflushDataForModel:(FilmConfirmModel *)model;

@end
