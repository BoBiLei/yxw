//
//  FilmConfirmPhoneCell.m
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "FilmConfirmPhoneCell.h"

@implementation FilmConfirmPhoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_phoneTextField addTarget:self action:@selector(goodsNameChange:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - textField
- (void)goodsNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.PhoneInputBlock) {
        self.PhoneInputBlock(field.text);
    }
}

-(void)reflushDataForModel:(FilmConfirmModel *)model{
    _phoneTextField.text=model.user_phone;
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
