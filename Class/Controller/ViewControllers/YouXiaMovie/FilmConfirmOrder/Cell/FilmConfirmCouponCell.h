//
//  FilmConfirmCouponCell.h
//  youxia
//
//  Created by mac on 2016/11/26.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectFilmCouponModel.h"

@interface FilmConfirmCouponCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *youhuilabel;
@property (nonatomic,retain) UILabel * yuELabel;
@property (nonatomic,retain) UILabel * tiShiYuELabel;
@property (nonatomic,retain) UIButton * zhiFButton;
@property (nonatomic,retain) UILabel* totalLabel;
-(void)reflushDataForModel:(SelectFilmCouponModel *)model;

@end
