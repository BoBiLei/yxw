//
//  HomePageController.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageController.h"
#import "UINavigationBar+Awesome.h"
#import "HomePageHeaderViewCell.h"
#import "HomePageReusableView.h"
#import "YouXiaShowController.h"
#import "YouXiaShowCell.h"
#import "SearchViewController.h"
#import "StrategyDetailController.h"
#import "HotDestinationCell.h"
#import "CurrentHotSellCell.h"
#import "HeaderShowCell.h"      //新游侠秀顶部
#import "NewYouxiaShowCell.h"   //新游侠秀item
#import "NewIslandCell.h"       //新海岛Cell
#import "NewTSHeaderCell.h"     //新游记攻略顶部
#import "HomePageFQCell.h"
#import "NewTSItem.h"     //新游记攻略item
#import "NewReusableFillterModel.h"
#import "SectionHeadercCollectionViewLayout.h"
#import "NewMaldiThematiController.h"
#import "NewReusableView.h"
#import "HomePageCategoryCell.h"
#import "PalauThemticController.h"
#import "YouXiaShowPhotoDetailController.h"
#import "YouXiaStageController.h"
#import "ThemticStragoryController.h"
#import "MoreChoicenessController.h"
#import "DistingcOrtherListController.h"
#import "StaticAdvController.h"
#import "IslandOnlineController.h"
#import "FilmController.h"
#import "MovieActivityController.h"
#import "MMGRootViewController.h"
#import "HotelHomeController.h"
#define cellWidth (SCREENSIZE.width-32)/3
#define NavigaBar_Height 64

#define IMAGE_OFFSET_SPEED 20

static NSString *reusableHeaderIdentifier = @"header";

@interface HomePageController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HomePageReusableViewDelegate,AdvImageViewDelegate,ShowImageDelegate,HPCategoryDelegate,CurrentHotSellDelegate,HomepageThemticTravelDelegate,HomePageFillterSeleDelegate,HomePageCategoryDelegate>

@property (nonatomic, strong) UICollectionView * homePageCollection;

@end

@implementation HomePageController{
    
    NSMutableArray *fillterBtnArr;
    
    HomepageFenqiModel *fenqiModel;
    
    MJRefreshNormalHeader *refleshHeader;
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    UIImageView *phImgv;
    CGFloat topContentInset;
    
    UIWebView *phoneView;
    NSURLRequest *phoneRequest;
    
    //
    NSArray *hotSellArr;
    NSArray *themticTravelArr;          //主题游
    NSMutableArray *yxxHeaderArr;       //游侠秀
    NSMutableArray *yxxArr;
    NSMutableArray *straHeaderArr;      //游记攻略
    NSMutableArray *straArr;
    
    NSMutableArray *categoryDataArr;
    
    NSMutableArray *islandArr;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"游侠首页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"游侠首页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestHttpsSignKey];
    topContentInset = IMAGEPLAY_HEIHGT;
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initCollectionView];

    [self setUpCustomSearBar];

    [self requestFillterButtonData];
    
    //检测版本更新，延迟加载
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestHttpsForVersion];
    });
}

-(void)setUpCustomSearBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    CGFloat phoneWidth=44;
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    searchView=[[UIImageView alloc] initWithFrame:CGRectMake(12, 26, SCREENSIZE.width-24-phoneWidth, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"目的地/岛屿/酒店";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    //
    phImgv=[[UIImageView alloc] initWithFrame:CGRectMake(cusBar.width-searchView.height-10, searchView.origin.y, searchView.height, searchView.height)];
    phImgv.userInteractionEnabled=YES;
    phImgv.image=[UIImage imageNamed:@"search_phone"];
    phImgv.userInteractionEnabled=YES;
    [self.view addSubview:phImgv];
    UITapGestureRecognizer *phTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [phImgv addGestureRecognizer:phTap];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

-(void)callPhone{
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - initCollectionView
-(void)initCollectionView{
    SectionHeadercCollectionViewLayout *myLayout= [[SectionHeadercCollectionViewLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    myLayout.floatSection=5;
    _homePageCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HomePageFQCell" bundle:nil] forCellWithReuseIdentifier:@"HomePageFQCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HotDestinationCell" bundle:nil] forCellWithReuseIdentifier:@"HotDestinationCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"NewIslandCell" bundle:nil] forCellWithReuseIdentifier:@"NewIslandCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"NewTSItem" bundle:nil] forCellWithReuseIdentifier:@"NewTSItem"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"NewTSHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NewTSHeaderCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"NewYouxiaShowCell" bundle:nil] forCellWithReuseIdentifier:@"NewYouxiaShowCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HeaderShowCell" bundle:nil] forCellWithReuseIdentifier:@"HeaderShowCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"CurrentHotSellCell" bundle:nil] forCellWithReuseIdentifier:@"CurrentHotSellCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HomePageHeaderViewCell" bundle:nil] forCellWithReuseIdentifier:@"headercell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"YouXiaShowCell" bundle:nil] forCellWithReuseIdentifier:@"YouXiaShowCell"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HomePageCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"HomePageCategoryCell"];
    _homePageCollection.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    _homePageCollection.dataSource=self;
    _homePageCollection.delegate=self;
    _homePageCollection.showsVerticalScrollIndicator=NO;
    
    
    [_homePageCollection registerNib:[UINib nibWithNibName:@"NewReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"fillterheader"];
    [_homePageCollection registerNib:[UINib nibWithNibName:@"HomePageReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:reusableHeaderIdentifier];
    
    [self.view addSubview:_homePageCollection];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestHttpsForHomePage];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    
    // 设置字体
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    _homePageCollection.mj_header = refleshHeader;
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 10;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:case 1:case 2:case 3:case 4:
            return 1;
            break;
        case 5:
            return islandArr.count;
            break;
        case 6:
            return yxxHeaderArr.count;
            break;
        case 7:
            return yxxArr.count;
            break;
        case 8:
            return straHeaderArr.count;
            break;
        default:
            return straArr.count;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        HomePageHeaderViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"headercell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HomePageHeaderViewCell alloc]init];
        }
        cell.delegate=self;
        return cell;
    }else if (indexPath.section==1){
        HomePageCategoryCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HomePageCategoryCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HomePageCategoryCell alloc]init];
        }
        cell.CategoryDelegate=self;
        return cell;
    }else if (indexPath.section==2){
        CurrentHotSellCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CurrentHotSellCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[CurrentHotSellCell alloc]init];
        }
        cell.dataArr=hotSellArr;
        cell.delegate=self;
        return cell;
    }
    
    //主题游
    else if (indexPath.section==3){
        HotDestinationCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HotDestinationCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HotDestinationCell alloc]init];
        }
        cell.dataArr=themticTravelArr;
        cell.delegate=self;
        return cell;
    }
    
    //游侠分期
    else if (indexPath.section==4){
        HomePageFQCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HomePageFQCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HomePageFQCell alloc]init];
        }
        cell.model=fenqiModel;
        return cell;
    }
    
    //海岛
    else if (indexPath.section==5){
        NewIslandCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewIslandCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewIslandCell alloc]init];
        }
        HomePageIslandModel *model=islandArr[indexPath.row];
        [cell reflushHomepageIslandData:model];
        //
        CGFloat yOffset = ((self.homePageCollection.contentOffset.y - cell.frame.origin.y) / (SCREENSIZE.width/(5/2)+30)) * IMAGE_OFFSET_SPEED;
        cell.imageOffset = CGPointMake(0.0f, yOffset);
        return cell;
    }
    
    //游侠秀header
    else if (indexPath.section==6){
        HeaderShowCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HeaderShowCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[HeaderShowCell alloc]init];
        }
        HomePageYxxModel *model=yxxHeaderArr[indexPath.row];
        cell.model=model;
        return cell;
    }
    
    //游侠秀item
    else if (indexPath.section==7){
        NewYouxiaShowCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewYouxiaShowCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewYouxiaShowCell alloc]init];
        }
        
        if (yxxArr.count!=0) {
            HomePageYxxModel *model=yxxArr[indexPath.row];
            cell.model=model;
        }
        return cell;
    }
    
    //游记攻略header
    else if (indexPath.section==8){
        NewTSHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSHeaderCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSHeaderCell alloc]init];
        }
        
        if (straHeaderArr.count!=0) {
            HomepageStrategyModel *model=straHeaderArr[indexPath.row];
            cell.model=model;
        }
        return cell;
    }
    
    //游记攻略item
    else{
        NewTSItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSItem" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSItem alloc]init];
        }
        
        if (straArr.count!=0) {
            HomepageStrategyModel *model=straArr[indexPath.row];
            cell.model=model;
        }
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(SCREENSIZE.width, IMAGEPLAY_HEIHGT-1);
            break;
        case 1:
            return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/2+8);
            break;
        case 2:
            return CGSizeMake(SCREENSIZE.width, cellWidth+44);
            break;
        case 3:
            return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width-8)/3+8);
            break;
        case 4:
            return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width*279/720);
            break;
        case 5:
            return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/(5/2)+37);
            break;
        case 6:
            return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3+29);
            break;
        case 7:
            return CGSizeMake((SCREENSIZE.width)/2, (SCREENSIZE.width-32)/2+12);
            break;
        case 8:
            return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3+29);
            break;
        default:
            return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width-32)/3-28);
            break;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==0||section==6||section==8) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else{
        return UIEdgeInsetsMake(0, 0, 16, 0);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==4) {
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }
    else if (indexPath.section==5) {
        HomePageIslandModel *model=islandArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=MaldivesIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==6) {
        HomePageYxxModel *model=yxxHeaderArr[indexPath.row];
        YouXiaShowPhotoDetailController *ctr=[YouXiaShowPhotoDetailController new];
        ctr.titleName=[NSString stringWithFormat:@"游侠秀%@",model.xxName];
        ctr.photoId=model.xId;
        ctr.cid=model.cid;      //cid 31-相册、客照   32-视频  其他-攻略
        ctr.type=yxxPhoto;  //都是photo类型
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (indexPath.section==7){
        HomePageYxxModel *model=yxxArr[indexPath.row];
        YouXiaShowPhotoDetailController *ctr=[YouXiaShowPhotoDetailController new];
        ctr.titleName=[NSString stringWithFormat:@"游侠秀%@",model.xxName];
        ctr.photoId=model.xId;
        ctr.cid=model.cid;      //cid 31-相册、客照   32-视频  其他-攻略
        ctr.type=yxxPhoto;  //都是photo类型
        [self.navigationController pushViewController:ctr animated:YES];
    }else if (indexPath.section==8){
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if (indexPath.section==9){
        HomepageStrategyModel *model=straArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if(indexPath.section==5){
            NewReusableView *fillterView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"fillterheader" forIndexPath:indexPath];
            fillterView.delegate=self;
            fillterView.fillterArr=fillterBtnArr;
            return fillterView;
        }else{
            HomePageReusableView *hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reusableHeaderIdentifier forIndexPath:indexPath];
            [hpReusableView reflushDataForIndexPath:indexPath];
            hpReusableView.delegate=self;
            return hpReusableView;
        }
    }
    return nil;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (section==0||section==1||section==2||section==4||section==7||section==9) {
        return CGSizeMake(0, 0);
    }else{
        if(section==5){
            NSString *str=[AppUtils getValueWithKey:@"HP_FilterBtnLine"];
            return CGSizeMake(SCREENSIZE.width, [str isEqualToString:@"1"]?44*2:44*3);
        }else{
            return CGSizeMake(SCREENSIZE.width, 56);
        }
    }
}

#pragma mark - 点击目的地 Delegate
-(void)selectedCategoryItemAtIndexPath:(NSIndexPath *)indexPath andUrl:(NSString *)url{
    switch (indexPath.row) {
        case 0:
        {
            NewMaldiThematiController *malCtr=[NewMaldiThematiController new];
            [self.navigationController pushViewController:malCtr animated:YES];
        }
            break;
        case 1:
        {
            PalauThemticController *plCtr=[PalauThemticController new];
            [self.navigationController pushViewController:plCtr animated:YES];
        }
            break;
        case 2:
        {
            DistingcOrtherListController *distShow=[[DistingcOrtherListController alloc]init];
            distShow.reveivleftTag=6;
            distShow.reveivRightTag=1;
            NSString *urlStr=url;
            NSRange rang=[urlStr rangeOfString:@"com/"];
            if (rang.location!=NSNotFound) {
                urlStr=[urlStr substringFromIndex:rang.location+rang.length];
            }
            distShow.urlStr=urlStr;
            [self.navigationController pushViewController:distShow animated:YES];
        }
            break;
        case 3:
        {
            DistingcOrtherListController *distShow=[[DistingcOrtherListController alloc]init];
            distShow.reveivleftTag=7;
            distShow.reveivRightTag=1;
            NSString *urlStr=url;
            NSRange rang=[urlStr rangeOfString:@"com/"];
            if (rang.location!=NSNotFound) {
                urlStr=[urlStr substringFromIndex:rang.location+rang.length];
            }
            distShow.urlStr=urlStr;
            [self.navigationController pushViewController:distShow animated:YES];
        }
            break;
        case 4:
        {
            DistingcOrtherListController *distShow=[[DistingcOrtherListController alloc]init];
            distShow.reveivleftTag=6;
            distShow.reveivRightTag=9;
            NSString *urlStr=url;
            NSRange rang=[urlStr rangeOfString:@"com/"];
            if (rang.location!=NSNotFound) {
                urlStr=[urlStr substringFromIndex:rang.location+rang.length];
            }
            distShow.urlStr=urlStr;
            [self.navigationController pushViewController:distShow animated:YES];
        }
            break;
        case 5:
        {
            YouXiaStageController *ctr=[YouXiaStageController new];
            [self.navigationController pushViewController:ctr animated:YES];
            
        }
            break;
        case 6:
        {
            YouXiaShowController *ctr=[YouXiaShowController new];
            [self.navigationController pushViewController:ctr animated:YES];
            
        }
            break;
        case 7:
        {
            
            ThemticStragoryController *ctr=[ThemticStragoryController new];
            ctr.titleStr=@"游记攻略";
            ctr.urlStr=url;
            ctr.type=AllStragoryType;
            [self.navigationController pushViewController:ctr animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 点击当季热卖 Delegate
-(void)clickHotSellItem:(HomepageHotSellModel *)model{
    if ([model.name isEqualToString:@"游侠秀"]){
        [self.navigationController pushViewController:[FilmController new] animated:YES];
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
        
    }else if ([model.name isEqualToString:@"游侠信用卡"]){
        StaticAdvController *strCtr=[StaticAdvController new];
        strCtr.titleStr=model.name;
        strCtr.urlStr=model.url;
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if ([model.name isEqualToString:@"游侠电影"]){
       
//        HotelHomeController *dd=[[HotelHomeController alloc]init];
//       [self.navigationController pushViewController:dd animated:YES];
        [self.rdv_tabBarController setSelectedIndex:2];
    }else{
        
        HotelHomeController *dd=[[HotelHomeController alloc]init];
        [self.navigationController pushViewController:dd animated:YES];
        
        
//        MoreChoicenessController *ctr=[MoreChoicenessController new];
//        ctr.urlStr=model.url;
//        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - 点击跳转到游侠商城
-(void)clickGoYXShop{
    MMGRootViewController *vc=[MMGRootViewController new];
    vc.hidesBottomBarWhenPushed=YES;
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 点击主题游 Delegate
-(void)clickThemticTravelItem:(HomepageThemticTravelModel *)model{
    MoreChoicenessController *ctr=[MoreChoicenessController new];
    ctr.urlStr=model.url;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - Barner广告 Delegate
-(void)clickAdvModel:(ImageModel *)model{
    
    if ([model.advType isEqualToString:@"maldives"]) {
        NewMaldiThematiController *malCtr=[NewMaldiThematiController new];
        [self.navigationController pushViewController:malCtr animated:YES];
    }else if ([model.advType isEqualToString:@"palau"]) {
        PalauThemticController *plCtr=[PalauThemticController new];
        [self.navigationController pushViewController:plCtr animated:YES];
    }else if ([model.advType isEqualToString:@"show"]) {
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"zxxd"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"links"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",model.imgId]]];
    }else if ([model.advType isEqualToString:@"strategy_maldives"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_palau"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi"]) {
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_show"]) {
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.imgId;
        strCtr.cId=@"234";
        strCtr.strategyTitle=@"title";
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi_list"]) {
        
    }else if ([model.advType isEqualToString:@"yx_movie"]) {
        [self.rdv_tabBarController setSelectedIndex:2];
    }else if ([model.advType isEqualToString:@"is_movie_activity"]) {
        MovieActivityController *ctr=[MovieActivityController new];
        ctr.model=model;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"is_movie_list"]) {
        self.rdv_tabBarController.selectedIndex=2;
    }
}

#pragma mark - 点击分期付款广告 delegate
-(void)clickAdvImageView{
    YouXiaStageController *fqCtr=[[YouXiaStageController alloc]init];
    [self.navigationController pushViewController:fqCtr animated:YES];
}

#pragma mark - ReusableFillter Delegate
-(void)clickFillterResult:(NSArray *)array andIslandName:(NSString *)name{
    islandArr=[NSMutableArray array];
    for (NSDictionary *dic in array) {
        HomePageIslandModel *model=[HomePageIslandModel new];
        model.islandName=name;
        model.model=dic;
        [islandArr addObject:model];
    }
    [self.homePageCollection reloadData];
    
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (app.hp_reflush_reusable==0) {
        //滑动到headerview位置
        NSString *flTag=[AppUtils getValueWithKey:@"FloatTag"];
        [self.homePageCollection setContentOffset:CGPointMake(0,flTag.floatValue-64) animated:NO];
    }
}

#pragma mark - Reusableview delegate
-(void)clickMore:(NSString *)strId{
    
    if ([strId isEqualToString:@"00"]){
        MoreChoicenessController *ctr=[MoreChoicenessController new];
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&c=index&a=zhuti_v6_ios&id=99&is_iso=1";
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"11"]){
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"22"]){
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&a=list_strategy_v6_ios&is_iso=1";
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"33"]){
        
    }else{
        
    }
}

#pragma mark - 点击游侠秀图片 delegate
-(void)clickShowImageWithId:(NSString *)xId name:(NSString *)name{
    
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offsetY = scrollView.contentOffset.y+_homePageCollection.contentInset.top;
    
    if (refleshHeader.state!=MJRefreshStateRefreshing) {
        if (offsetY<0) {
            searchView.hidden=YES;
            phImgv.hidden=YES;
        }else{
            searchView.hidden=NO;
            phImgv.hidden=NO;
        }
    }
    
    if (offsetY==0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    
    if (offsetY<-63) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((IMAGEPLAY_HEIHGT-NavigaBar_Height - offsetY) / topContentInset));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
    
    //岛屿显示效果
    for(NewIslandCell *view in self.homePageCollection.visibleCells) {
        if ([view isKindOfClass:NewIslandCell.class]) {
            CGFloat yOffset = ((self.homePageCollection.contentOffset.y - view.frame.origin.y) / (SCREENSIZE.width/(5/2)+30)) * IMAGE_OFFSET_SPEED;
            view.imageOffset = CGPointMake(0.0f, yOffset);
        }
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForHomePage{
    if ([[AppUtils getValueWithKey:Sign_NewKey] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        yxxHeaderArr=[NSMutableArray array];
        yxxArr=[NSMutableArray array];
        straHeaderArr=[NSMutableArray array];
        straArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&c=index&a=index_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            DSLog(@"%@",responseObject);
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //banner
                NSArray *imgArr=responseObject[@"retData"][@"new_banner"];
                [[NSNotificationCenter defaultCenter] postNotificationName:HomePageImageNoti object:imgArr];
                
                //category
                NSArray *ctgArr=responseObject[@"retData"][@"mudi"];
                [[NSNotificationCenter defaultCenter] postNotificationName:HomePageCategoryNoti object:ctgArr];
                
                //hotSellArr
                hotSellArr=responseObject[@"retData"][@"tuijianwei"];
                
                //fenqi
                fenqiModel=[HomepageFenqiModel new];
                fenqiModel.model=responseObject[@"retData"][@"fenqi"];
                
                //themticTravelArr
                themticTravelArr=responseObject[@"retData"][@"zhuti"];
                
                //游侠秀
                NSArray *yxttAA=responseObject[@"retData"][@"yxx_data"];
                for (int i=0; i<yxttAA.count; i++) {
                    NSDictionary *dic=yxttAA[i];
                    if (i==0) {
                        HomePageYxxModel *model=[HomePageYxxModel new];
                        model.model=dic;
                        [yxxHeaderArr addObject:model];
                    }else{
                        HomePageYxxModel *model=[HomePageYxxModel new];
                        model.model=dic;
                        [yxxArr addObject:model];
                    }
                }
                //NSLog(@"%@------%@",yxxHeaderArr,yxxArr);
                
                //攻略游记
                NSArray *glArr=responseObject[@"retData"][@"strategy_data"];
                //DSLog(@"%@",glArr);
                for (int i=0; i<glArr.count; i++) {
                    NSDictionary *dic=glArr[i];
                    if (i==0) {
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straHeaderArr addObject:model];
                    }else{
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straArr addObject:model];
                    }
                }
            }
            [_homePageCollection reloadData];
            [_homePageCollection.mj_header endRefreshing];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [_homePageCollection.mj_header endRefreshing];
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:Sign_NewKey] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@============================================",responseObject);
        /*
         key = "fdfgjasd&sd(ajsgdha13%1asd";
         msg = "\U7b7e\U540d\U5df2\U91cd\U65b0\U751f\U6210";
         signsn = 15120254469;
         */
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:Sign_NewKey];
            [self requestHttpsForHomePage];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [_homePageCollection.mj_header endRefreshing];
    }];
}

#pragma mark -
-(void)requestFillterButtonData{
    if ([[AppUtils getValueWithKey:@"new_sKey"] isEqualToString:@"1"]) {
        [self requestFBHttpsSignKey];
    }else{
        fillterBtnArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:@"http://m.youxia.com/index.php?m=wap&a=index_json_list_v6_ios&is_iso=1" method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            //DSLog(@"%@",responseObject);
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //
                NSArray *dyArr=responseObject[@"retData"][@"index_nav"];
                for (NSDictionary *dic in dyArr) {
                    NewReusableFillterModel *model=[NewReusableFillterModel new];
                    model.model=dic;
                    [fillterBtnArr addObject:model];
                }
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

//重新获取key
-(void)requestFBHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new_sKey"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new_sKey"];
            [self requestFillterButtonData];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 检查版本是否更新
-(void)requestHttpsForVersion{
//    [[NetWorkRequest defaultClient] requestWithPath:@"https://itunes.apple.com/CN/lookup?id=1076463089" method:HttpRequestGet parameters:nil prepareExecute:^{
    [[NetWorkRequest defaultClient] requestWithPath:@"http://itunes.apple.com/lookup?id=1076463089" method:HttpRequestPost parameters:nil prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        NSDictionary *dic=responseObject[@"results"][0];
        
        NSMutableString *appStoreVersion=[[NSMutableString alloc] initWithString:dic[@"version"]];
        NSRange rang=[appStoreVersion rangeOfString:@"."];
        while (rang.location!=NSNotFound) {
            [appStoreVersion deleteCharactersInRange:rang];
            rang=[appStoreVersion rangeOfString:@"."];
        }
        
        NSMutableString *currentVersion=[[NSMutableString alloc] initWithString:APP_VERSION];
        NSRange crang=[currentVersion rangeOfString:@"."];
        while (crang.location!=NSNotFound) {
            [currentVersion deleteCharactersInRange:crang];
            crang=[currentVersion rangeOfString:@"."];
        }
        if (appStoreVersion.integerValue>currentVersion.integerValue) {
            UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"发现新版本，是否更新！"preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
                
            }];
            UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1076463089"]];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:yesAction];
            UIViewController *ctr=[[UIApplication sharedApplication] keyWindow].rootViewController;
            [ctr presentViewController:alertController animated:YES completion:nil];
        }else{
            NSLog(@"111111111");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}


@end
