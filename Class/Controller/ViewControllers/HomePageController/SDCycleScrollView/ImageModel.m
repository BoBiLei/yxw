//
//  ImageModel.m
//  Pods
//
//  Created by mac on 15/9/22.
//
//

#import "ImageModel.h"

@implementation ImageModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.imgId=dic[@"url"];
    self.imgStr=dic[@"pic"];
    self.advType=dic[@"type"];
}

@end
