//
//  CurrentHotSellCell.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageHotSellModel.h"

@protocol CurrentHotSellDelegate <NSObject>

-(void)clickHotSellItem:(HomepageHotSellModel *)model;

-(void)clickGoYXShop;

@end

@interface CurrentHotSellCell : UICollectionViewCell

@property(nonatomic,retain) NSArray *dataArr;

@property (nonatomic, weak) id <CurrentHotSellDelegate> delegate;

@end
