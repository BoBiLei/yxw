//
//  HotSellView.h
//  youxia
//
//  Created by mac on 16/5/10.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageHotSellModel.h"

@interface HotSellView : UIView

@property (nonatomic,retain) HomepageHotSellModel *model;

@end
