//
//  CurrentHotSellCell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CurrentHotSellCell.h"
#import "HotSellView.h"
#import "MMGRootViewController.h"
#import "RDVTabBarController.h"
#define cellWidth (SCREENSIZE.width-36)/3+44

@implementation CurrentHotSellCell{
    HotSellView *bitView;
    UIImageView *bitImg;
    YYLabel *title01;
    UILabel *hotSellLabel;
    
    //
    HotSellView *topView;
    UIImageView *topImgv;
    YYLabel *top_title01;
    YYLabel *top_title02;
    
    //
    HotSellView *btmView;
    UIImageView *btmImgv;
    YYLabel *btm_title01;
    YYLabel *btm_title02;
    
    
    
    HotSellView *leftView;
    UIImageView *leftImgv;
    UILabel *leftTopLabel01;
    UILabel *leftTopLabel02;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    
    
//
    //左边shang面
    
    leftView=[[HotSellView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2-8, (cellWidth-1)/2)];
//    leftView=[[HotSellView alloc]initWithFrame:CGRectMake(0, line03.origin.y+line03.height, bitView.width, bitView.height)];
//    leftView.backgroundColor=[UIColor orangeColor];
    leftView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapleftTop=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickleftTop)];
    [leftView addGestureRecognizer:tapleftTop];
    [self addSubview:leftView];
    
    
    leftImgv=[[UIImageView alloc] initWithFrame:CGRectMake(SCREENSIZE.width/2-(leftView.height/2-12), 10, leftView.height-20, leftView.height-20)];
    leftImgv.contentMode=UIViewContentModeScaleToFill;
    leftImgv.center=CGPointMake(leftView.width-leftImgv.width+18, leftView.height/2);
    leftImgv.layer.cornerRadius=leftImgv.height/2;
    leftImgv.layer.masksToBounds = YES;
    [leftView addSubview:leftImgv];
    
    leftTopLabel01=[[UILabel alloc]initWithFrame:CGRectMake(0, 16, leftView.width-leftImgv.width-16, (leftView.height-32)/2)];
    leftTopLabel01.textAlignment=NSTextAlignmentCenter;
    leftTopLabel01.font=kFontSize15;
//    leftTopLabel01.textColor=[UIColor colorWithHexString:@"#00b38c"];
    leftTopLabel01.textColor = [UIColor colorWithRed:234.0/255 green:155.0/255 blue:61.0/255 alpha:1];    [leftView addSubview:leftTopLabel01];
    
    leftTopLabel02=[[UILabel alloc]initWithFrame:CGRectMake(8, leftTopLabel01.origin.y+leftTopLabel01.height, leftTopLabel01.width, leftTopLabel01.height)];
    leftTopLabel02.textAlignment=NSTextAlignmentLeft;
    leftTopLabel02.font=kFontSize12;
    leftTopLabel02.textColor=[UIColor lightGrayColor];
    [leftView addSubview:leftTopLabel02];
    
    //竖线
    UIView *lineTop=[[UIView alloc]initWithFrame:CGRectMake(leftView.width-1, 0, 1.0f, leftView.height+2)];
    lineTop.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [leftView addSubview:lineTop];
    
    //横线
    UIView *line03=[[UIView alloc]initWithFrame:CGRectMake(0, leftView.height, leftView.width, 1)];
    line03.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [self addSubview:line03];

    //左边下面的
    bitView=[[HotSellView alloc]initWithFrame:CGRectMake(0, line03.origin.y+line03.height, leftView.width, leftView.height)];
    bitView.userInteractionEnabled=YES;
    [self addSubview:bitView];
    
    bitImg=[[UIImageView alloc]initWithFrame:CGRectMake(SCREENSIZE.width/2-(bitView.height/2-12), 10, bitView.height-20, bitView.height-20)];
    bitImg.contentMode=UIViewContentModeScaleToFill;
    bitImg.center=CGPointMake(bitView.width-bitImg.width+18, bitView.height/2);
    bitImg.layer.cornerRadius=bitImg.height/2;
    bitImg.layer.masksToBounds = YES;
    [bitView addSubview:bitImg];
    
    
    
    title01=[[YYLabel alloc]initWithFrame:CGRectMake(0, 16, bitView.width-bitImg.width-16, (bitView.height-32)/2)];
    [bitView addSubview:title01];
    
    hotSellLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, title01.origin.y+title01.height, title01.width, title01.height)];
    hotSellLabel.adjustsFontSizeToFitWidth=YES;
    hotSellLabel.font=kFontSize13;
    hotSellLabel.textAlignment=NSTextAlignmentCenter;
    hotSellLabel.textColor = [UIColor lightGrayColor];
    [bitView addSubview:hotSellLabel];
    
    //竖线
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(bitView.width-1, 0, 1.0f, bitView.height+2)];
    line.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [bitView addSubview:line];
    
    
    //右边上面
    topView=[[HotSellView alloc]initWithFrame:CGRectMake(line.origin.x+line.width, 0, SCREENSIZE.width-(line.origin.x+line.width), (cellWidth-1)/2)];
    topView.userInteractionEnabled=YES;
    [self addSubview:topView];
    
    top_title01=[[YYLabel alloc]initWithFrame:CGRectMake(0, topView.height/2-18, topView.width/2+12, 21)];
    [topView addSubview:top_title01];
    
    top_title02=[[YYLabel alloc]initWithFrame:CGRectMake(2, top_title01.origin.y+top_title01.height, top_title01.width-4, 19)];
    [topView addSubview:top_title02];
    
    topImgv=[[UIImageView alloc] initWithFrame:CGRectMake(top_title01.origin.x+top_title01.width+4, 12, topView.width-(top_title01.origin.x+top_title01.width+12), topView.height-24)];
    topImgv.contentMode=UIViewContentModeScaleToFill;
    [topView addSubview:topImgv];
    
    
    //横线
    UIView *line02=[[UIView alloc]initWithFrame:CGRectMake(line.origin.x+line.width, topView.height, topView.width, 1)];
    line02.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [self addSubview:line02];
    
    //右边下面
    btmView=[[HotSellView alloc]initWithFrame:CGRectMake(topView.origin.x, line02.origin.y+line02.height, topView.width, topView.height)];
    btmView.userInteractionEnabled=YES;
    [self addSubview:btmView];
    
    btm_title01=[[YYLabel alloc]initWithFrame:CGRectMake(0, btmView.height/2-18, btmView.width/2+4, 21)];
    [btmView addSubview:btm_title01];
    
    btm_title02=[[YYLabel alloc]initWithFrame:CGRectMake(0, btm_title01.origin.y+btm_title01.height, btm_title01.width, 19)];
    [btmView addSubview:btm_title02];
    
    btmImgv=[[UIImageView alloc] initWithFrame:CGRectMake(btm_title01.origin.x+btm_title01.width+4, 12, btmView.width-(btm_title01.origin.x+btm_title01.width+12), btmView.height-24)];
    btmImgv.contentMode=UIViewContentModeScaleToFill;
    [btmView addSubview:btmImgv];
    
    //
    CGRect frame=self.frame;
    frame.size.height=cellWidth;
    self.frame=frame;
}

//
-(void)setDataArr:(NSArray *)dataArr{
    for (int i=0; i<dataArr.count; i++) {
        NSDictionary *dic=dataArr[i];
        HomepageHotSellModel *model=[HomepageHotSellModel new];
        model.model=dic;
        if (i==0) {
            SDImageCache *imgCache=[SDImageCache sharedImageCache];
            if ([imgCache diskImageExistsWithKey:model.pic]) {
                UIImage *image = [imgCache imageFromDiskCacheForKey:model.pic];
                bitImg.image=image;
            }else{
                [bitImg sd_setImageWithURL:[NSURL URLWithString:model.pic]];
            }
            NSMutableAttributedString *title01Attr = [[NSMutableAttributedString alloc] initWithString:model.name];
            title01Attr.font=kFontSize15;
            title01Attr.alignment=NSTextAlignmentCenter;
            title01Attr.color = [UIColor colorWithHexString:@"#e7341e"];
            title01.attributedText=title01Attr;
            
            hotSellLabel.text=model.name2;
            
            bitView.model=model;
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickItem)];
            [bitView addGestureRecognizer:tap];
        }else if (i==3){
            SDImageCache *imgCache=[SDImageCache sharedImageCache];
            if ([imgCache diskImageExistsWithKey:model.pic]) {
                UIImage *image = [imgCache imageFromDiskCacheForKey:model.pic];
                topImgv.image=image;
            }else{
                [topImgv sd_setImageWithURL:[NSURL URLWithString:model.pic]];
            }
            
            NSMutableAttributedString *top_title01Attr = [[NSMutableAttributedString alloc] initWithString:model.name];
            top_title01Attr.alignment=NSTextAlignmentCenter;
            top_title01Attr.font=kFontSize15;
            top_title01Attr.color=[UIColor colorWithHexString:@"#00b38c"];
            top_title01.attributedText=top_title01Attr;
            
            NSMutableAttributedString *top_title02Attr = [[NSMutableAttributedString alloc] initWithString:model.name2];
            top_title02Attr.alignment=NSTextAlignmentCenter;
            top_title02Attr.font=kFontSize12;
            top_title02Attr.color=[UIColor lightGrayColor];
            top_title02.attributedText=top_title02Attr;
            
            topView.model=model;
            UITapGestureRecognizer *tap01=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickTopView)];
            [topView addGestureRecognizer:tap01];
        }else if (i==2){
            SDImageCache *imgCache=[SDImageCache sharedImageCache];
            if ([imgCache diskImageExistsWithKey:model.pic]) {
                UIImage *image = [imgCache imageFromDiskCacheForKey:model.pic];
                btmImgv.image=image;
            }else{
                [btmImgv sd_setImageWithURL:[NSURL URLWithString:model.pic]];
            }
            
            NSMutableAttributedString *btm_title01Attr = [[NSMutableAttributedString alloc] initWithString:model.name];
            btm_title01Attr.alignment=NSTextAlignmentCenter;
            btm_title01Attr.font=kFontSize15;
            btm_title01Attr.color=[UIColor colorWithHexString:@"#1ea1c1"];
            btm_title01.attributedText=btm_title01Attr;
            
            NSMutableAttributedString *btm_title02Attr = [[NSMutableAttributedString alloc] initWithString:model.name2];
            btm_title02Attr.alignment=NSTextAlignmentCenter;
            btm_title02Attr.font=kFontSize12;
            btm_title02Attr.color=[UIColor lightGrayColor];
            btm_title02.attributedText=btm_title02Attr;
            
            btmView.model=model;
            UITapGestureRecognizer *tap02=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickBtmView)];
            [btmView addGestureRecognizer:tap02];
        }
        //新加的
        else if(i==1)
        {
            {
                [leftImgv sd_setImageWithURL:[NSURL URLWithString:model.pic]];
                
                
                leftTopLabel01.text=model.name;
                leftTopLabel02.text=model.name2;
                leftView.model=model;
     
            }
            
        }
        
    }
}

#pragma mark - 点击 左上
-(void)clickleftTop{
    
    [self.delegate clickGoYXShop];
    
}

-(void)clickItem{
    HomepageHotSellModel *model = bitView.model;
    [self.delegate clickHotSellItem:model];
}


-(void)clickTopView{
    HomepageHotSellModel *model = topView.model;
    [self.delegate clickHotSellItem:model];
}

-(void)clickBtmView{
    HomepageHotSellModel *model = btmView.model;
    [self.delegate clickHotSellItem:model];
}

@end
