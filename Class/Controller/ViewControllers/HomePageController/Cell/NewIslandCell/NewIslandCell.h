//
//  NewIslandCell.h
//  youxia
//
//  Created by mac on 16/3/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageIslandModel.h"

@interface NewIslandCell : UICollectionViewCell

@property (nonatomic, strong, readwrite) UIImage *image;

@property (nonatomic, assign, readwrite) CGPoint imageOffset;

-(void)reflushHomepageIslandData:(HomePageIslandModel *)model;

-(void)reflushMDThemtic:(HomePageIslandModel *)model;

@end
