//
//  NewIslandCell.m
//  youxia
//
//  Created by mac on 16/3/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewIslandCell.h"

#define titleColor @"#2a2a2a"
#define descriptColor @"#a0a0a0"

#define blueColor @"#0080c5"

@implementation NewIslandCell{
    UIImageView *imgv;
    YYLabel *stagePrice;
    YYLabel *topLabel;
    YYLabel *ckLabel;
    YYLabel *title;
    TTTAttributedLabel *description;
    
    UIImageView *btnImg;
    UIImageView *ltView;
    
    UIView *wView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    imgv=[UIImageView new];
    [self.contentView addSubview:imgv];
    
    //顶部灰色透明的View
    CGFloat ltWidth=SCREENSIZE.width*2/5+2;
    CGFloat ltHeight=30;
    ltView=[[UIImageView alloc]initWithFrame:CGRectMake(8, 10, ltWidth, ltHeight)];
    ltView.image=[UIImage imageNamed:@"island_leftop"];
    [self.contentView addSubview:ltView];
    
    btnImg=[[UIImageView alloc]initWithFrame:CGRectMake(7, 6, 25, ltView.height-13)];
    btnImg.image=[UIImage imageNamed:@"island_10"];
    btnImg.contentMode=UIViewContentModeScaleAspectFit;
    [ltView addSubview:btnImg];
    
    topLabel=[[YYLabel alloc]initWithFrame:CGRectMake(btnImg.origin.x+btnImg.width+2, btnImg.origin.y+1, ltView.width-btnImg.width-20, btnImg.height)];
    [ltView addSubview:topLabel];
    
    //分期价
    stagePrice=[YYLabel new];
    stagePrice.backgroundColor=[UIColor colorWithHexString:blueColor];
    stagePrice.font=[UIFont systemFontOfSize:IS_IPHONE5?11:12];
    [self.contentView addSubview:stagePrice];
    
    //参考价
    ckLabel=[YYLabel new];
    ckLabel.backgroundColor=[UIColor whiteColor];
    ckLabel.textAlignment=NSTextAlignmentCenter;
    ckLabel.textColor=[UIColor colorWithHexString:blueColor];
    ckLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?11:12];
    [self.contentView addSubview:ckLabel];
    
    //title
    title=[YYLabel new];
    title.backgroundColor=[UIColor whiteColor];
    title.textColor=[UIColor colorWithHexString:titleColor];
    title.font=kFontSize14;
    title.numberOfLines=0;
    [self.contentView addSubview:title];
    
    //描述
    description=[TTTAttributedLabel new];
    description.backgroundColor=[UIColor whiteColor];
    description.textColor=[UIColor colorWithHexString:descriptColor];
    description.font=kFontSize13;
    [self.contentView addSubview:description];
    
    wView=[UIView new];
    wView.backgroundColor=[UIColor whiteColor];
    [self.contentView addSubview:wView];
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    [self setImageOffset:self.imageOffset];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat myFloat=SCREENSIZE.width/(5/2)-20;
    
    //
    CGRect frame=self.frame;
    frame.size.height=SCREENSIZE.width/(5/2)+37;
    self.frame=frame;
    
    imgv.frame=CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width/(5/2)+30);
    
    CGFloat pvWidth=(SCREENSIZE.width-48)/3+(IS_IPHONE5?30:20);
    CGFloat pvHeight=pvWidth/4-2;
    
    stagePrice.frame=CGRectMake(SCREENSIZE.width-pvWidth-8, myFloat-pvHeight*2-14, pvWidth, pvHeight);
    
    ckLabel.frame=CGRectMake(SCREENSIZE.width-pvWidth-8, myFloat-pvHeight*2-14+pvHeight, pvWidth,pvHeight);
    
    title.frame=CGRectMake(0, myFloat+6, SCREENSIZE.width, 25);
    
    description.frame=CGRectMake(0, title.origin.y+title.height-2, title.width, 18);
    
    wView.frame=CGRectMake(0, description.origin.y+description.height, description.width, self.height-(description.origin.y+description.height));
}

-(void)reflushHomepageIslandData:(HomePageIslandModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    NSString *toLStr=[NSString stringWithFormat:@"%@ | %@",model.islandName,model.lvType];
    NSMutableAttributedString *topAttr = [[NSMutableAttributedString alloc] initWithString:toLStr];
    topAttr.font=[UIFont systemFontOfSize:IS_IPHONE5?12:13];
    topAttr.color=[UIColor whiteColor];
    topLabel.attributedText=topAttr;
    
    //重新设置透明条宽度
    CGSize size=[AppUtils getStringSize:toLStr withFont:13];
    CGRect frame=ltView.frame;
    frame.size.width=btnImg.origin.x+btnImg.width+2+size.width+8;
    ltView.frame=frame;
    
    //分期价
    NSString *priceStr=model.islandPrice;
    NSString *stageStr=[NSString stringWithFormat:@"分期价:￥%@起*9期",priceStr];
    NSMutableAttributedString *stageAttr = [[NSMutableAttributedString alloc] initWithString:stageStr];
    stageAttr.color = [UIColor whiteColor];
    NSRange sumRange = [[stageAttr string] rangeOfString:priceStr options:NSCaseInsensitiveSearch];
    //设置选择文本的大小
    UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
    if (font) {
        [stageAttr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
        CFRelease(font);
    }
    stagePrice.attributedText=stageAttr;
    stagePrice.textAlignment = NSTextAlignmentCenter;
    stagePrice.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    
    //参考价
    NSString *tPriceStr=model.totalPrice;
    NSString *crStr=[NSString stringWithFormat:@"参考价:￥%@起",tPriceStr];
    NSMutableAttributedString *crAttr = [[NSMutableAttributedString alloc] initWithString:crStr];
    NSRange crRange = [[crAttr string] rangeOfString:tPriceStr options:NSCaseInsensitiveSearch];
    //设置选择文本的大小
    UIFont *crsystemFont = [UIFont systemFontOfSize:IS_IPHONE5?12:14];
    CTFontRef crfont = CTFontCreateWithName((__bridge CFStringRef)crsystemFont.fontName, crsystemFont.pointSize, NULL);
    if (crfont) {
        [crAttr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)crfont range:crRange];
        CFRelease(crfont);
    }
    
    //
    if ([model.lvType isEqualToString:@"当地游"]) {
        btnImg.image=[UIImage imageNamed:@"zzy"];
//        fqView.hidden=YES;
        stagePrice.hidden=YES;
        ckLabel.backgroundColor=[UIColor colorWithHexString:blueColor];
        crAttr.color = [UIColor whiteColor];
    }else{
        btnImg.image=[UIImage imageNamed:@"island_10"];
//        fqView.hidden=NO;
        stagePrice.hidden=NO;
        ckLabel.backgroundColor=[UIColor whiteColor];
        crAttr.color = [UIColor colorWithHexString:blueColor];
    }
    
    ckLabel.attributedText=crAttr;
    ckLabel.textAlignment = NSTextAlignmentCenter;
    ckLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    
    //title
    title.text=[NSString stringWithFormat:@"  %@  ",model.islandTitle];
    
    //descript
    NSString *descriptStr;
    if ([model.posids isEqualToString:@"1"]) {
        descriptStr=[NSString stringWithFormat:@"  热卖推荐  %@",model.islandDescript];
        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"热卖推荐" options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *systemFont = kFontSize13;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
            if (font) {
                
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                
                //字体背景
                [mutableAttributedString addAttribute:(NSString *)kTTTBackgroundFillColorAttributeName value:(id)[UIColor redColor].CGColor range:sumRange];
                
                //字体颜色
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
                
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        descriptStr=[NSString stringWithFormat:@"  %@",model.islandDescript];
        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            return mutableAttributedString;
        }];
    }
}

//马代主题的
-(void)reflushMDThemtic:(HomePageIslandModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    // Update padding
    [self setImageOffset:self.imageOffset];
    
    btnImg.image=[UIImage imageNamed:@"mathem_star"];
    
    NSString *toLStr=[NSString stringWithFormat:@"岛屿星级 | %@",model.islandName];
    NSMutableAttributedString *topAttr = [[NSMutableAttributedString alloc] initWithString:toLStr];
    topAttr.color = [UIColor whiteColor];
    topAttr.font=[UIFont systemFontOfSize:IS_IPHONE5?11:12];
    topLabel.attributedText=topAttr;
    
    //重新设置透明条宽度
    CGSize size=[AppUtils getStringSize:toLStr withFont:13];
    CGRect frame=ltView.frame;
    frame.size.width=btnImg.origin.x+btnImg.width+2+size.width+8;
    ltView.frame=frame;
    
    //
    NSString *stageStr=[NSString stringWithFormat:@"分期价:￥%@起*9期",model.islandPrice];
    NSMutableAttributedString *stageAttr = [[NSMutableAttributedString alloc] initWithString:stageStr];
    stageAttr.color = [UIColor whiteColor];
    NSRange sumRange = [[stageAttr string] rangeOfString:model.islandPrice options:NSCaseInsensitiveSearch];
    //设置选择文本的大小
    UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
    if (font) {
        [stageAttr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
        CFRelease(font);
    }
    stagePrice.attributedText=stageAttr;
    stagePrice.textAlignment = NSTextAlignmentCenter;
    stagePrice.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    
    //
    ckLabel.text=[NSString stringWithFormat:@"参考价:￥%@",model.totalPrice];
    
    //
    title.text=[NSString stringWithFormat:@"  %@  ",model.islandTitle];
    
    //
    NSString *descriptStr;
    if ([model.posids isEqualToString:@"1"]) {
        descriptStr=[NSString stringWithFormat:@"  热卖推荐  %@",model.islandDescript];
        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"热卖推荐" options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *systemFont = kFontSize13;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
            if (font) {
                
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                
                //字体背景
                [mutableAttributedString addAttribute:(NSString *)kTTTBackgroundFillColorAttributeName value:(id)[UIColor redColor].CGColor range:sumRange];
                
                //字体颜色
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
                
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        descriptStr=[NSString stringWithFormat:@"  %@",model.islandDescript];
        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            return mutableAttributedString;
        }];
    }
}

- (void)setImageOffset:(CGPoint)imageOffset{
    // Store padding value
    
    // Grow image view
    CGRect frame = imgv.bounds;
    CGRect offsetFrame = CGRectOffset(frame, imageOffset.x, imageOffset.y);
    imgv.frame = offsetFrame;
}

@end
