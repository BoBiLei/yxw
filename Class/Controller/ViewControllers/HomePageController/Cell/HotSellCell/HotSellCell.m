//
//  HotSellCell.m
//  youxia
//
//  Created by mac on 15/12/14.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HotSellCell.h"

@implementation HotSellCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _priceLabel=[TTTAttributedLabel new];
    _priceLabel.font=kFontSize13;
    _priceLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:_priceLabel];
    _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *priceStrFormat=[NSString stringWithFormat:@"H:[_imgv]-8-[_priceLabel(%f)]",(SCREENSIZE.width-28-_imgv.width)/2];
    NSArray* _priceLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:priceStrFormat options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imgv,_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_h];
    NSArray* _priceLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_priceLabel(21)]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceLabel)];
    [NSLayoutConstraint activateConstraints:_priceLabel_w];
    
    //
    _stageLabel=[TTTAttributedLabel new];
    _stageLabel.font=kFontSize13;
    _stageLabel.adjustsFontSizeToFitWidth=YES;
    _stageLabel.textAlignment=NSTextAlignmentRight;
    [self addSubview:_stageLabel];
    _stageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *stageStrFormat=[NSString stringWithFormat:@"H:[_priceLabel][_stageLabel]-12-|"];
    NSArray* _stageLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:stageStrFormat options:0 metrics:nil views:NSDictionaryOfVariableBindings(_priceLabel,_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_h];
    NSArray* _stageLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_stageLabel(21)]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_stageLabel)];
    [NSLayoutConstraint activateConstraints:_stageLabel_w];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushDataForModel:(HotSellModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.hsImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.hsImg];
        self.imgv.image=image;
    }else{
        [self.imgv sd_setImageWithURL:[NSURL URLWithString:model.hsImg] placeholderImage:[UIImage imageNamed:Image_Default]];
    }
    
    CGSize size=[AppUtils getStringSize:model.hsTitle withFont:14];
    CGRect frame=_title.frame;
    if (size.width<SCREENSIZE.width-114) {
        frame.size.height=21;
        _title.frame=frame;
    }
    self.title.text=model.hsTitle;
    NSString *priceStr=[NSString stringWithFormat:@"参考价:￥%@/人",model.hsPrice];
    [self.priceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@/人",model.hsPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize14;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    NSString *stageStr=[NSString stringWithFormat:@"分期价:￥%@*9期",model.hsStagePrice];
    [self.stageLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@*9期",model.hsStagePrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize14;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
}

@end
