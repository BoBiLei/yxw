//
//  HotSellCell.h
//  youxia
//
//  Created by mac on 15/12/14.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotSellModel.h"

@interface HotSellCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) TTTAttributedLabel *priceLabel;
@property (strong, nonatomic) TTTAttributedLabel *stageLabel;

-(void)reflushDataForModel:(HotSellModel *)model;

@end
