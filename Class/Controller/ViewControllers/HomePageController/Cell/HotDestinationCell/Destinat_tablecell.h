//
//  Destinat_tablecell.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageThemticTravelModel.h"

@interface Destinat_tablecell : UITableViewCell

@property (nonatomic,retain) HomepageThemticTravelModel *model;

@end
