//
//  HotDestinationCell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HotDestinationCell.h"
#import "Destinat_tablecell.h"

#define cellWidth (SCREENSIZE.width-8)/3

@interface HotDestinationCell()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation HotDestinationCell{
    UITableView *myTable;
    NSMutableArray *sourceArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    //
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=cellWidth;
    self.frame=frame;
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, cellWidth+8, SCREENSIZE.width) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    //逆时针
    myTable.center=CGPointMake(SCREENSIZE.width/2, myTable.width/2);
    myTable.transform = CGAffineTransformMakeRotation(-M_PI_2);
    myTable.showsVerticalScrollIndicator = NO;
    myTable.delegate=self;
    myTable.dataSource=self;
    [myTable registerNib:[UINib nibWithNibName:@"Destinat_tablecell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self addSubview:myTable];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return sourceArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellWidth;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0000001f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Destinat_tablecell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[Destinat_tablecell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    HomepageThemticTravelModel *model=sourceArr[indexPath.row];
    cell.model=model;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HomepageThemticTravelModel *model=sourceArr[indexPath.row];
    [self.delegate clickThemticTravelItem:model];
}

-(void)setDataArr:(NSArray *)dataArr{
    sourceArr=[NSMutableArray array];
    for (NSDictionary *dic in dataArr) {
        HomepageThemticTravelModel *model=[HomepageThemticTravelModel new];
        model.model=dic;
        [sourceArr addObject:model];
    }
    [myTable reloadData];
}

@end
