//
//  HotDestinationCell.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageThemticTravelModel.h"


@protocol HomepageThemticTravelDelegate <NSObject>

-(void)clickThemticTravelItem:(HomepageThemticTravelModel *)model;

@end

@interface HotDestinationCell : UICollectionViewCell

@property (nonatomic, retain) NSArray *dataArr;

@property (nonatomic, weak) id <HomepageThemticTravelDelegate> delegate;

@end
