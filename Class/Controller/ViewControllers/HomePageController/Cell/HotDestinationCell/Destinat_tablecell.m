//
//  Destinat_tablecell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "Destinat_tablecell.h"

#define cellWidth (SCREENSIZE.width-8)/3

@implementation Destinat_tablecell{
    UIImageView *imgv;
    UILabel *label;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 8, cellWidth-8, cellWidth-28)];
    imgv.contentMode=UIViewContentModeScaleToFill;
    [self.contentView addSubview:imgv];
    
    label=[[UILabel alloc]initWithFrame:CGRectMake(0, imgv.origin.y+imgv.height+4, imgv.width, 18)];
    label.textAlignment=NSTextAlignmentCenter;
    label.font=kFontSize13;
    label.textColor=[UIColor colorWithHexString:@"#3c3c3c"];
    [self.contentView addSubview:label];
    
    frame.size.width=(SCREENSIZE.width-32)/3;
    frame.size.height=cellWidth;
    self.frame=frame;
    
    //顺时针
    self.contentView.transform = CGAffineTransformMakeRotation(M_PI_2);
}

-(void)setModel:(HomepageThemticTravelModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.pic]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.pic];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"DefaultImg_120x93"]];
    }
    
    label.text=model.name;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
