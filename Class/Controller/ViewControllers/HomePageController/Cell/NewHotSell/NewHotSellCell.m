//
//  NewHotSellCell.m
//  youxia
//
//  Created by mac on 16/4/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewHotSellCell.h"


#define IMGHEIGHT (SCREENSIZE.width)/2-14

@implementation NewHotSellCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.masksToBounds=YES;
    self.layer.cornerRadius=5;
    
    self.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
    [self setUpUi];
}

-(void)setUpUi{
    
    CGRect frame=self.frame;
    frame.size.height=IS_IPHONE5?130:150;
    
    //图片
    _islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGHEIGHT, IS_IPHONE5?00:100)];
    [self addSubview:_islandImg];
    
    //推荐提示
    _zmImg=[[UIImageView alloc]initWithFrame:CGRectMake(_islandImg.width-56, 0, 56, 56*85/100)];
    _zmImg.image=[UIImage imageNamed:@"newhotselle"];
    [_islandImg addSubview:_zmImg];
    
    //
    _titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(4, _islandImg.height+5, IMGHEIGHT-8, 19)];
    _titleLabel.font=kFontSize13;
    _titleLabel.textColor=[UIColor whiteColor];
    [self addSubview:_titleLabel];
    
    //stagePrice
    _stagePrice=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(_titleLabel.origin.x, _titleLabel.origin.y+_titleLabel.height+2, _titleLabel.width, 19)];
    _stagePrice.font=kFontSize12;
    _stagePrice.textColor=[UIColor colorWithHexString:@"#f8e00c"];
    [self addSubview:_stagePrice];
}

-(void)setModel:(HomePageIslandModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        _islandImg.image=image;
    }else{
        [_islandImg sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x193"]];
    }
    
    _titleLabel.text=model.islandTitle;
    if ([model.lvType isEqualToString:@"自由行"]) {
        NSString *stageStr=[NSString stringWithFormat:@"分期就行:￥%@/9期",model.islandPrice];
        [_stagePrice setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.islandPrice options:NSCaseInsensitiveSearch];
            
            UIFont *boldSystemFont = kFontSize14;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        NSString *stageStr=[NSString stringWithFormat:@"参考价:￥%@",model.totalPrice];
        [_stagePrice setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.totalPrice options:NSCaseInsensitiveSearch];
            
            UIFont *boldSystemFont = kFontSize14;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
}


@end
