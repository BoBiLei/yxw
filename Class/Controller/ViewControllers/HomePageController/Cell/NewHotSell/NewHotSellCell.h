//
//  NewHotSellCell.h
//  youxia
//
//  Created by mac on 16/4/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageIslandModel.h"


@interface NewHotSellCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *zmImg;
@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,strong) UIImageView *islandImg;
@property (nonatomic,strong) UILabel *titleLabel;  //
@property (nonatomic,strong) TTTAttributedLabel *stagePrice;  //分期
@property (nonatomic,strong) UILabel *orgPrice;   //原价

@property (nonatomic, retain) HomePageIslandModel *model;


@end
