//
//  NewYouxiaShowCell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewYouxiaShowCell.h"

#define IMGWIDTH (SCREENSIZE.width-32)/2

@implementation NewYouxiaShowCell{
    UIImageView *imgv;
    UILabel *title;
    UILabel *timeLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    //图片
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 12, IMGWIDTH, IMGWIDTH-60)];
    imgv.layer.cornerRadius=4;
    imgv.layer.masksToBounds=YES;
    imgv.contentMode=UIViewContentModeScaleAspectFill;
    [self addSubview:imgv];
    
    //title   IMGWIDTH-44+53
    title=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x, imgv.origin.y+imgv.height+4, imgv.width, 21)];
    title.font=[UIFont systemFontOfSize:IS_IPHONE5?14:15];
    title.textColor=[UIColor colorWithHexString:@"#383838"];
    [self addSubview:title];
    
    
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(title.origin.x, title.origin.y+title.height+1, title.width, 19)];
    timeLabel.textColor=[UIColor colorWithHexString:@"#7b7b7b"];
    timeLabel.font=kFontSize12;
    [self addSubview:timeLabel];
}

-(void)setModel:(HomePageYxxModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.xImgs]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.xImgs];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.xImgs] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x193"]];
    }
    
    title.text=model.xTitle;
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.xTime];
    timeLabel.text=timeStr;
}

@end
