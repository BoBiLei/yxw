//
//  HeaderShowCell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HeaderShowCell.h"

#define cellHeight SCREENSIZE.width/3+24

@implementation HeaderShowCell{
    UIImageView *imgv;
    YYLabel *title;
    UILabel *timeLabel;
    UILabel *scanLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, cellHeight)];
    imgv.contentMode=UIViewContentModeScaleAspectFill;
    [self addSubview:imgv];
    
    //title
    title=[[YYLabel alloc]initWithFrame:CGRectMake(4, 8, SCREENSIZE.width-8, 24)];
    [imgv addSubview:title];
    
    //timeLabel
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, title.origin.y+title.height+2, 80, 18)];
    [imgv addSubview:timeLabel];
    
    //
    scanLabel=[[UILabel alloc]initWithFrame:CGRectMake(timeLabel.origin.x+timeLabel.width+8, timeLabel.origin.y, 80, timeLabel.height)];
    [imgv addSubview:scanLabel];
    
    CGRect frame=self.frame;
    frame.size.height=cellHeight;
    self.frame=frame;
}

-(void)setModel:(HomePageYxxModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.xImgs]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.xImgs];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.xImgs] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:model.xTitle];
    titleAttr.color=[UIColor whiteColor];
    titleAttr.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
    title.attributedText=titleAttr;
    
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.xTime];
    NSMutableAttributedString *timeLabelAttr = [[NSMutableAttributedString alloc] initWithString:timeStr];
    timeLabelAttr.color=[UIColor whiteColor];
    timeLabelAttr.font=kFontSize13;
    timeLabel.attributedText=timeLabelAttr;
    
    NSMutableAttributedString *scanLabelAttr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@浏览",model.xScan]];
    scanLabelAttr.color=[UIColor whiteColor];
    scanLabelAttr.font=kFontSize13;
    scanLabel.attributedText=scanLabelAttr;
}

@end
