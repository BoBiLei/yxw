//
//  NewYouxiaShowCell.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageYxxModel.h"

@interface NewYouxiaShowCell : UICollectionViewCell

@property (nonatomic,retain) HomePageYxxModel *model;

@end
