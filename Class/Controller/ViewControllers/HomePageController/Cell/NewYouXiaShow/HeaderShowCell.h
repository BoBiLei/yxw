//
//  HeaderShowCell.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  游侠秀顶部

#import <UIKit/UIKit.h>
#import "HomePageYxxModel.h"

@interface HeaderShowCell : UICollectionViewCell

@property (nonatomic,retain) HomePageYxxModel *model;

@end
