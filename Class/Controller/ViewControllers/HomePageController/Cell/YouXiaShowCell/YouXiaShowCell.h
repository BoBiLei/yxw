//
//  YouXiaShowCell.h
//  youxia
//
//  Created by mac on 16/1/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageYxxModel.h"

@protocol ShowImageDelegate <NSObject>

-(void)clickShowImageWithId:(NSString *)xId name:(NSString *)name;

@end

@protocol AdvImageViewDelegate <NSObject>

-(void)clickAdvImageView;

@end

@interface YouXiaShowCell : UICollectionViewCell

@property (weak, nonatomic) id<AdvImageViewDelegate> advDelegate;
@property (weak, nonatomic) id<ShowImageDelegate> showDelegate;

-(void)reflushDataForModel:(NSArray *)modelArr;

@end
