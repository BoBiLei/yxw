//
//  YouXiaShowCell.m
//  youxia
//
//  Created by mac on 16/1/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "YouXiaShowCell.h"
#import <UIButton+WebCache.h>
@implementation YouXiaShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=284;
    self.frame=frame;
    self.backgroundColor=[UIColor whiteColor];
}

-(void)reflushDataForModel:(NSArray *)modelArr{
    CGFloat imgX=6;
    CGFloat imgWidth=(SCREENSIZE.width-4*imgX)/3;
    CGFloat imgHight=170;
    CGFloat imgRHight=0.0f;
    CGFloat imgRX=0.0f;
    CGFloat imgY=0;
    CGFloat imgCY=imgHight/2;
    for (int i=0; i<modelArr.count; i++) {
        UIButton *imgv=[UIButton buttonWithType:UIButtonTypeCustom];

        HomePageYxxModel *model=modelArr[i];
        if (i==0) {
            imgRX=imgX;
            imgRHight=imgHight;
        }else{
            imgRHight=(imgHight-imgX)/2;
            imgRX=i*imgX+i*imgWidth+imgX;
            if (imgRX>=SCREENSIZE.width) {
                imgRX=(i-2)*imgX+(i-2)*imgWidth+imgX;
                imgY=imgCY+imgX/2;
            }
        }
        
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, imgRHight-26, imgWidth, 26)];
        label.alpha=0.80f;
        label.font=kFontSize11;
        label.adjustsFontSizeToFitWidth = YES;
        label.textAlignment=NSTextAlignmentCenter;
        label.numberOfLines=0;
        label.text=model.xTitle;
        label.textColor=[UIColor whiteColor];
        [imgv addSubview:label];
        if (i==0) {
            label.backgroundColor=[UIColor colorWithHexString:@"#004A80"];
        }else if (i==1){
            label.backgroundColor=[UIColor colorWithHexString:@"#9E005D"];
        }else if (i==2){
            label.backgroundColor=[UIColor colorWithHexString:@"#ED1C24"];
        }else if (i==3){
            label.backgroundColor=[UIColor colorWithHexString:@"#F26522"];
        }else if (i==4){
            label.backgroundColor=[UIColor colorWithHexString:@"#00A651"];
        }
        
        //720*200/SCREENSIZE.width
        imgv.frame=CGRectMake(imgRX, imgY, imgWidth, imgRHight);
        [self addSubview:imgv];
        
        objc_setAssociatedObject(imgv, "advModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [imgv addTarget:self action:@selector(tapImg:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIImageView *advImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, imgHight+8, SCREENSIZE.width, SCREENSIZE.width*200/720)];
    advImg.userInteractionEnabled=YES;
    advImg.image=[UIImage imageNamed:@"ad_fenqi"];
    UITapGestureRecognizer *advTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAdvImg)];
    [advImg addGestureRecognizer:advTap];
    [self addSubview:advImg];
}

-(void)tapImg:(id)sender{
    UIButton *btn=sender;
    HomePageYxxModel *model=objc_getAssociatedObject(btn, "advModel");
    [self.showDelegate clickShowImageWithId:model.xId name:model.xTitle];
}

-(void)clickAdvImg{
    [self.advDelegate clickAdvImageView];
}

@end
