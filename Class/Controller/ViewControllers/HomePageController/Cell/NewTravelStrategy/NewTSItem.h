//
//  NewTSItem.h
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  新游记攻略item

#import <UIKit/UIKit.h>
#import "HomepageStrategyModel.h"

@interface NewTSItem : UICollectionViewCell

@property (nonatomic, retain) HomepageStrategyModel *model;

@end
