//
//  NewTSHeaderCell.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewTSHeaderCell.h"

#define cellHeight SCREENSIZE.width/3+29

@implementation NewTSHeaderCell{
    UIImageView *imgv;
    UILabel *title;
    UILabel *scanLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, cellHeight)];
    imgv.contentMode=UIViewContentModeScaleAspectFill;
    [self addSubview:imgv];
    
    title=[[UILabel alloc] initWithFrame:CGRectMake(SCREENSIZE.width/4, 0, SCREENSIZE.width/2, imgv.height/3)];
    title.center=CGPointMake(imgv.center.x, imgv.center.y);
    title.textAlignment=NSTextAlignmentCenter;
    title.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
    title.textColor=[UIColor whiteColor];
    title.numberOfLines=0;
    [self addSubview:title];
    
    //浏览
    scanLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, imgv.height-26, SCREENSIZE.width-24, 18)];
    scanLabel.textColor=[UIColor whiteColor];
    scanLabel.font=kFontSize13;
    [imgv addSubview:scanLabel];
    
    
    CGRect frame=self.frame;
    frame.size.height=cellHeight;
    self.frame=frame;
}

-(void)setModel:(HomepageStrategyModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.strateImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.strateImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.strateImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    title.text=model.strateTitle;
    scanLabel.text=[NSString stringWithFormat:@" %@ 浏览 / %@ 喜欢",model.views,model.zan];
}

@end
