//
//  NewTSItem.m
//  youxia
//
//  Created by mac on 16/3/17.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewTSItem.h"

#define ImageWidth (SCREENSIZE.width-32)/3-8

@implementation NewTSItem{
    UIImageView *imgv;
    UILabel *title;
    UILabel *timeLabel;
    UILabel *viewLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, ImageWidth, ImageWidth-36)];
    imgv.contentMode=UIViewContentModeScaleAspectFill;
    imgv.layer.cornerRadius=3;
    imgv.layer.masksToBounds=YES;
    [self addSubview:imgv];
    
    //title
    title=[[UILabel alloc] init];
    title.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
    title.alpha=0.8f;
    title.numberOfLines=0;
    [self addSubview:title];
    
    
    UIImageView *tiImgv=[[UIImageView alloc] initWithFrame:CGRectMake(imgv.origin.x+imgv.width+12, imgv.origin.y+imgv.height-18, 18, 18)];
    tiImgv.image=[UIImage imageNamed:@"icon_calendar_gray"];
    [self addSubview:tiImgv];
    
    //timeLabel
    timeLabel=[[UILabel alloc] initWithFrame:CGRectMake(tiImgv.origin.x+tiImgv.width+2, imgv.origin.y+imgv.height-18, 76, 18)];
    timeLabel.textColor=[UIColor lightGrayColor];
    timeLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?12:13];
    [self addSubview:timeLabel];
    
    //views
    UIImageView *imgv02=[[UIImageView alloc] initWithFrame:CGRectMake(timeLabel.origin.x+timeLabel.width+12, tiImgv.origin.y, 18, 18)];
    imgv02.image=[UIImage imageNamed:@"icon_eye_gray"];
    [self addSubview:imgv02];
    
    //viewLabel
    viewLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgv02.origin.x+imgv02.width+2, timeLabel.origin.y, SCREENSIZE.width-(imgv02.origin.x+imgv02.width+2)-8, 18)];
    viewLabel.textColor=[UIColor lightGrayColor];
    viewLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?12:13];
    [self addSubview:viewLabel];
}

-(void)setModel:(HomepageStrategyModel *)model{
    
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.strateImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.strateImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.strateImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_120x93"]];
    }
    
    
    CGSize size=[AppUtils getStringSize:model.strateTitle withFont:14];
    if (size.width<=SCREENSIZE.width-imgv.width-24) {
        title.frame=CGRectMake(imgv.origin.x+imgv.width+12, imgv.origin.y, SCREENSIZE.width-imgv.width-24, 21);
    }else{
       title.frame=CGRectMake(imgv.origin.x+imgv.width+12, imgv.origin.y-2, SCREENSIZE.width-imgv.width-24, 40);
    }
    title.text=model.strateTitle;
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.strateTime];
    timeLabel.text=timeStr;
    
    viewLabel.text=[NSString stringWithFormat:@"READ: %@",model.views];
}

//[super drawRect:rect] 得到一条线条
- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}


@end
