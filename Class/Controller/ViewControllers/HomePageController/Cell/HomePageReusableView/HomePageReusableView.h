//
//  HomePageReusableView.h
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomePageReusableViewDelegate <NSObject>

@optional
/**
 点击更多
 */
-(void)clickMore:(NSString *)strId;
@end


@interface HomePageReusableView : UICollectionReusableView

@property (nonatomic,copy) NSString *reusableId;
@property (nonatomic,weak) id <HomePageReusableViewDelegate> delegate;

/**
 首页
 */
-(void)reflushDataForIndexPath:(NSIndexPath *)indexPath;

/**
 游侠秀
 */
-(void)reflushYXSDataForIndexPath:(NSIndexPath *)indexPath;

/**
 精选热卖
 */
-(void)reflushHotSellerDataForIndexPath:(NSIndexPath *)indexPath;

/**
 马代主题
 */
-(void)reflushMDThemticDataForIndexPath:(NSIndexPath *)indexPath;

@end
