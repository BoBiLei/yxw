//
//  HomePageReusableView.m
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageReusableView.h"


@implementation HomePageReusableView{
    UIImageView *iimgv;
    TTTAttributedLabel *titleLabel;
    UIButton *moreButton;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.height=56;
    self.frame=frame;
    
    //iimgv
    iimgv=[[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 27, 27)];
    [self addSubview:iimgv];
    
    //titleLabel
    titleLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(iimgv.origin.x+iimgv.width+6, iimgv.origin.y+1, SCREENSIZE.width-(iimgv.origin.x+iimgv.width+6)-67, iimgv.height-1)];
    titleLabel.font=kFontSize16;
    titleLabel.textColor=[UIColor colorWithHexString:@"#2a2a2a"];
    [self addSubview:titleLabel];
    
    //moreButton
    moreButton=[UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame=CGRectMake(SCREENSIZE.width-67, titleLabel.origin.y, 65, titleLabel.height);
    moreButton.titleLabel.font=kFontSize16;
    [moreButton setTitleColor:[UIColor colorWithHexString:@"#a0a0a0"] forState:UIControlStateNormal];
    [moreButton setTitle:@"更多>" forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(cliclMoreBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreButton];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 45.3, SCREENSIZE.width, 0.7f)];
    line.backgroundColor=[UIColor lightGrayColor];
    line.alpha=0.7f;
    [self addSubview:line];
}

- (void)cliclMoreBtnEvent{
    [self.delegate clickMore:self.reusableId];
}

-(void)reflushDataForIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 3:
            self.reusableId=@"00";
            iimgv.image=[UIImage imageNamed:@"hp_reu_01"];
            titleLabel.text=@"主题游";
            break;
        case 6:
            self.reusableId=@"11";
            iimgv.image=[UIImage imageNamed:@"hp_reu_02"];
            titleLabel.text=@"游侠Show";
            break;
        case 8:
            self.reusableId=@"22";
            iimgv.image=[UIImage imageNamed:@"hp_reu_03"];
            titleLabel.text=@"游记攻略";
            break;
            
        default:
            break;
    }
}

-(void)reflushYXSDataForIndexPath:(NSIndexPath *)indexPath{
    titleLabel.textColor=[UIColor colorWithHexString:@"#0080c5"];
    [moreButton setTitleColor:[UIColor colorWithHexString:@"#0080c5"] forState:UIControlStateNormal];
    if (indexPath.section==1) {
        self.reusableId=@"11";
        iimgv.image=[UIImage imageNamed:@"yyx_reusa01"];
        NSString *str=[NSString stringWithFormat:@"游侠秀客照"];
        [titleLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"客照"] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ffc926"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
    }else if (indexPath.section==2){
        self.reusableId=@"22";
        iimgv.image=[UIImage imageNamed:@"yyx_reusa02"];
        NSString *str=[NSString stringWithFormat:@"游侠秀相册"];
        [titleLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"相册"] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ffc926"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
    }else if (indexPath.section==3){
        self.reusableId=@"33";
        iimgv.image=[UIImage imageNamed:@"yyx_reusa03"];
        NSString *str=[NSString stringWithFormat:@"游侠秀MV"];
        [titleLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"MV"] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ffc926"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
    }else if (indexPath.section==4){
        self.reusableId=@"44";
        iimgv.image=[UIImage imageNamed:@"yyx_reusa04"];
        NSString *str=[NSString stringWithFormat:@"游记攻略"];
        [titleLabel setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"攻略"] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ffc926"] CGColor] range:sumRange];
            return mutableAttributedString;
        }];
    }
}

-(void)reflushHotSellerDataForIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        self.reusableId=@"11";
        titleLabel.text=@"精选热卖";
        iimgv.image=[UIImage imageNamed:@"hp_reu_01"];
    }else if (indexPath.section==2){
        self.reusableId=@"22";
        titleLabel.text=@"帕劳酒店";
        iimgv.image=[UIImage imageNamed:@"hp_reu_hotel"];
    }else if (indexPath.section==3){
        self.reusableId=@"33";
        titleLabel.text=@"帕劳当地游";
        iimgv.image=[UIImage imageNamed:@"hp_reu_hotel"];
    }else if (indexPath.section==4){
        self.reusableId=@"44";
        titleLabel.text=@"游记攻略";
        iimgv.image=[UIImage imageNamed:@"hp_reu_03"];
    }
}

-(void)reflushMDThemticDataForIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        self.reusableId=@"00";
        titleLabel.text=@"精选热卖";
        iimgv.image=[UIImage imageNamed:@"hp_reu_01"];
    }
    else{
        switch (indexPath.section) {
            case 3:
                self.reusableId=@"3";
                titleLabel.text=@"游记攻略";
                iimgv.image=[UIImage imageNamed:@"hp_reu_03"];
                break;
            default:
                
                break;
        }
    }
}

@end
