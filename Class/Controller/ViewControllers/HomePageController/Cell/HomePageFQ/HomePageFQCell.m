//
//  HomePageFQCell.m
//  youxia
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HomePageFQCell.h"

#define frame_height SCREENSIZE.width*279/720
@implementation HomePageFQCell{
    UIImageView *imgv;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=frame_height;
    self.frame=frame;
    
    imgv=[[UIImageView alloc] initWithFrame:self.frame];
    [self addSubview:imgv];
}

-(void)setModel:(HomepageFenqiModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.banner]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.banner];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.banner]];
    }
}

@end
