//
//  ButtonItem.h
//  youxia
//
//  Created by mac on 16/3/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//  重写titleRectForContentRect函数

#import <UIKit/UIKit.h>

@interface ButtonItem : UIButton

- (CGRect)titleRectForContentRect:(CGRect)contentRect;

@end
