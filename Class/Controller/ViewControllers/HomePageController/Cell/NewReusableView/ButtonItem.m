//
//  ButtonItem.m
//  youxia
//
//  Created by mac on 16/3/22.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ButtonItem.h"

@implementation ButtonItem{
    CGRect boundingRect;
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGFloat imageX=12;
    CGFloat imageY=contentRect.origin.y+10;
    CGFloat width=SCREENSIZE.width/4-24;
    CGFloat height=25;
    return CGRectMake(imageX, imageY, width, height);
}

@end
