//
//  NewReusableView.h
//  youxia
//
//  Created by mac on 16/3/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageReusableFillterIslandModel.h"

@protocol HomePageFillterSeleDelegate <NSObject>

-(void)clickFillterResult:(NSArray *)array andIslandName:(NSString *)name;

@end

@interface NewReusableView : UICollectionReusableView

@property (nonatomic, weak) id<HomePageFillterSeleDelegate> delegate;
@property (nonatomic, retain) NSArray *fillterArr;
@end
