//
//  NewReusableView.m
//  youxia
//
//  Created by mac on 16/3/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewReusableView.h"
#import "ButtonItem.h"
#import "NewReusableFillterModel.h"
#import "YYCache.h"

#define SeleColor @"#0080c5"
#define DefaultColor @"#2a2a2a"
#define grateColor @"#a0a0a0"
@implementation NewReusableView{
    
    AppDelegate *app;
    
    //
    NSMutableArray *fillterBtnArr;
    
    //
    NewReusableFillterModel *seleModel;
    
    NSMutableArray *itemArr;
    NSInteger seleItemTag;
    
    UIButton *seleFilterBtn;
    UIButton *seleItemBtn;
    UIView *line;       //按钮下面蓝色横条
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        seleItemTag=100;    //初始化tag=100
        NetWorkStatus *nws=[[NetWorkStatus alloc] init];
        nws.NetWorkStatusBlock=^(BOOL status){
            if (status) {
                [self createFillterBtnForArray:_fillterArr];
            }
        };
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)createFillterBtnForArray:(NSArray *)arr{
    
    app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [AppUtils saveValue:@"1" forKey:@"HP_FilterBtnLine"];//默认
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    
    CGFloat btnHeihgt=44;
    
    
    CGFloat lineX=0;
    for (int i=0; i<arr.count; i++) {
        
        NewReusableFillterModel *model=arr[i];
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i;
        btn.frame=CGRectMake(i*btnWidth, 0, btnWidth, btnHeihgt);
        btn.titleLabel.font=kFontSize16;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:model.name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"#2a2a2a"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        objc_setAssociatedObject(btn, "FillterModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        
        //如果跟之前的tag相等
        if (i==0) {
            lineX=btn.origin.x;
            seleModel=model;
            seleFilterBtn=btn;
            [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
            
            //
            //默认的
            itemArr=[NSMutableArray array];
            NSArray *arr=seleModel.data;
            for(int j=0; j<arr.count; j++){
                NSDictionary *itDic=arr[j];
                HomepageReusableFillterIslandModel *model=[HomepageReusableFillterIslandModel new];
                model.model=itDic;
                [itemArr addObject:model];
                
                [self createFillterItem:itemArr];
                
                //
                if (j==0) {
                    app.hp_reflush_reusable=1;
                    [self.delegate clickFillterResult:model.data andIslandName:model.name];
                }
            }
        }
    }
    
    line=[[UIView alloc]initWithFrame:CGRectMake(lineX, 41.0f, btnWidth, 3.0f)];
    line.backgroundColor=[UIColor colorWithHexString:SeleColor];
    [self addSubview:line];
}


#pragma mark - 点击筛选按钮
-(void)clickFBtn:(id)sender{
    
    seleItemTag=100;    //每次点击还原tag=100
    
    [itemArr removeAllObjects];
    
    
    [seleFilterBtn setTitleColor:[UIColor colorWithHexString:DefaultColor] forState:UIControlStateNormal];
    seleFilterBtn.enabled=YES;
    
    
    UIButton *btn=(UIButton *)sender;
    NewReusableFillterModel *model = objc_getAssociatedObject(btn, "FillterModel");
    
    NSArray *arr=model.data;
    
    //筛选按钮行数
    if(arr.count<=4){
        [AppUtils saveValue:@"1" forKey:@"HP_FilterBtnLine"];
    }else{
        [AppUtils saveValue:@"2" forKey:@"HP_FilterBtnLine"];
    }
    
    for (int i=0; i<arr.count; i++) {
        NSDictionary *dic=arr[i];
        HomepageReusableFillterIslandModel *model=[HomepageReusableFillterIslandModel new];
        model.model=dic;
        [itemArr addObject:model];
        [self createFillterItem:itemArr];
        
        if (i==0) {
            app.hp_reflush_reusable=0;
            [self.delegate clickFillterResult:model.data andIslandName:model.name];
        }
    }
    
    [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    //设置蓝色横条位置
    CGRect frame=line.frame;
    frame.origin.x=btn.origin.x;
    [UIView animateWithDuration:0.20 animations:^{
        line.frame=frame;
    } completion:^(BOOL finished) {
        
    }];
    
    //用一个button代替当前选择的
    seleFilterBtn=btn;
}

#pragma mark - 创建筛选的子类item
-(void)createFillterItem:(NSArray *)array{
    
    for (UIView *subView in self.subviews){
        if (subView.tag>=100) {
            [subView removeFromSuperview];
        }
    }
    
    //
    CGFloat btnWidth=array.count>4?SCREENSIZE.width/4:SCREENSIZE.width/array.count;
    CGFloat btnHeight=44;
    
    for (int i=0; i<array.count; i++) {
        
        HomepageReusableFillterIslandModel *model=array[i];
        
        //计算X、Y轴
        CGFloat btnX=0.0f;
        CGFloat btnY=0.0f;
        if (i-4>=0) {
            btnX=(i-4)*btnWidth;
            btnY=btnHeight+44;
        }else{
            btnX=i*btnWidth;
            btnY=44;
        }
        
        
        ButtonItem *btn=[ButtonItem buttonWithType:UIButtonTypeCustom];
        objc_setAssociatedObject(btn, "ItemModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        btn.tag=100+i;
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
        btn.backgroundColor=[UIColor colorWithHexString:@"#f8f8f8"];
        btn.frame=CGRectMake(btnX, btnY, btnWidth, btnHeight);
        btn.titleLabel.font=kFontSize14;
        [btn setTitle:model.name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:grateColor] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        btn.titleLabel.layer.cornerRadius=4;
        btn.titleLabel.layer.masksToBounds=YES;
        [btn addTarget:self action:@selector(clickItemBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        if (seleItemTag==btn.tag) {
            seleItemBtn=btn;
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.titleLabel.backgroundColor=[UIColor colorWithHexString:SeleColor];
        }
    }
}

#pragma mark - 点击子类按钮
-(void)clickItemBtn:(id)sender{
    [seleItemBtn setTitleColor:[UIColor colorWithHexString:grateColor] forState:UIControlStateNormal];
    seleItemBtn.titleLabel.backgroundColor=[UIColor clearColor];
    seleItemBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    seleItemTag=btn.tag;
    HomepageReusableFillterIslandModel *model = objc_getAssociatedObject(btn, "ItemModel");
    app.hp_reflush_reusable=0;
    [self.delegate clickFillterResult:model.data andIslandName:model.name];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.backgroundColor=[UIColor colorWithHexString:SeleColor];
    btn.enabled=NO;
    
    //用一个button代替当前选择的
    seleItemBtn=btn;
}

@end
