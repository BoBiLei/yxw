//
//  HomePageHeaderViewCell.h
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageModel.h"

@protocol HomePageCategoryDelegate <NSObject>

-(void)clickAdvModel:(ImageModel *)model;

@end

@interface HomePageHeaderViewCell : UICollectionViewCell

@property (nonatomic, weak) id<HomePageCategoryDelegate> delegate;

@end
