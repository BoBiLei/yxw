//
//  HomePageHeaderViewCell.m
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageHeaderViewCell.h"
#import "HomePageCategoryCell.h"


@interface HomePageHeaderViewCell ()<SDCycleScrollViewDelegate>

@end

@implementation HomePageHeaderViewCell{
    
    //
    NSMutableArray *advImageArr;
    SDCycleScrollView *cycleScroll;
    
    //游侠秀
    UIImageView *showImg_left01;
    UIImageView *showImg_left02;;
    UIImageView *showImg_rightTop;
    UIImageView *showImg_center01;
    UIImageView *showImg_center02;
    UIImageView *showImg_right;
    UIImageView *showImg_btm;   //底部广告
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self addSubview:[self setUpHeaderView]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpScrollerImageView:) name:HomePageImageNoti object:nil];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=IMAGEPLAY_HEIHGT+CATEGORY_HEIHGT;
    self.frame=frame;
}

-(UIView *)setUpHeaderView{
    //
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, IMAGEPLAY_HEIHGT)];
    headerView.backgroundColor=[UIColor whiteColor];
    [headerView addSubview:[self setUpImageViewDisPlayView]];
    
    return headerView;
}

#pragma mark - 设置滚动图片view (SDCycleScrollView)
-(SDCycleScrollView *)setUpImageViewDisPlayView{
    //-----------------
    advImageArr=[NSMutableArray array];
    cycleScroll = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, IMAGEPLAY_HEIHGT)];
    cycleScroll.autoScrollTimeInterval=6;//设置滚动间隔
    cycleScroll.infiniteLoop = YES;
    cycleScroll.delegate = self;
    cycleScroll.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    cycleScroll.backgroundColor=[UIColor lightGrayColor];
    //-----------------
    return cycleScroll;
}

#pragma mark - sycle scrollview delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSString *)index andAdvModel:(ImageModel *)model{
    [self.delegate clickAdvModel:model];
}


-(void)setUpScrollerImageView:(NSNotification *)noti{
    advImageArr=noti.object;
    [cycleScroll setImageURLStringsGroup:advImageArr];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
