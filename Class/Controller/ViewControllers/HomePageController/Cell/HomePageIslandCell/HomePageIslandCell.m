//
//  HomePageIslandCell.m
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageIslandCell.h"

#define IMGHEIGHT (SCREENSIZE.width)/2-14
@implementation HomePageIslandCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.height=IS_IPHONE5?140:160;
    self.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self setUpUi];
}

-(void)setUpUi{
    //图片
    _islandImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMGHEIGHT, IS_IPHONE5?110:130)];
    _islandImg.backgroundColor=[UIColor colorWithHexString:YxColor_LightBlue];
    [self addSubview:_islandImg];
    
    //推荐提示
    _zmImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 46, 46)];
    _zmImg.image=[UIImage imageNamed:@"Homepage_islandtj"];
    [_islandImg addSubview:_zmImg];
    
    //
    _titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, _islandImg.height-30, IMGHEIGHT, 30)];
    _titleLabel.backgroundColor=[UIColor colorWithHexString:@"#00356F"];
    _titleLabel.alpha=0.9f;
    _titleLabel.font=kFontSize14;
    _titleLabel.textColor=[UIColor whiteColor];
    _titleLabel.textAlignment=NSTextAlignmentCenter;
    [_islandImg addSubview:_titleLabel];
    
    //sfPrice
    _sfPrice=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(4, _islandImg.origin.y+_islandImg.height+2, 90, 30)];
    _sfPrice.font=kFontSize12;
    _sfPrice.textColor=[UIColor colorWithHexString:@"#EF662F"];
    [self addSubview:_sfPrice];
    
    //paySum
    _paySum=[[UILabel alloc]initWithFrame:CGRectMake(_islandImg.width-(8+_sfPrice.origin.x+_sfPrice.width), _sfPrice.origin.y, _sfPrice.origin.x+_sfPrice.width+4, _sfPrice.height)];
    _paySum.textAlignment=NSTextAlignmentRight;
    _paySum.textColor=[UIColor colorWithHexString:@"#959595"];
    _paySum.font=kFontSize12;
    [self addSubview:_paySum];
}

-(void)reflushTopicDataForModel:(HomePageIslandModel *)model{
    self.islandId=model.islandId;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        SDImageCache *imgCache=[SDImageCache sharedImageCache];
        if ([imgCache diskImageExistsWithKey:model.islandImg]) {
            UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
            self.islandImg.image=image;
        }else{
            [self.islandImg sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:Image_Default]];
        }
        
    });
    _titleLabel.text=model.islandTitle;
    NSString *ttStr=[NSString stringWithFormat:@"首付:%@元起",model.islandPrice];
    //设定文字的的大小
    //
    [_sfPrice setText:ttStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.islandPrice] options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize15;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];

    //
    _paySum.text=[NSString stringWithFormat:@"%@人付款",model.buySum];
}

-(void)reflushPalauDataForModel:(HomePageIslandModel *)model{
    self.islandId=model.islandId;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        SDImageCache *imgCache=[SDImageCache sharedImageCache];
        if ([imgCache diskImageExistsWithKey:model.islandImg]) {
            UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
            self.islandImg.image=image;
        }else{
            [self.islandImg sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:Image_Default]];
        }
        
    });
    _titleLabel.text=model.islandTitle;
    NSString *ttStr=[NSString stringWithFormat:@"特价:%@元起",model.totalPrice];
    //设定文字的的大小
    _sfPrice.adjustsFontSizeToFitWidth=YES;
    //
    [_sfPrice setText:ttStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.totalPrice] options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize15;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    //
    _paySum.text=[NSString stringWithFormat:@"%@人付款",model.buySum];
}

-(void)reflushSupperCartForModel:(HomePageIslandModel *)model{
    _sfPrice.frame=CGRectMake(4, _islandImg.origin.y+_islandImg.height+2, IMGHEIGHT, 30);
    self.islandId=model.islandId;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        SDImageCache *imgCache=[SDImageCache sharedImageCache];
        if ([imgCache diskImageExistsWithKey:model.islandImg]) {
            UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
            self.islandImg.image=image;
        }else{
            [self.islandImg sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:Image_Default]];
        }
    });
    _titleLabel.text=model.islandTitle;
    NSString *ttStr=[NSString stringWithFormat:@"活动价:%@元起",model.islandPrice];
    //设定文字的的大小
    //
    [_sfPrice setText:ttStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.islandPrice] options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize15;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    _paySum.text=@"";
}

@end
