//
//  HomePageIslandCell.h
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageIslandModel.h"

@interface HomePageIslandCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *zmImg;
@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,strong) UIImageView *islandImg;
@property (nonatomic,strong) UILabel *titleLabel;  //
@property (nonatomic,strong) TTTAttributedLabel *sfPrice;  //首付
@property (nonatomic,strong) UILabel *paySum;   //付款人数
@property (nonatomic,strong) UILabel *stagePriceLabel;//分期价

-(void)reflushTopicDataForModel:(HomePageIslandModel *)model;

//帕劳的
-(void)reflushPalauDataForModel:(HomePageIslandModel *)model;

//超跑自驾
-(void)reflushSupperCartForModel:(HomePageIslandModel *)model;

@end
