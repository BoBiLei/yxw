//
//  HomePageCategoryCell.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HPCategoryDelegate <NSObject>

-(void)selectedCategoryItemAtIndexPath:(NSIndexPath *)indexPath andUrl:(NSString *)url;

@end

@interface HomePageCategoryCell : UICollectionViewCell

@property (weak, nonatomic) id<HPCategoryDelegate> CategoryDelegate;

@end
