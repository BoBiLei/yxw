//
//  HomePageCategoryCell.m
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageCategoryCell.h"
#import "CateoryItemCell.h"


@interface HomePageCategoryCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end


@implementation HomePageCategoryCell{
    NSMutableArray *categoryDataArr;
    UICollectionView *homePageCollection;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=SCREENSIZE.width/2+8;
    self.frame=frame;
    
    //
    UIImageView *bg=[[UIImageView alloc] initWithFrame:self.frame];
    bg.image=[UIImage imageNamed:@"category_bg"];
    bg.userInteractionEnabled=YES;
    [self addSubview:bg];
    
    //
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myLayout.minimumLineSpacing=0.0f;
    myLayout.minimumInteritemSpacing=0.0f;
    homePageCollection=[[UICollectionView alloc]initWithFrame:bg.frame collectionViewLayout:myLayout];
    [bg addSubview:homePageCollection];
    [homePageCollection registerNib:[UINib nibWithNibName:@"CateoryItemCell" bundle:nil] forCellWithReuseIdentifier:@"CateoryItemCell"];
    homePageCollection.backgroundColor=[UIColor clearColor];
    homePageCollection.dataSource=self;
    homePageCollection.delegate=self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCateArr:) name:HomePageCategoryNoti object:nil];
}

-(void)getCateArr:(NSNotification *)noti{
    
    categoryDataArr=[NSMutableArray array];
    NSArray *arr=noti.object;
    
    for (int i=0; i<arr.count; i++) {
        NSDictionary *dic=arr[i];
        HomePageCagegoryModel *model=[[HomePageCagegoryModel alloc]init];
        model.model=dic;
        switch (i) {
            case 0:
                model.img=@"category_01";
                break;
            case 1:
                model.img=@"category_02";
                break;
            case 2:
                model.img=@"category_03";
                break;
            case 3:
                model.img=@"category_04";
                break;
            case 4:
                model.img=@"category_05";
                break;
            case 5:
                model.img=@"category_06";
                break;
            case 6:
                model.img=@"category_07";
                break;
            case 7:
                model.img=@"category_08";
                break;
                
            default:
                break;
        }
        [categoryDataArr addObject:model];
    }
    
    [homePageCollection reloadData];
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return categoryDataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CateoryItemCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CateoryItemCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[CateoryItemCell alloc]init];
    }
    HomePageCagegoryModel *model=categoryDataArr[indexPath.row];
    cell.model=model;
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREENSIZE.width-32)/4, (SCREENSIZE.width-32)/4);
}


//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 8;
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 8, 8, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    HomePageCagegoryModel *model=categoryDataArr[indexPath.row];
    [self.CategoryDelegate selectedCategoryItemAtIndexPath:indexPath andUrl:model.url];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
