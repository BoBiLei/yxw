//
//  CateoryItemCell.h
//  youxia
//
//  Created by mac on 16/4/19.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageCagegoryModel.h"

@interface CateoryItemCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UIImageView *imgv;

@property (nonatomic, retain) HomePageCagegoryModel *model;

@end
