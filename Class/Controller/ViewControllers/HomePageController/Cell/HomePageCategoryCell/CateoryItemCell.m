//
//  CateoryItemCell.m
//  youxia
//
//  Created by mac on 16/4/19.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CateoryItemCell.h"

@implementation CateoryItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //
    _imgv=[[UIImageView alloc]initWithFrame:CGRectZero];
    [self addSubview:_imgv];
    
    //
    _title=[[UILabel alloc] initWithFrame:CGRectZero];
    _title.font=[UIFont systemFontOfSize:IS_IPHONE5?12:13];
    _title.textAlignment=NSTextAlignmentCenter;
    _title.textColor=[UIColor whiteColor];
    _title.adjustsFontSizeToFitWidth=YES;
    [self addSubview:_title];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame=self.frame;
    frame.size.width=(SCREENSIZE.width-32)/4;
    frame.size.height=(SCREENSIZE.width-32)/4;
    self.frame=frame;
    _imgv.frame=CGRectMake(12, 2, self.width-24, self.height-24);
    _title.frame=CGRectMake(_imgv.origin.x, _imgv.origin.y+_imgv.height, _imgv.width, 21);
}

#pragma  mark - - - -
-(void)setModel:(HomePageCagegoryModel *)model{
    self.title.text=model.title;
    [self.imgv setImage:[UIImage imageNamed:model.img]];
}

@end
