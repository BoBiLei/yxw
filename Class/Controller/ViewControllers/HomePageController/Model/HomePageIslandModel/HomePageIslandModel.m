//
//  HomePageIslandModel.m
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HomePageIslandModel.h"

@implementation HomePageIslandModel

-(void)setModel:(NSDictionary *)model{
    self.islandId=model[@"id"];
    self.islandPId=model[@"pid"];
    self.mdId=model[@"mdid"];
    self.islandDescript=model[@"description"];
    self.islandImg=model[@"thumb"];
    self.islandTitle=model[@"title"];
    self.navTitle=model[@"title"];
    self.islandPrice=[NSString stringWithFormat:@"%@",model[@"p_f_price"]];
    self.totalPrice=[NSString stringWithFormat:@"%@",model[@"p_price"]];
    self.buySum=[NSString stringWithFormat:@"%@",model[@"yd_num"]];
    self.posids=model[@"posids"];
    self.lvType=model[@"lvtype"];
}


@end
