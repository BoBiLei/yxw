//
//  HomePageIslandModel.h
//  youxia
//
//  Created by mac on 15/12/2.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageIslandModel : NSObject

@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,copy) NSString *islandPId;
@property (nonatomic,copy) NSString *mdId;
@property (nonatomic,copy) NSString *islandDescript;
@property (nonatomic,copy) NSString *islandImg;
@property (nonatomic,copy) NSString *islandTitle;
@property (nonatomic,copy) NSString *islandPrice;
@property (nonatomic,copy) NSString *totalPrice;
@property (nonatomic,copy) NSString *navTitle;
@property (nonatomic,copy) NSString *buySum;
@property (nonatomic,copy) NSString *posids;    //是否推荐
@property (nonatomic,copy) NSString *lvType;    //自由行有分期价

@property (nonatomic,copy) NSString *islandName;

@property (nonatomic,copy) NSDictionary *model;

@end
