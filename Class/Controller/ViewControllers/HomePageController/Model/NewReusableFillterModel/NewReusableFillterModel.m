//
//  NewReusableFillterModel.m
//  youxia
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewReusableFillterModel.h"

@implementation NewReusableFillterModel

-(void)setModel:(NSDictionary *)model{
    self.name=model[@"name"];
    self.data=model[@"list"];
}

@end
