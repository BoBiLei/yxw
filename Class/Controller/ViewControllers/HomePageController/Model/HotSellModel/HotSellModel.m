//
//  HotSellModel.m
//  youxia
//
//  Created by mac on 15/12/14.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "HotSellModel.h"

@implementation HotSellModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.catId=[NSString stringWithFormat:@"%@",dic[@"catid"]];
    self.hsId=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.mdId=[NSString stringWithFormat:@"%@",dic[@"id"]];
    self.pId=[NSString stringWithFormat:@"%@",dic[@"pid"]];
    self.hsImg=[NSString stringWithFormat:@"%@",dic[@"bpic"]];
    self.hsTitle=[NSString stringWithFormat:@"%@",dic[@"title"]];
    self.hsDescription=[NSString stringWithFormat:@"%@",dic[@"description"]];
    self.hsPrice=[NSString stringWithFormat:@"%@",dic[@"p_price"]];
    self.hsStagePrice=[NSString stringWithFormat:@"%@",dic[@"p_f_price"]];
}

@end
