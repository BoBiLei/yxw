//
//  HotSellModel.h
//  youxia
//
//  Created by mac on 15/12/14.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotSellModel : NSObject

@property (nonatomic,copy) NSString *catId;
@property (nonatomic,copy) NSString *hsId;
@property (nonatomic,copy) NSString *pId;
@property (nonatomic,copy) NSString *mdId;
@property (nonatomic,copy) NSString *hsImg;
@property (nonatomic,copy) NSString *hsTitle;
@property (nonatomic,copy) NSString *hsDescription;
@property (nonatomic,copy) NSString *hsPrice;
@property (nonatomic,copy) NSString *hsStagePrice;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

@end
