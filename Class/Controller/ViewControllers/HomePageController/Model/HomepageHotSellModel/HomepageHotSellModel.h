//
//  HomepageHotSellModel.h
//  youxia
//
//  Created by mac on 16/5/10.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomepageHotSellModel : NSObject

@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *name2;
@property(nonatomic,copy) NSString *pic;
@property(nonatomic,copy) NSString *url;
@property(nonatomic,retain) NSDictionary *model;
@end
