//
//  HomePageCagegoryModel.h
//  youxia
//
//  Created by mac on 15/12/1.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageCagegoryModel : NSObject

@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *url;
@property (nonatomic,retain) NSDictionary *model;

@end
