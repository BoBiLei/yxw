//
//  HomepageStrategyModel.h
//  youxia
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomepageStrategyModel : NSObject

@property (nonatomic,copy) NSString *strateId;
@property (nonatomic,copy) NSString *cId;
@property (nonatomic,copy) NSString *strateImg;
@property (nonatomic,copy) NSString *strateTitle;
@property (nonatomic,copy) NSString *strateTime;
@property (nonatomic,copy) NSString *strateUrl;
@property (nonatomic,copy) NSString *views;
@property (nonatomic,copy) NSString *zan;

@property (nonatomic,retain) NSDictionary *model;

@end
