//
//  HomepageStrategyModel.m
//  youxia
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HomepageStrategyModel.h"

@implementation HomepageStrategyModel

-(void)setModel:(NSDictionary *)model{
    self.strateId=model[@"id"];
    self.cId=model[@"catid"];
    self.strateImg=model[@"thumb"];
    self.strateTitle=model[@"title"];
    self.strateTime=model[@"inputtime"];
    self.strateUrl=model[@"url"];
    self.views=model[@"views"];
    self.zan=model[@"zan"];
}

@end
