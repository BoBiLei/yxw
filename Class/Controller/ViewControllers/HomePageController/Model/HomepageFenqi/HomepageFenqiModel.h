//
//  HomepageFenqiModel.h
//  youxia
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomepageFenqiModel : NSObject

@property (nonatomic, copy) NSString     *banner;
@property (nonatomic, copy) NSString     *url;

@property (nonatomic, copy) NSDictionary *model;

@end
