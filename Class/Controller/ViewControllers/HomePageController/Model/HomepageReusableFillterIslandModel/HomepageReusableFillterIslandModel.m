//
//  HomepageReusableFillterIslandModel.m
//  youxia
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HomepageReusableFillterIslandModel.h"

@implementation HomepageReusableFillterIslandModel

-(void)setModel:(NSDictionary *)model{
    self.name=model[@"name"];
    self.data=model[@"data"];
}

@end
