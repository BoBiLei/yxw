//
//  HomepageReusableFillterIslandModel.h
//  youxia
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomepageReusableFillterIslandModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) NSArray *data;

@property (nonatomic, retain) NSDictionary *model;

@end
