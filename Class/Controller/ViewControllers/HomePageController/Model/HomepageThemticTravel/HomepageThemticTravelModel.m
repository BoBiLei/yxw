//
//  HomepageThemticTravelModel.m
//  youxia
//
//  Created by mac on 16/5/10.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HomepageThemticTravelModel.h"

@implementation HomepageThemticTravelModel

-(void)setModel:(NSDictionary *)model{
    self.name=model[@"name"];
    self.pic=model[@"pic"];
    self.url=model[@"url"];
}

@end
