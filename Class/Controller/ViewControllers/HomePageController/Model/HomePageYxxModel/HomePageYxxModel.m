//
//  HomePageYxxModel.m
//  youxia
//
//  Created by mac on 16/1/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HomePageYxxModel.h"

@implementation HomePageYxxModel

-(void)setModel:(NSDictionary *)model{
    self.xId=model[@"id"];
    self.cid=model[@"catid"];
    self.xxName=model[@"type"];
    self.xImgs=model[@"thumb"];
    self.xTitle=model[@"title"];
    self.xTime=model[@"inputtime"];
    self.xUrl=model[@"url"];
    self.xScan=model[@"views"];
}

@end
