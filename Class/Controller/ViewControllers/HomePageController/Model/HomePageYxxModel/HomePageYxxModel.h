//
//  HomePageYxxModel.h
//  youxia
//
//  Created by mac on 16/1/9.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageYxxModel : NSObject

@property (nonatomic, copy) NSString *xId;
@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *xxName;
@property (nonatomic, copy) NSString *xImgs;
@property (nonatomic, copy) NSString *xTitle;
@property (nonatomic, copy) NSString *xTime;
@property (nonatomic, copy) NSString *xUrl;
@property (nonatomic, copy) NSString *xScan;

@property (nonatomic, retain) NSDictionary *model;

@end
