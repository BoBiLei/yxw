//
//  ServierOnlineController.m
//  youxia
//
//  Created by mac on 16/1/11.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ServierOnlineController.h"

@interface ServierOnlineController ()<UIWebViewDelegate>

@end

@implementation ServierOnlineController{
    UIWebView *_webView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"产品咨询页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"产品咨询页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self setUpCustomSearBar];
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"在线咨询";
    [self.view addSubview:navBar];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.backgroundColor=[UIColor whiteColor];
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.bounces=NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    [self.view addSubview:_webView];
    
    //
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestHttpsForUrl];
        }
    };
}

#pragma mark - webview delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    static BOOL isRequestWeb = YES;
    
    if (isRequestWeb) {
        NSHTTPURLResponse *response = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
        
        DSLog(@"%ld",(long)response.statusCode);
        if (response.statusCode == 404) {
            
            return NO;
        } else if (response.statusCode == 403) {
            
            return NO;
        }
        
        [webView loadData:data MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[request URL]];
        
        isRequestWeb = NO;
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [AppUtils showProgressInView:self.view];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [AppUtils dismissHUDInView:self.view];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [AppUtils dismissHUDInView:self.view];
}

#pragma mark Request 请求
-(void)requestHttpsForUrl{
    
    [[NetWorkRequest defaultClient] requestWithPath:ZixunUrl method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"error"]) {
            
        }else{
            
            NSString *urlStr=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"zx_url"]];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            
            //1.创建request
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            
            //2.创建一个 NSMutableURLRequest 添加 header
            NSMutableURLRequest *mutableRequest = [request mutableCopy];
            
            [mutableRequest addValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
            [mutableRequest addValue:@"Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0" forHTTPHeaderField:@"User-Agent"];
            
            //时间戳
            NSString *time_stamp;
            time_t now;
            time(&now);
            time_stamp  = [NSString stringWithFormat:@"%ld", now];
            //            DSLog(@"%@",time_stamp);
            
            NSString *str=[NSString stringWithFormat:@"BRIDGE_HOTKEY=E; Hm_lvt_8db5c93095eb9ce1eecafd9c1ff57ab5=1462244366,1462244857; Hm_lvt_2d2ed72c744e301be04d113503ce96c9=1462244371; BAIDUID=C9077252ED7FFD402C8F9AA8FCDC91AC:FG=1; BIDUPSID=1E6F8CE8FE072A793225F1185E20575B; PSTM=%@; QIAO_CK_3242464_R=http%%3A//www.baidu.com/link%%3Furl%%3DoOZ1XJJsimn1sbvJID-pFPBGw-SVx2rT9JE-8y3bqyu%%26wd%%3D%%26eqid%%3Dadc46eb100447b800000000657281405; __ag_cm_=1462244894785; ag_fid=0MmA41zgGIVnIvqF;uc_login_unique=9868b355d3e770249a2eb135976fc483; H_PS_PSSID=18880_19507_1456_19671_18241_19782_17942_19805_19900_19559_19808_19843_19902_17001_15599_11969_10633; DDSOS_cdb5637fea1cf242690a5140=VbKWFmx+oZuJ7dI753pAFg==; BDSFRCVID=yO8sJeCCxG3xdooR25POO6E5XI7HzsMMdzFs3J; H_BDCLCKID_SF=tR3-sJoq2RbhKROvhjRBXTkyyxom3bvxt5bbWb74tD5m8pACQjoiyj-n2lbuW43E3jAeaDcJ-J8XMKtxD53P; SIGNIN_UC=70a2711cf1d3d9b1a82d2f87d633bd8a02122591799; SFSSID=0ec68be5481c1ac7cbeb35c9fddee5c2;Hm_lpvt_8db5c93095eb9ce1eecafd9c1ff57ab5=1462248965;Hm_lpvt_2d2ed72c744e301be04d113503ce96c9=1462244879",time_stamp];
            [mutableRequest addValue:str forHTTPHeaderField:@"Cookie"];
            
            //3.把值覆给request
            request = [mutableRequest copy];
            
            //4.查看请求头
            //            DSLog(@"%@", request.allHTTPHeaderFields);
            
            [_webView loadRequest:request];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
