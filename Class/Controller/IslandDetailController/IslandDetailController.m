//
//  IslandDetailController.m
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "IslandDetailController.h"
#import "NewTravelPlanController.h"
#import "WelcomeController.h"
#import "ServierOnlineController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIEditorViewStyle.h>

#define NavigaBar_Height 64
@interface IslandDetailController ()<UIWebViewDelegate,UIScrollViewDelegate>

@end

@implementation IslandDetailController{
    
    CGFloat topContentInset;
    
    //
    UINavigationBar *cusBar;
    UILabel *titleLabel;
    UIImageView *searchView;
    
    UIWebView *webView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"产品详情页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"产品详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickBuyBtn) name:@"GoIslandDetail" object:nil];
    
    topContentInset = IMAGEPLAY_HEIHGT;
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    DSLog(@"\n%@\n%@\n%@\n%@\n%@",_islandId,_islandImg,_islandName,_islandDescript,_islandPId);
    [self setUpWebView];
    
    [self setUpCustomSearBar];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    //
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"产品详情";
    titleLabel.hidden=YES;
    [self.view addSubview:titleLabel];
    
    
    //分享
    UIButton *shareBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-40, 25, 34, 34)];
    [shareBtn setImage:[UIImage imageNamed:@"isld_share"] forState:UIControlStateNormal];
    shareBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [shareBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
    [self.view addSubview:shareBtn];
    [shareBtn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickShareBtn:(id)sender{
    
    //自定义视图标题颜色，字体大小
    [SSUIEditorViewStyle setTitleColor:[UIColor whiteColor]];
    [SSUIEditorViewStyle setTitle:@"分享到新浪"];
    
    
    //1、创建分享参数（必要）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSArray *imgArr=@[[NSString stringWithFormat:@"%@",_islandImg]];
    NSString *imgUrl;
    switch (_detail_landType) {
        case MaldivesIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/maldives/tours/%@.html",_islandId];
            break;
        case BaliIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/bali/tours/%@.html",_islandId];
            break;
        case PalauIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/palau/tours/%@.html",_islandId];
            break;
        default:
            break;
    }
    [shareParams SSDKSetupShareParamsByText:_islandDescript
                                     images:imgArr
                                        url:[NSURL URLWithString:imgUrl]
                                      title:_islandName
                                       type:SSDKContentTypeAuto];
   // 2、分享
    [ShareSDK showShareActionSheet:self.view
                             items:nil
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   DSLog(@"%@",error);
                   switch (state) {

                       case SSDKResponseStateSuccess:{
                           [AppUtils showSuccessMessage:@"分享成功！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateFail:{
                           [AppUtils showSuccessMessage:@"分享失败！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateCancel:{
                           
                           break;
                       }
                       default:
                           break;
                   }
               }];
}

#pragma mark - init WebView
-(void)setUpWebView{
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-74+25)];
    webView.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    webView.scrollView.bounces = YES;
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    webView.paginationMode = UIWebPaginationModeUnpaginated;
    webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSString *urlStr;
    switch (self.detail_landType) {
        case MaldivesIslandType:
            urlStr=[NSString stringWithFormat:@"%@maldives/tours/%@.html?is_iso=1&is_v6=1",DefaultHost,self.islandId];
            break;
        case BaliIslandType:
            urlStr=[NSString stringWithFormat:@"%@bali/tours/%@.html?is_v6=1&is_iso=1&is_v6=1",DefaultHost,self.islandId];
            break;
        case PalauIslandType:
            urlStr=[NSString stringWithFormat:@"%@palau/tours/%@.html?is_v6=1&is_iso=1&is_v6=1",DefaultHost,self.islandId];
            break;
        default:
            break;
    }
    NSURL *url = [NSURL URLWithString:urlStr];
    [self.view addSubview:webView];
    webView.delegate = self;
    webView.scrollView.delegate=self;
    
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [webView loadRequest:[NSURLRequest requestWithURL:url]];
        }
    };
}

-(void)setUpBottomViewTest{
    //
    UIView *btmBiew=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-74, SCREENSIZE.width, 74)];
    btmBiew.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btmBiew];
    
    //imgv
    UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, btmBiew.width, 25)];
    imgv.image=[UIImage imageNamed:@"isld_btline"];
    [btmBiew addSubview:imgv];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, imgv.width, 1.2f)];
    line.backgroundColor=[UIColor colorWithHexString:@"#ffeda8"];
    [btmBiew addSubview:line];
    
    //
    UILabel *tttLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.width, 23.8f)];
    tttLabel.font=kFontSize11;
    tttLabel.textAlignment=NSTextAlignmentCenter;
    tttLabel.textColor=[UIColor colorWithHexString:@"#909090"];
    [btmBiew addSubview:tttLabel];
    
    NSTextAttachment *attachMent = [[NSTextAttachment alloc] init];
    
    attachMent.image = [UIImage imageNamed:@"isld_btmtip"];
    
    CGFloat height = tttLabel.font.lineHeight;
    attachMent.bounds = CGRectMake(0, -3, height, height);
    
    NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:attachMent];
    
    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] init];
    [strM appendAttributedString:attrString];
    [strM appendAttributedString: [[NSAttributedString alloc] initWithString: @" 支持分期付款，满六期即可获赠游侠秀套餐"]];
    tttLabel.backgroundColor = [UIColor clearColor];
    tttLabel.attributedText = strM;
    
    
    //leftBtn
    UIButton *leftBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn01.frame=CGRectMake(0, imgv.height, btmBiew.width/3, 49);
    leftBtn01.backgroundColor=[UIColor colorWithHexString:@"#595959"];
    leftBtn01.titleLabel.font=kFontSize16;
    [leftBtn01 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn01 setImage:[UIImage imageNamed:@"isld_btm01"] forState:UIControlStateNormal];
    [leftBtn01 setTitle:@"咨询" forState:UIControlStateNormal];
    [leftBtn01 setTitleEdgeInsets:UIEdgeInsetsMake(0, 14, 0, 0)];
    [leftBtn01 addTarget:self action:@selector(clickConsultBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:leftBtn01];
    
    UIButton *leftBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn02.frame=CGRectMake(btmBiew.width/3, imgv.height, btmBiew.width/3, 49);
    leftBtn02.backgroundColor=leftBtn01.backgroundColor;
    leftBtn02.titleLabel.font=leftBtn01.titleLabel.font;
    [leftBtn02 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn02 setImage:[UIImage imageNamed:@"collect_nor"] forState:UIControlStateNormal];
    [leftBtn02 setTitle:@"收藏" forState:UIControlStateNormal];
    [leftBtn02 setImageEdgeInsets:UIEdgeInsetsMake(0, -49, 0, 0)];
    [leftBtn02 setTitleEdgeInsets:UIEdgeInsetsMake(0, -35, 0, 0)];
    [leftBtn02 addTarget:self action:@selector(clickConsultBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:leftBtn02];
    
    //rightBtn
    UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(btmBiew.width/3*2, imgv.height, btmBiew.width/3, leftBtn01.height);
    rightBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
    rightBtn.titleLabel.font=kFontSize16;
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitle:@"立即预定" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:rightBtn];
}

-(void)setUpBottomView{
    //
    UIView *btmBiew=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-74+25, SCREENSIZE.width, 74-25)];
    btmBiew.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btmBiew];
    
    //imgv
//    UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, btmBiew.width, 25)];
//    imgv.image=[UIImage imageNamed:@"isld_btline"];
//    [btmBiew addSubview:imgv];
//    
//    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, imgv.width, 1.2f)];
//    line.backgroundColor=[UIColor colorWithHexString:@"#ffeda8"];
//    [btmBiew addSubview:line];
//    
//    //
//    UILabel *tttLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgv.width, 23.8f)];
//    tttLabel.font=[UIFont systemFontOfSize:11];
//    tttLabel.textAlignment=NSTextAlignmentCenter;
//    tttLabel.textColor=[UIColor colorWithHexString:@"#909090"];
//    [btmBiew addSubview:tttLabel];
//    
//    NSTextAttachment *attachMent = [[NSTextAttachment alloc] init];
//    
//    attachMent.image = [UIImage imageNamed:@"isld_btmtip"];
//    
//    CGFloat height = tttLabel.font.lineHeight;
//    attachMent.bounds = CGRectMake(0, -3, height, height);
//    
//    NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:attachMent];
//    
//    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] init];
//    [strM appendAttributedString:attrString];
//    [strM appendAttributedString: [[NSAttributedString alloc] initWithString: @" 支持分期付款，满六期即可获赠游侠秀套餐"]];
//    tttLabel.backgroundColor = [UIColor clearColor];
//    tttLabel.attributedText = strM;
    
    
    //leftBtn
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame=CGRectMake(0, 0, btmBiew.width/2, 49);
    leftBtn.backgroundColor=[UIColor colorWithHexString:@"#595959"];
    leftBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"isld_btm01"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"咨询" forState:UIControlStateNormal];
    [leftBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 14, 0, 0)];
    [leftBtn addTarget:self action:@selector(clickConsultBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:leftBtn];
    
    //rightBtn
    UIButton *rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(leftBtn.frame.origin.x+leftBtn.width, 0, btmBiew.width/2, leftBtn.height);
    rightBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
    rightBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitle:@"立即抢购" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    [btmBiew addSubview:rightBtn];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    [self setUpBottomView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [AppUtils dismissHUDInView:self.view];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [AppUtils dismissHUDInView:self.view];
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY<0) {
        searchView.hidden=YES;
    }else{
        searchView.hidden=NO;
    }
    
    if (offsetY<-44) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((IMAGEPLAY_HEIHGT-NavigaBar_Height - offsetY-32) / topContentInset));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
        if (alpha==1) {
            titleLabel.hidden=NO;
        }else{
            titleLabel.hidden=YES;
        }
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
}

#pragma mark - 点击在线客服
-(void)clickConsultBtn{
    ServierOnlineController *serv=[[ServierOnlineController alloc]init];
    [self.navigationController pushViewController:serv animated:YES];
}

#pragma mark - 点击立即抢购
-(void)clickBuyBtn{
    if ([AppUtils loginState]) {
        NewTravelPlanController *travelPlan=[[NewTravelPlanController alloc]init];
        travelPlan.pId=self.islandPId;
        travelPlan.mdId=self.mdId;
        travelPlan.islandId=self.islandId;
        travelPlan.islandName=self.islandName;
        travelPlan.islandType=self.detail_landType;
        [self.navigationController pushViewController:travelPlan animated:YES];
    }else{
        WelcomeController *login=[[WelcomeController alloc]init];
        login.isGoIslandDetail=YES;
        UINavigationController *logNav=[[UINavigationController alloc]initWithRootViewController:login];
        [logNav.navigationBar setShadowImage:[UIImage new]];
        [self presentViewController:logNav animated:YES completion:nil];
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
