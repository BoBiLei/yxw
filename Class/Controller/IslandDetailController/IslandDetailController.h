//
//  IslandDetailController.h
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

//海岛主题类型
typedef NS_ENUM(NSUInteger, IslandType) {
    MaldivesIslandType,
    BaliIslandType,
    PalauIslandType
};

@interface IslandDetailController : UIViewController

@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,copy) NSString *islandImg;
@property (nonatomic,copy) NSString *islandName;
@property (nonatomic,copy) NSString *islandDescript;
@property (nonatomic,copy) NSString *islandPId;
@property (nonatomic,copy) NSString *mdId;
@property (nonatomic,copy) NSString *nav_supercarTitle;
@property (nonatomic) IslandType detail_landType;

@end
