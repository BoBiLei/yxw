//
//  HotelListModel.m
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelListModel.h"

@implementation HotelListModel

//-(void)jsonDataForDictionary:(NSDictionary *)dic{
//    self.islandId=dic[@"id"];
//    self.pId=dic[@"pid"];
//    self.mdId=dic[@"mdid"];
//    self.islandImg=dic[@"pic_position"];
//    self.islandType=dic[@"lvtype"];
//    self.islandTitle=dic[@"title"];
//    self.islandPrice=dic[@"p_price"];
//    self.stagePrice=dic[@"p_f_price"];
//    self.islandYwName=dic[@"mudi_name"];
//    self.starLevel=dic[@"xingji"];
//    self.jiaotong=dic[@"jiaotong"];
//}

-(void)setModel:(NSDictionary *)model{
    if ([model[@"Address"] isKindOfClass:[NSNull class]]) {
        self.address=@"";
    }else{
        self.address=[NSString stringWithFormat:@"%@",model[@"Address"]];
    }
    
    if ([model[@"HotelID"] isKindOfClass:[NSNull class]]) {
        self.hoteid=@"";
    }else{
        self.hoteid=[NSString stringWithFormat:@"%@",model[@"HotelID"]];
    }
    if ([model[@"HotelName"] isKindOfClass:[NSNull class]]) {
        self.hotename=@"";
    }else{
        self.hotename=[NSString stringWithFormat:@"%@",model[@"HotelName"]];
    }
    if ([model[@"LowestPrice"] isKindOfClass:[NSNull class]]) {
        self.lowestPrice=@"";
    }else{
        self.lowestPrice=[NSString stringWithFormat:@"%@",model[@"LowestPrice"]];
    }
    if ([model[@"HotelType"] isKindOfClass:[NSNull class]]) {
        self.HotelType=@"";
    }else{
        self.HotelType=[NSString stringWithFormat:@"%@",model[@"HotelType"]];
    }
    if ([model[@"HotelLogo"] isKindOfClass:[NSNull class]]) {
        self.hotelogo=@"";
    }else{
        self.hotelogo=model[@"HotelLogo"];
    }
    
}

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.address=dic[@"Address"];
    self.hoteid=dic[@"HotelID"];
    self.hotename=dic[@"HotelName"];
    self.lowestPrice=dic[@"LowestPrice"];
    self.HotelType=dic[@"HotelType"];
    self.hotelogo=dic[@"HotelLogo"];

}
@end
