//
//  HoteLabTitleCell.h
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HoteStandardModel.h"

@interface HoteLabTitleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;


-(void)reflushData:(HoteStandardModel *)model atIndexPath:(NSIndexPath *)indexPath;

@end
