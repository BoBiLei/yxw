//
//  HotelSumChildCell.m
//  youxia
//
//  Created by mac on 2017/5/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelSumChildCell.h"

@implementation HotelSumChildCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickAddBtn:(id)sender {
    NSString *str=self.childSumLabel.text;
    if (str.intValue<3) {
        UITableView *tableView = (UITableView *)self.superview.superview;
        NSIndexPath *indexPath = [tableView indexPathForCell:self];
        [self.delegate clickAddChildBtnAtIndexPatch:indexPath];
    }
}
- (IBAction)clickJianBtn:(id)sender {
    NSString *str=self.childSumLabel.text;
    if (str.intValue>0) {
        UITableView *tableView = (UITableView *)self.superview.superview;
        NSIndexPath *indexPath = [tableView indexPathForCell:self];
        [self.delegate clickJianChildBtnAtIndexPatch:indexPath];
    }
}

-(void)reflushSumData:(NSInteger)labelSum{
    self.childSumLabel.text=[NSString stringWithFormat:@"%ld",labelSum];
    if (labelSum>=3) {
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
    
    if (labelSum<=0) {
        self.jianBtn.enabled=NO;
    }else{
        self.jianBtn.enabled=YES;
    }
}

@end
