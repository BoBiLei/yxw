//
//  HotelreservationModel.m
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelreservationModel.h"

@implementation HotelreservationModel
-(void)setModel:(NSDictionary *)model
{
    if ([model[@"Name_CN"]isKindOfClass:[NSNull class]]) {
        self.name_cn=@"";
    }else
    {
        self.name_cn=model[@"Name_CN"];
    }
}
@end
