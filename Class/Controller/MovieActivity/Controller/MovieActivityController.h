//
//  MovieActivityController.h
//  youxia
//
//  Created by mac on 2017/1/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageModel.h"
#import <WebKit/WebKit.h>

@interface MovieActivityController : UIViewController<WKNavigationDelegate>

@property (nonatomic,strong) ImageModel *model;

@end
