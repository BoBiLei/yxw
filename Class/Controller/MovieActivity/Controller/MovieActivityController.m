//
//  MovieActivityController.m
//  youxia
//
//  Created by mac on 2017/1/2.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "MovieActivityController.h"
#import "WebViewJavascriptBridge.h"
#import "PickingSeatController.h"

@interface MovieActivityController ()

@property WebViewJavascriptBridge* bridge;

@property (strong, nonatomic) UIProgressView *progressView;

@end

@implementation MovieActivityController{
    WKWebView* myWebView;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    FilmNavigationBar *navBar=[[FilmNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"活动详情";
    [self.view addSubview:navBar];
}
-(void)viewDidDisappear:(BOOL)animated{
    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@""]]];
    [myWebView removeAllSubviews];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [self deleteWebCache];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    if (_bridge) { return; }
    myWebView = [[NSClassFromString(@"WKWebView") alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    myWebView.navigationDelegate = self;
    [myWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.view addSubview:myWebView];
    //
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 63, SCREENSIZE.width, 1.5)];
    _progressView.trackTintColor=[UIColor whiteColor];
    _progressView.progressTintColor=[UIColor colorWithHexString:@"#0080c5"];
    [self.view addSubview:_progressView];
    
    _bridge = [WebViewJavascriptBridge bridgeForWebView:myWebView];
    [_bridge setWebViewDelegate:self];
    
    
    [_bridge registerHandler:@"ObjcCallback" handler:^(id data, WVJBResponseCallback responseCallback) {
        if ([data isKindOfClass:[NSDictionary class]]) {
            /*
             *  返回必须传  foretellId  cinemaId 两个ID
             */
            NSLog(@"返回: %@", data);
            GouPiaoInfoModel *model=[GouPiaoInfoModel new];
            model.foretellId=data[@"foretellId"];
            model.cinemaId=@"1518154279";
            
            PickingSeatController *ctr=[PickingSeatController new];
            ctr.gpModel=model;
            ctr.isActive=YES;
            [self.navigationController pushViewController:ctr animated:YES];
        }
    }];
    [self loadExamplePage:myWebView];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
}

- (void)loadExamplePage:(WKWebView*)webView {
//    NSString* htmlPath = [[NSBundle mainBundle] pathForResource:@"ExampleApp" ofType:@"html"];
//    NSString* appHtml = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
//    NSURL *baseURL = [NSURL fileURLWithPath:htmlPath];
//    [webView loadHTMLString:appHtml baseURL:baseURL];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_model.imgId]]];
    NSLog(@"%@---%@",_model.imgId,[NSURL URLWithString:_model.imgId]);
}

// 计算进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == myWebView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            _progressView.hidden = YES;
            [_progressView setProgress:0 animated:NO];
        }else {
            _progressView.hidden = NO;
            [_progressView setProgress:myWebView.estimatedProgress animated:YES];
        }
    }
}

#pragma mark - 清除缓存
- (void)deleteWebCache {
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
        
        NSSet *websiteDataTypes
        
        = [NSSet setWithArray:@[
                                
//                                WKWebsiteDataTypeDiskCache,
                                
//                                WKWebsiteDataTypeOfflineWebApplicationCache,
                                
//                                WKWebsiteDataTypeMemoryCache,
                                
//                                WKWebsiteDataTypeLocalStorage,
                                
                                WKWebsiteDataTypeCookies,
                                
//                                WKWebsiteDataTypeSessionStorage,
                                
//                                WKWebsiteDataTypeIndexedDBDatabases,
                                
//                                WKWebsiteDataTypeWebSQLDatabases
                                
                                ]];
        
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
            // Done
        }];
    } else {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        
        NSError *errors;
        
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        
    }
}

// 记得取消监听
- (void)dealloc {
    [myWebView removeObserver:self forKeyPath:@"estimatedProgress"];
}

@end
