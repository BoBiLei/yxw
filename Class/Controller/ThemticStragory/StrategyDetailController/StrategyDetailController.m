//
//  StrategyDetailController.m
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "StrategyDetailController.h"
#import <WebKit/WebKit.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIEditorViewStyle.h>

@interface StrategyDetailController ()<WKNavigationDelegate, WKUIDelegate>

@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) UIProgressView *progressView;

@end

@implementation StrategyDetailController{
    UINavigationBar *cusBar;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"攻略详情页"];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"攻略详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
//    navBar.titleStr=self.strategyTitle;
    navBar.titleStr=@"攻略详情";
    [self.view addSubview:navBar];
    
    //添加分享按钮
    UIButton *shareBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-40, 26, 34, 34)];
    [shareBtn setImage:[UIImage imageNamed:@"isld_share"] forState:UIControlStateNormal];
    shareBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [shareBtn setImageEdgeInsets:UIEdgeInsetsMake(3, 6, 0, 0)];
    [self.view addSubview:shareBtn];
    [shareBtn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113)];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    [self.view addSubview:_webView];
    
    _progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(0, 61, CGRectGetWidth(self.view.frame),2)];
    _progressView.trackTintColor=[UIColor colorWithHexString:@"#0080c5"];
    _progressView.progressTintColor=[UIColor whiteColor];
    [self.view addSubview:_progressView];
    
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew| NSKeyValueObservingOptionOld context:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@index.php?m=wap&c=index&a=show&catid=%@&id=%@&is_v6=1&is_iso=1",DefaultHost,self.cId,self.idStr]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(7, 28, 25.f, 25.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    self.navReturnBtn=back;
    [self.view addSubview:back];
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor whiteColor];
    title.font=kFontSize16;
    title.text=self.strategyTitle;
    [self.view addSubview:title];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickShareBtn:(id)sender{
    
    //自定义视图标题颜色，字体大小
    [SSUIEditorViewStyle setTitleColor:[UIColor whiteColor]];
    [SSUIEditorViewStyle setTitle:@"分享到新浪"];
    
    //1、创建分享参数（必要）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSArray *imgArr=@[[NSString stringWithFormat:@"%@",_strategyImgStr]];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@palau/strategy/%@.html?is_iso=1",DefaultHost,self.idStr]];
    [shareParams SSDKSetupShareParamsByText:_strategyDescription
                                     images:imgArr
                                        url:url
                                      title:_strategyTitle
                                       type:SSDKContentTypeAuto];
    // 2、分享
    [ShareSDK showShareActionSheet:self.view
                             items:nil
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   DSLog(@"%@",error);
                   switch (state) {
                           
                       case SSDKResponseStateSuccess:{
                           [AppUtils showSuccessMessage:@"分享成功！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateFail:{
                           [AppUtils showSuccessMessage:@"分享失败！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateCancel:{
                           
                           break;
                       }
                       default:
                           break;
                   }
               }];
}


#pragma mark - webview delegate
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [AppUtils dismissHUDInView:self.view];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    DSLog(@"3");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
//
//
//页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    DSLog(@"%s", __FUNCTION__);
    [_progressView setProgress:0.0 animated:false];
}

//加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
    DSLog(@"%s", __FUNCTION__);
}

//接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    
    DSLog(@"%s", __FUNCTION__);
}

//在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
}

//在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqual: @"estimatedProgress"] && object == _webView) {
        [self.progressView setAlpha:1.0f];
        [self.progressView setProgress:_webView.estimatedProgress animated:YES];
        if(_webView.estimatedProgress >= 1.0f)
        {
            [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc {
    
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    
    [_webView setNavigationDelegate:nil];
    [_webView setUIDelegate:nil];
}

@end
