//
//  StrategyDetailController.h
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

@interface StrategyDetailController : BaseController

@property (nonatomic,copy) NSString *strategyTitle;
@property (nonatomic,copy) NSString *strategyDescription;
@property (nonatomic,copy) NSString *strategyImgStr;
@property (nonatomic,copy) NSString *idStr;
@property (nonatomic,copy) NSString *cId;

@end
