//
//  StrategyModel.h
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StrategyModel : NSObject

@property (nonatomic,copy) NSString *islandYwName;

@property (nonatomic,copy) NSString *strateId;
@property (nonatomic,copy) NSString *cId;
@property (nonatomic,copy) NSString *strateImg;
@property (nonatomic,copy) NSString *strateTitle;
@property (nonatomic,copy) NSString *strateTime;
@property (nonatomic,copy) NSString *strateDetail;
@property (nonatomic,copy) NSString *strateType;
@property (nonatomic,copy) NSString *strateZan;     //点赞
@property (nonatomic,copy) NSString *strateViews;   //浏览

@property (nonatomic,retain) NSDictionary *model;

@end
