//
//  StrategyModel.m
//  youxia
//
//  Created by mac on 15/12/3.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "StrategyModel.h"

@implementation StrategyModel

/*
 10马代
 29帕劳
 36毛里求斯
 30其他
 */

-(void)setModel:(NSDictionary *)model{
    self.strateId=model[@"id"];
    self.cId=model[@"catid"];
    self.strateImg=model[@"thumb"];
    self.strateTitle=model[@"title"];
    self.strateTime=model[@"inputtime"];
    self.strateDetail=model[@"description"];
    self.strateType=model[@"ltype"];
    self.strateZan=model[@"zan"];
    self.strateViews=model[@"views"];
}

@end
