//
//  ThemticStragoryController.m
//  youxia
//
//  Created by mac on 16/4/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ThemticStragoryController.h"
#import "NewTravelStrategyCell.h"


@interface ThemticStragoryController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,DOPDropDownMenuDataSource,DOPDropDownMenuDelegate,ZanSuccessDelegate>

@end

@implementation ThemticStragoryController{
    
    NSInteger page;
    
    NSMutableArray *dataArr;
    UICollectionView *myCollection;
    
    //
    //筛选id
    NSString *fillId01;
    NSString *fillId02;
    
    NSArray *starLevelArr;
    NSArray *goIslandTypeArr;
    DOPDropDownMenu *myMenu;
    
    MJRefreshAutoNormalFooter *footer;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"游记攻略列表页"];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"游记攻略列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    page=2;
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=self.titleStr;
    [self.view addSubview:navBar];
    
    [self setUpDopMenu];
    
    [self initCollectionView];
    
    [self requestStrategoryData];
}

#pragma mark - init CollectionView
-(void)initCollectionView{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    myCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, myMenu.height+64, SCREENSIZE.width, SCREENSIZE.height-64-myMenu.height-49) collectionViewLayout:myLayout];
    myCollection.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    myCollection.dataSource=self;
    myCollection.delegate=self;
    [myCollection registerNib:[UINib nibWithNibName:@"NewTravelStrategyCell" bundle:nil] forCellWithReuseIdentifier:@"NewTravelStrategyCell"];
    
    [self.view addSubview:myCollection];
 
    
    //
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = kFontSize14;
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    myCollection.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (page!=0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestStrategoryDataForType:fillId01 mudi:fillId02 andPage:page];
        });
    }
}

#pragma mark - setup CustomDropDownMenu
-(void)setUpDopMenu{
    
    switch (self.type) {
        case MaldStragoryType:
        {
            fillId01=@"medf";
            starLevelArr=@[@{@"id":@"medf",@"name":@"马尔代夫"}];
        }
            
            break;
        case PalauStragoryType:
        {
            fillId01=@"pl";
            starLevelArr=@[@{@"id":@"pl",@"name":@"帕劳"}];
        }
            break;
        case AllStragoryType:
        {
            fillId01=@"all";
            starLevelArr=@[@{@"id":@"all",@"name":@"全部"},
                           @{@"id":@"medf",@"name":@"马尔代夫"},
                           @{@"id":@"bld",@"name":@"巴厘岛"},
                           @{@"id":@"pl",@"name":@"帕劳"},
                           @{@"id":@"mlqs",@"name":@"毛里求斯"},
                           @{@"id":@"other",@"name":@"其他目的地"},];
        }
            break;
            
        default:
            break;
    }
    fillId02=@"0";
    
    goIslandTypeArr = @[@{@"id":@"0",@"name":@"全部属性"},
                        @{@"id":@"1",@"name":@"游记"},
                        @{@"id":@"2",@"name":@"美食"},
                        @{@"id":@"3",@"name":@"娱乐"},
                        @{@"id":@"4",@"name":@"浮潜"},
                        @{@"id":@"5",@"name":@"酒店"},
                        @{@"id":@"6",@"name":@"注意事项"},];
    
    // 添加下拉菜单
    myMenu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    myMenu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    myMenu.textColor=[UIColor colorWithHexString:@"#686868"];
    myMenu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    myMenu.delegate = self;
    myMenu.dataSource = self;
    [myMenu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:myMenu];
}


#pragma mark -DOPMenu Delegate
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu{
    return 2;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column{
    if (column == 0) {
        return starLevelArr.count;
    }else{
        return goIslandTypeArr.count;
    }
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath{
    if (indexPath.column == 0) {
        return starLevelArr[indexPath.row][@"name"];
    }else {
        return goIslandTypeArr[indexPath.row][@"name"];
    }
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath{
    
    dataArr=[NSMutableArray array];
    
    page=1;  //默认第一页（每次点击都把page设置为第一页）
    
    if (indexPath.column == 0) {
        fillId01=starLevelArr[indexPath.row][@"id"];
    }else{
        fillId02=goIslandTypeArr[indexPath.row][@"id"];
    }
    
    [self requestStrategoryDataForType:fillId02 mudi:fillId01 andPage:page];
}

#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NewTravelStrategyCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTravelStrategyCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[NewTravelStrategyCell alloc]init];
    }
    StrategyModel *model=dataArr[indexPath.row];
    cell.model=model;
    cell.delegate=self;
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width*150/300)+126);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//每个item之间的间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 12.0f;
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(12, 0, 12, 0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    StrategyModel *model=dataArr[indexPath.row];
    StrategyDetailController *strCtr=[StrategyDetailController new];
    strCtr.strategyTitle=model.strateTitle;
    strCtr.strategyDescription=model.strateDetail;
    strCtr.strategyImgStr=model.strateImg;
    strCtr.idStr=model.strateId;
    strCtr.cId=model.cId;
    [self.navigationController pushViewController:strCtr animated:YES];
}

#pragma mark - 点赞成功 Delegate
-(void)zanSuccess{
//    [self updateRequestDataForType:fillId02 mudi:fillId01 andPage:page];
}

#pragma mark - Request 请求
-(void)requestStrategoryData{
    dataArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    [[NetWorkRequest defaultClient] requestWithPath:self.urlStr method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"strategy_list"];
            for (NSDictionary *dic in arr) {
                StrategyModel *model=[StrategyModel new];
                model.model=dic;
                [dataArr addObject:model];
            }
            [myCollection reloadData];
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 筛选 请求
-(void)requestStrategoryDataForType:(NSString *)type mudi:(NSString *)mudi andPage:(NSInteger)cpage{
    DSLog(@"%ld--",(long)page);
    NSDictionary *dic=@{@"is_iso":@"1",
                        @"m":@"wap",
                        @"c":@"index",
                        @"a":@"list_strategy_v6_ios",
                        @"page":[NSString stringWithFormat:@"%ld",(long)cpage],
                        @"type":type,
                        @"mudi":mudi};
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dic prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"strategy_list"];
            for (NSDictionary *dic in arr) {
                StrategyModel *model=[StrategyModel new];
                model.model=dic;
                [dataArr addObject:model];
            }
            
            [myCollection reloadData];
            
            //
            if (arr.count!=0) {
                page++;
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 筛选 请求
//-(void)updateRequestDataForType:(NSString *)type mudi:(NSString *)mudi andPage:(NSInteger)cpage{
//    dataArr=[NSMutableArray array];
//    NSDictionary *dic=@{@"is_iso":@"1",
//                        @"m":@"wap",
//                        @"c":@"index",
//                        @"a":@"list_strategy_v6_ios",
//                        @"page":[NSString stringWithFormat:@"%ld",cpage],
//                        @"type":type,
//                        @"mudi":mudi};
//    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dic prepareExecute:^{
//        
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        //DSLog(@"%@",responseObject);
//        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
//            NSArray *arr=responseObject[@"retData"][@"strategy_list"];
//            for (NSDictionary *dic in arr) {
//                StrategyModel *model=[StrategyModel new];
//                model.model=dic;
//                [dataArr addObject:model];
//            }
//            
//            [myCollection reloadData];
//            
//            [footer endRefreshing];
//            
//        }else{
//            DSLog(@"请求数据异常");
//        }
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        DSLog(@"%@",error);
//    }];
//}

@end
