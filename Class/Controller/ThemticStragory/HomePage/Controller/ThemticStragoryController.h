//
//  ThemticStragoryController.h
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ThemticType) {
    MaldStragoryType,
    PalauStragoryType,
    AllStragoryType,
};

@interface ThemticStragoryController : UIViewController

@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,copy) NSString *urlStr;

@property (nonatomic, assign) ThemticType type;

@end
