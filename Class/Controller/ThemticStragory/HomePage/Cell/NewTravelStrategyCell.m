//
//  NewTravelStrategyCell.m
//  youxia
//
//  Created by mac on 16/4/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewTravelStrategyCell.h"

@implementation NewTravelStrategyCell{
    UIImageView *tipImgv;
    UILabel *tipLabel;
    UILabel *timeLabel;
    UIImageView *bigImgv;
    UILabel *titleLabel;
    UILabel *descriptLabel;
    UIView *line;
    
    //底部三个按钮
    UIButton *btn01;
    UIButton *btn02;
    UIButton *btn03;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor=[UIColor whiteColor];
    
    CGFloat height=(SCREENSIZE.width*150/300)+126;
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=height;
    self.frame=frame;
    
    //
    UIView *mainView=[[UIView alloc]initWithFrame:CGRectMake(9, 0, SCREENSIZE.width-18, self.height)];
    mainView.layer.masksToBounds=YES;
    mainView.layer.cornerRadius=1;
    mainView.backgroundColor=[UIColor whiteColor];
    [self addSubview:mainView];
    
    //类属图标
    tipImgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 27, 27)];
    tipImgv.layer.masksToBounds=YES;
    tipImgv.layer.cornerRadius=tipImgv.height/2;
    [mainView addSubview:tipImgv];
    
    //
    tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(tipImgv.origin.x+tipImgv.width+8, tipImgv.origin.y+2, mainView.width-(tipImgv.origin.x+tipImgv.width+16), tipImgv.height/2)];
    tipLabel.textColor=[UIColor colorWithHexString:@"#383838"];
    tipLabel.font=kFontSize14;
    [mainView addSubview:tipLabel];
    
    //时间
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(tipLabel.origin.x, tipLabel.origin.y+tipLabel.height+1, tipLabel.width, tipLabel.height-1)];
    timeLabel.textColor=[UIColor colorWithHexString:@"#686868"];
    timeLabel.font=kFontSize12;
    [mainView addSubview:timeLabel];
    
    //大图
    bigImgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, tipImgv.origin.y+tipImgv.height+8, mainView.width, SCREENSIZE.width*150/300)];
    bigImgv.contentMode=UIViewContentModeScaleToFill;
    [mainView addSubview:bigImgv];
    
    //内容标题
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, bigImgv.origin.y+bigImgv.height+2, mainView.width-16, 20)];
    titleLabel.contentMode=UIViewContentModeScaleAspectFill;
    titleLabel.textColor=[UIColor colorWithHexString:@"#383838"];
    titleLabel.font=kFontSize13;
    [mainView addSubview:titleLabel];
    
    //小标题
    descriptLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, titleLabel.origin.y+titleLabel.height, mainView.width-16, 15)];
    descriptLabel.textColor=[UIColor colorWithHexString:@"#686868"];
    descriptLabel.font=kFontSize12;
    [mainView addSubview:descriptLabel];
    
    //
    line=[[UIView alloc] initWithFrame:CGRectMake(0, descriptLabel.origin.y+descriptLabel.height+5, mainView.width, 0.6f)];
    line.backgroundColor=[[UIColor colorWithHexString:@"#878787"] colorWithAlphaComponent:0.7];
    [mainView addSubview:line];
    
    [self createButtonToView:mainView];
}


#pragma mark - SetUp Button
-(CGFloat)createButtonToView:(UIView *)view{
    
    CGFloat endY=0;
    
    CGFloat btnX=0;
    CGFloat btnY=line.origin.y+line.height;
    CGFloat btnW=view.width/3;
    CGFloat btnH=40;
    
    for (int i=0; i<3; i++) {
        
        btnX=i*btnW;
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor=[UIColor whiteColor];
        btn.frame=CGRectMake(btnX, btnY, btnW, btnH);
        btn.titleLabel.font=kFontSize11;
        
        NSString *imgStr;
        if (i==0) {
            imgStr=@"travelstr_share";
            btn.imageEdgeInsets=UIEdgeInsetsMake(0, 8, 0, 0);
            btn01=btn;
        }else if (i==1){
            imgStr=@"travelstr_comment";
            btn.imageEdgeInsets=UIEdgeInsetsMake(0, -8, 0, 0);
            btn02=btn;
        }else if (i==2){
            imgStr=@"travelstr_zan";
            btn.imageEdgeInsets=UIEdgeInsetsMake(0, -8, 0, 0);
            btn03=btn;
        }
        [btn setImage:[UIImage imageNamed:imgStr] forState:UIControlStateNormal];
        
        btn.tag=i;
        [btn setTitleColor:[UIColor colorWithHexString:@"#747474"] forState:UIControlStateNormal];
        
        [view addSubview:btn];
        
        //
        if (i!=2) {
            UIView *btLine=[[UIView alloc]initWithFrame:CGRectMake(btn.origin.x+btn.width-0.7f, btn.origin.y+5, 0.6f, btn.height-10)];
            btLine.backgroundColor=[[UIColor colorWithHexString:@"#878787"] colorWithAlphaComponent:0.7];
            
            endY=btLine.origin.y+btLine.height;
            DSLog(@"%f",btLine.origin.y+btLine.height);
            
            [view insertSubview:btLine aboveSubview:btn];
        }
    }
    return  endY;
}

-(void)setModel:(StrategyModel *)model{
    
    /*
     cid:
        10-马代
        29-帕劳
        36-毛里求斯
        30-其他
     
     strateType:
        0-全部
        1-游记
        2-美食
        3-娱乐
        4-浮潜
        5-酒店
        6-注意事项
     */
    
    NSString *nameStr01;
    if ([model.cId isEqualToString:@"10"]) {
        nameStr01=@"马尔代夫";
    }else if ([model.cId isEqualToString:@"29"]){
        nameStr01=@"帕劳";
    }else if ([model.cId isEqualToString:@"30"]){
        nameStr01=@"毛里求斯";
    }else if ([model.cId isEqualToString:@"36"]){
        nameStr01=@"其他目的地";
    }
    
    NSString *nameStr02;
    NSString *imgStr;
    if ([model.strateType isEqualToString:@"0"]) {
        nameStr02=@"全部";
        imgStr=@"newstrategy_01";
    }else if ([model.strateType isEqualToString:@"1"]){
        nameStr02=@"游记";
        imgStr=@"newstrategy_01";
    }else if ([model.strateType isEqualToString:@"2"]){
        nameStr02=@"美食";
        imgStr=@"newstrategy_02";
    }else if ([model.strateType isEqualToString:@"3"]){
        nameStr02=@"娱乐";
        imgStr=@"newstrategy_06";
    }else if ([model.strateType isEqualToString:@"4"]){
        nameStr02=@"浮潜";
        imgStr=@"newstrategy_05";
    }else if ([model.strateType isEqualToString:@"5"]){
        nameStr02=@"酒店";
        imgStr=@"newstrategy_03";
    }else if ([model.strateType isEqualToString:@"6"]){
        nameStr02=@"注意事项";
        imgStr=@"newstrategy_04";
    }
    
    tipImgv.image=[UIImage imageNamed:imgStr];
    
    NSString *nameStr=[NSString stringWithFormat:@"%@-%@",nameStr01,nameStr02];
    tipLabel.text=nameStr;
    
    //
    //
    NSString *timeStr=[AppUtils handleTimeStampStringToTime:model.strateTime];
    timeLabel.text=timeStr;
    
    //
    //
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.strateImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.strateImg];
        bigImgv.image=image;
    }else{
        [bigImgv sd_setImageWithURL:[NSURL URLWithString:model.strateImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    
    //
    //
    titleLabel.text=model.strateTitle;
    
    //
    //
    descriptLabel.text=model.strateDetail;
    
    //
    //
    [btn02 setTitle:model.strateViews forState:UIControlStateNormal];
    
    [btn03 setTitle:model.strateZan forState:UIControlStateNormal];
    objc_setAssociatedObject(btn03, "threeObject", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    btn03.backgroundColor=[UIColor orangeColor];
    [btn03 addTarget:self action:@selector(clickeZanBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    //
    //
    objc_setAssociatedObject(btn01, "firstShareObject", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [btn01 addTarget:self action:@selector(clickeShareBtn:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 点击点赞
-(void)clickeZanBtn:(id)sender{
    UIButton *btn=sender;
    StrategyModel *model = objc_getAssociatedObject(btn, "threeObject");
    [self dianzanRequestForModel:model forButton:btn];
}

#pragma mark - 点赞请求
-(void)dianzanRequestForModel:(StrategyModel *)model forButton:(UIButton *)button{
    
    NSDictionary *dic=@{
                        @"is_iso":@"1",
                        @"m":@"wap",
                        @"c":@"index",
                        @"a":@"addzan_ios",
                        @"catid":model.cId,
                        @"id":model.strateId
                        };
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?"] method:HttpRequestPost parameters:dic prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //DSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            if ([responseObject[@"retData"][@"data"][@"add"] isEqualToString:@"1"]) {
                
                NSString *str=button.titleLabel.text;
                NSInteger strInt=str.integerValue+1;
                [button setTitle:[NSString stringWithFormat:@"%ld",(long)strInt] forState:UIControlStateNormal];
                
                //[self showHeartNamed:@"btn_close_date"];
                
                [self.delegate zanSuccess];
            }else{
                [AppUtils showSuccessMessage:@"已点过赞" inView:self];
            }
        }else{
            DSLog(@"请求数据异常");
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - 点击分享
-(void)clickeShareBtn:(UIButton *)sender{
    UIButton *btn=sender;
    StrategyModel *model = objc_getAssociatedObject(btn, "firstShareObject");
    [self shareEventWithId:model.strateId image:model.strateImg name:model.strateTitle descript:model.strateDetail];
//    DSLog(@"%@",model.strateTitle);
}

-(void)shareEventWithId:(NSString *)strateId image:(NSString *)imgStr name:(NSString *)name descript:(NSString *)descript{
    
    //1、创建分享参数（必要）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSArray *imgArr=@[[NSString stringWithFormat:@"%@",imgStr]];
    NSString *imgUrl;
    imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/palau/strategy/%@.html",strateId];
    [shareParams SSDKSetupShareParamsByText:descript
                                     images:imgArr
                                        url:[NSURL URLWithString:imgUrl]
                                      title:name
                                       type:SSDKContentTypeAuto];
    // 2、分享
    [ShareSDK showShareActionSheet:self.superview
                             items:nil
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   switch (state) {
                           
                       case SSDKResponseStateSuccess:{
                           [AppUtils showSuccessMessage:@"分享成功！" inView:self.superview];
                           break;
                       }
                       case SSDKResponseStateFail:{
                           [AppUtils showSuccessMessage:@"分享失败！" inView:self.superview];
                           break;
                       }
                       case SSDKResponseStateCancel:{
                           
                           break;
                       }
                       default:
                           break;
                   }
               }];
}

#pragma mark 点赞动画效果
- (void)showHeartNamed:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    imageView.frame = CGRectOffset(btn03.frame, 0, -20);
    [self addSubview:imageView];
    [self animateImage:imageView];
}

- (void)animateImage:(UIImageView *)animationView{
    animationView.alpha = 1.0f;
    // 调整心形位置
    CGRect imageFrame = animationView.frame;
    CGPoint viewOrigin = animationView.frame.origin;
    viewOrigin.y = viewOrigin.y + imageFrame.size.height / 2.0f-4;
    viewOrigin.x = viewOrigin.x + imageFrame.size.width / 2.0f;
    animationView.frame = imageFrame;
    animationView.layer.position = viewOrigin;
    
    // 动画：渐变
    CABasicAnimation *fadeOutAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [fadeOutAnimation setToValue:[NSNumber numberWithFloat:0.0]];
    fadeOutAnimation.fillMode = kCAFillModeForwards;
    fadeOutAnimation.removedOnCompletion = NO;
    
    // 动画：位置
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, viewOrigin.x, viewOrigin.y);
    CGFloat btnPoint_y = btn03.frame.origin.y;
    
    //增加三个水平方向的拐点
    CGPathAddLineToPoint(curvedPath, NULL, btn03.frame.origin.x+btn03.frame.size.width/2, btnPoint_y - 64.f);
    
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    [group setAnimations:@[fadeOutAnimation, pathAnimation]];
    group.duration = 0.8f;
//    group.delegate = self;
    [group setValue:animationView forKey:@"imageViewBeingAnimated"];
    [animationView.layer addAnimation:group forKey:@"groupAnimation"];
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    //动画结束后，将view从界面中清除
    [[anim valueForKey:@"imageViewBeingAnimated"] removeFromSuperview];
}

@end
