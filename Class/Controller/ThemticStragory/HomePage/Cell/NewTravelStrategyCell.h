//
//  NewTravelStrategyCell.h
//  youxia
//
//  Created by mac on 16/4/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrategyModel.h"


@protocol ZanSuccessDelegate <NSObject>

-(void)zanSuccess;

@end

@interface NewTravelStrategyCell : UICollectionViewCell

@property (nonatomic, retain) StrategyModel *model;
@property (nonatomic, weak) id<ZanSuccessDelegate> delegate;

@end
