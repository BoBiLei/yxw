//
//  HoteStandardModel.h
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HoteStandardModel : NSObject

@property(nonatomic,copy)NSString *DefaultOccupancy;
@property(nonatomic,copy)NSString *MaxOccupancy;
@property(nonatomic,copy)NSString *bedtype;
@property(nonatomic,copy)NSString *breakfast;
@property(nonatomic,copy)NSString *receiveChild;
@property(nonatomic,retain)NSArray *roomFacility;

-(void)jsonToModel:(NSDictionary *)dic;

@end
