//
//  MoreChoicenessController.m
//  youxia
//
//  Created by mac on 16/6/1.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "MoreChoicenessController.h"
#import "NewIslandOnlineCell.h"
#import "MoreChoicenessCell.h"

#define  NiocellHeight SCREENSIZE.width/3-12
@interface MoreChoicenessController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MoreChoicenessController{
    BaseNavigationBar *navBar;
    NSMutableArray *dataArr;
    NSString *headUrlStr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [self.view addSubview:navBar];
    
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestDistingceData];
        }
    };
}


-(void)setUpTableView{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}


#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return SCREENSIZE.width/2-20;
    }else{
        return NiocellHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.00000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        MoreChoicenessCell *cell=[tableView dequeueReusableCellWithIdentifier:@"mcell"];
        if (!cell) {
            cell=[[MoreChoicenessCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.urlStr=headUrlStr;
        return  cell;
    }else{
        NewIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[NewIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (dataArr.count!=0) {
            DistinceShowModel *model=dataArr[indexPath.row-1];
            cell.model=model;
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row!=0) {
        DistinceShowModel *model=dataArr[indexPath.row-1];
        
        IslandDetailController *islDetail=[[IslandDetailController alloc] init];
        if ([model.islandYwName isEqualToString:@"帕劳"]) {
            islDetail.detail_landType=PalauIslandType;
        }else{
            islDetail.detail_landType=MaldivesIslandType;
        }
        islDetail.islandName=model.islandTitle;
        islDetail.islandId=model.islandId;
        islDetail.islandImg=model.islandImg;
        islDetail.islandPId=model.pId;
        islDetail.mdId=model.islandId;
        islDetail.islandDescript=model.islandTitle;
        [self.navigationController pushViewController:islDetail animated:YES];
    }
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestDistingceData{
    
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    
    dataArr=[NSMutableArray array];
    
    [[NetWorkRequest defaultClient] requestWithPath:self.urlStr method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            navBar.titleStr=responseObject[@"retData"][@"title"];
            
            headUrlStr=responseObject[@"retData"][@"thumb"];
            
            //
            NSArray *arr=responseObject[@"retData"][@"list"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                model.islandYwName=dic[@"dy"];
                [dataArr addObject:model];
            }
            [self setUpTableView];
            [myTable reloadData];
        }else{
            DSLog(@"请求数据异常");
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
