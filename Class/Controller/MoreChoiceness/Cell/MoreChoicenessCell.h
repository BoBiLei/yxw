//
//  MoreChoicenessCell.h
//  youxia
//
//  Created by mac on 16/6/1.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreChoicenessCell : UITableViewCell

@property (nonatomic ,copy) NSString *urlStr;

@end
