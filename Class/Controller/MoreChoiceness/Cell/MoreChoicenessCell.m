//
//  MoreChoicenessCell.m
//  youxia
//
//  Created by mac on 16/6/1.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "MoreChoicenessCell.h"

@implementation MoreChoicenessCell{
    UIImageView *imgv;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width/2-20)];
        [self addSubview:imgv];
    }
    return self;
}

-(void)setUrlStr:(NSString *)urlStr{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:urlStr]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:urlStr];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:urlStr]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
