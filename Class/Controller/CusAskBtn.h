//
//  CusImageTitleBtn.h
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CusAskBtn : UIButton

@property (nonatomic, strong) UIButton *imgv;

@property (nonatomic, strong) UILabel *cusTitleLabel;

-(id)initWithFrame:(CGRect)frame title:(NSString *)title image:(UIImage *)image;

@end
