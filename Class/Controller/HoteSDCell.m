//
//  HoteSDCell.m
//  youxia
//
//  Created by mac on 2017/3/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteSDCell.h"

@implementation HoteSDCell
@synthesize starDates,endDates,cDate;
- (void)awakeFromNib {
    [super awakeFromNib];
//    [self optiondate];
    // Initialization code
}

//获取当前时间和后天时间
-(void)optiondate
{
    
    NSDate *  senddate=[NSDate date];//当前时间
    NSDate *nextDay = [NSDate dateWithTimeInterval:24*60*60 sinceDate:senddate];//后一天
    
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    
    
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString *nextString=[dateformatter stringFromDate:nextDay];
    starDates.text=locationString;
    endDates.text=nextString;
    cDate.text=@"1";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)refluStarD:(NSString *)starStr endDate:(NSString *)endDate totalDays:(NSString *)days{
    starDates.text=starStr;
    endDates.text=endDate;
    cDate.text=days;
}


@end
