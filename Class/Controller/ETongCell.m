//
//  ETongCell.m
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ETongCell.h"

@implementation ETongCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)clickAdd:(id)sender {
    NSString *str=self.countnum.text;
    if (str.intValue<3) {
        UITableView *tableView = (UITableView *)self.superview.superview;
        NSIndexPath *indexPath = [tableView indexPathForCell:self];
        NSLog(@"%ld",indexPath.section);
        [self.delegate clickAddBtnAtIndexPatch:indexPath];
    }
}

- (IBAction)clickJian:(id)sender {
    NSString *str=self.countnum.text;
    if (str.intValue>1) {
        UITableView *tableView = (UITableView *)self.superview.superview;
        NSIndexPath *indexPath = [tableView indexPathForCell:self];
        NSLog(@"%ld",indexPath.section);
        [self.delegate clickJianBtnAtIndexPatch:indexPath];
    }
}

-(void)reflushData:(NSInteger)labelSum{
    self.countnum.text=[NSString stringWithFormat:@"%ld",labelSum];
    NSString *str=self.countnum.text;
    if (str.intValue>=3) {
        self.addBtn.enabled=NO;
    }else{
        self.addBtn.enabled=YES;
    }
    
    if (str.intValue<=1) {
        self.jianBtn.enabled=NO;
    }else{
        self.jianBtn.enabled=YES;
    }
}

@end
