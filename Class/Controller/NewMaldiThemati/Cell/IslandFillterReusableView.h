//
//  IslandFillterReusableView.h
//  youxia
//
//  Created by mac on 16/4/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ThemticFillterSeleArrayDelegate <NSObject>

-(void)clickFillterResult:(NSArray *)array;

-(void)clickLeftBtn;

@end

@interface IslandFillterReusableView : UICollectionReusableView

@property (nonatomic, weak) id<ThemticFillterSeleArrayDelegate> delegate;

@end
