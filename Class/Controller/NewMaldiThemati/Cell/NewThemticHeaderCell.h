//
//  NewThemticHeaderCell.h
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemticRecommendModel.h"
#import "ImageModel.h"

@protocol ThemticBarnerEventDelegate <NSObject>

-(void)clickThemticBarner:(ImageModel *)model;

@end

@protocol ThemticRecommendDelegate <NSObject>

-(void)clickThemticRecommend:(ThemticRecommendModel *)model;

@end

@interface NewThemticHeaderCell : UICollectionViewCell

@property (nonatomic, strong) ThemticRecommendModel *model;

@property (nonatomic, weak) id<ThemticBarnerEventDelegate> barnerClickDelegate;
@property (nonatomic, weak) id<ThemticRecommendDelegate> themticRecommendDelegate;

@end
