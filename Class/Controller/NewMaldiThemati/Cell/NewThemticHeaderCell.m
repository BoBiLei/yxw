//
//  NewThemticHeaderCell.m
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewThemticHeaderCell.h"
#import <UIButton+WebCache.h>

@interface NewThemticHeaderCell ()<SDCycleScrollViewDelegate>

@end

@implementation NewThemticHeaderCell{
    //
    NSMutableArray *advImageArr;
    SDCycleScrollView *cycleScroll;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGRect frame=self.frame;
    frame.size.width=SCREENSIZE.width;
    frame.size.height=Themtic_IMAGEPLAY_HEIHGT;
    self.frame=frame;
    [self addSubview:[self setUpHeaderView]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpScrollerImageView:) name:ThemitPageImageNoti object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpTjwView:) name:ThemitTJWImageNoti object:nil];
}

-(void)setUpTjwView:(NSNotification *)noti{
    NSArray *arr=noti.object;
    
    CGFloat btnX=0;
    CGFloat btnY=self.height-78;
    CGFloat btnWidth=self.width/arr.count;
    CGFloat btnHeight=64;
    
    for (int i=0; i<arr.count; i++) {
        
        btnX=btnWidth*i;
        
        ThemticRecommendModel *model=arr[i];
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        objc_setAssociatedObject(btn, "RecommendModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        btn.titleLabel.font=kFontSize13;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.imageView.contentMode=UIViewContentModeScaleAspectFit;
        [btn sd_setImageWithURL:[NSURL URLWithString:model.pic] forState:UIControlStateNormal];
        btn.frame=CGRectMake(btnX, btnY, btnWidth, btnHeight);
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
}

-(void)clickBtn:(id)sender{
    UIButton *btn=sender;
    ThemticRecommendModel *model = objc_getAssociatedObject(btn, "RecommendModel");
    [self.themticRecommendDelegate clickThemticRecommend:model];
}

-(UIView *)setUpHeaderView{
    //
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, Themtic_IMAGEPLAY_HEIHGT)];
    headerView.backgroundColor=[UIColor whiteColor];
    [headerView addSubview:[self setUpImgScrollerView]];
    
    return headerView;
}

#pragma mark - 设置滚动图片view (SDCycleScrollView)
-(SDCycleScrollView *)setUpImgScrollerView{
    //-----------------
    advImageArr=[NSMutableArray array];
    cycleScroll = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, Themtic_IMAGEPLAY_HEIHGT)];
    cycleScroll.autoScrollTimeInterval=6;//设置滚动间隔
    cycleScroll.infiniteLoop = YES;
    cycleScroll.delegate = self;
    cycleScroll.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    cycleScroll.backgroundColor=[UIColor lightGrayColor];
    cycleScroll.pageControlAliment=SDCycleScrollViewPageContolAlimentTop;
    //-----------------
    return cycleScroll;
}


-(void)setUpScrollerImageView:(NSNotification *)noti{
    advImageArr=noti.object;
    [cycleScroll setImageURLStringsGroup:advImageArr];
}

#pragma mark - sycleScrollView Delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSString *)index andAdvModel:(ImageModel *)model{
    [self.barnerClickDelegate clickThemticBarner:model];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
