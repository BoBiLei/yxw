//
//  IslandFillterReusableView.m
//  youxia
//
//  Created by mac on 16/4/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "IslandFillterReusableView.h"
#import "ButtonItem.h"
#import "ThemticIslandStarFillterModel.h"
#import "HomePageIslandModel.h"

#define SeleColor @"#0080c5"
#define DefaultColor @"#686868"
#define grateColor @"#a0a0a0"

@implementation IslandFillterReusableView{
    
    AppDelegate *app;
    
    UIView *btLine;
    NSMutableArray *fillterBtnArr;
    UIButton *seleFilterBtn;
    UIButton *seleItemBtn;
    
    NSMutableArray *dataArr;
    
    ThemticIslandStarFillterModel *selectedModel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    //
    UIImageView *tipImgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 12, 27, 27)];
    tipImgv.image=[UIImage imageNamed:@"mdReusable_fillter"];
    [self addSubview:tipImgv];
    
    //
    UILabel *titleLabel=[[UILabel alloc]initWithFrame: CGRectMake(tipImgv.origin.x+tipImgv.width+6, tipImgv.origin.y, 80, tipImgv.height)];
    titleLabel.font=kFontSize16;
    titleLabel.text=@"星级岛屿";
    titleLabel.textColor=[UIColor colorWithHexString:@"#2a2a2a"];
    [self addSubview:titleLabel];
    
    //moreButton
    UIButton *moreButton=[UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame=CGRectMake(SCREENSIZE.width-67, titleLabel.origin.y, 65, titleLabel.height);
    moreButton.titleLabel.font=kFontSize16;
    [moreButton setTitleColor:[UIColor colorWithHexString:@"#a0a0a0"] forState:UIControlStateNormal];
    [moreButton setTitle:@"更多>" forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(cliclMoreBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreButton];
    
    //
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 43.4, SCREENSIZE.width, 0.6f)];
    line.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [self addSubview:line];
    
    [self requestButtonData];
}

- (void)cliclMoreBtnEvent{
    [self.delegate clickLeftBtn];
}

-(void)setUpTopBtnForArray:(NSArray *)arr{
    
    app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    CGFloat btnWidth=SCREENSIZE.width/arr.count;
    CGFloat btnHeihgt=44;
    CGFloat lineX=0;
    for (int i=0; i<arr.count; i++) {
        
        ThemticIslandStarFillterModel *model=arr[i];
        
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag=i;
        btn.frame=CGRectMake(i*btnWidth, 44, btnWidth, btnHeihgt);
        btn.titleLabel.font=kFontSize15;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitle:model.name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:DefaultColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        objc_setAssociatedObject(btn, "ThemticFillterModel", model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        if (i==0) {
            lineX=btn.origin.x;
            seleFilterBtn=btn;
            selectedModel=model;        //默认第一个model
            [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
            
            //获取默认的
            dataArr=[NSMutableArray array];
            NSArray *aarr=selectedModel.xinpin;
            for (NSDictionary *dic in aarr) {
                HomePageIslandModel *model=[HomePageIslandModel new];
                model.model=dic;
                model.islandName=selectedModel.name;
                [dataArr addObject:model];
            }
            app.md_reflush_reusable=1;
            [self.delegate clickFillterResult:dataArr];
        }
    }
    
    btLine=[[UIView alloc]initWithFrame:CGRectMake(lineX, 85.0f, btnWidth, 3.0f)];
    btLine.backgroundColor=[UIColor colorWithHexString:SeleColor];
    [self addSubview:btLine];
    
    //默认的
    [self createFillterItem:@[
                              @"新 品",
                              @"销 量",
                              @"价 格",
                              @"人 气"]];
}

#pragma mark - 点击筛选按钮
-(void)clickFBtn:(id)sender{
    
    [self createFillterItem:@[
                              @"新 品",
                              @"销 量",
                              @"价 格",
                              @"人 气"]];
    
    [seleFilterBtn setTitleColor:[UIColor colorWithHexString:DefaultColor] forState:UIControlStateNormal];
    seleFilterBtn.enabled=YES;
    
    
    UIButton *btn=(UIButton *)sender;
    ThemticIslandStarFillterModel *model = objc_getAssociatedObject(btn, "ThemticFillterModel");
    selectedModel=model;
    
    //每次点击选取到新品
    dataArr=[NSMutableArray array];
    for (NSDictionary *dic in model.xinpin) {
        HomePageIslandModel *model=[HomePageIslandModel new];
        model.model=dic;
        model.islandName=selectedModel.name;
        [dataArr addObject:model];
    }
    app.md_reflush_reusable=0;
    [self.delegate clickFillterResult:dataArr];
    
    //
    [btn setTitleColor:[UIColor colorWithHexString:SeleColor] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    //设置蓝色横条位置
    CGRect frame=btLine.frame;
    frame.origin.x=btn.origin.x;
    [UIView animateWithDuration:0.20 animations:^{
        btLine.frame=frame;
    } completion:^(BOOL finished) {
        
    }];
    
    //用一个button代替当前选择的
    seleFilterBtn=btn;
}

#pragma mark - 筛选的子类item
-(void)createFillterItem:(NSArray *)array{
    
    for (UIView *subView in self.subviews){
        if (subView.tag>=100) {
            [subView removeFromSuperview];
        }
    }
    
    CGFloat btnWidth=array.count>4?SCREENSIZE.width/4:SCREENSIZE.width/array.count;
    CGFloat btnHeight=44;
    
    for (int i=0; i<array.count; i++) {
        
        //计算X、Y轴
        CGFloat btnX=0.0f;
        CGFloat btnY=0.0f;
        if (i-4>=0) {
            btnX=(i-4)*btnWidth;
            btnY=btnHeight+44;
        }else{
            btnX=i*btnWidth;
            btnY=44;
        }
        
        
        ButtonItem *btn=[ButtonItem buttonWithType:UIButtonTypeCustom];
        btn.tag=100+i;
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
        btn.backgroundColor=[UIColor colorWithHexString:@"#f8f8f8"];
        btn.frame=CGRectMake(btnX, btnY+44, btnWidth, btnHeight);
        btn.titleLabel.font=kFontSize14;
        [btn setTitle:array[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:grateColor] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        btn.titleLabel.layer.cornerRadius=5;
        btn.titleLabel.layer.masksToBounds=YES;
        [btn addTarget:self action:@selector(clickItemBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        if (i==0) {
            seleItemBtn=btn;
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.titleLabel.backgroundColor=[UIColor colorWithHexString:SeleColor];
        }
    }
}

-(void)clickItemBtn:(id)sender{
    
    dataArr=[NSMutableArray array];
    
    [seleItemBtn setTitleColor:[UIColor colorWithHexString:grateColor] forState:UIControlStateNormal];
    seleItemBtn.titleLabel.backgroundColor=[UIColor clearColor];
    seleItemBtn.enabled=YES;
    
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 100:
        {
            for (NSDictionary *dic in selectedModel.xinpin) {
                HomePageIslandModel *model=[HomePageIslandModel new];
                model.model=dic;
                model.islandName=selectedModel.name;
                [dataArr addObject:model];
            }
        }
            break;
        case 101:
        {
            for (NSDictionary *dic in selectedModel.souliang) {
                HomePageIslandModel *model=[HomePageIslandModel new];
                model.model=dic;
                model.islandName=selectedModel.name;
                [dataArr addObject:model];
            }
        }
            break;
        case 102:
        {
            for (NSDictionary *dic in selectedModel.jiage) {
                HomePageIslandModel *model=[HomePageIslandModel new];
                model.model=dic;
                model.islandName=selectedModel.name;
                [dataArr addObject:model];
            }
        }
            break;
        case 103:
        {
            for (NSDictionary *dic in selectedModel.renqi) {
                HomePageIslandModel *model=[HomePageIslandModel new];
                model.model=dic;
                model.islandName=selectedModel.name;
                [dataArr addObject:model];
            }
        }
            break;
            
        default:
            break;
    }
    app.md_reflush_reusable=0;
    [self.delegate clickFillterResult:dataArr];
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.backgroundColor=[UIColor colorWithHexString:SeleColor];
    btn.enabled=NO;
    
    //用一个button代替当前选择的
    seleItemBtn=btn;
}

#pragma mark - Request 请求
-(void)requestButtonData{
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        fillterBtnArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&c=index&a=index_maldives_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            DSLog(@"%@",responseObject);
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //岛屿
                NSArray *dyArr=responseObject[@"retData"][@"list"];
                for (NSDictionary *dic in dyArr) {
                    ThemticIslandStarFillterModel *model=[ThemticIslandStarFillterModel new];
                    model.model=dic;
                    [fillterBtnArr addObject:model];
                }
                
                [self setUpTopBtnForArray:fillterBtnArr];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
            [self requestButtonData];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
