//
//  ThemticIslandStarFillterModel.m
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ThemticIslandStarFillterModel.h"

@implementation ThemticIslandStarFillterModel

-(void)setModel:(NSDictionary *)model{
    self.name=model[@"name"];
    self.xinpin=model[@"id"];
    self.souliang=model[@"xiaoliang"];
    self.jiage=model[@"price"];
    self.renqi=model[@"renqi"];
}

@end
