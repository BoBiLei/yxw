//
//  ThemticIslandStarFillterModel.h
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemticIslandStarFillterModel : NSObject

@property (nonatomic,copy) NSString *name;
@property (nonatomic,strong) NSArray *xinpin;
@property (nonatomic,strong) NSArray *souliang;
@property (nonatomic,strong) NSArray *jiage;
@property (nonatomic,strong) NSArray *renqi;
@property (nonatomic,strong) NSDictionary *model;

@end
