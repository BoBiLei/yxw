//
//  NewMaldiThematiController.m
//  youxia
//
//  Created by mac on 16/4/18.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewMaldiThematiController.h"
#import "NewThemticHeaderCell.h"
#import "HomePageReusableView.h"
#import "SearchViewController.h"
#import "StrategyDetailController.h"
#import "NewIslandCell.h"       //新海岛Cell
#import "NewTSHeaderCell.h"     //新游记攻略顶部
#import "NewTSItem.h"           //新游记攻略item
#import "NewHotSellCell.h"
#import "MDSectionHeadercLayout.h"
#import "IslandFillterReusableView.h"
#import "IslandOnlineController.h"
#import "YouXiaStageController.h"
#import "PalauThemticController.h"
#import "ThemticStragoryController.h"
#import "YouXiaShowController.h"
#import "MaldDistinceListController.h"
#
#define cellWidth (SCREENSIZE.width-32)/3
#define NavigaBar_Height 64
#define IMAGE_OFFSET_SPEED 20
@interface NewMaldiThematiController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HomePageReusableViewDelegate,ThemticBarnerEventDelegate,ThemticRecommendDelegate,ThemticFillterSeleArrayDelegate>

@end

@implementation NewMaldiThematiController{
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    UIImageView *phImgv;
    CGFloat topContentInset;
    
    UIWebView *phoneView;
    NSURLRequest *phoneRequest;
    //
    NSMutableArray *hotSellArr;         //精选热卖
    NSMutableArray *straHeaderArr;      //攻略游记header
    NSMutableArray *straArr;            //攻略游记
    NSMutableArray *fillterBtnArr;      //赛选的arr
    NSMutableArray *fillterDataArr;     //赛选的岛屿
    
    
    UICollectionView *homePageCollection;
    
    HomePageReusableView *hpReusableView;
    IslandFillterReusableView *newReusableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    topContentInset = IMAGEPLAY_HEIHGT;
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initCollectionView];
    
    [self setUpCustomSearBar];
    
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestHttpsForHomePage];
        }
    };
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    CGFloat phoneWidth=44;
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    
    searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-phoneWidth-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"目的地/岛屿/酒店";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    //
    phImgv=[[UIImageView alloc] initWithFrame:CGRectMake(cusBar.width-searchView.height-10, searchView.origin.y, searchView.height, searchView.height)];
    phImgv.userInteractionEnabled=YES;
    phImgv.image=[UIImage imageNamed:@"search_phone"];
    phImgv.userInteractionEnabled=YES;
    [self.view addSubview:phImgv];
    UITapGestureRecognizer *phTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [phImgv addGestureRecognizer:phTap];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

-(void)callPhone{
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - init CollectionView
-(void)initCollectionView{
    MDSectionHeadercLayout *myLayout= [[MDSectionHeadercLayout alloc]init];
    myLayout.floatSection=2;
    homePageCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    homePageCollection.hidden=YES;
    homePageCollection.showsVerticalScrollIndicator=NO;
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewIslandCell" bundle:nil] forCellWithReuseIdentifier:@"NewIslandCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSItem" bundle:nil] forCellWithReuseIdentifier:@"NewTSItem"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NewTSHeaderCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewThemticHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"headercell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewHotSellCell" bundle:nil] forCellWithReuseIdentifier:@"NewHotSellCell"];
    homePageCollection.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    homePageCollection.dataSource=self;
    homePageCollection.delegate=self;
    
    
    [homePageCollection registerNib:[UINib nibWithNibName:@"IslandFillterReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"myReusableView"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"HomePageReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview"];
    
    [self.view addSubview:homePageCollection];
}


#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return hotSellArr.count;
            break;
        case 2:
            return fillterDataArr.count;
            break;
        case 3:
            return straHeaderArr.count;
            break;
        default:
            return straArr.count;
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        NewThemticHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"headercell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewThemticHeaderCell alloc]init];
        }
        cell.barnerClickDelegate=self;
        cell.themticRecommendDelegate=self;
        return cell;
    }else if (indexPath.section==1){
        NewHotSellCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewHotSellCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewHotSellCell alloc]init];
        }
        HomePageIslandModel *model=hotSellArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==2){
        NewIslandCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewIslandCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewIslandCell alloc]init];
        }
        HomePageIslandModel *model=fillterDataArr[indexPath.row];
        [cell reflushMDThemtic:model];
        return cell;
    }else if (indexPath.section==3){
        NewTSHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSHeaderCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSHeaderCell alloc]init];
        }
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        cell.model=model;
        return cell;
    }else{
        NewTSItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSItem" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSItem alloc]init];
        }
        HomepageStrategyModel *model=straArr[indexPath.row];
        cell.model=model;
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, Themtic_IMAGEPLAY_HEIHGT);
    }else if(indexPath.section==1){
        return CGSizeMake((SCREENSIZE.width)/2-14, IS_IPHONE5?130:150);
    }else if(indexPath.section==2){
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/(5/2)+48);
    }else if (indexPath.section==3){
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3+29);
    }else{
        return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width-32)/3-28);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//每个item之间的间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==2||section==4) {
        return 0.0f;
    }else{
        return 9.0f;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    switch (section) {
        case 0:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        case 1:
            return UIEdgeInsetsMake(16, 9, 0, 9);
            break;
        case 2:
            return UIEdgeInsetsMake(16, 0, 16, 0);
            break;
        case 3:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        default:
            return UIEdgeInsetsMake(0, 0, 16, 0);
            break;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DSLog(@"%ld",(long)indexPath.section);
    if (indexPath.section==1) {
        HomePageIslandModel *model=hotSellArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=MaldivesIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==2){
        HomePageIslandModel *model=fillterDataArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=MaldivesIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==3){
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if (indexPath.section==4){
        HomepageStrategyModel *model=straArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    if(indexPath.section==2){
        newReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"myReusableView" forIndexPath:indexPath];
        newReusableView.delegate=self;
        reusableview = newReusableView;
    }else{
        hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview" forIndexPath:indexPath];
        [hpReusableView reflushMDThemticDataForIndexPath:indexPath];
        hpReusableView.delegate=self;
        if (indexPath.section==1) {
            hpReusableView.reusableId=@"11";
        }else if (indexPath.section==3) {
            hpReusableView.reusableId=@"33";
        }
        reusableview = hpReusableView;
    }
    return reusableview;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (section==0||section==4) {
        return CGSizeMake(0, 0);
    }else if(section==1){
        return CGSizeMake(SCREENSIZE.width, 46);
    }else if(section==2){
        return CGSizeMake(SCREENSIZE.width, 44*3);
    }else{
        return CGSizeMake(SCREENSIZE.width, 56);
    }
}

#pragma mark - HeaderviewCategory delegate
-(void)clickCollectionItem:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击Barner广告 Delegate
//广告
-(void)clickThemticBarner:(ImageModel *)model{
    if ([model.advType isEqualToString:@"maldives"]) {
        NewMaldiThematiController *malCtr=[NewMaldiThematiController new];
        [self.navigationController pushViewController:malCtr animated:YES];
    }else if ([model.advType isEqualToString:@"palau"]) {
        PalauThemticController *plCtr=[PalauThemticController new];
        [self.navigationController pushViewController:plCtr animated:YES];
    }else if ([model.advType isEqualToString:@"show"]) {
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"zxxd"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"links"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",model.imgId]]];
    }else if ([model.advType isEqualToString:@"strategy_maldives"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_palau"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi"]) {
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_show"]) {
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.imgId;
        strCtr.cId=@"234";
        strCtr.strategyTitle=@"title";
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi_list"]) {
        
    }
}

#pragma mark - 点击星级岛屿更多
-(void)clickLeftBtn{
    IslandOnlineController *ctr=[IslandOnlineController new];
    [self.navigationController pushViewController:ctr animated:YES];
}

//推荐
-(void)clickThemticRecommend:(ThemticRecommendModel *)model{
    DSLog(@"%@",model.name);
    if ([model.name isEqualToString:@"在线选岛"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"分期推荐"]){
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"马代攻略"]){
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=model.url;
        
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - 点击赛选按钮 Delegate
-(void)clickFillterResult:(NSArray *)array{
    fillterDataArr=[NSMutableArray array];
    for (HomePageIslandModel *model in array) {
        [fillterDataArr addObject:model];
    }
    [homePageCollection reloadData];
    
    //滑动到headerview位置
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (app.md_reflush_reusable==0) {
        NSString *flTag=[AppUtils getValueWithKey:@"MDFloatTag"];
        [homePageCollection setContentOffset:CGPointMake(0,flTag.floatValue-44) animated:YES];
    }
}

#pragma mark - Reusableview delegate
-(void)clickMore:(NSString *)strId{
    if ([strId isEqualToString:@"11"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"33"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&a=list_strategy_v6_ios&mudi=medf&is_iso=1";
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offsetY = scrollView.contentOffset.y+homePageCollection.contentInset.top;
    
    if (offsetY<0) {
        searchView.hidden=YES;
        phImgv.hidden=YES;
    }else{
        searchView.hidden=NO;
        phImgv.hidden=NO;
    }
    
    if (offsetY<-44) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((Themtic_IMAGEPLAY_HEIHGT-NavigaBar_Height - offsetY) / topContentInset));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
    
    //岛屿显示效果
    for(NewIslandCell *view in homePageCollection.visibleCells) {
        if ([view isKindOfClass:NewIslandCell.class]) {
            CGFloat yOffset = ((homePageCollection.contentOffset.y - view.frame.origin.y) / (SCREENSIZE.width/(5/2)+30)) * IMAGE_OFFSET_SPEED;
            view.imageOffset = CGPointMake(0.0f, yOffset);
        }
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForHomePage{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        hotSellArr=[NSMutableArray array];
        straHeaderArr=[NSMutableArray array];
        straArr=[NSMutableArray array];
        fillterBtnArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&c=index&a=index_maldives_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
//            DSLog(@"%@",responseObject[@"retData"][@"list"]);
            
            [AppUtils dismissHUDInView:self.view];
            
            homePageCollection.hidden=NO;
            
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //banner
                NSArray *imgArr=responseObject[@"retData"][@"banner"];
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitPageImageNoti object:imgArr];
                
                //tuijianwei
                NSArray *tjwArr=responseObject[@"retData"][@"tuijianwei"];
                NSMutableArray *headTjwArr=[NSMutableArray array];
                for (NSDictionary *dic in tjwArr) {
                    ThemticRecommendModel *model=[ThemticRecommendModel new];
                    model.model=dic;
                    [headTjwArr addObject:model];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitTJWImageNoti object:headTjwArr];
                
                //精选热卖
                NSArray *hsArr=responseObject[@"retData"][@"remai"];
                for (NSDictionary *dic in hsArr) {
                    HomePageIslandModel *model=[HomePageIslandModel new];
                    model.model=dic;
                    [hotSellArr addObject:model];
                }
                
                //游记攻略
                NSArray *glArr=responseObject[@"retData"][@"strategy_data"];
                for (int i=0; i<glArr.count; i++) {
                    NSDictionary *dic=glArr[i];
                    if (i==0) {
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straHeaderArr addObject:model];
                    }else{
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straArr addObject:model];
                    }
                }
                
            }
            [homePageCollection reloadData];
            [homePageCollection.mj_header endRefreshing];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
            [self requestHttpsForHomePage];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
