//
//  HotelDetailPriceModel.h
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelDetailPriceModel : NSObject

@property(nonatomic,retain)NSArray *CancelPolicy;
@property(nonatomic,copy)NSString *Currency;
@property(nonatomic,copy)NSString *InventoryCount;      //库存
@property(nonatomic,copy)NSString *MaxOccupancy;        //最大入住人数
@property(nonatomic,copy)NSString *MaxChild;            //最大入住儿童数
@property(nonatomic,retain)NSArray *PriceList;
@property(nonatomic,copy)NSString *RatePlanID;
@property(nonatomic,copy)NSString  *RatePlanName;
@property(nonatomic,retain)NSArray *RoomImg;
@property(nonatomic,copy)NSString *RoomStatus;
@property(nonatomic,copy)NSString *TotalPrice;

-(void)jsonToModel:(NSDictionary *)dic;

@end
