//
//  RoomSumHeadCell.m
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RoomSumHeadCell.h"

@implementation RoomSumHeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickCleanBtn:(id)sender {
    UITableView *tableView = (UITableView *)self.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    [self.delegate clickCleanBtn:indexPath];
}

@end
