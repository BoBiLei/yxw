//
//  HotelListController.m
//  youxia
//
//  Created by mac on 16/4/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "HotelListController.h"
#import "SearchViewController.h"
#import "MdIslandOnlineCell.h"
#import "DistingDropDownMenu.h"
#import "IslandDetailController.h"
#import "DistingcOrtherListController.h"
#import "NewMaldiThematiController.h"
#import "JDDistinOneCell.h"
#import "HoteldetailsController.h"
#import "SelectCheckDateViewController.h"
#import "HotelDetailsController.h"
#import "HotelHomeModel.h"
#import "CtripChinaCityModel.h"
#import "CtripOverseaCityModel.h"
#import "HotelHomeController.h"
#import "WLRangeSlider.h"
#import "HoteldetailsModel.h"
#import "MdModel.h"

#define  NiocellHeight SCREENSIZE.width/3-12
#define kAppViewH 80 //每个小视图高80
#define kAppViewW 80 //每个小视图宽80
#define kColCount 4 //每行视图数量一定，都是三个
#define kStart 30   //适配屏幕，起点30
#define kTableBorderWidth 10
@interface HotelListController ()<DistingDropDownMenuDataSource,DistingDropDownMenuDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong) NSArray *moreArr;
@property (nonatomic ,strong) UIView *deverView; //底部View
@property (nonatomic ,strong) UIView *bgView; //遮罩
@property (nonatomic,strong)WLRangeSlider *rangeSlider;
@end

@implementation HotelListController{
    
    //在线岛屿筛选id
    UITextField *pal;
    NSString *xuanzhe;
    NSMutableArray *leftArr;
    NSMutableArray *rightArr;
//    UIButton *button;
    NSArray *dzarr;
    NSString *firstId;
    NSString *secondId;
    NSString *threeId;
    NSString *froundId;
    NSArray *menuArr01;
    NSArray *menuArr02;
    NSArray *menuArr03;
    NSArray *menuArr05;
    NSArray *menuArrname;
    NSArray *menuArrname2;
    NSArray *menuArr04;
    NSString *successMes;
    DistingDropDownMenu *menu;
    NSString *dec;
    NSInteger hotelStar;
    UIView *view2;
    NSInteger lex;
    HotelHomeController *home;
    NSString *Tprice; //价格;
    NSMutableArray *dataArr;
    UITableView *myTable;
    NSString *s;
    int as;
    UIButton *titlebtn;
    UIButton *titlebtn2;
    NSString *kong;
    UIButton *seleBtn;
    NSInteger filterTag;
    NSInteger pageInt;
    NSString *requestType;
    MJRefreshNormalHeader *refleshHeader;
    MJRefreshAutoNormalFooter *refleshFooter;
    NSMutableArray * choiceStarArray;
}

@synthesize dzarrs;
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    pageInt = 1;
    [TalkingData trackPageBegin:@"酒店列表页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    choiceStarArray = [[NSMutableArray alloc]init];
//    _pricers = @"2000";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"酒店地列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dec=@"default";
    kong=@"";
    
    if ([_hotetype isEqualToString:@"CN"]) {
        lex=0;
    }else{
        lex=1;
    }
    [self setUpCustomSearBar];
    
    [self hoteRequestStarId:firstId gplaceId:secondId gIslandId:threeId frouid:froundId];
    
    [self setUpDopMenu];
    
    [self setUpTableView];
    
    [self requestDistingceData];
}


#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    self.navReturnBtn=back;
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
  //  UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
   // [searchView addGestureRecognizer:searchTap];
    
    titlebtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 1, 88, 15.5)];
    NSString *string2 = [_chein substringFromIndex:5];
    [titlebtn setTitle:[NSString stringWithFormat:@"住 %@",string2] forState:UIControlStateNormal];
    
    titlebtn.titleLabel.font=[UIFont systemFontOfSize:11];
    [titlebtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [titlebtn addTarget:self action:@selector(clickDate:) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:titlebtn];
    titlebtn2=[[UIButton alloc]initWithFrame:CGRectMake(0, 15.5, 88, 15.5)];
    [titlebtn2 addTarget:self action:@selector(clickDate:) forControlEvents:UIControlEventTouchUpInside];
    [titlebtn2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    NSString *string3 = [_cheout substringFromIndex:5];
    
    [titlebtn2 setTitle:[NSString stringWithFormat:@"离 %@",string3] forState:UIControlStateNormal];
    titlebtn2.titleLabel.font=[UIFont systemFontOfSize:11];
    
    [searchView addSubview:titlebtn2];
    
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(86, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    pal=[[UITextField alloc] initWithFrame:CGRectMake(112, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.delegate=self;
    pal.placeholder=@"关键字/酒店名";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)clickDate:(NSString *)date{
    SelectCheckDateViewController *vc=[[SelectCheckDateViewController alloc]init];
    
    [vc setSelectCheckDateBlock:^(NSString *startDateStr, NSString *endDateStr, NSString *daysStr) {
        _chein=startDateStr;
        _cheout=endDateStr;
        
        [titlebtn setTitle:[NSString stringWithFormat:@"住 %@",_chein] forState:UIControlStateNormal];
        
        [titlebtn2 setTitle:[NSString stringWithFormat:@"离 %@",_cheout] forState:UIControlStateNormal];
    }];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - 搜索栏点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}
*/

-(void)setUpTableView{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //下拉刷新
    refleshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 延迟加载
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            pageInt=1;
            dataArr=[NSMutableArray array];
            [self requestDistingceData];
        });
    }];
    
    [refleshHeader setMj_h:64];//设置高度
    [refleshHeader setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
    
    // 设置字体
    [refleshHeader.stateLabel setMj_y:13];
    refleshHeader.stateLabel.font = kFontSize13;
    refleshHeader.lastUpdatedTimeLabel.font = kFontSize13;
    myTable.mj_header = refleshHeader;
    
    ////////////////////
    //加载更多
    refleshFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        pageInt++;
        [self requestDistingceData];
    }];
    refleshFooter.stateLabel.font = [UIFont systemFontOfSize:14];
    refleshFooter.stateLabel.textColor = [UIColor lightGrayColor];
    [refleshFooter setTitle:@"" forState:MJRefreshStateIdle];
    [refleshFooter setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [refleshFooter setTitle:@"没有更多" forState:MJRefreshStateNoMoreData];
    refleshFooter.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = refleshFooter;
}

-(void)setUpDopMenu{
    
    firstId=@"0";
    secondId=@"0";
    threeId=@"0";
    froundId=@"0";
    
    
    // 数据
    NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"Sorting" ofType:@"plist"];
    menuArr03 = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
    NSLog(@"/*/*/*/%@",menuArr03);
    //
    NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"Starprice" ofType:@"plist"];
    menuArr02 = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
    
    // 添加下拉菜单
    menu = [[DistingDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    menu.isHotelMenu=YES;
    menu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    menu.textColor=[UIColor colorWithHexString:@"#686868"];
    menu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    menu.delegate =self;
    menu.dataSource =self;
    [menu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:menu];
    
    [menu selectDefalutIndexPath];
}

#pragma mark -DOPMenu Delegate


- (NSInteger)numberOfColumnsInMenu:(DistingDropDownMenu *)menu{
    return 2;
}

- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column{
    if (column == 0) {
        return menuArr01.count;
    }else{
        return 1;
    }
}

- (NSString *)menu:(DistingDropDownMenu *)menu titleForRowAtIndexPath:(DistingIndexPath *)indexPath{
    
    if (indexPath.column == 0) {
        if (menuArr01.count==0) {
            return menuArr03[indexPath.row][@"name"];
        }else
        {
            return menuArr01[indexPath.row];
        }
    } else{
        return menuArr02[indexPath.row][@"name"];
    }
}

//-添加的自定义view
-(UIView *)menu:(DistingDropDownMenu *)menu addHotelAtIndexPath:(DistingIndexPath *)indexPath{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UIView class]]) {
            [view2 removeFromSuperview];
        }
    }
    view2=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 0)];
    view2.userInteractionEnabled=YES;
    view2.backgroundColor = [UIColor whiteColor];
    
    UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 50)];
    lab.text=@"价格筛选";
    lab.textColor=[UIColor colorWithHexString:@"707070"];
    lab.font = [UIFont systemFontOfSize:16];
    [view2 addSubview:lab];
    
    _rangeSlider=[[WLRangeSlider alloc]initWithFrame:CGRectMake(lab.origin.x+4, lab.size.height+lab.origin.y+20, SCREENSIZE.width-(lab.origin.x+4)*2, 24)];
    _rangeSlider.leftValue = _minPricers.floatValue;
    _rangeSlider.rightValue = _pricers.floatValue;
    [_rangeSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    _rangeSlider.layer.borderColor=[[UIColor clearColor]CGColor];
    [view2 addSubview:_rangeSlider];
    [self valueChanged:_rangeSlider];
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab.origin.x,lab.origin.y+lab.height+60, lab.width, lab.height)];
    lab2.text=@"星级筛选";
    lab2.textColor=[UIColor colorWithHexString:@"707070"];
    lab2.font = [UIFont systemFontOfSize:16];
    [view2 addSubview:lab2];
   
    CGFloat lastYH=0;
    CGFloat btnWidth=(SCREENSIZE.width-lab.origin.x*2-24)/4;
    CGFloat btnHeight=38;
    CGFloat btnY=lab2.origin.y+lab2.height+46;
    CGFloat btnX=lab.origin.x;
    
    for (int i=0; i<dzarrs.count; i++) {
        if(i<4){
            btnX=lab.origin.x+i*btnWidth+8*i;
            btnY=lab2.origin.y+lab2.height+4;
        }else{
            btnX=lab.origin.x+(i%4)*btnWidth+8*(i%4);
            btnY=btnY+btnHeight+8;
        }
        
       UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(btnX,btnY,btnWidth,btnHeight)];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
        [button.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
        [button.layer setBorderWidth:1.2f];
        [button.layer setMasksToBounds:YES];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:dzarrs[i] forState:UIControlStateNormal];
        button.tag =i;
        [button.layer setMasksToBounds:YES];
        [choiceStarArray addObject:button];
        [button addTarget:self action:@selector(tagButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [view2 addSubview:button];
            if(i==0 && _sta.intValue==0){
                seleBtn=button;
                [button setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
                [button.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
            }

            if(i==(_sta.intValue-1)){
                seleBtn=button;
                [button setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
                [button.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
            }
        
        if (i==dzarrs.count-1) {
            lastYH=button.origin.y+button.height+16;
        }
    }
    UIButton *cancelBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, lastYH, view2.width/2, 48)];
    cancelBtn.backgroundColor=[UIColor whiteColor];
    [cancelBtn setTitle:@"清空选择" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font=kFontSize16;
    cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancelBtn.layer.borderWidth=0.8;
    cancelBtn.layer.borderColor=[[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]CGColor];
    [cancelBtn setTitleColor:[UIColor colorWithRed:155/255.0 green:155/255.0 blue:155/255.0 alpha:1] forState:UIControlStateNormal];
    [view2 addSubview: cancelBtn];
    
    UIButton *confimBtn=[[UIButton alloc]initWithFrame:CGRectMake(view2.width/2, cancelBtn.origin.y, cancelBtn.width, cancelBtn.height)];
    [confimBtn setTitle:@"确 定" forState:UIControlStateNormal];
    confimBtn.titleLabel.font=kFontSize16;
    [confimBtn setBackgroundColor:[UIColor colorWithHexString:@"55b2f0"]];
    [confimBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confimBtn addTarget:self action:@selector(clickConfimBtn) forControlEvents:UIControlEventTouchUpInside];
    [view2 addSubview: confimBtn];
    
    CGRect frame=view2.frame;
    frame.size.height=confimBtn.origin.y+confimBtn.height+120;
    view2.frame=frame;
    
    return view2;
}


- (void)menu:(DistingDropDownMenu *)menu didSelectRowAtIndexPath:(DistingIndexPath *)indexPath{
    if (indexPath.column == 0) {
      
//        NSLog(@"点击的是%@", menuArr01[indexPath.row]);
        if ([menuArr01[indexPath.row] isEqualToString:@"低价优先"]) {
            dec=@"ASC";
        }else if([menuArr01[indexPath.row] isEqualToString:@"高价优先"])
        {
            dec=@"DESC";
        }else
        {
            dec=@"default";
        }
        
        [self requestDistingceData];
    }
}

-(void)clickConfimBtn{
    _pri=Tprice==nil||Tprice==NULL||[Tprice isEqualToString:@""]?@"0:2000":Tprice;
    _sta = [NSString stringWithFormat:@"%ld",hotelStar];
    [self requestDistingceData];
    [self exitClick];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return NiocellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JDDistinOneCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[JDDistinOneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (indexPath.row<dataArr.count) {
        HotelListModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (dataArr.count>0) {
        HotelListModel *model=dataArr[indexPath.row];
        HotelDetailsController *islDetail=[[HotelDetailsController alloc]init];
        islDetail.hoteID=model.hoteid;
        islDetail.hotename=model.hotename;
        islDetail.hoteaddress=model.address;
        islDetail.ruzhuDate=_chein;
        islDetail.endDate=_cheout;
        islDetail.crnum=_adult;
        islDetail.etnum=_child;
        islDetail.etage=_childage;
        islDetail.numday=_numdays;
        [self.navigationController pushViewController:islDetail animated:YES];
    }
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}
#pragma mark - Request 请求
-(void)requestDistingceData{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"siftingHotel" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    //    [dict setObject:@"1" forKey:@"is_get"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
//    if (lex==0) {
//        if ([_pos isEqualToString:@""] || _pos==NULL || _pos==nil) {
//            _pos=@"CN:3354";
//            _pods2 = @"";
//        }
//    }else
//    {
//        if ([_pods2 isEqualToString:@""] || _pods2==NULL || _pods2==nil) {
//            _pods2=@"CN:3354";
//        }else{
//            _pods2 = @"";
//        }
//    }
    
    NSDictionary *dic=@{@"type":_hotetype,
                        @"position":_pos,
                        @"order":dec,
                        @"star":_sta,
                        @"price":_pri,
                        @"checkin":_chein,
                        @"checkout":_cheout,
                        @"searchKey":_sea,
                        @"page":[NSString stringWithFormat:@"%ld",pageInt],
                        @"adult":@(lex)==0?@"1":_adult,
                        @"child":@(lex)==0?@"0":_child,
                        @"childAge":@(lex)==0?kong:_childage
                        };
    
    NSData *data=[AppUtils toJSONData:dic];
    NSString *jsonString = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    
    [dict setObject:jsonString forKey:@"condition"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"=================%@",responseObject);

       
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
//             successMes =responseObject[@"retData"][@"msg"];
//            if ([responseObject[@"retData"][@"msg"] isEqualToString:@"没有请求的数据!"]) {
//                [AppUtils showSuccessMessage:@"没有请求的数据!" inView:self.view];
//            }else{
            id resObj=responseObject[@"retData"];
            if ([resObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in resObj) {
                    HotelListModel *model=[HotelListModel new];
                    model.model=dic;
                    [dataArr addObject:model];
                }
                [refleshHeader endRefreshing];
                [refleshFooter endRefreshingWithNoMoreData];

                [myTable.mj_header endRefreshing];
                [myTable.mj_footer endRefreshing];
                [myTable reloadData];
            }else{
                [myTable.mj_footer endRefreshing];
                [refleshFooter endRefreshingWithNoMoreData];
                [myTable reloadData];
                [AppUtils showSuccessMessage:@"没有请求的数据" inView:self.view];
                DSLog(@"%@",responseObject[@"retData"][@"msg"]);
            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [myTable.mj_header endRefreshing];
        [refleshFooter endRefreshing];
        [myTable reloadData];

        DSLog(@"%@",error);
    }];
}



#pragma mark - 酒店列表搜索Request 请求
-(void)requestsearchData{
    
    [AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"siftingHotel" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    //    [dict setObject:@"1" forKey:@"is_get"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
//    if (lex==0) {
//        if ([_pos isEqualToString:@""] || _pos==NULL || _pos==nil) {
//            _pos=@"CN:3354";
//        }
//    }else
//    {
//        if ([_pods2 isEqualToString:@""] || _pods2==NULL || _pods2==nil) {
//            //_pods2=@"CN:3354";
//        }
//    }
    
    
    NSDictionary *dic=@{@"type":_hotetype,
                        @"position":_pos,
                        @"order":dec,
                        @"star":_sta,
                        @"price":_pri,
                        @"checkin":_chein,
                        @"checkout":_cheout,
                        @"searchKey":pal.text,
                        @"page":@"1",
                        @"adult":lex==0?@"1":_adult,
                        @"child":lex==0?@"0":_child,
                        @"childAge":lex==0?kong:_childage
                        };
    
    
    NSData *data=[AppUtils toJSONData:dic];
    NSString *jsonString = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    
    [dict setObject:jsonString forKey:@"condition"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"=================%@",responseObject);
        
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            //             successMes =responseObject[@"retData"][@"msg"];
            //            if ([responseObject[@"retData"][@"msg"] isEqualToString:@"没有请求的数据!"]) {
            //                [AppUtils showSuccessMessage:@"没有请求的数据!" inView:self.view];
            //            }else{
            id resObj=responseObject[@"retData"];
            if ([resObj isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dic in resObj) {
                    HotelListModel *model=[HotelListModel new];
                    model.model=dic;
                    [dataArr addObject:model];
                }
                [refleshFooter endRefreshing];
                [refleshFooter endRefreshingWithNoMoreData];
                
                [myTable.mj_header endRefreshing];
                [myTable reloadData];
            }else{
                [myTable.mj_header endRefreshing];
                [refleshFooter endRefreshingWithNoMoreData];
                [myTable reloadData];
                [AppUtils showSuccessMessage:@"没有请求的数据" inView:self.view];
                DSLog(@"%@",responseObject[@"retData"][@"msg"]);
                
                
            }
            
            
            
        }else{
            
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        [myTable.mj_header endRefreshing];
        [refleshFooter endRefreshing];
        [myTable reloadData];
        
        DSLog(@"%@",error);
    }];}


#pragma mark 酒店列表页筛选条件接口
-(void)hoteRequestStarId:(NSString *)sid gplaceId:(NSString *)gplId gIslandId:(NSString *)gIsId frouid:(NSString *)frdid{
    dataArr =[NSMutableArray array];
    menuArr01=[NSArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"siftingOption" forKey:@"code"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            NSDictionary *dic01=responseObject[@"retData"][@"orderOption"]; //排序条件
            
            menuArr01 = @[dic01[@"default"],dic01[@"ASC"],dic01[@"DESC"]];
            
//            NSDictionary *dic04=responseObject[@"retData"][@"multiOptions"]; //综合筛选
            //左边
//            for (NSDictionary  *dic in dic04) {
//                [leftArr addObject:dic];
//            }
//            NSArray *rDicArr=leftArr[0][@"name"];
//            for (NSDictionary  *dic in rDicArr) {
//                MdModel *model=[MdModel new];
//                [model jsonDataForDictionary:dic];
//                [rightArr addObject:model];
//            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}
//筛选请求
-(void)fillterIslandRequestStarId:(NSString *)sId gplaceId:(NSString *)gplId gIslandId:(NSString *)gIsId{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    dataArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"typeid"];
    [dict setObject:sId forKey:@"dy"];
    [dict setObject:gplId forKey:@"jg"];
    [dict setObject:gIsId forKey:@"jt"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[@"http://m.youxia.com/" stringByAppendingString:@"maldives/search.html?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"sealist"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


- (void)valueChanged:(WLRangeSlider *)slider{
    int b;int c;
    b=ceilf(_rangeSlider.leftValue*1.0);
    c=ceilf(_rangeSlider.rightValue*1.0);
    Tprice=[NSString stringWithFormat:@"%d:%d",b,c];
    _pricers = [NSString stringWithFormat:@"%d",c];
    _minPricers = [NSString stringWithFormat:@"%d",b];
    [self changeStr:[NSString stringWithFormat:@"%f",_rangeSlider.leftValue]];
}

-(NSString *)notRounding:(float)price afterPoint:(int)position{
    
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *ouncesDecimal;
    
    NSDecimalNumber *roundedOunces;
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    return [NSString stringWithFormat:@"%@",roundedOunces];
    
}

-(NSString*)changeStr:(NSString*)myStr{
    NSString*str=nil;
    NSString*fenStr=[NSString stringWithFormat:@"0.%@",[myStr componentsSeparatedByString:@"."][1]];
    NSString*fen=[self notRounding:[fenStr floatValue]*60  afterPoint:0];
    
    str=[NSString stringWithFormat:@"%@:%@",[NSString stringWithFormat:@"%@",[myStr componentsSeparatedByString:@"."][0]],fen];
    NSLog(@"str--%@",str);
    return str;
}

#pragma mark - 点击价格赛选确定
-(void)deterBrtn:(NSString *)send{
    [self exitClick];
}

-(void)cancelView{
    s=@"0";
    _minPricers = @"0";
    _pricers   = @"2000";
    _sta = @"0";
    hotelStar = 0;
    [self exitClick];
}

- (void)exitClick {
    [menu backgroundTappedClear];
//    [UIView animateWithDuration:0.3 animations:^{
//        self.deverView.transform = CGAffineTransformMakeTranslation(0.01, SCREEN_HEIGHT);
//        self.deverView.alpha = 0.2;
//        self.bgView.alpha = 0;
//        
//    } completion:^(BOOL finished) {
//        
//        [self.bgView removeFromSuperview];
//        [self.deverView removeFromSuperview];
//    }];
}

- (void)tagButtonClick:(UIButton *)sender{
//    [sender setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
//    [sender.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
////    sender.enabled=NO;
//    
//    [seleBtn setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
//    [seleBtn.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
////    seleBtn.enabled=YES;
//    
//    seleBtn= sender;
    s = [NSString stringWithFormat:@"%ld",sender.tag];
    xuanzhe=sender.titleLabel.text;
    for (UIButton * selectButton in choiceStarArray) {
        if (selectButton.tag == sender.tag) {
            [sender setTitleColor:[UIColor colorWithHexString:@"55b2f0"] forState:UIControlStateNormal];
            [sender.layer setBorderColor:[UIColor colorWithHexString:@"55b2f0"].CGColor];
        }else{
            [selectButton setTitleColor:[UIColor colorWithHexString:@"ababab"] forState:UIControlStateNormal];
            [selectButton.layer setBorderColor:[UIColor colorWithHexString:@"f2f2f2"].CGColor];
        }
    }
    if (sender.tag==0) {
        hotelStar = 0;
    }else{
        hotelStar = sender.tag+1;
    }
    
}
/*
-(void)textFieldDidBeginEditing:(UITextField*)textField
{
    
    [textField resignFirstResponder];
   
}*/

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self exitClick];
    [self requestsearchData];
    //[self requestDistingceData];
}
@end
