//
//  HotelListController.h
//  youxia
//
//  Created by mac on 16/4/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BaseController.h"
#import "HotelListModel.h"

@interface HotelListController : BaseController


@property (nonatomic,copy) NSString *urlStr;


@property (nonatomic,copy) NSString *cityName;
@property (nonatomic,copy) NSString *hotetype;
@property (nonatomic,copy) NSString *pos;
@property (nonatomic,copy) NSString *pods2;
@property (nonatomic,copy) NSString *ord;
@property (nonatomic,copy) NSString *sta;
@property (nonatomic,copy) NSString *pri;
@property (nonatomic,copy) NSString *chein;
@property (nonatomic,copy) NSString *cheout;
@property (nonatomic,copy) NSString *numdays; //总天数
@property (nonatomic,copy) NSString *sea;
@property (nonatomic,copy) NSString *pag;
@property (nonatomic,copy) NSString *adult; //成人
@property (nonatomic,copy) NSString *child; //儿童
@property (nonatomic,copy) NSString *childage; //儿童年龄
@property (nonatomic,retain)NSArray *dzarrs;
@property (nonatomic,copy) NSString *Hprice;
@property(nonatomic,copy)NSString *pricers;
@property(nonatomic,copy)NSString *minPricers;
@property(nonatomic,copy)NSString *maxPricers;
@end
