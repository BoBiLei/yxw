//
//  MdModel.h
//  youxia
//
//  Created by mac on 2017/4/6.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MdModel : NSObject
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *type2;
@property(nonatomic,copy)NSString *dafault;
@property(nonatomic,copy)NSString *tid;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *name_cn;
-(void)jsonDataForDictionary:(NSDictionary *)dic;
@end
