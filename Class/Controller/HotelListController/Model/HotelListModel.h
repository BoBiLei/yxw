//
//  HotelListModel.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelListModel : NSObject


//@property (nonatomic,copy) NSString *islandId;
//@property (nonatomic,copy) NSString *pId;
//@property (nonatomic,copy) NSString *mdId;
//@property (nonatomic,copy) NSString *islandImg;
//@property (nonatomic,copy) NSString *islandType;
//@property (nonatomic,copy) NSString *islandYwName;
//@property (nonatomic,copy) NSString *islandTitle;
//@property (nonatomic,copy) NSString *islandPrice;
//@property (nonatomic,copy) NSString *stagePrice;
//@property (nonatomic,copy) NSString *starLevel;
//@property (nonatomic,copy) NSString *jiaotong;




@property(nonatomic,copy) NSString *address;
@property(nonatomic,copy)NSString *hoteid;
@property(nonatomic,copy)NSString *hotename;
@property(nonatomic,copy)NSString *lowestPrice;
@property(nonatomic,copy)NSString *HotelType;
@property(nonatomic,copy)NSString *hotelogo;
@property (nonatomic, retain) NSDictionary *model;


-(void)jsonDataForDictionary:(NSDictionary *)dic;
@end
