//
//  JDDistinOneCell.m
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "JDDistinOneCell.h"
#import <TTTAttributedLabel.h>
#define  NiocellHeight SCREENSIZE.width/3-12

@implementation JDDistinOneCell
{
    UIImageView *imgv;
    UILabel *tagLabel;
    UIImageView *typeImgv;
    UILabel *typeNameLabel;
    UILabel *titleLabel;
    UIButton *btn;
    UILabel *lab;

    TTTAttributedLabel *priceLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Hotelname:) name:@"Hotel_name" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Hotelhotelowest:) name:@"Hotel_hotelowest" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Hotelhoteladdress:) name:@"Hotel_hoteladdress" object:nil];

}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(8, 12, SCREENSIZE.width/3-8, SCREENSIZE.width/3-36)];
        imgv.layer.cornerRadius=4;
        imgv.layer.masksToBounds=YES;
        [self addSubview:imgv];
        CGRect frame=self.frame;
        frame.size.height=NiocellHeight;
        self.frame=frame;
        
        //
        //
        typeNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+6, imgv.origin.y, SCREENSIZE.width-(imgv.origin.x+imgv.width+16), IS_IPHONE5?38:40)];
        typeNameLabel.textColor=[UIColor blackColor];
        typeNameLabel.font=kFontSize15;
        typeNameLabel.numberOfLines=0;
        [self addSubview:typeNameLabel];
        
        
        UIButton *dwImgv=[UIButton buttonWithType:UIButtonTypeCustom];
        dwImgv.frame=CGRectMake(typeNameLabel.origin.x-2, typeNameLabel.origin.y+typeNameLabel.height+(IS_IPHONE5?1:2), 18, 18);
        [dwImgv setImage:[UIImage imageNamed:@"hotellist_dw"] forState:UIControlStateNormal];
        [self addSubview:dwImgv];
        
        //
        titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(dwImgv.origin.x+dwImgv.width, typeNameLabel.origin.y+typeNameLabel.height+(IS_IPHONE5?0:1), SCREENSIZE.width-(typeNameLabel.origin.x+8+18), IS_IPHONE5?17:19)];
        titleLabel.textColor=[UIColor colorWithRed:194.0/255 green:194.0/255 blue:194.0/255 alpha:1];
        titleLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
        [self addSubview:titleLabel];
        
        lab=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+6, titleLabel.size.height+titleLabel.origin.y+6, SCREENSIZE.width-(imgv.origin.x+imgv.width+16), IS_IPHONE5?17:18)];
        lab.textColor=[UIColor colorWithRed:171.0/255 green:171.0/255 blue:171.0/255 alpha:1];
        lab.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
        [self addSubview:lab];
        
     
        
        //根据星级显示图片
//        UIImageView *imgview=[[UIImageView alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+6, titleLabel.origin.y-10, 20, 20)];
        
        
        priceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(SCREENSIZE.width-136, lab.origin.y, 120, lab.height)];
        priceLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?15:16];
        priceLabel.textAlignment=NSTextAlignmentRight;
        priceLabel.textColor=[UIColor colorWithHexString:@"55b2f0"];
        [self addSubview:priceLabel];
    }
    return self;
}

-(void)reflushDataForModel:(HotelListModel *)model{
    
    self.model=model;
    typeNameLabel.text=model.hotename;
    priceLabel.text=[NSString stringWithFormat:@"￥%@元起",model.lowestPrice];
    
    [imgv sd_setImageWithURL:[NSURL URLWithString:model.hotelogo] placeholderImage:[UIImage imageNamed:Image_Default]];
//    [btn setTitle:model.HotelType forState:UIControlStateNormal];
    lab.text=model.HotelType;
    titleLabel.text=model.address;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
