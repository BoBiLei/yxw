//
//  ContactInformationCell.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactInformationDelegate <NSObject>

-(void)textChange:(NSString *)text;

@end

//联系人信息
@interface ContactInformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ContactInfotitle;
@property (weak, nonatomic) IBOutlet UITextField *Contacttext;
@property (weak, nonatomic) IBOutlet UIButton *Contactbtn;

@property (nonatomic, copy) void (^ContactInformation)(NSString *text);

@property (weak, nonatomic) id <ContactInformationDelegate> delegate;

@end
