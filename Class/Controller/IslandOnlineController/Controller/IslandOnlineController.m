//
//  IslandOnlineController.m
//  youxia
//
//  Created by mac on 16/4/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "IslandOnlineController.h"
#import "SearchViewController.h"
#import "MdIslandOnlineCell.h"
#import "DistingDropDownMenu.h"
#import "IslandDetailController.h"
#import "DistingcOrtherListController.h"
#import "NewMaldiThematiController.h"

#define  NiocellHeight SCREENSIZE.width/3-12
@interface IslandOnlineController ()<DistingDropDownMenuDataSource,DistingDropDownMenuDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSArray *moreArr;

@end

@implementation IslandOnlineController{
    
    //在线岛屿筛选id
    NSString *firstId;
    NSString *secondId;
    NSString *threeId;
    
    NSArray *menuArr01;
    NSArray *menuArr02;
    NSArray *menuArr03;
    DistingDropDownMenu *menu;
    
    //
    NSMutableArray *dataArr;
    UITableView *myTable;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpCustomSearBar];
    
    [self setUpDopMenu];
    
    [self setUpTableView];
    
    [self requestDistingceData];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    UIImageView *searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"请输入目的地";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

-(void)setUpTableView{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-157) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}

-(void)setUpDopMenu{
    
    firstId=@"0";
    secondId=@"0";
    threeId=@"0";
    
    
    
    // 数据
    NSString *starLevelPath = [[NSBundle mainBundle] pathForResource:@"StarLevel" ofType:@"plist"];
    menuArr01 = [[NSArray alloc] initWithContentsOfFile:starLevelPath];
    
    //
    NSString *travelPlanPath = [[NSBundle mainBundle] pathForResource:@"travelPlan" ofType:@"plist"];
    menuArr02 = [[NSArray alloc] initWithContentsOfFile:travelPlanPath];
    
    
    NSString *goIslandTypePath = [[NSBundle mainBundle] pathForResource:@"goIslandType" ofType:@"plist"];
    menuArr03 = [[NSArray alloc] initWithContentsOfFile:goIslandTypePath];
    
    // 添加下拉菜单
    menu = [[DistingDropDownMenu alloc] initWithOrigin:CGPointMake(0, 64) andHeight:44];
    menu.indicatorColor=[UIColor colorWithHexString:@"#686868"];
    menu.textColor=[UIColor colorWithHexString:@"#686868"];
    menu.textSelectedColor=[UIColor colorWithHexString:@"#0080c5"];
    menu.delegate = self;
    menu.dataSource = self;
    [menu setLayoutMargins:UIEdgeInsetsZero];
    [self.view addSubview:menu];
    
    // 创建menu 第一次显示 不会调用点击代理，可以用这个手动调用
    [menu selectDefalutIndexPath];
}

#pragma mark -DOPMenu Delegate
- (NSInteger)numberOfColumnsInMenu:(DistingDropDownMenu *)menu{
    return 3;
}

- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column{
    if (column == 0) {
        return menuArr01.count;
    }else if (column == 1){
        return menuArr02.count;
    }else {
        return menuArr03.count;
    }
}

- (NSString *)menu:(DistingDropDownMenu *)menu titleForRowAtIndexPath:(DistingIndexPath *)indexPath{
    
    if (indexPath.column == 0) {
        return menuArr01[indexPath.row][@"name"];
    } else if (indexPath.column == 1){
        return menuArr02[indexPath.row][@"name"];
    } else {
        return menuArr03[indexPath.row][@"name"];
    }
}


- (NSInteger)menu:(DistingDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column{
    if (column == 1) {
        if (row == 0) {
            return 0;
        } else if (row == 2){
            return 0;
        } else if (row == 3){
            return 0;
        }
    }
    return 0;
}

- (NSString *)menu:(DistingDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DistingIndexPath *)indexPath{
    if (indexPath.column == 0) {
        if (indexPath.row == 0) {
            return 0;
        } else if (indexPath.row == 2){
            return 0;
        } else if (indexPath.row == 3){
            return 0;
        }
    }
    return nil;
}

- (void)menu:(DistingDropDownMenu *)menu didSelectRowAtIndexPath:(DistingIndexPath *)indexPath{
    if (indexPath.column == 0) {
        firstId=menuArr01[indexPath.row][@"id"];
    }else if (indexPath.column == 1){
        secondId=menuArr02[indexPath.row][@"id"];
    }else{
        threeId=menuArr03[indexPath.row][@"id"];
    }
    [self fillterIslandRequestStarId:firstId gplaceId:secondId gIslandId:threeId];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return NiocellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MdIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[MdIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    DistinceShowModel *model=dataArr[indexPath.row];
    cell.model=model;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DistinceShowModel *model=dataArr[indexPath.row];
    
    IslandDetailController *islDetail=[[IslandDetailController alloc] init];
    if ([model.islandYwName isEqualToString:@"帕劳"]) {
        islDetail.detail_landType=PalauIslandType;
    }else{
        islDetail.detail_landType=MaldivesIslandType;
    }
    islDetail.islandName=model.islandTitle;
    islDetail.islandId=model.islandId;
    islDetail.islandImg=model.islandImg;
    islDetail.islandPId=model.pId;
    islDetail.mdId=model.mdId;
    islDetail.islandDescript=model.islandTitle;
    [self.navigationController pushViewController:islDetail animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestDistingceData{
    dataArr=[NSMutableArray array];
    [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"maldives/search.html?is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
        //        DSLog(@"%@",[DefaultHost stringByAppendingString:self.urlStr]);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //DSLog(@"%@",responseObject);
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"sealist"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                model.islandYwName=@"马尔代夫";
                [dataArr addObject:model];
            }
            [myTable reloadData];
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//筛选请求
-(void)fillterIslandRequestStarId:(NSString *)sId gplaceId:(NSString *)gplId gIslandId:(NSString *)gIsId{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    dataArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"typeid"];
    [dict setObject:sId forKey:@"dy"];
    [dict setObject:gplId forKey:@"jg"];
    [dict setObject:gIsId forKey:@"jt"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[@"http://m.youxia.com/" stringByAppendingString:@"maldives/search.html?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"sealist"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
