//
//  MdIslandOnlineCell.m
//  youxia
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "MdIslandOnlineCell.h"
#import <TTTAttributedLabel.h>

#define  NiocellHeight SCREENSIZE.width/3-12
@implementation MdIslandOnlineCell{
    UIImageView *imgv;
    UILabel *tagLabel;
    UIImageView *typeImgv;
    UILabel *typeNameLabel;
    UILabel *titleLabel;
    TTTAttributedLabel *priceLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imgv=[[UIImageView alloc] initWithFrame:CGRectMake(8, 12, SCREENSIZE.width/3, SCREENSIZE.width/3-36)];
        [self addSubview:imgv];
        
        CGRect frame=self.frame;
        frame.size.height=NiocellHeight;
        self.frame=frame;
        
        //
        tagLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+8, imgv.origin.y+2, 58, IS_IPHONE5?17:18)];
        tagLabel.textAlignment=NSTextAlignmentCenter;
        tagLabel.textColor=[UIColor whiteColor];
        tagLabel.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        tagLabel.layer.masksToBounds=YES;
        tagLabel.layer.cornerRadius=tagLabel.height/2;
        tagLabel.font=kFontSize11;
        [self addSubview:tagLabel];
        
        //
        typeImgv=[[UIImageView alloc]initWithFrame:CGRectMake(tagLabel.origin.x+tagLabel.width+10, tagLabel.origin.y-1, 20, IS_IPHONE5?19:20)];
        typeImgv.contentMode=UIViewContentModeScaleAspectFit;
        [self addSubview:typeImgv];
        
        //
        typeNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(typeImgv.origin.x+typeImgv.width+6, tagLabel.origin.y, SCREENSIZE.width-(typeImgv.origin.x+typeImgv.width+16), IS_IPHONE5?17:18)];
        typeNameLabel.textColor=[UIColor colorWithHexString:@"#484848"];
        typeNameLabel.font=kFontSize12;
        [self addSubview:typeNameLabel];
        
        //
        titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(tagLabel.origin.x, tagLabel.origin.y+tagLabel.height+(IS_IPHONE5?2:4), SCREENSIZE.width-(tagLabel.origin.x+8), IS_IPHONE5?34:36)];
        titleLabel.textColor=[UIColor colorWithHexString:@"#484848"];
        titleLabel.numberOfLines=0;
        titleLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
        [self addSubview:titleLabel];
        
        //
        priceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(titleLabel.origin.x, imgv.origin.y+imgv.height-(IS_IPHONE5?16:20), titleLabel.width, IS_IPHONE5?18:19)];
        priceLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?11:12];
        priceLabel.textColor=[UIColor colorWithHexString:@"#ed4523"];
        [self addSubview:priceLabel];
    }
    return self;
}

-(void)setModel:(DistinceShowModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x193"]];
    }
    
    tagLabel.text=model.starLevel;
    
    //starTypeImg--船    starTypeImg2--飞机
    
    typeNameLabel.text=model.jiaotong;
    if([model.jiaotong isEqualToString:@"水上飞机"]){
        typeImgv.image=[UIImage imageNamed:@"starTypeImg2"];
    }else{
        typeImgv.image=[UIImage imageNamed:@"starTypeImg"];
    }
    titleLabel.text=model.islandTitle;
    if ([model.islandType isEqualToString:@"自由行"]) {
        NSString *priceStr=[NSString stringWithFormat:@"分期就行:￥%@*9期",model.stagePrice];
        [priceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.stagePrice options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        NSString *priceStr=[NSString stringWithFormat:@"参考价￥%@",model.islandPrice];
        [priceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.islandPrice options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
