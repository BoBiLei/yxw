//
//  NewIslandOnlineCell.h
//  youxia
//
//  Created by mac on 16/4/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DistinceShowModel.h"

@interface NewIslandOnlineCell : UITableViewCell

@property (nonatomic,retain) DistinceShowModel *model;

@end
