//
//  MdIslandOnlineCell.h
//  youxia
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DistinceShowModel.h"

@interface MdIslandOnlineCell : UITableViewCell

@property (nonatomic,retain) DistinceShowModel *model;

@end
