//
//  NewIslandOnlineCell.m
//  youxia
//
//  Created by mac on 16/4/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "NewIslandOnlineCell.h"
#import <TTTAttributedLabel.h>

#import <UIImageView+WebCache.h>
#import <SDImageCache.h>
#import <SDWebImageManager.h>

#define  NiocellHeight SCREENSIZE.width/3-12
@implementation NewIslandOnlineCell{
    UIImageView *imgv;
    UILabel *tagLabel;
    UIImageView *typeImgv;
    UILabel *typeNameLabel;
    UILabel *titleLabel;
    TTTAttributedLabel *priceLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imgv=[[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:imgv];
        
        CGRect frame=self.frame;
        frame.size.height=NiocellHeight;
        self.frame=frame;
        
        //
        tagLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        tagLabel.textAlignment=NSTextAlignmentCenter;
        tagLabel.textColor=[UIColor whiteColor];
        tagLabel.backgroundColor=[UIColor colorWithHexString:@"#0080c5"];
        tagLabel.layer.masksToBounds=YES;
        tagLabel.layer.cornerRadius=tagLabel.height/2;
        tagLabel.font=kFontSize11;
        [self.contentView addSubview:tagLabel];
        
        //
        typeImgv=[[UIImageView alloc]initWithFrame:CGRectZero];
        typeImgv.contentMode=UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:typeImgv];
        
        //
        typeNameLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        typeNameLabel.textColor=[UIColor colorWithHexString:@"#484848"];
        typeNameLabel.font=kFontSize12;
        [self.contentView addSubview:typeNameLabel];
        
        //
        titleLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        titleLabel.textColor=[UIColor colorWithHexString:@"#484848"];
        titleLabel.numberOfLines=0;
        titleLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?13:14];
        [self.contentView addSubview:titleLabel];
        
        //
        priceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
        priceLabel.font=[UIFont systemFontOfSize:IS_IPHONE5?11:12];
        priceLabel.textColor=[UIColor colorWithHexString:@"#ed4523"];
        [self.contentView addSubview:priceLabel];
    }
    return self;
}

-(void)setModel:(DistinceShowModel *)model{
    
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_250x193"]];
    }
    
    tagLabel.text=model.islandType;
    typeImgv.image=[UIImage imageNamed:@"distince_type"];
    typeNameLabel.text=model.islandYwName;
    titleLabel.text=model.islandTitle;
    if ([model.islandType isEqualToString:@"自由行"]) {
        NSString *priceStr=[NSString stringWithFormat:@"分期就行:￥%@*9期",model.stagePrice];
        [priceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.stagePrice options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        NSString *priceStr=[NSString stringWithFormat:@"参考价￥%@",model.islandPrice];
        [priceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.islandPrice options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *boldSystemFont = [UIFont systemFontOfSize:IS_IPHONE5?14:15];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    imgv.frame=CGRectMake(8, 12, SCREENSIZE.width/3, SCREENSIZE.width/3-36);
    tagLabel.frame=CGRectMake(imgv.origin.x+imgv.width+8, imgv.origin.y+2, 50, IS_IPHONE5?17:18);
    typeImgv.frame=CGRectMake(tagLabel.origin.x+tagLabel.width+10, tagLabel.origin.y+1, 16, IS_IPHONE5?16:17);
    typeNameLabel.frame=CGRectMake(typeImgv.origin.x+typeImgv.width+4, typeImgv.origin.y-1, SCREENSIZE.width-(typeImgv.origin.x+typeImgv.width+16), IS_IPHONE5?17:18);
    titleLabel.frame=CGRectMake(tagLabel.origin.x, tagLabel.origin.y+tagLabel.height+(IS_IPHONE5?2:4), SCREENSIZE.width-(tagLabel.origin.x+8), IS_IPHONE5?34:36);
    priceLabel.frame=CGRectMake(titleLabel.origin.x, imgv.origin.y+imgv.height-(IS_IPHONE5?16:20), titleLabel.width, IS_IPHONE5?18:19);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
