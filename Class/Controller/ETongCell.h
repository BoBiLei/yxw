//
//  ETongCell.h
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelInSumDelegate <NSObject>

-(void)clickAddBtnAtIndexPatch:(NSIndexPath *)indexPatch;

-(void)clickJianBtnAtIndexPatch:(NSIndexPath *)indexPatch;

@end

@interface ETongCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *adult;

@property (weak, nonatomic) IBOutlet UILabel *countnum;

@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (weak, nonatomic) IBOutlet UIButton *jianBtn;

@property (weak, nonatomic) id<HotelInSumDelegate> delegate;

-(void)reflushData:(NSInteger)labelSum;

@end
