//
//  HotelMapController.h
//  youxia
//
//  Created by mac on 2017/4/14.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface HotelMapController : UIViewController
@property (nonatomic) MKMapType mapType;
@property(nonatomic)CGFloat longitude;
@property(nonatomic)CGFloat latitude;

@end
