//
//  RoomnumController.h
//  youxia
//
//  Created by mac on 2017/3/31.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomSumModel.h"

@interface RoomnumController : UIViewController


@property(nonatomic,copy)NSString *inventcount;         //库存
@property(nonatomic,copy)NSString *max_Occupancy;       //最大入住人数
@property(nonatomic,copy)NSString *max_Child;           //最大入住儿童数
@property(nonatomic,retain)NSMutableArray *dataArr;

@property(nonatomic,copy) void(^selectRoomSumBlock)(NSMutableArray *dataArr);

@end
