//
//
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelHomeController.h"
#import "NSMutableAttributedString+TY.h"
#import "RoomnumController.h"
#import "RoomnumberCell.h"
#import "RoomSumHeadCell.h"
#import "RoomSumAdultCell.h"
#import "RoomSumChildAgeCell.h"

@interface RoomnumController ()<UITableViewDelegate,UITableViewDataSource,RoomSumHeadDelegate,RoomSumAdultDelegate>

@end

@implementation RoomnumController{
    UITableView *myTable;
    UIButton *addBtn;       //房间+
    UIButton *jianBtn;      //房间-
    UILabel *roomSumLabel;  //房间数Label
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    [self seNavBar];
    
    [self addHeaderView];
    
    [self setTablew];
}

-(void)seNavBar{
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UIButton *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    btnlab.textAlignment=NSTextAlignmentCenter;
    btnlab.text=@"选择房间数及人数";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
}

-(void)fanhui{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 点击确定
-(void)queBut:(NSString *)send{
    for (RoomSumModel *model in _dataArr) {
        if (model.childArr.count!=0) {
            for (NSDictionary *dic in model.childArr) {
                NSString *str=dic[@"age"];
                if ([str isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:@"请填写儿童年龄" inView:self.view];
                    return;
                }
            }
        }
    }
    if (_selectRoomSumBlock) {
        _selectRoomSumBlock(_dataArr);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setTablew{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 112, SCREENSIZE.width, SCREENSIZE.height-112) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"RoomSumHeadCell" bundle:nil] forCellReuseIdentifier:@"RoomSumHeadCell"];
    [myTable registerNib:[UINib nibWithNibName:@"RoomSumAdultCell" bundle:nil] forCellReuseIdentifier:@"RoomSumAdultCell"];
    [myTable registerNib:[UINib nibWithNibName:@"RoomSumChildAgeCell" bundle:nil] forCellReuseIdentifier:@"RoomSumChildAgeCell"];
    [self.view addSubview:myTable];
    
    UIView *fView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 160)];
    myTable.tableFooterView=fView;
    
    UILabel *tipLabel=[[UILabel alloc]init];
    tipLabel.text=@"请按实际入住情况选择人数,否则入住时酒店可能会收取额外费用";
    tipLabel.numberOfLines = 0;
    tipLabel.frame=CGRectMake(16,0, SCREENSIZE.width-32, 44);
    tipLabel.font = [UIFont systemFontOfSize:14];
    tipLabel.textColor=[UIColor grayColor];
    [fView addSubview:tipLabel];
    
    //
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(10, tipLabel.origin.y+tipLabel.size.height+10, SCREENSIZE.width-25, 44);
    confirmBtn.backgroundColor=[UIColor colorWithRed:70/255.0 green:161/255.0 blue:236/255.0 alpha:1];
    [confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirmBtn.layer.cornerRadius = 4.0;
    confirmBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(queBut:) forControlEvents:UIControlEventTouchUpInside];
    [fView addSubview:confirmBtn];
}

-(void)addHeaderView{
    UIView *roomview=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 48)];
    [roomview setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:roomview];
    
    UILabel *labe=[[UILabel alloc]initWithFrame:CGRectMake(16, 0, 80, roomview.height)];
    labe.text=@"房间数";
    labe.font=kFontSize16;
    [roomview addSubview:labe];
    
    //addBtn
    addBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.frame = CGRectMake(SCREENSIZE.width-roomview.height-10, 0, roomview.height, roomview.height);
    [addBtn setImage:[UIImage imageNamed:@"hotelsum_add"] forState:UIControlStateNormal];
    addBtn.enabled=self.dataArr.count>=5?NO:YES;
    [addBtn addTarget:self action:@selector(clickAddRoomBtn:) forControlEvents:UIControlEventTouchUpInside];
    [roomview addSubview:addBtn];
    
    //roomSumLabel
    roomSumLabel=[[UILabel alloc]initWithFrame:CGRectMake(addBtn.origin.x-24, 0, 24, roomview.height)];
    roomSumLabel.text=[NSString stringWithFormat:@"%ld",self.dataArr.count];
    roomSumLabel.textAlignment=NSTextAlignmentCenter;
    roomSumLabel.font=kFontSize16;
    [roomview addSubview:roomSumLabel];
    
    //jianBtn
    jianBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    jianBtn.frame = CGRectMake(roomSumLabel.origin.x-roomview.height, 0, roomview.height, roomview.height);
    jianBtn.enabled=self.dataArr.count>1?YES:NO;
    [jianBtn setImage:[UIImage imageNamed:@"hotelsum_jian"] forState:UIControlStateNormal];
    [jianBtn addTarget:self action:@selector(clickJianRoomBtn:) forControlEvents:UIControlEventTouchUpInside];
    [roomview addSubview:jianBtn];
}

#pragma mark - table delegate
//section的个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}

//第section分区一共有多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    RoomSumModel *model=_dataArr[section];
    if (_max_Child.integerValue==0) {
        return 2;
    }else{
        return 3+model.childArr.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        RoomSumHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoomSumHeadCell"];
        if (!cell) {
            cell = [[RoomSumHeadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomSumHeadCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text = [NSString stringWithFormat:@"房间 %ld",indexPath.section+1];
        cell.cleanBtn.hidden=indexPath.section==0?YES:NO;
        cell.delegate=self;
        return cell;
    }else if(indexPath.row==1){
        RoomSumAdultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoomSumAdultCell"];
        if (!cell) {
            cell = [[RoomSumAdultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomSumAdultCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text = @"成人";
        RoomSumModel *model=_dataArr[indexPath.section];
        
        //成人数+儿童数不能大于可用房间数
        if (model.adultSum.count>0&&model.adultSum.count+model.childArr.count<_max_Occupancy.integerValue) {
            cell.addBtn.enabled=YES;
        }else{
            cell.addBtn.enabled=NO;
        }
        
        if (model.adultSum.count==1) {
            cell.jianBtn.enabled=NO;
        }else{
            cell.jianBtn.enabled=YES;
        }
        cell.sumLabel.text = [NSString stringWithFormat:@"%ld",model.adultSum.count];
        cell.delegate=self;
        return cell;
    }else if(indexPath.row==2){
        RoomSumAdultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoomSumAdultCell"];
        if (!cell) {
            cell = [[RoomSumAdultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomSumAdultCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text = @"儿童";
        RoomSumModel *model=_dataArr[indexPath.section];
        cell.sumLabel.text = [NSString stringWithFormat:@"%ld",model.childArr.count];
        //成人数+儿童数不能大于可用房间数
        if (model.childArr.count<_max_Occupancy.integerValue-model.adultSum.count) {
            cell.addBtn.enabled=YES;
        }else{
            cell.addBtn.enabled=NO;
        }
        
        if (model.childArr.count==0) {
            cell.jianBtn.enabled=NO;
        }else{
            cell.jianBtn.enabled=YES;
        }
        cell.delegate=self;
        return cell;
    }else{
        RoomSumChildAgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoomSumChildAgeCell"];
        if (!cell) {
            cell = [[RoomSumChildAgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomSumChildAgeCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.leftLabel.text=[NSString stringWithFormat:@"儿童年龄 %ld",indexPath.row-2];
        
        RoomSumModel *model=_dataArr[indexPath.section];
        if(model.childArr.count!=0){
            NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:model.childArr[indexPath.row-3]];
            cell.childAgeTF.text = [NSString stringWithFormat:@"%@",dic[@"age"]];
            cell.RoomSumChildAgeBlock=^(NSString *text){
                [dic setObject:text forKey:@"age"];
                [model.childArr replaceObjectAtIndex:indexPath.row-3 withObject:dic];
                [_dataArr replaceObjectAtIndex:indexPath.section withObject:model];
            };
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?16:0.00000001;
}

#pragma mark - Section 删除按钮
-(void)clickCleanBtn:(NSIndexPath *)indexPach{
    [_dataArr removeObjectAtIndex:indexPach.section];
    if(_dataArr.count<=1){
        jianBtn.enabled=NO;
        addBtn.enabled=YES;
    }else{
        jianBtn.enabled=YES;
        addBtn.enabled=YES;
    }
    roomSumLabel.text=[NSString stringWithFormat:@"%ld",_dataArr.count];
    [myTable reloadData];
}

#pragma mark - 点击成人或儿童加
-(void)clickAddBtn:(NSIndexPath *)indexPach{
    if (indexPach.row==1) {
        RoomSumModel *model=_dataArr[indexPach.section];
        NSInteger sumInt=model.adultSum.count;
        sumInt++;
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        for (int i=0;i<sumInt;i++) {
            [dic setObject:@"" forKey:@"firstName"];
            [dic setObject:@"" forKey:@"lastName"];
        }
        [model.adultSum addObject:dic];
        [_dataArr replaceObjectAtIndex:indexPach.section withObject:model];
        [myTable reloadData];
    }else if (indexPach.row==2){
        RoomSumModel *model=_dataArr[indexPach.section];
        [model.childArr addObject:@{@"firstName":@"",@"lastName":@"",@"age":@""}];
        [_dataArr replaceObjectAtIndex:indexPach.section withObject:model];
        [myTable reloadData];
    }
}

#pragma mark - 点击成人或儿童减
-(void)clickJianBtn:(NSIndexPath *)indexPach{
    if (indexPach.row==1) {
        RoomSumModel *model=_dataArr[indexPach.section];
        NSInteger sumInt=model.adultSum.count;
        sumInt--;
        [model.adultSum removeLastObject];
        [_dataArr replaceObjectAtIndex:indexPach.section withObject:model];
        [myTable reloadData];
    }else if (indexPach.row==2){
        RoomSumModel *model=_dataArr[indexPach.section];
        [model.childArr removeLastObject];
        [_dataArr replaceObjectAtIndex:indexPach.section withObject:model];
        [myTable reloadData];
    }
}

#pragma mark - 点击房间加
-(void)clickAddRoomBtn:(id)sender{
    RoomSumModel *model=[RoomSumModel new];
    model.adultSum=[NSMutableArray arrayWithObject:@{@"firstName":@"",@"lastName":@"",@"age":@""}];
    model.childArr=[NSMutableArray array];
    [_dataArr addObject:model];
    
    /*
        如果库存>5 最多只能要5间房
        否则，取最大库存
     */
    if (_inventcount.intValue<=5) {
        if (_dataArr.count<_inventcount.intValue) {
            addBtn.enabled=YES;
            jianBtn.enabled=YES;
        }else{
            addBtn.enabled=NO;
            jianBtn.enabled=YES;
        }
    }else{
        if (_dataArr.count<5) {          //最多只能选择5间房
            addBtn.enabled=YES;
            jianBtn.enabled=YES;
        }else{
            addBtn.enabled=NO;
            jianBtn.enabled=YES;
        }
    }
    
    roomSumLabel.text=[NSString stringWithFormat:@"%ld",_dataArr.count];
    
    [myTable reloadData];
}

#pragma mark - 点击房间减
-(void)clickJianRoomBtn:(NSIndexPath *)index{
    [_dataArr removeLastObject];
    if(_dataArr.count<=1){
        jianBtn.enabled=NO;
        addBtn.enabled=YES;
    }else{
        jianBtn.enabled=YES;
        addBtn.enabled=YES;
    }
    roomSumLabel.text=[NSString stringWithFormat:@"%ld",_dataArr.count];
    
    [myTable reloadData];
}

@end
