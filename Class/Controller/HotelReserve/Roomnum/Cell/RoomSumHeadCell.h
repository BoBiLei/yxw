//
//  RoomSumHeadCell.h
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RoomSumHeadDelegate <NSObject>

-(void)clickCleanBtn:(NSIndexPath *)indexPach;

@end

@interface RoomSumHeadCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIButton *cleanBtn;
@property (weak, nonatomic) id <RoomSumHeadDelegate> delegate;

@end
