//
//  RoomSumChildAgeCell.m
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RoomSumChildAgeCell.h"

@implementation RoomSumChildAgeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)ageDidChange:(id)sender {
    UITextField *field = (UITextField *)sender;
    if (self.RoomSumChildAgeBlock) {
        self.RoomSumChildAgeBlock(field.text);
    }
}

@end
