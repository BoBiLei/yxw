//
//  RoomSumModel.h
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomSumModel : NSObject

@property (nonatomic, retain) NSMutableArray *adultSum;
@property (nonatomic, retain) NSMutableArray *childArr;
@property (nonatomic, copy) NSString *age;

@end
