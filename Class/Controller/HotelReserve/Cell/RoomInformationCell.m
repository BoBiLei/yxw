//
//  RoomInformationCell.m
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RoomInformationCell.h"

@implementation RoomInformationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CGFloat labWidth=(SCREENSIZE.width-64-38-28)/2;
    self.firstName=[[UITextField alloc] initWithFrame:CGRectMake(102, 0, labWidth, self.height)];
    self.firstName.font=kFontSize16;
    self.firstName.textColor=[UIColor grayColor];
    self.firstName.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.firstName.placeholder=@"拼音(姓)";
    self.firstName.textAlignment=NSTextAlignmentCenter;
    [self.firstName addTarget:self action:@selector(firstNameChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.firstName];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(self.firstName.origin.x+self.firstName.width+7, 16, 0.5, 14)];
    line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [self addSubview:line];
    
    //
    self.lastName=[[UITextField alloc] initWithFrame:CGRectMake(self.firstName.origin.x+self.firstName.width+14, 0, labWidth, self.height)];
    self.lastName.font=kFontSize16;
    self.lastName.textColor=[UIColor grayColor];
    self.lastName.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.lastName.placeholder=@"拼音(名)";
    self.lastName.textAlignment=NSTextAlignmentCenter;
    [self.lastName addTarget:self action:@selector(lastNameChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.lastName];
    
    //
    UIView *topLine=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 0.5)];
    topLine.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [self addSubview:topLine];
}

#pragma mark - textField
- (void)firstNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.RoomInforFirstNameBlock) {
        self.RoomInforFirstNameBlock(field.text);
    }
}

- (void)lastNameChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    if (self.RoomInforLastNameBlock) {
        self.RoomInforLastNameBlock(field.text);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
