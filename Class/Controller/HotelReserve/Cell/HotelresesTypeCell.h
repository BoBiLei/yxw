//
//  HotelresesTypeCell.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelresesTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *miyueBtn;
@property (weak, nonatomic) IBOutlet UIButton *handBtn;
@property (weak, nonatomic) IBOutlet UIButton *quietBtn;
@property (weak, nonatomic) IBOutlet UIButton *adjacentBtn;

@end
