//
//  HoteLableCell.m
//  youxia
//
//  Created by mac on 2017/3/24.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteLableCell.h"

@implementation HoteLableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    NSString *labelTtt=@"此价格为优惠价，预定后只需在酒店前台提供备有入住人姓名的有效证件即可办理入住。";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:8];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    self.myLabel.attributedText = attributedString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
