//
//  HoteLabTitleCell.m
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteLabTitleCell.h"

@implementation HoteLabTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reflushData:(HoteStandardModel *)model atIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            self.leftLabel.text=@"早餐";
            self.rightLabel.text=model.breakfast;
            break;
        case 1:
            self.leftLabel.text=@"床型";
            self.rightLabel.text=model.bedtype;
            break;
        case 2:
            self.leftLabel.text=@"默认入住人数";
            self.rightLabel.text=model.DefaultOccupancy;
            break;
        case 3:
            self.leftLabel.text=@"最大入住人数";
            self.rightLabel.text=model.MaxOccupancy;
            break;
        case 4:
            self.leftLabel.text=@"是否接待儿童";
            self.rightLabel.text=model.receiveChild;
            break;
        default:
        {
            self.leftLabel.text=@"提供房间设施";
            if (model.roomFacility!=0) {
                NSString *str=@"";
                for (NSDictionary *dic in model.roomFacility) {
                    str=[str stringByAppendingString:[NSString stringWithFormat:@"%@",dic[@"Name_CN"]]];
                }
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                
                [paragraphStyle setLineSpacing:8];//调整行间距
                
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
                self.rightLabel.attributedText = attributedString;
            }
        }

            break;
    }
}

@end
