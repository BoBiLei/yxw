//
//  HoteStandardModel.m
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteStandardModel.h"

@implementation HoteStandardModel

-(void)jsonToModel:(NSDictionary *)dic{
    if ([dic[@"DefaultOccupancy"] isKindOfClass:[NSNull class]]) {
        self.DefaultOccupancy=@"";
    }else{
        self.DefaultOccupancy=[NSString stringWithFormat:@"%@",dic[@"DefaultOccupancy"]];
    }
    
    if ([dic[@"MaxOccupancy"] isKindOfClass:[NSNull class]]) {
        self.MaxOccupancy=@"";
    }else{
        self.MaxOccupancy=[NSString stringWithFormat:@"%@",dic[@"MaxOccupancy"]];
    }
    
    if ([dic[@"bedtype"] isKindOfClass:[NSNull class]]) {
        self.bedtype=@"";
    }else{
        self.bedtype=[NSString stringWithFormat:@"%@",dic[@"bedtype"]];
    }
    
    if ([dic[@"breakfast"] isKindOfClass:[NSNull class]]) {
        self.breakfast=@"";
    }else{
        self.breakfast=[NSString stringWithFormat:@"%@",dic[@"breakfast"]];
    }
    
    if ([dic[@"receiveChild"] isKindOfClass:[NSNull class]]) {
        self.receiveChild=@"";
    }else{
        self.receiveChild=[NSString stringWithFormat:@"%@",dic[@"receiveChild"]];
    }
    
    //
    if ([dic[@"roomFacility"] isKindOfClass:[NSNull class]]) {
        self.roomFacility=@[];
    }else{
        self.roomFacility=dic[@"roomFacility"];
    }
}

@end
