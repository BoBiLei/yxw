//
//  HoteStandardController.h
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteStandardController : UIViewController

@property (nonatomic,copy)NSString *hoteID;     //酒店id
@property(nonatomic,copy)NSString *reteplanID;  //房型id
@end
