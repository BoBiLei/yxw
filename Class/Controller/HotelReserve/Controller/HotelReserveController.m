//
//  HotelReserveController.m
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelReserveController.h"
#import "HotelreservationCell.h"
#import "RoomInformationCell.h"
#import "HoteLableCell.h"
#import "HoteStandardController.h"
#import "RoomnumController.h"
#import <AddressBook/AddressBook.h>
#import "HoteEtongCell.h"
#import <AddressBookUI/AddressBookUI.h>
#import "RoomSumHeadCell.h"
#import "CusAskBtn.h"
#import "PSCHotelOrderDetail.h"

@interface HotelReserveController ()<UITableViewDelegate,UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate,UITextFieldDelegate,TTTAttributedLabelDelegate,UITextFieldDelegate>

@end

@implementation HotelReserveController{
    NSMutableArray *dataArr;
    UITableView *myTable;
    UIView *footView;
    NSMutableArray *specialArr;                 //常见要求Arr
    
    UIButton *buyBtn;                           //购买按钮
    TTTAttributedLabel *totalPriceLabel;        //总价格label
    UILabel *askLabel01;
    UILabel *askLabel02;
    UILabel *sjLabel02;
    UISwitch *askSwitch;                        //特殊要求
    UISwitch *shoujuSwitch;                        //需要收据
    CusAskBtn *seleBtn;
    BOOL isSeleSakBtn;                          //是否选择了常见要求button
    
    NSString *specialStr;   //提交的常见要求参数
    NSString *hadInvoice;   //是否需要发票1是,0否 提交的参数
    
    UITextField *contact_firstName;             //联系人姓
    UITextField *contact_lastName;              //联系人名
    UITextField *phoneTF;                       //联系人手机号
    UITextField *eMailTF;                       //联系人邮箱
    UITextField *specialField;                  //特殊要求
    UITextField *shoujuTF;                      //收据抬头
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"酒店预定"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:NO];
    NSLog(@"----");
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"酒店预定"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpCustomSearBar];
    
    //设置默认房间数
    dataArr=[NSMutableArray array];
    RoomSumModel *model=[RoomSumModel new];
    model.adultSum=[NSMutableArray array];
    [model.adultSum addObject:@{@"firstName":@"",@"lastName":@"",@"age":@""}];
    model.childArr=[NSMutableArray array];
    [dataArr addObject:model];
    
    specialStr=@"";
    hadInvoice=@"1";
    [self specialRequest];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 164)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturn) forControlEvents:UIControlEventTouchUpInside];
    self.navReturnBtn=back;
    [self.view addSubview:back];
    
    UILabel *titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(54, 20, SCREENSIZE.width-108, 44)];
    titleLabel.text=_hoteName;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    [titleLabel setTextColor:[UIColor whiteColor]];
    [self.view addSubview:titleLabel];
    
    UIImageView *headView=[[UIImageView alloc] initWithFrame:CGRectMake(24, 66, cusBar.frame.size.width-48, 88)];
    headView.userInteractionEnabled=YES;
    headView.alpha=0.9;
    headView.layer.cornerRadius=3.0;
    headView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:headView];
    
    //
    UILabel *fangxing=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 44)];
    fangxing.text=_rateplanName;
    fangxing.font=[UIFont systemFontOfSize:16];
    [headView addSubview:fangxing];
    
    //
    UIButton *detailBtn=[[UIButton alloc]initWithFrame:CGRectMake(headView.width-90, 0, 80, 44)];
    detailBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [detailBtn setTitle:@"房屋详情" forState:UIControlStateNormal];
    detailBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [detailBtn setTitleColor:[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1] forState:UIControlStateNormal];
    [detailBtn addTarget:self action:@selector(clientBtnfan:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:detailBtn];
    
    //
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 44, headView.frame.size.width, 0.5)];
    [line setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
    [headView addSubview:line];
    
    //入住
    UILabel *inLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 44.5, 126, 44)];
    inLabel.text=[NSString stringWithFormat:@"入住: %@",_starDate];
    inLabel.font = [UIFont systemFontOfSize:15];
    inLabel.textColor=[UIColor grayColor];
    [headView addSubview:inLabel];
    
    //离店
    UILabel *outLabel=[[UILabel alloc]initWithFrame:CGRectMake(inLabel.size.width+inLabel.origin.x+6,  inLabel.origin.y, inLabel.width, inLabel.height)];
    outLabel.textColor=[UIColor grayColor];
    outLabel.text=[NSString stringWithFormat:@"离店: %@",_endDate];
    outLabel.font = [UIFont systemFontOfSize:15];
    [headView addSubview:outLabel];
    
    UILabel *sumLabel=[[UILabel alloc]initWithFrame:CGRectMake(headView.width-70, line.size.height+line.origin.y, 60, 44)];
    sumLabel.text=[NSString stringWithFormat:@"%@晚",_numDay];
    sumLabel.textColor=[UIColor grayColor];
    sumLabel.textAlignment=NSTextAlignmentRight;
    sumLabel.font = [UIFont systemFontOfSize:15];
    [headView addSubview:sumLabel];
}

-(void)clickReturn{
    [self .navigationController popViewControllerAnimated:YES];
}

#pragma mark - 酒店房屋详情

-(void)clientBtnfan:(NSString *)tit{
    HoteStandardController *stand=[[HoteStandardController alloc]init];
    stand.hoteID=_hoteid;
    stand.reteplanID=_rateplanId;
    [self.navigationController pushViewController:stand animated:YES];
}

-(void)setTupTableView{
    self.automaticallyAdjustsScrollViewInsets = NO; //解决表无故偏移
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 164, SCREENSIZE.width, SCREENSIZE.height-164) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"RoomSumHeadCell" bundle:nil] forCellReuseIdentifier:@"RoomSumHeadCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteLableCell" bundle:nil] forCellReuseIdentifier:@"HoteLableCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HotelreservationCell" bundle:nil] forCellReuseIdentifier:@"HotelreservationCell"];
    [myTable registerNib:[UINib nibWithNibName:@"RoomInformationCell" bundle:nil] forCellReuseIdentifier:@"RoomInformationCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteEtongCell" bundle:nil] forCellReuseIdentifier:@"HoteEtongCell"];
    [self.view addSubview:myTable];
    
    [self setUpFooterView];
}

-(void)setUpFooterView{
#pragma mark footView
    footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 1000)];
    footView.backgroundColor=[UIColor whiteColor];
    myTable.tableFooterView=footView;
    
#pragma mark contactView
    UIView *contactView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 46)];
    contactView.backgroundColor=[UIColor whiteColor];
    [footView addSubview:contactView];
    
    UIView *vLine01=[[UIView alloc] initWithFrame:CGRectMake(0, 14, 2, 18)];
    vLine01.backgroundColor=[UIColor colorWithHexString:@"33AEF0"];
    [contactView addSubview:vLine01];
    
    UILabel *contactTip=[[UILabel alloc]initWithFrame:CGRectMake(14,0, contactView.width-28, contactView.height)];
    contactTip.text=@"联系人信息";
    contactTip.font=kFontSize16;
    contactTip.textColor=[UIColor colorWithHexString:@"33AEF0"];
    [contactView addSubview:contactTip];
    
    UIView *contactLine01=[[UIView alloc]initWithFrame:CGRectMake(0, contactView.origin.y+contactView.height, SCREENSIZE.width, 0.5f)];
    contactLine01.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:contactLine01];
    
    //联系人
    UILabel *contactLabel=[[UILabel alloc]initWithFrame:CGRectMake(14, contactLine01.origin.y+contactLine01.size.height, 100, 46)];
    contactLabel.text=@"联系人";
    contactLabel.font=kFontSize16;
    [footView addSubview:contactLabel];
    
    
    CGFloat labWidth=(SCREENSIZE.width-64-38-28)/2;
    contact_firstName=[[UITextField alloc] initWithFrame:CGRectMake(102, contactLabel.origin.y, labWidth, contactLabel.height)];
    contact_firstName.font=kFontSize16;
    contact_firstName.textColor=[UIColor grayColor];
    contact_firstName.clearButtonMode=UITextFieldViewModeWhileEditing;
    contact_firstName.placeholder=@"拼音(姓)";
    contact_firstName.textAlignment=NSTextAlignmentCenter;
    [footView addSubview:contact_firstName];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(contact_firstName.origin.x+contact_firstName.width+7, contact_firstName.origin.y+16, 0.5, 14)];
    line.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:line];
    
    //
    contact_lastName=[[UITextField alloc] initWithFrame:CGRectMake(contact_firstName.origin.x+contact_firstName.width+14, contact_firstName.origin.y, labWidth, contact_firstName.height)];
    contact_lastName.font=kFontSize16;
    contact_lastName.textColor=[UIColor grayColor];
    contact_lastName.clearButtonMode=UITextFieldViewModeWhileEditing;
    contact_lastName.placeholder=@"拼音(名)";
    contact_lastName.textAlignment=NSTextAlignmentCenter;
    [footView addSubview:contact_lastName];
    
    //
//    contactName=[[UITextField alloc]initWithFrame:CGRectMake(contactLabel.origin.x+contactLabel.size.width+12, contactLabel.origin.y, SCREENSIZE.width-(contactLabel.origin.x*2+contactLabel.size.width+12), contactLabel.height)];
//    contactName.placeholder=@"姓名[可输入中文]";
//    contactName.font=kFontSize16;
//    contactName.textColor=[UIColor grayColor];
//    contactName.clearButtonMode=UITextFieldViewModeWhileEditing;
//    [footView addSubview:contactName];
    
    UIView *contactLine02=[[UIView alloc]initWithFrame:CGRectMake(0, contactLabel.origin.y+contactLabel.frame.size.height, SCREENSIZE.width, 0.5f)];
    [contactLine02 setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
    [footView addSubview:contactLine02];
    
    //手机号
    UILabel *phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(contactLabel.origin.x, contactLine02.origin.y+contactLine02.height, contactLabel.width, contactLabel.height)];
    phoneLabel.text=@"手机号";
    phoneLabel.font=kFontSize16;
    [footView addSubview:phoneLabel];
    
    UIButton *contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    contactBtn.frame=CGRectMake(SCREENSIZE.width-56, phoneLabel.origin.y, 46, 46);
    [contactBtn setImage:[UIImage imageNamed:@"1_033.png"] forState:UIControlStateNormal];
    contactBtn.backgroundColor = [UIColor clearColor];
    [contactBtn addTarget:self action:@selector(btnsearchclicked:) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:contactBtn];
    
    UIView *ctLine=[[UIView alloc]initWithFrame:CGRectMake(contactBtn.origin.x-8.5, phoneLabel.origin.y+14, 0.5, 18)];
    [ctLine setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
    [footView addSubview:ctLine];
    
    phoneTF=[[UITextField alloc]initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.size.width+12, phoneLabel.origin.y, ctLine.origin.x-(phoneLabel.origin.x+phoneLabel.size.width+12), phoneLabel.height)];
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    phoneTF.placeholder=@"用于接收确认短信";
    phoneTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTF.font=kFontSize16;
    phoneTF.textColor=[UIColor grayColor];
    [footView addSubview:phoneTF];
    
    UIView *contactLine03=[[UIView alloc]initWithFrame:CGRectMake(0, phoneLabel.origin.y+phoneLabel.size.height, SCREENSIZE.width, 0.5f)];
    [contactLine03 setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
    [footView addSubview:contactLine03];
    
    //E-mail
    UILabel *emLabel=[[UILabel alloc]initWithFrame:CGRectMake(contactLabel.origin.x, contactLine03.origin.y+contactLine03.height, contactLabel.width, contactLabel.height)];
    emLabel.text=@"E-mail";
    emLabel.font=kFontSize16;
    [footView addSubview:emLabel];
    
    
    eMailTF=[[UITextField alloc]initWithFrame:CGRectMake(emLabel.origin.x+emLabel.size.width+12,emLabel.origin.y, SCREENSIZE.width-(emLabel.origin.x*2+emLabel.size.width+12), contactLabel.height)];
    eMailTF.placeholder=@"用于接收确认邮件";
    eMailTF.font=kFontSize16;
    eMailTF.textColor=[UIColor grayColor];
    eMailTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [footView addSubview:eMailTF];
    
    
#pragma mark 特殊要求
    UIView *secView01=[[UIView alloc]initWithFrame:CGRectMake(0, emLabel.origin.y+emLabel.size.height, SCREENSIZE.width, 14)];
    [secView01 setBackgroundColor:[UIColor colorWithHexString:@"f2f2f2"]];
    [footView addSubview:secView01];
    
    UIView *vLine02=[[UIView alloc] initWithFrame:CGRectMake(0, secView01.origin.y+secView01.height+14, 2, 18)];
    vLine02.backgroundColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:vLine02];
    
    UILabel *askTip=[[UILabel alloc]initWithFrame:CGRectMake(14,secView01.origin.y+secView01.height, 120, contactView.height)];
    askTip.text=@"特殊要求";
    askTip.font=kFontSize16;
    askTip.textColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:askTip];
    
    askSwitch=[[UISwitch alloc] initWithFrame:CGRectMake(SCREENSIZE.width-66, askTip.origin.y+8, 48, 28)];
    askSwitch.onTintColor=[UIColor colorWithHexString:@"ffa800"];
    askSwitch.on=YES;
    [askSwitch addTarget:self action:@selector(askSwitchChange:) forControlEvents:UIControlEventValueChanged];
    [footView addSubview:askSwitch];
    
    UIView *askLine01=[[UIView alloc]initWithFrame:CGRectMake(0, askTip.origin.y+askTip.height, SCREENSIZE.width, 0.5f)];
    askLine01.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:askLine01];
    
    UILabel *askTip02=[[UILabel alloc]initWithFrame:CGRectMake(14,askLine01.origin.y+askLine01.height, SCREENSIZE.width-28, 40)];
    askTip02.text=@"酒店无法保证您的特殊要求,但会尽力安排";
    askTip02.font=kFontSize15;
    askTip02.textColor=[UIColor colorWithHexString:@"ababab"];
    [footView addSubview:askTip02];
    
    UIView *askLine02=[[UIView alloc]initWithFrame:CGRectMake(0, askTip02.origin.y+askTip02.height, SCREENSIZE.width, 0.5f)];
    askLine02.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:askLine02];
    
    
    askLabel01=[[UILabel alloc]initWithFrame:CGRectMake(askTip.origin.x, askLine02.origin.y+askLine02.height, contactLabel.width, contactLabel.height)];
    askLabel01.text=@"常见要求";
    askLabel01.font=kFontSize16;
    [footView addSubview:askLabel01];
    
    CGFloat btnWidth=120;
    CGFloat btnX=0;
    CGFloat btnY=0;
    for (int i=0; i<specialArr.count; i++) {
        btnX=(i%2)*btnWidth+(i%2)*10+askLabel01.origin.x+askLabel01.width;
        btnY=(i/2)*askLabel01.height+(i/2)*0.5+askLabel01.origin.y;
        CusAskBtn *askBtn= [[CusAskBtn alloc] initWithFrame:CGRectMake(btnX, btnY, btnWidth, 46) title:specialArr[i] image:[UIImage imageNamed:@"hotelorder_nor"]];
        [askBtn addTarget:self action:@selector(clickAskBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:askBtn];
    }
    
    UIView *askLine03=[[UIView alloc]initWithFrame:CGRectMake(0, askLabel01.origin.y+askLabel01.height, SCREENSIZE.width, 0.5f)];
    askLine03.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    askLine03.hidden=specialArr.count<=2?YES:NO;
    [footView addSubview:askLine03];
    
    UIView *askLine04=[[UIView alloc]initWithFrame:CGRectMake(0, askLabel01.origin.y+askLabel01.height*((specialArr.count-1)/2+1), SCREENSIZE.width, 0.5f)];
    askLine04.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:askLine04];
    
    askLabel02=[[UILabel alloc]initWithFrame:CGRectMake(askTip.origin.x, askLine04.origin.y+askLine04.height, contactLabel.width, contactLabel.height)];
    askLabel02.text=@"特殊要求";
    askLabel02.font=kFontSize16;
    [footView addSubview:askLabel02];
    
    specialField=[[UITextField alloc]initWithFrame:CGRectMake(askLabel02.origin.x+askLabel02.size.width+12, askLabel02.origin.y, eMailTF.width, eMailTF.height)];
    specialField.placeholder=@"请填写你的要求";
    specialField.font=kFontSize16;
    specialField.textColor=[UIColor grayColor];
    specialField.clearButtonMode=UITextFieldViewModeWhileEditing;
    specialField.delegate=self;
    [specialField addTarget:self action:@selector(specialFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [footView addSubview:specialField];
    
#pragma mark 需要收据
    UIView *secView02=[[UIView alloc]initWithFrame:CGRectMake(0, askLabel02.origin.y+askLabel02.size.height, SCREENSIZE.width, 14)];
    [secView02 setBackgroundColor:[UIColor colorWithHexString:@"f2f2f2"]];
    [footView addSubview:secView02];
    
    UIView *vLine03=[[UIView alloc] initWithFrame:CGRectMake(0, secView02.origin.y+secView02.height+14, 2, 18)];
    vLine03.backgroundColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:vLine03];
    
    UILabel *sjLabel=[[UILabel alloc]initWithFrame:CGRectMake(14,secView02.origin.y+secView02.height, 120, contactView.height)];
    sjLabel.text=@"需要收据";
    sjLabel.font=kFontSize16;
    sjLabel.textColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:sjLabel];
    
    shoujuSwitch=[[UISwitch alloc] initWithFrame:CGRectMake(SCREENSIZE.width-66, sjLabel.origin.y+8, 48, 28)];
    shoujuSwitch.onTintColor=[UIColor colorWithHexString:@"ffa800"];
    shoujuSwitch.on=YES;
    [shoujuSwitch addTarget:self action:@selector(speSwitchChange:) forControlEvents:UIControlEventValueChanged];
    [footView addSubview:shoujuSwitch];
    
    UIView *sjLine01=[[UIView alloc]initWithFrame:CGRectMake(0, sjLabel.origin.y+askLabel01.height, SCREENSIZE.width, 0.5f)];
    sjLine01.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:sjLine01];
    
    sjLabel02=[[UILabel alloc]initWithFrame:CGRectMake(sjLabel.origin.x, sjLabel.origin.y+sjLabel.height, contactLabel.width, contactLabel.height)];
    sjLabel02.text=@"收据抬头";
    sjLabel02.font=kFontSize16;
    [footView addSubview:sjLabel02];
    
    shoujuTF=[[UITextField alloc]initWithFrame:CGRectMake(askLabel02.origin.x+askLabel02.size.width+12, sjLabel02.origin.y, eMailTF.width, eMailTF.height)];
    shoujuTF.placeholder=@"请填写公司或个人名";
    shoujuTF.font=kFontSize16;
    shoujuTF.textColor=[UIColor grayColor];
    shoujuTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    [footView addSubview:shoujuTF];
    
#pragma mark 入住须知
    UIView *secView03=[[UIView alloc]initWithFrame:CGRectMake(0, sjLabel02.origin.y+sjLabel02.size.height, SCREENSIZE.width, 14)];
    [secView03 setBackgroundColor:[UIColor colorWithHexString:@"f2f2f2"]];
    [footView addSubview:secView03];
    
    UIView *vLine04=[[UIView alloc] initWithFrame:CGRectMake(0, secView03.origin.y+secView03.height+14, 2, 18)];
    vLine04.backgroundColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:vLine04];
    
    UILabel *inTipLabel=[[UILabel alloc]initWithFrame:CGRectMake(14,secView03.origin.y+secView03.height, 120, contactView.height)];
    inTipLabel.text=@"入住须知";
    inTipLabel.font=kFontSize16;
    inTipLabel.textColor=[UIColor colorWithHexString:@"33AEF0"];
    [footView addSubview:inTipLabel];
    
    UIView *inLine01=[[UIView alloc]initWithFrame:CGRectMake(0, inTipLabel.origin.y+inTipLabel.height, SCREENSIZE.width, 0.5f)];
    inLine01.backgroundColor=[UIColor colorWithHexString:@"c7c7c7"];
    [footView addSubview:inLine01];
    
    TTTAttributedLabel *inDetail=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(inTipLabel.origin.x, inLine01.origin.y+4, SCREENSIZE.width-2*inTipLabel.origin.x, 118)];
    inDetail.textColor=[UIColor colorWithHexString:@"ababab"];
    inDetail.font=kFontSize15;
    inDetail.numberOfLines=0;
    inDetail.delegate=self;
    [footView addSubview:inDetail];
    NSString *str=@"同意 游侠旅游网会员服务协议\n酒店通常14点开始办理入住,早到酒店可能需要等待,\n请您在30分钟内完成支付,否则订单将自动关闭。\n我们保障快速确认,到店有房。";
    __block NSRange sumRange;
    [inDetail setText:str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        sumRange = [[mutableAttributedString string] rangeOfString:@"游侠旅游网会员服务协议" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#55b2f0"] CGColor] range:sumRange];
        //调整行间距
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:8];
        [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        return mutableAttributedString;
    }];
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
    [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    inDetail.linkAttributes = linkAttributes;
    [inDetail addLinkToURL:[NSURL URLWithString:@""] withRange:sumRange];
    
#pragma mark 购买View
    UIView *secView04=[[UIView alloc]initWithFrame:CGRectMake(0, inDetail.origin.y+inDetail.size.height, SCREENSIZE.width, 14)];
    [secView04 setBackgroundColor:[UIColor colorWithHexString:@"f2f2f2"]];
    [footView addSubview:secView04];
    
    UIView *buyView=[[UIView alloc]initWithFrame:CGRectMake(0, secView04.origin.y+secView04.size.height, SCREENSIZE.width, 48)];
    [buyView setBackgroundColor:[UIColor whiteColor]];
    [footView addSubview:buyView];
    
    buyBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buyBtn.frame=CGRectMake(SCREENSIZE.width-116, 0, 116, buyView.height);;
    buyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [buyBtn setTitle:@"预 订" forState:UIControlStateNormal];
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buyBtn addTarget:self action:@selector(clickBuy) forControlEvents:UIControlEventTouchUpInside];
    buyBtn.titleLabel.font=kFontSize16;
    [buyBtn setBackgroundColor:[UIColor colorWithHexString:@"33AEF0"]];
    [buyView addSubview:buyBtn];
    
    CGRect frame=footView.frame;
    frame.size.height=buyView.origin.y+buyView.height;
    footView.frame=frame;
    
    totalPriceLabel=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(14, 0, buyBtn.origin.x-22, buyView.height)];
    totalPriceLabel.font=kFontSize18;
    NSString *priceStr=[NSString stringWithFormat:@"总价：￥%@",_totalPrice];
    [totalPriceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@",_totalPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
        return mutableAttributedString;
    }];
    [buyView addSubview:totalPriceLabel];
}

-(void)btnsearchclicked:(UIButton *)btn{
    [self selectPeople];
}

#pragma mark 点击常见要求按钮
-(void)clickAskBtn:(id)sender{
    if (askSwitch.on) {
        
        isSeleSakBtn=YES;
        
        [AppUtils closeKeyboard];
        
        specialField.text=@"";
        
        //
        [seleBtn.imgv setImage:[UIImage imageNamed:@"hotelorder_nor"] forState:UIControlStateNormal];
        CusAskBtn *btn=sender;
        specialStr=btn.cusTitleLabel.text;
        [btn.imgv setImage:[UIImage imageNamed:@"hotelorder_sele"] forState:UIControlStateNormal];
        seleBtn=btn;
    }
}

-(void)askSwitchChange:(id)sender{
    specialStr=@"";
    UISwitch *mySwitch=sender;
    for (UIView *view in footView.subviews) {
        if ([view isKindOfClass:[CusAskBtn class]]) {
            [view removeFromSuperview];
        }
    }
    CGFloat btnWidth=120;
    CGFloat btnX=0;
    CGFloat btnY=0;
    for (int i=0; i<specialArr.count; i++) {
        btnX=(i%2)*btnWidth+(i%2)*10+askLabel01.origin.x+askLabel01.width;
        btnY=(i/2)*askLabel01.height+(i/2)*0.5+askLabel01.origin.y;
        CusAskBtn *askBtn= [[CusAskBtn alloc] initWithFrame:CGRectMake(btnX, btnY, btnWidth, 46) title:specialArr[i] image:[UIImage imageNamed:@"hotelorder_nor"]];
        askBtn.cusTitleLabel.textColor=mySwitch.on?[UIColor grayColor]:[UIColor colorWithHexString:@"ababab"];
        [askBtn addTarget:self action:@selector(clickAskBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:askBtn];
    }
    
    if (mySwitch.on) {
        askLabel01.textColor=[UIColor blackColor];
        askLabel02.textColor=[UIColor blackColor];
        specialField.enabled=YES;
    }else{
        isSeleSakBtn=NO;
        askLabel01.textColor=[UIColor colorWithHexString:@"ababab"];
        askLabel02.textColor=[UIColor colorWithHexString:@"ababab"];
        specialField.enabled=NO;
        specialField.text=@"";
    }
}

-(void)speSwitchChange:(id)sender{
    UISwitch *mySwitch=sender;
    if (mySwitch.on) {
        hadInvoice=@"1";
        sjLabel02.textColor=[UIColor blackColor];
        shoujuTF.enabled=YES;
    }else{
        hadInvoice=@"0";
        sjLabel02.textColor=[UIColor colorWithHexString:@"ababab"];
        shoujuTF.enabled=NO;
        shoujuTF.text=@"";
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    isSeleSakBtn=NO;
    specialStr=@"";
    for (UIView *view in footView.subviews) {
        if ([view isKindOfClass:[CusAskBtn class]]) {
            [view removeFromSuperview];
        }
    }
    CGFloat btnWidth=120;
    CGFloat btnX=0;
    CGFloat btnY=0;
    for (int i=0; i<specialArr.count; i++) {
        btnX=(i%2)*btnWidth+(i%2)*10+askLabel01.origin.x+askLabel01.width;
        btnY=(i/2)*askLabel01.height+(i/2)*0.5+askLabel01.origin.y;
        CusAskBtn *askBtn= [[CusAskBtn alloc] initWithFrame:CGRectMake(btnX, btnY, btnWidth, 46) title:specialArr[i] image:[UIImage imageNamed:@"hotelorder_nor"]];
        askBtn.cusTitleLabel.textColor=[UIColor grayColor];
        [askBtn addTarget:self action:@selector(clickAskBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:askBtn];
    }
    return YES;
}

#pragma mark - TTTAttributedLabelDelegate 点击服务协议

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"点击服务协议");
}

#pragma mark - Table Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2+dataArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0||section==1) {
        return 1;
    }else{
        if (dataArr.count!=0) {
            RoomSumModel *model=dataArr[section-2];
            return 1+model.adultSum.count+model.childArr.count;
        }else{
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?14:0.0000001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 72;
    }else if (indexPath.section==1){
        return 56;
    }else{
        return 46;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        RoomnumController *room=[[RoomnumController alloc]init];
        [room setSelectRoomSumBlock:^(NSMutableArray *reDataArr){
            dataArr=reDataArr;
            [myTable reloadData];
            [self hotePriceRequest];
        }];
        room.dataArr=dataArr;
        room.inventcount=_hoteInventoryCount;
        room.max_Occupancy=_max_Occupancy;
        room.max_Child=_max_Child;
        [self.navigationController pushViewController:room animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0){
        HoteLableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteLableCell"];
        if (!cell) {
            cell=[[HoteLableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteLableCell"];
        }
        return cell;
    }else if(indexPath.section==1){
        HotelreservationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelreservationCell"];
        if (!cell) {
            cell=[[HotelreservationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelreservationCell"];
            return cell;
        }
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (dataArr.count!=0) {
            NSInteger adSum=0;
            NSInteger chiSum=0;
            for (RoomSumModel *model in dataArr) {
                adSum=adSum+model.adultSum.count;
                chiSum=chiSum+model.childArr.count;
            }
            cell.rightLabel.text=[NSString stringWithFormat:@"%ld间房    %ld成人    %ld儿童",dataArr.count,adSum,chiSum];
        }else{
            cell.rightLabel.text=@"请选择房间及人数";
        }
        return  cell;
    }else{
        if (indexPath.row==0) {
            RoomSumHeadCell *cell=[tableView dequeueReusableCellWithIdentifier:@"RoomSumHeadCell"];
            if (!cell) {
                cell=[[RoomSumHeadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomSumHeadCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.leftLabel.text=[NSString stringWithFormat:@"房间 %ld",indexPath.section-1];
            return cell;
        }else{
            RoomSumModel *model=dataArr[indexPath.section-2];
            if (model.adultSum.count!=2) {
                if(indexPath.row==1){
                    RoomInformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"RoomInformationCell"];
                    if (!cell) {
                        cell=[[RoomInformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomInformationCell"];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.leftLabel.text=[NSString stringWithFormat:@"成人 %ld",indexPath.row];
                    NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:model.adultSum[indexPath.row-1]];
                    cell.firstName.text=dic[@"firstName"];
                    cell.lastName.text=dic[@"lastName"];
                    cell.RoomInforFirstNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"firstName"];
                        [model.adultSum replaceObjectAtIndex:indexPath.row-1 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    cell.RoomInforLastNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"lastName"];
                        [model.adultSum replaceObjectAtIndex:indexPath.row-1 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    return cell;
                }else{
                    HoteEtongCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteEtongCell"];
                    if (!cell){
                        cell=[[HoteEtongCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteEtongCell"];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.leftLabel.text=[NSString stringWithFormat:@"儿童 %ld",indexPath.row-1];
                    NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:model.childArr[indexPath.row-2]];
                    cell.firstName.text=dic[@"firstName"];
                    cell.lastName.text=dic[@"lastName"];
                    cell.ageLabel.text=[NSString stringWithFormat:@"%@岁",dic[@"age"]];
                    cell.HoteEtongFirstNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"firstName"];
                        [model.childArr replaceObjectAtIndex:indexPath.row-2 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    cell.HoteEtongLastNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"lastName"];
                        [model.childArr replaceObjectAtIndex:indexPath.row-2 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    return cell;
                }
            }else{
                if(indexPath.row==1||indexPath.row==2){
                    RoomInformationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"RoomInformationCell"];
                    if (!cell) {
                        cell=[[RoomInformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RoomInformationCell"];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.leftLabel.text=[NSString stringWithFormat:@"成人 %ld",indexPath.row];
                    NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:model.adultSum[indexPath.row-1]];
                    cell.firstName.text=dic[@"firstName"];
                    cell.lastName.text=dic[@"lastName"];
                    cell.RoomInforFirstNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"firstName"];
                        [model.adultSum replaceObjectAtIndex:indexPath.row-1 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    cell.RoomInforLastNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"lastName"];
                        [model.adultSum replaceObjectAtIndex:indexPath.row-1 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    return cell;
                }else{
                    HoteEtongCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteEtongCell"];
                    if (!cell){
                        cell=[[HoteEtongCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteEtongCell"];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.leftLabel.text=[NSString stringWithFormat:@"儿童 %ld",indexPath.row-2];
                    NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:model.childArr[indexPath.row-3]];
                    cell.firstName.text=dic[@"firstName"];
                    cell.lastName.text=dic[@"lastName"];
                    cell.ageLabel.text=[NSString stringWithFormat:@"%@岁",dic[@"age"]];
                    cell.HoteEtongFirstNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"firstName"];
                        [model.childArr replaceObjectAtIndex:indexPath.row-3 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    cell.HoteEtongLastNameBlock=^(NSString *text){
                        [dic setObject:text forKey:@"lastName"];
                        [model.childArr replaceObjectAtIndex:indexPath.row-3 withObject:dic];
                        [dataArr replaceObjectAtIndex:indexPath.section-2 withObject:model];
                    };
                    return cell;
                }
            }
        }
    }
    return nil;
}

-(void)specialFieldChange:(id)sender{
    UITextField *textField=sender;
    specialStr=textField.text;
    NSLog(@"%@",textField.text);;
}

#pragma mark - 点击预订

-(void)clickBuy{
    for (int k=0; k<dataArr.count; k++) {
        RoomSumModel *model=dataArr[k];
        if (model.adultSum.count!=0) {
            for (int i=0; i<model.adultSum.count; i++) {
                NSDictionary *dic=model.adultSum[i];
                if ([dic[@"firstName"] isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:[NSString stringWithFormat:@"房间%d  成人%d （姓）不能为空",k+1,i+1] inView:self.view];
                    return;
                }
                if ([dic[@"lastName"] isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:[NSString stringWithFormat:@"房间%d  成人%d （名）不能为空",k+1,i+1] inView:self.view];
                    return;
                }
            }
        }
        
        if (model.childArr.count!=0) {
            for (int i=0; i<model.childArr.count; i++) {
                NSDictionary *dic=model.childArr[i];
                if ([dic[@"firstName"] isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:[NSString stringWithFormat:@"房间%d  儿童%d （姓）不能为空",k+1,i+1] inView:self.view];
                    return;
                }
                if ([dic[@"lastName"] isEqualToString:@""]) {
                    [AppUtils showSuccessMessage:[NSString stringWithFormat:@"房间%d  儿童%d （名）不能为空",k+1,i+1] inView:self.view];
                    return;
                }
            }
        }
    }
    
    
    if (contact_firstName.text==nil||contact_firstName.text==NULL||[contact_firstName.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"联系人（姓）不能为空" inView:self.view];
        return;
    }
    
    if (contact_lastName.text==nil||contact_lastName.text==NULL||[contact_lastName.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"联系人（名）不能为空" inView:self.view];
        return;
    }
    
    if ([phoneTF.text isEqualToString:@""]||phoneTF.text==NULL||phoneTF.text==nil) {
        [AppUtils showSuccessMessage:@"手机号不能为空" inView:self.view];
        return;
    }
    if (![AppUtils checkPhoneNumber:phoneTF.text]){
        [AppUtils showSuccessMessage:@"手机号码不正确" inView:self.view];
        return;
    }
    
    
    if (askSwitch.on) {
        if ([specialStr isEqualToString:@""]) {
            [AppUtils showSuccessMessage:@"请选择常见要求或填写特殊要求" inView:self.view];
            return;
        }
    }
    
    //
    if (shoujuSwitch.on) {
        if ([shoujuTF.text isEqualToString:@""]||shoujuTF.text==NULL||shoujuTF.text==nil) {
            [AppUtils showSuccessMessage:@"收据抬头不能为空" inView:self.view];
            return;
        }
    }
    
    [self hotelSubmitRequest];
}

#pragma mark - 酒店价格 Request
-(void)hotePriceRequest{
    [AppUtils showProgressInView:self.view];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"orderPriceCheck" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    //得到 roomDetail 参数
    NSMutableArray *sRoomArr=[NSMutableArray array];
    for (RoomSumModel *model in dataArr) {
        NSMutableDictionary *roomDic=[NSMutableDictionary dictionary];
        [roomDic setObject:[NSString stringWithFormat:@"%ld",model.adultSum.count] forKey:@"adult"];
        [roomDic setObject:[NSString stringWithFormat:@"%ld",model.childArr.count] forKey:@"child"];
        NSString *ageStr=@"";
        for (int i=0; i<model.childArr.count; i++) {
            NSDictionary *ageDic=model.childArr[i];
            ageStr=[ageStr stringByAppendingString:i!=model.childArr.count-1?[NSString stringWithFormat:@"%@:",ageDic[@"age"]]:[NSString stringWithFormat:@"%@",ageDic[@"age"]]];
        }
        [roomDic setObject:ageStr forKey:@"childAge"];
        [sRoomArr addObject:roomDic];
    }
    
    NSDictionary *dic=@{
                        @"hotelID":_hoteid,
                        @"RatePlanID":_rateplanId,
                        @"checkin":_starDate,
                        @"checkout":_endDate,
                        @"rooms":[NSString stringWithFormat:@"%ld",dataArr.count],
                        @"roomDetail":sRoomArr
                        };
    NSData *data=[AppUtils toJSONData:dic];
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [dict setObject:jsonString forKey:@"option"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"酒店价格确认=================%@",responseObject);
        NSString *str1=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str1 isEqualToString:@"1"]){
            NSString *priceStr=[NSString stringWithFormat:@"总价：￥%@",responseObject[@"retData"][@"totalPrice"]];
            [totalPriceLabel setText:priceStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@",responseObject[@"retData"][@"totalPrice"]] options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
                return mutableAttributedString;
            }];
        }else{
            [AppUtils showSuccessMessage:@"当前房型已售尽,请重新选择" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

//客户要求
-(void)specialRequest{
    [AppUtils showProgressInView:self.view];
    specialArr=[NSMutableArray array];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"hotelCustomerRequest" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:_hoteid forKey:@"hotelID"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"======%@====",responseObject);
        NSString *str1=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str1 isEqualToString:@"1"]){
            id obj=responseObject[@"retData"];
            if ([obj isKindOfClass:[NSArray class]]) {
                for (NSString *str in obj) {
                    [specialArr addObject:str];
                }
                [self setTupTableView];
            }
        }else{
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

#pragma mark 提交酒店预订订单 Request
-(void)hotelSubmitRequest{
    [AppUtils showProgressMessage:@"确认订单中……" inView:self.view];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"buildOrder" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    
    //房间及人数参数
    NSMutableArray *roomDetailArr=[NSMutableArray array];
    for (RoomSumModel *model in dataArr) {
        NSMutableDictionary *roomDic=[NSMutableDictionary dictionary];
        [roomDic setObject:[NSString stringWithFormat:@"%ld",model.adultSum.count] forKey:@"adult"];
        [roomDic setObject:[NSString stringWithFormat:@"%ld",model.childArr.count] forKey:@"child"];
        NSString *ageStr=@"";
        for (int i=0; i<model.childArr.count; i++) {
            NSDictionary *ageDic=model.childArr[i];
            ageStr=[ageStr stringByAppendingString:i!=model.childArr.count-1?[NSString stringWithFormat:@"%@:",ageDic[@"age"]]:[NSString stringWithFormat:@"%@",ageDic[@"age"]]];
        }
        [roomDic setObject:ageStr forKey:@"childAge"];
        [roomDetailArr addObject:roomDic];
    }
    
    //房间成人跟儿童姓名、年龄 参数
    NSMutableArray *guestDic=[NSMutableArray array];
    for (int i=0; i<dataArr.count; i++) {
        NSMutableDictionary *ddic=[NSMutableDictionary dictionary];
        NSString *keyStr=[NSString stringWithFormat:@"room%d",i+1];
        RoomSumModel *model=dataArr[i];
        NSMutableArray *pamArr=[NSMutableArray array];
        for (NSDictionary *dic in model.adultSum) {
            NSMutableDictionary *nDic=[NSMutableDictionary dictionaryWithDictionary:dic];
            NSString *ageStr=dic[@"age"];
            [nDic setObject:[ageStr isEqualToString:@""]?@"1":@"0" forKey:@"isAdult"];
            [nDic removeObjectForKey:@"age"];
            [pamArr addObject:nDic];
        }
        for (NSDictionary *dic in model.childArr) {
            NSMutableDictionary *nDic=[NSMutableDictionary dictionaryWithDictionary:dic];
            NSString *ageStr=dic[@"age"];
            [nDic setObject:[ageStr isEqualToString:@""]?@"1":@"0" forKey:@"isAdult"];
            [nDic removeObjectForKey:@"age"];
            [pamArr addObject:nDic];
        }
//        [ddic setObject:pamArr forKey:keyStr];
        [guestDic addObject:pamArr];
    }
    
    NSDictionary *contactDic=@{
                               @"name":[NSString stringWithFormat:@"%@:%@",contact_firstName.text,contact_lastName.text],
                               @"phone":phoneTF.text,
                               @"email":eMailTF.text};
    
    NSInteger adSum=0;
    NSInteger chiSum=0;
    for (RoomSumModel *model in dataArr) {
        adSum=adSum+model.adultSum.count;
        chiSum=chiSum+model.childArr.count;
    }
    
    NSDictionary *dic=@{
                        @"userid":[AppUtils getValueWithKey:User_ID],
                        @"option":@{@"hotelID":_hoteid,
                                    @"RatePlanID":_rateplanId,
                                    @"RatePlanName":_rateplanName,
                                    @"checkin":_starDate,
                                    @"checkout":_endDate,
                                    @"rooms":[NSString stringWithFormat:@"%ld",dataArr.count],
                                    @"roomDetail": roomDetailArr
                                    },
                        @"adultCount":[NSString stringWithFormat:@"%ld",adSum],
                        @"childCount":[NSString stringWithFormat:@"%ld",chiSum],
                        @"nights":_numDay,
                        @"guest": guestDic,
                        @"contact":contactDic,
                        @"CustomerRequest":specialStr,
                        @"Invoice":hadInvoice,
                        @"InvoiceHeader":shoujuTF.text
                        };
    NSData *data=[AppUtils toJSONData:dic];
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [dict setObject:jsonString forKey:@"info"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"酒店预订订单=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            NSString *orderId=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"orderid"]];
            [self.navigationController pushViewController:[PSCHotelOrderDetail initWithOrderId:orderId] animated:YES];
        }
        else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}

-(void)selectPeople{
    int __block tip=0;
    
    ABAddressBookRef addBook =nil;
    
    addBook=ABAddressBookCreateWithOptions(NULL,NULL);
    
    dispatch_semaphore_t sema=dispatch_semaphore_create(0);
    
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool greanted,CFErrorRef error)        {
        if (!greanted) {
            tip=1;
        }
        dispatch_semaphore_signal(sema);
    });
         dispatch_semaphore_wait(sema,DISPATCH_TIME_FOREVER);
    if (tip) {
        NSString *tipStr=[NSString stringWithFormat:@"您没有访问联系人权限，您可以在\n设置-隐私-通讯录-游侠网\n中启用访问权限"];
        UIAlertView * alart = [[UIAlertView alloc]initWithTitle:@"提示"message:tipStr delegate:self cancelButtonTitle:@"知道了"otherButtonTitles:nil,nil];
        [alart show];
    }else{
        ABPeoplePickerNavigationController *peoplePicker= [[ABPeoplePickerNavigationController alloc] init];
        
        [peoplePicker.navigationBar setBarStyle:UIBarStyleBlack];
        
        [peoplePicker.view setBackgroundColor:[UIColor whiteColor]];
        
        peoplePicker.peoplePickerDelegate =self;
        
        NSArray *displayItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty],nil];
        
        peoplePicker.displayedProperties=displayItems;
        
        if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0){
            peoplePicker.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
        }
        [self presentViewController:peoplePicker animated:YES completion:nil];
    }
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef phone= ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    
    //获取联系人电话
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSRange prang=[phoneNO rangeOfString:@"+86 "];
    if (prang.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang.location+prang.length];
    }
    
    NSRange prang02=[phoneNO rangeOfString:@"00 86 "];
    if (prang02.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang02.location+prang02.length];
    }
    
    NSRange prang03=[phoneNO rangeOfString:@"0086"];
    if (prang03.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang03.location+prang03.length];
    }
    
    NSRange prang04=[phoneNO rangeOfString:@"86 "];
    if (prang04.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:prang04.location+prang04.length];
    }
    
    NSRange spaceRang=[phoneNO rangeOfString:@" "];
    if (spaceRang.location!=NSNotFound) {
        phoneNO=[phoneNO substringFromIndex:spaceRang.location+spaceRang.length];
    }
    
    phoneTF.text=phoneNO;
    
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0){
    ABPersonViewController *personViewController = [[ABPersonViewController alloc] init];
    personViewController.displayedPerson = person;
    [peoplePicker pushViewController:personViewController animated:YES];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person NS_DEPRECATED_IOS(2_0, 8_0){
    return YES;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_DEPRECATED_IOS(2_0, 8_0){
    
    //获取联系人姓名
    NSString *name = (__bridge NSString*)ABRecordCopyCompositeName(person);
    NSLog(@"%@",name);
    
    //获取联系人电话
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    return YES;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
