//
//  HotelReserveController.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelReserveController : BaseController

@property(nonatomic,copy) NSString *hoteInventoryCount;  //库存
@property(nonatomic,copy) NSString *max_Occupancy;       //最大入住人数
@property(nonatomic,copy) NSString *max_Child;           //最大入住儿童数
@property(nonatomic,copy) NSString *hoteid;
@property(nonatomic,copy) NSString *hoteName;
@property(nonatomic,copy) NSString *starDate;
@property(nonatomic,copy) NSString *endDate;
@property(nonatomic,copy) NSString *numDay;
@property(nonatomic,copy) NSString *rateplanId;
@property(nonatomic,copy) NSString *rateplanName;
@property(nonatomic,copy) NSString *totalPrice;

@end
