//
//  ShowServiceTypeView.h
//  youxia
//
//  Created by mac on 16/1/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowServiceTypeView : UIView

-(id)initWithPickerViewInView:(UIView *)view;

-(void)showPickerView;

@end
