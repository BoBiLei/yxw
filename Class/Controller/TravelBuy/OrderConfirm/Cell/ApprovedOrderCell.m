//
//  ApprovedOrderCell.m
//  youxia
//
//  Created by mac on 16/1/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ApprovedOrderCell.h"

#define TextFont 15
#define LeftLabelWidth 70
#define LeftLabelHeight 21
@implementation ApprovedOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor groupTableViewBackgroundColor];
}

-(void)reflushDataForModel:(OrderListModel *)model islandType:(IslandType)type rateArr:(NSMutableArray *)rateArr{
    DSLog(@"%ld",(unsigned long)type);
    UIView *orderInfoView=[[UIView alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, 145)];
    orderInfoView.layer.borderWidth=0.5f;
    orderInfoView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    orderInfoView.backgroundColor=[UIColor whiteColor];
    [self addSubview:orderInfoView];
    
    CGRect frame=orderInfoView.frame;
    CGFloat cellHeight=0.0f;
    
    //
    UILabel *ordLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 12, LeftLabelWidth, LeftLabelHeight)];
    ordLabel.font=[UIFont systemFontOfSize:TextFont];
    ordLabel.text=@"订单编号:";
    [orderInfoView addSubview:ordLabel];
    
    UILabel *ordNum=[[UILabel alloc]initWithFrame:CGRectMake(ordLabel.origin.x+ordLabel.width+10, ordLabel.origin.y, SCREENSIZE.width-(ordLabel.origin.x+ordLabel.width+16), ordLabel.height)];
    ordNum.font=[UIFont systemFontOfSize:TextFont];
    ordNum.text=model.orderId;
    ordNum.numberOfLines=0;
    [orderInfoView addSubview:ordNum];
    
#pragma mark 产品名称
    UILabel *label01=[[UILabel alloc]initWithFrame:CGRectMake(8, ordLabel.origin.y+ordLabel.height+10, LeftLabelWidth, LeftLabelHeight)];
    label01.font=[UIFont systemFontOfSize:TextFont];
    label01.text=@"产品名称:";
    [orderInfoView addSubview:label01];
    
    NSString *proName=model.islandTitle;
    CGSize size=[AppUtils getStringSize:proName withFont:TextFont];
    CGFloat pnWidth=orderInfoView.width-(label01.origin.x+label01.width+16);
    CGFloat pnHeight=0;
    if (size.width>pnWidth) {
        pnHeight=40;
    }else{
        pnHeight=21;
    }
    
    UILabel *productName=[[UILabel alloc]initWithFrame:CGRectMake(label01.origin.x+label01.width+10, label01.origin.y, pnWidth, pnHeight)];
    productName.font=[UIFont systemFontOfSize:TextFont];
    productName.text=proName;
    productName.numberOfLines=0;
    [orderInfoView addSubview:productName];
    
#pragma mark 酒店类型
    UILabel *label02=[[UILabel alloc]initWithFrame:CGRectMake(8, productName.origin.y+productName.height+8, LeftLabelWidth, LeftLabelHeight)];
    label02.font=[UIFont systemFontOfSize:TextFont];
    NSString *lab02Str=@"酒店类型:";
    label02.text=lab02Str;
    [orderInfoView addSubview:label02];
    
    NSString *roomTypeStr=model.roomType;
    CGSize roomSize=[AppUtils getStringSize:roomTypeStr withFont:TextFont];
    CGFloat roomHeight=0;
    if (roomSize.width<pnWidth) {
        roomHeight=21;
    }else{
        roomHeight=42;
    }
    UILabel *roomType=[[UILabel alloc]initWithFrame:CGRectMake(label02.origin.x+label02.width+10, label02.origin.y, pnWidth, roomHeight)];
    roomType.font=[UIFont systemFontOfSize:TextFont];
    NSString *labelTtt=roomTypeStr;
    roomType.numberOfLines=0;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    roomType.attributedText = attributedString;
    [orderInfoView addSubview:roomType];
    
    ////////////////////////////////////////////////
#pragma mark 机票类型
    UILabel *label03=[[UILabel alloc]initWithFrame:CGRectMake(8, roomType.origin.y+roomType.height+8, LeftLabelWidth, LeftLabelHeight)];
    label03.font=[UIFont systemFontOfSize:TextFont];
    label03.text=@"机票类型:";
    [orderInfoView addSubview:label03];
    
    UILabel *flightLabel=[[UILabel alloc]initWithFrame:CGRectMake(label03.origin.x+label03.width+10, label03.origin.y, pnWidth, 21)];
    flightLabel.font=[UIFont systemFontOfSize:TextFont];
    flightLabel.text=model.flightInfo;
    [orderInfoView addSubview:flightLabel];
    
#pragma mark 总计金额
    CGRect label04Frame;
    UILabel *label04=[UILabel new];
    label04.font=[UIFont systemFontOfSize:TextFont];
    label04.text=@"总计金额:";
    [orderInfoView addSubview:label04];
    
    label04Frame=CGRectMake(8, flightLabel.origin.y+flightLabel.height+8, LeftLabelWidth, LeftLabelHeight);
    label04.frame=label04Frame;
    //
    CGFloat ttpHeight=0;
    if ([model.stageSum isEqualToString:@"1"]) {
        ttpHeight=21;
    }else{
        ttpHeight=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?42:84;
    }
    UILabel *totalPrice=[[UILabel alloc]initWithFrame:CGRectMake(label04.origin.x+label04.width+10, label04.origin.y, pnWidth, ttpHeight)];
    totalPrice.font=[UIFont systemFontOfSize:TextFont];
    totalPrice.numberOfLines=0;
    
    NSString *stageFee;
    
    NSString *rateStr;
    if ([model.stageSum isEqualToString:@"1"]) {
        rateStr=rateArr[0];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"3"]){
        rateStr=rateArr[1];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"6"]){
        rateStr=rateArr[2];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"9"]){
        rateStr=rateArr[3];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"12"]){
        rateStr=rateArr[4];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }
    
    CGFloat ttPrice=model.totalShowPrice.floatValue;
    CGFloat coupon=model.couponPrice.floatValue;
    
    NSString *ttpStr=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?[NSString stringWithFormat:@"%.f元 （产品总额%.f元，分%@期支付,每期应付 %@元）",[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,model.stageSum,model.price_everyStage]:[NSString stringWithFormat:@"%.f元 （分%@期支付，%@期手续费率 %@ ,手续费总计 %@元 ,每期应付 %@元 ，其中首期需额外支付全部手续费 %@元 ）",[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,model.stageSum,model.stageSum,stageFee,model.stagePrice,model.price_everyStage,model.stagePrice];
    NSMutableAttributedString *ttpAtString = [[NSMutableAttributedString alloc] initWithString:ttpStr];
    NSMutableParagraphStyle *ttpStyle = [[NSMutableParagraphStyle alloc] init];
    [ttpStyle setLineSpacing:2];//调整行间距
    [ttpAtString addAttribute:NSParagraphStyleAttributeName value:ttpStyle range:NSMakeRange(0, [ttpStr length])];
    if ([model.stageSum isEqualToString:@"1"]) {
        totalPrice.text=[NSString stringWithFormat:@"%@元",model.totalShowPrice];
    }else{
        totalPrice.attributedText = ttpAtString;
    }
    [orderInfoView addSubview:totalPrice];
    
#pragma mark 应付金额
    UILabel *label05=[[UILabel alloc]initWithFrame:CGRectMake(8, totalPrice.origin.y+totalPrice.height+8, LeftLabelWidth, LeftLabelHeight)];
    label05.font=[UIFont systemFontOfSize:TextFont];
    label05.text=@"应付金额:";
    [orderInfoView addSubview:label05];
    
    UILabel *payPrice=[[UILabel alloc]initWithFrame:CGRectMake(label05.origin.x+label05.width+10, label05.origin.y, pnWidth, 21)];
    payPrice.font=[UIFont systemFontOfSize:TextFont];
    NSString *payStr;
    if ([model.stageSum isEqualToString:@"1"]) {
        payStr=[NSString stringWithFormat:@"%@元",model.payMoney];
    }else{
        payStr=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?[NSString stringWithFormat:@"%@元",model.payMoney]:[NSString stringWithFormat:@"%@元（含全部手续费）",model.payMoney];
    }
    payPrice.text=payStr;
    [orderInfoView addSubview:payPrice];
    
    cellHeight=payPrice.origin.y+payPrice.height+12;
    
    frame.size.height=cellHeight;
    orderInfoView.frame=frame;
    self.frame=frame;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
