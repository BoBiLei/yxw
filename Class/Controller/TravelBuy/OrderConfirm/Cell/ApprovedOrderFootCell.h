//
//  ApprovedOrderFootCell.h
//  youxia
//
//  Created by mac on 16/1/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderNotConfirmDelegate <NSObject>

-(void)clickNotConfirmBtn;

@end

@protocol OrderHadConfirmDelegate <NSObject>

-(void)clickHadConfirmBtn;;

@end

@protocol KFOnlineDelegate <NSObject>

-(void)clickKeFu;

@end

@protocol PhoneOnlineDelegate <NSObject>

-(void)clickPhone;

@end

@interface ApprovedOrderFootCell : UITableViewCell

@property (weak, nonatomic) id<OrderNotConfirmDelegate> notConfirmDelegate;
@property (weak, nonatomic) id<OrderHadConfirmDelegate> hadConfirmDelegate;
@property (weak, nonatomic) id<KFOnlineDelegate> kfDelegate;
@property (weak, nonatomic) id<PhoneOnlineDelegate> phoneDelegate;

@end
