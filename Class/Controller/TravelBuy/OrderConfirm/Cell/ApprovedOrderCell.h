//
//  ApprovedOrderCell.h
//  youxia
//
//  Created by mac on 16/1/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListModel.h"

@interface ApprovedOrderCell : UITableViewCell

-(void)reflushDataForModel:(OrderListModel *)model islandType:(IslandType)type rateArr:(NSMutableArray *)rateArr;

@end
