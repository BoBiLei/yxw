//
//  ApprovedOrderFootCell.m
//  youxia
//
//  Created by mac on 16/1/7.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ApprovedOrderFootCell.h"

@implementation ApprovedOrderFootCell{
    UIView *orderInfoView;
    UIButton *rightBtn;
    BOOL isClickNot;
    
    BOOL isShow;
    CGFloat newHeight;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    isClickNot=NO;
    isShow=NO;
    self.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    orderInfoView=[[UIView alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width-16, 280)];
    orderInfoView.layer.borderWidth=0.5f;
    orderInfoView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    orderInfoView.backgroundColor=[UIColor whiteColor];
    [self addSubview:orderInfoView];
    
    //
    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(8, 12, 40, 40)];
    img.image=[UIImage imageNamed:@"examine_order2.jpg"];
    img.contentMode=UIViewContentModeScaleAspectFit;
    [orderInfoView addSubview:img];
    
    UILabel *tip=[[UILabel alloc]initWithFrame:CGRectMake(img.origin.x+img.width+8, img.origin.y, orderInfoView.width-(img.origin.x+img.width+16), img.height)];
    CGFloat tipFont=0.0f;
    if (IS_IPHONE5) {
        tipFont=13;
    }else{
        tipFont=15;
    }
    tip.font=[UIFont systemFontOfSize:tipFont];
    //
    NSString *labelTtt=[NSString stringWithFormat:@"旅游产品均为即时计价，请以我方客服最终确认价格为准，请问是否和客服核实？"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    tip.attributedText = attributedString;
    tip.numberOfLines=0;
    tip.adjustsFontSizeToFitWidth=YES;
    [orderInfoView addSubview:tip];
    
    //
    UIButton *leftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.layer.cornerRadius=2;
    leftBtn.frame=CGRectMake(tip.origin.x, tip.origin.y+tip.height+16, (SCREENSIZE.width-24)/3, 36);
    leftBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
    [leftBtn setTitle:@"还没和客服确定" forState:UIControlStateNormal];
    leftBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    leftBtn.titleLabel.font=kFontSize15;
    [leftBtn addTarget:self action:@selector(clickLeftBtn) forControlEvents:UIControlEventTouchDown];
    [orderInfoView addSubview:leftBtn];
    
    //
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.layer.cornerRadius=2;
    rightBtn.frame=CGRectMake(SCREENSIZE.width-(leftBtn.origin.x+leftBtn.width), leftBtn.origin.y, (SCREENSIZE.width-24)/3, 36);
    rightBtn.backgroundColor=[UIColor colorWithHexString:@"#00A651"];
    [rightBtn setTitle:@"已经和客服确定" forState:UIControlStateNormal];
    rightBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font=kFontSize15;
    [rightBtn addTarget:self action:@selector(clickRightBtn) forControlEvents:UIControlEventTouchDown];
    [orderInfoView addSubview:rightBtn];
    
    //
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, rightBtn.origin.y+rightBtn.height+12, orderInfoView.width, 0.5f)];
    line.backgroundColor=[UIColor lightGrayColor];
    [orderInfoView addSubview:line];
    
    UILabel *lxTip=[[UILabel alloc]initWithFrame:CGRectMake(8, line.origin.y+8, orderInfoView.width-16, 21)];
    lxTip.font=kFontSize15;
    lxTip.text=@"请选择联系方式与客服核实订单";
    [orderInfoView addSubview:lxTip];
    
    CGFloat kfViewX=orderInfoView.width/6;
    
    UIButton *kfView01=[[UIButton alloc]initWithFrame:CGRectMake(kfViewX, lxTip.origin.y+lxTip.height+8, (orderInfoView.width-kfViewX*3)/2, 110)];
    [kfView01 setImage:[UIImage imageNamed:@"examine_order_button2_kf"] forState:UIControlStateNormal];
    [kfView01 addTarget:self action:@selector(clickKFOnline) forControlEvents:UIControlEventTouchUpInside];
    [orderInfoView addSubview:kfView01];
    
    UIButton *kfView02=[[UIButton alloc]initWithFrame:CGRectMake(kfView01.origin.x+kfView01.width+kfViewX, kfView01.origin.y, kfView01.width, kfView01.height)];
    [kfView02 setImage:[UIImage imageNamed:@"examine_order_button2_dh"] forState:UIControlStateNormal];
    [kfView02 addTarget:self action:@selector(clickPhoneOnline) forControlEvents:UIControlEventTouchUpInside];
    [orderInfoView addSubview:kfView02];
}

-(void)clickKFOnline{
    [self.kfDelegate clickKeFu];
}

-(void)clickPhoneOnline{
    [self.phoneDelegate clickPhone];
}

-(void)clickLeftBtn{
    isClickNot=YES;
    [self setNeedsLayout];
    [self.notConfirmDelegate clickNotConfirmBtn];
}

-(void)clickRightBtn{
    [self.hadConfirmDelegate clickHadConfirmBtn];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
