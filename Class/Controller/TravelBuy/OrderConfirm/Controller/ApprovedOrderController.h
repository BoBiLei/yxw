//
//  ApprovedOrderController.h
//  youxia
//
//  Created by mac on 15/12/5.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApprovedOrderController : UIViewController

@property (nonatomic) IslandType islandType;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,retain) NSMutableArray *rateArr;   //费率
@end
