//
//  ApprovedOrderController.m
//  youxia
//
//  Created by mac on 15/12/5.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ApprovedOrderController.h"
#import "ApprovedOrderCell.h"
#import "ApprovedOrderFootCell.h"
#import "ShowServiceTypeView.h"
#import "ServierOnlineController.h"
#import "TestController.h"
#import "NewTravelPlanController.h"
#import "OrderListController.h"

@interface ApprovedOrderController ()<UITableViewDataSource,UITableViewDelegate,OrderNotConfirmDelegate,OrderHadConfirmDelegate,UIGestureRecognizerDelegate,KFOnlineDelegate,PhoneOnlineDelegate>

@end

@implementation ApprovedOrderController{
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    
    //
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    BOOL isSeleNotBtn;
    
    NSString *cardType;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"确认订单页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"确认订单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    [self RequestOrderPayWithOrderId:_orderId];
    
    [self setUpUI];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"确认订单";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}


-(void)clickReturnBtn{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[PersonalCenterController class]]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if ([controller isKindOfClass:[NewTravelPlanController class]]){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - init ui
-(void)setUpUI{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"ApprovedOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [myTable registerNib:[UINib nibWithNibName:@"ApprovedOrderFootCell" bundle:nil] forCellReuseIdentifier:@"footcell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.hidden=YES;
    myTable.showsVerticalScrollIndicator=NO;
    [self.view addSubview:myTable];
    
    //headView
    UIView *headView=[[UIView alloc]init];
    headView.backgroundColor=[UIColor whiteColor];
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, ((SCREENSIZE.width-16)*56)/660)];
    imgv.image=[UIImage imageNamed:@"order_title_2"];
    headView.frame=CGRectMake(0, 0, SCREENSIZE.width, imgv.height+16);
    [headView addSubview:imgv];
    myTable.tableHeaderView=headView;
}

#pragma mark - button event

-(void)clickGoOrderList{
    OrderListController *orderList=[[OrderListController alloc]init];
    [self.navigationController pushViewController:orderList animated:YES];
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return dataArr.count;
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ApprovedOrderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[ApprovedOrderCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderListModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model islandType:_islandType rateArr:_rateArr];
        return cell.frame.size.height;
    }else{
        return 280;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ApprovedOrderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell=[[ApprovedOrderCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        OrderListModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model islandType:_islandType rateArr:_rateArr];
        return cell;
    }else{
        ApprovedOrderFootCell *cell=[tableView dequeueReusableCellWithIdentifier:@"footcell"];
        if (!cell) {
            cell=[[ApprovedOrderFootCell alloc]init];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.notConfirmDelegate=self;
        cell.hadConfirmDelegate=self;
        cell.kfDelegate=self;
        cell.phoneDelegate=self;
        return cell;
    }
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 44)];
    headerSectionView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    CGRect frame=headerSectionView.frame;
    frame.size.height=79;
    
    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(8, 11, headerSectionView.frame.size.height-22, headerSectionView.frame.size.height-22)];
    img.contentMode=UIViewContentModeScaleAspectFit;
    img.image=[UIImage imageNamed:@"icon_wddd"];
    [headerSectionView addSubview:img];
    //订单编号
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(img.origin.x+img.width+8, 0, 100, headerSectionView.height)];
    titleLabel.font=kFontSize15;
    [headerSectionView addSubview:titleLabel];
    
    NSString *str;
    if (section==0) {
        str=@"订单详情";
    }else{
        str=@"客服确定";
    }
    titleLabel.text=str;
    return headerSectionView;
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==1) {
        return 30;
    }else{
        return 0.1f;
    }
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Button Delegate
-(void)clickNotConfirmBtn{
    isSeleNotBtn=YES;
    //一个section刷新
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
    [myTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - 点击已经和客服确定
-(void)clickHadConfirmBtn{
    TestController *payCtr=[[TestController alloc]init];
    payCtr.orderId=_orderId;
    payCtr.rateArr=_rateArr;
    [self.navigationController pushViewController:payCtr animated:YES];
}

-(void)clickKeFu{
    ServierOnlineController *servon=[[ServierOnlineController alloc]init];
    [self.navigationController pushViewController:servon animated:YES];
}

-(void)clickPhone{
    //PhoneView
    UIWebView *phoneView;
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - reques 请求
-(void)RequestOrderPayWithOrderId:(NSString *)orderId{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"confirm_order_iso" forKey:@"action"];
    [dict setObject:orderId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    //DSLog(@"%@",dict);
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self RequestOrderPayWithOrderId:orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSDictionary *dic=responseObject[@"retData"];
                OrderListModel *model=[[OrderListModel alloc]init];
                [model jsonApproveDataForDictionary:dic];
                [dataArr addObject:model];
                myTable.hidden=NO;
                
            }
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

@end
