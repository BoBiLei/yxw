//
//  SelectDateView.h
//  youxia
//
//  Created by mac on 16/1/19.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectDateView : UIView

-(id)initWithPickerViewInView:(UIView *)view whitYear:(NSString *)year month:(NSString *)month;

-(void)showPickerViewCompletion:(void (^)(id))completion;

@end
