//
//  SelectDateView.m
//  youxia
//
//  Created by mac on 16/1/19.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SelectDateView.h"

@interface SelectDateView ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIPickerView *myPickerView;
@property (copy) void (^onDismissCompletion)(NSString *);
@property (copy) NSString *(^objectToStringConverter)(id object);

@end

@implementation SelectDateView{
    NSMutableArray *yearArray;
    NSMutableArray *monthArray;
}

-(void)initDataSource{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // Get Current Year
    [formatter setDateFormat:@"yyyy"];
    NSString *currentYearString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    
    // PickerView -  Years data
    yearArray = [[NSMutableArray alloc] init];
    for (int i = currentYearString.intValue; i <= 2050 ; i++){
        [yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    // PickerView -  Months data
    monthArray = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 12 ; i++){
        [monthArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

#pragma mark - Init PickerView
-(id)initWithPickerViewInView:(UIView *)view whitYear:(NSString *)year month:(NSString *)month{
    self = [super init];
    if (self) {
        
        [self initDataSource];
        
        [self setFrame: view.bounds];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //半透明黑色背景
        _backgroundView = [[UIView alloc] initWithFrame:view.bounds];
        [_backgroundView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
        [_backgroundView setAlpha:0.0];
        [_backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
        [self addSubview:_backgroundView];
        
        //PickerView Container with top bar
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _backgroundView.height - 236.0, self.width, 236.0)];
        _containerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_containerView];
        
        //ToolbarBackgroundColor - Black
        UIColor *toolbarBackgroundColor = [[UIColor alloc] initWithCGColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8].CGColor];
        
        //Top bar view
        UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _containerView.width, 44.0)];
        [_containerView addSubview:topBarView];
        [topBarView setBackgroundColor:[UIColor whiteColor]];
        
        
        UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:topBarView.frame];
        [_containerView addSubview:pickerToolBar];
        
        CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        if (iOSVersion < 7.0) {
            pickerToolBar.tintColor = toolbarBackgroundColor;
        }else{
            [pickerToolBar setBackgroundColor:toolbarBackgroundColor];
            
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
            pickerToolBar.barTintColor = toolbarBackgroundColor;
#endif
        }
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.width-60, 4, 40, 40)];
        [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
        [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
        [closeBtn setTitleColor:[UIColor colorWithHexString:YxColor_Blue] forState:UIControlStateNormal];
        closeBtn.titleLabel.font=kFontSize16;
        [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
        pickerToolBar.items = @[flexibleSpace, barButtonItem];
        
        //Add pickerView
        _myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, topBarView.height, self.width, _containerView.height-topBarView.height)];
        _myPickerView.dataSource=self;
        _myPickerView.delegate=self;
        
        for (int i=0; i<yearArray.count; i++) {
            if ([year isEqualToString:yearArray[i]]) {
                [_myPickerView selectRow:i inComponent:0 animated:YES];
            }
        }
        for (int i=0; i<monthArray.count; i++) {
            if ([month isEqualToString:monthArray[i]]) {
                [_myPickerView selectRow:i inComponent:1 animated:YES];
            }
        }
        [_containerView addSubview:_myPickerView];
        
        [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
        [view addSubview:self];
    }
    return self;
}

#pragma mark - Show Methods
-(void)showPickerViewCompletion:(void (^)(id))completion{
    [self setPickerHidden:NO callBack:nil];
    self.onDismissCompletion = completion;
}

#pragma mark - Dismiss Methods
-(void)dismissWithCompletion:(void (^)(NSString *))completion{
    [self setPickerHidden:YES callBack:completion];
}

-(void)dismiss{
    [self dismissWithCompletion:self.onDismissCompletion];
}

-(void)removePickerView{
    [self removeFromSuperview];
}

#pragma mark - Show/hide PickerView methods
-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(id))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_backgroundView setAlpha:0.0];
                             [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
                         } else {
                             [_backgroundView setAlpha:1.0];
                             [_containerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [self removePickerView];
                             callBack([self selectedObject]);
                         }
                     }];
}

#pragma mark - 点击透明背景hidden
- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_backgroundView setAlpha:1.0];
                         [_containerView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component {
    if (component == 0) {
        return yearArray.count;
    }else{
        return monthArray.count;
    }
}

- (NSString *)pickerView: (UIPickerView *)pickerView
             titleForRow: (NSInteger)row
            forComponent: (NSInteger)component {
    NSString *componentStr;
    if (component == 0) {
        componentStr=[NSString stringWithFormat:@"%@年",yearArray[row]];
    }
    else{
        componentStr=[NSString stringWithFormat:@"%@月",monthArray[row]];
    }
    return componentStr;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        pickerLabel.textAlignment=NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:kFontSize18];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (id)selectedObject {
    NSArray *arr;
    NSString *proStr=[yearArray objectAtIndex:[_myPickerView selectedRowInComponent:0]];
    NSString *cityStr=[monthArray objectAtIndex:[_myPickerView selectedRowInComponent:1]];
    arr=@[proStr,cityStr==NULL?@"":cityStr];
    return arr;
}

@end
