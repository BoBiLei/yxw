//
//  PayTypeSectionModel.h
//  youxia
//
//  Created by mac on 16/1/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayTypeSectionModel : NSObject

@property (nonatomic,copy) NSString *groupId;
@property (nonatomic,copy) NSString *groupName;
@property (nonatomic) NSInteger modelTag;
@property int groupNum;
@property (nonatomic, assign) BOOL isHide;
@property (nonatomic,strong) NSMutableArray *groupCell;

@end
