//
//  TestController.h
//  youxia
//
//  Created by mac on 16/1/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestController : UIViewController

@property (nonatomic) IslandType islandType;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *selectedXYKId;
@property (nonatomic,retain) NSMutableArray *rateArr;   //费率

@end
