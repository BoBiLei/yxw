//
//  TestController.m
//  youxia
//
//  Created by mac on 16/1/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "TestController.h"
#import "PayTypeCell.h"
#import "MMGAliPay.h"
#import "WXApi.h"
#import "PaySuccessController.h"
#import "BankTransferCell.h"
#import "NewTravelPlanController.h"
#import "OrderListController.h"
#import "OrderListModel.h"
#import "FilmWeiXinPayModel.h"

#define TextFont 16
#define LeftLabelWidth 70
#define LeftLabelHeight 21
#define kCell_Height 44
#define seleImg_nor @"allBtn_nor"
#define seleImg_sele @"allBtn_sele"

#define sectionTitle01 @"收款账户信息（注:暂不支持建设银行卡转账）";
#define sectionTitle02 @"账户名：深圳市游侠网国际旅行社有限公司";
#define sectionTitle03 @"开户银行：工商银行深圳分行宝民支行";
#define sectionTitle04 @"银行账号：40000 9250 9100 1208 78";

@interface TestController ()<UITableViewDataSource,UITableViewDelegate,PayFinishDelegate>

@end

@implementation TestController{
    NSMutableArray      *dataSource;
    NSMutableArray      *sectionArray;
    NSMutableArray      *stateArray;
    UITableView *myTable;
    UIView *headerSectionView;
    //
    NSMutableArray *payMsgDicArr;
    NSMutableArray *payTypeArr;
    CGRect tableFrame;
    NSIndexPath *selectedIndexPath;
    UIView *payView;    //立即购买的view
    UIButton *payBtn;
    
    OrderListModel *payModel;
    
    NSMutableArray *bankDataArr;//所有银行列表
    NSMutableArray *userXYKDataArr;//我的信用卡列表
    UIButton *seleSectionHeadBtn;
    
    PayTypeSectionModel *seleModel;
    NSMutableArray *dataArray;
    
    NSString *selePayTypeId;
    PayTypeSectionModel *headClickmodel;
    
    NSString *seleGroupId;
    NSString * yuEMony;
    UILabel* totalLabel;
}

-(void)clickReturnBtn{
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示！"message:@"是否离开付款页面"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction*okAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[OrderListController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
            else if ([controller isKindOfClass:[NewTravelPlanController class]]){
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }];
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"订单支付页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"订单支付页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //支付监听从后台进入前台的通知（重要哦）
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxSender:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self dataRequestOrderId:_orderId];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"订单支付";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}


#pragma mark - 支付宝支付回调
-(void)alipaySender:(id)sender{
    NSDictionary *resultDic=[sender object];
    NSString *statusStr=resultDic[@"resultStatus"];
    int statusInt=statusStr.intValue;
    switch (statusInt) {
        case 9000:
            [self requestHttpsForOrderId:_orderId price:payModel.payMoney payId:selePayTypeId];
            break;
        case 4000:
            break;
        case 6001:
            break;
        case 6002:
            break;
        default:
            break;
    }
}

#pragma mark - 微信支付回调
-(void)wxSender:(id)sender{
    [self payResultRequest];
    //    BaseResp * resp = [sender object];
    //
    //    if([resp isKindOfClass:[PayResp class]]){
    //
    //        switch (resp.errCode) {
    //            case WXSuccess:
    //                [self requestHttpsForOrderId:_orderId price:payModel.payMoney payId:selePayTypeId];
    //                break;
    //            default:
    //                break;
    //        }
    //    }
}

#pragma mark - init ui
-(void)setUpUI{
    [self initDataSource];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"PayType" ofType:@"plist"];
    payTypeArr = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    
    payMsgDicArr=[NSMutableArray array];
    
    //
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStylePlain];
    myTable.backgroundColor=[UIColor whiteColor];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable registerNib:[UINib nibWithNibName:@"BankTransferCell" bundle:nil] forCellReuseIdentifier:@"BankTransferCell"];
    [self.view addSubview:myTable];
    
    //headView
    UIView *headView=[[UIView alloc]init];
    headView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    UIView *img_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, ((SCREENSIZE.width)*56)/660+16)];
    img_view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headView addSubview:img_view];
    
    
    //
    headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 44)];
    headerSectionView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headView addSubview:headerSectionView];
    
    UIImageView *secImgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 11, headerSectionView.height-24, headerSectionView.height-24)];
    secImgv.contentMode=UIViewContentModeScaleAspectFit;
    secImgv.image=[UIImage imageNamed:@"icon_wddd"];
    [headerSectionView addSubview:secImgv];
    
    //订单编号
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(secImgv.frame.origin.x+secImgv.width+8, 0, 120, headerSectionView.height)];
    label.font=kFontSize15;
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    label.text=@"订单详情";
    [headerSectionView addSubview:label];
    
    //订单信息
    CGFloat orInfoYH=[self initOrderInfoForModel:payModel view:headView];
    
    UIView * yuEView = [[UIView alloc]initWithFrame:CGRectMake(0, orInfoYH+20, SCREENSIZE.width, 89)];
    [yuEView setBackgroundColor:[UIColor whiteColor]];
    [headView addSubview:yuEView];
    
    UILabel * yuELabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 7, 90, 30)];
    yuELabel.text = @"我的余额：";
    [yuEView addSubview:yuELabel];
    
     NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
    UILabel * tiShiYuELabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-200, 7, 155, 30)];
    if (usrMony.floatValue==0) {
       tiShiYuELabel.text = @"0元";
    }else{
        tiShiYuELabel.text = [NSString stringWithFormat:@"%.2f",usrMony.floatValue];
    }
    
    tiShiYuELabel.textAlignment = 2;
    tiShiYuELabel.textColor = [UIColor colorWithHexString:@"ff5742"];
    [yuEView addSubview:tiShiYuELabel];
    
    UIImage * zhiButtonImage = [UIImage imageNamed:@"toursuccess_hs"];
    UIButton * zhiFButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-35*W_UNIT, 10*H_UNIT, 22*W_UNIT,22*W_UNIT)];
    [zhiFButton addTarget:self action:@selector(yuEPaySender:) forControlEvents:UIControlEventTouchUpInside];
    [zhiFButton setBackgroundImage:zhiButtonImage forState:UIControlStateNormal];
    [yuEView addSubview:zhiFButton];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, yuEView.size.height/2, SCREENSIZE.width, 0.5)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    [yuEView addSubview:lineView];
    
    totalLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,lineView.origin.y+lineView.size.height+10, SCREENSIZE.width-45, 30)];
    NSMutableAttributedString *string;
        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%.2f元",yuEMony.floatValue]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
    totalLabel.attributedText = string;
    totalLabel.textAlignment = 2;
    [yuEView addSubview:totalLabel];
    
    
    //headerSection02
    UIView *headerSection02=[[UIView alloc]initWithFrame:CGRectMake(0, yuEView.origin.y+yuEView.size.height, SCREENSIZE.width, 44)];
//    UIView *headerSection02=[[UIView alloc]initWithFrame:CGRectMake(0, orInfoYH, SCREENSIZE.width, 44)];
    headerSection02.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headView addSubview:headerSection02];
    
    UIImageView *sec02Imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 11, headerSection02.height-18, headerSection02.height-24)];
    sec02Imgv.contentMode=UIViewContentModeScaleAspectFit;
    sec02Imgv.image=[UIImage imageNamed:@"icon_zffs"];
    [headerSection02 addSubview:sec02Imgv];
    
    UILabel *label02=[[UILabel alloc]initWithFrame:CGRectMake(secImgv.frame.origin.x+secImgv.width+8, 0, 120, headerSection02.height)];
    label02.font=kFontSize15;
    label02.textColor=[UIColor colorWithHexString:@"#282828"];
    label02.text=@"选择支付方式";
    [headerSection02 addSubview:label02];
    
    headView.frame=CGRectMake(0, 0, SCREENSIZE.width, headerSection02.height+headerSection02.origin.y);
    
    myTable.tableHeaderView=headView;
    
    //
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 49)];
    myTable.tableFooterView=footView;
    
    
    //
    [self setUPPayView];
}
-(void)yuEPaySender:(UIButton *)sender{
    if (sender.selected==NO) {
        [sender setBackgroundImage:[UIImage imageNamed:@"toursuccess_icon"] forState:UIControlStateNormal];
        sender.selected=YES;
        NSString * usrMony = [[NSUserDefaults standardUserDefaults]objectForKey:User_Money];
        NSMutableAttributedString *string;
        NSString * newTotaMony = [NSString stringWithFormat:@"%.2f",yuEMony.floatValue - usrMony.floatValue];
        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%@元",newTotaMony]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
        totalLabel.attributedText = string;
    }else{
        [sender setBackgroundImage:[UIImage imageNamed:@"toursuccess_hs"] forState:UIControlStateNormal];
        sender.selected=NO;
        NSMutableAttributedString *string;
        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"合计应付：%.2f元",yuEMony.floatValue ]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"999999"] range:NSMakeRange(0,5)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff5742"] range:NSMakeRange(5,string.length-5)];
        totalLabel.attributedText = string;
    }
}
-(CGFloat)initOrderInfoForModel:(OrderListModel *)model view:(UIView *)view{
    yuEMony = model.payMoney;
    UIView *orderInfoView=[[UIView alloc]initWithFrame:CGRectMake(8, headerSectionView.origin.y+headerSectionView.height+0.5f, SCREENSIZE.width-16, 145)];
    orderInfoView.layer.borderWidth=0.5f;
    orderInfoView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    orderInfoView.backgroundColor=[UIColor whiteColor];
    [view addSubview:orderInfoView];
    CGRect frame=orderInfoView.frame;
    CGFloat cellHeight=0.0f;
    
#pragma mark 订单编号
    UILabel *ordLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 12, LeftLabelWidth, LeftLabelHeight)];
    ordLabel.font=[UIFont systemFontOfSize:TextFont];
    ordLabel.text=@"订单编号:";
    [orderInfoView addSubview:ordLabel];
    
    UILabel *ordNum=[[UILabel alloc]initWithFrame:CGRectMake(ordLabel.origin.x+ordLabel.width+10, ordLabel.origin.y, SCREENSIZE.width-(ordLabel.origin.x+ordLabel.width+16), ordLabel.height)];
    ordNum.font=[UIFont systemFontOfSize:TextFont];
    ordNum.text=model.orderId;
    ordNum.numberOfLines=0;
    [orderInfoView addSubview:ordNum];
    
#pragma mark 产品名称
    UILabel *label01=[[UILabel alloc]initWithFrame:CGRectMake(8, ordLabel.origin.y+ordLabel.height+10, LeftLabelWidth, LeftLabelHeight)];
    label01.font=[UIFont systemFontOfSize:TextFont];
    label01.text=@"产品名称:";
    [orderInfoView addSubview:label01];
    
    NSString *proName=model.islandTitle;
    CGSize size=[AppUtils getStringSize:proName withFont:TextFont];
    CGFloat pnWidth=orderInfoView.width-(label01.origin.x+label01.width+16);
    CGFloat pnHeight=0;
    if (size.width>pnWidth) {
        pnHeight=40;
    }else{
        pnHeight=21;
    }
    
    UILabel *productName=[[UILabel alloc]initWithFrame:CGRectMake(label01.origin.x+label01.width+10, label01.origin.y, pnWidth, pnHeight)];
    productName.font=[UIFont systemFontOfSize:TextFont];
    productName.text=proName;
    productName.numberOfLines=0;
    [orderInfoView addSubview:productName];
    
#pragma mark 酒店类型
    UILabel *label02=[[UILabel alloc]initWithFrame:CGRectMake(8, productName.origin.y+productName.height+8, LeftLabelWidth, LeftLabelHeight)];
    label02.font=[UIFont systemFontOfSize:TextFont];
    label02.text=@"酒店类型:";
    [orderInfoView addSubview:label02];
    
    NSString *roomTypeStr=model.roomType;
    CGSize roomSize=[AppUtils getStringSize:roomTypeStr withFont:TextFont];
    CGFloat roomHeight=0;
    if (roomSize.width<pnWidth) {
        roomHeight=21;
    }else{
        roomHeight=42;
    }
    UILabel *roomType=[[UILabel alloc]initWithFrame:CGRectMake(label02.origin.x+label02.width+10, label02.origin.y, pnWidth, roomHeight)];
    roomType.font=[UIFont systemFontOfSize:TextFont];
    NSString *labelTtt=roomTypeStr;
    roomType.numberOfLines=0;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
    roomType.attributedText = attributedString;
    [orderInfoView addSubview:roomType];
    
    ///////////////////////////////////
#pragma mark 机票类型
    UILabel *label03=[[UILabel alloc]initWithFrame:CGRectMake(8, roomType.origin.y+roomType.height+8, LeftLabelWidth, LeftLabelHeight)];
    label03.font=[UIFont systemFontOfSize:TextFont];
    label03.text=@"机票类型:";
    [orderInfoView addSubview:label03];
    
    UILabel *flightLabel=[[UILabel alloc]initWithFrame:CGRectMake(label03.origin.x+label03.width+10, label03.origin.y, pnWidth, 21)];
    flightLabel.font=[UIFont systemFontOfSize:TextFont];
    flightLabel.text=model.flightInfo;
    [orderInfoView addSubview:flightLabel];
    
#pragma mark 总计金额
    UILabel *label04=[UILabel new];
    label04.font=[UIFont systemFontOfSize:TextFont];
    label04.text=@"总计金额:";
    [orderInfoView addSubview:label04];
    
    CGFloat label04Y=flightLabel.origin.y+flightLabel.height+8;
    label04.frame=CGRectMake(8, label04Y, LeftLabelWidth, LeftLabelHeight);
    
    //
    CGFloat ttpHeight=0;
    if ([model.stageSum isEqualToString:@"1"]) {
        ttpHeight=21;
    }else{
        ttpHeight=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?42:84;
    }
    UILabel *totalPrice=[[UILabel alloc]initWithFrame:CGRectMake(label04.origin.x+label04.width+10, label04.origin.y, pnWidth, ttpHeight)];
    totalPrice.font=[UIFont systemFontOfSize:TextFont];
    totalPrice.numberOfLines=0;
    
    NSString *stageFee;
    
    NSString *rateStr;
    if ([model.stageSum isEqualToString:@"1"]) {
        rateStr=_rateArr[0];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"3"]){
        rateStr=_rateArr[1];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"6"]){
        rateStr=_rateArr[2];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"9"]){
        rateStr=_rateArr[3];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }else if ([model.stageSum isEqualToString:@"12"]){
        rateStr=_rateArr[4];
        stageFee=[NSString stringWithFormat:@"%.1f%%",rateStr.floatValue*100];
    }
    
    CGFloat ttPrice=model.totalShowPrice.floatValue;
    CGFloat coupon=model.couponPrice.floatValue;
    NSString *ttpStr=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?[NSString stringWithFormat:@"%.f元 （产品总额%.f元，分%@期支付,每期应付 %@元）",[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,model.stageSum,model.price_everyStage]:[NSString stringWithFormat:@"%.f元 （分%@期支付，%@期手续费率 %@ ,手续费总计 %@元 ,每期应付 %@元 ，其中首期需额外支付全部手续费 %@元 ）",[model.cardType isEqualToString:@"3"]?coupon+ttPrice:ttPrice,model.stageSum,model.stageSum,stageFee,model.stagePrice,model.price_everyStage,model.stagePrice];
    NSMutableAttributedString *ttpAtString = [[NSMutableAttributedString alloc] initWithString:ttpStr];
    NSMutableParagraphStyle *ttpStyle = [[NSMutableParagraphStyle alloc] init];
    [ttpStyle setLineSpacing:2];//调整行间距
    [ttpAtString addAttribute:NSParagraphStyleAttributeName value:ttpStyle range:NSMakeRange(0, [ttpStr length])];
    if ([model.stageSum isEqualToString:@"1"]) {
        totalPrice.text=[NSString stringWithFormat:@"%@元",model.totalShowPrice];
    }else{
        totalPrice.attributedText = ttpAtString;
    }
    [orderInfoView addSubview:totalPrice];
    
#pragma mark 应付金额
    UILabel *label05=[[UILabel alloc]initWithFrame:CGRectMake(8, totalPrice.origin.y+totalPrice.height+8, LeftLabelWidth, LeftLabelHeight)];
    label05.font=[UIFont systemFontOfSize:TextFont];
    label05.text=@"应付金额:";
    [orderInfoView addSubview:label05];
    
    UILabel *payPrice=[[UILabel alloc]initWithFrame:CGRectMake(label05.origin.x+label05.width+10, label05.origin.y, pnWidth, 21)];
    payPrice.font=[UIFont systemFontOfSize:TextFont];
    NSString *payStr;
    if ([model.stageSum isEqualToString:@"1"]) {
        payStr=[NSString stringWithFormat:@"%@元",model.payMoney];
    }else{
        payStr=[model.stageSum isEqualToString:@"1"]||[model.stageSum isEqualToString:@"3"]?[NSString stringWithFormat:@"%@元",model.payMoney]:[NSString stringWithFormat:@"%@元（含全部手续费）",model.payMoney];
    }
    payPrice.text=payStr;
    [orderInfoView addSubview:payPrice];
    
    cellHeight=payPrice.origin.y+payPrice.height+12;
    
    frame.size.height=cellHeight;
    orderInfoView.frame=frame;
    return orderInfoView.origin.y+orderInfoView.height;
}

-(void)setUPPayView{
    payView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 49)];
    payView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self.view addSubview:payView];
    //
    payBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    payBtn.frame=CGRectMake(0, 0, payView.width, payView.height);
    payBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
    [payBtn setTitle:@"立即支付" forState:UIControlStateNormal];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    payBtn.titleLabel.font=kFontSize17;
    [payBtn addTarget:self action:@selector(clickPay) forControlEvents:UIControlEventTouchUpInside];
    [payView addSubview:payBtn];
}

- (void)initDataSource{
    sectionArray  = [NSMutableArray arrayWithObjects:
                     @"银行转账",
                     @"支付宝",
                     @"微信支付",nil];
    dataArray=[NSMutableArray array];
    
    for (int group=0; group<sectionArray.count; group++) {
        
        PayTypeSectionModel *groupModel=[[PayTypeSectionModel alloc]init];
        
        
        NSString *titId;
        NSString *titStr;
        if (group==0){
            titId=@"0";
            titStr=@"银行转账";
        }else if (group==1){
            titId=@"1";
            titStr=@"支付宝";
        }else{
            titId=@"2";
            titStr=@"微信支付";
        }
        groupModel.isHide=YES;
        groupModel.groupId=titId;
        groupModel.groupName=titStr;
        [dataArray addObject:groupModel];
    }
    //默认选中支付宝
    selePayTypeId=@"3";
    seleGroupId=@"1";
    seleModel=dataArray[1];
    seleModel.modelTag=1;
}

#pragma mark -
#pragma mark - UITableViewDataSource UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0){
        return 133;
    }else{
        return 0.000000001;
    }
}

//组头高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    PayTypeSectionModel *model=dataArray[section];
    if (section==0) {
        //判断是否隐藏
        if (model.isHide) {
            return 0;
        }else{
            return 1;
        }
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BankTransferCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankTransferCell"];
    if (!cell) {
        cell=[[BankTransferCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BankTransferCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title01.text=sectionTitle01;
    cell.title02.text=sectionTitle02;
    cell.title03.text=sectionTitle03;
    cell.title04.text=sectionTitle04;
    return cell;
}

//返回组头的VIew
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    PayTypeCell *headView=nil;
    
    if (headView==nil) {
        
        headView=[[NSBundle mainBundle]loadNibNamed:@"PayTypeCell" owner:Nil options:Nil][0];
        
        PayTypeSectionModel *model=dataArray[section];
        
        headView.title.text=model.groupName;
        
        //
        if ([model.groupId isEqualToString:seleGroupId]) {
            headView.seleImgv.image=[UIImage imageNamed:seleImg_sele];
        }else{
            headView.seleImgv.image=[UIImage imageNamed:seleImg_nor];
        }
        
        //
        NSString *imgStr;
        if (section==0){
            imgStr=@"pay_bank";
        }else if (section==1){
            imgStr=@"支付宝";
        }else{
            imgStr=@"微信";
        }
        headView.imgv.image=[UIImage imageNamed:imgStr];
        [headView addTarget:self action:@selector(onHeadClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        headView.tag=section;
    }
    return headView;
}


#pragma mark - headview的点击事件
-(void)onHeadClicked:(PayTypeCell*)sender{
    
    //获取支付类型id
    [self getPayTypeIdWithTag:sender.tag];
    
    seleGroupId=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    //通过点击的headview的tag值，取出对应的数据源
    headClickmodel=dataArray[sender.tag];
    headClickmodel.modelTag=sender.tag;
    
    //显示隐藏支付按钮
    if(sender.tag==0){
        [UIView animateWithDuration:0.3 animations:^{
            payView.frame=CGRectMake(0, SCREENSIZE.height, 0, 0);
            myTable.frame=CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64);
        }];
    }else{
        [payBtn setTitle:@"立即支付" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            payView.frame=CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 64);
            myTable.frame=CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113);
        }];
    }
    
    //选择不同section
    if (seleModel.modelTag!=headClickmodel.modelTag) {
        
        [myTable reloadSections:[NSIndexSet indexSetWithIndex:seleModel.modelTag] withRowAnimation:UITableViewRowAnimationFade];
        
        //将数据里的是否隐藏取反
        headClickmodel.isHide=NO;
    }
    
    //选择相同section
    else{
        if(seleModel.isHide!=headClickmodel.isHide){
            headClickmodel.isHide=NO;
        }else{
            headClickmodel.isHide=!headClickmodel.isHide;
        }
    }
    
    //刷新对应的组
    [myTable reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationFade];
    seleModel=headClickmodel;
    seleModel.isHide=YES;
}


-(void)getPayTypeIdWithTag:(NSInteger)tag{
    switch (tag) {
        case 0:
            selePayTypeId=@"5";
            break;
        case 1:
            selePayTypeId=@"3";
            break;
        case 2:
            selePayTypeId=@"8";
            break;
        default:
            break;
    }
}

#pragma mark - 点击立即支付
-(void)clickPay{
    if ([selePayTypeId isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请选择支付方式" inView:self.view];
    }else if ([selePayTypeId isEqualToString:@"3"]){
        [self sendAliPay];
    }else if ([selePayTypeId isEqualToString:@"8"]){
        [self sendWXPay];
    }else{
        [self useXYKWithOrderId:_orderId cartId:_selectedXYKId];
    }
}


-(void)pushOrderDetailViewWithController{
    
}

#pragma mark - 支付宝支付
-(void)sendAliPay{
    [self aliPayRequest];
}

-(void)alliPayWithId:(NSString *)strId{
    NSString *appScheme = @"youxiaalipay";
    //支付异步请求并回调
    [[AlipaySDK defaultService] payOrder:strId fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ALIPAYCALLBACK" object:resultDic];
        [self payResultRequest];
    }];
}

#pragma mark - 微信支付
- (void)sendWXPay{
    if([WXApi isWXAppInstalled]){
        [self weixinPayRequest];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark
#pragma mark - request请求
-(void)dataRequestOrderId:(NSString *)orderId{
    
    [AppUtils showProgressInView:self.view];
    
    bankDataArr=[NSMutableArray array];
    userXYKDataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"1" forKey:@"is_kf"];
    [dict setObject:@"confirm_order_iso" forKey:@"action"];
    [dict setObject:orderId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self dataRequestOrderId:orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSDictionary *dic=responseObject[@"retData"];
                
                payModel=[[OrderListModel alloc]init];
                [payModel jsonApproveDataForDictionary:dic];
                
                [self setUpUI];
            }
            [myTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//更改订单状态
/*
 payId
 转账     id= 5
 代扣     id=10
 微信     id=8
 支付宝    id= 3
 */
-(void)requestHttpsForOrderId:(NSString *)orderId price:(NSString *)price payId:(NSString *)payId{
    [AppUtils showActivityIndicatorView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"set_pay_log_iso" forKey:@"action"];
    [dict setObject:orderId forKey:@"orderid"];
    [dict setObject:price forKey:@"price"];
    [dict setObject:payId forKey:@"payid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"\n--%@",responseObject[@"retData"][@"msg"]);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderId:orderId price:price payId:payId];
            });
        }else{
            [self setUpUI];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                PaySuccessController *pays=[[PaySuccessController alloc]init];
                pays.orderId=payModel.orderId;
                pays.orderName=payModel.islandTitle;
                pays.orderPrice=payModel.payMoney;
                [self.navigationController pushViewController:pays animated:YES];
            }
        }
        [AppUtils dismissActivityIndicatorView:self.view];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//使用信用卡代扣
-(void)useXYKWithOrderId:(NSString *)orderId cartId:(NSString *)cartId{
    [AppUtils showActivityIndicatorView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"confirm_order_credit_edit_iso" forKey:@"action"];
    [dict setObject:orderId forKey:@"orderid"];
    [dict setObject:cartId forKey:@"credit_id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"\n--%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self useXYKWithOrderId:orderId cartId:cartId];
            });
        }else{
            [self setUpUI];
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:responseObject[@"retData"][@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction*yesAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
                    for (UIViewController *controller in self.navigationController.viewControllers) {
                        if ([controller isKindOfClass:[OrderListController class]]) {
                            [self.navigationController popToViewController:controller animated:YES];
                        }
                        //                        else if ([controller isKindOfClass:[SubmitOrderController class]]){
                        //                            [self.navigationController popToRootViewControllerAnimated:YES];
                        //                        }
                    }
                }];
                [alertController addAction:yesAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        [AppUtils dismissActivityIndicatorView:self.view];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//向服务器发起微信支付请求
-(void)weixinPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"get_weixin_pre_payid" forKey:@"action"];
    [dict setObject:_orderId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:@"8" forKey:@"payid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"微信 ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"][@"post_data"];
                if ([resObj isKindOfClass:[NSDictionary class]]) {
                    FilmWeiXinPayModel *weixinPayModel=[FilmWeiXinPayModel new];
                    [weixinPayModel jsonDataForDictioanry:resObj];
                    [self weixinPayWithModel:weixinPayModel];
                }
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}
//支付宝支付
-(void)aliPayRequest{
    [AppUtils showProgressMessage:@"正在确认订单……" inView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"get_alipay" forKey:@"action"];
    [dict setObject:_orderId forKey:@"id"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:@"3" forKey:@"payid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"ali ： %@",responseObject);
        
        if ([responseObject isKindOfClass:[NSNull class]]||
            responseObject==nil||
            responseObject==NULL) {
            [AppUtils showSuccessMessage:[NSString stringWithFormat:@"服务器接口返回%@",responseObject] inView:self.view];
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                id resObj=responseObject[@"retData"][@"result"];
                if ([resObj isKindOfClass:[NSString class]]) {
                    [self alliPayWithId:resObj];
                }
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"msg"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

//查询订单交易状态
-(void)payResultRequest{
    [AppUtils showProgressInView:self.view];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"order" forKey:@"mod"];
    [dict setObject:@"queryOrder" forKey:@"action"];
    [dict setObject:_orderId forKey:@"orderid"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"userid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[NetWorkRequest defaultClient] requestWithPath:FilmHost method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSLog(@"查询订单支付状态=%@",responseObject);
        
        //0未支付 1已支付
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"pay"]];
            if ([str isEqualToString:@"1"]) {
                PaySuccessController *pays=[[PaySuccessController alloc]init];
                pays.orderId=_orderId;
                pays.orderName=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"islandTitle"]];
                pays.orderPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"payMoney"]];
                [self.navigationController pushViewController:pays animated:YES];
            }
            NSLog(@"%@",responseObject[@"retData"][@"islandTitle"]);
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        DSLog(@"%@",error);
    }];
}

#pragma mark - 调起客户端微信支付
-(void)weixinPayWithModel:(FilmWeiXinPayModel *)model{
    [WXApi registerApp:model.appid];
    if([WXApi isWXAppInstalled]){
        PayReq* req    = [[PayReq alloc] init];
        req.openID     = model.appid;
        req.partnerId  = model.partnerid;           //商户号
        req.prepayId   = model.prepayid;            //预支付交易会话ID
        req.package    = model.package;             //扩展字段
        req.nonceStr   = model.noncestr;            //随机字符串
        req.timeStamp  = model.timestamp.intValue;  //时间戳（防止重发）
        req.sign       = model.sign;                //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您未安装微信客户端" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
