//
//  BankTransferCell.h
//  youxia
//
//  Created by mac on 16/1/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankTransferCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title01;
@property (weak, nonatomic) IBOutlet UILabel *title02;
@property (weak, nonatomic) IBOutlet UILabel *title03;
@property (weak, nonatomic) IBOutlet UILabel *title04;

@end
