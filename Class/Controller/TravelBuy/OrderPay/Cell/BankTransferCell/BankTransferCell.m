//
//  BankTransferCell.m
//  youxia
//
//  Created by mac on 16/1/15.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "BankTransferCell.h"

@implementation BankTransferCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.title01.font=[UIFont systemFontOfSize:IS_IPHONE5?14:16];
    self.title02.font=[UIFont systemFontOfSize:IS_IPHONE5?14:16];
    self.title03.font=[UIFont systemFontOfSize:IS_IPHONE5?14:16];
    self.title04.font=[UIFont systemFontOfSize:IS_IPHONE5?14:16];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
