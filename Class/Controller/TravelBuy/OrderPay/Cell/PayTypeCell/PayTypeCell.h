//
//  PayTypeCell.h
//  youxia
//
//  Created by mac on 16/1/16.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayTypeSectionModel.h"

@interface PayTypeCell : UIControl

@property (weak, nonatomic) IBOutlet UIImageView *imgv;

@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UIImageView *seleImgv;

@end
