//
//  UseCouponController.h
//  youxia
//
//  Created by mac on 15/12/24.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UseCouponController : UIViewController

@property (nonatomic,assign) BOOL isVIP;
@property (nonatomic,retain) NSArray *couponArr;
@property (nonatomic,copy) NSString *selectedCouponId; //选择的优惠券ID
@property (nonatomic,copy) NSString *goodPrice;
@property (nonatomic, strong) void (^kCouponBlock)(id model);

@end
