//
//  UseCopuonModel.h
//  youxia
//
//  Created by mac on 15/12/24.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UseCopuonModel : NSObject

@property(nonatomic ,copy) NSString *couponID;
@property(nonatomic ,copy) NSString *couponNumber;
@property(nonatomic ,assign) BOOL isQuane;          //是否是全额
@property(nonatomic ,copy) NSString *couponMoney;
@property(nonatomic ,copy) NSString *couponEndTime;
@property (nonatomic,assign) BOOL isSelectedCoupon;

-(void)jsonDataForDictionay:(NSDictionary *)dic;

@end
