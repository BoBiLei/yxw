//
//  UseCopuonModel.m
//  youxia
//
//  Created by mac on 15/12/24.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "UseCopuonModel.h"

@implementation UseCopuonModel

-(void)jsonDataForDictionay:(NSDictionary *)dic{
    self.couponID=dic[@"id"];
    self.couponNumber=dic[@"number"];
    NSRange nRang=[self.couponNumber rangeOfString:@"YX"];
    if (nRang.location!=NSNotFound) {
        self.isQuane=YES;
    }else{
        self.isQuane=NO;
    }
    self.couponMoney=dic[@"price"];
    
    
    //时间戳转换
    NSString *timeStr=dic[@"duetime"];
    NSTimeInterval time=[timeStr doubleValue];
    
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/BeiJing"];
    [dateFormatter setTimeZone:timeZone];
    
    self.couponEndTime=[dateFormatter stringFromDate: detaildate];
}

@end
