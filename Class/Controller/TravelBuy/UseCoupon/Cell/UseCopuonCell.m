//
//  UseCopuonCell.m
//  youxia
//
//  Created by mac on 15/12/24.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "UseCopuonCell.h"

@implementation UseCopuonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _validate.adjustsFontSizeToFitWidth=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
