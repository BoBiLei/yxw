//
//  UseCopuonCell.h
//  youxia
//
//  Created by mac on 15/12/24.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UseCopuonCell : UITableViewCell

@property (copy, nonatomic) NSString *couponId;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *validate;

@end
