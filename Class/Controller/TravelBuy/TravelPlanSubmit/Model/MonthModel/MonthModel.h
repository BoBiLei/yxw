//
//  MonthModel.h
//  youxia
//
//  Created by mac on 15/12/18.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MonthModel : NSObject

@property (nonatomic ,copy) NSString *yearStr;
@property (nonatomic ,copy) NSString *monthStr;
@property (nonatomic ,copy) NSString *daySum;
@property (nonatomic ,copy) NSString *firstweek;
@property (nonatomic ,assign) BOOL isDefault;
@property (nonatomic ,retain) NSArray *canSeleDayArr;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

-(void)jsonDataForComboDictionary:(NSDictionary *)dic;      //信用卡套餐的

@end
