//
//  MonthModel.m
//  youxia
//
//  Created by mac on 15/12/18.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "MonthModel.h"

@implementation MonthModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.yearStr=dic[@"date_y"];
    self.monthStr=dic[@"date_m"];
    self.daySum=dic[@"day_num"];
    self.firstweek=dic[@"firstweek"];
    if ([dic objectForKey:@"is_m"]) {
        self.isDefault=YES;
    }else{
        self.isDefault=NO;
    }
    _canSeleDayArr=dic[@"date_d"];
}

-(void)jsonDataForComboDictionary:(NSDictionary *)dic{
    self.yearStr=dic[@"date_y"];
    self.monthStr=dic[@"date_m"];
    self.daySum=dic[@"day_num"];
    self.firstweek=dic[@"firstweek"];
    if ([dic objectForKey:@"is_m"]) {
        self.isDefault=YES;
    }else{
        self.isDefault=NO;
    }
    _canSeleDayArr=dic[@"data"];
}

@end
