//
//  CityModel.m
//  youxia
//
//  Created by mac on 15/12/17.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "CityModel.h"

@implementation CityModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.cityId=dic[@"id"];
    self.flightid=dic[@"flightid"];
    self.cityName=dic[@"city"];
    self.cityPrice=dic[@"price"];
}

-(void)jsonDataForCPDictionary:(NSDictionary *)dic{
    self.cityId=dic[@"roomid"];
    self.flightid=dic[@"id"];
    self.cityName=dic[@"tc_name"];
    self.cityPrice=dic[@"price"];
}

@end
