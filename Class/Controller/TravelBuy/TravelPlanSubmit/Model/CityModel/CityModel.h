//
//  CityModel.h
//  youxia
//
//  Created by mac on 15/12/17.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityModel : NSObject

@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *cityPrice;
@property (nonatomic, copy) NSString *flightid;
@property (nonatomic, assign) BOOL isDefaultSelect;

-(void)jsonDataForDictionary:(NSDictionary *)dic;

//超跑的
-(void)jsonDataForCPDictionary:(NSDictionary *)dic;

@end
