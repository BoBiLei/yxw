//
//  UseStageModel.h
//  youxia
//
//  Created by mac on 15/12/21.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UseStageModel : NSObject

@property (nonatomic, copy) NSString *allPrice;
@property (nonatomic, copy) NSString *stageSum;
@property (nonatomic, copy) NSString *sxfPrice;
@property (nonatomic, assign) BOOL isSele;
@property (nonatomic, copy) NSString *stagePrice;

//游侠分期价的
@property (nonatomic, copy) NSString *comboBtnNum;
@property (nonatomic, copy) NSString *comboBtnFqPrice;
@property (nonatomic, copy) NSString *comboBtnStageFee;
@property (nonatomic, copy) NSString *comboBtnStageRate;    //费率

-(void)jsonDataForDictionary:(NSDictionary *)dic;

-(void)jsondataForComboDic:(NSDictionary *)dic;

@end
