//
//  UseStageModel.m
//  youxia
//
//  Created by mac on 15/12/21.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "UseStageModel.h"

@implementation UseStageModel

-(void)jsonDataForDictionary:(NSDictionary *)dic{
    self.allPrice=dic[@"all_price"];
    self.stageSum=dic[@"fenqi"];
    self.sxfPrice=dic[@"fenqi_price"];
    NSString *taStr=dic[@"is_on"];
    if ([[NSString stringWithFormat:@"%@",taStr] isEqualToString:@"1"]) {
        self.isSele=YES;
    }else{
        self.isSele=NO;
    }
    self.stagePrice=dic[@"per_price"];
}

-(void)jsondataForComboDic:(NSDictionary *)dic{
    self.comboBtnNum=dic[@"text_fenqi"];
    self.comboBtnFqPrice=dic[@"fenqi_price"];
    self.comboBtnStageFee=dic[@"text_per_price"];
    self.comboBtnStageRate=dic[@"fenqi_lv"];
}

@end
