//
//  RoomModel.h
//  youxia
//
//  Created by mac on 16/5/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomModel : NSObject

@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *defaultPrice;
@property (nonatomic, copy) NSString *roomId;
@property (nonatomic, copy) NSString *isCur;
@property (nonatomic, copy) NSString *pnumber;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *tcName;
@property (nonatomic, retain) NSDictionary *model;

@end
