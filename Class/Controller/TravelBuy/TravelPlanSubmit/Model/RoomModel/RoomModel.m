//
//  RoomModel.m
//  youxia
//
//  Created by mac on 16/5/25.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "RoomModel.h"

@implementation RoomModel

-(void)setModel:(NSDictionary *)model{
    self.date=model[@"date"];
    self.defaultPrice=model[@"default_price"];
    self.roomId=model[@"roomid"];
    self.isCur=model[@"is_cur"];
    self.pnumber=model[@"pnumber"];
    self.price=model[@"price"];
    self.tcName=model[@"tc_name"];
}

@end
