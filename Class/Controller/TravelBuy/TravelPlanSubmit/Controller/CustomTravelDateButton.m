//
//  CustomTravelDateButton.m
//  youxia
//
//  Created by mac on 16/8/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CustomTravelDateButton.h"

@implementation CustomTravelDateButton

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.dateLabel];
    [self addSubview:self.priceLabel];
    [self addSubview:self.residueLabel];
}

-(UILabel *)dateLabel{
    if (!_dateLabel) {
        _dateLabel=[[UILabel alloc] init];
        _dateLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _dateLabel;
}

-(UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel=[[UILabel alloc] init];
        _priceLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _priceLabel;
}

-(UILabel *)residueLabel{
    if (!_residueLabel) {
        _residueLabel=[[UILabel alloc] init];
        _residueLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _residueLabel;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    self.dateLabel.frame = CGRectMake(0, height/16, width, height*2/5);
    self.priceLabel.frame = CGRectMake(0, height*2/5+2, width, (height-self.dateLabel.height)/2);
    self.residueLabel.frame = CGRectMake(0, self.priceLabel.origin.y+self.priceLabel.height-height/18, width, self.priceLabel.height);
}

@end
