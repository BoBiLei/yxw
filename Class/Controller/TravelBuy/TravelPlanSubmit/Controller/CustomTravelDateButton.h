//
//  CustomTravelDateButton.h
//  youxia
//
//  Created by mac on 16/8/5.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTravelDateButton : UIControl

@property (nonatomic, strong) UILabel *dateLabel;       //日期
@property (nonatomic, strong) UILabel *priceLabel;      //价格
@property (nonatomic, strong) UILabel *residueLabel;    //余票

@end
