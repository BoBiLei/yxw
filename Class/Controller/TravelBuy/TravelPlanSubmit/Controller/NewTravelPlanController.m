//
//  NewTravelPlanController.m
//  youxia
//
//  Created by mac on 15/12/5.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "NewTravelPlanController.h"
#import "PKYStepper.h"
#import "MonthModel.h"
#import "CityModel.h"
#import "RoomModel.h"
#import "UseStageModel.h"
#import "UseCouponController.h"
#import "UseCopuonModel.h"
#import "ApprovedOrderController.h"
#import "CustomTravelDateButton.h"

//
#define X_Space 8
#define Y_Space 36
#define Btn_Height 34

//
#define Btn_Margin 3
#define Btn_Y_Margin 3
#define DayOfWeek 7

#define Sele_Color @"#24B8FC"
@interface NewTravelPlanController ()

@end

@implementation NewTravelPlanController{
    
    BOOL isYXCoupon;              //是否含有YX的优惠券(如果有是正常的，没有可以做抵消的)
    
    //（重要）
    CGFloat handleFlightPrice;    //处理后的机票价格
    CGFloat handleTaocanPrice;    //处理后的套餐价格
    CGFloat handleChuHaiPrice;    //处理后的出海含税价格
    CGFloat handleAdultPrice;     //处理后的成人价格
    CGFloat handleCouponPrice;    //计算后的优惠券价格
    
    //是否有参数
    BOOL isHadCity;         //是否有城市
    BOOL isHadFlight;       //是否有航班
    BOOL isHadHotel;        //是否有酒店
    BOOL isHadFenqi;        //是否有分期
    BOOL isPalau;           //是否是帕劳
    BOOL isVIP;             //是否是VIP
    
    //
    UINavigationBar *cusBar;
    UIImageView *searchView;
    
    //
    NSMutableArray *rateArr;
    CGFloat rate01;
    CGFloat rate03;
    CGFloat rate06;
    CGFloat rate09;
    CGFloat rate12;
    
    //赛选日期（左边、右边）按钮
    UIButton *cLeftBtn;
    UIButton *cRightBtn;
    
    TTTAttributedLabel *seleDateLabel;
    
    BOOL is98zhe;         //是否选择98折
    NSString *hyNumber;   //选择的会员特权（0不是会员，1 98折，2满立减）
    
    //优惠券
    UIButton *cpPriceBtn;
    NSString *seleCouponId;
    NSString *seleCouponPrice;
    UseCopuonModel *rcvCouponModel;         //返回的model
    UseCouponController *userCouponCtr;
    
    //
    NSMutableArray *monthArr;
    NSMutableArray *roomArr;
    NSArray *couponArr;
    NSInteger seleMonthIndex;       //选择的月份数组下标
    NSMutableArray *cityArr;
    NSMutableArray *useStageArr;
    PKYStepper *myStepper;
    UITableView *myTable;
    UIView *calendarView;           //日历View
    UIView *footView;
    UIButton *seletedGoPlaceBtn;    //选择的城市按钮
    UIImageView *weekView;
    UIView *grideView;
    UILabel *yearLabel;
    UILabel *fqTitle;
    TTTAttributedLabel *crLabel;   //chengren价格Label
    
    //
    TTTAttributedLabel *topPriceLabel;
    TTTAttributedLabel *btmShowPriceLabel;      //最底部价格Label
    
    //选择的Model或按钮
    MonthModel *selectMonthModel;
    CustomTravelDateButton *selectedDayBtn;
    UIButton *selectedXYKBtn;
    UIButton *selectedTCBtn;
    
    BOOL isSeleHotel;       //是否选择酒店
    BOOL isSeleTC;          //是否选择套餐
    UIButton *hbBtn;
    BOOL isSeleHbBtn;
    UIButton *selectedUseStageBtn;
    UIButton *selectedCHHDayBtn;
    
    NSString *roomPrice;    //酒店price
    NSString *seletCityPrice;   //当前选择的城市航班价格
    
    UIButton *useStageBtn01;
    UIButton *useStageBtn02;
    UIButton *useStageBtn03;
    UIButton *useStageBtn04;
    UIButton *useStageBtn05;
    
    NSString *crLabelString;
    
    BOOL isSeleChuHai;      //是否选择出海
    NSString *dayOfCH;
    
    NSInteger btnTag;       //当前选择的分期数tag
    NSInteger chhBtnTag;    //出海天数tag
    
    //接口参数
    NSString *monthIndex;
    CGFloat per_num;  //出游人数
    NSString *stage_sum;
    NSString *seleDateStr;          //选择的时间
    NSString *showSeleDate;         //显示选择的时间
    NSTimeInterval timeStamp;
    NSString *flightId;             //航班出发地点ID
    NSString *cityId;               //选择的城市ID
    NSString *roomId;               //套餐房间、酒店ID
    NSString *vipPerem;             //提交的vip参数
    
    //
    UITextField *phoneTF;
    
    //
    UIButton *submitOrderBtn;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"提交订单页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TalkingData trackPageEnd:@"提交订单页"];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCouponModel:) name:@"SelctedCouponModel" object:nil];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=@"提交订单";
    [self.view addSubview:navBar];
    
    btnTag=0;
    rate01=0;
    rate03=0;
    rate06=0;
    rate09=0;
    rate12=0;
    cityId=@"0";
    flightId=@"0";
    roomId=@"0";
    seleCouponId=@"0";
    seleCouponPrice=@"0";
    vipPerem=@"0";
    
    isYXCoupon=YES;
    
    [self getStageRate];
    
    [self newRequestForUserId:[AppUtils getValueWithKey:User_ID] pid:self.pId mdid:self.islandId];
}

#pragma mark - init CalendarView（日历）
-(UIView *)setUpCalendarView{
    //（包含年份、周几、天数的View）
    calendarView=[[UIView alloc]initWithFrame:myTable.frame];
    
    //头部
    UIView *topView=[UIView new];
    [calendarView addSubview:topView];
    
    CGFloat topLH=21;
    CGSize htopSize=[AppUtils getStringSize:self.islandName withFont:16];
    CGFloat htrHeight=0;
    if (htopSize.width>=SCREENSIZE.width-24) {
        htrHeight=topLH*2;
    }else{
        htrHeight=topLH;
    }
    UILabel *topLabel=[[UILabel alloc] initWithFrame:CGRectMake(12, 5, SCREENSIZE.width-24, htrHeight)];
    topLabel.font=kFontSize16;
    topLabel.numberOfLines=0;
    topLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    topLabel.text=self.islandName;
    [topView addSubview:topLabel];
    
    topPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(12, topLabel.height+6, SCREENSIZE.width-24, topLH)];
    topPriceLabel.textAlignment=NSTextAlignmentRight;
    topPriceLabel.font=kFontSize14;
    topPriceLabel.textColor=[UIColor colorWithHexString:@"#ff5741"];
    [topView addSubview:topPriceLabel];
    
    UIView *blueView01=[[UIView alloc] initWithFrame:CGRectMake(0, topPriceLabel.origin.y+topPriceLabel.height+4, SCREENSIZE.width, 12)];
    blueView01.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [topView addSubview:blueView01];
    
    topView.frame=CGRectMake(0, 0, SCREENSIZE.width, blueView01.origin.y+blueView01.height);
    
    //
    //添加出发地点
    CGFloat goPlaceY=[self setUpNewGoPlaceView:cityArr afterView:topView];
    
    //最上面的年View
    UIView *yearMView=[[UIView alloc]initWithFrame:CGRectMake(0, goPlaceY+4, SCREENSIZE.width, 45)];
    [calendarView addSubview:yearMView];
    
    //年份Label
    yearLabel=[[UILabel alloc]initWithFrame:CGRectMake(64, 0, 112, yearMView.height)];
    yearLabel.center=CGPointMake(SCREENSIZE.width/2, yearMView.height/2);
    yearLabel.textColor=[UIColor colorWithHexString:@"#646464"];
    yearLabel.font=kFontSize16;
    yearLabel.textAlignment=NSTextAlignmentCenter;
    [yearMView addSubview:yearLabel];
    
    //周View
    weekView=[[UIImageView alloc]initWithFrame:CGRectMake(0, yearMView.origin.y+yearMView.height, SCREENSIZE.width, 27)];
    weekView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [calendarView addSubview:weekView];
    
    //周Label
    for (int i=0; i<DayOfWeek; i++) {
        UILabel *weekLabel=[[UILabel alloc]init];
        weekLabel.textColor=[UIColor colorWithHexString:@"#8f8f8f"];
        weekLabel.textAlignment=NSTextAlignmentCenter;
        weekLabel.font=kFontSize16;
        weekLabel.frame=CGRectMake(weekView.width/7*i, 0, weekView.width/7, weekView.height-2);
        NSString *weekStr;
        switch (i) {
            case 0:
                weekStr=@"日";
                break;
            case 1:
                weekStr=@"一";
                break;
            case 2:
                weekStr=@"二";
                break;
            case 3:
                weekStr=@"三";
                break;
            case 4:
                weekStr=@"四";
                break;
            case 5:
                weekStr=@"五";
                break;
            case 6:
                weekStr=@"六";
                break;
            default:
                break;
        }
        weekLabel.text=weekStr;
        [weekView addSubview:weekLabel];
    }
    
    //
    for (int i=0; i<monthArr.count; i++) {
        MonthModel *model=monthArr[i];
        //默认月份的日期
        if (model.isDefault) {
            yearLabel.text=[NSString stringWithFormat:@"%@年%@月",model.yearStr,model.monthStr];
            //
            CGRect frame=calendarView.frame;
            
            //创建天数
            CGFloat lastY=[self setGrideDayWithModel:model];
            
            frame.size.height=weekView.origin.y+weekView.height+lastY+36;
            calendarView.frame=frame;
            
            //
            seleDateLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(6, calendarView.height-36, SCREENSIZE.width-12, 36)];
            seleDateLabel.font=kFontSize14;
            seleDateLabel.textColor=[UIColor colorWithHexString:@"#474747"];
            NSString *ssdStr=[NSString stringWithFormat:@"● 已选日期：%@",showSeleDate];
            [seleDateLabel setText:ssdStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                NSRange range01 = [[mutableAttributedString string] rangeOfString:@"●" options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff5741"] CGColor] range:range01];
                NSRange range02 = [[mutableAttributedString string] rangeOfString:@"已选日期：" options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#979797"] CGColor] range:range02];
                return mutableAttributedString;
            }];
            [calendarView addSubview:seleDateLabel];
        }
    }
    
    //赛选月份按钮
    CGFloat filbtnWidth=33;
    cLeftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cLeftBtn.frame=CGRectMake(yearLabel.origin.x-filbtnWidth, 6, filbtnWidth, filbtnWidth);
    [cLeftBtn setImage:[UIImage imageNamed:@"calendar_left"] forState:UIControlStateNormal];
    [cLeftBtn addTarget:self action:@selector(clickLeftFillDateBtn) forControlEvents:UIControlEventTouchUpInside];
    cLeftBtn.enabled=NO;
    [yearMView addSubview:cLeftBtn];
    
    cRightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cRightBtn.frame=CGRectMake(yearLabel.origin.x+yearLabel.width, cLeftBtn.origin.y, cLeftBtn.width, cLeftBtn.height);
    [cRightBtn setImage:[UIImage imageNamed:@"calendar_right"] forState:UIControlStateNormal];
    [cRightBtn addTarget:self action:@selector(clickRightFillDateBtn) forControlEvents:UIControlEventTouchUpInside];
    cRightBtn.enabled=YES;
    [yearMView addSubview:cRightBtn];
    
    return  calendarView;
}

#pragma mark - init 出发地点
-(CGFloat)setUpNewGoPlaceView:(NSMutableArray *)modelArr afterView:(UIView *)view{
    
    UIView *goplaceView=[UIView new];
    [calendarView addSubview:goplaceView];
    
    CGFloat cityYH=6;
    
    if (isHadCity) {
        //标题
        UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(X_Space, cityYH, SCREENSIZE.width-16, 36)];
        title.font=kFontSize15;
        title.text=@"出发地点：";
        title.textColor=[UIColor colorWithHexString:@"#474747"];
        [goplaceView addSubview:title];
        
        CGFloat btnWidth=0.0f;
        CGFloat btnX=X_Space;
        CGFloat btnY=Y_Space;
        CGFloat btnHeight=Btn_Height;
        for (int i=0; i<modelArr.count; i++) {
            CityModel *model=modelArr[i];
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.layer.cornerRadius=2;
            btn.layer.borderWidth=0.8f;
            btn.layer.borderColor=[UIColor colorWithHexString:@"#8b8b8b"].CGColor;
            btnX=X_Space*i+i*btnWidth+X_Space;
            btnWidth=(SCREENSIZE.width-40)/4;
            if (modelArr.count>4) {
                if (btnX>=SCREENSIZE.width) {
                    btnX=X_Space*(i-4)+(i-4)*btnWidth+X_Space;
                    btnY=Y_Space+Btn_Height+8;
                }
            }
            btn.frame=CGRectMake(btnX, btnY+8, btnWidth, btnHeight);
            
            [btn setTitleColor:[UIColor colorWithHexString:@"#8b8b8b"] forState:UIControlStateNormal];
            
            //默认选择的
            if (model.isDefaultSelect) {
                [btn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
                btn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
                seletedGoPlaceBtn=btn;
                cityId=model.cityId;
                seletCityPrice=model.cityPrice;
                flightId=model.flightid;
                handleFlightPrice=model.cityPrice.floatValue;
            }
            btn.titleLabel.adjustsFontSizeToFitWidth=YES;
            [btn setTitle:model.cityName forState:UIControlStateNormal];
            objc_setAssociatedObject(btn, "CityModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            btn.titleLabel.font=kFontSize14;
            [btn addTarget:self action:@selector(clickCityBtn:) forControlEvents:UIControlEventTouchUpInside];
            [goplaceView addSubview:btn];
            if (i==modelArr.count-1) {
                UIView *line01=[[UIView alloc]initWithFrame:CGRectMake(8, btn.origin.y+btn.height+16, SCREENSIZE.width-16, 0.6f)];
                line01.alpha=0.9;
                line01.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
                [goplaceView addSubview:line01];
                cityYH=line01.origin.y+line01.height+6;
            }
        }
    }
    
    
    CGFloat hanbYH=cityYH;
    if (isHadFlight) {
        //航班机票
        isSeleHbBtn=YES;
        UILabel *hbTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, cityYH, SCREENSIZE.width-16, 36)];
        hbTitle.font=kFontSize15;
        hbTitle.text=@"航班机票：";
        hbTitle.textColor=[UIColor colorWithHexString:@"#474747"];
        [goplaceView addSubview:hbTitle];
        
        hbBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        hbBtn.layer.cornerRadius=2;
        hbBtn.layer.borderWidth=1.0f;
        hbBtn.frame=CGRectMake(8, hbTitle.origin.y+hbTitle.height+4, (SCREENSIZE.width-16)/4, 34);
        hbBtn.titleLabel.font=kFontSize14;
        [hbBtn setTitle:@"往返机票" forState:UIControlStateNormal];
        hbBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
        [hbBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
        [hbBtn addTarget:self action:@selector(clickHbBtn) forControlEvents:UIControlEventTouchUpInside];
        [goplaceView addSubview:hbBtn];
        
        UIView *line01=[[UIView alloc]initWithFrame:CGRectMake(8, hbBtn.origin.y+hbBtn.height+16, SCREENSIZE.width-16, 0.6f)];
        line01.alpha=0.9;
        line01.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
        [goplaceView addSubview:line01];
        
        hanbYH=line01.origin.y+0.6f+6;
    }
    
    
    
    //酒店类型
    CGFloat jdYH=hanbYH;
    if (isHadHotel) {
        UILabel *jdTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, hanbYH, SCREENSIZE.width-16, 36)];
        jdTitle.font=kFontSize15;
        NSString *jsStr=@"套餐类型：";
        jdTitle.text=jsStr;
        jdTitle.textColor=[UIColor colorWithHexString:@"474747"];
        [goplaceView addSubview:jdTitle];
        
        CGFloat jdFloat=jdTitle.origin.y+jdTitle.height+4;
        
        //创建套餐类型按钮
        jdFloat=[self createTaoCanArray:roomArr afterView:jdTitle toSupperView:goplaceView];
        
        UIView *line02=[[UIView alloc]initWithFrame:CGRectMake(8, jdFloat+16, SCREENSIZE.width-16, 0.5f)];
        line02.alpha=0.9;
        line02.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
        [goplaceView addSubview:line02];
        
        if (isPalau) {
            //
            // 出海天数
            //
            UILabel *chhTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, line02.origin.y+line02.height+6, SCREENSIZE.width-16, 36)];
            chhTitle.font=kFontSize15;
            chhTitle.text=@"出海天数：";
            chhTitle.textColor=[UIColor colorWithHexString:@"#474747"];
            [goplaceView addSubview:chhTitle];
            //创建出海天数按钮
            jdYH=[self createCHHDaysAfterView:chhTitle toSupperView:goplaceView];
        }else{
            jdYH=line02.origin.y+line02.height;
        }
    }
    
    //
    goplaceView.frame=CGRectMake(0, view.origin.y+view.height, SCREENSIZE.width, jdYH);
    return goplaceView.origin.y+goplaceView.height+5;
}

#pragma mark - 创建套餐类型按钮
-(CGFloat)createTaoCanArray:(NSArray *)array afterView:(UILabel *)tcaView toSupperView:(UIView *)supView{
    
    CGFloat btnYH = 0.0;
    
    CGFloat btnX=0.0f;
    CGFloat btnY=tcaView.origin.y+tcaView.height+8;
    CGFloat btnW=(SCREENSIZE.width-24)/2;
    CGFloat btnH=36;
    for (int i=0; i<array.count; i++) {
        RoomModel *model=array[i];
        
        btnX=i>=2?(i-2)*X_Space+(i-2)*btnW+X_Space:i*X_Space+i*btnW+X_Space;
        btnY=i>=2?tcaView.origin.y+tcaView.height+12+btnH:tcaView.origin.y+tcaView.height+4;
        
        UIButton *yhBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        yhBtn.titleEdgeInsets=UIEdgeInsetsMake(2, 2, 2, 2);
        yhBtn.titleLabel.numberOfLines=0;
        yhBtn.layer.cornerRadius=2;
        yhBtn.layer.borderWidth=1.0f;
        NSString *labelTtt=model.tcName;
        if ([model.isCur isEqualToString:@"1"]) {
            yhBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
            [yhBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
            selectedTCBtn=yhBtn;
            isSeleTC=YES;
            roomId=model.roomId;
            handleTaocanPrice=model.price.floatValue;
        }else{
            yhBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
            [yhBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
        }
        
        yhBtn.frame=CGRectMake(btnX, btnY, btnW, btnH);
        yhBtn.titleLabel.font=kFontSize13;
        yhBtn.tag=i;
        
        if (i==array.count-1) {
            btnYH=btnY+yhBtn.height;
        }
        
        //只有一个时不能点击
        if(array.count>1){
            objc_setAssociatedObject(yhBtn, "TaocanModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [yhBtn addTarget:self action:@selector(clickTaocanBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        //
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:2];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        yhBtn.titleLabel.attributedText = attributedString;
        yhBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        //
        [yhBtn setTitle:labelTtt forState:UIControlStateNormal];
        
        [supView addSubview:yhBtn];
    }
    return btnYH;
}

#pragma mark - 点击套餐类型按钮
-(void)clickTaocanBtn:(id)sender{
    
    selectedTCBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
    [selectedTCBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
    selectedTCBtn.enabled=YES;
    
    UIButton *btn=sender;
    RoomModel *model = objc_getAssociatedObject(btn, "TaocanModel");
    handleTaocanPrice=model.price.floatValue;
    roomId=model.roomId;
    
    btn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    [btn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    selectedTCBtn=btn;
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - init 出海
-(CGFloat)createCHHDaysAfterView:(UIView *)afView toSupperView:(UIView *)supview{
    CGFloat btnX=0.0f;
    CGFloat btnY=afView.origin.y+afView.height+4;
    CGFloat btnW=(SCREENSIZE.width-24)/2;
    CGFloat btnHeight=60;
    
    CGFloat returnFloat=0;
    for (int i=0; i<4; i++) {
        
        btnX=i>=2?(i-2)*X_Space+(i-2)*btnW+X_Space:i*X_Space+i*btnW+X_Space;
        btnY=i>=2?afView.origin.y+afView.height+12+btnHeight:afView.origin.y+afView.height+4;
        UIButton *chhBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        chhBtn.titleEdgeInsets=UIEdgeInsetsMake(2, 2, 2, 2);
        chhBtn.titleLabel.numberOfLines=0;
        chhBtn.layer.cornerRadius=2;
        chhBtn.layer.borderWidth=1.0f;
        dayOfCH=@"0";   //默认
        handleChuHaiPrice=0;
        chhBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
        [chhBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
        
        chhBtn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        CGFloat btnFont=0.0f;
        if (IS_IPHONE5) {
            btnFont=12;
        }else{
            btnFont=14;
        }
        chhBtn.titleLabel.font=[UIFont systemFontOfSize:btnFont];
        chhBtn.tag=i;
        
        //
        NSString *labelTtt;
        //帕劳出海固定的
        if (i==0) {
            labelTtt=@"3天出海\n￥2699(含税)";
        }else if (i==1){
            labelTtt=@"2天出海\n￥2299(含税)";
        }else if (i==2){
            labelTtt=@"3天出海\n￥1999(不含税)";
        }else{
            labelTtt=@"2天出海\n￥1699(不含税)";
        }
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:6];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        chhBtn.titleLabel.attributedText = attributedString;
        chhBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        //
        [chhBtn setTitle:labelTtt forState:UIControlStateNormal];
        [chhBtn addTarget:self action:@selector(clickCHDaysBtn:) forControlEvents:UIControlEventTouchUpInside];
        if (i==3) {
            UIView *chdline=[[UIView alloc]initWithFrame:CGRectMake(0, chhBtn.origin.y+chhBtn.height+12, SCREENSIZE.width, 0.5f)];
            chdline.alpha=0.9;
            chdline.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
            [supview addSubview:chdline];
            returnFloat=chdline.origin.y+chdline.height+4;
        }
        [supview addSubview:chhBtn];
    }
    return returnFloat;
}

#pragma mark - 点击出海天数
-(void)clickCHDaysBtn:(id)sender{
    
    UIButton *btn=sender;
    
    chhBtnTag=btn.tag;
    
    selectedCHHDayBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
    [selectedCHHDayBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
    
    if (selectedCHHDayBtn.tag!=btn.tag) {
        isSeleChuHai=NO;
    }
    
    //
    if (isSeleChuHai) {
        btn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
        [btn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
        dayOfCH=@"0";
        handleChuHaiPrice=0;
    }else{
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
        [btn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
        dayOfCH=[NSString stringWithFormat:@"%ld",btn.tag+1];
        switch (btn.tag) {
            case 0:
                handleChuHaiPrice=2699;
                break;
            case 1:
                handleChuHaiPrice=2299;
                break;
            case 2:
                handleChuHaiPrice=1999;
                break;
            case 3:
                handleChuHaiPrice=1699;
                break;
            default:
                break;
        }
    }
    
    isSeleChuHai=!isSeleChuHai;
    
    selectedCHHDayBtn=btn;
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - 点击赛选月份按钮（左边、右边）
-(void)clickLeftFillDateBtn{
    cRightBtn.enabled=YES;
    
    seleMonthIndex--;
    
    MonthModel *model=monthArr[seleMonthIndex];
    yearLabel.text=[NSString stringWithFormat:@"%@年%@月",model.yearStr,model.monthStr];
    selectMonthModel=model;
    for (UIView *view in calendarView.subviews) {
        if (view==grideView) {
            [grideView removeFromSuperview];
        }
    }
    [self updateGrideHeightWithModel:selectMonthModel];
    
    if (seleMonthIndex==0) {
        cLeftBtn.enabled=NO;
    }
}

-(void)clickRightFillDateBtn{
    
    cLeftBtn.enabled=YES;
    
    seleMonthIndex++;
    
    MonthModel *model=monthArr[seleMonthIndex];
    yearLabel.text=[NSString stringWithFormat:@"%@年%@月",model.yearStr,model.monthStr];
    selectMonthModel=model;
    for (UIView *view in calendarView.subviews) {
        if (view==grideView) {
            [grideView removeFromSuperview];
        }
    }
    [self updateGrideHeightWithModel:selectMonthModel];
    
    if (seleMonthIndex==monthArr.count-1) {
        cRightBtn.enabled=NO;
    }
}

#pragma mark - 创建月份、天数
-(CGFloat)setGrideDayWithModel:(MonthModel *)model{
    NSMutableArray *dayArr=[NSMutableArray array];
    for (NSDictionary *dic in model.canSeleDayArr) {
        NSString *dayStr=[NSString stringWithFormat:@"%@",dic[@"d"]];
        [dayArr addObject:dayStr];
        
    }
    //天数View
    grideView=[[UIView alloc]initWithFrame:CGRectMake(0, weekView.origin.y+weekView.height+4, SCREENSIZE.width, calendarView.height-weekView.origin.y+weekView.height)];
    grideView.backgroundColor=[UIColor whiteColor];
    [calendarView insertSubview:grideView belowSubview:weekView];
    
    CGFloat lastBtnYH=0;
    //Day Button
    CGFloat griBtnW=(SCREENSIZE.width-24)/7;    //按钮宽度
    CGFloat griBtnH=griBtnW;                    //按钮高度
    CGFloat firstBtnX=(model.firstweek.intValue*griBtnW)+(model.firstweek.intValue)*Btn_Margin;                             //第一个按钮x轴
    CGFloat y_margin=Btn_Y_Margin;              //y轴间距
    for (int j=0; j<model.daySum.intValue; j++) {
        CustomTravelDateButton *grideBtn=[[CustomTravelDateButton alloc] init];
        grideBtn.backgroundColor=[UIColor whiteColor];
        grideBtn.layer.cornerRadius=2;
        grideBtn.layer.borderWidth=0.8f;
        grideBtn.layer.borderColor=[UIColor colorWithHexString:@"#d1d1d1"].CGColor;
        grideBtn.enabled=NO;
        //默认灰色
        grideBtn.dateLabel.textColor=[UIColor colorWithHexString:@"#dadada"];
        grideBtn.backgroundColor=[UIColor whiteColor];
        objc_setAssociatedObject(grideBtn, "Month_DayModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        if (firstBtnX+Btn_Margin+j*griBtnW+j*Btn_Margin<SCREENSIZE.width) {
            grideBtn.frame=CGRectMake(firstBtnX+Btn_Margin+j*griBtnW+j*Btn_Margin, y_margin, griBtnW, griBtnH);
        }
        else{
            //y表示第几列
            int y=((firstBtnX+Btn_Margin*Btn_Y_Margin+j*griBtnW+j*Btn_Margin+j/DayOfWeek+Btn_Margin)/SCREENSIZE.width);
            y_margin=y*griBtnH+y*Btn_Y_Margin+Btn_Y_Margin;   //重新设置y轴
            CGFloat xs=(j+model.firstweek.intValue)%DayOfWeek;//xs表示第几行
            grideBtn.frame=CGRectMake(xs*griBtnW+xs*Btn_Margin+Btn_Margin, y_margin, griBtnW, griBtnH);
            
            //获取最后按钮的位置
            if(j==model.daySum.intValue-1){
                lastBtnYH=y_margin+griBtnH;
            }
        }
        
        grideBtn.dateLabel.font=kFontSize15;
        grideBtn.dateLabel.text=[NSString stringWithFormat:@"%d",j+1];
        
        for (NSString *str in dayArr) {
            
            //可用的日期day
            if ([str isEqualToString:[NSString stringWithFormat:@"%d",j+1]]) {
                
                grideBtn.priceLabel.font=kFontSize11;
//                grideBtn.priceLabel.text=@"￥1299";
                grideBtn.residueLabel.font=kFontSize10;
//                grideBtn.residueLabel.text=@"余2";
                
                grideBtn.tag=j+1;
                
                //可用的按钮设置
                grideBtn.dateLabel.textColor=[UIColor colorWithHexString:@"#8f8f8f"];
                grideBtn.priceLabel.textColor=[UIColor colorWithHexString:@"#ff5741"];
                grideBtn.residueLabel.textColor=[UIColor lightGrayColor];
                grideBtn.layer.borderColor=[UIColor colorWithHexString:@"#d1d1d1"].CGColor;
                grideBtn.enabled=YES;
                [grideBtn addTarget:self action:@selector(seleDayBtn:) forControlEvents:UIControlEventTouchUpInside];
                
                NSString *defaultDayStr=[dayArr objectAtIndex:0];
                if (model.isDefault) {
                    if([str isEqualToString:defaultDayStr]){
                        //默认的按钮选中设置
                        grideBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
                        grideBtn.dateLabel.textColor=[UIColor whiteColor];
                        grideBtn.priceLabel.textColor=[UIColor whiteColor];
                        grideBtn.residueLabel.textColor=[UIColor whiteColor];
                        selectedDayBtn=grideBtn;
                        grideBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
                        seleDateStr=[NSString stringWithFormat:@"%@-%@-%ld",model.yearStr,model.monthStr,(long)j+1];
                        showSeleDate=[NSString stringWithFormat:@"%@年%@月%ld日",model.yearStr,model.monthStr,(long)j+1];
                        NSString *ssdStr=[NSString stringWithFormat:@" ● 已选日期：%@",showSeleDate];
                        [seleDateLabel setText:ssdStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                            NSRange range01 = [[mutableAttributedString string] rangeOfString:@"●" options:NSCaseInsensitiveSearch];
                            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff5741"] CGColor] range:range01];
                            NSRange range02 = [[mutableAttributedString string] rangeOfString:@"已选日期：" options:NSCaseInsensitiveSearch];
                            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#979797"] CGColor] range:range02];
                            return mutableAttributedString;
                        }];
                    }
                }else{
                    if (j==0) {
                        grideBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
                        grideBtn.dateLabel.textColor=[UIColor whiteColor];
                        grideBtn.priceLabel.textColor=[UIColor whiteColor];
                        grideBtn.residueLabel.textColor=[UIColor whiteColor];
                        selectedDayBtn=grideBtn;
                        grideBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
                        seleDateStr=[NSString stringWithFormat:@"%@-%@-%ld",model.yearStr,model.monthStr,(long)j+1];
                        showSeleDate=[NSString stringWithFormat:@"%@年%@月%ld日",model.yearStr,model.monthStr,(long)j+1];
                        NSString *ssdStr=[NSString stringWithFormat:@" ● 已选日期：%@",showSeleDate];
                        [seleDateLabel setText:ssdStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                            NSRange range01 = [[mutableAttributedString string] rangeOfString:@"●" options:NSCaseInsensitiveSearch];
                            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff5741"] CGColor] range:range01];
                            NSRange range02 = [[mutableAttributedString string] rangeOfString:@"已选日期：" options:NSCaseInsensitiveSearch];
                            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#979797"] CGColor] range:range02];
                            return mutableAttributedString;
                        }];
                    }
                }
            }
        }
        
        [grideView addSubview:grideBtn];
    }
    
    CGRect frame=grideView.frame;
    frame.size.height=lastBtnYH+8;
    grideView.frame=frame;
    return grideView.height;
}

#pragma mark - 点击哪天
-(void)seleDayBtn:(id)sender{
    selectedDayBtn.backgroundColor=[UIColor whiteColor];
    selectedDayBtn.dateLabel.textColor=[UIColor colorWithHexString:@"#8f8f8f"];
    selectedDayBtn.priceLabel.textColor=[UIColor colorWithHexString:@"#ff5741"];
    selectedDayBtn.residueLabel.textColor=[UIColor lightGrayColor];
    selectedDayBtn.layer.borderColor=[UIColor colorWithHexString:@"#d1d1d1"].CGColor;
    selectedDayBtn.enabled=YES;
    
    CustomTravelDateButton *button=sender;
    MonthModel *model = objc_getAssociatedObject(button, "Month_DayModel");
    seleDateStr=[NSString stringWithFormat:@"%@-%@-%ld",model.yearStr,model.monthStr,(long)button.tag];
    showSeleDate=[NSString stringWithFormat:@"%@年%@月%ld日",model.yearStr,model.monthStr,(long)button.tag];
    NSString *ssdStr=[NSString stringWithFormat:@"● 已选日期：%@",showSeleDate];
    [seleDateLabel setText:ssdStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange range01 = [[mutableAttributedString string] rangeOfString:@"●" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ff5741"] CGColor] range:range01];
        NSRange range02 = [[mutableAttributedString string] rangeOfString:@"已选日期：" options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#979797"] CGColor] range:range02];
        return mutableAttributedString;
    }];
    button.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
    button.dateLabel.textColor=[UIColor whiteColor];
    button.priceLabel.textColor=[UIColor whiteColor];
    button.residueLabel.textColor=[UIColor whiteColor];
    button.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    button.enabled=NO;
    
    selectedDayBtn=button;
}

//更新高度
-(void)updateGrideHeightWithModel:(MonthModel *)model{
    
    CGRect frame=calendarView.frame;
    
    CGFloat lastY=[self setGrideDayWithModel:model];
    
    frame.size.height=lastY+weekView.origin.y+weekView.height+36;
    
    calendarView.frame=frame;
    
    seleDateLabel.frame=CGRectMake(2, calendarView.height-36, SCREENSIZE.width-4, 36);
    
    //===============必须加上beginUpdates================
    //===============必须加上endUpdates==================
    [myTable beginUpdates];
    myTable.tableHeaderView=calendarView;
    [myTable endUpdates];
}

#pragma mark - 初始化 UI
-(void)initUI{
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    myTable.sectionFooterHeight=0;
    myTable.showsVerticalScrollIndicator=NO;
    myTable.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:myTable];
    
    myTable.tableHeaderView=[self setUpCalendarView];
    
    //
    //footView
    //
    footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 800)];
    footView.backgroundColor=[UIColor whiteColor];
    
    UIView *fline02=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 0.5f)];
    fline02.alpha=0.9;
    fline02.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [footView addSubview:fline02];
    
    
    //出游人群
    UILabel *cyTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, fline02.origin.y+8, SCREENSIZE.width-16, 36)];
    cyTitle.font=kFontSize15;
    cyTitle.text=@"出游人数：";
    cyTitle.textColor=[UIColor colorWithHexString:@"#474747"];
    [footView addSubview:cyTitle];
    
    myStepper = [[PKYStepper alloc] initWithFrame:CGRectMake(8, cyTitle.origin.y+cyTitle.height+4, 136, 34)];
    myStepper.countLabel.textColor=[UIColor colorWithHexString:@"#ff5741"];
    [myStepper setBorderColor:[UIColor colorWithHexString:@"#cbcbcb"]];
    [myStepper setButtonTextColor:[UIColor colorWithHexString:@"#acacac"] forState:UIControlStateNormal];
    myStepper.countLabel.font=kFontSize15;
    myStepper.value=1;
    myStepper.minimum=1;
    myStepper.maximum=50;
    __weak typeof(self) weakSelf = self;
    myStepper.valueChangedCallback = ^(PKYStepper *stepper, float count) {
        per_num=count;
        stepper.countLabel.text = [NSString stringWithFormat:@"%@", @(count)];
        
        [weakSelf updatePersonCount];
    };
    [myStepper setup];
    [footView addSubview:myStepper];
    
    //成人价格
    crLabel=[TTTAttributedLabel new];
    crLabel.textAlignment=NSTextAlignmentRight;
    [footView addSubview:crLabel];
    crLabel.font=kFontSize14;
    crLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* crLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[myStepper]-8-[crLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(myStepper,crLabel)];
    [NSLayoutConstraint activateConstraints:crLabel_h];
    NSArray* crLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cyTitle]-4-[crLabel(34)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cyTitle,crLabel)];
    [NSLayoutConstraint activateConstraints:crLabel_w];
    
    //如果选择出发地
    NSString *stageStr;
    if (seletedGoPlaceBtn) {
        CGFloat roomPriceFloat=roomPrice.floatValue;
        CGFloat cityPriceFloat=seletCityPrice.floatValue;
        CGFloat totalPrice=roomPriceFloat+cityPriceFloat;
        NSString *totalPriceStr;
        if(isPalau){
            if (isSeleChuHai) {
                totalPriceStr=[NSString stringWithFormat:@"%.f",totalPrice+2699];
            }else{
                totalPriceStr=[NSString stringWithFormat:@"%.f",totalPrice];
            }
        }else{
            totalPriceStr=[NSString stringWithFormat:@"%.f",totalPrice];
        }
        //初始化时
        stageStr=[NSString stringWithFormat:@"成人 ￥%@/人",totalPriceStr];
        [crLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@/人",totalPriceStr] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
            //设置选择文本的大小
            UIFont *systemFont = kFontSize15;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        NSString *ttmp;
        if (isPalau) {
            if (isSeleChuHai) {
                ttmp=[NSString stringWithFormat:@"%.f",roomPrice.floatValue+2699];
            }else{
                ttmp=[NSString stringWithFormat:@"%.f",roomPrice.floatValue];
            }
        }else{
            ttmp=[NSString stringWithFormat:@"%.f",roomPrice.floatValue];;
        }
        stageStr=[NSString stringWithFormat:@"成人 ￥%@/人",ttmp];
        [crLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@/人",ttmp] options:NSCaseInsensitiveSearch];
            [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
            //设置选择文本的大小
            UIFont *systemFont = kFontSize15;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
    crLabelString=crLabel.text;
    
    UIView *gtrSLine=[[UIView alloc] initWithFrame:CGRectMake(8, myStepper.origin.y+myStepper.height+16, SCREENSIZE.width-16, 0.5f)];
    gtrSLine.alpha=0.9;
    gtrSLine.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [footView addSubview:gtrSLine];
    
    
    //
    //  优惠券
    //
    UILabel *couponLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, gtrSLine.origin.y+11, SCREENSIZE.width-16, 36)];
    couponLabel.font=kFontSize15;
    NSString *fqTStr=@"优惠券：";
    couponLabel.text=fqTStr;
    couponLabel.textColor=[UIColor colorWithHexString:@"#474747"];
    [footView addSubview:couponLabel];
    
    UIView *cpLine=[[UIView alloc] initWithFrame:CGRectMake(8, couponLabel.origin.y+couponLabel.height+12, SCREENSIZE.width-16, 0.5f)];
    cpLine.alpha=0.9;
    cpLine.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [footView addSubview:cpLine];
    
    
    //
    cpPriceBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cpPriceBtn.imageEdgeInsets=UIEdgeInsetsMake(8, 110, 8, 0);
    cpPriceBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [cpPriceBtn setImage:[UIImage imageNamed:@"safe4"] forState:UIControlStateNormal];
    [cpPriceBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    cpPriceBtn.titleLabel.font=kFontSize14;
    [cpPriceBtn setTitle:@"不使用" forState:UIControlStateNormal];
    [footView addSubview:cpPriceBtn];
    cpPriceBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* arrowBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cpPriceBtn(120)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cpPriceBtn)];
    [NSLayoutConstraint activateConstraints:arrowBtn_h];
    NSString *vflW=[NSString stringWithFormat:@"V:[gtrSLine]-5-[cpPriceBtn]-5-[cpLine]"];
    NSArray* arrowBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:vflW options:0 metrics:nil views:NSDictionaryOfVariableBindings(gtrSLine,cpPriceBtn,cpLine)];
    [NSLayoutConstraint activateConstraints:arrowBtn_w];
    [cpPriceBtn addTarget:self action:@selector(goToUseCoupon) forControlEvents:UIControlEventTouchUpInside];
    
    //
    //  游侠信用卡优惠特权
    //
    CGFloat tcY=cpLine.origin.y+cpLine.height+8;
    if (isVIP) {
        
        UILabel *xykLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, tcY, SCREENSIZE.width-16, 36)];
        xykLabel.font=kFontSize15;
        xykLabel.textColor=[UIColor colorWithHexString:@"#474747"];
        xykLabel.text=@"游侠信用卡优惠特权（2选1）";
        [footView addSubview:xykLabel];
        
        CGFloat xykBtnYH=[self createXYKBtnAfterView:xykLabel];
        
        UIView *xykLine=[[UIView alloc]initWithFrame:CGRectMake(8, xykBtnYH+8, SCREENSIZE.width-16, 0.5f)];
        xykLine.alpha=0.9;
        xykLine.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
        [footView addSubview:xykLine];
        
        tcY=xykLine.origin.y+xykLine.height;
    }
    
    //
    // 使用分期
    //
    CGFloat ustBtnY=tcY+2;
    if (isHadFenqi) {
        
        fqTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, ustBtnY, SCREENSIZE.width-16, 36)];
        fqTitle.font=kFontSize15;
        fqTitle.text=@"使用分期：";
        fqTitle.textColor=[UIColor colorWithHexString:@"#474747"];
        [footView addSubview:fqTitle];
        
        ustBtnY=[self createUseStageButtonWithArray:useStageArr];
    }
    
    //蓝色条
    UIView *blueView02=[[UIView alloc] initWithFrame:CGRectMake(0, ustBtnY+4, SCREENSIZE.width, 12)];
    blueView02.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    [footView addSubview:blueView02];
    
    //填写预订人信息
    UILabel *ydrTitle=[[UILabel alloc]initWithFrame:CGRectMake(X_Space, blueView02.origin.y+blueView02.height+12, SCREENSIZE.width-16, 36)];
    ydrTitle.font=kFontSize15;
    ydrTitle.text=@"填写预定人信息：";
    ydrTitle.textColor=[UIColor colorWithHexString:@"#474747"];
    [footView addSubview:ydrTitle];
    
    UIView *phLine01=[[UIView alloc]initWithFrame:CGRectMake(0, ydrTitle.origin.y+ydrTitle.height+8, SCREENSIZE.width, 0.5f)];
    phLine01.alpha=0.9;
    phLine01.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [footView addSubview:phLine01];
    
    //填写手机号码
    UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(X_Space, phLine01.origin.y+phLine01.height+6, 80, 32)];
    phoneLabel.font=kFontSize15;
    phoneLabel.text=@"手机号码：";
    phoneLabel.textColor=[UIColor colorWithHexString:@"#8d8d8d"];
    [footView addSubview:phoneLabel];
    
    phoneTF=[[UITextField alloc] initWithFrame:CGRectMake(phoneLabel.origin.x+phoneLabel.width, phoneLabel.origin.y, SCREENSIZE.width-(phoneLabel.origin.x+phoneLabel.width+8), phoneLabel.height)];
    phoneTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    phoneTF.font=kFontSize15;
    phoneTF.placeholder=@"请填写您的手机号码";
    phoneTF.keyboardType= UIKeyboardTypePhonePad;
    [phoneTF addTarget:self action:@selector(phoneDidChange:) forControlEvents:UIControlEventEditingChanged];
    [footView addSubview:phoneTF];
    
    UIView *phLine02=[[UIView alloc]initWithFrame:CGRectMake(0, phoneLabel.origin.y+phoneLabel.height+6, SCREENSIZE.width, 0.5f)];
    phLine02.alpha=0.9;
    phLine02.backgroundColor=[UIColor colorWithHexString:@"#c2c2c2"];
    [footView addSubview:phLine02];
    
    //重新frame
    CGRect frame=footView.frame;
    frame.size.height=phLine02.origin.y+phLine02.height+36;
    footView.frame=frame;
    
    weakSelf.ButtonPriceBlock = ^(CGFloat buttonPrice){
        [self resetButtonPrice:buttonPrice];
    };
    myTable.tableFooterView=footView;
}

#pragma mark - textfield
- (void)phoneDidChange:(id) sender {
    UITextField *ptext=sender;
    if (![ptext.text isEqualToString:@""]&&
        [AppUtils checkPhoneNumber:ptext.text]) {
        submitOrderBtn.backgroundColor=[UIColor colorWithHexString:@"#ff5741"];
        submitOrderBtn.enabled=YES;
    }else{
        submitOrderBtn.backgroundColor=[UIColor lightGrayColor];
        submitOrderBtn.enabled=NO;
    }
}

#pragma mark - 点击使用优惠券
-(void)goToUseCoupon{
    userCouponCtr.kCouponBlock = ^(id model){
        UseCopuonModel *receiveCouponModel=model;
        seleCouponId=receiveCouponModel.couponID;
        seleCouponPrice=receiveCouponModel.couponMoney;
        NSRange rrang=[receiveCouponModel.couponNumber rangeOfString:@"YX"];
        isYXCoupon=rrang.location!=NSNotFound?YES:NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SelctedCouponModel" object:model];
    };
    [self.navigationController pushViewController:userCouponCtr animated:YES];
}

#pragma mark - 更新优惠券
-(void)getCouponModel:(NSNotification *)noti{
    
    rcvCouponModel=noti.object;
    
    //处理优惠券价格
    [self handleCouponPriceWithCouponId:rcvCouponModel.couponID model:rcvCouponModel];
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - 创建信用卡按钮
-(CGFloat)createXYKBtnAfterView:(UILabel *)label{
    CGFloat btnYH = 0.0;
    
    CGFloat btnX=0.0f;
    CGFloat btnY=label.origin.y+label.height+6;
    CGFloat btnW=(footView.width-32)/3+24;
    for (int i=0; i<2; i++) {
        btnX=i*X_Space+i*btnW+X_Space;
        UIButton *yhBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        yhBtn.titleEdgeInsets=UIEdgeInsetsMake(2, 2, 2, 2);
        yhBtn.titleLabel.numberOfLines=0;
        yhBtn.layer.cornerRadius=2;
        yhBtn.layer.borderWidth=1.0f;
        NSString *labelTtt;
        if (i==0) {
            labelTtt=[NSString stringWithFormat:@"享98折\n优惠"];
            yhBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
            [yhBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
            selectedXYKBtn=yhBtn;
            vipPerem=@"1";
            yhBtn.enabled=NO;
        }else{
            labelTtt=[NSString stringWithFormat:@"满立减\n10000减300\n30000减1000\n50000减3000"];
            yhBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
            [yhBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
        }
        yhBtn.frame=CGRectMake(btnX, btnY, btnW, 84);
        yhBtn.titleLabel.font=kFontSize13;
        yhBtn.tag=i;
        btnYH=btnY+yhBtn.height+12;
        //
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:2];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        yhBtn.titleLabel.attributedText = attributedString;
        yhBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        //
        [yhBtn setTitle:labelTtt forState:UIControlStateNormal];
        [yhBtn addTarget:self action:@selector(clickXYKBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:yhBtn];
    }
    return btnYH;
}

#pragma mark - 点击信用卡优惠特权（2选1）
-(void)clickXYKBtn:(id)sender{
    selectedXYKBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
    [selectedXYKBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
    selectedXYKBtn.enabled=YES;
    
    UIButton *btn=sender;
    if (btn.tag==0) {
        is98zhe=YES;
        hyNumber=@"1";
        vipPerem=@"1";
    }else{
        is98zhe=NO;
        hyNumber=@"2";
        vipPerem=@"2";
    }
    
    btn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    [btn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    selectedXYKBtn=btn;
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - +-出游人数更新数据
-(void)updatePersonCount{
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
    
    //处理优惠券价格
    [self handleCouponPriceWithCouponId:seleCouponId model:rcvCouponModel];
}

#pragma mark - 处理优惠券价格方法
-(void)handleCouponPriceWithCouponId:(NSString *)sCouponId model:(UseCopuonModel *)model{
    if ([sCouponId isEqualToString:@"0"]) {
        handleCouponPrice=0.0f;
        [cpPriceBtn setTitle:@"不使用" forState:UIControlStateNormal];
    }else{
        //订单总金额*人数
        CGFloat crpTotFl=handleAdultPrice*per_num;
        //优惠券金额不能大于订单总金额的10%
        CGFloat fhaCoup=0;
        if (model.isQuane) {
            if (model.couponMoney.floatValue>crpTotFl) {
                fhaCoup=crpTotFl;
            }else{
                fhaCoup=model.couponMoney.floatValue;
            }
        }else{
            if (model.couponMoney.floatValue>crpTotFl*0.1) {
                fhaCoup=crpTotFl*0.1;
            }else{
                fhaCoup=model.couponMoney.floatValue;
            }
        }
        
        NSString *fhaCoupStr=[NSString stringWithFormat:@"%.2f",fhaCoup];
        NSRange fhaCoupRang=[fhaCoupStr rangeOfString:@"."];
        if (fhaCoupRang.location!=NSNotFound) {
            fhaCoupStr=[fhaCoupStr substringToIndex:fhaCoupRang.location];
        }
        handleCouponPrice=fhaCoupStr.floatValue;
        [cpPriceBtn setTitle:[NSString stringWithFormat:@"￥-%.f",handleCouponPrice] forState:UIControlStateNormal];
    }
}

#pragma mark - 创建分期按钮
-(CGFloat)createUseStageButtonWithArray:(NSMutableArray *)array{
    CGFloat btnX=0.0f;
    CGFloat fqTitleEndY=fqTitle.origin.y+fqTitle.height+4;
    CGFloat btnY=0.0f;
    CGFloat btnW=(SCREENSIZE.width-24)/2;
    CGFloat btnHeight=64;
    
    CGFloat returnFloat=0;
    for (int i=0; i<array.count; i++) {
        UseStageModel *model=array[i];
        
        //费率
        NSString *rateStr;
        if ([[AppUtils getValueWithKey:VIP_Flag] isEqualToString:@"1"]) {
            rateStr=@"0";
            rate01=0;
            rate03=0;
            rate06=0;
            rate09=0;
            rate12=0;
        }else{
            rateStr=rateArr[i];
            if (i==0) {
                rate01=rateStr.floatValue;
            }else if (i==1){
                rate03=rateStr.floatValue;
            }else if (i==2){
                rate06=rateStr.floatValue;
            }else if (i==3){
                rate09=rateStr.floatValue;
            }else if (i==4){
                rate12=rateStr.floatValue;
            }
        }
        
        //x轴
        btnX=i%2==0?X_Space:2*X_Space+btnW; //偶数奇数算出来
        
        //numOfRow表示第几列
        int numOfRow=i/2;
        btnY=numOfRow<=0?fqTitleEndY+numOfRow*btnHeight:fqTitleEndY+numOfRow*btnHeight+numOfRow*8;
 
        UIButton *fqBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        fqBtn.titleEdgeInsets=UIEdgeInsetsMake(2, 2, 2, 2);
        fqBtn.titleLabel.numberOfLines=0;
        fqBtn.layer.cornerRadius=2;
        fqBtn.layer.borderWidth=1.0f;
        dayOfCH=@"0";
        if (model.isSele) {
            fqBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
            [fqBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
            fqBtn.enabled=NO;
            selectedUseStageBtn=fqBtn;
            stage_sum=@"1";
        }else{
            fqBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
            [fqBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
        }
        fqBtn.frame=CGRectMake(btnX, btnY, btnW, btnHeight);
        CGFloat btnFont=0.0f;
        if (IS_IPHONE5) {
            btnFont=12;
        }else{
            btnFont=14;
        }
        fqBtn.titleLabel.font=[UIFont systemFontOfSize:btnFont];
        fqBtn.tag=i;
        
        //
        //默认的
        NSString *crlPrice=crLabel.text;
        NSRange crlRang=[crlPrice rangeOfString:@"￥"];
        if (crlRang.location != NSNotFound) {
            crlPrice=[crlPrice substringFromIndex:crlRang.location+crlRang.length];
        }
        CGFloat roomPriceFloat=crlPrice.floatValue;
        
        CGFloat lxRate=rateStr.floatValue*roomPriceFloat*per_num;
        NSString *lxfPrice=[NSString stringWithFormat:@"%.2f",lxRate];
        NSRange lxfRang=[lxfPrice rangeOfString:@"."];
        if (lxfRang.location != NSNotFound) {
            lxfPrice=[lxfPrice substringToIndex:lxfRang.location];
        }
        
        //
        NSString *labelTtt=[NSString stringWithFormat:@"￥%@起x%@期\n(手续费%@元)",model.stagePrice,model.stageSum,lxfPrice];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:2];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        fqBtn.titleLabel.attributedText = attributedString;
        fqBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        //
        [fqBtn setTitle:labelTtt forState:UIControlStateNormal];
        [fqBtn addTarget:self action:@selector(clickFqBtn:) forControlEvents:UIControlEventTouchUpInside];
        if (i==array.count-1) {
            returnFloat=fqBtn.origin.y+fqBtn.height+12;
        }
        
        //
        if (i==0) {
            useStageBtn01=fqBtn;
        }else if (i==1){
            useStageBtn02=fqBtn;
        }else if (i==2){
            useStageBtn03=fqBtn;
        }else if (i==3){
            useStageBtn04=fqBtn;
        }else{
            useStageBtn05=fqBtn;
        }
        [footView addSubview:fqBtn];
    }
    return returnFloat;
}

#pragma mark - （重要方法---）TotalPriceBlock method
- (void)resetButtonPrice:(CGFloat)buttonPrice{
    
    //设置成人Label
    NSString *stageStr=[NSString stringWithFormat:@"成人 ￥%.f/人",buttonPrice];
    [crLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%.f/人",buttonPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ec6b00"] CGColor] range:sumRange];
        UIFont *systemFont = kFontSize15;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    
    
    //分期按钮1
    /*
        按钮显示价格 = isVIP ?（成人价格 * 人数 - 优惠券）* 0.98 或 满立减逻辑 ：
                            （成人价格 * 人数 - 优惠券）
     */
    CGFloat stHandleFloat=isYXCoupon?buttonPrice*per_num-handleCouponPrice:buttonPrice*per_num;
    if(isVIP){
        if([hyNumber isEqualToString:@"1"]){
            stHandleFloat=stHandleFloat*0.98;
        }else{
            if (stHandleFloat>=50000) {
                stHandleFloat=stHandleFloat-3000;
            }else if (stHandleFloat>=30000){
                stHandleFloat=stHandleFloat-1000;
            }else if (stHandleFloat>=10000){
                stHandleFloat=stHandleFloat-300;
            }else{
                stHandleFloat=stHandleFloat;
            }
        }
    }
    NSString *bprice01=[NSString stringWithFormat:@"%.2f",stHandleFloat];
    NSRange rang01=[bprice01 rangeOfString:@"."];
    if (rang01.location != NSNotFound) {
        bprice01=[bprice01 substringToIndex:rang01.location];
    }
    
    NSString *labelTtt01=[NSString stringWithFormat:@"￥%@起x1期\n(手续费0元)",bprice01];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt01];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:6];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt01 length])];
    useStageBtn01.titleLabel.attributedText = attributedString;
    useStageBtn01.titleLabel.textAlignment=NSTextAlignmentCenter;
    [useStageBtn01 setTitle:labelTtt01 forState:UIControlStateNormal];
    
    
    //分期按钮2
    NSString *bprice02=[NSString stringWithFormat:@"%.2f",stHandleFloat/3];
    NSRange rang02=[bprice02 rangeOfString:@"."];
    if (rang02.location != NSNotFound) {
        bprice02=[bprice02 substringToIndex:rang02.location];
    }
    
    NSString *labelTtt02=[NSString stringWithFormat:@"￥%@起x3期\n(手续费0元)",bprice02];

    NSMutableAttributedString *attributedString02 = [[NSMutableAttributedString alloc] initWithString:labelTtt02];
    NSMutableParagraphStyle *paragraphStyle02 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle02 setLineSpacing:6];//调整行间距
    [attributedString02 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle02 range:NSMakeRange(0, [labelTtt02 length])];
    useStageBtn02.titleLabel.attributedText = attributedString02;
    useStageBtn02.titleLabel.textAlignment=NSTextAlignmentCenter;
    [useStageBtn02 setTitle:labelTtt02 forState:UIControlStateNormal];
    
    
    //分期按钮3
    NSString *bprice03=[NSString stringWithFormat:@"%.2f",stHandleFloat/6];
    NSRange rang03=[bprice03 rangeOfString:@"."];
    if (rang03.location != NSNotFound) {
        bprice03=[bprice03 substringToIndex:rang03.location];
    }
    
    CGFloat fqFee03=0;
    if (isVIP) {
        fqFee03=0;
    }else{
        fqFee03=stHandleFloat*rate06;
    }
    
    NSString *fqFeeStraa03=[NSString stringWithFormat:@"%.2f",fqFee03];
    NSRange fqFeeRang03=[fqFeeStraa03 rangeOfString:@"."];
    if (fqFeeRang03.location != NSNotFound) {
        fqFeeStraa03=[fqFeeStraa03 substringToIndex:fqFeeRang03.location];
    }
    
    CGFloat dxPrice03=0;
    if (!isYXCoupon) {
        dxPrice03=fqFeeStraa03.floatValue<handleCouponPrice?fqFeeStraa03.floatValue:handleCouponPrice;
    }
    
    NSString *labelTtt03=isYXCoupon?[NSString stringWithFormat:@"￥%@起x6期\n(手续费%@元)",bprice03,fqFeeStraa03]:[NSString stringWithFormat:@"￥%@起x6期\n(手续费%@元)\n(抵消手续费%.f元)",bprice03,fqFeeStraa03,dxPrice03];
    NSMutableAttributedString *attributedString03 = [[NSMutableAttributedString alloc] initWithString:labelTtt03];
    NSMutableParagraphStyle *paragraphStyle03 = [[NSMutableParagraphStyle alloc] init];
    //调整行间距
    if (isYXCoupon) {
        [paragraphStyle03 setLineSpacing:4];
    }
    [attributedString03 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle03 range:NSMakeRange(0, [labelTtt03 length])];
    useStageBtn03.titleLabel.attributedText = attributedString03;
    useStageBtn03.titleLabel.textAlignment=NSTextAlignmentCenter;
    [useStageBtn03 setTitle:labelTtt03 forState:UIControlStateNormal];
    
    
    //分期按钮4
    NSString *bprice04=[NSString stringWithFormat:@"%.2f",stHandleFloat/9];
    NSRange rang04=[bprice04 rangeOfString:@"."];
    if (rang04.location != NSNotFound) {
        bprice04=[bprice04 substringToIndex:rang04.location];
    }
    
    CGFloat fqFee04=0;
    if (isVIP) {
        fqFee04=0;
    }else{
        fqFee04=stHandleFloat*rate09;
    }
    
    NSString *fqFeeStraa04=[NSString stringWithFormat:@"%.2f",fqFee04];
    NSRange fqFeeRang04=[fqFeeStraa04 rangeOfString:@"."];
    if (fqFeeRang04.location != NSNotFound) {
        fqFeeStraa04=[fqFeeStraa04 substringToIndex:fqFeeRang04.location];
    }
    
    CGFloat dxPrice04=0;
    if (!isYXCoupon) {
        dxPrice04=fqFeeStraa04.floatValue<handleCouponPrice?fqFeeStraa04.floatValue:handleCouponPrice;
    }
    
    NSString *labelTtt04=isYXCoupon?[NSString stringWithFormat:@"￥%@起x9期\n(手续费%@元)",bprice04,fqFeeStraa04]:[NSString stringWithFormat:@"￥%@起x9期\n(手续费%@元)\n(抵消手续费%.f元)",bprice04,fqFeeStraa04,dxPrice04];
    
    NSMutableAttributedString *attributedString04 = [[NSMutableAttributedString alloc] initWithString:labelTtt04];
    NSMutableParagraphStyle *paragraphStyle04 = [[NSMutableParagraphStyle alloc] init];
    //调整行间距
    if (isYXCoupon) {
        [paragraphStyle04 setLineSpacing:4];
    }
    [attributedString04 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle04 range:NSMakeRange(0, [labelTtt04 length])];
    useStageBtn04.titleLabel.attributedText = attributedString04;
    useStageBtn04.titleLabel.textAlignment=NSTextAlignmentCenter;
    [useStageBtn04 setTitle:labelTtt04 forState:UIControlStateNormal];
    
    //分期按钮5
    NSString *bprice05=[NSString stringWithFormat:@"%.2f",stHandleFloat/12];
    NSRange rang05=[bprice05 rangeOfString:@"."];
    if (rang05.location != NSNotFound) {
        bprice05=[bprice05 substringToIndex:rang05.location];
    }
    
    CGFloat fqFee05=0;
    if (isVIP) {
        fqFee05=0;
    }else{
        fqFee05=stHandleFloat*rate12;
    }
    
    NSString *fqFeeStraa05=[NSString stringWithFormat:@"%.2f",fqFee05];
    NSRange fqFeeRang05=[fqFeeStraa05 rangeOfString:@"."];
    if (fqFeeRang05.location != NSNotFound) {
        fqFeeStraa05=[fqFeeStraa05 substringToIndex:fqFeeRang05.location];
    }
    
    CGFloat dxPrice05=0;
    if (!isYXCoupon) {
        dxPrice05=fqFeeStraa05.floatValue<handleCouponPrice?fqFeeStraa05.floatValue:handleCouponPrice;
    }
    
    NSString *labelTtt05=isYXCoupon?[NSString stringWithFormat:@"￥%@起x12期\n(手续费%@元)",bprice05,fqFeeStraa05]:[NSString stringWithFormat:@"￥%@起x12期\n(手续费%@元)\n(抵消手续费%.f元)",bprice05,fqFeeStraa05,dxPrice05];
    
    NSMutableAttributedString *attributedString05 = [[NSMutableAttributedString alloc] initWithString:labelTtt05];
    NSMutableParagraphStyle *paragraphStyle05 = [[NSMutableParagraphStyle alloc] init];
    //调整行间距
    if (isYXCoupon) {
        [paragraphStyle05 setLineSpacing:4];
    }
    [attributedString05 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle05 range:NSMakeRange(0, [labelTtt05 length])];
    useStageBtn05.titleLabel.attributedText = attributedString05;
    useStageBtn05.titleLabel.textAlignment=NSTextAlignmentCenter;
    [useStageBtn05 setTitle:labelTtt05 forState:UIControlStateNormal];
    
    //
    if (self.ReferencePriceBlock) {
        self.ReferencePriceBlock(selectedUseStageBtn.titleLabel.text);
    }
}

#pragma mark - 点击分期按钮
-(void)clickFqBtn:(id)sender{
    UIButton *myBtn=sender;
    btnTag=myBtn.tag;
    
    selectedUseStageBtn.layer.borderColor=[UIColor colorWithHexString:@"#b8b8b8"].CGColor;
    [selectedUseStageBtn setTitleColor:[UIColor colorWithHexString:@"#b8b8b8"] forState:UIControlStateNormal];
    
    selectedUseStageBtn.enabled=YES;
    UIButton *btn=sender;
    if (btn.tag==0) {
        stage_sum=@"1";
    }else if (btn.tag==1){
        stage_sum=@"3";
    }else if (btn.tag==2){
        stage_sum=@"6";
    }else if (btn.tag==3){
        stage_sum=@"9";
    }else{
        stage_sum=@"12";
    }
    
    btn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    [btn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    btn.enabled=NO;
    
    selectedUseStageBtn=btn;
    if (self.ReferencePriceBlock) {
        self.ReferencePriceBlock(selectedUseStageBtn.titleLabel.text);
    }
}

#pragma mark - 点击航班机票
-(void)clickHbBtn{
    //如果选择航班
    if (isSeleHbBtn) {
        hbBtn.layer.borderColor=[UIColor colorWithHexString:@"#8b8b8b"].CGColor;
        [hbBtn setTitleColor:[UIColor colorWithHexString:@"#8b8b8b"] forState:UIControlStateNormal];
        seletedGoPlaceBtn.layer.borderColor=[UIColor colorWithHexString:@"#8b8b8b"].CGColor;
        [seletedGoPlaceBtn setTitleColor:[UIColor colorWithHexString:@"#8b8b8b"] forState:UIControlStateNormal];
        seletedGoPlaceBtn.enabled=YES;
        
        handleFlightPrice=0;
        flightId=@"0";
        cityId=@"0";
    }else{
        hbBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
        [hbBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
        seletedGoPlaceBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
        [seletedGoPlaceBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
        CityModel *model = objc_getAssociatedObject(seletedGoPlaceBtn, "CityModel");
        seletedGoPlaceBtn.enabled=NO;
        
        handleFlightPrice=model.cityPrice.floatValue;
        flightId=model.flightid;
        cityId=model.cityId;
    }
    
    isSeleHbBtn=!isSeleHbBtn;
    crLabelString=crLabel.text;
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - 点击出发地点
-(void)clickCityBtn:(id)sender{
    
    isSeleHbBtn=YES;
    hbBtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    [hbBtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    
    //
    [seletedGoPlaceBtn setTitleColor:[UIColor colorWithHexString:@"#8b8b8b"] forState:UIControlStateNormal];
    seletedGoPlaceBtn.layer.borderColor=[UIColor colorWithHexString:@"#8b8b8b"].CGColor;
    seletedGoPlaceBtn.enabled=YES;
    
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    CityModel *model = objc_getAssociatedObject(sebtn, "CityModel");
    cityId=model.cityId;
    seletCityPrice=model.cityPrice;
    flightId=model.flightid;
    handleFlightPrice=model.cityPrice.floatValue;
    
    [sebtn setTitleColor:[UIColor colorWithHexString:@"#ff5741"] forState:UIControlStateNormal];
    sebtn.layer.borderColor=[UIColor colorWithHexString:@"#ff5741"].CGColor;
    sebtn.enabled=NO;
    
    crLabelString=crLabel.text;
    //用另一个button代替当前点击的button
    seletedGoPlaceBtn=sebtn;
    
    handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
    if (self.ButtonPriceBlock) {
        self.ButtonPriceBlock(handleAdultPrice);
    }
}

#pragma mark - init BottomView
-(void)setUpBottomView{
    //
    UIView *btmBiew=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-49, SCREENSIZE.width, 49)];
    btmBiew.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:btmBiew];
    
    //
    btmShowPriceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, 0.5f, btmBiew.width/2+44, btmBiew.height)];
    btmShowPriceLabel.backgroundColor=[UIColor colorWithHexString:@"#595959"];
    btmShowPriceLabel.font=kFontSize15;
    btmShowPriceLabel.textColor=[UIColor colorWithHexString:@"#ff5741"];
    btmShowPriceLabel.textAlignment=NSTextAlignmentCenter;
    
    //
    NSString *handelStr=selectedUseStageBtn.titleLabel.text;
    NSRange range=[handelStr rangeOfString:@"起"];
    if (range.location!=NSNotFound) {
        handelStr=[handelStr substringToIndex:range.location];
    }
    NSString *btnStr=handelStr;
    NSRange strRange=[btnStr rangeOfString:@"￥"];
    if (strRange.location!=NSNotFound) {
        btnStr=[btnStr substringFromIndex:strRange.location+strRange.length];
    }
    
    //注册block
    __weak typeof(self) weakSelf = self;
    weakSelf.ReferencePriceBlock = ^(NSString *buttonPrice){
        [self resetReferencePrice:buttonPrice];
    };
    [btmBiew addSubview:btmShowPriceLabel];
    
    //
    submitOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    submitOrderBtn.frame=CGRectMake(btmShowPriceLabel.frame.origin.x+btmShowPriceLabel.width, 0.5f, btmBiew.width-btmShowPriceLabel.width, btmBiew.height);
    submitOrderBtn.backgroundColor=[UIColor lightGrayColor];
    submitOrderBtn.titleLabel.font=kFontSize16;
    [submitOrderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitOrderBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [submitOrderBtn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
//    submitOrderBtn.enabled=NO;
    [btmBiew addSubview:submitOrderBtn];
}

#pragma mark - ReferencePrice Block
-(void)resetReferencePrice:(NSString *)price{
    NSString *handelStr=price;
    NSRange range=[handelStr rangeOfString:@"起"];
    if (range.location!=NSNotFound) {
        handelStr=[handelStr substringToIndex:range.location];
    }
    NSString *btnStr=handelStr;
    NSRange strRange=[btnStr rangeOfString:@"￥"];
    if (strRange.location!=NSNotFound) {
        btnStr=[btnStr substringFromIndex:strRange.location+strRange.length];
    }
    NSString *ckSt;
    if (btnTag==0) {
        ckSt=@"1";
    }else if (btnTag==1){
        ckSt=@"3";
    }else if (btnTag==2){
        ckSt=@"6";
    }else if(btnTag==3){
        ckSt=@"9";
    }else{
        ckSt=@"12";
    }
    
    NSString *bnpStr=[NSString stringWithFormat:@"￥%@起 x %@期",btnStr,ckSt];
    
    [btmShowPriceLabel setText:bnpStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        
        //
        NSRange prange01 = [[mutableAttributedString string] rangeOfString:btnStr options:NSCaseInsensitiveSearch];
        //
        NSRange prange02 = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@" %@",ckSt] options:NSCaseInsensitiveSearch];
        UIFont *priceFont1 = kFontSize18;
        CTFontRef font1 = CTFontCreateWithName((__bridge CFStringRef)priceFont1.fontName, priceFont1.pointSize, NULL);
        if (font1) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font1 range:prange01];
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font1 range:prange02];
            CFRelease(font1);
        }
        return mutableAttributedString;
    }];
    
    [topPriceLabel setText:bnpStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        
        //
        NSRange prange01 = [[mutableAttributedString string] rangeOfString:btnStr options:NSCaseInsensitiveSearch];
        //
        NSRange prange02 = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@" %@",ckSt] options:NSCaseInsensitiveSearch];
        UIFont *priceFont1 = kFontSize16;
        CTFontRef font1 = CTFontCreateWithName((__bridge CFStringRef)priceFont1.fontName, priceFont1.pointSize, NULL);
        if (font1) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font1 range:prange01];
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font1 range:prange02];
            CFRelease(font1);
        }
        return mutableAttributedString;
    }];
}

#pragma mark - 点击确定购买
-(void)clickBuyBtn{
    if ([phoneTF.text isEqualToString:@""]) {
        [AppUtils showSuccessMessage:@"请填写预订人电话号码" inView:self.view];
    }else{
        NSDateFormatter*format=[[NSDateFormatter alloc] init];//日期化。
        [format setDateFormat:@"YYYYMMdd"];//
        NSDate *date=[format dateFromString:seleDateStr];//字符串转换为日期。
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        
        NSInteger interval = [zone secondsFromGMTForDate: date];
        
        date = [date dateByAddingTimeInterval:interval];
        timeStamp= [date timeIntervalSince1970];
        //
        NetWorkStatus *nws=[[NetWorkStatus alloc] init];
        nws.NetWorkStatusBlock=^(BOOL status){
            if (status) {
                [self submitOrderRequest];
            }
        };
    }
}

#pragma mark - Request 提交订单
-(void)submitOrderRequest{
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
//    [dict setObject:@"show" forKey:@"mod"];
//    [dict setObject:@"do_save_order_iso" forKey:@"code"];
    [dict setObject:_pId forKey:@"product_id"];
    [dict setObject:_islandId forKey:@"mdid"];
    [dict setObject:isSeleHbBtn?@"1":@"0" forKey:@"is_fid"];
    [dict setObject:isSeleTC?@"1":@"0" forKey:@"is_rid"];
    [dict setObject:cityId forKey:@"hotel_id"];
    [dict setObject:flightId forKey:@"pro_fd_id"];
    [dict setObject:roomId forKey:@"pro_rd_id"];
    //0-没有 1-第一个……
    CGFloat chhTagFloat=0;
    if (isSeleChuHai) {
        chhTagFloat=chhBtnTag+1;
    }else{
        chhTagFloat=0;
    }
    [dict setObject:isPalau?[NSString stringWithFormat:@"%.f",chhTagFloat]:@"0" forKey:@"is_pl"];
    [dict setObject:[NSString stringWithFormat:@"%.f",per_num] forKey:@"person_number"];
    [dict setObject:@"0" forKey:@"childent_number"];
    [dict setObject:seleDateStr forKey:@"selectdate"];
    [dict setObject:@"0" forKey:@"cw_id"];
    [dict setObject:seleCouponId forKey:@"paycashcard_id"];
    
    NSString *t_hdCouponStr=[NSString stringWithFormat:@"%.2f",handleCouponPrice];
    NSRange thdRang=[t_hdCouponStr rangeOfString:@"."];
    if (thdRang.location!=NSNotFound) {
        t_hdCouponStr=[t_hdCouponStr substringToIndex:thdRang.location];
    }
    [dict setObject:t_hdCouponStr forKey:@"card_price"];
    
    [dict setObject:stage_sum forKey:@"fenqi"];
    [dict setObject:vipPerem forKey:@"yxyh"];     //0-没有 1-98折 2-满立减
    [dict setObject:phoneTF.text forKey:@"linkphone"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];

    //时间戳
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    
    //版本key
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    DSLog(@"%@",[AppUtils getValueWithKey:Sign_Key]);
    
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?code=do_save_order_iso&mod=show"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            ApprovedOrderController *approveCtr=[[ApprovedOrderController alloc]init];
            approveCtr.islandType=_islandType;
            approveCtr.orderId=responseObject[@"retData"][@"orderid"];
            approveCtr.rateArr=rateArr;
            [self.navigationController pushViewController:approveCtr animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

//获取费率
-(void)getStageRate{
    rateArr=[NSMutableArray array];
    [[NetWorkRequest defaultClient] requestWithPath:RateUrl method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"费率%@---",responseObject);
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"];
            for (NSDictionary *dic in arr) {
                NSString *rateStr=dic[@"feilv"];
                [rateArr addObject:rateStr];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


-(void)newRequestForUserId:(NSString *)userId pid:(NSString *)pid mdid:(NSString *)mdid{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    monthArr=[NSMutableArray array];
    cityArr=[NSMutableArray array];
    roomArr=[NSMutableArray array];
    useStageArr=[NSMutableArray array];
    //
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"index" forKey:@"mod"];
    [dict setObject:@"peo_order_v6" forKey:@"code"];
    [dict setObject:pid forKey:@"pid"];
    [dict setObject:userId forKey:@"user_id"];
    [dict setObject:mdid forKey:@"mdid"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@---",responseObject);
        
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            //参数
            NSDictionary *csDic=responseObject[@"retData"][@"canshu"];
            isHadCity=[csDic[@"is_city"] isEqualToString:@"1"]?YES:NO;
            isHadFlight=[csDic[@"is_flight"] isEqualToString:@"1"]?YES:NO;
            isHadHotel=[csDic[@"is_hotel"] isEqualToString:@"1"]?YES:NO;
            isHadFenqi=[csDic[@"is_fenqi"] isEqualToString:@"1"]?YES:NO;
            isPalau=[csDic[@"is_pl"] isEqualToString:@"1"]?YES:NO;
            isVIP=[responseObject[@"retData"][@"yxcredit"] isEqualToString:@"1"]?YES:NO;
            if (isVIP) {
                is98zhe=YES;
                hyNumber=@"1";
            }else{
                is98zhe=NO;
                hyNumber=@"0";
            }
            
            //出发城市
            NSString *defaultSelect=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"city_int"]];
            NSArray *ctArr=responseObject[@"retData"][@"city"];
            for (NSDictionary *dic in ctArr) {
                CityModel *model=[[CityModel alloc]init];
                [model jsonDataForDictionary:dic];
                //默认选中
                if ([model.cityId isEqualToString:defaultSelect]) {
                    model.isDefaultSelect=YES;
                }else{
                    model.isDefaultSelect=NO;
                }
                [cityArr addObject:model];
            }
            
            //房间
            NSArray *rmArr=responseObject[@"retData"][@"room"];
            for (NSDictionary *dic in rmArr) {
                RoomModel *model=[[RoomModel alloc]init];
                model.model=dic;
                [roomArr addObject:model];
            }
            
            //月份
            NSArray *mtArr=responseObject[@"retData"][@"date"];
            for (NSDictionary *dic in mtArr) {
                MonthModel *model=[MonthModel new];
                [model jsonDataForDictionary:dic];
                [monthArr addObject:model];
            }
            
            //房间ID
            roomId=responseObject[@"retData"][@"room_id"];
            
            //房间价格
            roomPrice=responseObject[@"retData"][@"room_price"];
            
            //优惠券
            couponArr=responseObject[@"retData"][@"cashcard"];
            userCouponCtr=[UseCouponController new];
            userCouponCtr.isVIP=isVIP;
            userCouponCtr.couponArr=couponArr;
            
            //使用分期Array
            NSArray *fqArr=responseObject[@"retData"][@"fenqi"];
            for (NSDictionary *dic in fqArr) {
                UseStageModel *model=[[UseStageModel alloc]init];
                [model jsonDataForDictionary:dic];
                [useStageArr addObject:model];
            }
            
            //
            [self initUI];
            [self setUpBottomView];
            
            //刷新一下头部底部分期价格
            handleAdultPrice=handleFlightPrice+handleTaocanPrice+handleChuHaiPrice;
            if (self.ButtonPriceBlock) {
                self.ButtonPriceBlock(handleAdultPrice);
            }
            
        }else{
            DSLog(@"请求错误");
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
