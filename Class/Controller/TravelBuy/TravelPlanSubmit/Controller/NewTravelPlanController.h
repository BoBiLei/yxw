//
//  NewTravelPlanController.h
//  youxia
//
//  Created by mac on 15/12/5.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTravelPlanController : UIViewController

@property (nonatomic ,copy) NSString *pId;
@property (nonatomic ,copy) NSString *mdId;
@property (nonatomic ,copy) NSString *islandId;
@property (nonatomic ,copy) NSString *islandName;
@property (nonatomic) IslandType islandType;


@property (nonatomic, strong) void (^ButtonPriceBlock)(CGFloat buttonPrice);
@property (nonatomic, strong) void (^ReferencePriceBlock)(NSString * referencePrice);//参考价格

@property (nonatomic, strong) void (^PaluaPersonPriceBlock)(NSString * personPrice);//帕劳成人价格

@end
