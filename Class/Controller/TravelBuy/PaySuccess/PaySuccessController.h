//
//  PaySuccessController.h
//  youxia
//
//  Created by mac on 16/1/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaySuccessController : UIViewController

@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *orderName;
@property (nonatomic,copy) NSString *orderPrice;

@end
