//
//  PaySuccessController.m
//  youxia
//
//  Created by mac on 16/1/8.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PaySuccessController.h"
#import "OrderDetailController.h"
#import "PersonalCenterController.h"
#import "OrderListController.h"
#import "TestController.h"

#define BagColor @"#ebebeb"
#define BlackColor @"#282828"
@interface PaySuccessController ()<UITableViewDelegate>

@end

@implementation PaySuccessController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"购买成功页"];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"购买成功页"];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用 iOS7 滑动返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setUpCustomSearBar];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self setUpUI];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(clickReturnBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(back.origin.x+back.width, 20, SCREENSIZE.width-(back.origin.x+back.width)*2, 44)];
    titleLabel.font=kFontSize17;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=@"购买成功";
    [self.view addSubview:titleLabel];
    
    UIColor * ccolor = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [self.navigationController.navigationBar lt_setBackgroundColor:ccolor];
}

-(void)clickReturnBtn{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[OrderListController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else if ([controller isKindOfClass:[TestController class]]){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - init UI
-(void)setUpUI{
    
    //
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:BagColor];
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, myTable.height)];
    footView.backgroundColor=[UIColor colorWithHexString:BagColor];
    myTable.tableFooterView=footView;
    
    //付款成功图片
    UIImageView *psImgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/2, SCREENSIZE.width/2)];
    psImgv.center=CGPointMake(SCREENSIZE.width/2, psImgv.height/2);
    psImgv.image=[UIImage imageNamed:@"pay_success.jpg"];
    psImgv.contentMode=UIViewContentModeScaleAspectFit;
    [footView addSubview:psImgv];
    
    //
    TTTAttributedLabel *tipLabel=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(20, psImgv.origin.y+psImgv.height+16, SCREENSIZE.width-40, 24)];
    tipLabel.font=kFontSize16;
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.textColor=[UIColor colorWithHexString:@"#17A554"];
    NSString *tipStr=[NSString stringWithFormat:@"您已成功付款 %@元！",_orderPrice];
    [tipLabel setText:tipStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",_orderPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize18;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    [footView addSubview:tipLabel];
    
    //
    UILabel *orderId_left=[[UILabel alloc]initWithFrame:CGRectMake(36, tipLabel.origin.y+tipLabel.height+12, 86, 26)];
    orderId_left.font=kFontSize16;
    orderId_left.textColor=[UIColor colorWithHexString:BlackColor];
    orderId_left.text=@"订单编号：";
    [footView addSubview:orderId_left];
    
    UILabel *orderId_right=[[UILabel alloc]initWithFrame:CGRectMake(orderId_left.origin.x+orderId_left.width+2, orderId_left.origin.y, SCREENSIZE.width-(orderId_left.origin.x+orderId_left.width+2+40), orderId_left.height)];
    orderId_right.font=kFontSize16;
    orderId_right.textColor=[UIColor colorWithHexString:BlackColor];
    orderId_right.text=_orderId;
    orderId_right.numberOfLines=0;
    [footView addSubview:orderId_right];
    
    //
    UILabel *orderName=[[UILabel alloc]initWithFrame:CGRectMake(orderId_left.origin.x, orderId_left.origin.y+orderId_left.height+8, orderId_left.width, 26)];
    orderName.font=kFontSize16;
    orderName.textColor=[UIColor colorWithHexString:BlackColor];
    orderName.text=@"产品名称：";
    [footView addSubview:orderName];
    CGSize size=[AppUtils getStringSize:_orderName withFont:16];
    CGFloat pnWidth=SCREENSIZE.width-(orderName.origin.x+orderName.width+2+40);
    CGFloat pnHeight=0;
    if (size.width>pnWidth) {
        pnHeight=46;
    }else{
        pnHeight=26;
    }
    
    UILabel *orderName_right=[[UILabel alloc]initWithFrame:CGRectMake(orderName.origin.x+orderName.width+2, orderName.origin.y, SCREENSIZE.width-(orderName.origin.x+orderName.width+2+40), pnHeight)];
    orderName_right.font=kFontSize16;
    orderName_right.textColor=[UIColor colorWithHexString:BlackColor];
    if (size.width>pnHeight) {
        NSString *labelTtt=_orderName;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:2];//调整行间距
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
        orderName_right.attributedText = attributedString;
    }else{
        orderName_right.text=_orderName;
    }
    orderName_right.numberOfLines=0;
    [footView addSubview:orderName_right];
    
    //
    CGFloat btnWidth=(SCREENSIZE.width-60)/2;
    CGFloat btnHeight=44;
    for (int i=0; i<2; i++) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.cornerRadius=3;
        btn.titleLabel.font=kFontSize16;
        if (i==0) {
            btn.backgroundColor=[UIColor whiteColor];
            [btn setTitle:@"查看订单列表" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithHexString:BlackColor] forState:UIControlStateNormal];
        }else{
            btn.backgroundColor=[UIColor colorWithHexString:@"#1DAFED"];
            [btn setTitle:@"看看其他产品" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        btn.tag=i;
        btn.frame=CGRectMake(i*20+i*btnWidth+20, orderName_right.origin.y+orderName_right.height+24, btnWidth, btnHeight);
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:btn];
    }
}

-(void)clickBtn:(id)sender{
    UIButton *btn=sender;
    if (btn.tag==0) {
        OrderListController *orderDetail=[[OrderListController alloc]init];
        [self.navigationController pushViewController:orderDetail animated:YES];
    }else{
        self.rdv_tabBarController.selectedIndex=1;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01f;
}

@end
