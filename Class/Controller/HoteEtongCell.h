//
//  HoteEtongCell.h
//  youxia
//
//  Created by mac on 2017/4/26.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteEtongCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (strong, nonatomic) UILabel *ageLabel;
@property (strong, nonatomic) UITextField *firstName;
@property (strong, nonatomic) UITextField *lastName;

@property (nonatomic, copy) void (^HoteEtongFirstNameBlock)(NSString *text);
@property (nonatomic, copy) void (^HoteEtongLastNameBlock)(NSString *text);

@end
