//
//  HotelSumChildCell.h
//  youxia
//
//  Created by mac on 2017/5/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelInSumChildDelegate <NSObject>

-(void)clickAddChildBtnAtIndexPatch:(NSIndexPath *)indexPatch;

-(void)clickJianChildBtnAtIndexPatch:(NSIndexPath *)indexPatch;

@end

@interface HotelSumChildCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *jianBtn;
@property (weak, nonatomic) IBOutlet UILabel *childSumLabel;

@property (weak, nonatomic) id<HotelInSumChildDelegate> delegate;

-(void)reflushSumData:(NSInteger)labelSum;

@end
