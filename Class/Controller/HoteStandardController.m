//
//  HoteStandardController.m
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteStandardController.h"
#import "HotefanghandCell.h"
#import "HoteStandCell.h"
#import "HoteLabTitleCell.h"
#import "HotelreservationController.h"
#import "HoteStandardCell.h"
#import "HotelreservationModel.h"
#import "HoteStandardModel.h"

@interface HoteStandardController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (nonatomic, strong) UITableView *myTable;

@end

@implementation HoteStandardController{
    HoteStandardModel *dataModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1]];
    
    [self seNavBar];
    
    [self rooomRequest];
}

-(void)seNavBar{
    UINavigationBar *cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:1]];
    [self.view addSubview:cusBar];
    
    UIButton *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    UILabel *btnlab = [[UILabel alloc] initWithFrame:CGRectMake(54, 20, SCREENSIZE.width-108, 44)];
    btnlab.text=@"房屋详情";
    
    btnlab.textAlignment=NSTextAlignmentCenter;
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
        _myTable.scrollEnabled=NO;
        _myTable.dataSource=self;
        _myTable.delegate=self;
        [_myTable registerNib:[UINib nibWithNibName:@"HoteLabTitleCell" bundle:nil] forCellReuseIdentifier:@"HoteLabTitleCell"];
        [self.view addSubview:_myTable];
        
        _myTable.tableFooterView=[UIView new];
    }
    return _myTable;
}

-(void)fanhui{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HoteLabTitleCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteLabTitleCell"];
    if (!cell) {
        cell=[[HoteLabTitleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteLabTitleCell"];
        return cell;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell reflushData:dataModel atIndexPath:indexPath];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==5) {
        return dataModel.roomFacility.count*30;
    }else{
        return 44;
    }
}

-(void)rooomRequest{
    [AppUtils showProgressInView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"roomDetail" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [dict setObject:_hoteID forKey:@"HotelID"];
    [dict setObject:_reteplanID forKey:@"rateID"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"酒店房型详情接口=================%@",responseObject);
        NSString *str1=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str1 isEqualToString:@"1"]){
            id obj=responseObject[@"retData"];
            if([obj isKindOfClass:[NSDictionary class]]){
                dataModel=[HoteStandardModel new];
                [dataModel jsonToModel:obj];
            }
            [self.myTable reloadData];
        }
        else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
    
}

-(void)addAlertView{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                              @"提示" message:@"当前无房型数据" delegate:self
                                             cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

#pragma mark - Alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:
(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            NSLog(@"Cancel button clicked");
            break;
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}
@end
