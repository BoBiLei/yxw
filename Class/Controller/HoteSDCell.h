//
//  HoteSDCell.h
//  youxia
//
//  Created by mac on 2017/3/27.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteSDCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *starDates;
@property (weak, nonatomic) IBOutlet UILabel *endDates;
@property (weak, nonatomic) IBOutlet UILabel *cDate;


-(void)refluStarD:(NSString *)starStr endDate:(NSString *)endDate totalDays:(NSString *)days;

@end
