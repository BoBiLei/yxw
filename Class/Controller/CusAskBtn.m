//
//  CusImageTitleBtn.m
//  youxia
//
//  Created by mac on 2017/4/25.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "CusAskBtn.h"

@interface CusAskBtn()

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIImage *image;

@end

@implementation CusAskBtn

-(id)initWithFrame:(CGRect)frame title:(NSString *)title image:(UIImage *)image{
    self=[super initWithFrame:frame];
    if (self) {
        self.title=title;
        self.image=image;
        [self addSubview:self.imgv];
        [self addSubview:self.cusTitleLabel];
    }
    return self;
}

-(UIButton *)imgv{
    if (!_imgv) {
        _imgv=[[UIButton alloc] init];
        _imgv.userInteractionEnabled=NO;
        [_imgv setImage:self.image forState:UIControlStateNormal];
    }
    return _imgv;
}

-(UILabel *)cusTitleLabel{
    if (!_cusTitleLabel) {
        _cusTitleLabel=[[UILabel alloc] init];
        _cusTitleLabel.text=self.title;
        _cusTitleLabel.textColor=[UIColor grayColor];
        _cusTitleLabel.font=kFontSize15;
    }
    return _cusTitleLabel;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    _imgv.frame=CGRectMake(0, 4, self.height-8, self.height-8);
    _cusTitleLabel.frame=CGRectMake(_imgv.origin.x+_imgv.width+4, _imgv.origin.y, self.width-(_imgv.origin.x+_imgv.width+4), _imgv.height);
    
    
}

@end
