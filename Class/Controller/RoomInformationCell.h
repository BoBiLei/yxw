//
//  RoomInformationCell.h
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomInformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (strong, nonatomic) UITextField *firstName;
@property (strong, nonatomic) UITextField *lastName;

@property (nonatomic, copy) void (^RoomInforFirstNameBlock)(NSString *text);
@property (nonatomic, copy) void (^RoomInforLastNameBlock)(NSString *text);

@end
