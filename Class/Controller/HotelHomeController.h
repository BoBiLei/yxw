//
//  HotelHomeController.h
//  youxia
//
//  Created by mac on 2017/3/7.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelHomeController : UIViewController

@property (nonatomic, retain) NSArray *peopleArr;
@property(nonatomic,copy) NSString *adultcount; //成人数量
@property(nonatomic,copy) NSString *childrencount; //儿童数量
@property(nonatomic,copy) NSString *childage;
@property(nonatomic,retain)NSArray *dzarrs;

@end
