//
//  HoteLintroductionController.h
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoteLintroductionController : UIViewController
@property(nonatomic,copy)NSString *hoteid;
@property(nonatomic,copy)NSMutableArray *hoteimg;
@end
