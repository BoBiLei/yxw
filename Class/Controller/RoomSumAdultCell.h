//
//  RoomSumAdultCell.h
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RoomSumAdultDelegate <NSObject>

-(void)clickAddBtn:(NSIndexPath *)indexPach;

-(void)clickJianBtn:(NSIndexPath *)indexPach;

@end

@interface RoomSumAdultCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *jianBtn;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) id <RoomSumAdultDelegate> delegate;

@end
