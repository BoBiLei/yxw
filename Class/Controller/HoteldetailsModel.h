//
//  HoteldetailsModel.h
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HoteldetailsModel : NSObject

@property (nonatomic,copy)NSString *hoteID;

@property (nonatomic,copy)NSString *ruzhuDate;
@property (nonatomic,copy)NSString *endDate;
@property (nonatomic,copy)NSString *crnum;
@property (nonatomic,copy)NSString *etnum;
@property (nonatomic,copy)NSString *etage;

//房型
@property(nonatomic,copy)NSString *bedtype;
@property(nonatomic,copy)NSString *breakfastype;        //早餐类型
@property(nonatomic,copy)NSString *currency;  //付款货币类型
@property(nonatomic,copy)NSString *kucun;  //库存
@property(nonatomic,copy)NSString *maxqccpancy; //最大入住数
@property(nonatomic,copy)NSString *rateplanid; // 房型id
@property(nonatomic,copy)NSArray  *pricelist;
@property(nonatomic,copy)NSString *rateplaname;  //房间名称
@property(nonatomic,copy)NSString *roomstatus; //房间状态 为1可入住
@property(nonatomic,copy)NSString *tprice; //总价
@property(nonatomic,copy)NSString *name_cn;
@property(nonatomic,retain)NSArray *roomimg;

//@property(nonatomic,copy)NSString *amout;
@property (nonatomic, retain) NSDictionary *model;



-(void)jsonDataForDictioanry:(NSDictionary *)model;

@end
