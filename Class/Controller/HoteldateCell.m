//
//  HoteldateCell.m
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteldateCell.h"
#import "SelectCheckDateViewController.h"

@implementation HoteldateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    [self optiondate];
}
//获取当前时间和后天时间
-(void)optiondate
{
    
    NSDate *  senddate=[NSDate date];//当前时间
//    NSDate *lastDay = [NSDate dateWithTimeInterval:-24*60*60 sinceDate:senddate];//前一天
    NSDate *nextDay = [NSDate dateWithTimeInterval:24*60*60 sinceDate:senddate];//后一天
    NSDate *nextnextDay=[NSDate dateWithTimeInterval:(24*60*60)*2 sinceDate:senddate];  //大后天
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    
    
    NSString *  locationString=[dateformatter stringFromDate:nextDay];
    NSString *nextString=[dateformatter stringFromDate:nextnextDay];
    _hoteDateone.text=locationString;
    _hoteDatetwo.text=nextString;
    _Hotecount.text=@"1";
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    
}

-(void)refluStarDate:(NSString *)starStr endDate:(NSString *)endDate totalDays:(NSString *)days{
    _hoteDateone.text=starStr;
    _hoteDatetwo.text=endDate;
    _Hotecount.text=days;
}

@end
