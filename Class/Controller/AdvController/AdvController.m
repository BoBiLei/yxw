//
//  AdvController.m
//  youxia
//
//  Created by mac on 15/12/4.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "AdvController.h"

@interface AdvController ()

@end

@implementation AdvController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"广告页"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
