//
//  ContactInformationCell.m
//  youxia
//
//  Created by mac on 2017/3/8.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "ContactInformationCell.h"

@implementation ContactInformationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_Contacttext addTarget:self action:@selector(textChange) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)textChange{
    if (self.ContactInformation) {
        self.ContactInformation(_Contacttext.text);
    }
//    [self.delegate textChange:_Contacttext.text];
}

@end
