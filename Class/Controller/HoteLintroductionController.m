//
//  HoteLintroductionController.m
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteLintroductionController.h"
#import "HotelFacilitiesCell.h"
#import "HoteLintroductionCell.h"
#import "AVCollectionCell.h"
@interface HoteLintroductionController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation HoteLintroductionController
{
    UITableView *myTable;
    NSMutableArray *photoArr;
    UIScrollView *myScrolview;
    UICollectionView *photoCollect;
    UIWebView *webview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCustomSearBar];
    webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64)];
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youxia.com/mgo/index.php?mod=hotel&code=hotelIntroduce_ios&is_get=1&hotelID=%@",_hoteid]];
    [webview loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:webview];
//    [self setUpTableView];
//    [self setUpMainScrollview];
    
//    [self HoteimageRequest];
//    [self HoteintroduceRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    label.font=kFontSize17;
    label.textAlignment=NSTextAlignmentCenter;
    label.text=@"酒店介绍";
    label.textColor=[UIColor whiteColor];
    [self.view addSubview:label];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    
}
-(void)setUpMainScrollview
{
    myScrolview=[[UIScrollView alloc] initWithFrame:CGRectMake(0,webview.size.height+webview.origin.y+10, SCREENSIZE.width, SCREENSIZE.height)];
    myScrolview.contentSize=CGSizeMake(SCREENSIZE.width*2, 0);
    myScrolview.bounces=NO;
    myScrolview.showsHorizontalScrollIndicator=NO;
    myScrolview.showsVerticalScrollIndicator=NO;
    myScrolview.scrollEnabled=NO;
    myScrolview.pagingEnabled=NO;
    myScrolview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:myScrolview];
    
    //
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    photoCollect=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, myScrolview.height) collectionViewLayout:myLayout];
    [photoCollect registerNib:[UINib nibWithNibName:@"AVCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"AVCollectionCell"];
    photoCollect.backgroundColor=[UIColor clearColor];
    photoCollect.dataSource=self;
    photoCollect.delegate=self;
    photoCollect.bounces=YES;
    [myScrolview addSubview:photoCollect];
    
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}
////加载html酒店介绍页
//-(UIWebView *)webView
//{
//    if (!_webView) {
//        _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
//        _webView.dataDetectorTypes = UIDataDetectorTypeAll;
//    }
//    return _webView; 
//}
//- (void)loadFile
//{
//    // 应用场景:加载从服务器上下载的文件,例如pdf,或者word,图片等等文件
//    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"关于.txt" withExtension:nil];
//    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
//    [self.webView loadRequest:request];
//}

-(void)setUpTableView
{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height) style:UITableViewStyleGrouped];
    myTable.delegate=self;
    myTable.dataSource=self;
    [myTable registerNib:[UINib nibWithNibName:@"HotelFacilitiesCell" bundle:nil]forCellReuseIdentifier:@"HotelFacilitiesCell"];
    [myTable registerNib:[UINib nibWithNibName:@"HoteLintroductionCell" bundle:nil]forCellReuseIdentifier:@"HoteLintroductionCell"];

    [self.view addSubview:myTable];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        
            HotelFacilitiesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelFacilitiesCell"];
        if (!cell) {
            cell=[[HotelFacilitiesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelFacilitiesCell"];
            return cell;
        }
        cell.titleName.text=@"酒店简介";
        [cell.titleName setFont:[UIFont systemFontOfSize:18]];
        cell.titleName.textColor=[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1];
        cell.imgs.hidden=YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }else
    {
        HoteLintroductionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteLintroductionCell"];
        if (!cell) {
            cell=[[HoteLintroductionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteLintroductionCell"];
            return cell;
        }
        UILabel *introduction
                    =[[UILabel alloc]initWithFrame:CGRectMake(10, 0, SCREENSIZE.width-10, SCREENSIZE.height)];
                    introduction.text=@"我们在金巴兰的人气好评5星酒店之一 海滩距离住宿有5分钟步行路程。AYANA Resort and Spa Bali度假酒店距离金巴兰中心。Ji Karang Mas Sejahtera. 金巴兰地区，巴厘岛，巴厘省，80364，印度尼西亚";
                    introduction.numberOfLines=0;
        
                    introduction.lineBreakMode=NSLineBreakByWordWrapping;
                    CGSize size=[introduction sizeThatFits:CGSizeMake(introduction.frame.size.width, MAXFLOAT)];
        
                    introduction.font =[UIFont systemFontOfSize:13];
                    introduction.textColor=[UIColor grayColor];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;

                    introduction.frame=CGRectMake(introduction.frame.origin.x, introduction.frame.origin.y, introduction.frame.size.width, size.height);
                    [cell addSubview:introduction];
        
        
        
//        NSString *str = @"我们在金巴兰的人气好评5星酒店之一 海滩距离住宿有5分钟步行路程。AYANA Resort and Spa Bali度假酒店距离金巴兰中心Ji Karang Mas Sejahtera. 金巴兰地区，巴厘岛，巴厘省，80364，印度尼西亚";
//
//        UILabel *label1  = [[UILabel alloc]initWithFrame:CGRectMake(100 ,150, 200, 200)];
//        label1.font = [UIFont systemFontOfSize:15];
//        label1.backgroundColor = [UIColor grayColor];
//        label1.numberOfLines = 0;
//        label1.text = str;
//        [label1 sizeToFit];
//        NSLog(@"label1.frame = %@",NSStringFromCGRect(label1.frame));
//        [cell addSubview:label1];
        
        
        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    
    }


}

- ( CGFloat )tableView:( UITableView *)tableView heightForHeaderInSection:(NSInteger )section{
    return 1.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return 44;
    }else
    {
        return 186;
    }


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;

}
#pragma mark - 数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _hoteimg.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AVCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"AVCollectionCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[AVCollectionCell alloc]init];
    }
    NSString *str=_hoteimg[indexPath.row];
    cell.imgv.clipsToBounds  = YES;
    cell.imgv.contentMode=UIViewContentModeScaleAspectFill;
    [cell.imgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:Image_Default]];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth=(SCREENSIZE.width-40)/3;
    
    return CGSizeMake(cellWidth, cellWidth-30);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 8, 10, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *imgArr=[NSMutableArray array];
    for (NSString *img in _hoteimg) {
        NSURL *url=[NSURL URLWithString:img];
        [imgArr addObject:url];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:imgArr];
    browser.displayToolbar=YES;
    browser.displayCounterLabel=YES;
    browser.displayActionButton = NO;
    [browser setInitialPageIndex:indexPath.row];
    [self presentViewController:browser animated:YES completion:nil];
}

//压缩图片
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


/*
#pragma mark -酒店图片接口
-(void)HoteimageRequest
{
    
    photoArr=[NSMutableArray array];
//    _hoteimg=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp=[NSString stringWithFormat:@"%ld",now];
    NSString *nonce_str=[YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"hotelPics" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [dict setObject:_hoteid forKey:@"ID"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            //原始大小图数组
            id photoObj=responseObject[@"retData"];
            
            if ([photoObj isKindOfClass:[NSArray class]]) {
                for (NSString *str in photoObj) {
                    if (![str isEqualToString:@""]||str!=nil) {
                        [photoArr addObject:str];
                    }
                }
            }
            
            [photoCollect reloadData];
        }
        
        
        
        DSLog(@"%@",responseObject);
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        
    }];
}
#pragma mark -酒店介绍接口
-(void)HoteintroduceRequest
{
    
//    photoArr=[NSMutableArray array];
    //    _hoteimg=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp=[NSString stringWithFormat:@"%ld",now];
    NSString *nonce_str=[YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"hotelIntroduce_ios" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [dict setObject:_hoteid forKey:@"hotelID"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            
//            NSString *aa=responseObject[@"retData"][@"msg"];
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
//            if ([photoObj isKindOfClass:[NSArray class]]) {
//                for (NSString *str in photoObj) {
//                    if (![str isEqualToString:@""]||str!=nil) {
//                        [photoArr addObject:str];
//                    }
//                }
//            }
//            
        }
        
        
        
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        
    }];
}*/
/*
//将要进入编辑模式
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{return YES;}
//已经进入编辑模式
- (void)textViewDidBeginEditing:(UITextView *)textView{}
//将要结束/退出编辑模式
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{return YES;}
//已经结束/退出编辑模式
- (void)textViewDidEndEditing:(UITextView *)textView{}
//当textView的内容发生改变的时候调用
- (void)textViewDidChange:(UITextView *)textView{}
//选中textView 或者输入内容的时候调用
- (void)textViewDidChangeSelection:(UITextView *)textView{}
//从键盘上将要输入到textView 的时候调用
//rangge  光标的位置
//text  将要输入的内容
//返回YES 可以输入到textView中  NO不能
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    return YES;


}
*/
@end
