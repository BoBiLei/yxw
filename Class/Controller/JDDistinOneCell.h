//
//  JDDistinOneCell.h
//  youxia
//
//  Created by mac on 2017/3/23.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelListModel.h"
@interface JDDistinOneCell : UITableViewCell

@property(nonatomic,retain)HotelListModel *model;
-(void)reflushDataForModel:(HotelListModel *)model;

@end
