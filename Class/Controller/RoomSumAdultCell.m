//
//  RoomSumAdultCell.m
//  youxia
//
//  Created by mac on 2017/5/17.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "RoomSumAdultCell.h"

@implementation RoomSumAdultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickAddBtn:(id)sender {
    UITableView *tableView = (UITableView *)self.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    [self.delegate clickAddBtn:indexPath];
}
- (IBAction)clickJianBtn:(id)sender {
    UITableView *tableView = (UITableView *)self.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    [self.delegate clickJianBtn:indexPath];
}

@end
