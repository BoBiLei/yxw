//
//  ThemticRecommendModel.m
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "ThemticRecommendModel.h"

@implementation ThemticRecommendModel

-(void)setModel:(NSDictionary *)model{
    self.name=model[@"name"];
    self.pic=model[@"pic"];
    self.url=model[@"url"];
}

@end
