//
//  ThemticRecommendModel.h
//  youxia
//
//  Created by mac on 16/5/12.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemticRecommendModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, retain) NSDictionary *model;

@end
