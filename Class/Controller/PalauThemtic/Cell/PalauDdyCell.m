//
//  PalauDdyCell.m
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PalauDdyCell.h"

#define titleColor @"#2a2a2a"
#define descriptColor @"#a0a0a0"

#define blueColor @"#0080c5"

@implementation PalauDdyCell{
    UIImageView *imgv;
    TTTAttributedLabel *stagePrice;
    UILabel *title;
    TTTAttributedLabel *description;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
    
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width/(5/2)-20)];
    [self addSubview:imgv];
    
    //价格的View
    CGFloat pvWidth=92;
    CGFloat pvHeight=32;
    UIView *priceView=[[UIView alloc]initWithFrame:CGRectMake(SCREENSIZE.width-pvWidth-8, imgv.height-pvHeight-14, pvWidth, pvHeight)];
    priceView.layer.shadowOffset = CGSizeMake(1, 2);
    priceView.layer.shadowOpacity = 0.7;
    priceView.backgroundColor=[UIColor colorWithHexString:blueColor];
    [self addSubview:priceView];
    
    //参考价
    stagePrice=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, 1, priceView.width, pvHeight-1)];
    stagePrice.backgroundColor=[UIColor colorWithHexString:blueColor];
    stagePrice.textColor=[UIColor whiteColor];
    stagePrice.font=kFontSize12;
    stagePrice.textAlignment=NSTextAlignmentCenter;
    [priceView addSubview:stagePrice];
    
    //title
    title=[[UILabel alloc]initWithFrame:CGRectMake(8, imgv.origin.y+imgv.height+6, SCREENSIZE.width-16, 19)];
    title.textColor=[UIColor colorWithHexString:titleColor];
    title.font=kFontSize14;
    [self addSubview:title];
    
    //描述
    description=[[TTTAttributedLabel alloc]initWithFrame:CGRectMake(title.origin.x, title.origin.y+title.height+2, title.width, 17)];
    description.textColor=[UIColor colorWithHexString:descriptColor];
    description.font=kFontSize13;
    [self addSubview:description];
}

-(void)setModel:(HomePageIslandModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_480x240"]];
    }
    
    NSString *stageStr=[NSString stringWithFormat:@"参考价:￥%@",model.totalPrice];
    [stagePrice setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.totalPrice options:NSCaseInsensitiveSearch];
        //设置选择文本的大小
        UIFont *boldSystemFont = kFontSize15;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
            CFRelease(font);
        }
        return mutableAttributedString;
    }];
    title.text=model.islandTitle;
    
    NSString *descriptStr;
//    if ([model.posids isEqualToString:@"1"]) {
        descriptStr=[NSString stringWithFormat:@"热卖推荐  %@",model.islandDescript];
        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:@"热卖推荐" options:NSCaseInsensitiveSearch];
            //设置选择文本的大小
            UIFont *systemFont = kFontSize13;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)systemFont.fontName, systemFont.pointSize, NULL);
            if (font) {
                
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                
                //字体背景
                [mutableAttributedString addAttribute:(NSString *)kTTTBackgroundFillColorAttributeName value:(id)[UIColor redColor].CGColor range:sumRange];
                
                //字体颜色
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor whiteColor] CGColor] range:sumRange];
                
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
//    }else{
//        descriptStr=[NSString stringWithFormat:@"热卖推荐%@",model.islandDescript];
//        [description setText:descriptStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
//            return mutableAttributedString;
//        }];
//    }
}

@end
