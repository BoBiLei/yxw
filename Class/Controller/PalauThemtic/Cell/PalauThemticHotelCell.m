//
//  PalauThemticHotelCell.m
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PalauThemticHotelCell.h"

@implementation PalauThemticHotelCell{
    UIImageView *imgv;
    UILabel *typeNameLabel;
    UILabel *titleLabel;
    TTTAttributedLabel *priceLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor=[UIColor whiteColor];
    
    [self setUpUI];
}

-(void)setUpUI{
    imgv=[[UIImageView alloc] initWithFrame:CGRectMake(8, 12, SCREENSIZE.width/3, SCREENSIZE.width/3-36)];
    [self addSubview:imgv];
    
    CGRect frame=self.frame;
    frame.size.height=SCREENSIZE.width/3-12;
    self.frame=frame;
    
    //
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, self.height-0.6, SCREENSIZE.width, 0.6)];
    line.backgroundColor=[UIColor lightGrayColor];
    [self addSubview:line];
    
    //
    CGFloat titlW=SCREENSIZE.width-(imgv.origin.x+imgv.width+16);
    
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.origin.x+imgv.width+8, imgv.origin.y+1, titlW, 34)];
    titleLabel.textColor=[UIColor colorWithHexString:@"#4a4a4a"];
    titleLabel.font=kFontSize14;
    titleLabel.numberOfLines=0;
    [self addSubview:titleLabel];
    
    //
    typeNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.origin.x, titleLabel.origin.y+titleLabel.height+1, titleLabel.width, 18)];
    typeNameLabel.textColor=[UIColor colorWithHexString:@"#a0a0a0"];
    typeNameLabel.font=kFontSize13;
    [self addSubview:typeNameLabel];
    
    //
    priceLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(titleLabel.origin.x, imgv.origin.y+imgv.height-20, titleLabel.width, 18)];
    priceLabel.font=kFontSize12;
    priceLabel.textColor=[UIColor colorWithHexString:@"#ed4523"];
    [self addSubview:priceLabel];
}

-(void)setModel:(HomePageIslandModel *)model{
    SDImageCache *imgCache=[SDImageCache sharedImageCache];
    if ([imgCache diskImageExistsWithKey:model.islandImg]) {
        UIImage *image = [imgCache imageFromDiskCacheForKey:model.islandImg];
        imgv.image=image;
    }else{
        [imgv sd_setImageWithURL:[NSURL URLWithString:model.islandImg] placeholderImage:[UIImage imageNamed:@"DefaultImg_120x93"]];
    }
    
    titleLabel.text=model.islandTitle;
    typeNameLabel.text=[NSString stringWithFormat:@"帕劳旅游%@",model.lvType];
    if ([model.lvType isEqualToString:@"自由行"]) {
        NSString *stageStr=[NSString stringWithFormat:@"分期就行:￥%@/9期",model.islandPrice];
        [priceLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.islandPrice options:NSCaseInsensitiveSearch];
            
            UIFont *boldSystemFont = kFontSize14;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }else{
        NSString *stageStr=[NSString stringWithFormat:@"参考价:￥%@",model.totalPrice];
        [priceLabel setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
            NSRange sumRange = [[mutableAttributedString string] rangeOfString:model.totalPrice options:NSCaseInsensitiveSearch];
            
            UIFont *boldSystemFont = kFontSize14;
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
                CFRelease(font);
            }
            return mutableAttributedString;
        }];
    }
}

@end
