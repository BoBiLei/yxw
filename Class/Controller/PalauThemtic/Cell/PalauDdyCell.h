//
//  PalauDdyCell.h
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageIslandModel.h"

@interface PalauDdyCell : UICollectionViewCell

@property (nonatomic, retain) HomePageIslandModel *model;

@end
