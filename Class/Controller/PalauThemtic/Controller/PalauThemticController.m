//
//  PalauThemticController.m
//  youxia
//
//  Created by mac on 16/5/13.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PalauThemticController.h"
#import "NewThemticHeaderCell.h"
#import "HomePageReusableView.h"
#import "SearchViewController.h"
#import "StrategyDetailController.h"
#import "NewTSHeaderCell.h"     //新游记攻略顶部
#import "NewTSItem.h"           //新游记攻略item
#import "NewHotSellCell.h"
#import "YouXiaShowController.h"
#import "IslandOnlineController.h"
#import "NewMaldiThematiController.h"
#import "PalauThemticHotelCell.h"
#import "PalauDdyCell.h"
#import "PalauDistingceController.h"
#import "ThemticStragoryController.h"
#import "DistingcOrtherListController.h"

#define cellWidth (SCREENSIZE.width-32)/3
#define NavigaBar_Height 64
@interface PalauThemticController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HomePageReusableViewDelegate,ThemticBarnerEventDelegate,ThemticRecommendDelegate>

@end

@implementation PalauThemticController{
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    UIImageView *phImgv;
    CGFloat topContentInset;
    
    UIWebView *phoneView;
    NSURLRequest *phoneRequest;
    
    //
    NSMutableArray *hotSellArr;         //精选热卖
    NSMutableArray *jiudianArr;           //帕劳酒店
    NSMutableArray *ddyArr;             //当地游
    NSMutableArray *straHeaderArr;      //游记攻略header
    NSMutableArray *straArr;            //游记攻略
    
    
    UICollectionView *homePageCollection;
    
    HomePageReusableView *hpReusableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    topContentInset = IMAGEPLAY_HEIHGT;
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initCollectionView];
    
    [self setUpCustomSearBar];
    
    //
    NetWorkStatus *nws=[[NetWorkStatus alloc] init];
    nws.NetWorkStatusBlock=^(BOOL status){
        if (status) {
            [self requestHttpsForHomePage];
        }
    };
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    //
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    CGFloat phoneWidth=44;
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    CGFloat sViewX=back.origin.x+back.width+12;
    
    searchView=[[UIImageView alloc] initWithFrame:CGRectMake(sViewX, 26, SCREENSIZE.width-sViewX-phoneWidth-16, 31)];
    searchView.userInteractionEnabled=YES;
    searchView.alpha=0.9;
    searchView.layer.cornerRadius=2;
    searchView.backgroundColor=[UIColor colorWithHexString:@"#f5f5f5"];
    [self.view addSubview:searchView];
    UITapGestureRecognizer *searchTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewEvent)];
    [searchView addGestureRecognizer:searchTap];
    
    //
    UIImageView *tipImg=[[UIImageView alloc] initWithFrame:CGRectMake(8, 6, searchView.height-12, searchView.height-12)];
    tipImg.image=[UIImage imageNamed:@"search_tip"];
    [searchView addSubview:tipImg];
    
    UILabel *pal=[[UILabel alloc] initWithFrame:CGRectMake(tipImg.origin.x+tipImg.width+6, 0, searchView.width-(tipImg.origin.x+tipImg.width+12), searchView.height)];
    pal.font=kFontSize14;
    pal.text=@"目的地/岛屿/酒店";
    pal.alpha=0.7f;
    pal.textColor=[UIColor colorWithHexString:@"#71808c"];
    [searchView addSubview:pal];
    
    //
    phImgv=[[UIImageView alloc] initWithFrame:CGRectMake(cusBar.width-searchView.height-10, searchView.origin.y, searchView.height, searchView.height)];
    phImgv.userInteractionEnabled=YES;
    phImgv.image=[UIImage imageNamed:@"search_phone"];
    phImgv.userInteractionEnabled=YES;
    [self.view addSubview:phImgv];
    UITapGestureRecognizer *phTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
    [phImgv addGestureRecognizer:phTap];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 搜索栏/电话点击事件
-(void)searchViewEvent{
    SearchViewController *searCtr=[[SearchViewController alloc]init];
    searCtr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searCtr animated:YES];
}

-(void)callPhone{
    if (phoneView == nil) {
        phoneView = [[UIWebView alloc] init];
        phoneView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    phoneRequest=[NSURLRequest requestWithURL:url];
    [self.view addSubview:phoneView];
    [phoneView loadRequest:phoneRequest];
}

#pragma mark - init CollectionView
-(void)initCollectionView{
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    
    homePageCollection=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-49) collectionViewLayout:myLayout];
    homePageCollection.hidden=YES;
    homePageCollection.showsVerticalScrollIndicator=NO;
    [homePageCollection registerNib:[UINib nibWithNibName:@"PalauDdyCell" bundle:nil] forCellWithReuseIdentifier:@"PalauDdyCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"PalauThemticHotelCell" bundle:nil] forCellWithReuseIdentifier:@"PalauThemticHotelCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSItem" bundle:nil] forCellWithReuseIdentifier:@"NewTSItem"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewTSHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"NewTSHeaderCell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewThemticHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"headercell"];
    [homePageCollection registerNib:[UINib nibWithNibName:@"NewHotSellCell" bundle:nil] forCellWithReuseIdentifier:@"NewHotSellCell"];
    homePageCollection.backgroundColor=[UIColor colorWithHexString:@"#d7e9f5"];
    homePageCollection.dataSource=self;
    homePageCollection.delegate=self;
    
    [homePageCollection registerNib:[UINib nibWithNibName:@"HomePageReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview"];
    
    [self.view addSubview:homePageCollection];
}


#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 6;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else if (section==1){
        return hotSellArr.count;
    }else if (section==2){
        return jiudianArr.count;
    }else if (section==3){
        return ddyArr.count;
    }else if (section==4){
        return straHeaderArr.count;
    }else{
        return straArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        NewThemticHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"headercell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewThemticHeaderCell alloc]init];
        }
        cell.barnerClickDelegate=self;
        cell.themticRecommendDelegate=self;
        return cell;
    }else if (indexPath.section==1){
        NewHotSellCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewHotSellCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewHotSellCell alloc]init];
        }
        HomePageIslandModel *model=hotSellArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==2){
        PalauThemticHotelCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PalauThemticHotelCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[PalauThemticHotelCell alloc]init];
        }
        HomePageIslandModel *model=jiudianArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==3){
        PalauDdyCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PalauDdyCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[PalauDdyCell alloc]init];
        }
        HomePageIslandModel *model=ddyArr[indexPath.row];
        cell.model=model;
        return cell;
    }else if (indexPath.section==4){
        NewTSHeaderCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSHeaderCell" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSHeaderCell alloc]init];
        }
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        cell.model=model;
        return cell;
    }else{
        NewTSItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NewTSItem" forIndexPath:indexPath];
        if (cell==nil) {
            cell=[[NewTSItem alloc]init];
        }
        HomepageStrategyModel *model=straArr[indexPath.row];
        cell.model=model;
        return cell;
    }
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return CGSizeMake(SCREENSIZE.width, Themtic_IMAGEPLAY_HEIHGT);
    }else if(indexPath.section==1){
        return CGSizeMake((SCREENSIZE.width)/2-14, IS_IPHONE5?130:150);
    }else if(indexPath.section==2){
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3-12);
    }else if (indexPath.section==3){
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/(5/2)+48);
    }else if (indexPath.section==4){
//        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/2-12);
        return CGSizeMake(SCREENSIZE.width, SCREENSIZE.width/3+29);
    }else{
        return CGSizeMake(SCREENSIZE.width, (SCREENSIZE.width-32)/3-18);
    }
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//每个item之间的间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section==2||section==3||section==4||section==5) {
        return 0.0f;
    }else{
        return 9.0f;
    }
}

//定义每个section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    switch (section) {
        case 0:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        case 1:
            return UIEdgeInsetsMake(10, 9, 10, 9);
            break;
        case 2:
            return UIEdgeInsetsMake(0, 0, 16, 0);
            break;
        case 3:
            return UIEdgeInsetsMake(0, 0, 12, 0);
            break;
        case 4:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        default:
            return UIEdgeInsetsMake(0, 0, 12, 0);
            break;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==1) {
        HomePageIslandModel *model=hotSellArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=PalauIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==2){
        HomePageIslandModel *model=jiudianArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=PalauIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==3){
        HomePageIslandModel *model=ddyArr[indexPath.row];
        IslandDetailController *islandDetail=[[IslandDetailController alloc]init];
        islandDetail.islandId=model.islandId;
        islandDetail.islandImg=model.islandImg;
        islandDetail.islandPId=model.islandPId;
        islandDetail.mdId=model.mdId;
        islandDetail.islandName=model.islandTitle;
        islandDetail.islandDescript=model.islandDescript;
        islandDetail.detail_landType=PalauIslandType;
        [self.navigationController pushViewController:islandDetail animated:YES];
    }else if (indexPath.section==4){
        HomepageStrategyModel *model=straHeaderArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if (indexPath.section==5){
        HomepageStrategyModel *model=straArr[indexPath.row];
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.strateId;
        strCtr.cId=model.cId;
        strCtr.strategyTitle=model.strateTitle;
        [self.navigationController pushViewController:strCtr animated:YES];
    }
}

//设置reusableview
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    hpReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableview" forIndexPath:indexPath];
    [hpReusableView reflushHotSellerDataForIndexPath:indexPath];
    if (indexPath.section==1) {
        hpReusableView.reusableId=@"11";
    }else if (indexPath.section==2) {
        hpReusableView.reusableId=@"22";
    }else if (indexPath.section==3) {
        hpReusableView.reusableId=@"33";
    }else if (indexPath.section==4) {
        hpReusableView.reusableId=@"44";
    }
    hpReusableView.delegate=self;
    reusableview = hpReusableView;
    return reusableview;
}

//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (section==0||section==5) {
        return CGSizeMake(0, 0);
    }else if(section==1){
        return CGSizeMake(SCREENSIZE.width, 46.3f);
    }else{
        return CGSizeMake(SCREENSIZE.width, 56);
    }
}

#pragma mark - HeaderviewCategory delegate
-(void)clickCollectionItem:(NSIndexPath *)indexPath{
    
}

#pragma mark - 点击Barner广告 Delegate
//广告
-(void)clickThemticBarner:(ImageModel *)model{
    if ([model.advType isEqualToString:@"maldives"]) {
        NewMaldiThematiController *malCtr=[NewMaldiThematiController new];
        [self.navigationController pushViewController:malCtr animated:YES];
    }else if ([model.advType isEqualToString:@"palau"]) {
        PalauThemticController *plCtr=[PalauThemticController new];
        [self.navigationController pushViewController:plCtr animated:YES];
    }else if ([model.advType isEqualToString:@"show"]) {
        YouXiaShowController *ctr=[YouXiaShowController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=AllStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"zxxd"]) {
        IslandOnlineController *ctr=[IslandOnlineController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"links"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",model.imgId]]];
    }else if ([model.advType isEqualToString:@"strategy_maldives"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"马尔代夫游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=MaldStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_palau"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=model.imgId;
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi"]) {
        YouXiaStageController *ctr=[YouXiaStageController new];
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.advType isEqualToString:@"strategy_show"]) {
        StrategyDetailController *strCtr=[StrategyDetailController new];
        strCtr.idStr=model.imgId;
        strCtr.cId=@"234";
        strCtr.strategyTitle=@"title";
        [self.navigationController pushViewController:strCtr animated:YES];
    }else if ([model.advType isEqualToString:@"fenqi_list"]) {
        
    }
}

//推荐
-(void)clickThemticRecommend:(ThemticRecommendModel *)model{
    DSLog(@"%@",model.url);
    if ([model.name isEqualToString:@"酒店"]) {
        PalauDistingceController *ctr=[PalauDistingceController new];
        ctr.titleStr=model.name;
        ctr.urlStr=model.url;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"当地游"]){
        PalauDistingceController *ctr=[PalauDistingceController new];
        ctr.titleStr=model.name;
        ctr.urlStr=model.url;
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([model.name isEqualToString:@"帕劳攻略"]){
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=model.url;
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}


#pragma mark - Reusableview delegate
-(void)clickMore:(NSString *)strId{
    if ([strId isEqualToString:@"11"]) {
        DistingcOrtherListController *distShow=[[DistingcOrtherListController alloc]init];
        distShow.reveivleftTag=0;
        distShow.reveivRightTag=1;
        distShow.urlStr=@"index.php?m=wap&c=index&a=list_mudi_v6_ios&mudi=pl&is_iso=1";
//        DSLog(@"%ld--%ld--%@",leftTag,rightTag,model.url);
        [self.navigationController pushViewController:distShow animated:YES];
    }else if ([strId isEqualToString:@"22"]) {
        PalauDistingceController *ctr=[PalauDistingceController new];
        ctr.titleStr=@"酒店";
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&a=list_mudi_v6_ios&mudi=pl&lvtype=zyx&is_iso=1";
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"33"]) {
        PalauDistingceController *ctr=[PalauDistingceController new];
        ctr.titleStr=@"当地游";
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&a=list_mudi_v6_ios&mudi=pl&lvtype=ddy&is_iso=1";
        [self.navigationController pushViewController:ctr animated:YES];
    }else if ([strId isEqualToString:@"44"]) {
        ThemticStragoryController *ctr=[ThemticStragoryController new];
        ctr.titleStr=@"帕劳游记攻略";
        ctr.urlStr=@"http://m.youxia.com/index.php?m=wap&c=index&a=list_strategy_v6_ios&mudi=pl&is_iso=1";
        ctr.type=PalauStragoryType;
        [self.navigationController pushViewController:ctr animated:YES];
    }
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offsetY = scrollView.contentOffset.y+homePageCollection.contentInset.top;
    
    if (offsetY<0) {
        searchView.hidden=YES;
        phImgv.hidden=YES;
    }else{
        searchView.hidden=NO;
        phImgv.hidden=NO;
    }
    
    if (offsetY<-44) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((Themtic_IMAGEPLAY_HEIHGT-NavigaBar_Height - offsetY) / topContentInset));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForHomePage{
    [AppUtils showProgressMessage:@"Loading…" inView:self.view];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [self requestHttpsSignKey];
    }else{
        hotSellArr=[NSMutableArray array];
        jiudianArr=[NSMutableArray array];
        ddyArr=[NSMutableArray array];
        straHeaderArr=[NSMutableArray array];
        straArr=[NSMutableArray array];
        [[NetWorkRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"index.php?m=wap&c=index&a=index_palau_v6_ios&is_iso=1"] method:HttpRequestPost parameters:nil prepareExecute:^{
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
//            DSLog(@"%@",responseObject[@"retData"]);
            
            [AppUtils dismissHUDInView:self.view];
            
            homePageCollection.hidden=NO;
            
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                //banner
                NSArray *imgArr=responseObject[@"retData"][@"banner"];
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitPageImageNoti object:imgArr];
                
                //tuijianwei
                NSArray *tjwArr=responseObject[@"retData"][@"tuijianwei"];
                NSMutableArray *headTjwArr=[NSMutableArray array];
                for (NSDictionary *dic in tjwArr) {
                    ThemticRecommendModel *model=[ThemticRecommendModel new];
                    model.model=dic;
                    [headTjwArr addObject:model];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:ThemitTJWImageNoti object:headTjwArr];
                
                //精选热卖
                NSArray *hsArr=responseObject[@"retData"][@"remai"];
                for (NSDictionary *dic in hsArr) {
                    HomePageIslandModel *model=[HomePageIslandModel new];
                    model.model=dic;
                    [hotSellArr addObject:model];
                }
                
                //酒店
                NSArray *jdArr=responseObject[@"retData"][@"jiudian"];
                DSLog(@"%ld",(unsigned long)jdArr.count);
                for (NSDictionary *dic in jdArr) {
                    HomePageIslandModel *model=[HomePageIslandModel new];
                    model.model=dic;
                    [jiudianArr addObject:model];
                }
                
                //当地游
                NSArray *dyArr=responseObject[@"retData"][@"ddy"];
                for (NSDictionary *dic in dyArr) {
                    HomePageIslandModel *model=[HomePageIslandModel new];
                    model.model=dic;
                    [ddyArr addObject:model];
                }
                
                //游记攻略
                NSArray *glArr=responseObject[@"retData"][@"strategy"];
                DSLog(@"%@",glArr);
                for (int i=0; i<glArr.count; i++) {
                    NSDictionary *dic=glArr[i];
                    if (i==0) {
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straHeaderArr addObject:model];
                    }else{
                        HomepageStrategyModel *model=[HomepageStrategyModel new];
                        model.model=dic;
                        [straArr addObject:model];
                    }
                }
                
            }
            [homePageCollection reloadData];
            [homePageCollection.mj_header endRefreshing];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            DSLog(@"%@",error);
        }];
    }
}

//重新获取key
-(void)requestHttpsSignKey{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:@"1" forKey:@"sign_refresh"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    if ([[AppUtils getValueWithKey:@"new"] isEqualToString:@"1"]) {
        [dict setObject:@"1" forKey:@"sign_new"];
    }
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[NetWorkRequest defaultClient] requestWithPath:[NewYXHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *sign=responseObject[@"retData"][@"key"];
            [AppUtils saveValue:sign forKey:Sign_Key];
            [AppUtils saveValue:responseObject[@"retData"][@"signsn"] forKey:Verson_Key];
            [AppUtils saveValue:@"0" forKey:@"new"];
            [self requestHttpsForHomePage];
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}

@end
