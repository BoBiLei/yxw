//
//  PalauDistingceController.m
//  youxia
//
//  Created by mac on 16/4/29.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "PalauDistingceController.h"
#import "SearchViewController.h"
#import "NewIslandOnlineCell.h"
#import "DistingDropDownMenu.h"
#import "IslandDetailController.h"

@interface PalauDistingceController ()<UITableViewDataSource,UITableViewDelegate>


@end

@implementation PalauDistingceController{
    
    UINavigationBar *cusBar;
    UIImageView *searchView;
    
    NSArray *starLevelArr;
    NSArray *travelPlanArr;
    NSArray *goIslandTypeArr;
    
    //
    NSMutableArray *dataArr;
    UITableView *myTable;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BaseNavigationBar *navBar=[[BaseNavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    navBar.titleStr=[NSString stringWithFormat:@"帕劳%@",self.titleStr];
    [self.view addSubview:navBar];
    
    [self setUpTableView];
    
    [self requestDistingceData];
}

#pragma mark - init UI
-(void)setUpTableView{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewIslandOnlineCell *cell=[[NewIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    return cell.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.000001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewIslandOnlineCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[NewIslandOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    DistinceShowModel *model=dataArr[indexPath.row];
    cell.model=model;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DistinceShowModel *model=dataArr[indexPath.row];
    
    IslandDetailController *islDetail=[[IslandDetailController alloc] init];
    if ([model.islandYwName isEqualToString:@"帕劳"]) {
        islDetail.detail_landType=PalauIslandType;
    }else{
        islDetail.detail_landType=MaldivesIslandType;
    }
    islDetail.islandName=model.islandTitle;
    islDetail.islandId=model.islandId;
    islDetail.islandImg=model.islandImg;
    islDetail.islandPId=model.pId;
    islDetail.mdId=model.mdId;
    islDetail.islandDescript=model.islandTitle;
    [self.navigationController pushViewController:islDetail animated:YES];
}

#pragma mark - 重绘分割线

-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestDistingceData{
    dataArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    [[NetWorkRequest defaultClient] requestWithPath:self.urlStr method:HttpRequestPost parameters:nil prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        DSLog(@"%@",responseObject);
        [AppUtils dismissHUDInView:self.view];
        
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"data"];
            for (NSDictionary *dic in arr) {
                DistinceShowModel *model=[DistinceShowModel new];
                [model jsonDataForDictionary:dic];
                model.islandYwName=responseObject[@"retData"][@"dy"];
                [dataArr addObject:model];
            }
            [myTable reloadData];
        }else{
            DSLog(@"请求数据异常");
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DSLog(@"%@",error);
    }];
}


@end
