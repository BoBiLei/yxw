//
//  HotelDetailPriceCell.h
//  youxia
//
//  Created by mac on 2017/5/16.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelDetailPriceModel.h"

@protocol HotelDetailPriceDelegate <NSObject>

-(void)clickBuyBtn:(HotelDetailPriceModel *)model;

@end

@interface HotelDetailPriceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgv;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) id<HotelDetailPriceDelegate> delegate;
@property (strong, nonatomic) HotelDetailPriceModel *model;

-(void)reflushModel:(HotelDetailPriceModel *)model;

@end
