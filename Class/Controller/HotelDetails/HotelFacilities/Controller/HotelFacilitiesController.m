//
//  HotelFacilitiesController.m
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelFacilitiesController.h"
#import "HotelFacilitiesCell.h"
#import "HoteldetailsModel.h"
@interface HotelFacilitiesController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation HotelFacilitiesController
{
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCustomSearBar];
    
    UIView *views=[[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, 44)];
    [views setBackgroundColor:[UIColor whiteColor]];
    UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 150, 44)];
    lab.text=@"酒店设施";
    [lab setFont:[UIFont systemFontOfSize:17]];
    lab.textColor=[UIColor colorWithRed:85.0/255 green:178.0/255 blue:240.0/255 alpha:1];
    [views addSubview:lab];
    [self.view addSubview:views];
    
    [self setUpTableView];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    
    
    [self.navigationController.navigationBar setHidden:YES];
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [statusBarView setBackgroundColor:[UIColor colorWithHexString:@"0274BC"]];
    [self.view addSubview:statusBarView];
    
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    label.font=kFontSize17;
    label.textAlignment=NSTextAlignmentCenter;
    label.text=@"酒店设施";
    label.textColor=[UIColor whiteColor];
    [self.view addSubview:label];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    
}
-(void)setUpTableView
{
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 108, SCREENSIZE.width, SCREENSIZE.height-108) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
    myTable.delegate=self;
    myTable.dataSource=self;
    [myTable registerNib:[UINib nibWithNibName:@"HotelFacilitiesCell" bundle:nil]forCellReuseIdentifier:@"HotelFacilitiesCell"];
    [self.view addSubview:myTable];
}


-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotelFacilitiesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelFacilitiesCell"];
    if (!cell) {
        cell=[[HotelFacilitiesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelFacilitiesCell"];
        return cell;
    }
    HoteldetailsModel *model=_nameArr[indexPath.row];
    cell.titleName.font=kFontSize16;
    cell.titleName.text=model.name_cn;
    
    cell.selectionStyle=UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _nameArr.count;
}

- ( CGFloat )tableView:( UITableView *)tableView heightForHeaderInSection:(NSInteger )section{
    return 1.0;
}



@end
