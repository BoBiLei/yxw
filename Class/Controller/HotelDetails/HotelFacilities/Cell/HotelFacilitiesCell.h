//
//  HotelFacilitiesCell.h
//  youxia
//
//  Created by mac on 2017/3/29.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelFacilitiesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UIImageView *imgs;

@end
