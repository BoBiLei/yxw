//
//  HoteImgsListController.m
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HoteImgsListController.h"
#import "AVCollectionCell.h"
@interface HoteImgsListController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation HoteImgsListController
{
    NSMutableArray *showModels;
    NSMutableArray *goodsList;
    NSMutableArray *photoArr;

    NSMutableArray  *prototypeimage;
    UICollectionView *photoCollect;
    UIScrollView *myScrolview;


}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self seNavBar];
    [self setUpUI];
    [self HoteimageRequest];
}
-(void)seNavBar{
    
    //必须设置为Hidden，不然自定义Bar不能点击
    [self.navigationController.navigationBar setHidden:YES];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    
    
    [statusBarView setBackgroundColor:[UIColor colorWithRed:70/255.0 green:161/255.0 blue:236/255.0 alpha:1]];
    [self.view addSubview:statusBarView];
    
    UIButton    *fanHuiButton=[UIButton buttonWithType:UIButtonTypeCustom];
    fanHuiButton.frame=CGRectMake(10, 27,60,30);
    [fanHuiButton setImage:[UIImage imageNamed:@"yy_intro_back"] forState:UIControlStateNormal];
    fanHuiButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [fanHuiButton setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [fanHuiButton addTarget:self action:@selector(fanhui) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fanHuiButton];
    
    UILabel *btnlab  = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    btnlab.textAlignment=NSTextAlignmentCenter;
    btnlab.text=@"酒店图片";
    [btnlab setTextColor:[UIColor whiteColor]];
    [self.view addSubview:btnlab];
    
}
-(void)fanhui
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpUI{
    //
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    photoCollect=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:myLayout];
    [photoCollect registerNib:[UINib nibWithNibName:@"AVCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"AVCollectionCell"];
    photoCollect.backgroundColor=[UIColor clearColor];
    photoCollect.dataSource=self;
    photoCollect.delegate=self;
    photoCollect.bounces=YES;
    [self.view addSubview:photoCollect];
}

#pragma mark -酒店图片接口
-(void)HoteimageRequest{
    [AppUtils showProgressInView:self.view];
    photoArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp=[NSString stringWithFormat:@"%ld",now];
    NSString *nonce_str=[YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"hotelPics" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [dict setObject:_hoteid forKey:@"ID"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            //原始大小图数组
            id photoObj=responseObject[@"retData"];
            if ([photoObj isKindOfClass:[NSArray class]]) {
                for (NSString *str in photoObj) {
                    if (![str isEqualToString:@""]||str!=nil) {
                        [photoArr addObject:str];
                    }
                }
            }
            [photoCollect reloadData];
        }
        DSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@",error);
    }];
}


#pragma mark - 数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return photoArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AVCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"AVCollectionCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[AVCollectionCell alloc]init];
    }
    NSString *str=photoArr[indexPath.row];
    cell.imgv.clipsToBounds  = YES;
    cell.imgv.contentMode=UIViewContentModeScaleAspectFill;
    [cell.imgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:Image_Default]];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth=(SCREENSIZE.width-40)/3;
    
    return CGSizeMake(cellWidth, cellWidth-30);
}

- (CGFloat)minimumInteritemSpacing {
    return 0.0f;
}

//设置行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
        return 10;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 8, 10, 8);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *imgArr=[NSMutableArray array];
    for (NSString *img in photoArr) {
        NSURL *url=[NSURL URLWithString:img];
        [imgArr addObject:url];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:imgArr];
    browser.displayToolbar=YES;
    browser.displayCounterLabel=YES;
    browser.displayActionButton = NO;
    [browser setInitialPageIndex:indexPath.row];
    [self presentViewController:browser animated:YES completion:nil];
}

@end
