//
//  HoteImgsListController.h
//  youxia
//
//  Created by mac on 2017/5/4.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "BaseController.h"

@interface HoteImgsListController : UIViewController
@property(nonatomic,copy)NSString *hoteid;
@property(nonatomic,copy)NSMutableArray *hoteImgarr;
@end
