//
//  HotelDetailsController.m
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "HotelDetailsController.h"
#import "HoteldetailsCell.h"
#import "HoteldetailsynopsisCell.h"
#import "WelcomeController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIEditorViewStyle.h>
#import "HoteldetailsModel.h"
#import "UIScrollView+PullScale.h"
#import "HotejiejTableViewCell.h"
#import "StayindetailsCell.h"
#import "DetailsXCell.h"
#import "HoteTitleBTCell.h"
#import "HoteFacilitiesCell.h"
#import "HoteIntroductionCell.h"
#import "HotelImageCell.h"
#import "HotelReserveController.h"
#import "HotelFacilitiesController.h"
#import "HoteLintroductionController.h"
#import "HoteStandardController.h"
#import "HoteImgsListController.h"
#import "HotelDetailPriceCell.h"
#import "HoteldetailsModel.h"
#import "HotelDetailPriceModel.h"

#define NavigaBar_Height 64
#define BIG_IMG_WIDTH  325
#define BIG_IMG_HEIGHT 325
#define ExpandCount 1

@interface HotelDetailsController ()<UITableViewDelegate,UITableViewDataSource,HotelDetailPriceDelegate>

@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) UIProgressView *progressView;
@property(nonatomic,strong)NSString *hotenamedetails;
@property(nonatomic,strong)NSString *hotelatitude;
@property(nonatomic,strong)NSString *hotelongitude;
@property(nonatomic,strong)NSString *hoteInventoryCount;//库存
@property(nonatomic,strong)NSString *hoteMaxOccupancy;//最大入住人数

@property(nonatomic,strong)NSString *hotedefultImg; //酒店详情图
@property(nonatomic,strong)NSString *hoteImgcount; //酒店详情图数量


@property(nonatomic,strong)NSString *hoteaddressdetails;

@property (assign, nonatomic) BOOL isExpand; //是否展开
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;//展开的cell的下标

@property(nonatomic,strong)UITableView *myTable;

@end

@implementation HotelDetailsController
{
    NSString *hotelAddress;
    
    CGFloat topContentInset;
    UIView *background;
    
    //
    UIImageView *imgv;
    CGFloat itemW;
    CGFloat itemH;
    
    UINavigationBar *cusBar;
    UILabel *titleLabel;
    UIImageView *searchView;
    UIScrollView *myScrolview;
    NSMutableArray *dataArr;
    NSMutableArray *nameArr;
    NSArray *hoteRooms;
    NSArray *hotenamess;
    UIButton *btnsearch;
    UIImageView *imageView;
    
    NSString *hotelDesc;
}
@synthesize hoteimgarr;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"酒店详情页"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.rdv_tabBarController setTabBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [TalkingData trackPageEnd:@"酒店详情页"];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    hotelDesc=@"";
    
    [self hoteDataRequest];
    
    [self setUpCustomSearBar];
}

#pragma mark - 自定义NavBar
-(void)setUpCustomSearBar{
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    
    cusBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    [cusBar setShadowImage:[UIImage new]];
    [cusBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:cusBar];
    
    titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, SCREENSIZE.width-80, 44)];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.text=@"酒店详情";
    titleLabel.font=kFontSize17;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.hidden=YES;
    [self.view addSubview:titleLabel];
    
    //返回按钮
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(5, 20, 52.f, 44.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(turnBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    //分享
//        UIButton *shareBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-40, 25, 34, 34)];
//        [shareBtn setImage:[UIImage imageNamed:@"isld_share"] forState:UIControlStateNormal];
//        shareBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
//        [shareBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
//        [self.view addSubview:shareBtn];
//        [shareBtn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)turnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickShareBtn:(id)sender{
    
    //自定义视图标题颜色，字体大小
    [SSUIEditorViewStyle setTitleColor:[UIColor whiteColor]];
    [SSUIEditorViewStyle setTitle:@"分享到新浪"];
    
    //1、创建分享参数（必要）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSArray *imgArr=@[[NSString stringWithFormat:@"%@",hoteimgarr]];
    NSString *imgUrl;
    switch (_detail_landType) {
        case MaldivesIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/maldives/tours/%@.html",_hoteID];
            break;
        case BaliIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/bali/tours/%@.html",_hoteID];
            break;
        case PalauIslandType:
            imgUrl=[NSString stringWithFormat:@"http://m.youxia.com/palau/tours/%@.html",_hoteID];
            break;
        default:
            break;
    }
    [shareParams SSDKSetupShareParamsByText:_islandDescript
                                     images:imgArr
                                        url:[NSURL URLWithString:imgUrl]
                                      title:_islandName
                                       type:SSDKContentTypeAuto];
    // 2、分享
    [ShareSDK showShareActionSheet:self.view
                             items:nil
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   DSLog(@"%@",error);
                   switch (state) {
                           
                       case SSDKResponseStateSuccess:{
                           [AppUtils showSuccessMessage:@"分享成功！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateFail:{
                           [AppUtils showSuccessMessage:@"分享失败！" inView:self.view];
                           break;
                       }
                       case SSDKResponseStateCancel:{
                           
                           break;
                       }
                       default:
                           break;
                   }
               }];
}


#pragma mark -酒店详情接口
-(void)hoteDataRequest{
    [AppUtils showProgressInView:self.view];
    dataArr=[NSMutableArray array];
    nameArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [YXWSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"hotel" forKey:@"mod"];
    [dict setObject:@"hotelDetail" forKey:@"code"];
    [dict setObject:@"1" forKey:@"is_iso"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[YXWSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    NSDictionary *dic=@{@"HotelID":_hoteID,
                        @"checkin":_ruzhuDate,
                        @"checkout":_endDate,
                        @"adult":_crnum,
                        @"child":_etnum,
                        @"childAge":_etage
                        };
    NSData *data=[AppUtils toJSONData:dic];
    NSString *jsonString = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    [dict setObject:jsonString forKey:@"condition"];
    [[NetWorkRequest defaultClient] requestWithPath:[HoteHost stringByAppendingString:@"mgo/index.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSLog(@"酒店详情=================%@",responseObject);
        NSString *str=[NSString stringWithFormat:@"%@",responseObject[@"retCode"]];
        if ([str isEqualToString:@"1"]){
            _hotenamedetails=responseObject[@"retData"][@"HotelName"];
            hotelAddress=responseObject[@"retData"][@"Address"];
            _hotelatitude=responseObject[@"retData"][@"Latitude"];
            _hotelongitude=responseObject[@"retData"][@"Longitude"];
            _hotedefultImg=responseObject[@"retData"][@"defaultImg"];
            _hoteImgcount=responseObject[@"retData"][@"imgCount"];
            hotelDesc=responseObject[@"retData"][@"Desc"];
            NSArray *amenityArr=responseObject[@"retData"][@"Amenity"];
            if([amenityArr isKindOfClass:[NSNull class]]){
                
            }else{
                for (NSDictionary *dic in amenityArr) {
                    HoteldetailsModel *model=[HoteldetailsModel new];
                    model.model=dic;
                    [nameArr addObject:model];
                }
            }
            
            NSArray *roomArr=responseObject[@"retData"][@"rooms"];
            if ([roomArr isKindOfClass:[NSNull class]]) {
                [AppUtils showSuccessMessage:@"无房型数据" inView:self.view];
            }else{
                for (NSDictionary *dic in roomArr) {
                    HotelDetailPriceModel *model=[HotelDetailPriceModel new];
                    [model jsonToModel:dic];
                    [dataArr addObject:model];
                }
                [self.myTable reloadData];
            }
        }else{
            DSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        
        NSLog(@"%@",error);
    }];
    
}

-(UITableView *)myTable{
    if (!_myTable) {
        _myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height) style:UITableViewStyleGrouped];
        _myTable.backgroundColor=[UIColor colorWithHexString:@"f2f2f2"];
        _myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
        _myTable.delegate=self;
        _myTable.dataSource=self;
        [_myTable registerNib:[UINib nibWithNibName:@"HoteldetailsCell" bundle:nil]forCellReuseIdentifier:@"HoteldetailsCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HotejiejTableViewCell" bundle:nil]forCellReuseIdentifier:@"HotejiejTableViewCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"StayindetailsCell" bundle:nil]forCellReuseIdentifier:@"StayindetailsCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"DetailsXCell" bundle:nil]forCellReuseIdentifier:@"DetailsXCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HoteTitleBTCell" bundle:nil]forCellReuseIdentifier:@"HoteTitleBTCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HotelDetailPriceCell" bundle:nil]forCellReuseIdentifier:@"HotelDetailPriceCell"];
        
        [_myTable registerNib:[UINib nibWithNibName:@"HoteFacilitiesCell" bundle:nil]forCellReuseIdentifier:@"HoteFacilitiesCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HoteIntroductionCell" bundle:nil]forCellReuseIdentifier:@"HoteIntroductionCell"];
        [_myTable registerNib:[UINib nibWithNibName:@"HotelImageCell" bundle:nil]forCellReuseIdentifier:@"HotelImageCell"];
        
        [self.view addSubview:_myTable];
        
        [_myTable addPullScaleFuncInVC:self originalHeight:200 hasNavBar:NO];
        _myTable.bouncesZoom=NO;
        [_myTable.imageV sd_setImageWithURL:[NSURL URLWithString:_hotedefultImg] placeholderImage:[UIImage imageNamed:Image_Default]];
        _myTable.imageV.userInteractionEnabled=YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hotetaps:)];
        [_myTable.imageV addGestureRecognizer:tap];
        [self.view insertSubview:_myTable atIndex:0];
        
        btnsearch = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnsearch.frame=CGRectMake(SCREENSIZE.width-80, -30, 60, 21);
        [btnsearch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnsearch setTitle:[NSString stringWithFormat:@"%@张",_hoteImgcount] forState:UIControlStateNormal];
        btnsearch.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [btnsearch setBackgroundColor:[UIColor clearColor]];
        [_myTable addSubview:btnsearch];
        
        //
        _myTable.tableFooterView=[UIView new];
    }
    return _myTable;
}

#pragma mark - 点击头部图片
-(void)hotetaps:(UITapGestureRecognizer*)sender{
    
    HoteImgsListController *img=[[HoteImgsListController alloc]init];
    img.hoteid=_hoteID;
    [self.navigationController pushViewController:img animated:YES];
}


-(void)btnsearchclicked:(id)send{
    //创建一个黑色背景
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    background = bgView;
    [bgView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:bgView];
    
    //创建显示图像的视图
    //初始化要显示的图片内容的imageView（这里位置继续偷懒...没有计算）
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,150, SCREENSIZE.width, BIG_IMG_HEIGHT)];
    //要显示的图片，即要放大的图片
    [imgView setImage:[UIImage imageNamed:@"组-1"]];
    [bgView addSubview:imgView];
    
    imgView.userInteractionEnabled = YES;
    //添加点击手势（即点击图片后退出全屏）
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeView)];
    [imgView addGestureRecognizer:tapGesture];
    
    [self shakeToShow:bgView];//放大过程中的动画
}

#pragma mark - Table
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        HoteldetailsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteldetailsCell"];
        if (!cell){
            cell=[[HoteldetailsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteldetailsCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.nameLabel.text=_hotenamedetails;
        cell.addressLabel.text=hotelAddress;
        return cell;
        
    }else if (indexPath.section==1){
        if (indexPath.row==0){
            StayindetailsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"StayindetailsCell"];
            if (!cell) {
                cell=[[StayindetailsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StayindetailsCell"];
                return cell;
            }
            
            
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
            }
            NSString *dd=_ruzhuDate;
            NSString *aa=[dd substringWithRange:NSMakeRange(5,2)];
            NSString *bb=[dd substringWithRange:NSMakeRange(8, 2)];
            UILabel *starData=[[UILabel alloc]initWithFrame:CGRectMake(10, 4, 120, 40)];
            starData.font=kFontSize16;
            starData.text=[NSString stringWithFormat:@"%@月%@日",aa,bb];
            
            
            UIImageView *imgview=[[UIImageView alloc]init];
            imgview.frame=CGRectMake(100, 26, 20, 16);
            [imgview setImage:[UIImage imageNamed:@"下一页-(2)"]];
            
            UIView *views=[[UIView alloc]initWithFrame:CGRectMake(135, 23, 0.5, 23)];
            [views setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
            
            
            NSString *end=_endDate;
            NSString *ed=[end substringWithRange:NSMakeRange(5,2)];
            NSString *nd=[end substringWithRange:NSMakeRange(8, 2)];
            UILabel *endData=[[UILabel alloc]initWithFrame:CGRectMake(150, 4, 120, 40)];
            
            endData.text=[NSString stringWithFormat:@"%@月%@日",ed,nd];
            
            UIImageView *imgview2=[[UIImageView alloc]init];
            imgview2.frame=CGRectMake(235, 26, 20, 16);
            [imgview2 setImage:[UIImage imageNamed:@"下一页-(2)"]];
            
            UIView *views2=[[UIView alloc]initWithFrame:CGRectMake(265, 23, 0.5, 23)];
            [views2 setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"]];
            
            
            UILabel *cr=[[UILabel alloc]initWithFrame:CGRectMake(285, 4, 100, 40)];
            cr.font=kFontSize16;
            cr.text=[NSString stringWithFormat:@"%@成人",_crnum];
            
            UILabel *et=[[UILabel alloc]initWithFrame:CGRectMake(285, 29, 100, 40)];
            et.font=kFontSize16;
            et.text=[NSString stringWithFormat:@"%@儿童",_etnum];
            
            [cell addSubview:cr];
            [cell addSubview:et];
            [cell addSubview:endData];
            [cell addSubview:starData];
            [cell addSubview:imgview];
            [cell addSubview:imgview2];
            [cell addSubview:views];
            [cell addSubview:views2];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            return cell;
        }else{
            HotelDetailPriceCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HotelDetailPriceCell"];
            if (!cell){
                cell=[[HotelDetailPriceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotelDetailPriceCell"];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            HotelDetailPriceModel *model=dataArr[indexPath.row-1];
            [cell reflushModel:model];
            cell.delegate=self;
            return cell;
        }
    }else if (indexPath.section==2)
    {
        if (indexPath.row==0) {
            HoteTitleBTCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteTitleBTCell"];
            if (!cell) {
                cell=[[HoteTitleBTCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteTitleBTCell"];
                return cell;
            }
            cell.hotelab.text=@"酒店设施";
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            return cell;
        }else
        {
            HoteFacilitiesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteFacilitiesCell"];
            if (!cell) {
                cell=[[HoteFacilitiesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteFacilitiesCell"];
                return cell;
            }
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            return cell;
        }
        
    }else{
        if (indexPath.row==0) {
            HoteTitleBTCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HoteTitleBTCell"];
            if (!cell) {
                cell=[[HoteTitleBTCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HoteTitleBTCell"];
                return cell;
            }
            cell.hotelab.text=@"酒店介绍";
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            return cell;
        }else{
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (!cell) {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            }
            cell.textLabel.numberOfLines=0;
            cell.textLabel.font=kFontSize14;
            cell.textLabel.textColor=[UIColor colorWithHexString:@"ababab"];
            cell.selectionStyle=UITableViewCellSeparatorStyleNone;
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            NSString *labelTtt=hotelDesc;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelTtt];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:4];//调整行间距
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelTtt length])];
            cell.textLabel.attributedText = attributedString;
            return cell;
        }
    }
}

-(void)closeView{
    [background removeFromSuperview];
}
//放大过程中出现的缓慢动画
- (void) shakeToShow:(UIView*)aView{
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.3;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aView.layer addAnimation:animation forKey:nil];
}

- (NSArray *)indexPathsForExpandRow:(NSInteger)row {
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 1; i <= ExpandCount; i++) {
        NSIndexPath *idxPth = [NSIndexPath indexPathForRow:row + i inSection:1];
        [indexPaths addObject:idxPth];
    }
    return [indexPaths copy];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return 1;
    }else if (section==1){
        return dataArr.count+1;
    }
    else if (section==2){
        return 2;
    }
    else{
        return 2;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        
    }else if (indexPath.section==1)
    {
        if (indexPath.row==0) {
        }else{
        }
    }else if (indexPath.section==2){
        if (indexPath.row==1) {
            HotelFacilitiesController *dd=[[HotelFacilitiesController alloc]init];
            dd.nameArr=nameArr;
            [self.navigationController pushViewController:dd animated:YES];
        }
    }else if (indexPath.section==3){
        if (indexPath.row==1){
            HoteLintroductionController *alint=[[HoteLintroductionController alloc]init];
            alint.hoteid=_hoteID;
            [self.navigationController pushViewController:alint animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 132;
    }else if (indexPath.section==1){
        if (indexPath.row==0) {
            return 69;
        }else
        {
            return 74;
        }
    }else if (indexPath.section==2){
        if (indexPath.row==0) {
            return 44;
        }else{
            return 74;
        }
    }else{
        if (indexPath.row==0) {
            return 44;
        }else{
            return 94;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?0.0000001:8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==3?16:10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [hotelDesc isEqualToString:@""]?3:4;
}

#pragma mark - 点击预定按钮
-(void)clickBuyBtn:(HotelDetailPriceModel *)model{
    //跳转到酒店准备预定页面
    HotelReserveController *vc=[[HotelReserveController alloc]init];
    vc.hoteid=_hoteID;
    vc.hoteInventoryCount=model.InventoryCount;
    vc.max_Occupancy=model.MaxOccupancy;
    vc.max_Child=model.MaxChild;
    vc.hoteName=_hotename;
    vc.starDate=_ruzhuDate;
    vc.endDate=_endDate;
    vc.numDay=_numday;
    vc.rateplanId=model.RatePlanID;
    vc.rateplanName=model.RatePlanName;
    vc.totalPrice=model.TotalPrice;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([_myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [_myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([_myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [_myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY<0) {
        searchView.hidden=YES;
        titleLabel.hidden=YES;
    }else{
        searchView.hidden=NO;
        titleLabel.hidden=NO;
    }
    //
    UIColor * color = [[UIColor colorWithHexString:@"#0080c5"] colorWithAlphaComponent:1];
    
    CGFloat alpha = MIN(1, 1 - ((NavigaBar_Height - offsetY) / 200));
    
    if (offsetY > 0) {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
        if (alpha==1) {
            titleLabel.hidden=NO;
        }else{
            titleLabel.hidden=YES;
        }
    } else {
        [cusBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
    }
}

@end
