//
//  HotelDetailsController.h
//  youxia
//
//  Created by mac on 2017/3/15.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <UIKit/UIKit.h>
//酒店主题类型
typedef NS_ENUM(NSUInteger, HotelandType) {
    MaldivesslandType,
    BaliIsndType,
    PalauIlandType
};

@interface HotelDetailsController : BaseController

@property (nonatomic,copy) NSString *islandId;
@property (nonatomic,copy) NSString *islandImg;
@property (nonatomic,copy) NSString *islandName;
@property (nonatomic,copy) NSString *islandDescript;
@property (nonatomic,copy) NSString *islandPId;
@property (nonatomic,copy) NSString *mdId;
@property (nonatomic,copy) NSString *nav_supercarTitle;
@property (nonatomic) IslandType detail_landType;



@property (nonatomic,copy)NSString *hoteID;
@property (nonatomic,copy)NSString *hotename;
@property (nonatomic,copy)NSString *hoteaddress;
@property (nonatomic,copy)NSString *ruzhuDate;
@property (nonatomic,copy)NSString *endDate;
@property (nonatomic,copy)NSString *numday;
@property (nonatomic,copy)NSString *crnum;
@property (nonatomic,copy)NSString *etnum;
@property (nonatomic,copy)NSString *etage;
@property(nonatomic,retain)NSMutableArray *hoteimgarr;


@end
