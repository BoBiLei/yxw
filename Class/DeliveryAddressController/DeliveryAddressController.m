//
//  DeliveryAddressController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "DeliveryAddressController.h"
#import "DeliveryAddressCell.h"
#import "SelectCityView.h"
#import "CityModel.h"
#import "CityFmdb.h"

NSString *addressidentifier=@"cell";

@interface DeliveryAddressController ()<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

//view
@property (strong, nonatomic) UIPickerView *myPicker;
@property (strong, nonatomic) UIView *maskView;
@property (strong, nonatomic) UIView *pickerBgView;


@property (nonatomic, strong) void (^CityIdsArrayBlock)(NSMutableArray *cityIds);

@end

@implementation DeliveryAddressController{
    
    NSMutableArray *dataArr;
    UITableView *myTableView;
    NSIndexPath *selectedIndexPath;
    
    UITextField *receiverText;
    UITextField *phoneNumText;
    UITextField *addressText;
    UITextField *adrDetailText;
    UIButton *button;
    
    NSMutableArray *provinceArr;//省份
    NSMutableArray *cityArr;    //市
    NSMutableArray *districtArr;//区
    
    NSString *provinceId;
    NSString *cityId;
    NSString *districtId;
    
    NSArray *provinceArray;
    NSArray *testCityArr001;
    NSArray *testDistrictArr001;
    NSArray *selectedArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"收货地址"];
    
    [self getPickerData];
    
    [self setUpUI];
    
    [self httpAddressList];
}

#pragma mark - get Addressdata
- (void)getPickerData {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    
    //得到一个省份的全部省份
    provinceArray=[db getProvince];
    
    //设置默认省份的市
    CityModel *model001=provinceArray[0];
    testCityArr001=[db getCityWithId:model001.parent_id];
    
    //设置默认市的区
    CityModel *model002=testCityArr001[0];
    testDistrictArr001=[db getCityWithId:model002.parent_id];
}

#pragma mark - init view
-(void)setUpUI{
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.userInteractionEnabled=YES;
    bagView.image=[UIImage imageNamed:@"login_baground"];
    
    myTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    [myTableView registerNib:[UINib nibWithNibName:@"DeliveryAddressCell" bundle:nil] forCellReuseIdentifier:addressidentifier];
    [self.view addSubview:myTableView];
    myTableView.dataSource=self;
    myTableView.delegate=self;
    myTableView.backgroundView=bagView;
    
    //init pickview
    self.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height)];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = 0;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker)]];
    
    self.pickerBgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 236)];
    self.pickerBgView.backgroundColor=[UIColor whiteColor];
    
    //tool view
    UIView *pickerTopBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, SCREENSIZE.width, 44.0)];
    [self.pickerBgView addSubview:pickerTopBarView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:pickerTopBarView.frame];
    [pickerTopBarView addSubview:toolBar];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(SCREENSIZE.width-60, 4, 40, 40)];
    [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
    [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
    [closeBtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [closeBtn addTarget:self action:@selector(hideMyPicker) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    toolBar.items = @[flexibleSpace, barButtonItem];
    
    //pickerview
    _myPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, pickerTopBarView.height, SCREENSIZE.width, _pickerBgView.height-pickerTopBarView.height)];
    [_myPicker setDelegate:self];
    [_myPicker setDataSource:self];
    _myPicker.userInteractionEnabled=YES;
    [self.pickerBgView addSubview:_myPicker];
    [self.pickerBgView bringSubviewToFront:_myPicker];
}

-(UIView *)setUpFootView{
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 244)];
    footView.backgroundColor=[UIColor whiteColor];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 0, footView.width, 0.5f)];
    line.alpha=0.7;
    line.backgroundColor=[UIColor lightGrayColor];
    [footView addSubview:line];
    
    //收件人View
    UIView *receiverView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, footView.frame.size.width-16, 39)];
    receiverView.layer.cornerRadius=2;
    receiverView.layer.borderWidth=0.5f;
    receiverView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [footView addSubview:receiverView];
    
    //receiverText
    receiverText=[UITextField new];
    receiverText.placeholder=@"收件人";
    receiverText.tag=0;
    receiverText.clearButtonMode=UITextFieldViewModeWhileEditing;
    [receiverText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    receiverText.font=[UIFont systemFontOfSize:15];
    receiverText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [receiverView addSubview:receiverText];
    receiverText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* receiverText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[receiverText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(receiverText)];
    [NSLayoutConstraint activateConstraints:receiverText_h];
    
    NSArray* receiverText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[receiverText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(receiverText)];
    [NSLayoutConstraint activateConstraints:receiverText_w];
    
    //电话号码View
    UIView *phoneNumView=[[UIView alloc]initWithFrame:CGRectMake(8, receiverView.frame.origin.y+receiverView.frame.size.height+8, footView.frame.size.width-16, 39)];
    phoneNumView.layer.cornerRadius=2;
    phoneNumView.layer.borderWidth=0.5f;
    phoneNumView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [footView addSubview:phoneNumView];
    
    //phoneNumText
    phoneNumText=[UITextField new];
    phoneNumText.placeholder=@"联系人电话";
    phoneNumText.tag=1;
    phoneNumText.clearButtonMode=UITextFieldViewModeWhileEditing;
    [phoneNumText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    phoneNumText.font=[UIFont systemFontOfSize:15];
    phoneNumText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [phoneNumView addSubview:phoneNumText];
    phoneNumText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* phoneNumText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[phoneNumText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(phoneNumText)];
    [NSLayoutConstraint activateConstraints:phoneNumText_h];
    
    NSArray* phoneNumText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[phoneNumText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(phoneNumText)];
    [NSLayoutConstraint activateConstraints:phoneNumText_w];
    
    //地址View
    UIView *addressView=[[UIView alloc]initWithFrame:CGRectMake(8, phoneNumView.frame.origin.y+phoneNumView.frame.size.height+8, footView.frame.size.width-16, 39)];
    addressView.layer.cornerRadius=2;
    addressView.layer.borderWidth=0.5f;
    addressView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [footView addSubview:addressView];
    UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showSeleteCityView)];
    [addressView addGestureRecognizer:tapGest];
    
    //addressText
    addressText=[UITextField new];
    addressText.enabled=NO;
    addressText.placeholder=@"联系人地址";
    addressText.tag=2;
    [addressText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    addressText.font=[UIFont systemFontOfSize:15];
    addressText.textColor=[UIColor colorWithHexString:@"#282828"];
    [addressView addSubview:addressText];
    addressText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* addressText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[addressText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(addressText)];
    [NSLayoutConstraint activateConstraints:addressText_h];
    
    NSArray* addressText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[addressText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(addressText)];
    [NSLayoutConstraint activateConstraints:addressText_w];
    
    __weak typeof(self) weakSelf = self;
    weakSelf.CityIdsArrayBlock = ^(NSMutableArray *cityBlockArr){
        NSString *provinceStr=@"";
        NSString *cityStr=@"";
        NSString *districtStr=@"";
        for (int i=0;i<3; i++) {
            CityModel *model=cityBlockArr[i];
            if (i==0) {
                provinceStr=[NSString stringWithFormat:@"%@",model.cityName];
            }else if (i==1){
                cityStr=[NSString stringWithFormat:@"%@",model.cityName];
            }else{
                districtStr=[NSString stringWithFormat:@"%@",model.cityName];
            }
        }
        if ([provinceStr isEqualToString:cityStr]) {
            addressText.text=[NSString stringWithFormat:@"%@市%@",cityStr,districtStr];
        }else{
            addressText.text=[NSString stringWithFormat:@"%@省%@市%@",provinceStr,cityStr,districtStr];
        }
    };
    
    //详细地址View
    UIView *adrDetailView=[[UIView alloc]initWithFrame:CGRectMake(8, addressView.frame.origin.y+addressView.frame.size.height+8, footView.frame.size.width-16, 39)];
    adrDetailView.layer.cornerRadius=2;
    adrDetailView.layer.borderWidth=0.5f;
    adrDetailView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [footView addSubview:adrDetailView];
    
    //adrDetailText
    adrDetailText=[UITextField new];
    adrDetailText.placeholder=@"详细地址";
    adrDetailText.tag=3;
    adrDetailText.clearButtonMode=UITextFieldViewModeWhileEditing;
    [adrDetailText addTarget:self action:@selector(phoneDidChange) forControlEvents:UIControlEventEditingChanged];
    adrDetailText.font=[UIFont systemFontOfSize:15];
    adrDetailText.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    [adrDetailView addSubview:adrDetailText];
    adrDetailText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* adrDetailText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[adrDetailText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(adrDetailText)];
    [NSLayoutConstraint activateConstraints:adrDetailText_h];
    
    NSArray* adrDetailText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[adrDetailText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(adrDetailText)];
    [NSLayoutConstraint activateConstraints:adrDetailText_w];
    
    //button
    button=[UIButton new];
    button.layer.cornerRadius=2;
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [button setTitle:@"使用新收货地址" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [footView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(39)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_w];
    
    [button addTarget:self action:@selector(clickUserNewAdrBtn) forControlEvents:UIControlEventTouchUpInside];
    button.enabled=NO;
    button.backgroundColor=[UIColor lightGrayColor];
    
    UIView *line02=[[UIView alloc]initWithFrame:CGRectMake(0, footView.height-0.5f, footView.width, 0.5f)];
    line02.alpha=0.7;
    line02.backgroundColor=[UIColor lightGrayColor];
    [footView addSubview:line02];
    return footView;
}

#pragma mark - private method
- (void)showMyPicker{
    [AppUtils closeKeyboard];
    [self.navigationController.view addSubview:self.maskView];
    [self.navigationController.view addSubview:self.pickerBgView];
    self.maskView.alpha = 0;
    self.pickerBgView.top = self.navigationController.view.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0.3;
        self.pickerBgView.bottom = self.navigationController.view.height;
    }];
}

- (void)hideMyPicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0;
        self.pickerBgView.top = self.navigationController.view.height;
    } completion:^(BOOL finished) {
        
        NSMutableArray *array=[NSMutableArray array];
        CityModel *model01=[provinceArray objectAtIndex:[self.myPicker selectedRowInComponent:0]];
        CityModel *model02=[testCityArr001 objectAtIndex:[self.myPicker selectedRowInComponent:1]];
        CityModel *model03=[testDistrictArr001 objectAtIndex:[self.myPicker selectedRowInComponent:2]];
        
        provinceId=model01.parent_id;
        cityId=model02.parent_id;
        districtId=model03.parent_id;
        [array addObjectsFromArray:@[model01,model02,model03]];
        if (self.CityIdsArrayBlock) {
            self.CityIdsArrayBlock(array);
        }
        [self.maskView removeFromSuperview];
        [self.pickerBgView removeFromSuperview];
    }];
}

#pragma mark -
- (void)phoneDidChange{
    if ([receiverText.text isEqualToString:@""]||
        [phoneNumText.text isEqualToString:@""]||
        ![AppUtils checkPhoneNumber:phoneNumText.text]||
        [addressText.text isEqualToString:@""]||
        [adrDetailText.text isEqualToString:@""]) {
        button.enabled=NO;
        button.backgroundColor=[UIColor lightGrayColor];
    }else{
        button.enabled=YES;
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    }
}

-(void)showSeleteCityView{
    [self showMyPicker];
}

#pragma mark - 点击使用新收货地址按钮
-(void)clickUserNewAdrBtn{
    [self requestHttpsForAddReceiverAddress];
}

#pragma mark TableView delegate---
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 59;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01f;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DeliveryAddressCell *cell=[tableView dequeueReusableCellWithIdentifier:addressidentifier forIndexPath:indexPath];
    if (!cell) {
        cell=[[DeliveryAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addressidentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    ReceiverModel *model=dataArr[indexPath.row];
    [cell reflushModeDataForModel:model];
    if ([self.seleAdrId isEqualToString:[NSString stringWithFormat:@"%@",model.receiverId]]) {
        [cell.imgv setImage:[UIImage imageNamed:@"allBtn_sele"]];
        selectedIndexPath=indexPath;
    }else{
        [cell.imgv setImage:[UIImage imageNamed:@"allBtn_nor"]];
    }
    return  cell;
}

-(void )tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectedIndexPath) {
        DeliveryAddressCell *cell=(DeliveryAddressCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
        [cell.imgv setImage:[UIImage imageNamed:@"allBtn_nor"]];
    }
    DeliveryAddressCell *cell=(DeliveryAddressCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell.imgv setImage:[UIImage imageNamed:@"allBtn_sele"]];
    selectedIndexPath=indexPath;
    ReceiverModel *model=dataArr[selectedIndexPath.row];
    _seleAdrId=[NSString stringWithFormat:@"%@",model.receiverId];
    if (self.kReceiverBlock) {
        self.kReceiverBlock(model);
    }
    [self httpSetDefaultAddressStatusForId:_seleAdrId];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return provinceArray.count;
    } else if (component == 1) {
        return testCityArr001.count;
    } else {
        return testDistrictArr001.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        CityModel *model=[provinceArray objectAtIndex:row];
        return model.cityName;
    } else if (component == 1) {
        if(testCityArr001.count==1){
            return nil;
        }else{
            CityModel *model=[testCityArr001 objectAtIndex:row];
            return model.cityName;
        }
    } else {
        CityModel *model=[testDistrictArr001 objectAtIndex:row];
        return model.cityName;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 110;
    } else if (component == 1) {
        return 100;
    } else {
        return 110;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    CityFmdb *db=[CityFmdb shareCityFMDB];
    if (component == 0) {
        selectedArray =provinceArray;
        if (selectedArray.count > 0) {
            CityModel *model=selectedArray[row];
            //得到一个当前省份的所有城市
            testCityArr001=[db getCityWithId:model.parent_id];
        } else {
            testCityArr001 = nil;
        }
        
        if (testCityArr001.count > 0) {
            //得到一个选择市的全部区域
            CityModel *model=testCityArr001[0];
            testDistrictArr001=[db getCityWithId:model.parent_id];
        } else {
            testDistrictArr001 = nil;
        }
        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectedRowInComponent:2];
    }
    
    if (component == 1) {
        if (selectedArray.count > 0 && testCityArr001.count > 0) {
            CityModel *model=testCityArr001[row];
            //得到一个市的全部区域
            testDistrictArr001=[db getCityWithId:model.parent_id];
        } else {
            testDistrictArr001 = nil;
        }
        [pickerView selectRow:2 inComponent:2 animated:YES];
    }
    [pickerView reloadComponent:2];
}

#pragma mark - dealloc
-(void)dealloc{
    provinceArray=nil;
    testCityArr001=nil;
    testDistrictArr001=nil;
    selectedArray=nil;
}



#pragma mark - Request请求
//（添加收货人地址）
-(void)requestHttpsForAddReceiverAddress{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"add_consignee" forKey:@"act"];
    [dict setObject:receiverText.text forKey:@"consignee"];
    [dict setObject:adrDetailText.text forKey:@"address"];
    [dict setObject:@"1" forKey:@"selCountries"];
    [dict setObject:provinceId forKey:@"selProvinces"];
    [dict setObject:cityId forKey:@"selCities"];
    [dict setObject:districtId forKey:@"selDistricts"];
    [dict setObject:phoneNumText.text forKey:@"tel"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForAddReceiverAddress];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                //
                ReceiverModel *model=[[ReceiverModel alloc]init];
                model.receiverId=responseObject[@"retData"][@"id"];
                model.receiverName=receiverText.text;
                model.phoneNum=phoneNumText.text;
                model.address=[NSString stringWithFormat:@"%@%@",addressText.text,adrDetailText.text];
                [dataArr addObject:model];
                _seleAdrId=[NSString stringWithFormat:@"%@",model.receiverId];
                if (self.kReceiverBlock) {
                    self.kReceiverBlock(model);
                }
                [myTableView reloadData];
                receiverText.text=@"";
                phoneNumText.text=@"";
                addressText.text=@"";
                adrDetailText.text=@"";
                [self httpSetDefaultAddressStatusForId:_seleAdrId];
            }else{
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//更改默认收货人状态请求
-(void)httpSetDefaultAddressStatusForId:(NSString *)seleAddId{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"change_default_address",
                         @"id":seleAddId,
                         @"is_nosign":@"1",
                         @"user_id":[AppUtils getValueWithKey:User_ID]
                         };
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//收货人列表请求
-(void)httpAddressList{
    dataArr=[NSMutableArray array];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"consignee_user_default" forKey:@"step"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self httpAddressList];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSArray *addressArr=responseObject[@"retData"][@"consignee_list"];
                for (NSDictionary *addressDic in addressArr) {
                    ReceiverModel *model=[[ReceiverModel alloc]init];
                    [model jsonDataForDictionary:addressDic];
                    [dataArr addObject:model];
                }
            }
            //footview
            myTableView.tableFooterView=[self setUpFootView];
            
            [myTableView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
