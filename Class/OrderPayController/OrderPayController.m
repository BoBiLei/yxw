//
//  OrderPayController.m
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "OrderPayController.h"
#import "OrderPayCell.h"
#import "PayTypeCell.h"
#import "MMGAliPay.h"
#import "PayRequsestHandler.h"
#import "PayFinishController.h"
#import "UserOrderController.h"
#import "ShoppingCartController.h"
#import "OrderDetailController.h"

@interface OrderPayController ()<UITableViewDataSource,UITableViewDelegate,PayFinishDelegate,OrderPayCellDelegate,UIGestureRecognizerDelegate>

@end

@implementation OrderPayController{
    CGRect tableFrame;
    NSMutableArray *payMsgDicArr;
    NSMutableArray *payTypeArr;
    NSArray *dataArr;
    UITableView *myTable;
    NSInteger payType;
    NSIndexPath *selectedIndexPath;
    UIView *buyView;    //立即购买的view
    //
    NSString *orderNumber;
    NSString *orderShopDescription;
    NSString *payPrice;
    
    NSString *couponId;  //如果为0，说明没有优惠券
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxSender:) name:@"WXPAY_SENDER" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alipaySender:) name:@"ALIPAYCALLBACK" object:nil];
    
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self setCustomNavigationTitle:@"订单支付"];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    [self requestHttpsForOrderPay:self.orderId];
    
    [self setUpUI];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}

#pragma mark - 覆盖BaseController的setCustomNavigationTitle方法
-(void)setCustomNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = [UIFont systemFontOfSize:NAV_TITLE_FONT_SIZE];
    navTitle.textColor = [UIColor colorWithHexString:@"#282828"];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,22.f,22.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(flagTurnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
}

-(void)flagTurnBack{
    
    UIAlertController*alertController = [UIAlertController alertControllerWithTitle:@"提示"message:@"是否放弃付款"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction*cancelAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleCancel handler:^(UIAlertAction*action) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[UserOrderController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
            if ([controller isKindOfClass:[ShoppingCartController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    
    UIAlertAction*otherAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleDefault handler:^(UIAlertAction*action) {
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:otherAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 微信支付回调
-(void)wxSender:(id)sender{
    
    BaseResp * resp = [sender object];
    
    if([resp isKindOfClass:[PayResp class]]){
        
        switch (resp.errCode) {
            case WXSuccess:
                NSLog(@"支付结果：成功！");
                [self requestHttpsSetOrderStatusForOrderNum:orderNumber orderAmout:payPrice];
                break;
            default:
                NSLog(@"支付结果：失败！");
                break;
        }
    }
}

#pragma mark - 支付宝支付回调
-(void)alipaySender:(id)sender{
    NSDictionary *resultDic=[sender object];
    NSString *statusStr=resultDic[@"resultStatus"];
    int statusInt=statusStr.intValue;
    switch (statusInt) {
        case 9000:
            NSLog(@"支付成功----");
            [self requestHttpsSetOrderStatusForOrderNum:orderNumber orderAmout:payPrice];
            break;
        case 4000:
            NSLog(@"订单支付失败----");
            break;
        case 6001:
            NSLog(@"用户中途取消----");
            break;
        case 6002:
            NSLog(@"网络连接出错----");
            break;
        default:
            break;
    }
}

#pragma mark - init UI
-(void)setUpUI{
    payType=0;
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"PayType" ofType:@"plist"];
    payTypeArr = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    dataArr=@[@[@""],payTypeArr];
    
    payMsgDicArr=[NSMutableArray array];
    
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 38)];
    imgv.contentMode=UIViewContentModeScaleToFill;
    imgv.image=[UIImage imageNamed:@"submit_order02"];
    [self.view addSubview:imgv];
    
    tableFrame=CGRectMake(8, 50, SCREENSIZE.width-16, SCREENSIZE.height-162);
    myTable=[[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"OrderPayCell" bundle:nil] forCellReuseIdentifier:@"orderpaycell"];
    [myTable registerNib:[UINib nibWithNibName:@"PayTypeCell" bundle:nil] forCellReuseIdentifier:@"paytypecell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.sectionFooterHeight = 0;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:myTable];
    
    //footview
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 240)];
    footView.backgroundColor=[UIColor whiteColor];
    myTable.tableFooterView=footView;
    //
    UILabel *tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, 6, footView.width-12, 36)];
    tipLabel.font=[UIFont systemFontOfSize:15];
    tipLabel.text=@"请务必在汇款时将订单编号填到备注中，非招行卡退款，周期较长，请谨慎选择。";
    tipLabel.numberOfLines=0;
    [footView addSubview:tipLabel];
    //
    UILabel *accountLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, tipLabel.frame.origin.y+tipLabel.frame.size.height+6, footView.width-12, 16)];
    accountLabel.font=[UIFont systemFontOfSize:14];
    accountLabel.text=@"账户名称：深圳猫猫购寄卖有限公司";
    accountLabel.textColor=[UIColor lightGrayColor];
    [footView addSubview:accountLabel];
    //
    UILabel *bankLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, accountLabel.frame.origin.y+accountLabel.frame.size.height+6, footView.width-12, 16)];
    bankLabel.font=[UIFont systemFontOfSize:14];
    bankLabel.text=@"开户银行：招商银行股份有限公司深圳蔡屋围支行";
    bankLabel.textColor=[UIColor lightGrayColor];
    [footView addSubview:bankLabel];
    //
    UILabel *bankAccount=[[UILabel alloc]initWithFrame:CGRectMake(6, bankLabel.frame.origin.y+bankLabel.frame.size.height+6, footView.width-12, 16)];
    bankAccount.font=[UIFont systemFontOfSize:14];
    bankAccount.text=@"银行账号：755922307810701";
    bankAccount.textColor=[UIColor lightGrayColor];
    [footView addSubview:bankAccount];
    
    //
    UILabel *tip02=[[UILabel alloc]initWithFrame:CGRectMake(6, bankAccount.frame.origin.y+bankAccount.frame.size.height+8, footView.width-12, 16)];
    tip02.font=[UIFont systemFontOfSize:15];
    tip02.text=@"温馨提示：";
    [footView addSubview:tip02];
    //
    UILabel *tip02_1=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02.frame.origin.y+tip02.frame.size.height+4, footView.width-12, 36)];
    tip02_1.font=[UIFont systemFontOfSize:14];
    tip02_1.text=@"1. “订单提交成功”表明猫猫购收到了您的订单，只有您的订单审核通过后，才能确认订单生效。";
    tip02_1.numberOfLines=0;
    [footView addSubview:tip02_1];
    //
    UILabel *tip02_2=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02_1.frame.origin.y+tip02_1.frame.size.height+4, footView.width-12, 36)];
    tip02_2.font=[UIFont systemFontOfSize:14];
    tip02_2.text=@"2. 对于“猫猫购”售出的商品，我们为您提供7天退换货保障。";
    tip02_2.numberOfLines=0;
    [footView addSubview:tip02_2];
    
    //
    UILabel *phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(6, tip02_2.frame.origin.y+tip02_2.frame.size.height+6, footView.width-12, 16)];
    phoneLabel.font=[UIFont systemFontOfSize:14];
    phoneLabel.text=@"客服电话：0755-22211222";
    [footView addSubview:phoneLabel];
    UITapGestureRecognizer *tapgest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickPhoneLabel)];
    phoneLabel.userInteractionEnabled=YES;
    [phoneLabel addGestureRecognizer:tapgest];
    
    myTable.tableFooterView.hidden=YES;
    
    //立即购买的View
    buyView=[[UIView alloc]initWithFrame:CGRectMake(0, SCREENSIZE.height-108, SCREENSIZE.width, 44)];
    buyView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:buyView];
    
    //button
    UIButton *submitBtn=[[UIButton alloc]init];
    submitBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"立即付款" forState:UIControlStateNormal];
    submitBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [submitBtn addTarget:self action:@selector(clickOrderPayBtn) forControlEvents:UIControlEventTouchUpInside];
    [buyView addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* submitBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[submitBtn]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_h];
    
    NSArray* submitBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[submitBtn]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(submitBtn)];
    [NSLayoutConstraint activateConstraints:submitBtn_w];
}

-(void)clickPhoneLabel{
    UIWebView *webView;
    if (webView == nil) {
        webView = [[UIWebView alloc] init];
        webView.translatesAutoresizingMaskIntoConstraints=NO;
    }
    NSString *phoneUrl=[NSString stringWithFormat:@"tel://%@",SERVICEPHONE];
    NSURL *url = [NSURL URLWithString:phoneUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

#pragma mark - Request获取付款参数请求
-(void)requestHttpsForOrderPay:(NSString *)orderId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"pay" forKey:@"step"];
    [dict setObject:orderId forKey:@"order"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"flow.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"》》》》》%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderPay:self.orderId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                
                couponId=responseObject[@"retData"][@"bonus_id"];
                
                orderNumber=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"order_sn"]];
                
                self.selectedPayId=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"pay_id"]];
                
                NSMutableString *orderShopDescriptionStr=[NSMutableString stringWithString:responseObject[@"retData"][@"good_names"]];
                NSRange range=[orderShopDescriptionStr rangeOfString:@","];
                if (range.location!=NSNotFound) {
                    NSString *newStr=[orderShopDescriptionStr substringToIndex:range.location];
                    orderShopDescription=newStr;
                }else{
                    orderShopDescription=orderShopDescriptionStr;
                }
                
                payPrice=[NSString stringWithFormat:@"%@",responseObject[@"retData"][@"order"][@"order_amount"]];
                
                [payMsgDicArr addObject:responseObject[@"retData"][@"order"]];
            }
            [myTable reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//更改订单状态
-(void)requestHttpsSetOrderStatusForOrderNum:(NSString *)orderNum orderAmout:(NSString *)orderAmout{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"paylog" forKey:@"act"];
    [dict setObject:orderNumber forKey:@"order_sn"];
    [dict setObject:payPrice forKey:@"order_amount"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    NSLog(@"%@",dict);
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"iso_pay.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsSetOrderStatusForOrderNum:orderNumber orderAmout:payPrice];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:@"付款成功" inView:self.view];
                // 延迟加载
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    PayFinishController *payFinish=[[PayFinishController alloc]init];
                    payFinish.payPrice=payPrice;
                    payFinish.orderNum=orderNumber;
                    payFinish.goodName=orderShopDescription;
                    [self.navigationController pushViewController:payFinish animated:YES];
                });
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 点击付款按钮
-(void)clickOrderPayBtn{
    PayFinishController *payFinish=[[PayFinishController alloc]init];
    payFinish.hidesBottomBarWhenPushed=YES;
    if(payType==0){
        [AppUtils showSuccessMessage:@"请选择支付方式" inView:self.view];
    }else{
        if (![orderNumber isEqualToString:@""]||
            ![payPrice isEqualToString:@""]) {
            switch (payType) {
                case 1:
                    [self sendAliPay];
                    break;
                case 2:
                    [self sendWXPay];
                    break;
                default:
                    break;
            }
        }
    }
}

#pragma mark - 支付宝支付
-(void)sendAliPay{
    MMGAliPay *aLiPay=[[MMGAliPay alloc]init];
    aLiPay.payFinishDelegate=self;
    aLiPay.shopName=orderShopDescription;
    aLiPay.shopDescription=orderShopDescription;
    aLiPay.money=payPrice;
    aLiPay.orderNum=orderNumber;
    [aLiPay sendAliPay];
}

#pragma mark - 微信支付
- (void)sendWXPay{
    if([WXApi isWXAppInstalled]){
        PayRequsestHandler *wxPayRequestHandler=[[PayRequsestHandler alloc]initRequestHandler];
        
        //设置付款金额格式
        NSString *monerStr=payPrice;
        CGFloat moneyFloat=monerStr.floatValue*100;
        NSString *priceStr=[NSString stringWithFormat:@"%.f",moneyFloat];
        
        NSMutableDictionary *dict = [wxPayRequestHandler sendPay_OrderWithTradeNo:orderNumber OrderTitle:orderShopDescription orderPrice:priceStr];
        
        //调起微信支付
        PayReq* req             = [[PayReq alloc] init];
        req.openID              = [dict objectForKey:@"appid"];     //appid
        req.partnerId           = [dict objectForKey:@"partnerid"]; //商户号
        req.prepayId            = [dict objectForKey:@"prepayid"];  //预支付交易会话ID
        req.package             = [dict objectForKey:@"package"];   //扩展字段
        req.nonceStr            = [dict objectForKey:@"noncestr"];  //随机字符串
        //防止重发
        NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
        req.timeStamp           = stamp.intValue;                   //时间戳
        req.sign                = [dict objectForKey:@"sign"];      //签名
        
        [WXApi sendReq:req];
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"您尚未安装微信" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)dataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderPayCell *cell=[tableView dequeueReusableCellWithIdentifier:@"orderpaycell"];
        if (!cell) {
            cell=[[OrderPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderpaycell"];
        }
        for (NSDictionary *dic in payMsgDicArr) {
            [cell reflushDataForDictionary:dic];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell.frame.size.height;
    }else{
        return 44;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 34;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 34)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    
    //订单编号
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width/2+20, headerSectionView.frame.size.height)];
    label.font=[UIFont systemFontOfSize:13];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    [headerSectionView addSubview:label];
    if (section==0) {
        label.text=@"支付信息";
    }else{
        label.text=@"支付方式";
    }
    return headerSectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        OrderPayCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"orderpaycell"];
        if (!cell) {
            cell=[[OrderPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orderpaycell"];
        }
        for (NSDictionary *dic in payMsgDicArr) {
            [cell reflushDataForDictionary:dic];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        return  cell;
    }else{
        PayTypeCell *cell;
        cell=[tableView dequeueReusableCellWithIdentifier:@"paytypecell"];
        if (!cell) {
            cell=[[PayTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"paytypecell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell._payTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImgv"]]];
        cell._payTypeName.text=dataArr[indexPath.section][indexPath.row][@"payTypeName"];
        
        //设置选中的支付方式
        if ([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:self.selectedPayId]) {
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_sele"]]];
            selectedIndexPath=indexPath;
            if([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:@"2"]){
                CGRect newFrame=tableFrame;
                newFrame.size.height=SCREENSIZE.height-118;
                myTable.frame=newFrame;
                myTable.tableFooterView.hidden=NO;
                buyView.hidden=YES;
            }else if ([dataArr[indexPath.section][indexPath.row][@"seleId"] isEqualToString:@"5"]){
                payType=1;
            }else{
                payType=2;
            }
        }else{
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_nor"]]];
        }
        return  cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        if (selectedIndexPath) {
            PayTypeCell *cell=(PayTypeCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
            [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_nor"]]];
        }
        PayTypeCell *cell=(PayTypeCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell.selePayTypeImgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"payTypeImg_sele"]]];
        selectedIndexPath = indexPath;
        
        CGRect newFrame=tableFrame;
        newFrame.size.height=SCREENSIZE.height-118;
        switch (indexPath.row) {
            case 0:
                payType=1;
                myTable.tableFooterView.hidden=YES;
                buyView.hidden=NO;
                break;
            case 1:
                payType=2;
                myTable.tableFooterView.hidden=YES;
                buyView.hidden=NO;
                break;
            case 2:
                myTable.frame=newFrame;
                myTable.tableFooterView.hidden=NO;
                buyView.hidden=YES;
                break;
            default:
                break;
        }
    }
}

#pragma mark - orderpayCell delegate
-(void)tapOrderNumLabel:(NSString *)orderNum{
    OrderDetailController *orderDetail=[[OrderDetailController alloc]init];
    orderDetail.orderId=orderNum;
    [self.navigationController pushViewController:orderDetail animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - pay finish delegate
-(void)pushOrderDetailViewWithController{
    NSLog(@"1");
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
