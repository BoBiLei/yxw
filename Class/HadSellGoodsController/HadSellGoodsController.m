//
//  HadSellGoodsController.m
//  MMG
//
//  Created by mac on 16/8/15.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "HadSellGoodsController.h"
#import "HadSellGoodsCell.h"
#import <TTTAttributedLabel.h>
#import "MaijiaOrderDetailController.h"
#import "CustomPickerView.h"
#import "SeleGoDateView.h"

@interface HadSellGoodsController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,HadSellGoodsDelegate>

@end

@implementation HadSellGoodsController{
    
    NSInteger pageInt;
    MJRefreshAutoNormalFooter *footer;
    
    UITextField *starTimeTF;
    UITextField *lastTimeTF;
    NSString *todayTime;
    NSString *subStarTime;                 //开始时间戳
    NSString *subLastTime;                 //结束时间戳
    NSString *star_date_y;
    NSString *star_date_m;
    NSString *star_date_d;
    NSString *last_date_y;
    NSString *last_date_m;
    NSString *last_date_d;
    
    UILabel *typeLabel;
    NSMutableArray *typeArr;
    NSString *seleTypeId;
    NSString *searKeyWork;
    TTTAttributedLabel *tipLabel;
    
    NSMutableArray *dataArr;
    UITableView *myTable;
    
    __block NSString *totalGoods;
    NSString *totalSellerPrice;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadListDate:) name:@"update_hadseller_list" object:nil];
    
    [self setCustomNavigationTitle:@"已售商品"];
    
    [self setUpSearchView];
    
    [self setUpTableView];
    
    [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
}

-(void)reloadListDate:(NSNotification *)noti{
    [dataArr removeAllObjects];
    [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
}

-(void)setUpSearchView{
    
    typeArr=[NSMutableArray array];
    NSString *str;
    NSString *gId;
    for (int i=0; i<3; i++) {
        SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
        switch (i) {
            case 0:
                str=@"商品";
                gId=@"g_name";
                break;
            case 1:
                str=@"订单号";
                gId=@"o_id";
                break;
            default:
                str=@"买家";
                gId=@"buyer";
                break;
        }
        model.typeName=str;
        model.typeId=gId;
        [typeArr addObject:model];
    }
    seleTypeId=@"g_name";
    
    searKeyWork=@"";
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 64)];
    view.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
    [self.view addSubview:view];
    
#pragma mark 商品分类
    UIImageView *flImgv=[[UIImageView alloc] initWithFrame:CGRectMake(12, 14, view.width/4, 36)];
    flImgv.image=[UIImage imageNamed:@"shangjia_ershou"];
    flImgv.userInteractionEnabled=YES;
    [view addSubview:flImgv];
    
    typeLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, flImgv.width-20, flImgv.height)];
    typeLabel.textColor=[UIColor colorWithHexString:@"#363636"];;
    typeLabel.font=[UIFont systemFontOfSize:15];
    typeLabel.textAlignment=NSTextAlignmentCenter;
    typeLabel.text=@"商品";
    [flImgv addSubview:typeLabel];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectFenlei)];
    [flImgv addGestureRecognizer:tap];
    
#pragma mark 搜索View
    UIView *sView=[[UIView alloc] initWithFrame:CGRectMake(flImgv.origin.x+flImgv.width+8, flImgv.origin.y, SCREENSIZE.width-(flImgv.origin.x+flImgv.width+20), flImgv.height)];
    sView.backgroundColor=[UIColor whiteColor];
    [view addSubview:sView];
    
    UITextField *searchTF=[[UITextField alloc] initWithFrame:CGRectMake(4, 0, sView.width-sView.height-8, sView.height)];
    searchTF.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchTF.tintColor=[UIColor colorWithHexString:@"#ec6b00"];
    searchTF.placeholder=@"Birkin";
    searchTF.font=[UIFont systemFontOfSize:15];
    searchTF.textColor=[UIColor colorWithHexString:@"#363636"];
    searchTF.returnKeyType=UIReturnKeySearch;
    searchTF.delegate=self;
    [searchTF addTarget:self action:@selector(searTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [sView addSubview:searchTF];
    
    //
    UIImageView *srImgv=[[UIImageView alloc] initWithFrame:CGRectMake(searchTF.origin.x+searchTF.width+4, 0, sView.height, sView.height)];
    srImgv.image=[UIImage imageNamed:@"maijia_search"];
    srImgv.userInteractionEnabled=YES;
    [sView addSubview:srImgv];
    UITapGestureRecognizer *stap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSearch)];
    [srImgv addGestureRecognizer:stap];
    
    [self setUpDateFillterViewAfterView:view];
}

- (void)searTextDidChange:(id) sender {
    UITextField *field = (UITextField *)sender;
    searKeyWork=field.text;
}

#pragma mark - 点击选择类型
-(void)selectFenlei{
    
    [AppUtils closeKeyboard];
    
    CustomPickerView *pick=[[CustomPickerView alloc]initWithPickerViewInView:self.navigationController.view withArray:typeArr selectedTypeId:seleTypeId];
    [pick showPickerViewCompletion:^(id selectedObject) {
        SubmitOrder_SeletedTypeModel *model=selectedObject;
        seleTypeId=[NSString stringWithFormat:@"%@",model.typeId];
        typeLabel.text=model.typeName;
    }];
}

#pragma mark - 点击搜索
-(void)clickSearch{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [AppUtils closeKeyboard];
    pageInt=1;
    [dataArr removeAllObjects];
    [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
    return YES;
}

#pragma mark - 日期筛选View
-(void)setUpDateFillterViewAfterView:(UIView *)view{
    //初始化当前年月日（必须）
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    star_date_y = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    last_date_y = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    [formatter setDateFormat:@"M"];
    star_date_m = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    last_date_m = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    [formatter setDateFormat:@"dd"];
    star_date_d = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    last_date_d = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    
    //获取到今天的时间戳
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    [formatter setTimeZone:timeZone];
    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@",star_date_y,star_date_m,star_date_d]]; //将字符串转成nsdate
    todayTime = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    
    //
    UIView *fview=[[UIView alloc] initWithFrame:CGRectMake(0, view.origin.y+view.height, SCREENSIZE.width, 84)];
    fview.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:fview];
    
    CGFloat zhiFloat=30;        //至的宽度
    CGFloat cxBtnWidth=64;    //查询按钮宽度
    CGFloat viewWidth=(SCREENSIZE.width-(cxBtnWidth+zhiFloat+32))/2;
    
    //starView
    UIView *starView=[[UIView alloc] initWithFrame:CGRectMake(12, 14, viewWidth, 36)];
    starView.backgroundColor=[UIColor whiteColor];
    starView.layer.borderWidth=0.5f;
    starView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    [fview addSubview:starView];
    starView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap01=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickStarTimeView)];
    [starView addGestureRecognizer:tap01];
    
    starTimeTF=[[UITextField alloc] initWithFrame:CGRectMake(6, 0, starView.width-12, starView.height)];
    starTimeTF.placeholder=@"开始日期";
    starTimeTF.font=[UIFont systemFontOfSize:15];
    starTimeTF.enabled=NO;
    [starView addSubview:starTimeTF];
    
    //zhiLabel
    UILabel *zhiLabel=[[UILabel alloc] initWithFrame:CGRectMake(starView.origin.x+starView.width, starView.origin.y, zhiFloat, starView.height)];
    zhiLabel.textAlignment=NSTextAlignmentCenter;
    zhiLabel.font=[UIFont systemFontOfSize:15];
    zhiLabel.textColor=[UIColor colorWithHexString:@"#363636"];
    zhiLabel.text=@"至";
    [fview addSubview:zhiLabel];
    
    //endView
    UIView *endView=[[UIView alloc] initWithFrame:CGRectMake(zhiLabel.origin.x+zhiLabel.width, zhiLabel.origin.y, viewWidth, zhiLabel.height)];
    endView.layer.borderWidth=0.5f;
    endView.layer.borderColor=[UIColor colorWithHexString:@"#c2c2c2"].CGColor;
    endView.backgroundColor=[UIColor whiteColor];
    [fview addSubview:endView];
    endView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap02=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickLastTimeView)];
    [endView addGestureRecognizer:tap02];
    
    lastTimeTF=[[UITextField alloc] initWithFrame:CGRectMake(6, 0, endView.width-12, endView.height)];
    lastTimeTF.placeholder=@"结束日期";
    lastTimeTF.font=[UIFont systemFontOfSize:15];
    lastTimeTF.enabled=NO;
    [endView addSubview:lastTimeTF];
    
    //
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(endView.origin.x+endView.width+8, endView.origin.y, cxBtnWidth, endView.height);
    btn.layer.cornerRadius=2;
    btn.backgroundColor=[UIColor colorWithHexString:@"#ea6b1f"];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"查询" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickDateFillterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [fview addSubview:btn];
    
    //
    tipLabel=[[TTTAttributedLabel alloc] initWithFrame:CGRectMake(starView.origin.x, endView.origin.y+endView.height, SCREENSIZE.width-2*starView.origin.x, fview.height-(endView.origin.y+endView.height))];
    tipLabel.font=[UIFont systemFontOfSize:15];
    [fview addSubview:tipLabel];
    
    [AppUtils drawZhiXian:fview withColor:[UIColor colorWithHexString:@"#c2c2c2"] height:1 firstPoint:CGPointMake(0, fview.height) endPoint:CGPointMake(SCREENSIZE.width, fview.height)];
}

-(void)clickStarTimeView{
    
    [AppUtils closeKeyboard];
    
    SeleGoDateView *dateView=[[SeleGoDateView alloc]initWithPickerViewInView:self.navigationController.view whitYear:star_date_y month:star_date_m day:star_date_d];
    [dateView showPickerViewCompletion:^(id object) {
        NSArray *callBackArr=object;
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [formatter setTimeZone:timeZone];
        NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@",callBackArr[0],callBackArr[1],callBackArr[2]]]; //将字符串转成nsdate
        subStarTime = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
        if (subStarTime.integerValue>todayTime.integerValue) {
            [AppUtils showSuccessMessage:@"开始时间不能大于今天" inView:self.view];
        }else{
            star_date_y=callBackArr[0];
            star_date_m=callBackArr[1];
            star_date_d=callBackArr[2];
            NSString *timeStr=[NSString stringWithFormat:@"%@-%@-%@",star_date_y,star_date_m,star_date_d];
            starTimeTF.text=timeStr;
        }
    }];
}

-(void)clickLastTimeView{
    
    [AppUtils closeKeyboard];
    
    SeleGoDateView *dateView=[[SeleGoDateView alloc]initWithPickerViewInView:self.navigationController.view whitYear:last_date_y month:last_date_m day:last_date_d];
    [dateView showPickerViewCompletion:^(id object) {
        NSArray *callBackArr=object;
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [formatter setTimeZone:timeZone];
        NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@",callBackArr[0],callBackArr[1],callBackArr[2]]]; //将字符串转成nsdate
        subLastTime = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
        if (subLastTime.integerValue>todayTime.integerValue) {
            [AppUtils showSuccessMessage:@"结束时间不能大于今天" inView:self.view];
        }else{
            last_date_y=callBackArr[0];
            last_date_m=callBackArr[1];
            last_date_d=callBackArr[2];
            NSString *timeStr=[NSString stringWithFormat:@"%@-%@-%@",last_date_y,last_date_m,last_date_d];
            lastTimeTF.text=timeStr;
        }
    }];
}

-(void)clickDateFillterBtn:(id)sender{
    
    [AppUtils closeKeyboard];
    
    if ([starTimeTF.text isEqualToString:@""]||starTimeTF==nil) {
        [AppUtils showSuccessMessage:@"请选择开始时间" inView:self.view];
    }else if ([lastTimeTF.text isEqualToString:@""]||lastTimeTF==nil){
        [AppUtils showSuccessMessage:@"请选择结束时间" inView:self.view];
    }else{
        if (subLastTime.integerValue<subStarTime.integerValue) {
            [AppUtils showSuccessMessage:@"结束时间不能小于开始时间" inView:self.view];
        }else{
            [AppUtils closeKeyboard];
            pageInt=1;
            [dataArr removeAllObjects];
            [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
        }
    }
}

#pragma mark - setUpTableView
-(void)setUpTableView{
    
    dataArr=[NSMutableArray array];
    
    pageInt=1;
    
    subStarTime=@"";
    
    subLastTime=@"";
    
    myTable=[[UITableView alloc] initWithFrame:CGRectMake(0, 148, SCREENSIZE.width, SCREENSIZE.height-212) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
    
    //加载更多
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = [UIFont systemFontOfSize:14];
    footer.stateLabel.textColor = [UIColor lightGrayColor];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载中，请稍后…" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据！" forState:MJRefreshStateNoMoreData];
    // 忽略掉底部inset
    footer.triggerAutomaticallyRefreshPercent=0.5;
    myTable.mj_footer = footer;
}

- (void)refreshForPullUp{
    if (pageInt!=1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestGoodsListDataWithTypeId:seleTypeId keyWork:searKeyWork];
        });
    }
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str=[AppUtils getValueWithKey:@"HadSellGoodsCell_Height"];
    return str.floatValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 16;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==7) {
        return 20;
    }else{
        return 0.000000000001f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HadSellGoodsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[HadSellGoodsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    HadSellerGoodsModel *model=dataArr[indexPath.section];
    cell.model=model;
    [cell reflushDataForModel:model];
    cell.delegate=self;
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Cell 按钮delegate
//订单详情
-(void)scanOrderDetail:(HadSellerGoodsModel *)model{
    MaijiaOrderDetailController *ctr=[MaijiaOrderDetailController new];
    ctr.orderId=model.order_id;
    ctr.goodsId=model.goodsID;
    ctr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:ctr animated:YES];
}

#pragma mark - Request 请求
-(void)requestGoodsListDataWithTypeId:(NSString *)type keyWork:(NSString *)keyWork{
    
    if (pageInt==1) {
        [AppUtils showProgressInView:self.view];
    }
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"sold_goods_list" forKey:@"act"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:keyWork forKey:@"keyword"];
    [dict setObject:subStarTime forKey:@"s_time"];
    [dict setObject:subLastTime forKey:@"e_time"];
    [dict setObject:[NSString stringWithFormat:@"%ld",pageInt] forKey:@"page"];
        [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[NSString stringWithFormat:@"%@sold_goods.php?",DefaultHost] method:HttpRequestPost parameters:dict prepareExecute:^{
    
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [AppUtils dismissHUDInView:self.view];
        
        NSLog(@"%@--%@",responseObject,type);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *ddArr=responseObject[@"retData"][@"goods_list"];
            for (NSDictionary *dic in ddArr) {
                HadSellerGoodsModel *model=[HadSellerGoodsModel new];
                [model jsonDataForDictionary:dic];
                [dataArr addObject:model];
            }
            
            totalGoods=responseObject[@"retData"][@"order_info"][@"order_count"];
            
            totalSellerPrice=responseObject[@"retData"][@"order_info"][@"amount"];
            
            NSString *stageStr=[NSString stringWithFormat:@"已售 %@ 件商品，总营业额 ￥%@",totalGoods,totalSellerPrice];
            __block TTTAttributedLabel *blockSelf = tipLabel;
            [blockSelf setText:stageStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
                NSRange range01 = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",totalGoods] options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ea6b1f"] CGColor] range:range01];
                NSRange range02 = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"￥%@",totalSellerPrice] options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#ea6b1f"] CGColor] range:range02];
                return mutableAttributedString;
            }];
            
            //总页数
            NSString *totalPage=[responseObject[@"retData"][@"totalPageCount"] stringValue];
            if (pageInt<totalPage.integerValue) {
                pageInt++;
                [footer endRefreshing];
            }else{
                [footer endRefreshingWithNoMoreData];
            }
            
        }else{
            
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [AppUtils dismissHUDInView:self.view];
        NSLog(@"%@--%@",error,error.localizedDescription);
    }];
}

@end
