//
//  SeleGoDateView.m
//  youxia
//
//  Created by mac on 16/1/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SeleGoDateView.h"

@interface SeleGoDateView ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIPickerView *myPickerView;
@property (copy) void (^onDismissCompletion)(NSString *);
@property (copy) NSString *(^objectToStringConverter)(id object);

@end

@implementation SeleGoDateView{
    NSString *currentYearString;
    NSString *currentMonthString;
    NSString *currentDayString;
    NSMutableArray *yearArray;
    NSMutableArray *monthArray;
    NSMutableArray *dayArray;
}

-(void)initDataSourceMonth:(NSString *)month{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    // Get Current Year
    [formatter setDateFormat:@"yyyy"];
    currentYearString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    
    // Get Current Month
    [formatter setDateFormat:@"M"];
    currentMonthString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    
    // Get Current Day
    [formatter setDateFormat:@"dd"];
    currentDayString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    
    // PickerView -  Years data
    yearArray = [[NSMutableArray alloc] init];
    for (int i = 2000; i <= currentYearString.intValue ; i++){
        [yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    // PickerView -  Months data
    monthArray = [[NSMutableArray alloc] init];
    for (int i = 1; i <=12  ; i++){
        [monthArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
//    for (int i = currentMonthString.intValue; i <=12  ; i++){
//        [monthArray addObject:[NSString stringWithFormat:@"%d",i]];
//    }
    
    //PickerView -  Day data
    if (![month isEqualToString:currentMonthString]) {
        NSInteger daySum=[AppUtils getSumOfDaysInYear:currentYearString month:month];
        dayArray=[NSMutableArray array];
        for (int i=1; i<=daySum; i++) {
            [dayArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }else{
        NSInteger daySum=[AppUtils getSumOfDaysInYear:currentYearString month:currentMonthString];
        dayArray=[NSMutableArray array];
        for (int i=1; i<=daySum; i++) {
            [dayArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
//        for (int i=currentDayString.intValue; i<=daySum; i++) {
//            [dayArray addObject:[NSString stringWithFormat:@"%d",i]];
//        }
    }
}

#pragma mark - Init PickerView
-(id)initWithPickerViewInView:(UIView *)view whitYear:(NSString *)year month:(NSString *)month day:(NSString *)day{
    self = [super init];
    if (self) {
        
        [self initDataSourceMonth:(NSString *)month];
        
        [self setFrame: view.bounds];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //半透明黑色背景
        _backgroundView = [[UIView alloc] initWithFrame:view.bounds];
        [_backgroundView setBackgroundColor: [UIColor colorWithRed:0.412 green:0.412 blue:0.412 alpha:0.4]];
        [_backgroundView setAlpha:0.0];
        [_backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker:)]];
        [self addSubview:_backgroundView];
        
        //PickerView Container with top bar
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, _backgroundView.height - 236.0, self.width, 236.0)];
        _containerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_containerView];
        
        //ToolbarBackgroundColor - Black
        UIColor *toolbarBackgroundColor = [[UIColor alloc] initWithCGColor:[UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:0.8].CGColor];
        
        //Top bar view
        UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _containerView.width, 44.0)];
        [_containerView addSubview:topBarView];
        [topBarView setBackgroundColor:[UIColor whiteColor]];
        
        
        UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:topBarView.frame];
        [_containerView addSubview:pickerToolBar];
        
        CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        if (iOSVersion < 7.0) {
            pickerToolBar.tintColor = toolbarBackgroundColor;
        }else{
            [pickerToolBar setBackgroundColor:toolbarBackgroundColor];
            
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
            pickerToolBar.barTintColor = toolbarBackgroundColor;
#endif
        }
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIButton *closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.width-60, 4, 40, 40)];
        [closeBtn setTitle:@"确定" forState:UIControlStateNormal];
        [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 9, 0, -9)];
        [closeBtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
        closeBtn.titleLabel.font=[UIFont systemFontOfSize:16];
        [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
        pickerToolBar.items = @[flexibleSpace, barButtonItem];
        
        //Add pickerView
        _myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, topBarView.height, self.width, _containerView.height-topBarView.height)];
        _myPickerView.dataSource=self;
        _myPickerView.delegate=self;
        
        for (int i=0; i<yearArray.count; i++) {
            if ([year isEqualToString:yearArray[i]]) {
                [_myPickerView selectRow:i inComponent:0 animated:YES];
            }
        }
        for (int i=0; i<monthArray.count; i++) {
            if ([month isEqualToString:monthArray[i]]) {
                [_myPickerView selectRow:i inComponent:1 animated:YES];
            }
        }
        for (int i=0; i<dayArray.count; i++) {
            if ([day isEqualToString:dayArray[i]]) {
                [_myPickerView selectRow:i inComponent:2 animated:YES];
            }
        }
        
        [_containerView addSubview:_myPickerView];
        
        [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
        [view addSubview:self];
    }
    return self;
}

#pragma mark - Show Methods
-(void)showPickerViewCompletion:(void (^)(id))completion{
    [self setPickerHidden:NO callBack:nil];
    self.onDismissCompletion = completion;
}

#pragma mark - Dismiss Methods
-(void)dismissWithCompletion:(void (^)(NSString *))completion{
    [self setPickerHidden:YES callBack:completion];
}

-(void)dismiss{
    [self dismissWithCompletion:self.onDismissCompletion];
}

-(void)removePickerView{
    [self removeFromSuperview];
}

#pragma mark - Show/hide PickerView methods
-(void)setPickerHidden: (BOOL)hidden
              callBack: (void(^)(id))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (hidden) {
                             [_backgroundView setAlpha:0.0];
                             [_containerView setTransform:CGAffineTransformMakeTranslation(0.0, CGRectGetHeight(_containerView.frame))];
                         } else {
                             [_backgroundView setAlpha:1.0];
                             [_containerView setTransform:CGAffineTransformIdentity];
                         }
                     } completion:^(BOOL completed) {
                         if(completed && hidden){
                             [self removePickerView];
                             callBack([self selectedObject]);
                         }
                     }];
}

#pragma mark - 点击透明背景hidden
- (void)hideMyPicker:(void(^)(NSString *))callBack{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_backgroundView setAlpha:1.0];
                         [_containerView setTransform:CGAffineTransformIdentity];
                     } completion:^(BOOL completed) {
                         if(completed){
                             [self dismiss];
                         }
                     }];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component {
    if (component == 0) {
        return yearArray.count;
    }else if(component == 1){
        return monthArray.count;
    }else{
        return dayArray.count;
    }
}

- (NSString *)pickerView: (UIPickerView *)pickerView
             titleForRow: (NSInteger)row
            forComponent: (NSInteger)component {
    NSString *componentStr;
    if (component == 0) {
        componentStr=[NSString stringWithFormat:@"%@年",yearArray[row]];
    }
    else if(component == 1){
        componentStr=[NSString stringWithFormat:@"%@月",monthArray[row]];
    }else{
        componentStr=[NSString stringWithFormat:@"%@日",dayArray[row]];
    }
    return componentStr;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 110;
    } else if (component == 1) {
        return 100;
    } else {
        return 110;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 44;
}

//每选中月份时查询有多少天
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 1) {
        if(dayArray.count>0){
            [dayArray removeAllObjects];
        }
        NSInteger daySum=[AppUtils getSumOfDaysInYear:currentYearString month:monthArray[row]];
        if ([monthArray[row] isEqualToString:currentMonthString]) {
            dayArray=[NSMutableArray array];
            for (int i=currentDayString.intValue; i<=daySum; i++) {
                [dayArray addObject:[NSString stringWithFormat:@"%d",i]];
            }
        }else{
            for (int i=1; i<=daySum; i++) {
                [dayArray addObject:[NSString stringWithFormat:@"%d",i]];
            }
        }
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        pickerLabel.textAlignment=NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:17]];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (id)selectedObject {
    NSArray *arr;
    NSString *proStr=[yearArray objectAtIndex:[_myPickerView selectedRowInComponent:0]];
    NSString *cityStr=[monthArray objectAtIndex:[_myPickerView selectedRowInComponent:1]];
    NSString *dayStr=[dayArray objectAtIndex:[_myPickerView selectedRowInComponent:2]];
    arr=@[proStr,cityStr==NULL?@"":cityStr,dayStr];
    return arr;
}

@end
