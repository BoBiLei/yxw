//
//  ShowViewController.m
//  瀑布流
//
//  Created by 王志盼 on 15/7/12.
//  Copyright (c) 2015年 王志盼. All rights reserved.
//

#import "SellerShowViewController.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "SellerfashionCell.h"
#import "SellerFashionModel.h"
#import "SellerWaterFlowLayout.h"
#import "SellerDetailController.h"

@interface SellerShowViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>


@end

@implementation SellerShowViewController{
    NSMutableArray *showModels;
    
    NSMutableArray *goodsList;
    UICollectionView *colllecView;
    SellerWaterFlowLayout *waterfallFlowLayout;
    
    int  strIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"买家秀"];
    
    strIndex=2;
    
    [self requestHttpsForSellerShowList];
    
    [self setUpUI];
}

-(void)setUpUI{
    waterfallFlowLayout=[[SellerWaterFlowLayout alloc]init];
    waterfallFlowLayout.columnCount=2;
    waterfallFlowLayout.goodsList=goodsList;
    colllecView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) collectionViewLayout:waterfallFlowLayout];
    [colllecView registerNib:[UINib nibWithNibName:@"SellerfashionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    colllecView.dataSource = self;
    colllecView.delegate = self;
    colllecView.backgroundColor=[UIColor clearColor];
    colllecView.alwaysBounceVertical = YES;
    [self.view addSubview:colllecView];
    
    //加载更多
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self requestMoreSellerShowList];
        waterfallFlowLayout.columnCount=2;
        waterfallFlowLayout.goodsList=goodsList;
        [colllecView reloadData];
    }];
    footer.stateLabel.font = [UIFont systemFontOfSize:13];
    footer.stateLabel.textColor = [UIColor colorWithHexString:@"#282828"];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    colllecView.mj_footer = footer;
}

#pragma mark - 数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return goodsList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // 创建可重用的cell
    SellerfashionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (!cell) {
        cell=[[SellerfashionCell alloc]init];
    }
    if (indexPath.item<goodsList.count) {
        SellerFashionModel *model = goodsList[indexPath.item];
        [cell reflushDataForModel:model];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SellerFashionModel *model=goodsList[indexPath.item];
    SellerDetailController *sellerDetail=[[SellerDetailController alloc]init];
    sellerDetail.sellerId=model.sellerId;
    [self.navigationController pushViewController:sellerDetail animated:YES];
}


#pragma mark - Request 请求
-(void)requestHttpsForSellerShowList{
    goodsList=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"is_nosign":@"1",
                         @"act":@"show_list"
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"shows_list"];
            for (NSDictionary *dic in arr) {
                SellerFashionModel *model=[[SellerFashionModel alloc]init];
                [model jsonDataForDictionary:dic[@"img"]];
                [goodsList addObject:model];
            }
        }
        [AppUtils dismissHUDInView:self.view];
        [colllecView reloadData];
        [colllecView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissHUDInView:self.view];
    }];
}

//加载更多
-(void)requestMoreSellerShowList{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"ajax_show",
                         @"page":[NSString stringWithFormat:@"%d",strIndex]
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            strIndex++;
            NSArray *arr=responseObject[@"retData"][@"shows_list"];
            for (NSDictionary *dic in arr) {
                SellerFashionModel *model=[[SellerFashionModel alloc]init];
                [model jsonDataForDictionary:dic[@"img"]];
                [goodsList addObject:model];
            }
        }
        [colllecView reloadData];
        [colllecView.mj_footer endRefreshing];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
