//
//  GoodsEditController.h
//  MMG
//
//  Created by mac on 16/8/23.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface GoodsEditController : BaseController

@property (nonatomic, copy) NSString *goodsID;

@property (nonatomic, assign) BOOL isGoodsManageer;     //是否是商品管理的

@end
