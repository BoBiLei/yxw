//
//  UserCenterController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "UserCenterController.h"
#import "UserCenterCell.h"
#import "UserInfoCell.h"
#import "UserOrderController.h"
#import "UserCollectionController.h"
#import "AccounInfoController.h"
#import "PassSecurityController.h"
#import "CouponController.h"
#import "NewMsgController.h"
#import "ApproveController.h"
#import "MaijiaInfoCell.h"
#import "NewGoodsAddedController.h"
#import "SoldOutGoodsController.h"
#import "HadSellGoodsController.h"
#import "MaijiaGoodsManager.h"
#import "AdverManagerController.h"
#import "MaijiaUserInfoController.h"

@interface UserCenterController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation UserCenterController{
    BOOL isMaiJia;
    NSArray *dataArr;
    UITableView *userCenterTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goManagerList) name:@"Insert_success" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endingReflush) name:@"usercenter_ending_reflush" object:nil];
    
    isMaiJia=NO;
    [self setCustomRootViewNavigationTitle:@"用户中心"];
    
    [self setUpUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePhotoNoti) name:NotificationUpdatePhoto object:nil];
    
    
//    [self requestHttpsSetOrderStatusForOrderNum:@"123" orderAmout:@"123"];
}

////更改订单状态
//-(void)requestHttpsSetOrderStatusForOrderNum:(NSString *)orderNum orderAmout:(NSString *)orderAmout{
//    time_t now;
//    time(&now);
//    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
//    NSString *nonce_str	= [MMGSign randomNumber];
//    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
//    [dict setObject:@"1" forKey:@"is_phone"];
//    [dict setObject:@"paylog" forKey:@"act"];
//    [dict setObject:@"121212" forKey:@"order_sn"];
//    [dict setObject:@"100" forKey:@"order_amount"];
//    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
//    [dict setObject:time_stamp forKey:@"stamp"];
//    [dict setObject:nonce_str forKey:@"noncestr"];
//    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
//    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
//    [dict setObject:sign forKey:@"sign"];
//    NSLog(@"%@",dict);
//    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"iso_pay.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
//        
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"%@",error);
//    }];
//}


-(void)goManagerList{
    MaijiaGoodsManager *ctr=[MaijiaGoodsManager new];
    ctr.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:ctr animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(obNotificate) name:NotifitionLogOut object:nil];
    
}

-(void)obNotificate{
    self.tabBarController.selectedIndex=0;
}

-(void)updatePhotoNoti{
    [userCenterTable reloadData];
}

-(void)endingReflush{
    [userCenterTable.mj_header endRefreshing];
}

#pragma mark - nav RightBarItem
-(void)setUpNavigationRightBarItem{
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 30, 30);
    [rightBtn setImage:[UIImage imageNamed:@"message"] forState:UIControlStateNormal];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(4, 10, 0, -8)];
    UIBarButtonItem *rightViewItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightViewItem.badgeBGColor = [UIColor redColor];
    rightViewItem.badgeTextColor = [UIColor whiteColor];
    [rightBtn addTarget:self action:@selector(goMsgCenter) forControlEvents:UIControlEventTouchDown];
    self.navigationItem.rightBarButtonItem = rightViewItem;
}

#pragma mark - 消息中心
-(void)goMsgCenter{
    
}

#pragma mark - alipay delegate
-(void)pushOrderDetailViewWithController{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - init UI
-(void)setUpUI{
    NSString *plistPath;
    if (isMaiJia) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"MaijiaCenter" ofType:@"plist"];
    }else{
        plistPath = [[NSBundle mainBundle] pathForResource:@"UserCenter" ofType:@"plist"];
    }
    dataArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    userCenterTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    userCenterTable.showsVerticalScrollIndicator=NO;
    userCenterTable.dataSource=self;
    userCenterTable.delegate=self;
    [userCenterTable registerNib:[UINib nibWithNibName:@"UserCenterCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [userCenterTable registerNib:[UINib nibWithNibName:@"UserInfoCell" bundle:nil] forCellReuseIdentifier:@"userinfocell"];
    [self.view addSubview:userCenterTable];
    
    
//    /*
//     *下拉刷新
//     */
//    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        // 延迟加载
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionLoginSuccess object:nil];
//        });
//    }];
//    header.arrowView.hidden=NO;
//    
//    [header setMj_h:52];//设置高度
//    [header setTitle:@"正在刷新…" forState:MJRefreshStateRefreshing];
//    // 设置字体
//    [header.stateLabel setMj_y:23];
//    header.stateLabel.font = [UIFont systemFontOfSize:13];
//    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:13];
//    userCenterTable.mj_header = header;
}

-(UIView *)setUpHeaderView{
    UIView *headView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width/2)];
    headView.backgroundColor=[UIColor clearColor];
    
    UIImageView *phoneView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headView.height/2-5, headView.height/2-5)];
    phoneView.center=CGPointMake(headView.width/2, headView.height/2-headView.height/6);
    phoneView.layer.cornerRadius=phoneView.height/2;
    phoneView.layer.masksToBounds=YES;
    if ([[AppUtils getValueWithKey:User_Photo] isEqualToString:ImageHost01]) {
        phoneView.image=[UIImage imageNamed:@"user_default"];
    }else{
        [phoneView setImageWithURL:[NSURL URLWithString:[AppUtils getValueWithKey:User_Photo]]];
    }
    phoneView.contentMode=UIViewContentModeScaleAspectFit;
    [headView addSubview:phoneView];
    
    //
    UILabel *dpNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, phoneView.origin.y+phoneView.height+4, SCREENSIZE.width, (headView.height-phoneView.origin.y-phoneView.height-4)/3)];
    dpNameLabel.textAlignment=NSTextAlignmentCenter;
    dpNameLabel.font=[UIFont systemFontOfSize:16];
    dpNameLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
    dpNameLabel.text=@"店铺名:XXX";
    [headView addSubview:dpNameLabel];
    
    //
    UILabel *yyeLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, dpNameLabel.origin.y+dpNameLabel.height, SCREENSIZE.width, dpNameLabel.height)];
    yyeLabel.textAlignment=NSTextAlignmentCenter;
    yyeLabel.font=[UIFont systemFontOfSize:16];
    yyeLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
    yyeLabel.text=@"营业额:200000";
    [headView addSubview:yyeLabel];
    
    //
    UILabel *spSumLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, yyeLabel.origin.y+yyeLabel.height, SCREENSIZE.width, yyeLabel.height)];
    spSumLabel.textAlignment=NSTextAlignmentCenter;
    spSumLabel.font=[UIFont systemFontOfSize:16];
    spSumLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
    spSumLabel.text=@"总商品数量:500件";
    [headView addSubview:spSumLabel];
    return headView;
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return ((NSArray*)dataArr[section]).count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 92;
    }else{
        return 46;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 18;
    }else{
        return 0.000000001;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isMaiJia) {
        if (indexPath.section==0) {
            MaijiaInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"mjcell"];
            if (cell==nil) {
                cell=[[MaijiaInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mjcell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return  cell;
        }else{
            UserCenterCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (cell==nil) {
                cell=[[UserCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.title.text=dataArr[indexPath.section][indexPath.row][@"nameStr"];
            [cell.imgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"img"]]];
            return  cell;
        }

    }else{
        if (indexPath.section==0) {
            UserInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"userinfocell"];
            if (cell==nil) {
                cell=[[UserInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"userinfocell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            return  cell;
        }else{
            UserCenterCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (cell==nil) {
                cell=[[UserCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            }
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.title.text=dataArr[indexPath.section][indexPath.row][@"nameStr"];
            [cell.imgv setImage:[UIImage imageNamed:dataArr[indexPath.section][indexPath.row][@"img"]]];
            return  cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isMaiJia) {
        if (indexPath.section==0) {
            MaijiaUserInfoController *accountInfo=[[MaijiaUserInfoController alloc]init];
            accountInfo.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:accountInfo animated:YES];
        }else if(indexPath.section==1){
            switch (indexPath.row) {
                case 0:
                {
                    NewGoodsAddedController *ctr=[NewGoodsAddedController new];
                    ctr.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
                    break;
                case 1:
                {
                    SoldOutGoodsController *ctr=[SoldOutGoodsController new];
                    ctr.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
                    break;
                case 2:
                {
                    HadSellGoodsController *ctr=[HadSellGoodsController new];
                    ctr.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
                    break;
                case 3:
                {
                    MaijiaGoodsManager *ctr=[MaijiaGoodsManager new];
                    ctr.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
                    break;
                default:
                    break;
            }
        }else if(indexPath.section==2){
            AdverManagerController *ctr=[AdverManagerController new];
            ctr.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:ctr animated:YES];
        }else{
            PassSecurityController *passSecurity=[[PassSecurityController alloc]init];
            passSecurity.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:passSecurity animated:YES];
        }
    }else{
        if (indexPath.section==0) {
            AccounInfoController *accountInfo=[[AccounInfoController alloc]init];
            accountInfo.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:accountInfo animated:YES];
        }
        else if(indexPath.section==1){
            switch (indexPath.row) {
                case 0:
                {
                    UserOrderController *userOrder=[[UserOrderController alloc]init];
                    userOrder.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:userOrder animated:YES];
                }
                    break;
                case 1:
                {
                    UserCollectionController *userCollect=[[UserCollectionController alloc]init];
                    userCollect.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:userCollect animated:YES];
                }
                    break;
                default:
                    break;
            }
        }else{
            switch (indexPath.row) {
                case 0:
                {
                    ApproveController *ctr=[ApproveController new];
                    ctr.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:ctr animated:YES];
                }
                    
                    break;
                case 1:
                {
                    PassSecurityController *passSecurity=[[PassSecurityController alloc]init];
                    passSecurity.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:passSecurity animated:YES];
                }
                    break;
                case 2:
                {
                    CouponController *coupon=[[CouponController alloc]init];
                    coupon.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:coupon animated:YES];
                }
                    break;
                case 3:
                {
                    NewMsgController *newMsg=[[NewMsgController alloc]init];
                    newMsg.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:newMsg animated:YES];
                }
                    break;
                default:
                    
                    break;
            }
        }
    }
}


#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
