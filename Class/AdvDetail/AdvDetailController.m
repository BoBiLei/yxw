//
//  AdvDetailController.m
//  MMG
//
//  Created by mac on 15/10/8.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "AdvDetailController.h"
#import "AdvDetailCell.h"

@interface AdvDetailController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation AdvDetailController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"注册有礼"];
    [self setUpUI];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"AdvDetail" ofType:@"plist"];
    NSArray *plistArr = [[NSArray alloc] initWithContentsOfFile:plistPath];
    for (NSDictionary *dic in plistArr) {
        AdvDetailModel *model=[[AdvDetailModel alloc]init];
        [model jsonDataForDictionary:dic];
        [dataArr addObject:model];
    }
    [myTable reloadData];
}

#pragma mark - init UI
-(void)setUpUI{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    myTable.dataSource=self;
    myTable.delegate=self;
    [myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [myTable registerNib:[UINib nibWithNibName:@"AdvDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
    myTable.tableFooterView=[[UIView alloc]init];
}

#pragma mark - TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (indexPath.row<dataArr.count) {
        AdvDetailModel *model=dataArr[indexPath.row];
        [cell reflushDataForModel:model];
    }
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdvDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[AdvDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    AdvDetailModel *model=dataArr[indexPath.row];
    [cell reflushDataForModel:model];
    return  cell;
}


@end
