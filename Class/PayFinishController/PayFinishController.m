//
//  PayFinishController.m
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "PayFinishController.h"
#import "PayFinishCell.h"
#import "UserOrderController.h"
#import "ShoppingCartController.h"

@interface PayFinishController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation PayFinishController{
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self setCustomNavigationTitle:@"支付完成"];
    
    //不允许手势滑动返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [self setUpUI];
}

#pragma mark - 覆盖BaseController的setCustomNavigationTitle方法
-(void)setCustomNavigationTitle:(NSString *)title{
    UILabel *navTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 100, 50)];
    navTitle.backgroundColor = [UIColor clearColor];
    navTitle.font = [UIFont boldSystemFontOfSize:NAV_TITLE_FONT_SIZE];
    navTitle.textColor = [UIColor whiteColor];
    navTitle.text = title;
    navTitle.textAlignment=NSTextAlignmentCenter;
    self.navigationItem.titleView = navTitle;
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame=CGRectMake(-6, 6,22.f,22.f);
    [back setBackgroundImage:[UIImage imageNamed:@"nav_turnback"] forState:UIControlStateNormal];
    back.imageEdgeInsets=UIEdgeInsetsMake(0, -10, 0, 0);
    [back addTarget:self action:@selector(flagTurnBack) forControlEvents:UIControlEventTouchDown];
    self.navReturnBtn=back;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backItem;
}

-(void)flagTurnBack{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[UserOrderController class]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateOrderList" object:nil];
            [self.navigationController popToViewController:controller animated:YES];
        }
        if ([controller isKindOfClass:[ShoppingCartController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

#pragma mark - init UI
-(void)setUpUI{
    
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 38)];
    imgv.contentMode=UIViewContentModeScaleToFill;
    imgv.image=[UIImage imageNamed:@"submit_order03"];
    [self.view addSubview:imgv];
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(8, 50, SCREENSIZE.width-16, SCREENSIZE.height-162) style:UITableViewStyleGrouped];
    myTable.showsVerticalScrollIndicator=NO;
    [myTable registerNib:[UINib nibWithNibName:@"PayFinishCell" bundle:nil] forCellReuseIdentifier:@"payfinishcell"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [self.view addSubview:myTable];
    
    //
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"我的订单" forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:14];
    btn.frame=CGRectMake(0, 0, SCREENSIZE.width, 40);
    btn.layer.cornerRadius=2;
    [btn addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchDown];
    myTable.tableFooterView=btn;
}

#pragma mark - 点击我的订单按钮
-(void)clickBtn{
    UserOrderController *userOrder=[[UserOrderController alloc]init];
    userOrder.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:userOrder animated:YES];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 133;
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 34;
}


//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTable.frame.size.width, 34)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    
    //订单编号
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, SCREENSIZE.width/2+20, headerSectionView.frame.size.height)];
    label.font=[UIFont systemFontOfSize:13];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    [headerSectionView addSubview:label];
    if (section==0) {
        label.text=@"支付信息";
    }
    return headerSectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PayFinishCell *cell;
    cell=[tableView dequeueReusableCellWithIdentifier:@"payfinishcell"];
    if (!cell) {
        cell=[[PayFinishCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"payfinishcell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.paySuccessLabel.text=[NSString stringWithFormat:@"您已成功支付%@元",self.payPrice];
    cell.orderNum.text=[NSString stringWithFormat:@"订单编号：%@元",self.orderNum];
    cell.orderTitle.text=[NSString stringWithFormat:@"产品名称：%@元",self.goodName];
    return  cell;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

@end
