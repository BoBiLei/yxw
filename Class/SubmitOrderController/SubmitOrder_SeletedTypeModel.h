//
//  SubmitOrder_SeletedTypeModel.h
//  MMG
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubmitOrder_SeletedTypeModel : NSObject

@property (nonatomic, copy) NSString *seleTag;

@property (nonatomic, copy) NSString *typeId;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, copy) NSString *url;

@property (nonatomic, retain) NSArray *ejFlArr;                 //二级分类Arr
@property (nonatomic, retain) NSMutableArray *paramArr_Item;    //属性-小属性
@property (nonatomic, copy) NSString *paramArr_ItemId;          //属性-小属性
@property (nonatomic, copy) NSString *paramArr_ItemName;        //属性-小属性
@property (nonatomic, copy) NSString *input_type;               //0输入  1下拉
@property (nonatomic, copy) NSString *attr_value;  

/**
 获取分类数据
 */
-(void)getFenleiJsonDataForDictionary:(NSDictionary *)dic;

/**
获取品牌数据
 */
-(void)getBranceJsonDataForDictionary:(NSDictionary *)dic;

/**
 获取品牌类型
 */
-(void)getGoodTypeDataForDictionary:(NSDictionary *)dic;

/**
 Param数据
 */
-(void)getParamDataForDictionary:(NSDictionary *)dic;

/**
 二级分类
 */
-(void)getErjiFenleiForDictionary:(NSDictionary *)dic;

@end
