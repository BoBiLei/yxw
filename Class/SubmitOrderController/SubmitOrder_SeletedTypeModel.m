//
//  SubmitOrder_SeletedTypeModel.m
//  MMG
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "SubmitOrder_SeletedTypeModel.h"

@implementation SubmitOrder_SeletedTypeModel

-(void)getFenleiJsonDataForDictionary:(NSDictionary *)dic{
    self.typeId=dic[@"id"];
    self.typeName=dic[@"name"];
    self.url=dic[@"url"];
    self.ejFlArr=dic[@"cat_id"];
//    self.paramArr=dic[@"attr"];
}

-(void)getBranceJsonDataForDictionary:(NSDictionary *)dic{
    self.typeId=dic[@"brand_id"];
    self.typeName=dic[@"brand_name"];
}


/**
 获取品牌类型
 */
-(void)getGoodTypeDataForDictionary:(NSDictionary *)dic{
    self.typeId=dic[@"cat_id"];
    self.typeName=dic[@"cat_name"];
}

-(void)getParamDataForDictionary:(NSDictionary *)dic{
    self.paramArr_ItemId=dic[@"attr_id"];
    self.paramArr_ItemName=dic[@"attr_name"];
    self.input_type=dic[@"attr_input_type"];
    if ([dic objectForKey:@"attr_value"]) {
        self.attr_value=dic[@"attr_value"];
    }else{
        self.attr_value=@"";
    }
    
    NSArray *arr=dic[@"attr_values"];
    NSMutableArray *iArr=[NSMutableArray array];
    for (int i=0; i<arr.count; i++) {
        SubmitOrder_SeletedTypeModel *model=[SubmitOrder_SeletedTypeModel new];
        model.seleTag=@"0";     //默认选择第一个
        model.typeId=[NSString stringWithFormat:@"%d",i];
        model.typeName=arr[i];
        if ([self.attr_value isEqualToString:model.typeName]) {
            self.seleTag=[NSString stringWithFormat:@"%d",i];
        }
        if (i==0) {
            self.typeName=arr[i];
        }
        [iArr addObject:model];
    }
    self.paramArr_Item=iArr;
}

-(void)getErjiFenleiForDictionary:(NSDictionary *)dic{
    self.typeId=dic[@"brothers_id"];
    self.typeName=dic[@"brothers_name"];
}

@end
