//
//  SubmitOrderController.h
//  MMG
//
//  Created by mac on 15/10/10.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"


@interface SubmitOrderController : BaseController

@property(nonatomic ,assign) CGFloat shuiFeeFloat;//税费
@property(nonatomic ,copy) NSString *stageSum;//分期数
@property(nonatomic ,copy) NSString *stageFee;//分期手续费
@property(nonatomic ,copy) NSArray *goodsIDArr;//商品所有id
@property(nonatomic ,copy) NSString *totalPrice;//总金额
@property(nonatomic ,copy) NSString *orderPrice;//订单总金额
@property(nonatomic ,copy) NSString *everyStagePrice;//每期应付
@property(nonatomic ,copy) NSString *seleGoodSum;//选择商品数
@property(nonatomic, assign)NSMutableArray *receiveDataArr;//商品列表

@property (nonatomic, strong) void (^kReceiverOrCouponModelBlock)(id model);

@end
