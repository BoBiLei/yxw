//
//  EditMaijiaInfoController.h
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "BaseController.h"



@interface EditMaijiaInfoController : BaseController

@property (nonatomic, copy) NSString *titleStr;

@property (nonatomic) NSIndexPath *indexPath;

@end
