//
//  EditMaijiaInfoController.m
//  MMG
//
//  Created by mac on 16/9/8.
//  Copyright © 2016年 猫猫购. All rights reserved.
//

#import "EditMaijiaInfoController.h"
#import "EditMaijiaInfoCell.h"

@interface EditMaijiaInfoController ()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,EditMaijiaInfoDelegate>

@end

@implementation EditMaijiaInfoController{
    NSString *retParStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavigation];
    
    UITableView *myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.dataSource=self;
    myTable.delegate=self;
    [self.view addSubview:myTable];
}

#pragma mark - setUpNavigation
-(void)setUpNavigation{
    [self setCustomNavigationTitle:self.titleStr];
    UIButton *navRightBtn=[self setCustomRightBarButtonItemSetFrame:CGRectMake(0, 0, 40, 40) Text:@"确定"];
    [navRightBtn addTarget:self action:@selector(clickSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 点击确定按钮
-(void)clickSaveBtn:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditMaijiaInfoText" object:@[retParStr,self.indexPath]];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark table delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EditMaijiaInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[EditMaijiaInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.nameStr=_titleStr;
    cell.delegate=self;
    return  cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - cell tf-delegate
-(void)setNameTextfield:(NSString *)nameStr indexPath:(NSIndexPath *)indexPath{
    retParStr=nameStr;
}

@end
