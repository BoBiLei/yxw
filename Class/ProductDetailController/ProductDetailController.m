//
//  ProductDetailController.m
//  CatShopping
//
//  Created by mac on 15/9/21.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "ProductDetailController.h"
#import "ShoppingCartModel.h"
#import "LoginController.h"
#import "ProductDetailCell.h"
#import "ProductDetailPicCell.h"
#import "ProdectParamDetailCell.h"
#import "GoodParamModel.h"
#import "AppDelegate.h"
#import "WXPartnerConfig.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
//自定义分享编辑页面需要
#import <ShareSDK/ISSShareViewDelegate.h>
#import "ShopController.h"
#import "CustomIOSAlertView.h"
#import "CopyLabel.h"

#define kTagShareEdit 101

#define LeftLabel_FontSize 14

@interface ProductDetailController ()<UITableViewDataSource,UITableViewDelegate,ISSShareViewDelegate>

/**
 *  面板
 */
@property (nonatomic, strong) UIView *panelView;

/**
 *  加载视图
 */
@property (nonatomic, strong) UIActivityIndicatorView *loadingView;

@end

@implementation ProductDetailController{
    
    //微信二维码、微博、QQ号
    NSString *ewmStr;
    NSString *ewmId;
    NSString *wbStr;
    NSString *qqStr;
    
    //
    CustomIOSAlertView *myAlertView;
    UIView *demoView;
    UIImage *saveImage;     //保存到相册的image
    
    BOOL isHadBsnName;
    
    NSString *jicun_id;     //寄存ID
    NSString *cat_id;     //寄存ID
    
    NSDictionary *headVerDic;
    UIButton *btn;
    
    NSMutableArray *goodPicArr;
    NSMutableArray *propertyArr;
    UITableView *myTable;
    UIButton *seletedBtn;
    
    CGFloat allPriceFloat;
    
    CGFloat stageRate01;
    CGFloat stageRate03;
    CGFloat stageRate06;
    CGFloat stageRate09;
    CGFloat stageRate12;
    CGFloat shuiFee;        //税费
    
    NSString *remittancesFee01; //1期Fee
    NSString *remittancesFee03; //3期Fee
    NSString *remittancesFee06; //6期Fee
    NSString *remittancesFee09; //9期Fee
    NSString *remittancesFee12; //12期Fee
    UIButton *seletedFenqiBtn;
    
    BOOL isClickGoodDetail;
    
    UIView *filterView;
    UIButton *sectionBtn01;
    UIButton *sectionBtn02;
    
    
    UILabel *locationR;
    
    //分享商品描述、图片
    NSString *goodDescribe;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setCustomNavigationTitle:@"商品详情"];
    
    UIButton *navRightBtn=[self setCustomRightBarButtonItemSetFrame:CGRectMake(0, 0, 40, 40) image:[UIImage imageNamed:@"share"]];
    [navRightBtn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    if ([WXApi isWXAppInstalled]||[QQApiInterface isQQInstalled]) {
        navRightBtn.hidden=NO;
    }else{
        navRightBtn.hidden=YES;
    }
    
    [self requestHttpsForStageRate];
    
    [self requestHttpsForGoodDetail:self.goodId];
}

//弹出列表方法presentSnsIconSheetView需要设置delegate为self
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}

#pragma mark - 点击分享按钮
-(void)clickShareBtn:(UIButton *)sender{
    //1.1、要分享的图片（以下分别是网络图片和本地图片的生成方式的示例）
    id<ISSCAttachment> shareImg = [ShareSDKCoreService attachmentWithUrl:[NSString stringWithFormat:@"%@%@",ImageHost01,self.goodImgStr]];
    
    //1.2、以下参数分别对应：内容、默认内容、图片、标题、链接、描述、分享类型
    id<ISSContent> publishContent = [ShareSDK content:goodDescribe
                                       defaultContent:nil
                                                image:shareImg
                                                title:self.goodTitle
                                                  url:[NSString stringWithFormat:@"http://m.maomaogo.com/m/goods.php?id=%@",self.goodId]
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeNews];
    
    //2、展现分享菜单
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:NO
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                //NSLog(@"%d-%u-%ld-%@",type,state,(long)error.errorCode,error.errorDescription);
                            }];
}

#pragma mark - share sdk自定义导航栏
- (void)viewOnWillDisplay:(UIViewController *)viewController shareType:(ShareType)shareType
{
    //修改导航栏背景颜色、图片
    viewController.navigationController.navigationBar.barTintColor = [UIColor blueColor];
    //    [viewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"ShareButtonBG.png"] forBarMetrics:UIBarMetricsDefault];
    
    //修改左右按钮的文字颜色
    UIBarButtonItem *leftBtn = viewController.navigationItem.leftBarButtonItem;
    UIBarButtonItem *rightBtn = viewController.navigationItem.rightBarButtonItem;
    [leftBtn setTintColor:[UIColor colorWithHexString:@"#282828"]];
    [rightBtn setTintColor:[UIColor colorWithHexString:@"#282828"]];
    
    //修改标题颜色和文字
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:@"#282828"];
    label.text = viewController.title;
    label.text = @"新浪分享";
    label.font = [UIFont systemFontOfSize:17];
    [label sizeToFit];
    viewController.navigationItem.titleView = label;
}

#pragma mark - Header View

-(UIView *)setUpHeaderViewWithDic:(NSDictionary *)dic{
    
    cat_id=[NSString stringWithFormat:@"%@",dic[@"cat_id"]];
    jicun_id=[NSString stringWithFormat:@"%@",dic[@"jicun_uid"]];
    
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.width+410)];
    
    //商品图片
    UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.width)];
    NSString *imgStr=headVerDic[@"goods"][@"original_img"];
    [imgv setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageHost03,imgStr]]];
    imgv.contentMode=UIViewContentModeScaleAspectFit;
    [headerView addSubview:imgv];
    
    //line
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, imgv.frame.size.height, SCREENSIZE.width, 0.8f)];
    line.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    [headerView addSubview:line];
    
    //商品名称
    UILabel *goodNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, line.origin.y+line.height+8, SCREENSIZE.width-16, 52)];
    goodNameLabel.numberOfLines=0;
    goodNameLabel.font=[UIFont systemFontOfSize:16];
    [headerView addSubview:goodNameLabel];
    NSString *gnStr=headVerDic[@"goods"][@"goods_name"];
    NSMutableAttributedString *gnString = [[NSMutableAttributedString alloc] initWithString:gnStr];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4];//调整行间距
    [gnString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [gnStr length])];
    goodNameLabel.attributedText = gnString;
    
    //商品编号
    UILabel *goodNumberL=[[UILabel alloc] initWithFrame:CGRectMake(8, goodNameLabel.origin.y+goodNameLabel.height+8, 72, 21)];
    goodNumberL.text=@"商品编号：";
    goodNumberL.textColor=[UIColor colorWithHexString:@"#282828"];
    goodNumberL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:goodNumberL];
    
    //
    UILabel *goodNumberR=[[UILabel alloc] initWithFrame:CGRectMake(goodNumberL.origin.x+goodNumberL.width, goodNumberL.origin.y, SCREENSIZE.width-(goodNumberL.origin.x+goodNumberL.width+8), 21)];
    goodNumberR.text=headVerDic[@"goods"][@"goods_sn"];
    goodNumberR.textColor=[UIColor colorWithHexString:@"#282828"];
    goodNumberR.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:goodNumberR];
    
    //系列
    UILabel *seriesL=[[UILabel alloc] initWithFrame:CGRectMake(8, goodNumberL.origin.y+goodNumberL.height+5, goodNumberL.width, goodNumberL.height)];
    seriesL.text=[NSString stringWithFormat:@"系        列:"];
    seriesL.textColor=[UIColor colorWithHexString:@"#282828"];
    seriesL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:seriesL];
    
    //
    
    TTTAttributedLabel *seriesR = [TTTAttributedLabel new];
    [seriesR setFont:[UIFont systemFontOfSize:LeftLabel_FontSize]];
    [seriesR setBackgroundColor:[UIColor clearColor]];
    [seriesR setTextAlignment:NSTextAlignmentLeft];
    [seriesR setLineBreakMode:NSLineBreakByWordWrapping];
    seriesR.textColor=[UIColor colorWithHexString:@"#282828"];
    NSString *quanweiStr=@"（本商品已通过猫猫购权威鉴定）";
    NSString *seriesStr=[NSString stringWithFormat:@"%@%@",headVerDic[@"category_name"],quanweiStr];
    CGFloat seriesR_height=0;
    CGSize seriesSize=[AppUtils getStringSize:seriesStr withFont:LeftLabel_FontSize];
    if (seriesSize.width>SCREENSIZE.width-(seriesL.origin.x+seriesL.width+8)) {
        seriesR_height=goodNumberR.height*2-4;
        seriesR.numberOfLines=0;
    }else{
        seriesR_height=goodNumberR.height;
        seriesR.numberOfLines=1;
    }
    seriesR.frame=CGRectMake(seriesL.origin.x+seriesL.width, seriesL.origin.y, SCREENSIZE.width-(seriesL.origin.x+seriesL.width+8), seriesR_height);
    [headerView addSubview:seriesR];
    
    UIFont *boldSystemFont = [UIFont systemFontOfSize:LeftLabel_FontSize];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
    [seriesR setText:seriesStr afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",quanweiStr] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor colorWithHexString:@"#0D9C0D"] CGColor] range:sumRange];
        [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:sumRange];
        return mutableAttributedString;
    }];
    
    //级别
    UILabel *levelL=[[UILabel alloc] initWithFrame:CGRectMake(8, seriesR.origin.y+seriesR.height+5, seriesL.width, seriesL.height)];
    levelL.text=@"级        别:";
    levelL.textColor=[UIColor colorWithHexString:@"#282828"];
    levelL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:levelL];
    
    //
    UILabel *levelR=[[UILabel alloc] initWithFrame:CGRectMake(seriesR.origin.x, levelL.origin.y, goodNumberR.width, goodNumberR.height)];
    NSString *levelStr=dic[@"ex"][@"level"];
    NSString *levelLabelStr;
    switch (levelStr.intValue) {
        case 1:
            levelLabelStr=@"全新";
            break;
        case 2:
            levelLabelStr=@"99新";
            break;
        case 3:
            levelLabelStr=@"95新";
            break;
        case 4:
            levelLabelStr=@"9新";
            break;
        case 5:
            levelLabelStr=@"85新";
            break;
        case 6:
            levelLabelStr=@"8新";
            break;
            
        default:
            break;
    }
    levelR.text=levelLabelStr;
    levelR.textColor=[UIColor colorWithHexString:@"#282828"];
    levelR.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:levelR];
    
    //说明
    UILabel *explainL=[[UILabel alloc] initWithFrame:CGRectMake(8, levelL.origin.y+levelL.height+5, levelL.width, levelL.height)];
    explainL.text=@"说        明:";
    explainL.textColor=[UIColor colorWithHexString:@"#282828"];
    explainL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:explainL];
    
    //
    UILabel *explainR=[UILabel new];
    NSString *explainStr=dic[@"ex"][@"explain"];
    NSLog(@"%@---------",explainStr);
    if ([explainStr isEqualToString:@""]) {
        explainStr=@"暂无说明";
    }
    CGFloat explainY=0;
    CGFloat explainR_height=0;
    CGSize explainRSize=[AppUtils getStringSize:explainStr withFont:LeftLabel_FontSize];
    CGFloat exp_param=explainRSize.width/(SCREENSIZE.width-(explainL.origin.x+explainL.width+8));
    NSString *exp_paramStr=[NSString stringWithFormat:@"%.2f",exp_param];
    NSRange expRang=[exp_paramStr rangeOfString:@"."];
    if (expRang.location!=NSNotFound) {
        exp_paramStr=[exp_paramStr substringToIndex:expRang.location];
    }
    if (explainRSize.width>SCREENSIZE.width-(explainL.origin.x+explainL.width+8)) {
        explainR_height=explainL.height*(exp_paramStr.intValue+1)-4;
        explainR.numberOfLines=0;
        explainY=explainL.origin.y-(exp_paramStr.intValue+0.8)*1;
    }else{
        explainR_height=explainL.height;
        explainR.numberOfLines=1;
        explainY=explainL.origin.y;
    }
    explainR.frame=CGRectMake(levelR.origin.x, explainY, levelR.width, explainR_height);
    
    explainR.text=explainStr;
    explainR.textColor=[UIColor colorWithHexString:@"#282828"];
    explainR.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:explainR];
    
    
    //所在地
    UILabel *locationL=[[UILabel alloc] initWithFrame:CGRectMake(8, explainR.origin.y+explainR.height+5, explainL.width, explainL.height)];
    locationL.text=@"所  在  地:";
    locationL.textColor=[UIColor colorWithHexString:@"#282828"];
    locationL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:locationL];
    
    //
    locationR=[[UILabel alloc] initWithFrame:CGRectMake(explainR.origin.x, locationL.origin.y, explainR.width, locationL.height)];
    NSString *locationStr;
    if ([dic[@"goods_number"] isEqualToString:@"0"]) {
        locationStr=[NSString stringWithFormat:@"深圳 （已售）"];
    }else{
        locationStr=[NSString stringWithFormat:@"深圳 （有货）"];
    }
    locationR.text=locationStr;
    locationR.textColor=[UIColor colorWithHexString:@"#282828"];
    locationR.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:locationR];
    
    
    //价格
    UILabel *priceL=[[UILabel alloc] initWithFrame:CGRectMake(8, locationL.origin.y+locationL.height+5, locationL.width, locationL.height)];
    priceL.text=@"价        格:";
    priceL.textColor=[UIColor colorWithHexString:@"#282828"];
    priceL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:priceL];
    
    //
    UILabel *priceR=[[UILabel alloc] initWithFrame:CGRectMake(locationR.origin.x, priceL.origin.y, locationR.width, locationR.height)];
    priceR.text=headVerDic[@"goods"][@"shop_price_formated"];
    priceR.textColor=[UIColor colorWithHexString:@"#cf1316"];
    priceR.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:priceR];
    
    
    //分期价
    UILabel *stagepriceL=[[UILabel alloc] initWithFrame:CGRectMake(8, priceL.origin.y+priceL.height+5, priceL.width, priceL.height)];
    stagepriceL.text=@"分  期  价:";
    stagepriceL.textColor=[UIColor colorWithHexString:@"#282828"];
    stagepriceL.font=[UIFont systemFontOfSize:LeftLabel_FontSize];
    [headerView addSubview:stagepriceL];
    
    //分期按钮
    CGFloat btnH=40;
    for (int i=0; i<5; i++) {
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(8, (stagepriceL.origin.y+stagepriceL.height)+i*btnH+i*6+8, SCREENSIZE.width-16, btnH);
        btn.layer.borderWidth=0.5f;
        btn.layer.borderColor=[UIColor colorWithHexString:@"#ababab"].CGColor;
        btn.layer.cornerRadius=2;
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        [headerView addSubview:btn];
        btn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        CGFloat remittancesFee=0;  //手续费
        NSString *rateStr01=@"分1期 ";
        NSString *rateStr03=@"分3期 ";
        NSString *rateStr06=@"分6期 ";
        NSString *rateStr09=@"分9期 ";
        NSString *rateStr12=@"分12期 ";
        CGFloat stagePriceFloat=0;  //各个分期价格
        NSString *stagePriceStr;       //各个分期价格
        
        NSString *btnStr;
        if (i==0) {
            btn.tag=1;
            remittancesFee=allPriceFloat*stageRate01;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee01=remittancesFeeStr;
            
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            stagePriceFloat=allPriceFloat/1;
            stagePriceStr=[NSString stringWithFormat:@"%@%.f元/期",rateStr01,stagePriceFloat];
            
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
            
            [self defaultSelected];
        }else if (i==1){
            btn.tag=3;
            remittancesFee=allPriceFloat*stageRate03;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee03=remittancesFeeStr;
            
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/3;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr03,subStageStr];
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if (i==2){
            btn.tag=6;
            remittancesFee=allPriceFloat*stageRate06;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee06=remittancesFeeStr;
            
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/6;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr06,subStageStr];
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if (i==3){
            btn.tag=9;
            remittancesFee=allPriceFloat*stageRate09;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee09=remittancesFeeStr;
            
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            
            //
            stagePriceFloat=allPriceFloat/9;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr09,subStageStr];
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
        }else if(i==4){
            btn.tag=12;
            remittancesFee=allPriceFloat*stageRate12;
            NSString *remittancesFeeStr=[NSString stringWithFormat:@"%.2f",remittancesFee];
            NSRange rang=[remittancesFeeStr rangeOfString:@"."];
            if (rang.location != NSNotFound) {
                remittancesFeeStr=[remittancesFeeStr substringToIndex:rang.location];
            }
            remittancesFee12=remittancesFeeStr;
            
            NSString *formatStr=[NSString stringWithFormat:@"（%@元手续费）",remittancesFeeStr];
            //
            stagePriceFloat=allPriceFloat/12;
            NSString *subStageStr=[NSString stringWithFormat:@"%f",stagePriceFloat];
            NSRange range=[subStageStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                subStageStr=[subStageStr substringToIndex:range.location];
            }
            stagePriceStr=[NSString stringWithFormat:@"%@%@/期",rateStr12,subStageStr];
            btnStr=[NSString stringWithFormat:@"%@%@",stagePriceStr,formatStr];
            
            //添加黑色分割条
            UIView *secView=[[UIView alloc] initWithFrame:CGRectMake(0, btn.origin.y+btn.height+12, SCREENSIZE.width, 12)];
            secView.backgroundColor=[UIColor groupTableViewBackgroundColor];
            [headerView addSubview:secView];
            
            if ([jicun_id isEqualToString:@""]||
                [jicun_id isEqualToString:@"0"]||
                !isHadBsnName) {
                //重新设置frame
                CGRect frame=headerView.frame;
                frame.size.height=secView.origin.y+secView.height;
                headerView.frame=frame;
            }else{
                //
                UIView *dpView=[self setUpDianPuView:secView dicData:dic];
                [headerView addSubview:dpView];
                
                //重新设置frame
                CGRect frame=headerView.frame;
                frame.size.height=dpView.origin.y+dpView.height;
                headerView.frame=frame;
            }
        }
        [btn setTitle:btnStr forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickFenqiBtn:) forControlEvents:UIControlEventTouchDown];
    }
    
    return headerView;
}

#pragma mark - 店铺View
-(UIView *)setUpDianPuView:(UIView *)upperView dicData:(NSDictionary *)dic{
    //店铺View
    UIView *dpView=[[UIView alloc] initWithFrame:CGRectMake(0, upperView.origin.y+upperView.height, upperView.width, 100)];
    dpView.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickDp)];
    [dpView addGestureRecognizer:tap];
    
    //来源
    UILabel *lyLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 8, dpView.width/2, 24)];
    lyLabel.text=@"商品来源";
    lyLabel.font=[UIFont systemFontOfSize:14];
    lyLabel.textAlignment=NSTextAlignmentCenter;
    lyLabel.textColor=[UIColor colorWithHexString:@"#919191"];
    [dpView addSubview:lyLabel];
    
    //店铺名
    UILabel *dpName=[[UILabel alloc] initWithFrame:CGRectMake(0, lyLabel.origin.y+lyLabel.height, dpView.width/2, 24)];
    dpName.adjustsFontSizeToFitWidth=YES;
    dpName.text=dic[@"business"][@"business_name"];
    dpName.font=[UIFont systemFontOfSize:16];
    dpName.textAlignment=NSTextAlignmentCenter;
    dpName.textColor=[UIColor colorWithHexString:@"#464646"];
    [dpView addSubview:dpName];
    
    UIView *ling=[[UIView alloc] initWithFrame:CGRectMake(12, dpName.origin.y+dpName.height+6, dpName.width-24, 1)];
    ling.backgroundColor=[UIColor colorWithHexString:@"#dedede"];
    [dpView addSubview:ling];
    
    //wx btn
    
    CGFloat btnWidth=(lyLabel.width-16)/3;
    
    UIButton *wxbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [wxbtn setImage:[UIImage imageNamed:@"wx"] forState:UIControlStateNormal];
    wxbtn.frame=CGRectMake(8, ling.origin.y+ling.height, btnWidth, btnWidth);
    [wxbtn addTarget:self action:@selector(clicTest) forControlEvents:UIControlEventTouchUpInside];
    [dpView addSubview:wxbtn];
    
    UIButton *wbbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [wbbtn setImage:[UIImage imageNamed:@"wb"] forState:UIControlStateNormal];
    wbbtn.frame=CGRectMake(wxbtn.origin.x+wxbtn.width, ling.origin.y+ling.height, btnWidth, btnWidth);
    [wbbtn addTarget:self action:@selector(clicTest) forControlEvents:UIControlEventTouchUpInside];
    [dpView addSubview:wbbtn];
    
    UIButton *qqbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [qqbtn setImage:[UIImage imageNamed:@"qq"] forState:UIControlStateNormal];
    qqbtn.frame=CGRectMake(wbbtn.origin.x+wbbtn.width, ling.origin.y+ling.height, btnWidth, btnWidth);
    [qqbtn addTarget:self action:@selector(clicTest) forControlEvents:UIControlEventTouchUpInside];
    [dpView addSubview:qqbtn];
    
    //负责人
    UILabel *label01_l=[[UILabel alloc] initWithFrame:CGRectMake(dpView.width/2, 15, 60, lyLabel.height)];
    label01_l.font=lyLabel.font;
    label01_l.textColor=lyLabel.textColor;
    label01_l.text=@"负 责 人:";
    [dpView addSubview:label01_l];
    
    UILabel *label01_r=[[UILabel alloc] initWithFrame:CGRectMake(label01_l.origin.x+label01_l.width, label01_l.origin.y, dpView.width-(label01_l.origin.x+label01_l.width+8), label01_l.height)];
    label01_r.font=lyLabel.font;
    label01_r.textColor=lyLabel.textColor;
    label01_r.text=dic[@"business"][@"person_name"];
    [dpView addSubview:label01_r];
    
    //电话
    UILabel *phone_l=[[UILabel alloc] initWithFrame:CGRectMake(label01_l.origin.x, label01_l.origin.y+label01_l.height, label01_l.width, label01_l.height)];
    phone_l.font=lyLabel.font;
    phone_l.textColor=lyLabel.textColor;
    phone_l.text=@"电      话:";
    [dpView addSubview:phone_l];
    
    UILabel *phone_r=[[UILabel alloc] initWithFrame:CGRectMake(label01_r.origin.x, phone_l.origin.y, label01_r.width, label01_r.height)];
    phone_r.font=lyLabel.font;
    phone_r.textColor=lyLabel.textColor;
    phone_r.text=dic[@"business"][@"business_phone"];
    [dpView addSubview:phone_r];
    
    //地址
    UILabel *address_l=[[UILabel alloc] initWithFrame:CGRectMake(label01_l.origin.x, phone_l.origin.y+phone_l.height, label01_l.width, label01_l.height)];
    address_l.font=lyLabel.font;
    address_l.textColor=lyLabel.textColor;
    address_l.text=@"地      址:";
    [dpView addSubview:address_l];
    
    UILabel *address_r=[UILabel new];
    address_r.font=lyLabel.font;
    address_r.textColor=lyLabel.textColor;
    [dpView addSubview:address_r];
    NSString *adrStr=dic[@"business"][@"business_address"];
    address_r.text=adrStr;
    CGFloat address_height=0;
    CGSize addressSize=[AppUtils getStringSize:adrStr withFont:14];
    if (addressSize.width>label01_r.width) {
        address_height=label01_r.height*2-6;
        address_r.numberOfLines=0;
    }else{
        address_height=label01_r.height;
        address_r.numberOfLines=1;
    }
    address_r.frame=CGRectMake(label01_r.origin.x, address_l.origin.y, phone_r.width, address_height);
    
    //section
    UIView *secView=[[UIView alloc] initWithFrame:CGRectMake(0, wbbtn.origin.y+wbbtn.height+12, dpView.width, 12)];
    secView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [dpView addSubview:secView];
    
    //
    CGRect frame=dpView.frame;
    frame.size.height=wbbtn.origin.y+wbbtn.height;
    dpView.frame=frame;
    
    return dpView;
}

#pragma mark - 点击店铺
-(void)clickDp{
    ShopController *shopCtr=[ShopController new];
    shopCtr.dp_id=jicun_id;
    [self.navigationController pushViewController:shopCtr animated:YES];
}

#pragma mark - 显示微信、QQ名称
-(void)clicTest{
    myAlertView = [[CustomIOSAlertView alloc] init];
    
    [myAlertView setContainerView:[self createDemoView]];
    [myAlertView setButtonTitles:nil];
    
    [myAlertView setUseMotionEffects:true];
    if (!([ewmStr isEqualToString:@""]&&[ewmId isEqualToString:@""]&&[wbStr isEqualToString:@""]&&[qqStr isEqualToString:@""])) {
        [myAlertView show];
    }
}

- (UIView *)createDemoView{
    demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    demoView.backgroundColor=[UIColor whiteColor];
    demoView.layer.cornerRadius=3;
    demoView.layer.borderWidth=1.0f;
    demoView.layer.borderColor=[UIColor colorWithHexString:@"#ec6b00"].CGColor;
    
    UIButton *closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.layer.cornerRadius=demoView.layer.cornerRadius;
    closeBtn.frame=CGRectMake(demoView.frame.size.width-30, 0, 30, 30);
    [closeBtn setImage:[UIImage imageNamed:@"iconfont-guanbi"] forState:UIControlStateNormal];;
    [closeBtn addTarget:self action:@selector(clickCloseBtn) forControlEvents:UIControlEventTouchUpInside];
    [demoView addSubview:closeBtn];
    
    //二维码图片
    CGFloat ewmWidth=demoView.width/2-28;
    UIImageView *ewmImgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ewmWidth, ewmWidth)];
    ewmImgv.userInteractionEnabled=YES;
    ewmImgv.center=CGPointMake(demoView.center.x, closeBtn.origin.y+closeBtn.height+ewmWidth/2);
    [ewmImgv setImageWithURL:[NSURL URLWithString:ewmStr]];
    saveImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ewmStr]]];
    [demoView addSubview:ewmImgv];
    
    //长按
    UILongPressGestureRecognizer *longG=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longSaveEwm:)];
    longG.minimumPressDuration=0.7f;
    [ewmImgv addGestureRecognizer:longG];
    
    //双击
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
    action:@selector(saveEwm)];
    tap.numberOfTapsRequired = 2;
    [ewmImgv addGestureRecognizer:tap];
    
    //微信
    CopyLabel *wxLabel=[[CopyLabel alloc] initWithFrame:CGRectMake(8, ewmImgv.origin.y+ewmImgv.height+14, demoView.width-16, 19)];
    wxLabel.showString=@"复制微信号";
    wxLabel.userInteractionEnabled=YES;
    wxLabel.font=[UIFont systemFontOfSize:15];
    wxLabel.text=[NSString stringWithFormat:@"微信ID：%@",ewmId];
    wxLabel.textAlignment=NSTextAlignmentCenter;
    [demoView addSubview:wxLabel];
    
    //虚线
    CGFloat fLineFloat=wxLabel.origin.y+wxLabel.height+16;
    [self drawMyLine:demoView firstPoint:CGPointMake(8, fLineFloat) endPoint:CGPointMake((demoView.origin.x+demoView.width)-8, fLineFloat)];
    
    //微博
    CopyLabel *wbLabel=[[CopyLabel alloc] initWithFrame:CGRectMake(8, fLineFloat+16, demoView.width-16, wxLabel.height)];
    wbLabel.showString=@"复制微博昵称";
    wbLabel.font=wxLabel.font;
    wbLabel.text=[NSString stringWithFormat:@"微博昵称：%@",wbStr];
    wbLabel.textAlignment=NSTextAlignmentCenter;
    [demoView addSubview:wbLabel];
    
    //虚线
    CGFloat sLineFloat=wbLabel.origin.y+wbLabel.height+16;
    [self drawMyLine:demoView firstPoint:CGPointMake(8, sLineFloat) endPoint:CGPointMake((demoView.origin.x+demoView.width)-8, sLineFloat)];
    
    //qq
    CopyLabel *qqLabel=[[CopyLabel alloc] initWithFrame:CGRectMake(8, sLineFloat+16, demoView.width-16, wbLabel.height)];
    qqLabel.showString=@"复制QQ号";
    qqLabel.font=wxLabel.font;
    qqLabel.text=[NSString stringWithFormat:@"QQ号码：%@",qqStr];
    qqLabel.textAlignment=NSTextAlignmentCenter;
    [demoView addSubview:qqLabel];
    
    //
    CGRect frame=demoView.frame;
    frame.size.height=qqLabel.origin.y+qqLabel.height+16;
    demoView.frame=frame;
    
    return demoView;
}

-(void)longSaveEwm:(UILongPressGestureRecognizer *)longP{
    if (longP.state==UIGestureRecognizerStateBegan) {
        [self savePhotoBtn];
    }
}

-(void)saveEwm{
    [self savePhotoBtn];
}

-(void)savePhotoBtn{
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMSheetViewConfig *sheetConfig = [MMSheetViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                UIImageWriteToSavedPhotosAlbum(saveImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"保存到相册", MMItemTypeNormal, block)];
    
    [[[MMSheetView alloc] initWithTitle:nil
                                  items:items] showWithBlock:completeBlock];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
   contextInfo:(void *)contextInfo{
    if (error == nil) {
        [AppUtils showSuccessMessage:@"已保存到手机相册" inView:demoView];
    }else{
        [AppUtils showSuccessMessage:@"保存失败" inView:demoView];
    }
}


#pragma mark - 画虚线
-(void)drawMyLine:(UIView *)view firstPoint:(CGPoint)fPoint endPoint:(CGPoint)ePoint{
    //虚线
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    // 设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:[[UIColor colorWithHexString:@"#959595"] CGColor]];
    
    // 3.0f设置虚线的宽度
    [shapeLayer setLineWidth:1.0f];
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    // 3=线的宽度 1=每条线的间距
    [shapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:4],
      [NSNumber numberWithInt:1],nil]];
    
    // Setup the path
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, fPoint.x, fPoint.y);
    CGPathAddLineToPoint(path, NULL, ePoint.x,ePoint.y);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [[view layer] addSublayer:shapeLayer];
}

-(void)clickCloseBtn{
    [myAlertView close];
}

#pragma mark - setUp Table
-(void)setUpUIWithDictionary:(NSDictionary *)dic{
    
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-113) style:UITableViewStylePlain];
    [self.view addSubview:myTable];
    [myTable registerNib:[UINib nibWithNibName:@"ProductDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [myTable registerNib:[UINib nibWithNibName:@"ProductDetailPicCell" bundle:nil] forCellReuseIdentifier:@"piccell"];
    [myTable registerNib:[UINib nibWithNibName:@"ProdectParamDetailCell" bundle:nil] forCellReuseIdentifier:@"paramcell"];
    [myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.tableHeaderView=[self setUpHeaderViewWithDic:dic];
    
    [self filterview];
    
    [self setUpBottomViewWithDic:dic];
}

-(void)setUpBottomViewWithDic:(NSDictionary *)dic{
    
    UIView *btmView=[[UIView alloc]init];
    btmView.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [self.view addSubview:btmView];
    btmView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray* btmView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[btmView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btmView)];
    [NSLayoutConstraint activateConstraints:btmView_h];
    
    NSArray* btmView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[btmView(49)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btmView)];
    [NSLayoutConstraint activateConstraints:btmView_w];
    
    //立即购买按钮
    UIButton *buyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.layer.cornerRadius=1;
    [buyBtn setTitle:@"立即购买" forState:UIControlStateNormal];
    buyBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btmView addSubview:buyBtn];
    [buyBtn addTarget:self action:@selector(clickBuyBtn) forControlEvents:UIControlEventTouchUpInside];
    buyBtn.translatesAutoresizingMaskIntoConstraints=NO;
    
    //加入购物车按钮
    UIButton *joinShoppingCartBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    joinShoppingCartBtn.layer.borderWidth=2;
    joinShoppingCartBtn.backgroundColor=[UIColor whiteColor];
    [joinShoppingCartBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    joinShoppingCartBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [btmView addSubview:joinShoppingCartBtn];
    [joinShoppingCartBtn addTarget:self action:@selector(clickJoinShoppingCartBtn) forControlEvents:UIControlEventTouchUpInside];
    joinShoppingCartBtn.translatesAutoresizingMaskIntoConstraints=NO;
    if ([dic[@"goods_number"] isEqualToString:@"0"]) {
        buyBtn.enabled=NO;
        buyBtn.backgroundColor=[UIColor lightGrayColor];
        joinShoppingCartBtn.enabled=NO;
        joinShoppingCartBtn.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [joinShoppingCartBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }else{
        buyBtn.enabled=YES;
        buyBtn.backgroundColor=[UIColor colorWithHexString:@"#e60000"];
        joinShoppingCartBtn.enabled=YES;
        joinShoppingCartBtn.layer.borderColor=[UIColor colorWithHexString:@"#e60000"].CGColor;
        [joinShoppingCartBtn setTitleColor:[UIColor colorWithHexString:@"#e60000"] forState:UIControlStateNormal];
    }
    
    //收藏按钮
    UIButton *collectionBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    collectionBtn.backgroundColor=[UIColor clearColor];
    collectionBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    [collectionBtn setImage:[UIImage imageNamed:@"colletion"] forState:UIControlStateNormal];
    collectionBtn.imageEdgeInsets=UIEdgeInsetsMake(4, 0, 4, -2);
    [collectionBtn setTitle:@"收藏" forState:UIControlStateNormal];
    collectionBtn.titleEdgeInsets=UIEdgeInsetsMake(0, -16, 0, 0);
    collectionBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [collectionBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btmView addSubview:collectionBtn];
    [collectionBtn addTarget:self action:@selector(clickCollectionBtn) forControlEvents:UIControlEventTouchUpInside];
    collectionBtn.translatesAutoresizingMaskIntoConstraints=NO;
    
    //布局
    NSArray* buyBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[buyBtn(100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyBtn)];
    [NSLayoutConstraint activateConstraints:buyBtn_h];
    
    NSArray* buyBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[buyBtn]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyBtn)];
    [NSLayoutConstraint activateConstraints:buyBtn_w];
    
    NSArray* joinShoppingCartBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[buyBtn]-8-[joinShoppingCartBtn(100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(buyBtn,joinShoppingCartBtn)];
    [NSLayoutConstraint activateConstraints:joinShoppingCartBtn_h];
    
    NSArray* joinShoppingCartBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[joinShoppingCartBtn]-6-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(joinShoppingCartBtn)];
    [NSLayoutConstraint activateConstraints:joinShoppingCartBtn_w];
    
    NSArray* collectionBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[joinShoppingCartBtn]-12-[collectionBtn]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(joinShoppingCartBtn,collectionBtn)];
    [NSLayoutConstraint activateConstraints:collectionBtn_h];
    
    NSArray* collectionBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[collectionBtn]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(collectionBtn)];
    [NSLayoutConstraint activateConstraints:collectionBtn_w];
}

#pragma mark - 默认选中第一个
-(void)defaultSelected{
    seletedFenqiBtn.backgroundColor=[UIColor whiteColor];
    [seletedFenqiBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    seletedFenqiBtn.enabled=YES;
    
    //当前点击的button
    UIButton *sebtn=btn;
    
    sebtn.backgroundColor=[UIColor colorWithHexString:@"#cf1316"];
    [sebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sebtn.enabled=NO;
    
    //用另一个button代替当前点击的button
    seletedFenqiBtn=sebtn;
    //
}

#pragma mark - 点击分期数
-(void)clickFenqiBtn:(id)sender{
    
    seletedFenqiBtn.backgroundColor=[UIColor whiteColor];
    [seletedFenqiBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    seletedFenqiBtn.enabled=YES;
    
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    
    sebtn.backgroundColor=[UIColor colorWithHexString:@"#cf1316"];
    [sebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sebtn.enabled=NO;
    
    //用另一个button代替当前点击的button
    seletedFenqiBtn=sebtn;
}

#pragma mark - button event
//点击后跳转到购物车
//1、发送通知以改变tabbar 的 seleteIndex
//2、popToRootViewController
//==============
-(void)clickBuyBtn{
    [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionGoodDetailGoToShoppingCart object:nil];
    SingFMDB *db=[SingFMDB shareFMDB];
    BOOL isExit=[db searchData:self.goodId];
    if (!isExit) {
        ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
        model.goodsId=self.goodId;
        model.goodsImgStr=self.goodImgStr;
        model.goodsTitle=self.goodTitle;
        model.goodsPrice=self.goodPrice;
        [db insertMyGood:model];
    }else{
        NSLog(@"添加失败，购物车已有");
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

//加入购物车
-(void)clickJoinShoppingCartBtn{
    SingFMDB *db=[SingFMDB shareFMDB];
    BOOL isExit=[db searchData:self.goodId];
    if (!isExit) {
        ShoppingCartModel *model=[[ShoppingCartModel alloc]init];
        model.goodsId=self.goodId;
        model.goodsImgStr=self.goodImgStr;
        model.goodsTitle=self.goodTitle;
        model.goodsPrice=self.goodPrice;
        BOOL isOk=[db insertMyGood:model];
        if (isOk) {
            [[MMPopupWindow sharedWindow] cacheWindow];
            [MMPopupWindow sharedWindow].touchWildToHide = YES;
            
            MMAlertViewConfig *sheetConfig = [MMAlertViewConfig globalConfig];
            sheetConfig.buttonFontSize=15;
            sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
            
            MMPopupItemHandler block = ^(NSInteger index){
                switch (index) {
                    case 0:
                        [[NSNotificationCenter defaultCenter] postNotificationName:NotifitionGoodDetailGoToShoppingCart object:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:UpDateShoppingCart object:nil];
                        [self.navigationController popToRootViewControllerAnimated:NO];
                        break;
                    default:
                        break;
                }
            };
            
            MMPopupBlock completeBlock = ^(MMPopupView *popupView){
                
            };
            NSArray *items =
            @[MMItemMake(@"前往购物车", MMItemTypeNormal, block),
              MMItemMake(@"继续购物", MMItemTypeNormal, block)];
            
            [[[MMAlertView alloc] initWithTitle:@"提示" detail:@"商品成功添加到购物车！" items:items] showWithBlock:completeBlock];
        }
    }else{
        [AppUtils showSuccessMessage:@"购物车已有该商品！" inView:self.view];
    }
}

//添加收藏
-(void)clickCollectionBtn{
    LoginController *login=[[LoginController alloc]init];
    UINavigationController *logNav=[[UINavigationController alloc]initWithRootViewController:login];
    if ([AppUtils loginState]) {
        [self requestAddCollectionForGoodId:self.goodId];
    }else{
        [self presentViewController:logNav animated:YES completion:nil];
    }
}

#pragma mark - 商品图片、商品详情切换
-(void)clickGoodPicBtn:(id)sender{
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    isClickGoodDetail=NO;
    [sebtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    
    [sectionBtn02 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [myTable reloadData];
}

-(void)clickGoodParamBtn:(id)sender{
    //当前点击的button
    UIButton *sebtn=(UIButton *)sender;
    isClickGoodDetail=YES;
    [sebtn setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    
    [sectionBtn01 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [myTable reloadData];
}

#pragma mark - TableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isClickGoodDetail) {
        return propertyArr.count;
    }else{
        
        return goodPicArr.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isClickGoodDetail) {
        ProdectParamDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"paramcell"];
        if (cell==nil) {
            cell=[[ProdectParamDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"paramcell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (indexPath.row<propertyArr.count) {
            GoodParamModel *model=propertyArr[indexPath.row];
            [cell reflushDataForModel:model];
        }
        return cell;
    }else{
        ProductDetailPicCell *cell;
        cell=(ProductDetailPicCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (!cell) {
            cell=[[ProductDetailPicCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"piccell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if (indexPath.row<goodPicArr.count) {
            NSDictionary *dic=goodPicArr[indexPath.row];
            [cell setUpUIAndInitDataForDictuinary:dic];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isClickGoodDetail) {
        return 36;
    }else{
        ProductDetailPicCell *cell=[tableView dequeueReusableCellWithIdentifier:@"piccell"];
        if (!cell) {
            cell=[[ProductDetailPicCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"piccell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        NSDictionary *dic=goodPicArr[indexPath.row];
        [cell setUpUIAndInitDataForDictuinary:dic];
        return cell.frame.size.height;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 49;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return filterView;
}

#pragma mark - filterview
-(UIView *)filterview{
    filterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 49)];
    filterView.backgroundColor=[UIColor whiteColor];
    
    //line01
    UIView *line01=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 1.0f)];
    line01.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [filterView addSubview:line01];
    
    //line02
    UIView *line02=[[UIView alloc]initWithFrame:CGRectMake(SCREENSIZE.width/2, 0, 1.0f, 49)];
    line02.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [filterView addSubview:line02];
    
    //line03
    UIView *line03=[[UIView alloc]initWithFrame:CGRectMake(0, 48.5f, SCREENSIZE.width, 1.0f)];
    line03.backgroundColor=[UIColor colorWithHexString:@"#e5e5e5"];
    [filterView addSubview:line03];
    
    sectionBtn01=[UIButton buttonWithType:UIButtonTypeCustom];
    sectionBtn01.frame=CGRectMake(0, 1.0f, SCREENSIZE.width/2, filterView.height-1);
    sectionBtn01.titleLabel.font=[UIFont systemFontOfSize:15];
    [sectionBtn01 setTitle:@"商品图片" forState:UIControlStateNormal];
    [sectionBtn01 setTitleColor:[UIColor colorWithHexString:@"#ec6b00"] forState:UIControlStateNormal];
    sectionBtn01.tag=100;
    [sectionBtn01 addTarget:self action:@selector(clickGoodPicBtn:) forControlEvents:UIControlEventTouchUpInside];
    [filterView addSubview:sectionBtn01];
    
    sectionBtn02=[UIButton buttonWithType:UIButtonTypeCustom];
    sectionBtn02.frame=CGRectMake(SCREENSIZE.width/2, 1.0f, SCREENSIZE.width/2, filterView.height-1);
    sectionBtn02.titleLabel.font=[UIFont systemFontOfSize:15];
    [sectionBtn02 setTitle:@"商品详情" forState:UIControlStateNormal];
    [sectionBtn02 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    sectionBtn02.tag=101;
    [sectionBtn02 addTarget:self action:@selector(clickGoodParamBtn:) forControlEvents:UIControlEventTouchUpInside];
    [filterView addSubview:sectionBtn02];
    return filterView;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request
//商品详情请求
-(void)requestHttpsForGoodDetail:(NSString *)goodId{
    propertyArr=[NSMutableArray array];
    [AppUtils showProgressInView:self.view];
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"id":goodId
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"goods.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            NSDictionary *resDic=responseObject[@"retData"];
            
            //获取商品描述
            goodDescribe=resDic[@"goods"][@"keywords"];
            
            ewmStr=resDic[@"business"][@"business_wechat"];
            ewmId=resDic[@"business"][@"business_wechat_id"];
            wbStr=resDic[@"business"][@"business_sina"];
            qqStr=resDic[@"business"][@"business_qq"];
            
            /*
             business =         {
                "business_wechat" = "";
                "business_wechat_id" = "";
                "business_sina" = "";
                "business_qq" = @"";
             };
             */
            
            //获取商品id、图片、title、价格
            self.goodId=resDic[@"goods"][@"goods_id"];
            self.goodImgStr=resDic[@"goods"][@"goods_img"];
            self.goodTitle=resDic[@"goods"][@"goods_name"];
            self.goodPrice=resDic[@"goods"][@"shop_price"];
            
            //商品详情参数
            NSArray *paramArr=resDic[@"properties"];
            for (NSDictionary *dic in paramArr) {
                GoodParamModel *model=[[GoodParamModel alloc]init];
                [model jsonDataForDictionary:dic];
                [propertyArr addObject:model];
            }
            
            if ([resDic[@"goods"][@"business"] objectForKey:@"business_name"]) {
                isHadBsnName=YES;
            }else{
                isHadBsnName=NO;
            }
            //"<img src='data/images/1105_thumb_G_1608294669680.png'>";
            //商品图片列表
            NSString *imgListStr=resDic[@"goods"][@"goods_desc"];
            NSMutableString *imgMutableStr=[NSMutableString stringWithString:imgListStr];
            [self subSecondParams:imgMutableStr];
            
            headVerDic=[NSDictionary dictionaryWithDictionary:resDic];
            
            NSString *allPriceStr=headVerDic[@"goods"][@"shop_price"];
            allPriceFloat=allPriceStr.floatValue;
            
            [self setUpUIWithDictionary:resDic[@"goods"]];
        }else{
            NSLog(@"%@",responseObject[@"retData"][@"msg"]);
        }
        [AppUtils dismissHUDInView:self.view];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissHUDInView:self.view];
    }];
}

#pragma mark - Request(获取分期利率)
-(void)requestHttpsForStageRate{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"act":@"iso_fenqi_fee"
                         };
    [[HttpRequest defaultClient] requestWithPath:[ImageHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSString *rateStr01=responseObject[@"retData"][@"fenqi"][@"1"];
            NSString *rateStr03=responseObject[@"retData"][@"fenqi"][@"3"];
            NSString *rateStr06=responseObject[@"retData"][@"fenqi"][@"6"];
            NSString *rateStr09=responseObject[@"retData"][@"fenqi"][@"9"];
            NSString *rateStr12=responseObject[@"retData"][@"fenqi"][@"12"];
            NSString *shuiFeeStr=responseObject[@"retData"][@"sw"];
            
            stageRate01=rateStr01.floatValue;
            stageRate03=rateStr03.floatValue;
            stageRate06=rateStr06.floatValue;
            stageRate09=rateStr09.floatValue;
            stageRate12=rateStr12.floatValue;
            shuiFee=shuiFeeStr.floatValue;
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//字符串处理
-(void)subSecondParams:(NSMutableString *)str{
    goodPicArr=[NSMutableArray array];
    
    NSArray *picarry=[str componentsSeparatedByString:@" /></p>"];
    for (NSString *str in picarry) {
        NSRange substrRang = [str rangeOfString:@"src="];
        if (substrRang.location!=NSNotFound) {
            NSString *newStr=[str substringFromIndex:substrRang.location+substrRang.length+1];
            NSMutableString *mstr=[NSMutableString stringWithString:newStr];
            NSRange jpgRang=[mstr rangeOfString:@"jpg"];
            //字典
            NSMutableDictionary *whDic=[NSMutableDictionary dictionary];
            if (jpgRang.location != NSNotFound) {
                NSString *lastStr=[mstr substringToIndex:jpgRang.location+jpgRang.length];
                [whDic setObject:lastStr forKey:@"imgStr"];//得到图片URL路径
                NSRange _rang=[lastStr rangeOfString:@".jpg"];
                if (_rang.location!=NSNotFound) {
                    NSString *firStr=[lastStr substringToIndex:_rang.location];
                    NSArray *_rangeArry=[firStr componentsSeparatedByString:@"_"];
                    if (_rangeArry.count<2) {
                        [whDic setObject:[NSString stringWithFormat:@"%f",SCREENSIZE.width] forKey:@"imgW"];
                        [whDic setObject:@"1" forKey:@"imgH"];
                    }else{
                        for(int i=0; i<_rangeArry.count; i++){
                            if (i==_rangeArry.count-2) {
                                [whDic setObject:_rangeArry[i] forKey:@"imgW"];
                            }
                        }
                        [whDic setObject:_rangeArry.lastObject forKey:@"imgH"];
                    }
                }
                [goodPicArr addObject:whDic];
//                NSString *wStr=[whDic objectForKey:@"imgW"];
//                if ([AppUtils isStringIncludeDigitChar:wStr]) {
//                    [goodPicArr addObject:whDic];
//                }
            }
        }
    }
}
//添加收藏请求
-(void)requestAddCollectionForGoodId:(NSString *)goodId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:goodId forKey:@"id"];
    [dict setObject:@"collect" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestAddCollectionForGoodId:self.goodId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [AppUtils showSuccessMessage:responseObject[@"retData"][@"message"] inView:self.view];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
