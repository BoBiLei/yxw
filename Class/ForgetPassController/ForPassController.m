//
//  ForPassController.m
//  CatShopping
//
//  Created by mac on 15/9/16.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "ForPassController.h"

@interface ForPassController ()

@end

@implementation ForPassController{
    int timerCount;
    NSString *receiverify;
    
    UITextField *cartText;
    UITextField *passText;
    UIButton *getVerifyBtn;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"忘记密码"];
    timerCount = RESETTIME;
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 180)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    //卡号View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    UILabel *cartLabel=[[UILabel alloc]init];
    cartLabel.textAlignment=NSTextAlignmentCenter;
    cartLabel.text=@"手机号：";
    cartLabel.font=[UIFont systemFontOfSize:15];
    cartLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartLabel];
    cartLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_h];
    
    NSArray* cartLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_w];
    
    //获取验证码按钮
    getVerifyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
    getVerifyBtn.enabled=NO;
    getVerifyBtn.layer.cornerRadius=2;
    [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getVerifyBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    [cartView addSubview:getVerifyBtn];
    getVerifyBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* getVerifyBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[getVerifyBtn(75)]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_h];
    
    NSArray* getVerifyBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[getVerifyBtn]-5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:getVerifyBtn_w];
    [getVerifyBtn addTarget:self action:@selector(clickVerifyBtn) forControlEvents:UIControlEventTouchUpInside];
    
    //
    cartText=[[UITextField alloc]init];
    cartText.clearButtonMode=UITextFieldViewModeWhileEditing;
    cartText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [cartText addTarget:self action:@selector(phoneTextDidChange) forControlEvents:UIControlEventEditingChanged];
    cartText.placeholder=@"请输入手机号码";
    cartText.font=[UIFont systemFontOfSize:15];
    cartText.textColor=[UIColor colorWithHexString:@"#282828"];
    [cartView addSubview:cartText];
    cartText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cartLabel]-4-[cartText]-2-[getVerifyBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel,cartText,getVerifyBtn)];
    [NSLayoutConstraint activateConstraints:cartText_h];
    
    NSArray* cartText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartText)];
    [NSLayoutConstraint activateConstraints:cartText_w];
    
    //密码View
    UIView *passView=[[UIView alloc]init];
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[passView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[[UILabel alloc]init];
    passLabel.textAlignment=NSTextAlignmentCenter;
    passLabel.text=@"验证码：";
    passLabel.font=[UIFont systemFontOfSize:15];
    passLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    //
    passText=[[UITextField alloc]init];
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [passText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.placeholder=@"请输入验证码";
    passText.font=[UIFont systemFontOfSize:15];
    passText.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-4-[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"提交" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(40)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickBtn) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - text值改变时
-(void)phoneTextDidChange{
    if ([AppUtils checkPhoneNumber:cartText.text]&&
        ![cartText.text isEqualToString:@""]) {
        getVerifyBtn.enabled=YES;
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    }else{
        getVerifyBtn.enabled=NO;
        getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
    }
}

-(void)textDidChange{
    if ([AppUtils checkPhoneNumber:cartText.text]&&
        ![cartText.text isEqualToString:@""]&&
        ![passText.text isEqualToString:@""]
        ) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

#pragma mark - 提交按钮
/*
 *1、判断验证码与输入是否正确
 *2、查询该号码是否被注册过
 *3、没被注册则提交（否则提示错误）
 */
-(void)clickBtn{
    [AppUtils closeKeyboard];
    
    NSString *verifyStr=[NSString stringWithFormat:@"%@",receiverify];
    if (![verifyStr isEqualToString:passText.text]) {
        [AppUtils showSuccessMessage:@"验证码有误" inView:self.view];
    }else{
        [self checkPhoneIsRegist:cartText.text];
    }
}

#pragma mark - request查询手机是否已注册
-(BOOL)checkPhoneIsRegist:(NSString *)phone{
    __block BOOL res = NO;
    NSDictionary *dict=@{
                         @"act":@"check_mobile_phone",
                         @"is_phone":@"1",
                         @"mobile_phone":phone
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //=========================
        //error表示存在，succe表示没有
        //=========================
        NSString *str=responseObject[@"retMsg"];
        if ([str isEqualToString:@"error"]) {
            res=YES;
            [self requestVerifyAndPhoneForPhone:cartText.text verify:passText.text];
        }else{
            [AppUtils showSuccessMessage:@"该号码未注册" inView:self.view];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
    return res;
}

#pragma mark - request提交手机号、验证码
-(void)requestVerifyAndPhoneForPhone:(NSString *)phone verify:(NSString *)verify{
    //时间戳
    NSString *time_stamp;
    time_t now;
    time(&now);
    time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSDictionary *dict=@{
                         @"act":@"email_reset_psw2",
                         @"is_phone":@"1",
                         @"is_nosign":@"is_nosign",
                         @"phone":phone,
                         @"smscode":verify,
                         @"time":time_stamp
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
             UpDateNewPassController *newPass=[[UpDateNewPassController alloc]init];
            NSString *phoneStr=responseObject[@"retData"][@"phone"];
            newPass.receivePhone=phoneStr;
             [self.navigationController pushViewController:newPass animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 获取验证码按钮
-(void)clickVerifyBtn{
    getVerifyBtn.backgroundColor=[UIColor lightGrayColor];
    getVerifyBtn.enabled=NO;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTimer:) userInfo:nil repeats:YES];
    [self requestVerifyHttpsForPhone:cartText.text];
}

-(void)setTimer:(NSTimer *)timer{
    [getVerifyBtn setTitle:[NSString stringWithFormat:@"重发(%dS)",timerCount] forState:UIControlStateNormal];
    if (timerCount<0){
        [timer invalidate];
        [getVerifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        getVerifyBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        getVerifyBtn.enabled=YES;
        timerCount = RESETTIME;
        return;
    }
    timerCount--;
}

#pragma mark - request 获取验证码请求
-(void)requestVerifyHttpsForPhone:(NSString *)phone{
    NSDictionary *dict=@{
                         @"act":@"send_sms_code_find_pw",
                         @"is_phone":@"1",
                         @"phone":phone
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"ajax_data.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            receiverify=responseObject[@"retData"][@"rcode"];
        }
        NSLog(@"%@",responseObject[@"retData"][@"rcode"]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end




#pragma mark
#pragma mark - 设置新密码
@interface UpDateNewPassController ()

@end

@implementation UpDateNewPassController{
    UITextField *cartText;
    UITextField *passText;
    UIButton *button;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"设置新密码"];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self setUpUI];
}

#pragma mark - init UI
-(void)setUpUI{
    UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, SCREENSIZE.width-16, 180)];
    topView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topView];
    
    //
    //卡号View
    UIView *cartView=[[UIView alloc]init];
    cartView.layer.borderWidth=0.5f;
    cartView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:cartView];
    cartView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_h];
    
    NSArray* cartView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[cartView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView)];
    [NSLayoutConstraint activateConstraints:cartView_w];
    
    //
    UILabel *cartLabel=[[UILabel alloc]init];
    cartLabel.textAlignment=NSTextAlignmentCenter;
    cartLabel.text=@"手机号：";
    cartLabel.font=[UIFont systemFontOfSize:15];
    cartLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartLabel];
    cartLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[cartLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_h];
    
    NSArray* cartLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel)];
    [NSLayoutConstraint activateConstraints:cartLabel_w];
    
    //
    cartText=[[UITextField alloc]init];
    cartText.enabled=NO;
    cartText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    cartText.text=self.receivePhone;
    cartText.font=[UIFont systemFontOfSize:15];
    cartText.textColor=[UIColor colorWithHexString:@"#888888"];
    [cartView addSubview:cartText];
    cartText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* cartText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cartLabel]-4-[cartText]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartLabel,cartText)];
    [NSLayoutConstraint activateConstraints:cartText_h];
    
    NSArray* cartText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cartText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartText)];
    [NSLayoutConstraint activateConstraints:cartText_w];
    
    //密码View
    UIView *passView=[[UIView alloc]init];
    passView.layer.borderWidth=0.5f;
    passView.layer.borderColor=[UIColor colorWithHexString:@"#e5e5e5"].CGColor;
    [topView addSubview:passView];
    passView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passView_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passView]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView)];
    [NSLayoutConstraint activateConstraints:passView_h];
    
    NSArray* passView_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cartView]-12-[passView(44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cartView,passView)];
    [NSLayoutConstraint activateConstraints:passView_w];
    
    //
    UILabel *passLabel=[[UILabel alloc]init];
    passLabel.textAlignment=NSTextAlignmentCenter;
    passLabel.text=@"新密码：";
    passLabel.font=[UIFont systemFontOfSize:15];
    passLabel.textColor=[UIColor colorWithHexString:@"#888888"];
    [passView addSubview:passLabel];
    passLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passLabel_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[passLabel(60)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_h];
    
    NSArray* passLabel_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel)];
    [NSLayoutConstraint activateConstraints:passLabel_w];
    
    //
    passText=[[UITextField alloc]init];
    passText.secureTextEntry=YES;
    passText.clearButtonMode=UITextFieldViewModeWhileEditing;
    passText.tintColor=[UIColor colorWithHexString:@"ec6b00"];
    [passText addTarget:self action:@selector(textDidChange) forControlEvents:UIControlEventEditingChanged];
    passText.placeholder=@"请输入新密码";
    passText.font=[UIFont systemFontOfSize:15];
    passText.textColor=[UIColor colorWithHexString:@"#282828"];
    [passView addSubview:passText];
    passText.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* passText_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[passLabel]-4-[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passLabel,passText)];
    [NSLayoutConstraint activateConstraints:passText_h];
    
    NSArray* passText_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[passText]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passText)];
    [NSLayoutConstraint activateConstraints:passText_w];
    
    //绑定button
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor=[UIColor lightGrayColor];
    button.enabled=NO;
    button.layer.cornerRadius=2;
    [button setTitle:@"确定设置" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:15];
    [topView addSubview:button];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* button_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[button]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)];
    [NSLayoutConstraint activateConstraints:button_h];
    
    NSArray* button_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[passView]-12-[button(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(passView,button)];
    [NSLayoutConstraint activateConstraints:button_w];
    [button addTarget:self action:@selector(clickBtnUpDatePass) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 点击更新密码
-(void)clickBtnUpDatePass{
    [self requestVerifyAndPhoneForPhone:self.receivePhone newPass:passText.text];
}

#pragma mark - request设置新密码
-(void)requestVerifyAndPhoneForPhone:(NSString *)phone newPass:(NSString *)newPass{
    NSDictionary *dict=@{
                         @"act":@"email_reset_psws2",
                         @"is_phone":@"1",
                         @"is_nosign":@"is_nosign",
                         @"phone":phone,
                         @"psw_new":newPass
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            sleep(1);
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - text值改变时
-(void)textDidChange{
    if (![passText.text isEqualToString:@""]) {
        button.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
        button.enabled=YES;
    }else{
        button.backgroundColor=[UIColor lightGrayColor];
        button.enabled=NO;
    }
}

@end