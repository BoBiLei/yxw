//
//  CityFmdb.h
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
@interface CityFmdb : NSObject
+(CityFmdb *)shareCityFMDB;

/**
 *获取省
 */
-(NSMutableArray *)getProvince;

/**
 *获取市/区
 */
-(NSMutableArray *)getCityWithId:(NSString *)regionId;

@property (strong,nonatomic) FMDatabase *db;
@property (readonly) NSString *databasePath;
@end
