//
//  CtripOverseaCityModel.m
//  YXJR_SH
//
//  Created by mac on 2016/11/18.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "CtripOverseaCityModel.h"

@implementation CtripOverseaCityModel

-(void)getDataForResultSet:(FMResultSet *)resultSet{
    self.countryCode=[resultSet stringForColumn:@"cityID"];
    self.cityCode=[resultSet stringForColumn:@"cityCode"];
    self.cityName=[resultSet stringForColumn:@"cityName"];
    self.cityNameEN=[resultSet stringForColumn:@"cityNameEN"];
    self.cityNamePY=[resultSet stringForColumn:@"cityNamePY"];
    self.cityNameJP=[resultSet stringForColumn:@"cityNameJP"];
    self.firstLetter=[resultSet stringForColumn:@"firstLetterPY"];
}

@end
