//
//  SearchHistoryFmdb.h
//  youxia
//
//  Created by mac on 16/1/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"

@interface SearchHistoryFmdb : NSObject

+(SearchHistoryFmdb *)shareFmdb;

/**
 *添加
 */
-(BOOL)insertSearchHistoryDate:(NSString *)searchText;

/**
 *历史记录
 */
-(NSMutableArray *)getAllHistory;

/**
 *清空
 */
-(void)clearHistory;

@end
