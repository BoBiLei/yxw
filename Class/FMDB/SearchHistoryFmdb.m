//
//  SearchHistoryFmdb.m
//  youxia
//
//  Created by mac on 16/1/21.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "SearchHistoryFmdb.h"

@implementation SearchHistoryFmdb{
    FMDatabaseQueue *dbQueue;
}

static SearchHistoryFmdb *singleDataBase=nil;

+(SearchHistoryFmdb *)shareFmdb{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[SearchHistoryFmdb alloc]init];
        [singleDataBase openDB];
    });
    return singleDataBase;
}

-(void)openDB{
    NSString *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    //创建数据库名称
    path=[path stringByAppendingPathComponent:@"history.sqlite"];
    //DSLog(@"%@",path);
    //连接数据库
    dbQueue=[FMDatabaseQueue databaseQueueWithPath:path];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [dbQueue inDatabase:^(FMDatabase *db) {
            if (![db open]) {
                DSLog(@"不能打开 citydb !");
            }
            [db executeUpdate:@"create table if not exists SearchHistory (history_name text)"];
        }];
    });
}

#pragma mark  添加
-(BOOL)insertSearchHistoryDate:(NSString *)searchText{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [dbQueue inDatabase:^(FMDatabase *db) {
            [db open];
            FMResultSet *result=[db executeQuery:@"select * from SearchHistory where history_name=?",searchText];
            if (!result.next) {
                [db executeUpdate:@"insert into SearchHistory values(?)",searchText];
            }
            [db close];
        }];
    });
    return YES;
}

#pragma mark  清空
-(void)clearHistory{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [dbQueue inDatabase:^(FMDatabase *db) {
            [db open];
            [db executeUpdate:@"delete from SearchHistory"];
            [db close];
        }];
    });
}

#pragma mark  查询
-(NSMutableArray *)getAllHistory{
    __block NSMutableArray *arr=[[NSMutableArray alloc] initWithCapacity:2];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [dbQueue inDatabase:^(FMDatabase *db) {
            [db open];
            FMResultSet *result=[db executeQuery:@"select history_name from SearchHistory"];
            while ([result next]) {
                NSString *searText=[result stringForColumn:@"history_name"];
                if (arr.count==8) {
                    [self deleteAHistory:arr[0]];
                    [arr removeFirstObject];
                }
                [arr addObject:searText];
            }
            //    DSLog(@"\n%@",arr);
            [db close];
        }];
    });
    return arr;
}

-(void)deleteAHistory:(NSString *)keyWork{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [dbQueue inDatabase:^(FMDatabase *db) {
            [db open];
            [db executeQuery:@"delete from SearchHistory where history_name=?",keyWork];
            [db close];
        }];
    });
}

@end
