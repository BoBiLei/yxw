//
//  CountyDB.m
//  YXJR_SH
//
//  Created by mac on 2016/11/16.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "CountyDB.h"

@implementation CountyDB

static CountyDB *singleDataBase=nil;

+(CountyDB *)shareDB{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[CountyDB alloc]init];
    });
    return singleDataBase;
}

-(void)openDBForPath:(NSString *)path{
    NSString* dbPath=[[NSBundle mainBundle] pathForResource:path ofType:@"db"];
    //连接数据库
    self.db=[FMDatabase databaseWithPath:dbPath];
    if (![self.db open]) {
        NSLog(@"不能打开 citydb !");
    }else{
        NSLog(@"打开db !");
    }
}

-(void)closeDB{
    if ([self.db open]) {
        [self.db close];
    }
}

-(NSDictionary *)getChinaCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where CountryCode='CN' ORDER by CityName_FirstWord"];
//    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCountryCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)getCtripChinaCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where CountryCode='CN'"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCtripChinaCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)getCtripOverSeaCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where CountryCode!='CN'"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCtripOverseaCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)searchChinaCtripCityWithKeyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_country_city where cityName_CN LIKE'%%%@%%' or cityLongName_CN LIKE'%%%@%%' or cityName_PinYin LIKE'%%%@%%'",keyWord,keyWord,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCtripChinaCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)searchOverseasCtripCityWithKeyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_country_city where cityName_CN LIKE'%%%@%%' or cityLongName_CN LIKE'%%%@%%' or cityName_PinYin LIKE'%%%@%%'",keyWord,keyWord,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCtripOverseaCityArray:arr];
    [self.db close];
    return dic;
}

/**
 * 搜索ChinaHotelCity
 */
-(NSDictionary *)searchChinaHotelCityWithKeyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_country_city where CountryCode='CN' AND CityName_CN LIKE'%%%@%%' OR CityName_PinYin LIKE'%%%@%%' ORDER by CityName_FirstWord",keyWord,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCountryCityArray:arr];
    [self.db close];
    return dic;
}

/**
 * 搜索OverseasHotelCountry国家
 */
-(NSDictionary *)searchOverseasHotelCountryWithKeyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_country_city where CountryCode!='CN' AND CountryName_CN LIKE'%%%@%%' OR CountryName LIKE'%%%@%%' ORDER by CountryCode_FirstWord",keyWord,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)getOverseasCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where CountryCode!='CN' ORDER by CountryCode_FirstWord"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCityArray:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)getCityWithCode:(CountyCityModel *)model{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where CountryCode=? ORDER by CityName_FirstWord",model.countryCode];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCountryCityArray:arr];
    [self.db close];
    return dic;
}

//搜索获取某个国家城市
-(NSDictionary *)searchCityWithModel:(CountyCityModel *)model keyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_country_city where CountryCode='%@' AND CityName_CN LIKE'%%%@%%' ORDER by CityName_FirstWord",model.countryCode,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CountyCityModel *model=[CountyCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCountryCityArray:arr];
    [self.db close];
    return dic;
}

//航班的
//-(NSArray *)getFlightCompanyWithCode:(NSString *)code{
//    [self.db open];
//    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_country_city where airlineCode==? COLLATE NOCASE",code];
//    NSMutableArray *arr=[NSMutableArray array];
//    while ([result next]) {
//        FlightCompanyModel *model=[FlightCompanyModel new];
//        [model getDataForResultSet:result];
//        [arr addObject:model];
//    }
//    [self.db close];
//    return arr;
//}

#pragma mark - public method
//CountryCity
-(NSDictionary *)handleCountryCityArray:(NSArray *)arr{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i ++) {
        CountyCityModel *model=array[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.cityName_FirstWord];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < array.count; j ++) {
            CountyCityModel *jmodel = array[j];
            if([model.cityName_FirstWord isEqualToString:jmodel.cityName_FirstWord]){
                str=model.cityName_FirstWord;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [array removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

//CtripChinaCity
-(NSDictionary *)handleCtripChinaCityArray:(NSArray *)arr{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i ++) {
        CountyCityModel *model=array[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.cityName_FirstWord];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < array.count; j ++) {
            CountyCityModel *jmodel = array[j];
            if([model.cityName_FirstWord isEqualToString:jmodel.cityName_FirstWord]){
                str=model.cityName_FirstWord;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [array removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

//CtripOverseaCity
-(NSDictionary *)handleCtripOverseaCityArray:(NSArray *)arr{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i ++) {
        CountyCityModel *model=array[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.cityName_FirstWord];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < array.count; j ++) {
            CountyCityModel *jmodel = array[j];
            if([model.cityName_FirstWord isEqualToString:jmodel.cityName_FirstWord]){
                str=model.cityName_FirstWord;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [array removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

//City
-(NSDictionary *)handleCityArray:(NSArray *)arr{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i ++) {
        CountyCityModel *model=array[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.cityName_FirstWord];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < array.count; j ++) {
            CountyCityModel *jmodel = array[j];
            if([model.cityName_FirstWord isEqualToString:jmodel.cityName_FirstWord]){
                str=model.cityName_FirstWord;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [array removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

@end
