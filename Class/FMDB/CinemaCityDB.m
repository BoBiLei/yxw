//
//  CinemaCityDB.m
//  youxia
//
//  Created by mac on 2016/12/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CinemaCityDB.h"
#import "CinemaCityModel.h"

@implementation CinemaCityDB

static CinemaCityDB *singleDataBase=nil;

+(CinemaCityDB *)shareDB{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[CinemaCityDB alloc]init];
    });
    return singleDataBase;
}

-(void)openDBForPath:(NSString *)path{
    NSString* dbPath=[[NSBundle mainBundle] pathForResource:path ofType:@"db"];
    //连接数据库
    self.db=[FMDatabase databaseWithPath:dbPath];
    if (![self.db open]) {
        NSLog(@"不能打开 citydb !");
    }
}

-(void)closeDB{
    if ([self.db open]) {
        [self.db close];
    }
}

-(NSDictionary *)getCinemaCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_movie_city"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CinemaCityModel *model=[CinemaCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCtripChinaCityArray:arr];
    [self.db close];
    return dic;
}

-(NSArray *)getHotCinemaCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from cenwor_tttuangou_movie_city where ishot='1'"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CinemaCityModel *model=[CinemaCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    [self.db close];
    return arr;
}

//
-(NSDictionary *)handleCtripChinaCityArray:(NSArray *)arr{
    
    NSMutableArray *dataArr = [NSMutableArray arrayWithArray:arr];
    
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    
    for (int i = 0; i < dataArr.count; i ++) {
        CinemaCityModel *model=dataArr[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.cityFirst];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < dataArr.count; j ++) {
            CinemaCityModel *jmodel = dataArr[j];
            if([model.cityFirst isEqualToString:jmodel.cityFirst]){
                str=model.cityFirst;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [dataArr removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

-(NSString *)getCityIdWithName:(NSString *)cityName{
    [self.db open];
    
    NSString *cityStr=cityName;
    NSRange rang=[cityName rangeOfString:@"市"];
    if (rang.location!=NSNotFound) {
        cityStr=[cityStr substringToIndex:rang.location];
    }
    NSString *formatStr=[NSString stringWithFormat:@"select * from cenwor_tttuangou_movie_city where cityName like '%%%@%%'",cityStr];
    FMResultSet *result=[self.db executeQuery:formatStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        CinemaCityModel *model=[CinemaCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSString *idStr;
    if (arr.count!=0) {
        CinemaCityModel *model=arr[0];
        idStr=model.cityId;
    }
    [self.db close];
    return idStr;
}

@end
