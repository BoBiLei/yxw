//
//  CountyDB.h
//  YXJR_SH
//
//  Created by mac on 2016/11/16.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB.h>
#import "CountyCityModel.h"
#import "CtripChinaCityModel.h"


@interface CountyDB : NSObject

+(CountyDB *)shareDB;

/**
 *  获取中国城市
 */
-(NSDictionary *)getChinaCity;

-(NSDictionary *)getCtripChinaCity;

-(NSDictionary *)getCtripOverSeaCity;

/**
 * 搜索ChinaCtripCity
 */
-(NSDictionary *)searchChinaCtripCityWithKeyWord:(NSString *)keyWord;

/**
 * 搜索OverseasCtripCity
 */
-(NSDictionary *)searchOverseasCtripCityWithKeyWord:(NSString *)keyWord;

/**
 * 搜索ChinaHotelCity
 */
-(NSDictionary *)searchChinaHotelCityWithKeyWord:(NSString *)keyWord;

/**
 * 搜索OverseasHotelCountry国家
 */
-(NSDictionary *)searchOverseasHotelCountryWithKeyWord:(NSString *)keyWord;

/**
 *  获取海外城市
 */
-(NSDictionary *)getOverseasCity;

/**
 *  获取城市
 */
-(NSDictionary *)getCityWithCode:(CountyCityModel *)model;

/**
 *  搜索获取某个国家城市
 */
-(NSDictionary *)searchCityWithModel:(CountyCityModel *)model keyWord:(NSString *)keyWord;

/**
 *  获取航班公司
 */
-(NSArray *)getFlightCompanyWithCode:(NSString *)code;

-(void)openDBForPath:(NSString *)path;

-(void)closeDB;

@property (strong,nonatomic) FMDatabase *db;

@property (readonly) NSString *databasePath;

@end
