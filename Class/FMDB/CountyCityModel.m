//
//  CountyCityModel.m
//  YXJR_SH
//
//  Created by mac on 2016/11/16.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "CountyCityModel.h"

@implementation CountyCityModel

-(void)getDataForResultSet:(FMResultSet *)resultSet{
    self.cId=[resultSet stringForColumn:@"id"];
    self.countryCode=[resultSet stringForColumn:@"CountryCode"];
    self.cityCode=[resultSet stringForColumn:@"CityCode"];
    self.cityName=[resultSet stringForColumn:@"CityName"];
    self.cityLongName=[resultSet stringForColumn:@"CityLongName"];
    self.cityName_CN=[resultSet stringForColumn:@"CityName_CN"];
    self.cityLongName_CN=[resultSet stringForColumn:@"CityLongName_CN"];
    self.cityName_PinYin=[resultSet stringForColumn:@"CityName_PinYin"];
    self.cityName_FirstWord=[resultSet stringForColumn:@"CityName_FirstWord"];
}

@end
