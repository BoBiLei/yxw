//
//  CtripChinaCityModel.m
//  YXJR_SH
//
//  Created by mac on 2016/11/18.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import "CtripChinaCityModel.h"

@implementation CtripChinaCityModel

-(void)getDataForResultSet:(FMResultSet *)resultSet{
    self.cityId=[resultSet stringForColumn:@"cityID"];
    self.cityName=[resultSet stringForColumn:@"cityName"];
    self.cityNamePY=[resultSet stringForColumn:@"cityNamePY"];
    self.cityNameJP=[resultSet stringForColumn:@"cityNameJP"];
    self.firstLetter=[resultSet stringForColumn:@"firstLetter"];
    
    self.countryCode=[resultSet stringForColumn:@"cityID"];
    self.cityCode=[resultSet stringForColumn:@"cityCode"];
}

@end
