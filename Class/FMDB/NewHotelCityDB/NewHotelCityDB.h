//
//  NewHotelCityDB.h
//  youxia
//
//  Created by 雷 on 2017/6/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB.h>
#import "NewHotelCityModel.h"

@interface NewHotelCityDB : NSObject

+(NewHotelCityDB *)shareDB;

/**
 *  获取城市
 */
-(NSDictionary *)getHotelCity;

/**
 * 关键字搜索城市
 */
-(NSDictionary *)searchCityWithKeyWord:(NSString *)keyWord;


-(void)openDBForPath:(NSString *)path;

-(void)closeDB;

@property (strong,nonatomic) FMDatabase *db;

@property (readonly) NSString *databasePath;

@end
