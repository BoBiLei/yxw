//
//  NewHotelCityModel.h
//  youxia
//
//  Created by 雷 on 2017/6/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMResultSet.h>

@interface NewHotelCityModel : NSObject

@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *cityName_cn;
@property (nonatomic, copy) NSString *patentCityCode;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *firstCapital;
@property (nonatomic, copy) NSString *hbdc;

@property (nonatomic, assign) NSInteger seleCityType;

-(void)getDataForResultSet:(FMResultSet *)resultSet;

@end
