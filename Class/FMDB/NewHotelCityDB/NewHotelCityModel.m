//
//  NewHotelCityModel.m
//  youxia
//
//  Created by 雷 on 2017/6/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewHotelCityModel.h"

@implementation NewHotelCityModel

-(void)getDataForResultSet:(FMResultSet *)resultSet{
    self.cId=[resultSet stringForColumn:@"_id"];
    self.countryCode=[resultSet stringForColumn:@"countryCode"];
    self.cityCode=[resultSet stringForColumn:@"cityCode"];
    self.cityName=[resultSet stringForColumn:@"cityName"];
    self.cityName_cn=[resultSet stringForColumn:@"cityName_cn"];
    self.patentCityCode=[resultSet stringForColumn:@"parentCityCode"];
    self.type=[resultSet stringForColumn:@"type"];
    self.firstCapital=[resultSet stringForColumn:@"firstCapital"];
    self.hbdc=[resultSet stringForColumn:@"hbdc"];
}

@end
