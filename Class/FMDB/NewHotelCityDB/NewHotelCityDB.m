//
//  NewHotelCityDB.m
//  youxia
//
//  Created by 雷 on 2017/6/12.
//  Copyright © 2017年 游侠旅游. All rights reserved.
//

#import "NewHotelCityDB.h"

@implementation NewHotelCityDB

static NewHotelCityDB *singleDataBase=nil;

+(NewHotelCityDB *)shareDB{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[NewHotelCityDB alloc]init];
    });
    return singleDataBase;
}

-(void)openDBForPath:(NSString *)path{
    NSString* dbPath=[[NSBundle mainBundle] pathForResource:path ofType:@"db"];
    //连接数据库
    self.db=[FMDatabase databaseWithPath:dbPath];
    if (![self.db open]) {
        NSLog(@"不能打开 citydb !");
    }else{
        NSLog(@"打开db !");
    }
}

-(void)closeDB{
    if ([self.db open]) {
        [self.db close];
    }
}

-(NSDictionary *)getHotelCity{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from city"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        NewHotelCityModel *model=[NewHotelCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCitys:arr];
    [self.db close];
    return dic;
}

-(NSDictionary *)searchCityWithKeyWord:(NSString *)keyWord{
    [self.db open];
    NSString *sqlStr=[NSString stringWithFormat:@"select * from city where cityName_cn LIKE'%%%@%%' or cityName LIKE'%%%@%%' or hbdc LIKE'%%%@%%'",keyWord,keyWord,keyWord];
    FMResultSet *result=[self.db executeQuery:sqlStr];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        NewHotelCityModel *model=[NewHotelCityModel new];
        [model getDataForResultSet:result];
        [arr addObject:model];
    }
    NSDictionary *dic=[self handleCitys:arr];
    [self.db close];
    return dic;
}


-(NSDictionary *)handleCitys:(NSArray *)arr{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    NSMutableDictionary *hadDic=[NSMutableDictionary dictionary];
    for (int i = 0; i < array.count; i ++) {
        NewHotelCityModel *model=array[i];
        NSMutableArray *tempArray = [NSMutableArray array];
        NSMutableDictionary *dic=[NSMutableDictionary dictionary];
        NSString *str;
        [tempArray addObject:model];
        [dic setObject:tempArray forKey:model.firstCapital];
        [hadDic addEntriesFromDictionary:dic];
        for (int j = i+1; j < array.count; j ++) {
            NewHotelCityModel *jmodel = array[j];
            if([model.firstCapital isEqualToString:jmodel.firstCapital]){
                str=model.firstCapital;
                [tempArray addObject:jmodel];
                [dic setObject:tempArray forKey:str];
                [array removeObjectAtIndex:j];
                j = j-1;
            }
        }
        [hadDic addEntriesFromDictionary:dic];
    }
    return hadDic;
}

@end
