//
//  FmdbCityModel.h
//  youxia
//
//  Created by mac on 15/12/30.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FmdbCityModel : NSObject
@property (nonatomic,copy) NSString *parent_id; //父级ID
@property (nonatomic,copy) NSString *cityName;  //城市名
@property (nonatomic,copy) NSString *ids;       //城市ID
@property (nonatomic,copy) NSString *type;      //类型（省1、市、区3）
@end
