//
//  CinemaCityModel.m
//  youxia
//
//  Created by mac on 2016/12/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import "CinemaCityModel.h"

@implementation CinemaCityModel

-(void)getDataForResultSet:(FMResultSet *)resultSet{
    self.cId=[resultSet stringForColumn:@"id"];
    self.cityId=[resultSet stringForColumn:@"cityId"];
    self.cityName=[resultSet stringForColumn:@"cityName"];
    self.cityPinyin=[resultSet stringForColumn:@"cityPinyin"];
    self.ishot=[resultSet stringForColumn:@"ishot"];
    self.cityFirst=[[resultSet stringForColumn:@"cityFirst"] uppercaseString];
}

@end
