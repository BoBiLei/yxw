//
//  CtripOverseaCityModel.h
//  YXJR_SH
//
//  Created by mac on 2016/11/18.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMResultSet.h>

@interface CtripOverseaCityModel : NSObject

@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *cityNameEN;
@property (nonatomic, copy) NSString *cityNamePY;
@property (nonatomic, copy) NSString *cityNameJP;
@property (nonatomic, copy) NSString *firstLetter;

@property (nonatomic, assign) NSInteger seleCityType;

-(void)getDataForResultSet:(FMResultSet *)resultSet;

@end
