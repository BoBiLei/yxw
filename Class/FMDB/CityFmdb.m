//
//  CityFmdb.m
//  MMG
//
//  Created by mac on 15/11/5.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "CityFmdb.h"
#import "FmdbCityModel.h"
@implementation CityFmdb
#pragma mark - 获取省市区
static CityFmdb *singleDataBase=nil;
+(CityFmdb *)shareCityFMDB{
    static dispatch_once_t once;
    dispatch_once(&once,^{
        singleDataBase=[[CityFmdb alloc]init];
        [singleDataBase openCityDB];
    });
    return singleDataBase;
}
-(void)openCityDB{
    NSString* cityDBPath=[[NSBundle mainBundle] pathForResource:@"city" ofType:@"sqlite"];
    //连接数据库
    self.db=[FMDatabase databaseWithPath:cityDBPath];
    if (![self.db open]) {
        DSLog(@"不能打开 citydb !");
    }
}

#pragma mark - 获取省
-(NSMutableArray *)getProvince{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from ecs_region where parent_id='1'"];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        FmdbCityModel *model=[[FmdbCityModel alloc]init];
        model.parent_id=[result stringForColumn:@"region_id"];
        model.cityName=[result stringForColumn:@"region_name"];
        model.type=[result stringForColumn:@"region_type"];
        [arr addObject:model];
    }
    [self.db close];
    return arr;
}

#pragma mark - 根据省份id获取市/区
-(NSMutableArray *)getCityWithId:(NSString *)regionId{
    [self.db open];
    FMResultSet *result=[self.db executeQuery:@"select * from ecs_region where parent_id=?",regionId];
    NSMutableArray *arr=[NSMutableArray array];
    while ([result next]) {
        FmdbCityModel *model=[[FmdbCityModel alloc]init];
        model.parent_id=[result stringForColumn:@"region_id"];
        model.cityName=[result stringForColumn:@"region_name"];
        model.type=[result stringForColumn:@"region_type"];
        [arr addObject:model];
    }
    [self.db close];
    return arr;
}
@end
