//
//  CinemaCityModel.h
//  youxia
//
//  Created by mac on 2016/12/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMResultSet.h>

@interface CinemaCityModel : NSObject

@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *cityPinyin;
@property (nonatomic, copy) NSString *ishot;
@property (nonatomic, copy) NSString *cityFirst;

-(void)getDataForResultSet:(FMResultSet *)resultSet;

@end
