//
//  CinemaCityDB.h
//  youxia
//
//  Created by mac on 2016/12/30.
//  Copyright © 2016年 游侠旅游. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB.h>

@interface CinemaCityDB : NSObject

@property (strong,nonatomic) FMDatabase *db;

+(CinemaCityDB *)shareDB;

-(void)openDBForPath:(NSString *)path;

-(void)closeDB;

-(NSDictionary *)getCinemaCity;

-(NSArray *)getHotCinemaCity; //热门城市

-(NSString *)getCityIdWithName:(NSString *)cityName;

@end
