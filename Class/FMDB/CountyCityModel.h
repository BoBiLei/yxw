//
//  CountyCityModel.h
//  YXJR_SH
//
//  Created by mac on 2016/11/16.
//  Copyright © 2016年 游侠金融商户版. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMResultSet.h>

@interface CountyCityModel : NSObject

@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *cityLongName;
@property (nonatomic, copy) NSString *cityName_CN;
@property (nonatomic, copy) NSString *cityLongName_CN;
@property (nonatomic, copy) NSString *cityName_PinYin;
@property (nonatomic, copy) NSString *cityName_FirstWord;

@property (nonatomic, assign) NSInteger seleCityType;

-(void)getDataForResultSet:(FMResultSet *)resultSet;

@end
