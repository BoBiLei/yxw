//
//  ReceiverManagerController.m
//  CatShopping
//
//  Created by mac on 15/9/25.
//  Copyright © 2015年 游侠旅游. All rights reserved.
//

#import "ReceiverManagerController.h"

@interface ReceiverManagerController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ReceiverManagerController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"收件人信息"];
}

-(void)setUpUI{
    UIImageView *bagView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64)];
    bagView.image=[UIImage imageNamed:@"login_baground"];
    
    UITableView *myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStylePlain];
    [self.view addSubview:myTable];
    myTable.backgroundView=bagView;
    myTable.dataSource=self;
    myTable.delegate=self;
}

#pragma mark TableView delegate---
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
