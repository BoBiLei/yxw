//
//  NewMsgController.m
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "NewMsgController.h"
#import "NewMsgCell.h"
#import "NewMsgDetailController.h"

@interface NewMsgController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation NewMsgController{
    NSMutableArray *dataArr;
    UITableView *myTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"最新信息"];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    
    [self initUI];
    [self requestHttpsForNewMsgList];
}

#pragma mark - init UI
-(void)initUI{
    dataArr=[NSMutableArray array];
    myTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTable.backgroundColor=[UIColor colorWithHexString:@"#e6e6e6"];
    myTable.dataSource=self;
    myTable.delegate=self;
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [myTable registerNib:[UINib nibWithNibName:@"NewMsgCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:myTable];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 326;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 9;
    }else{
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewMsgCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[NewMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (indexPath.row<dataArr.count) {
        NewMsgModel *model=dataArr[indexPath.section];
        [cell reflushModelDataForModel:model];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewMsgModel *model=dataArr[indexPath.section];
    NewMsgDetailController *msgDetail=[[NewMsgDetailController alloc]init];
    msgDetail.msgId=model.msgId;
    [self.navigationController pushViewController:msgDetail animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTable setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTable respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTable setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request 请求
-(void)requestHttpsForNewMsgList{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"id":@"11",
                         @"is_nosign":@"1"
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"article_cat.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            NSArray *arr=responseObject[@"retData"][@"artciles_list"];
            for (NSDictionary *dic in arr) {
                NewMsgModel *model=[[NewMsgModel alloc]init];
                [model jsonDataForDictionay:dic];
                [dataArr addObject:model];
            }
        }else{
            NSLog(@"请求错误");
        }
        [myTable reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
