//
//  CategoryController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "CategoryController.h"
#import "HomePageCategoryCell.h"
#import "CategoryCell.h"
#import "CategoryLeftModel.h"
#import "ProductListController.h"
#import "CategoryLeftCell.h"

#define kSeparatorColor [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1]
#define IMAGEPLAY_HEIHGT SCREENSIZE.height/3-28
#define CATEGORY_HEIHGT (SCREENSIZE.width-60)/4.0*2+24
@interface CategoryController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end


@implementation CategoryController{
    
    NSInteger seleTableRow;
    
    NSMutableArray *leftViewArr;
    UITableView *leftTableView;
    NSMutableArray *categoryArr;//快捷分类Array
    UICollectionView *rightTableView;
    
    CategoryLeftModel *seleCateModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomRootViewNavigationTitle:@"分类"];
    [self setUpUI];
    
    [self requestHttpsForCategoryList];
}

#pragma mark - init view
-(void)setUpUI{
    
    seleTableRow=0;
    //left
    leftViewArr=[NSMutableArray array];
    leftTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width/4, SCREENSIZE.height-113) style:UITableViewStyleGrouped];
    leftTableView.backgroundColor=[UIColor colorWithHexString:@"#f8f8f8"];
    leftTableView.dataSource=self;
    leftTableView.delegate=self;
    leftTableView.bounces=NO;
    leftTableView.showsVerticalScrollIndicator=NO;
    leftTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:leftTableView];
    [leftTableView registerNib:[UINib nibWithNibName:@"CategoryLeftCell" bundle:nil] forCellReuseIdentifier:@"cellID"];
    leftTableView.tableFooterView=[[UIView alloc]init];
    
    //right
    categoryArr=[NSMutableArray array];
    UICollectionViewFlowLayout *myLayout= [[UICollectionViewFlowLayout alloc]init];
    rightTableView=[[UICollectionView alloc]initWithFrame:CGRectMake(leftTableView.width, 0, SCREENSIZE.width-leftTableView.width, SCREENSIZE.height-113) collectionViewLayout:myLayout];
    [rightTableView registerNib:[UINib nibWithNibName:@"CategoryCell" bundle:nil] forCellWithReuseIdentifier:@"collcell"];
    rightTableView.showsVerticalScrollIndicator=NO;
    rightTableView.backgroundColor=[UIColor clearColor];
    rightTableView.dataSource=self;
    rightTableView.delegate=self;
    [self.view addSubview:rightTableView];
}

#pragma mark - tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return leftViewArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0000001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CategoryLeftCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if(cell==nil){
        cell=[[CategoryLeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    
    cell.myTitle.highlightedTextColor = [UIColor orangeColor];
    CategoryLeftModel *model=leftViewArr[indexPath.row];
    cell.myTitle.text=model.name;
    UIView *seletedView=[[UIView alloc]initWithFrame:cell.frame];
    seletedView.backgroundColor=[UIColor whiteColor];
    cell.selectedBackgroundView=seletedView;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    seleCateModel=leftViewArr[indexPath.row];
    if (seleTableRow!=indexPath.row) {
        [self requestHttpsCategoryForId:seleCateModel.cid];
    }
    seleTableRow=indexPath.row;
}

#pragma mark - UICollectionView delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return categoryArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CategoryCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"collcell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[CategoryCell alloc]init];
    }
    CategoryRightModel *model=categoryArr[indexPath.row];
    [cell reflushDataForModel:model];
    return cell;
}

//每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREENSIZE.width-leftTableView.frame.size.width)/2-18, 112);
}

- (CGFloat)minimumInteritemSpacing {
    return 0;
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 10, 0, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CategoryRightModel *model=categoryArr[indexPath.row];
    
    ProductListController *productList=[[ProductListController alloc]init];
    productList.requestType=CategoryRequestType;
    productList.productListId=model.cateId;
    productList.productListName=model.cateName;
    productList.hidesBottomBarWhenPushed=YES;
    
    [self.navigationController pushViewController:productList animated:YES];
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([leftTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [leftTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([leftTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [leftTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Request（分类列表请求）
-(void)requestHttpsForCategoryList{
    [AppUtils showActivityIndicatorView:self.view];
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"10000" forKey:@"id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"category.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCategoryList];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                NSArray *arr=responseObject[@"retData"][@"navigator_list"];
                for (NSDictionary *dic in arr) {
                    CategoryLeftModel *model=[[CategoryLeftModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [leftViewArr addObject:model];
                }
                
                NSArray *cateArr=responseObject[@"retData"][@"categories"];
                for (NSDictionary *dic in cateArr) {
                    CategoryRightModel *model=[[CategoryRightModel alloc]init];
                    [model jsonDataForDictionary:dic];
                    [categoryArr addObject:model];
                }
                
            }
            
            //设置默认选中第一行
            [leftTableView reloadData];
            NSIndexPath *firstPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [leftTableView selectRowAtIndexPath:firstPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [rightTableView reloadData];
            [AppUtils dismissActivityIndicatorView:self.view];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
        [AppUtils dismissActivityIndicatorView:self.view];
    }];
    [categoryArr removeAllObjects];
}

#pragma mark - Request（点击某个类别请求）
-(void)requestHttpsCategoryForId:(NSString *)categoryId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:categoryId forKey:@"id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"category.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsCategoryForId:seleCateModel.cid];
            });
        }else{
            NSArray *arr=responseObject[@"retData"][@"categories"];
            for (NSDictionary *dic in arr) {
                CategoryRightModel *model=[[CategoryRightModel alloc]init];
                [model jsonDataForDictionary:dic];
                [categoryArr addObject:model];
            }
            [rightTableView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
    [categoryArr removeAllObjects];
}

@end
