//
//  UIColor+Hex.h
//  DZ100
//
//  Created by dingzhi100-03 on 15/5/25.
//  Copyright (c) 2015年 深圳市方云互联网有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
+(UIColor *)colorWithHexString:(NSString *)color;
+(UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;
@end
