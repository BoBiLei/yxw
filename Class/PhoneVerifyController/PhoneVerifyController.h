//
//  PhoneVerifyController.h
//  CatShopping
//
//  Created by mac on 15/9/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "BaseController.h"

@interface PhoneVerifyController : BaseController

@property (nonatomic,copy) NSString *receiverPhone;

@end


//绑定新号码
@interface BoundNewPhoneController : BaseController

@end