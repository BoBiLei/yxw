//
//  NewMsgDetailController.m
//  MMG
//
//  Created by mac on 15/11/30.
//  Copyright © 2015年 猫猫购. All rights reserved.
//

#import "NewMsgDetailController.h"

@interface NewMsgDetailController ()<UIWebViewDelegate,NJKWebViewProgressDelegate>

@end

@implementation NewMsgDetailController{
    UIWebView *_webView;
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"信息详情"];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,SCREENSIZE.width , SCREENSIZE.height-64)];
    _webView.scrollView.bounces = YES;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.paginationMode = UIWebPaginationModeUnpaginated;
    _webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@article.php?id=%@&is_phone=1",ImageHost,self.msgId]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:_webView];
    
    [self setUpWebViewProgress];
}

//设置 NJKWebViewProgress
-(void)setUpWebViewProgress{
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress{
    [_progressView setProgress:progress animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}
@end
