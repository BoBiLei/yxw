//
//  UserOrderController.m
//  CatShopping
//
//  Created by mac on 15/9/14.
//  Copyright (c) 2015年 猫猫购. All rights reserved.
//

#import "UserOrderController.h"
#import "UserOrdersCell.h"
#import "OrderDetailController.h"
#import "ProductDetailController.h"
#import "OrderPayController.h"

NSString *userOrderIdentifier=@"userordercell";
@interface UserOrderController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation UserOrderController{
    NSMutableArray *dataArr;
    NSMutableArray *goodDataArr;
    UITableView *myTableview;
    UILabel *tipLabel;
    NSString *moreDataStr;
    
    NSString *selectedId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomNavigationTitle:@"我的订单"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upDateOrderList) name:@"UpdateOrderList" object:nil];
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    [self setUpUI];
    
    [self requestHttpsForOrderList];
}


#pragma mark - Notification method
-(void)upDateOrderList{
    [self requestHttpsForOrderList];
}

#pragma mark - init view
-(void)setUpUI{
    //
    tipLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, 21)];
    tipLabel.text=@"您暂无订单";
    tipLabel.textColor=[UIColor lightGrayColor];
    tipLabel.textAlignment=NSTextAlignmentCenter;
    tipLabel.font=[UIFont systemFontOfSize:16];
    tipLabel.center=CGPointMake(SCREENSIZE.width/2, (SCREENSIZE.height-113)/2);
    [self.view addSubview:tipLabel];
    
    //
    dataArr=[NSMutableArray array];
    
    goodDataArr=[NSMutableArray array];
    //
    myTableview=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREENSIZE.width, SCREENSIZE.height-64) style:UITableViewStyleGrouped];
    myTableview.dataSource=self;
    myTableview.delegate=self;
    myTableview.sectionHeaderHeight = 50;
    [myTableview registerNib:[UINib nibWithNibName:@"UserOrdersCell" bundle:nil] forCellReuseIdentifier:userOrderIdentifier];
    [self.view addSubview:myTableview];
    myTableview.backgroundColor=[UIColor groupTableViewBackgroundColor];
    myTableview.scrollsToTop=YES;
    
    /*
     *下拉刷新
     */
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestHttpsForOrderList];
    }];
    header.arrowView.hidden=NO;
    // 设置字体
    header.stateLabel.font = [UIFont systemFontOfSize:12];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:11];
    myTableview.mj_header = header;
    
    //================
    //加载更多
    //================
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self refreshForPullUp];
    }];
    footer.stateLabel.font = [UIFont systemFontOfSize:13];
    footer.stateLabel.textColor = [UIColor colorWithHexString:@"#282828"];
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    myTableview.mj_footer = footer;
    
}

- (void)refreshForPullUp{
    if (![moreDataStr isEqualToString:@""]) {
        [self requestHttpsMoreProduct:moreDataStr];
    }
    [myTableview.mj_footer endRefreshing];
}

#pragma mark - TableView delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return goodDataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)goodDataArr[section]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserOrdersCell *cell=[tableView dequeueReusableCellWithIdentifier:userOrderIdentifier];
    if (cell==nil) {
        cell=[[UserOrdersCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:userOrderIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    //解决崩溃办法indexPath.row<goodDataArr.count
    if(indexPath.row<goodDataArr.count){
        NSDictionary *dic=goodDataArr[indexPath.section][indexPath.row];
        
        NSString *imgStr=[NSString stringWithFormat:@"%@%@",ImageHost,dic[@"goods_thumb"]];
        [cell.imgv setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:[UIImage imageNamed:@"placeholder_100x100"]];
        cell.goodTitle.text=dic[@"goods_name"];
        cell.goodPrice.text=dic[@"goods_price"];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic=goodDataArr[indexPath.section][indexPath.row];
    ProductDetailController *prodectDetail=[[ProductDetailController alloc]init];
    prodectDetail.goodId=dic[@"goods_id"];
    [self.navigationController pushViewController:prodectDetail animated:YES];
}

//section的标题栏高度
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 64;
}

//自定义组头标题
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //解决崩溃要判断section<dataArr.count
    UserOrdersModel *headModel;
    if (section<dataArr.count) {
        headModel=dataArr[section];
    }
    
    UIView *headerSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, myTableview.frame.size.width, 64)];
    headerSectionView.backgroundColor=[UIColor colorWithHexString:@"#fafafa"];
    
    UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headerSectionView.width, 14)];
    imgv.backgroundColor=[UIColor groupTableViewBackgroundColor];
    [headerSectionView addSubview:imgv];
    
    //订单编号
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(8, 18, SCREENSIZE.width-16, 23)];
    label.font=[UIFont systemFontOfSize:14];
    label.textColor=[UIColor colorWithHexString:@"#282828"];
    NSString *headTitle=headModel.orderNumber;
    label.text=[NSString stringWithFormat:@"订单编号：%@",headTitle];
    [headerSectionView addSubview:label];
    
    //审核状态
    UILabel *statusLabel=[[UILabel alloc] initWithFrame:CGRectMake(8, label.origin.y+label.height, headerSectionView.width-16, headerSectionView.height-(label.origin.y+label.height+4))];
    [headerSectionView addSubview:statusLabel];
    statusLabel.font=[UIFont systemFontOfSize:14];
    statusLabel.textColor=[UIColor colorWithHexString:@"#282828"];
    
    statusLabel.text=[NSString stringWithFormat:@"订单状态：%@",headModel.headerStatus];
    return headerSectionView;
}

//section的尾部标题高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    UserOrdersModel *model=dataArr[section];
    if ([model.couponId isEqualToString:@"0"]) {
        return 108;
    }else{
        return 128;
    }
}

//自定义组尾标题
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UserOrdersModel *model;
    if(section<dataArr.count){
        model=dataArr[section];
    }
    
    UIView *sectionFootView=[UIView new];
    sectionFootView.backgroundColor=[UIColor colorWithHexString:@"#ffffff"];
    
    //取消订单button
    UIButton *cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.backgroundColor=[UIColor colorWithHexString:@"#ababab"];
    cancelBtn.layer.cornerRadius=2;
    [cancelBtn setTitle:@"取消订单" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    //向button传递对象
    objc_setAssociatedObject(cancelBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [cancelBtn addTarget:self action:@selector(clickRebateBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:cancelBtn];
    cancelBtn.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    //查看详情button
    UIButton *seeDetailBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    seeDetailBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    seeDetailBtn.layer.cornerRadius=2;
    [seeDetailBtn setTitle:@"查看详情" forState:UIControlStateNormal];
    [seeDetailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    seeDetailBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    //向button传递对象
    objc_setAssociatedObject(seeDetailBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [seeDetailBtn addTarget:self action:@selector(clickSeeDetailBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:seeDetailBtn];
    seeDetailBtn.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    //付款button
    UIButton *payBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    payBtn.backgroundColor=[UIColor colorWithHexString:@"#ec6b00"];
    payBtn.layer.cornerRadius=2;
    [payBtn setTitle:@"付款" forState:UIControlStateNormal];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    payBtn.titleLabel.font=[UIFont systemFontOfSize:13];
    //向button传递对象
    objc_setAssociatedObject(payBtn,"UserOrderModel",model, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [payBtn addTarget:self action:@selector(clickPayBtn:) forControlEvents:UIControlEventTouchUpInside];
    [sectionFootView addSubview:payBtn];
    payBtn.translatesAutoresizingMaskIntoConstraints = NO;
    
    if ([model.orderStatus isEqualToString:@"0"]) {
        //
        cancelBtn.hidden=NO;
        
//        cancelBtn.frame=CGRectMake(sectionFootView.width-68, sectionFootView.height-30, 60, 22);
        NSArray* cancelBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[cancelBtn(60)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cancelBtn)];
        [NSLayoutConstraint activateConstraints:cancelBtn_h];
        
        NSArray* cancelBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[cancelBtn(22)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(cancelBtn)];
        [NSLayoutConstraint activateConstraints:cancelBtn_w];
        //
        seeDetailBtn.hidden=NO;
        
//        seeDetailBtn.frame=CGRectMake(cancelBtn.origin.x-70, cancelBtn.origin.y, cancelBtn.width, cancelBtn.height);
        
        NSArray* seeDetailBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[seeDetailBtn(60)]-10-[cancelBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(seeDetailBtn,cancelBtn)];
        [NSLayoutConstraint activateConstraints:seeDetailBtn_h];
        
        NSArray* seeDetailBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[seeDetailBtn(22)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(seeDetailBtn)];
        [NSLayoutConstraint activateConstraints:seeDetailBtn_w];
        
        //
        payBtn.hidden=NO;
        
//        payBtn.frame=CGRectMake(seeDetailBtn.origin.x-70, seeDetailBtn.origin.y, seeDetailBtn.width, seeDetailBtn.height);
        
        NSArray* payBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[payBtn(60)]-10-[seeDetailBtn]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(payBtn,seeDetailBtn)];
        [NSLayoutConstraint activateConstraints:payBtn_h];
        
        NSArray* payBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[payBtn(22)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(payBtn)];
        [NSLayoutConstraint activateConstraints:payBtn_w];
    }else{
        cancelBtn.hidden=YES;
        seeDetailBtn.hidden=NO;
        payBtn.hidden=YES;
        
        NSArray* seeDetailBtn_h = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[seeDetailBtn(60)]-12-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(seeDetailBtn)];
        [NSLayoutConstraint activateConstraints:seeDetailBtn_h];
        
        NSArray* seeDetailBtn_w = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[seeDetailBtn(22)]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(seeDetailBtn)];
        [NSLayoutConstraint activateConstraints:seeDetailBtn_w];
    }
    
    //label01
    TTTAttributedLabel *label01=[TTTAttributedLabel new];
    label01.textAlignment=NSTextAlignmentRight;
    label01.textColor=[UIColor colorWithHexString:@"#ababab"];
    label01.font=[UIFont systemFontOfSize:13];
    NSString *label01Str=[NSString stringWithFormat:@"%@件商品，总金额：%@",model.goodSum,model.totalFee];
    [label01 setText:label01Str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange sumRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.goodSum] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:sumRange];
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@",model.totalFee] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    [sectionFootView addSubview:label01];
    
    //label02
    TTTAttributedLabel *label02=[TTTAttributedLabel new];
    label02.textAlignment=NSTextAlignmentRight;
    label02.textColor=[UIColor colorWithHexString:@"#ababab"];
    label02.font=[UIFont systemFontOfSize:13];
    NSString *label02Str=[NSString stringWithFormat:@"分期手续费：%@元",model.stageFee];
    [label02 setText:label02Str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",model.stageFee] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    [sectionFootView addSubview:label02];
    
    //label03
    TTTAttributedLabel *label03=[TTTAttributedLabel new];
    label03.textAlignment=NSTextAlignmentRight;
    label03.textColor=[UIColor colorWithHexString:@"#ababab"];
    label03.font=[UIFont systemFontOfSize:13];
    NSString *label03Str=[NSString stringWithFormat:@"订单总金额：%@元",model.orderTotalPrice];
    [label03 setText:label03Str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"%@元",model.orderTotalPrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    [sectionFootView addSubview:label03];
    
    //couponLabel
    UILabel *couponLabel=[UILabel new];
    couponLabel.textAlignment=NSTextAlignmentRight;
    couponLabel.textColor=[UIColor colorWithHexString:@"#ababab"];
    couponLabel.font=[UIFont systemFontOfSize:13];
    couponLabel.text=[NSString stringWithFormat:@"(已优惠%@)",model.couponMoney];
    couponLabel.textColor=[UIColor colorWithHexString:@"#ec6b00"];
    [sectionFootView addSubview:couponLabel];
    
    //label04
    TTTAttributedLabel *label04=[TTTAttributedLabel new];
    label04.textAlignment=NSTextAlignmentRight;
    label04.textColor=[UIColor colorWithHexString:@"#ababab"];
    label04.font=[UIFont systemFontOfSize:13];
    NSString *label04Str=[NSString stringWithFormat:@"分期支付(分%@期，每期%@元)",model.stageSum,model.stageOnePrice];
    [label04 setText:label04Str afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        NSRange boldRange = [[mutableAttributedString string] rangeOfString:[NSString stringWithFormat:@"(分%@期，每期%@元)",model.stageSum,model.stageOnePrice] options:NSCaseInsensitiveSearch];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor redColor] CGColor] range:boldRange];
        return mutableAttributedString;
    }];
    [sectionFootView addSubview:label04];
    
    if ([model.couponId isEqualToString:@"0"]) {
        label01.frame=CGRectMake(8, 6, SCREENSIZE.width-16, 17);
        label02.frame=CGRectMake(8, label01.frame.origin.y+label01.frame.size.height, SCREENSIZE.width-16, 17);
        label03.frame=CGRectMake(8, label02.frame.origin.y+label02.frame.size.height, SCREENSIZE.width-16, 17);
        couponLabel.frame=CGRectMake(0, 0, 0, 0);
        label04.frame=CGRectMake(8, label03.frame.origin.y+label03.frame.size.height, SCREENSIZE.width-16, 17);
        
        sectionFootView.frame=CGRectMake(0, 0, myTableview.frame.size.width, 108);
    }else{
        label01.frame=CGRectMake(8, 6, SCREENSIZE.width-16, 17);
        label02.frame=CGRectMake(8, label01.frame.origin.y+label01.frame.size.height, SCREENSIZE.width-16, 17);
        label03.frame=CGRectMake(8, label02.frame.origin.y+label02.frame.size.height, SCREENSIZE.width-16, 17);
        couponLabel.frame=CGRectMake(8, label03.frame.origin.y+label03.frame.size.height, SCREENSIZE.width-16, 17);
        label04.frame=CGRectMake(8, couponLabel.frame.origin.y+couponLabel.frame.size.height, SCREENSIZE.width-16, 17);
        
        sectionFootView.frame=CGRectMake(0, 0, myTableview.frame.size.width, 128);
    }
    
    return sectionFootView;
}

#pragma mark - 重绘分割线
-(void)viewDidLayoutSubviews {
    if ([myTableview respondsToSelector:@selector(setSeparatorInset:)]) {
        [myTableview setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([myTableview respondsToSelector:@selector(setLayoutMargins:)])  {
        [myTableview setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - button event

//点击查看详情
-(void)clickSeeDetailBtn:(id)sender{
    //获取button传递过来的对象
    UserOrdersModel *model=(UserOrdersModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    
    OrderDetailController *orderDetail=[[OrderDetailController alloc]init];
    orderDetail.orderId=model.orderId;
    orderDetail.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:orderDetail animated:YES];
}

//点击取消
-(void)clickRebateBtn:(id)sender{
    //获取button传递过来的对象
    UserOrdersModel *model=(UserOrdersModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    selectedId=model.orderId;
    //配置一下
    [[MMPopupWindow sharedWindow] cacheWindow];
    [MMPopupWindow sharedWindow].touchWildToHide = YES;
    
    MMAlertViewConfig *sheetConfig = [MMAlertViewConfig globalConfig];
    sheetConfig.buttonFontSize=15;
    sheetConfig.titleColor=[UIColor colorWithHexString:@"#ec6b00"];
    
    MMPopupItemHandler block = ^(NSInteger index){
        switch (index) {
            case 0:
                [self requestHttpsForCancelOrder:selectedId];
                break;
            default:
                break;
        }
    };
    
    MMPopupBlock completeBlock = ^(MMPopupView *popupView){
        
    };
    NSArray *items =
    @[MMItemMake(@"确定", MMItemTypeNormal, block),
      MMItemMake(@"取消", MMItemTypeNormal, block)];
    
    [[[MMAlertView alloc] initWithTitle:@"提示" detail:@"是否删除该商品" items:items] showWithBlock:completeBlock];
}

//点击付款
-(void)clickPayBtn:(id)sender{
    //获取button传递过来的对象
    UserOrdersModel *model=(UserOrdersModel *)objc_getAssociatedObject(sender,"UserOrderModel");
    
    OrderPayController *orderPayContr=[[OrderPayController alloc]init];
    orderPayContr.orderId=model.orderNumber;
    [self.navigationController pushViewController:orderPayContr animated:YES];
}

#pragma mark - HTTP Request
//(我的订单)
-(void)requestHttpsForOrderList{
    
    if (dataArr.count>0) {
        [dataArr removeAllObjects];
    }
    if (goodDataArr.count>0) {
        [goodDataArr removeAllObjects];
    }
    
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"order_list" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        NSLog(@"%@---%@",responseObject,responseObject[@"retData"][@"msg"]);
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForOrderList];
            });
        }else{
            NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
            if ([codeStr isEqualToString:@"100"]) {
                //创建队列
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_group_t queueGroup = dispatch_group_create();
                //1、重新获取signkey
                dispatch_group_async(queueGroup, aQueue, ^{
                    [AppUtils requestHttpsSignKey];
                });
                //2、重新请求
                dispatch_group_async(queueGroup, aQueue, ^{
                    [self requestHttpsForOrderList];
                });
            }else{
                if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                    
                    moreDataStr=responseObject[@"retData"][@"pager"][@"page_next"];
                    
                    NSArray *orderArr=responseObject[@"retData"][@"orders"];
                    if (orderArr.count<=0) {
                        [self.view bringSubviewToFront:tipLabel];
                    }else{
                        [self.view bringSubviewToFront:myTableview];
                    }
                    for (NSDictionary *goodDic in orderArr) {
                        UserOrdersModel *model=[[UserOrdersModel alloc]init];
                        [model jsonDataForDictionary:goodDic];
                        [goodDataArr addObject:model.goodModelArr];
                        [dataArr addObject:model];
                    }
                    [myTableview reloadData];
                    [myTableview.mj_header endRefreshing];
                }else{
                    
                }
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//（取消订单请求）
-(void)requestHttpsForCancelOrder:(NSString *)orderId{
    time_t now;
    time(&now);
    NSString *time_stamp  = [NSString stringWithFormat:@"%ld", now];
    NSString *nonce_str	= [MMGSign randomNumber];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:@"1" forKey:@"is_phone"];
    [dict setObject:@"cancel_order" forKey:@"act"];
    [dict setObject:[AppUtils getValueWithKey:User_ID] forKey:@"user_id"];
    [dict setObject:orderId forKey:@"order_id"];
    [dict setObject:time_stamp forKey:@"stamp"];
    [dict setObject:nonce_str forKey:@"noncestr"];
    [dict setObject:[AppUtils getValueWithKey:Verson_Key] forKey:@"signsn"];
    NSString *sign=[[MMGSign shareSign] getSign:[AppUtils getValueWithKey:Sign_Key] forDictionary:dict];
    [dict setObject:sign forKey:@"sign"];
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:@"user.php?"] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *codeStr=[NSString stringWithFormat:@"%@",responseObject[@"errCode"]];
        if ([codeStr isEqualToString:@"100"]) {
            //创建队列
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t queueGroup = dispatch_group_create();
            //1、重新获取signkey
            dispatch_group_async(queueGroup, aQueue, ^{
                [AppUtils requestHttpsSignKey];
            });
            //2、重新请求
            dispatch_group_async(queueGroup, aQueue, ^{
                [self requestHttpsForCancelOrder:selectedId];
            });
        }else{
            if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
                [self requestHttpsForOrderList];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

//更多请求
-(void)requestHttpsMoreProduct:(NSString *)moreHref{
    NSDictionary *dict=@{
                         @"is_phone":@"1",
                         @"is_nosign":@"1",
                         @"user_id":[AppUtils getValueWithKey:User_ID]
                         };
    [[HttpRequest defaultClient] requestWithPath:[DefaultHost stringByAppendingString:moreHref] method:HttpRequestPost parameters:dict prepareExecute:^{
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"retMsg"] isEqualToString:@"success"]) {
            
            moreDataStr=responseObject[@"retData"][@"pager"][@"page_next"];
            
            NSArray *orderArr=responseObject[@"retData"][@"orders"];
            
            for (NSDictionary *goodDic in orderArr) {
                UserOrdersModel *model=[[UserOrdersModel alloc]init];
                [model jsonDataForDictionary:goodDic];
                [goodDataArr addObject:model.goodModelArr];
                [dataArr addObject:model];
            }
        }
        
        [myTableview reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - dealloc
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
